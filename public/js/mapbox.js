var map;
var marker;

function inicializar_mapBox(id_mapa, lng = -80.7214469337831, lat = -0.9485041219484316) {

    console.log(lng + lat);
    mapboxgl.accessToken = 'pk.eyJ1IjoiY2dwZzk0IiwiYSI6ImNrOHl6cDByMDA0ZTAzZG55ZDNhNzR6ZzQifQ.S6YdKKPLdCVTVd-xxvim0g';
    map = new mapboxgl.Map({
        container: id_mapa, // container id
        style: 'mapbox://styles/cgpg94/ck8yzs10c04nf1io4oth7zjuf', // stylesheet location
        center: [lng, lat], // starting position [lng, lat]
        zoom: 16, // starting zoom,
        attributionControl: false
    });
    marker = new mapboxgl.Marker({
        draggable: true
    }).setLngLat([lng, lat]).addTo(map);

    function onDragEnd() {
        var lngLat = marker.getLngLat();

        console.log('Longitude: ' + lngLat.lng + 'Latitude: ' + lngLat.lat);
        $('#longitud').val(lngLat.lng);
        $('#latitud').val(lngLat.lat);
        try {
            document.getElementById('mapa_lat_lng').value = '[' + lngLat.lat + ',' + lngLat.lng + ']';
        } catch (error) {

        }
        // console.log(lngLat);
        // mapa_lat_lng
    }
    marker.on('dragend', onDragEnd);


    map.on('click', function(e) {
        marker = new mapboxgl.Marker({
            draggable: true
        }).setLngLat([e.lngLat.lng, e.lngLat.lat]);
        // console.log(e.lngLat);
        $('#longitud').val(e.lngLat.lng);
        $('#latitud').val(e.lngLat.lat);
    });

}