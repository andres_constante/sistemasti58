var map = null;
var myMarker;
var myLatlng;

function initMap(lat, lng) {
  myLatlng = new google.maps.LatLng(lat, lng);

  let myOptions = {
    zoom: 18,
    zoomControl: true,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

  var input = document.getElementById("direccion");
  //   var autocomplete = new google.maps.places.Autocomplete(input);

  myMarker = new google.maps.Marker({
    position: myLatlng,
    draggable: true
  });

  myMarker.setMap(map);

  google.maps.event.addListener(myMarker, "dragend", function(event) {
    $("#ubicacion").val(event.latLng.lat() + "," + event.latLng.lng());
  });

  google.maps.event.addListener(map, "click", function(event) {
    myMarker.setPosition(event.latLng);
    $("#ubicacion").val(event.latLng.lat() + "," + event.latLng.lng());
  });

  //   autocomplete.addListener("place_changed", function () {
  //     var place = autocomplete.getPlace();
  //     console.log('Place: ', place)
  //   });
}

function guardar(event) {
  event.preventDefault();

  if (confirm("Seguro de continuar?")) {
    $("#btn_guardar").attr("disabled", true);
    $("#form-tram").submit();
  } else return;
}

$(document).ready(function() {
  $("#position").css("display", "none");
  initMap(-0.9484151471928869, -80.72165359236527);

  $('input#cedula_remitente').keypress(function (event) {
      if (this.value.length === 10) {
        return false;
      }
  });

  let form = document.getElementById("form-tram");
  form.addEventListener("submit", guardar, true);

  $(".numeros-letras-guion").on("input", function() {
    this.value = this.value.replace(/[^0-9a-zA-Z-]/g, "");
  });

  $(".mayuscula").on("keyup", function(e) {
    $(this).val(
      $(this)
        .val()
        .toUpperCase()
    );
  });

  $('#fecha_inicio').focusout(function() {
    fecha_inicio_agenda = $("#fecha_inicio").val();
    if(fecha_inicio_agenda != '')
    {
        $("#fecha_fin").val(fecha_inicio_agenda) 
    }
  });

  $(".consultar_fecha").focusout(function(e) {
    fecha_inicio_agenda = $("#fecha_inicio").val();
    fecha_fina_agenda = $("#fecha_fin").val();
    if (fecha_fina_agenda == "") {
      fecha_fina_agenda = "0";
    }
    if (fecha_inicio_agenda == "") {
      fecha_inicio_agenda = "0";
    }
    
    e.preventDefault();

    $.ajax({
      type: "GET",
      url: "/coordinacioncronograma/verficar_existente/" + fecha_inicio_agenda + "/" +fecha_fina_agenda +"/0",
      data: null,
      dataType: "json",
      success: function(response) {
        if (response.estado == "false") {
         
            Swal.fire({
              title: response.msg,
              text: "¿Desea agregarlo a una lista de espera?",
              html: response.actividades,
              showCancelButton: true,
              confirmButtonText: "SI",
              cancelButtonText: "NO",
              showLoaderOnConfirm: true,
              preConfirm: codigo => {
                if (codigo) {
                  $("<input>")
                    .attr({
                      type: "hidden",
                      id: "en_espera",
                      name: "en_espera",
                      value: "SI"
                    })
                    .appendTo("form");
                } else {
                  try {
                    $("#en_espera").remove();
                  } catch (error) {}
                }
              },
              allowOutsideClick: () => !Swal.isLoading()
            });
          
        } else {
          try {
            $("#en_espera").remove();
          } catch (error) {}
        }
      }
    });
  });

  $("#direccion").on("blur", function() {
    let direccion = $("#direccion").val();

    if (direccion != "" || direccion != null) {
      // geolocate();
      $.ajax({
        type: "GET",
        url: `/tramitesalcaldia/obtener_ubicacion/${direccion}`,
        beforeSend: function() {
          $("#position").css("display", "block");
        },
        error: function(err) {
          $("#position").css("display", "block");
          $(".loader").fadeOut("slow");
          toastr["error"]("No se encontró la ubicación, intente de nuevo.");
          $("#direccion").focus();
          // alert();
        }
      }).done(function(data) {
        // console.log(data["status"]);
        // console.log(direccion);
        if (data["status"] == "OK") {
          // console.log(data);
          let lat = data["results"][0].geometry["location"].lat;
          let lng = data["results"][0].geometry["location"].lng;
          $("#ubicacion").val(`${lat},${lng}`);
          initMap(lat, lng);
        } else {
          toastr["error"]("No se encontró la ubicación, intente de nuevo.");
          $("#direccion").focus();
        }
        $("#position").css("display", "block");
        $(".loader").fadeOut("slow");
      });
    } else $("#direccion").focus();
  });

  $('#id_parroquia').change(function() {
    let parroquia = $(this).val();
    let barrio = $('#id_barrio');
    
    if(parroquia == "")
    {
      barrio.find('option').remove();
      barrio.append('<option value="">Escoja opción...</option>');
    }
    else
    {
      $.ajax({
        type: 'GET',
        url: "/tramitesalcaldia/obtenerbarrios/" + parroquia
      }).done(function(data){
        barrio.find('option').remove();
        barrio.append('<option value="">Escoja opción...</option>');
        $(data).each(function(i, v) {
            barrio.append('<option value="' + v.id + '">' + v.barrio + '</option>');
        })
      });
    }
  })

  $("#cedula_remitente").on("keyup", function() {
    let cedula = $(this).val();
    let fecha = $("#fecha_ingreso").val();
    console.log('aqui')

    if (cedula.length === 10) {
      $.ajax({
        type: "GET",
        url: "/tramitesalcaldia/consultar_cedula/" + cedula,
        beforeSend: function() {
          $("#position").css("display", "block");
        },
        error: function(err) {
          $("#position").css("display", "block");
          $(".loader").fadeOut("slow");
        }
      }).done(function(data) {
        if (data.ok) {
          if (data.message.id) {
            $("#remitente").val(data.message.name);
            $("#fecha_nacimiento").val(data.message.fecha_nacimiento);
            $("#correo_electronico").val(data.message.email);
            $("#telefono").val(data.message.telefono);
          } else {
            $("#remitente").val(data.message.nombre);
            let info = data.message.fechaNacimiento.split("/");
            let fechaNacimiento = info[2] + "-" + info[1] + "-" + info[0];
            $("#fecha_nacimiento").val(fechaNacimiento);
          }
          $("#position").css("display", "block");
          $(".loader").fadeOut("slow");
        } else {
          toastr["error"](data.message);
          $("#remitente").val("");
          $("#fecha_nacimiento").val("");
          $("#position").css("display", "block");
          $(".loader").fadeOut("slow");
        }
      });
    }
  });
});
