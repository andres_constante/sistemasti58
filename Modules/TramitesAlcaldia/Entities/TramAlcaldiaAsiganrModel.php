<?php

namespace Modules\TramitesAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabModel;

class TramAlcaldiaAsiganrModel extends Model
{
    protected $table = 'tram_peticiones_direccion_asignada';
    protected $fillable = ['id_cab', 'id_direccion', 'estado', 'direccion_solicitante', 'respondido', 'observacion'];
    public $timestamps = true;

    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                
            //'numero_tramite'=>'required|unique:tram_peticiones_cab'. ($id ? ",id,$id" : ''),
            // 'nombres'=>'required'/*,
            // 'direccion'=>'required',
            // 'telefono'=>'required',
            // 'correo'=>'required|numeric'*/
        ], $merge);
    }

    public function cabecera()
    {
        return $this->belongsTo(TramAlcaldiaCabModel::class, 'id_cab');
    }
}
