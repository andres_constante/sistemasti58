<?php

namespace Modules\TramitesAlcaldia\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AsignadoTramiteInternoModel extends Model
{
    protected $table = 'tma_tram_interno_asignados';
    protected $fillable = ['id_tramite', 'id_usuario_asignado', 'observacion', 'estado','tipo_firma','tipo_envio'];

    public function tramite()
    {
        return $this->belongsTo(TramiteInternoModel::class, 'id_tramite');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'id_usuario_asignado');
    }
}
