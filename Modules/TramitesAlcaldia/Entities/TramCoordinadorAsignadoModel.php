<?php

namespace Modules\TramitesAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;

class TramCoordinadorAsignadoModel extends Model
{
    protected $table = 'tram_coordinador_asignado';
    protected $fillable = ['id_cab', 'id_coordinador', 'id_direccion', 'estado', 'observacion'];
}
