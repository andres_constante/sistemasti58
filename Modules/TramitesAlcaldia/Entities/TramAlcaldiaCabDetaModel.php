<?php namespace Modules\Tramitesalcaldia\Entities;
   
use Illuminate\Database\Eloquent\Model;

class TramAlcaldiaCabDetaModel extends Model {

    protected $fillable = [];
    protected $table="tram_peticiones_cab_deta";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                //'numero_tramite'=>'required|unique:tram_peticiones_cab'. ($id ? ",id,$id" : ''),
                // 'nombres'=>'required'/*,
                // 'direccion'=>'required',
                // 'telefono'=>'required',
                // 'correo'=>'required|numeric'*/
            ], $merge);
        } 

}