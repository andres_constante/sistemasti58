<?php

namespace Modules\TramitesAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;

class AreasModel extends Model
{
    protected $fillable = ['id', 'id_direccion', 'area', 'estado'];
    protected $table = 'tram_peticiones_areas';
}
