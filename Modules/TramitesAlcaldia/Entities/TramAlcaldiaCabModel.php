<?php namespace Modules\Tramitesalcaldia\Entities;

use App\ArchivosModel;
use Illuminate\Database\Eloquent\Model;
use Modules\TramitesAlcaldia\Entities\AnexosModel;
use Modules\TramitesAlcaldia\Entities\AreasModel;
use Modules\TramitesAlcaldia\Entities\TipoTramiteModel;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaAsiganrModel;

class TramAlcaldiaCabModel extends Model
{
    protected $guarded = ['id'];
    protected $table="tram_peticiones_cab";

    public static function rules ($id=0, $merge=[])
    {
        return array_merge([
            'numtramite'=>'required|unique:tram_peticiones_cab'. ($id ? ",id,$id" : ''),
            'remitente'=>'required',
            'asunto'=>'required',
            'peticion'=>'required',
            // 'correo'=>'required|numeric'
            'prioridad'=>'required',
            // 'direccion_atender'=>'required',
            'cedula_remitente' => 'required',
            // 'fecha_fin'=>'required',
            'fecha_ingreso'=>'required',
            'id_parroquia'=>'required',
            'id_barrio'=>'required'
        ], $merge);
    }

    public static function rulesToAgenda ($id=0, $merge=[]) {
        return array_merge([                
            'numtramite'=>'required|unique:tram_peticiones_cab'. ($id ? ",id,$id" : ''),
            'remitente'=>'required',
            'asunto'=>'required',
            'peticion'=>'required',
            'fecha_ingreso'=>'required'
        ], $merge);
    }

    public function asignados()
    {
        return $this->hasMany(TramAlcaldiaAsiganrModel::class, 'id_cab');
    }

    public function area()
    {
        return $this->belongsTo(AreasModel::class, 'id_area');
    }

    public function tipoTramite()
    {
        return $this->belongsTo(TipoTramiteModel::class, 'tipo_tramite');
    }

    public function archivos()
    {
        return $this->hasMany(ArchivosModel::class, 'id_referencia');
    }

    public function anexos()
    {
        return $this->hasMany(AnexosModel::class, 'id_referencia');
    }

    public function analistas()
    {
        return $this->hasMany(TramAlcaldiaAsiganrModel::class, 'id_cab');
    }
}