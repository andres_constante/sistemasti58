<?php

namespace Modules\TramitesAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;

class TramTipoTramiteModel extends Model
{
    protected $fillable = [];
    protected $table = 'tram_tipo_tramite';
}
