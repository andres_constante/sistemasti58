<?php

namespace Modules\TramitesAlcaldia\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TramiteInternoModel extends Model
{
    protected $table = 'tma_tram_interno';

    protected $fillable = [
        'id_remitente', 'numtramite',
        'tipo_tramite', 'asunto',
        'peticion', 'observacion', 'prioridad', 'documento',
        'delegados_para', 'delegados_copia', 'tipo_firma', 'tipo_envio','fecha','modifico_fecha','numtramite_externo','nombre_informe'
    ];

    public function remitente()
    {
        return $this->belongsTo(User::class, 'id_remitente');
    }

    public function asignados()
    {
        return $this->hasMany(AsignadoTramiteInternoModel::class, 'id_tramite');
    }
}
