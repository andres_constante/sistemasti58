<?php

namespace Modules\TramitesAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;

class AnexosModel extends Model
{
    protected $table = 'tmov_tram_anexos';
    protected $fillable = ['usuario_id', 'id_referencia', 'ruta', 'nombre', 'hojas', 'descripcion', 'tipo'];
}
