<?php

namespace Modules\TramitesAlcaldia\Http\Controllers;

use App\ArchivosModel;
use App\Http\Controllers\FirmaElectronicaController;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\TramitesAlcaldia\Entities\AnexosModel;
use Modules\TramitesAlcaldia\Entities\AsignadoTramiteInternoModel;
use Modules\TramitesAlcaldia\Entities\HistorialInternosModel;
use Modules\TramitesAlcaldia\Entities\TramInternoAutorizacionesModel;
use Modules\TramitesAlcaldia\Entities\TramInternoCodigosModel;
use Modules\TramitesAlcaldia\Entities\TramiteInternoModel;
use stdClass;

class TramitesInternosController extends Controller
{
    public $configuraciongeneral = ['Gestión de trámites internos', 'tramitesinternos.store', 'tramitesinternos.form', 'crear', '', 6 => ''];

    protected $notificacion;
    private $archivos_path;
    private $nombre_documento;

    var $prioridad = [
        'ALTA' => 'ALTA',
        'MEDIA' => 'MEDIA',
        'BAJA' => 'BAJA'
    ];

    var $tipo_tramite = [
        'MEM' => 'MEMO',
        'OFI' => 'OFICIO',
        'INF' => 'INFORME'
    ];

    var $tipo_tramite_analistas = [
        'MEM' => 'MEMO',
        'INF' => 'INFORME'
    ];


    const DIRECTORES = [
        0 => 3,
        1 => 9,
        2 => 5,
        3 => 15,
        4 => 16,
        5 => 21,
        6 => 22,
        7 => 25,
        8 => 37,
        9 => 42,
        10 => 49,
        11 => 52,
        12 => 54,
        13 => 12,
        14 => 44,
        15 => 4,
        16 => 45,
        17 => 58,
        18 => 56,
        19 => 50,
        20 => 67,
        21 => 66,
        22 => 70,
        23=>55
    ];

    const ANALISTA = [
        56,
        63,
        53,
        51,
        48,
        47,
        50,
    ];

    const DIRECCIONES_INA = [
        0 => 23,
        1 => 32,
        2 => 33,
        3 => 41
    ];

    var $titulos = [
        'Sr.' => 'Sr.',
        'Sr. Abogado' => 'Sr. Abogado',
        'Sr. Analista' => 'Sr. Analista',
        'Sr. Arquitecto' => 'Sr. Arquitecto',
        'Sr. Capitán' => 'Sr. Capitán',
        'Sr. Contador' => 'Sr. Contador',
        'Sr. Coronel' => 'Sr. Coronel',
        'Sr. Doctor' => 'Sr. Doctor',
        'Sr. Economista' => 'Sr. Economista',
        'Sr. Ingeniero' => 'Sr. Ingeniero',
        'Sr. Licenciado' => 'Sr. Licenciado',
        'Sr. Magister' => 'Sr. Magister',
        'Sr. Odontólogo' => 'Sr. Odontólogo',
        'Sr. Psicólogo' => 'Sr. Psicólogo',
        'Sr. Tecnólogo' => 'Sr. Tecnólogo',
        'Sra.' => 'Sra.',
        'Sra. Abogada' => 'Sra. Abogada',
        'Sra. Analista' => 'Sra. Analista',
        'Sra. Arquitecta' => 'Sra. Arquitecta',
        'Sra. Capitán' => 'Sra. Capitán',
        'Sra. Contadora' => 'Sra. Contadora',
        'Sra. Coronel' => 'Sra. Coronel',
        'Sra. Doctora' => 'Sra. Doctora',
        'Sra. Economista' => 'Sra. Economista',
        'Sra. Ingeniera' => 'Sra. Ingeniera',
        'Sra. Licenciada' => 'Sra. Licenciada',
        'Sra. Magister' => 'Sra. Magister',
        'Sra. Odontóloga' => 'Sra. Odontóloga',
        'Sra. Psicóloga' => 'Sra. Psicóloga',
        'Sra. Tecnóloga' => 'Sra. Tecnóloga',
        'Srta.' => 'Srta.',
        'Srta. Abogada' => 'Srta. Abogada',
        'Srta. Analista' => 'Srta. Analista',
        'Srta. Arquitecta' => 'Srta. Arquitecta',
        'Srta. Capitán' => 'Srta. Capitán',
        'Srta. Contadora' => 'Srta. Contadora',
        'Srta. Coronel' => 'Srta. Coronel',
        'Srta. Doctora' => 'Srta. Doctora',
        'Srta. Economista' => 'Srta. Economista',
        'Srta. Ingeniera' => 'Srta. Ingeniera',
        'Srta. Licenciada' => 'Srta. Licenciada',
        'Srta. Magister' => 'Srta. Magister',
        'Srta. Odontóloga' => 'Srta. Odontóloga',
        'Srta. Psicóloga' => 'Srta. Psicóloga',
        'Srta. Tecnóloga' => 'Srta. Tecnóloga',
        // 'Ing.' => 'Ing.',
        // 'Lic.' => 'Lic.',
        // 'Eco.' => 'Eco.',
        // 'Mgs.' => 'Mgs.',
    ];

    public function __construct(
        NotificacionesController $notificacion,
        FirmaElectronicaController $firmaElectronicaController
    ) {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
        $this->firmaElectronicaController = $firmaElectronicaController;
        $this->archivos_path = public_path('archivos_sistema');
    }

    /**TRÁMITES ELABORADOS */
    public function index()
    {
        $this->configuraciongeneral[4] = 'Creación de trámite';
        return view('tramitesalcaldia::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'SI',
            'ajax' => 'tramites.ajax',
            'estado' => 'INICIAL',
            'tipo_envio' => 'ENVIAR'
        ]);
    }

    public function indexAjax(Request $request)
    {
        $estado = $request->estado;
        $tipo_envio = $request->tipo_envio;
        $query = TramiteInternoModel::with('remitente', 'asignados.usuario');
        if ($request->tipo_envio) {
            $query = $query->where(['tipo_envio' => $tipo_envio]);
        }
        // dd($query->get());
        if ($estado != 'INICIAL') {
            $tramites = $query->whereHas('asignados', function ($q) use ($estado) {
                $q->where(['id_usuario_asignado' => Auth::user()->id, 'estado' => $estado]);
            });
        } else {
            $tramites = $query->where(['id_remitente' => Auth::user()->id]);
        }
        return DataTables::of($tramites)->addColumn('asignados', function ($tramite) {
            if ($tramite->tipo_tramite == 'MEMO' || $tramite->tipo_tramite == 'INFORME') {
                return $tramite->asignados->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO'])->map(function ($asignado) {
                    return $asignado->usuario->name;
                })->implode(', ');
            } else {
                $para = json_decode($tramite->delegados_para);
                $para = collect($para);
                return $tramite->asignados->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO'])->map(function ($asignado) {
                    return $asignado->usuario->name;
                })->implode(', ') . ', ' . $para->map(function ($asignado) {
                    return "$asignado->titulo $asignado->nombre  - $asignado->cargo";
                })->implode(', ');
            }
        })->addColumn('copias', function ($tramite) {
            if ($tramite->tipo_tramite == 'MEMO' || $tramite->tipo_tramite == 'INFORME') {
                return $tramite->asignados->where('estado', '=', 'COPIA')->map(function ($asignado) {
                    return $asignado->usuario->name;
                })->implode(', ');
            } else {
                $copia = json_decode($tramite->delegados_copia);
                $copia = collect($copia);
                return $tramite->asignados->where('estado', '=', 'COPIA')->map(function ($asignado) {
                    return $asignado->usuario->name;
                })->implode(', ') . ', ' . $copia->map(function ($asignado) {
                    return "$asignado->titulo $asignado->nombre  - $asignado->cargo";
                })->implode(', ');
            }
        })->addColumn('action', function ($tramite) use ($estado) {
            if ($tramite->tipo_tramite == 'MEMO' || $tramite->tipo_tramite == 'INFORME') {
                if ($estado == 'INICIAL') {
                    $acciones = '<a href="' . route('tramitesinternos.edit', Crypt::encrypt($tramite->id)) . '" class="">
                        <i class="fa fa-edit"></i></i></a> ';
                    if ($tramite->tipo_envio == 'ENVIAR') {
                        if (Auth::user()->id != 1182) {
                        }
                        $acciones = '<a href="' . route('internos.show', ['id' => $tramite->id]) . '" class="divpopup" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left" target="_blank" onclick="popup(this)">
                        <i class="fa fa-newspaper-o"></i></i></a>';
                    }
                } elseif ($estado == 'ASIGNADO') {
                    $acciones = '<a href="' . route('internos.show', ['id' => $tramite->id]) . '" class="divpopup" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left" target="_blank" onclick="popup(this)">
                    <i class="fa fa-newspaper-o"></i></i></a> <a href="' . route('tramitesinternos.create') . '?tramite=' . Crypt::encrypt($tramite->id) . '" class="" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">
                    <i class="fa fa-history"></i></i></a> <a href="#" class="" onclick="archivarTramiteInterno(' . $tramite->id . ')">
                    <i class="fa fa-archive"></i></i></a>';
                } elseif ($estado == 'COPIA') {
                    // $acciones = '<a href="' . route('copia.edit', Crypt::encrypt($tramite->id)) . '" class="btn btn-xs btn-primary">
                    // <i class="fa fa-newspaper-o"></i></i></a>';
                    $acciones = '';
                } elseif ($estado == 'ARCHIVADO') {
                    $acciones = '<a href="' . route('internos.show', ['id' => $tramite->id]) . '" class="divpopup" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left" target="_blank" onclick="popup(this)">
                    <i class="fa fa-newspaper-o"></i></i></a>';
                } elseif ($estado == 'CONTESTADO') {
                    $acciones = '<a href="' . route('revision.edit', Crypt::encrypt($tramite->id)) . '" class="btn btn-xs btn-primary">
                    <i class="fa fa-newspaper-o"></i></i></a>';
                } else {
                    if (Auth::user()->id != 1182) {
                    }
                    $acciones = '';
                }
                // dd($tramite->tipo_envio);
                if ($tramite->tipo_envio == 'BORRADOR') {
                    $acciones .= '<a href="' . route('internos.show', ['id' => $tramite->id]) . '" class="divpopup" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left" target="_blank" onclick="popup(this)">
                    <i class="fa fa-newspaper-o"></i></i></a> <a class="" onclick="eliminarBorrador(\'' . Crypt::encrypt($tramite->id) . '\')"><i class="fa fa-trash-o"></i></i></a>';
                }
            } elseif ($tramite->tipo_tramite = 'OFICIO') {
                $acciones = '';
                $acciones = '<a href="' . route('tramitesinternos.edit', Crypt::encrypt($tramite->id)) . '" class="btn btn-xs btn-primary">
                <i class="fa fa-newspaper-o"></i></i></a>';
                if ($tramite->tipo_envio == 'ENVIAR') {
                    $acciones = '';
                }
                if ($tramite->tipo_envio == 'BORRADOR') {


                    $acciones .= '<a class="btn btn-xs btn-danger" onclick="eliminarBorrador(\'' . Crypt::encrypt($tramite->id) . '\')"><i class="fa fa-trash-o"></i></i></a>';
                }
            } else $acciones = '';

            return $acciones;
        })->addColumn('memo', function ($tramite) {
            if ($tramite->documento) {
                $memo = '<a href="' . asset('archivos_firmados/' . $tramite->documento) . '" class="btn btn-success dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>';
            } else {
                $memo = '';
            }
            return $memo;
        })->addColumn('anexos', function ($tramite) {
            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $tramite->id, 'tipo' => 11])->get();
            $archivos = '';

            foreach ($anexos as $anexo) {
                $archivos .= '<a href="' . asset('archivos_sistema/' . $anexo->ruta) . '" class="btn btn-sm btn-info dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>&nbsp;&nbsp;';
            }

            return $archivos;
        })->rawColumns(['memo', 'action', 'anexos'])->make(true);
    }

    public function oficios()
    {
        $this->configuraciongeneral[4] = 'Oficios creados';
        return view('tramitesalcaldia::tramitesinternos.oficios', [
            'configuraciongeneral' => $this->configuraciongeneral,
        ]);
    }

    public function oficiosAjax()
    {
        $oficios = TramiteInternoModel::with('remitente')
            ->where(['tipo_tramite' => 'OFICIO', 'tipo_envio' => 'ENVIAR', 'id_remitente' => Auth::user()->id]);

        return DataTables::of($oficios)->addColumn('asignados', function ($oficios) {
            return '';
        })->addColumn('copias', function ($oficios) {
            return '';
        })->addColumn('oficio', function ($oficios) {
            $oficion = '<a href="' . asset('archivos_firmados/' . $oficios->documento) . '" class="btn btn-success dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
            <i class="fa fa-file-pdf-o"></i></i></a>';
            return $oficion;
        })->addColumn('anexos', function ($oficios) {
            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $oficios->id, 'tipo' => 11])->get();
            $archivos = '';

            foreach ($anexos as $anexo) {
                $archivos .= '<a href="' . asset('archivos_sistema/' . $anexo->ruta) . '" class="btn btn-sm btn-info dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>&nbsp;&nbsp;';
            }

            return $archivos;
        })->rawColumns(['memo', 'action', 'anexos'])->make(true);
    }

    public function create()
    {
        $tramite = null;
        try {
            if (request()->tramite) {
                $id = Crypt::decrypt(request()->tramite);
                $tramite = TramiteInternoModel::findOrFail($id);
            }
        } catch (\Exception $ex) {
            return back();
        }

        $tabla = new TramiteInternoModel;
        $tabla['destinatarios'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
            ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
            // ->select('users.id', 'users.name')
            ->where('users.id', '<>', Auth::user()->id)
            // ->whereIn('ap.id', [3, 5, 9, 21, 22])->pluck('users.name', 'users.id');
            ->whereIn('ap.id', self::DIRECTORES)->orderBy('users.name')->pluck('users.name', 'users.id');

        $user = UsuariosModel::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')
            ->where('users.id', Auth::user()->id)->first();

        $numtramite = 'MTA-' . $user->mta . '-' . 'MEM' . '-' . date('dmYHi');
        $analista = collect(UsuariosModel::ANALISTAS)->contains(Auth::user()->id_perfil);

        $tabla['prioridades'] = [null => 'Elija una opción'] + $this->prioridad;
        $tabla['tipo_tramite'] = $analista ? $this->tipo_tramite_analistas : $this->tipo_tramite;
        $tabla['titulos'] = [null => 'Elija una opción'] + $this->titulos;

        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[5] = '<button class="btn btn-primary" type="submit" style="float:right; margin-right: 30px;" id="btn_guardar">
                Guardar</button>';

        if (in_array(Auth::user()->id_perfil, self::ANALISTA)) {
            $no_director = true;
        } else {
            $no_director = null;
        }
        // dd(in_array(Auth::user()->id_perfil, self::ANALISTA));

        return view('tramitesalcaldia::tramitesinternos.create', [
            'tabla' => $tabla,
            'configuraciongeneral' => $configuraciongeneral,
            'numtramite' => $numtramite,
            'no_director' => $no_director,
            'responder' => $tramite
        ]);
    }
    public function consultardireccion()
    {
        # code...
        return direccionesModel::where('id', Auth::user()->id_direccion)->first();
    }

    public function store(Request $request)
    {
        if ($request->tipo_tramite == 'MEM' || $request->tipo_tramite == 'INF') {
            $validaciones = [
                'numtramite' => 'required|unique:tma_tram_interno,numtramite',
                'asignados' => 'required',
                'asunto' => 'required',
                'peticion' => 'required',
            ];
            if ($request->tipo_envio == 'ENVIAR') {
                $validaciones['documento'] = 'required';
            }
            $validations = Validator::make($request->all(), $validaciones);
        } else {
            $validaciones = [
                'numtramite' => 'required|unique:tma_tram_interno,numtramite',
                // 'delegados_para' => 'required',
                'asunto' => 'required',
                'peticion' => 'required',
            ];
            if (!$request->asignados) {
                // dd($request->asignados);
                $array_validaciones['delegados_para'] = 'required';
            }
            if ($request->tipo_envio == 'ENVIAR') {
                $validaciones['documento'] = 'required';
            }
            $validations = Validator::make($request->all(), $validaciones);
        }

        if ($validations->fails()) {
            return back()->withErrors($validations)->withInput();
        }
        return $this->guardar($request);
    }

    public function edit($id)
    {
        try {
            $ocultarPeticion = null;
            $id = Crypt::decrypt($id);
            $tabla = TramiteInternoModel::with('remitente')->findOrFail($id);

            $asignados = AsignadoTramiteInternoModel::where(['id_tramite' => $id])
                ->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO'])
                ->pluck('id_usuario_asignado');
            // dd($asignados);
            // dd($tabla);
            if ($tabla->tipo_tramite == 'OFICIO') {
                $tabla->tipo_tramite_a = 'OFI';
            }
            if ($tabla->tipo_tramite == 'INFORME') {
                $tabla->tipo_tramite_a = 'INF';
            }

            $asignados_ = AsignadoTramiteInternoModel::where(['id_tramite' => $id, 'estado' => 'ASIGNADO'])->count();

            $copias = AsignadoTramiteInternoModel::where(['id_tramite' => $id, 'estado' => 'COPIA'])->pluck('id_usuario_asignado');

            $tabla['destinatarios'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                // ->whereIn('ap.id', [3, 5, 9, 21, 22])->pluck('users.name', 'users.id');
                ->whereIn('ap.id', self::DIRECTORES)->orderBy('users.name')->pluck('users.name', 'users.id');

            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 11])->get();

            $tabla['prioridades'] = [null => 'Elija una opción'] + $this->prioridad;
            $tabla['titulos'] = $this->titulos;
            $tabla['tipo_tramite'] = $this->tipo_tramite;

            $tabla['desvinculados'] = AsignadoTramiteInternoModel::where(['id_tramite' => $id, 'estado' => 'DESVINCULADO'])->get();
            // dd(count($tabla['desvinculados']));
            $configuraciongeneral = $this->configuraciongeneral;
            $configuraciongeneral[1] = 'tramitesinternos.update';
            $configuraciongeneral[3] = 'editar';
            $configuraciongeneral[4] = 'Editar trámite';

            if (count($asignados) == $asignados_) {
                $configuraciongeneral[5] = '<button class="btn btn-primary" type="submit" style="float:right; margin-right: 30px;" id="btn_guardar">
                    Guardar</button>';
            } else {
                $ocultarPeticion = 'SI';
                $configuraciongeneral[6] = 'copia';

                $tabla['contestados'] = AsignadoTramiteInternoModel::with('usuario')
                    ->where(['id_tramite' => $id, 'estado' => 'CONTESTADO'])->get();

                $configuraciongeneral[5] = '';
            }

            if (in_array(Auth::user()->id_perfil, self::DIRECTORES)) {
                $no_director = null;
            } else {
                $no_director = true;
            }

            return view('tramitesalcaldia::tramitesinternos.create', [
                'tabla' => $tabla,
                'configuraciongeneral' => $configuraciongeneral,
                'asignados' => $asignados,
                'copias' => $copias,
                'anexos' => $anexos,
                'no_director' => $no_director,
                'ocultarPeticion' => $ocultarPeticion
            ]);
        } catch (\Exception $ex) {
            return back()->withErrors([' ' .  $ex->getMessage() . ' - ' . $ex->getLine()]);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $id = Crypt::decrypt($id);
            // dd($request->all());
            if ($request->tipo_tramite == 'OFI' || $request->tipo_tramite == 'INF') {
                $array_validaciones = [
                    'numtramite' => [
                        'required',
                        Rule::unique('tma_tram_interno')->ignore($id)
                    ],
                    'asunto' => 'required',
                    'peticion' => 'required'
                ];
                if ($request->tipo_envio == 'ENVIAR') {
                    $array_validaciones['documento'] = 'required';
                }
                if (!$request->asignados) {
                    // dd($request->asignados);
                    $array_validaciones['delegados_para'] = 'required';
                }
            } else {
                $array_validaciones = [
                    'numtramite' => [
                        'required',
                        Rule::unique('tma_tram_interno')->ignore($id)
                    ],
                    'asignados' => 'required',
                    'asunto' => 'required',
                    'peticion' => 'required'
                ];
                if ($request->tipo_envio == 'ENVIAR') {
                    $array_validaciones['documento'] = 'required';
                }
            }


            $validations = Validator::make($request->all(), $array_validaciones);
            if ($validations->fails()) {
                return back()->withErrors($validations)->withInput();
            }

            return $this->guardar($request, $id);
        } catch (\Exception $ex) {
            return back()->withErrors(['' . $ex->getMessage() . ' - ' . $ex->getLine()])->withInput();
        }
    }

    public function guardar($request, $id = null)
    {
        DB::beginTransaction();
        try {
            $message = $id == null ? 'Trámite creado correctamente' : 'Trámite actualizado correctamente';
            // $documento = $id == null ? $request->documento : TramiteInternoModel::find($id)->documento;
            if ($request->tipo_tramite == 'MEM' || $request->tipo_tramite == 'INF') {
                $tipo_tramite = $request->tipo_tramite == 'MEM' ? 'MEMO' : 'INFORME';
                // dd($tipo_tramite);
            } else {
                $tipo_tramite = 'OFICIO';
            }
            // dd($request->all());

            $tramite = TramiteInternoModel::updateOrCreate(['id' => $id], [
                'id_remitente' => Auth::user()->id,
                'numtramite' => $request->numtramite,
                'asunto' => $request->asunto,
                'peticion' => $request->peticion,
                // 'prioridad' => $request->prioridad,
                'documento' => $request->documento,
                'tipo_envio' => $request->tipo_envio,
                'fecha' => $request->fecha,
                'tipo_firma' => $request->tipo_firma,
                'tipo_tramite' => $tipo_tramite,
                'modifico_fecha' => $request->modifico_fecha,
                'numtramite_externo' => $request->numtramite_externo,
                'nombre_informe' => $request->nombre_informe
            ]);
            
            if($id == null)
            {
                if($request->tipo_envio == 'BORRADOR')
                {
                    $this->guardarHistorial($tramite, "{$tipo_tramite} creado correctamente, pendiente de envío");
                }
                else
                {
                    $this->guardarHistorial($tramite, "{$tipo_tramite} creado correctamente");
                }
            }
            else
            {
                $this->guardarHistorial($tramite, "{$tipo_tramite} enviado correctamente");
            }

            if ($tipo_tramite == 'OFICIO') {
                $tramite->delegados_para = $request->delegados_para;
                $tramite->delegados_copia = $request->delegados_copia;
                $tramite->save();
            }

            if ($request->has('anexos_json')) {
                $this->guardarAnexos($request->archivo_anexo, $request->anexos_json, $tramite->id);
            }

            // $ruta = 'tramitesalcaldia/revisionedit/' . Crypt::encrypt($tramite->id);
            // if ($tipo_tramite == 'MEMO') {
            // $ruta = 'tramitesalcaldia/revisionedit/recibidos';
            $ruta = 'tramitesalcaldia/tramitesinternos/recibidos';
            $mensaje = Auth::user()->name . " le ha asignado el trámite N° $tramite->numtramite";
            if ($request->asignados) {

                foreach ($request->asignados as $asignado) {
                    $usuarioAsignado = User::findOrFail($asignado);
                    AsignadoTramiteInternoModel::updateOrCreate(['id_tramite' => $tramite->id, 'id_usuario_asignado' => $asignado], [
                        'estado' => 'ASIGNADO'
                    ]);
                    if ($request->tipo_envio == "ENVIAR") {
                        $this->notificacion->notificacionesweb($mensaje . '', $ruta, $asignado, '2c438f');
                        $this->notificacion->EnviarEmail($usuarioAsignado->email, 'Trámite asignado', '', $mensaje, $ruta);
                    }
                }
            }

            // $ruta_ = 'tramitesalcaldia/copiaedit/' . Crypt::encrypt($tramite->id);
            $ruta_ = 'tramitesalcaldia/tramitesinternos/copia';
            $mensaje_ = Auth::user()->name . " le ha enviado con copia el trámite N° $tramite->numtramite";
            if ($request->copia) {
                foreach ($request->copia as $copia) {
                    AsignadoTramiteInternoModel::updateOrCreate(['id_tramite' => $tramite->id, 'id_usuario_asignado' => $copia], [
                        'estado' => 'COPIA'
                    ]);

                    $usuarioCopia = User::findOrFail($copia);

                    if ($request->tipo_envio == "ENVIAR") {
                        $this->notificacion->notificacionesweb($mensaje_ . '', $ruta_, $copia, '2c438f');
                        $this->notificacion->EnviarEmail($usuarioCopia->email, 'Trámite asignado', '', $mensaje, $ruta_);
                    }
                }
            }
            // }

            if ($request->tipo_envio == "BORRADOR" && Auth::user()->id == 1643) {
                $usuario = UsuariosModel::where('id', Auth::user()->id)->first();
                if ($usuario) {
                    $players = [];
                    $telefonos_personas = DB::table('tma_playerid')
                        ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                        ->where('u.id', $usuario->id)->get();
                    foreach ($telefonos_personas as $key => $v) {
                        array_push($players, $v->id_player);
                    }

                    $this->notificacion->notificacion($players, $tramite->id, 'Nuevo ' . $tramite->tipo_tramite . ' N° ' . $tramite->numtramite . ' borrador para revisar', 3, ['Nulo']);
                }
            }

            DB::commit();
            return redirect()->route('tramitesinternos.index')->with('message', $message);
        } catch (\Exception $ex) {
            DB::rollback();
            return back()->withErrors(['No se pudo guardar el trámite. ' . $ex->getMessage() . ' - ' . $ex->getLine()])->withInput();
        }
    }

    /**TRÁMITES RECIBIDOS */
    public function recibidos()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites recibidos';
        return view('tramitesalcaldia::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'ASIGNADO',
            'tipo_envio' => 'ENVIAR',

        ]);
    }

    public function revisionEdit($id)
    {
        try {
            $id = Crypt::decrypt($id);
            $tabla = TramiteInternoModel::with('remitente')->findOrFail($id);

            $respuesta = $tabla->asignados()->where('id_usuario_asignado', Auth::user()->id)->first();

            // $tabla['destinatarios'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
            // ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
            // // ->select('users.id', 'users.name')
            // ->where('users.id', '<>', Auth::user()->id)
            // ->whereIn('ap.id', [3, 5, 9, 21, 22])->pluck('users.name', 'users.id');

            $asignados = AsignadoTramiteInternoModel::where(['id_tramite' => $id])
                ->where(function ($q) {
                    $q->where(['estado' => 'ASIGNADO'])
                        ->orWhere(['estado' => 'CONTESTADO']);
                })->pluck('id_usuario_asignado');

            // $copias = AsignadoTramiteInternoModel::where([
            //     'id_tramite' => $id,
            //     'estado' => 'COPIA'
            // ])->pluck('id_usuario_asignado');
            $copias = [];
            // dd($copias);
            $tabla['destinatarios'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                // ->select('users.id', 'users.name')
                // ->where('users.id', '<>', Auth::user()->id)
                // ->whereIn('ap.id', [3, 5, 9, 21, 22])->pluck('users.name', 'users.id');
                ->whereIn('ap.id', self::DIRECTORES)->pluck('users.name', 'users.id');
            // User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
            //     ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
            //     ->whereIn('users.id', $asignados)->pluck('users.name', 'users.id');

            $tabla['copias'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                // ->select('users.id', 'users.name')
                ->where('users.id', '<>', Auth::user()->id)
                ->whereIn('ap.id', [3, 5, 9, 21, 22])->pluck('users.name', 'users.id');
            // User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
            //     ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
            //     ->whereIn('users.id', $copias)->pluck('users.name', 'users.id');

            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 11])->get();

            $tabla['prioridades'] = [null => 'Elija una opción'] + $this->prioridad;

            $configuraciongeneral = $this->configuraciongeneral;
            $configuraciongeneral[1] = 'revision.store';
            $configuraciongeneral[3] = 'editar';
            $configuraciongeneral[4] = 'Revisión de trámites asignados';

            if ($respuesta->estado == 'CONTESTADO') {
                $configuraciongeneral[5] = '';
            } else {
                $configuraciongeneral[5] = '<button class="btn btn-primary" type="submit" style="float:right; margin-right: 30px;" id="btn_guardar">
                    Enviar respuesta</button> <button type="button" onclick="archivarTramiteInterno(' . $tabla->id . ')" 
                    id="btn_archivar_tramite" class="btn btn-info flotar" style="float:right; margin-right: 7px;">
                      Archivar
                    </button> <button type="button" onclick="desvincularTramiteInterno(' . $tabla->id . ')" 
                    id="btn_devolver_tramite" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                      ¿No corresponde a la dirección?
                    </button>';
            }

            $configuraciongeneral[6] = 'readonly';

            if (in_array(Auth::user()->id_perfil,  self::DIRECTORES)) {
                $no_director = null;
            } else {
                $no_director = true;
            }
            return view('tramitesalcaldia::tramitesinternos.create', [
                'tabla' => $tabla,
                'configuraciongeneral' => $configuraciongeneral,
                'asignados' => $asignados,
                'copias' => $copias,
                'anexos' => $anexos,
                'no_director' => $no_director,
                'respuesta' => $respuesta
            ]);
        } catch (\Exception $ex) {
            return back()->withErrors([' ' .  $ex->getMessage() . ' - ' . $ex->getLine()]);
        }
    }

    public function guardarRevision(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $id = Crypt::decrypt($id);
            $tramite = TramiteInternoModel::findOrFail($id);

            $asignado = AsignadoTramiteInternoModel::where([
                'id_tramite' => $id, 'estado' => 'ASIGNADO',
                'id_usuario_asignado' => Auth::user()->id,
            ])->first();

            $asignado->respuesta = $request->peticion;
            $asignado->documento = $request->documento;
            $asignado->tipo_envio = $request->tipo_envio;
            $asignado->tipo_firma = $request->tipo_firma;
            $asignado->estado = 'CONTESTADO';
            $asignado->save();

            if ($request->has('archivo_anexo')) {
                $this->guardarAnexos($request->archivo_anexo, $request->anexos_json, $tramite->id);
            }

            $ruta = 'tramitesalcaldia/tramitesinternos/' . Crypt::encrypt($tramite->id) . '/edit';
            $mensaje = Auth::user()->name . ' ha respondido al trámite N° ' . $tramite->numtramite;


            if ($request->tipo_envio == "ENVIAR") {
                $this->notificacion->notificacionesweb($mensaje . '', $ruta, $tramite->id_remitente, '2c438f');
                $this->notificacion->EnviarEmail($tramite->remitente->email, 'Trámite respondido', '', $mensaje, $ruta);
            }

            $copias = AsignadoTramiteInternoModel::where([
                'id_tramite' => $id, 'estado' => 'COPIA'
            ])->get();

            $rutaCopia = 'tramitesalcaldia/copiaedit/' . Crypt::encrypt($tramite->id);
            foreach ($copias as $copia) {

                if ($request->tipo_envio == "ENVIAR") {
                    $this->notificacion->EnviarEmail($copia->usuario->email, 'Trámite respondido', '', $mensaje, $rutaCopia);
                    $this->notificacion->notificacionesweb($mensaje . '', $ruta, $copia->id_usuario_asignado, '2c438f');
                }
            }

            DB::commit();
            return redirect()->route('tramites.recibidos')->with('message', 'Se ha respondido al trámite correctamente');
        } catch (\Exception $ex) {
            DB::rollback();
            return back()->withErrors(['No se pudo guardar la respuesta del trámite. ' . $ex->getMessage() . ' - ' . $ex->getLine()])->withInput();
        }
    }

    public function archivarTramite(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            // $id = Crypt::decrypt($id);
            $tramite = TramiteInternoModel::findOrFail($id);
            $asignado = AsignadoTramiteInternoModel::where([
                'id_tramite' => $id, 'estado' => 'ASIGNADO',
                'id_usuario_asignado' => Auth::user()->id
            ])->first();

            // $asignados = AsignadoTramiteInternoModel::where(['id_tramite' => $id])
            // ->where(function($q) {
            //     $q->where(['estado' => 'ASIGNADO'])
            //     ->orWhere(['estado' => 'ARCHIVADO']);
            // })->count();
            
            $this->guardarHistorial($tramite, 'Trámite archivado');

            $asignado->observacion = $request->observacion;
            $asignado->estado = 'ARCHIVADO';
            $asignado->save();

            // $ruta = 'tramitesalcaldia/tramitesinternos/' . Crypt::encrypt($tramite->id) . '/edit';
            $ruta = 'tramitesalcaldia/tramitesinternos';
            $mensaje = Auth::user()->name . ' ha archivado el trámite N° ' . $tramite->numtramite;

            if ($tramite->tipo_envio == "ENVIAR") {
                $this->notificacion->EnviarEmail($tramite->remitente->email, 'Trámite archivado', '', $mensaje, $ruta);
                $this->notificacion->notificacionesweb($mensaje . '', $ruta, $tramite->id_remitente, '2c438f');
            }

            $copias = AsignadoTramiteInternoModel::where([
                'id_tramite' => $id, 'estado' => 'COPIA'
            ])->get();

            $rutaCopia = 'tramitesalcaldia/copiaedit/' . Crypt::encrypt($tramite->id);
            foreach ($copias as $copia) {
                if ($request->tipo_envio == "ENVIAR") {
                    $this->notificacion->notificacionesweb($mensaje . '', $ruta, $copia->id_usuario_asignado, '2c438f');
                    $this->notificacion->EnviarEmail($copia->usuario->email, 'Trámite respondido', '', $mensaje, $rutaCopia);
                }
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Se ha archivado el trámite correctamente'
            ]);
            // return redirect()->route('tramites.revision')->with('message', 'Se ha archivado el trámite correctamente');
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo archivar el trámite',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
            // return back()->withErrors(['No se pudo guardar la respuesta del trámite. ' . $ex->getMessage() . ' - ' . $ex->getLine()])->withInput();
        }
    }

    public function desvincularTramiteInterno(Request $request, $id)
    {
        if (request()->ajax()) {
            DB::beginTransaction();
            try {
                $tramite = TramiteInternoModel::findOrFail($id);
                $usuario = Auth::user();

                AsignadoTramiteInternoModel::where([
                    'id_tramite' => $id,
                    'id_usuario_asignado' => $usuario->id,
                    'estado' => 'ASIGNADO'
                ])->update([
                    'estado' => 'DESVINCULADO',
                    'observacion' => $request->observacion
                ]);

                $ruta = 'tramitesalcaldia/tramitesinternos/' . Crypt::encrypt($tramite->id) . '/edit';
                $mensaje = Auth::user()->name . ' se ha desvinculado del trámite N° ' . $tramite->numtramite . 'por el siguiente motivo: ' . $request->observacion;
                $mensajeWeb = Auth::user()->name . ' se ha desvinculado del trámite N° ' . $tramite->numtramite;

                $this->notificacion->notificacionesweb($mensajeWeb . '', $ruta, $tramite->id_remitente, '2c438f');
                $this->notificacion->EnviarEmail($tramite->remitente->email, 'Usuario desvinculado de trámite', '', $mensaje, $ruta);

                DB::commit();
                return response()->json([
                    'ok' => true,
                    'message' => 'Se ha desvinculado del trámite N° ' . $tramite->numtramite
                ]);
            } catch (\Exception $ex) {
                DB::rollback();
                return response()->json([
                    'ok' => false,
                    'message' => $ex->getMessage() . ' - ' . $ex->getLine()
                ]);
            }
        } else abort(401);
    }

    /**TRÁMITES RECIBIDOS CON COPIA */
    public function conCopia()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites recibidos con copia';
        return view('tramitesalcaldia::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'COPIA',
            'tipo_envio' => 'ENVIAR',
        ]);
    }

    public function copiaEdit($id)
    {
        try {
            $id = Crypt::decrypt($id);
            $tabla = TramiteInternoModel::with('remitente')->findOrFail($id);
            $asignados = AsignadoTramiteInternoModel::where(['id_tramite' => $id])
                ->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO'])->pluck('id_usuario_asignado');
            $copias = AsignadoTramiteInternoModel::where(['id_tramite' => $id, 'estado' => 'COPIA'])->pluck('id_usuario_asignado');
            $tabla['destinatarios'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                ->whereIn('users.id', $asignados)->pluck('users.name', 'users.id');

            $tabla['copias'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                ->whereIn('users.id', $copias)->pluck('users.name', 'users.id');

            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 11])->get();

            $tabla['prioridades'] = [null => 'Elija una opción'] + $this->prioridad;

            $tabla['contestados'] = AsignadoTramiteInternoModel::with('usuario')
                ->where(['id_tramite' => $id, 'estado' => 'CONTESTADO'])->get();

            $configuraciongeneral = $this->configuraciongeneral;
            $configuraciongeneral[1] = 'revision.store';
            $configuraciongeneral[3] = 'editar';
            $configuraciongeneral[4] = 'Revisión de trámites asignados';
            $configuraciongeneral[5] = '';
            $configuraciongeneral[6] = 'copia';
            // dd($configuraciongeneral);

            if (in_array(Auth::user()->id_perfil,  self::DIRECTORES)) {
                $no_director = null;
            } else {
                $no_director = true;
            }
            return view('tramitesalcaldia::tramitesinternos.create', [
                'tabla' => $tabla,
                'configuraciongeneral' => $configuraciongeneral,
                'asignados' => $asignados,
                'copias' => $copias,
                'anexos' => $anexos,
                'no_director' => $no_director,
                'editcopia' => 'SI'
            ]);
        } catch (\Exception $ex) {
            return back()->withErrors([' ' .  $ex->getMessage() . ' - ' . $ex->getLine()]);
        }
    }

    /**TRÁMITES CONTESTADOS */
    public function contestados()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites contestados';
        return view('tramitesalcaldia::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'CONTESTADO',
            'tipo_envio' => 'ENVIAR'
        ]);
    }

    /**TRÁMITES ARCHIVADOS */
    public function archivados()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites archivados';
        return view('tramitesalcaldia::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'ARCHIVADO',
            'tipo_envio' => 'ENVIAR'
        ]);
    }
    /**TRÁMITES BORRADORES */
    public function borradores()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites por enviar';
        return view('tramitesalcaldia::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'INICIAL',
            'tipo_envio' => 'BORRADOR'
        ]);
    }

    public function guardarAnexos($archivos, $info, $id_tramite)
    {
        $info = json_decode($info);
        $anexos = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.*', 'u.name')->where(['id_referencia' => $id_tramite, 'tipo' => 11])->get();
        // dd($info);
        foreach ($anexos as $key => $value) {
            $bandera = true;
            foreach ($info as $kk => $vv) {
                # code...
                if ($vv->id_anexo == $value->id) {
                    $bandera = false;;
                }
            }
            if ($bandera) {
                $upload = AnexosModel::find($value->id);
                $ruta = $this->archivos_path . '/' . $upload->ruta;
                if (is_file($ruta)) {
                    unlink($ruta);
                }
                $upload->delete();
            }
        }

        // dd($archivos);
        if (is_array($archivos)) {
            foreach ($archivos as $key => $value) {
                $archivo = $value;
                $name = 'Archivo_Ref_' . $id_tramite . '_tipo_11_' . sha1(date('YmdHis') . Str::random(10));
                $resize_name = $name . Str::random(2) . '.' . $archivo->getClientOriginalExtension();

                $archivo->move($this->archivos_path, $resize_name);

                $upload = new AnexosModel;
                $upload->usuario_id = Auth::user()->id;
                $upload->id_referencia = $id_tramite;
                $upload->ruta = $resize_name;
                $upload->nombre = basename($archivo->getClientOriginalName());
                $upload->hojas = $info[$key]->hojas;
                $upload->descripcion = $info[$key]->descripcion;
                $upload->tipo = 11;
                $upload->save();
            }
        }
    }

    public function generarDocumento()
    {
        try {
            //code...
            $input = Input::all();
            $tipo_x = Input::get('tipo');
            // $tipo_x=2;
            $id = Auth::user()->id;
            $nombre_documento = 'Tramite_interno' . $input['numtramite'] . '_' . $id . '.pdf';
            $datos = new stdClass;
            $datos->nombre_documento = $nombre_documento;
            $userpara = explode(',', Input::get('para'));
            $para = UsuariosModel::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')->select('name', 'cargo', 'direccion')->whereIn('users.id', $userpara)->get();
            $usercopia = explode(',', Input::get('copia'));
            $copia = UsuariosModel::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')->select('name', 'cargo', 'direccion')->whereIn('users.id', $usercopia)->get();
            // dd($para);
            $datos->asunto = Input::get('asunto');

            $anexos_json = json_decode(Input::get('anexos_json'));
            if (is_array($anexos_json) && isset($anexos_json[0])) {
                $datos->anexos = $anexos_json;
            } else {
                $datos->anexos = null;
            }

            $datos->tipo_firma = $tipo_x;
            if ($tipo_x == 2) {
                $datos->fecha_firma = date('Y-m-d H:i:s');
                $datos->codigo = Input::get('codigo');
            } else {
                $datos->codigo = '';
                $datos->fecha_firma = '';
            }




            $tipo = Input::get('tipo_tramite_interno');
            $datos->tipo = 'MEMORANDO';
            if ($tipo == 'OFI') {
                $datos->tipo = 'OFICIO';
                $delegados_para = json_decode(Input::get('delegados_para'));

                if (is_array($delegados_para) && isset($delegados_para[0])) {
                    // $para = [];
                    foreach ($delegados_para as $key => $value) {
                        $para_datos = new stdClass;
                        $para_datos->titulo = $value->titulo;
                        $para_datos->name = $value->nombre;
                        $para_datos->cargo = $value->cargo;
                        $para_datos->institucion = $value->institucion;
                        $para[] = $para_datos;
                    }
                } else {
                    // $para = [];
                }

                // dd($para);

                $delegados_copia = json_decode(Input::get('delegados_copia'));

                if (is_array($delegados_copia) && isset($delegados_copia[0])) {
                    // $copia = [];
                    foreach ($delegados_copia as $key => $value) {
                        $copia_datos = new stdClass;
                        $copia_datos->name = $value->titulo . '<br>' . $value->nombre;
                        $copia_datos->cargo = $value->cargo;
                        $copia_datos->institucion = $value->institucion;
                        $copia[] = $copia_datos;
                    }
                } else {
                    // $copia = [];
                }
                //  dd($copia);
            }
            if ($tipo == 'INF') {
                $datos->tipo = 'INFORME';
                $datos->numtramite_externo = Input::get('numtramite_externo');
                $datos->nombre_informe = Input::get('nombre_informe');
            }



            if (Input::get('fecha')) {
                $datos->fecha = Input::get('fecha');
            }

            $datos->para = $para;
            $datos->copia = $copia;
            $datos->peticion = Input::get('peticion');
            $datos->borrador = $tipo_x;
            // $datos->prioridad = Input::get('prioridad');
            $datos->numtramite = Input::get('numtramite');
            $user = UsuariosModel::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')
                ->where('users.id', Auth::user()->id)->first();
            $datos->user = $user;
            // dd($datos);

            $pdf = \App::make('dompdf.wrapper');
            
            $context = stream_context_create([
                'ssl' => [
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                    'allow_self_signed' => TRUE
                ]
            ]);
            // $pdf->setOptions(['isPhpEnabled' => true]);
            // $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'Arial']);
            $pdf->getDompdf()->setHttpContext($context);
            $pdf->loadView('vistas.firmaelectronica.memo', [
                "datos" => $datos,
                "configuraciongeneral" => $this->configuraciongeneral
            ])->setPaper('a4', 'portrait'); //])->setPaper('a4','landscape'); 
            // return $pdf->stream($nombre_documento);
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();


            if ($tipo == 'INF') {
                $canvas->page_text(430, 100, "Pag. {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            } else {
                $canvas->page_text(10, 800, "Pag. {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            }

            $pdf->download($nombre_documento);


            $documento = $pdf->output();
            $carpeta  = public_path() . '/archivos_temporales';
            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777, true);
            }
            $cedula = Auth::user()->cedula;
            // $cedula = '1309029708';
            $rutafinal = $carpeta . '/' . $nombre_documento;
            file_put_contents($rutafinal, $documento);

            if ($tipo_x == 2) {
                $codigo = TramInternoCodigosModel::where(['estado' => 'ACT', 'numtramite' => Input::get('numtramite'), 'codigo' => Input::get('codigo')])->first();
                // $codigo = TramInternoCodigosModel::where(['estado' => 'ACT'])->first();
                if ($codigo) {
                    $codigo = TramInternoCodigosModel::find($codigo->id);
                    $codigo->fecha_firma = date('Y-m-d H:i:s');
                    $codigo->save();
                    $carpeta_2  = public_path() . '/archivos_firmados';
                    $rutafinal_2 = $carpeta_2 . '/' . $nombre_documento;
                    if (is_file($rutafinal_2)) {
                        unlink($rutafinal_2);
                    }
                    file_put_contents($rutafinal_2, $documento);
                    return ['estado' => true, 'documento' => $nombre_documento];
                } else {
                    return ['estado' => false, 'documento' => null,  'msg' => 'Código inválido'];
                }
            }

            if ($tipo_x == 3) {
                return ['estado' => true, 'msg' => Url::to('') . '/archivos_temporales/' . $nombre_documento];
            }
            ////para pruebas se generar localmente para dejarlo pasar sin firmar
            if (config('app.env') == 'local') {
                $carpeta_2  = public_path() . '/archivos_firmados';
                $rutafinal_2 = $carpeta_2 . '/' . $nombre_documento;
                if (is_file($rutafinal_2)) {
                    unlink($rutafinal_2);
                }
                file_put_contents($rutafinal_2, $documento);
            }
            $tipo_firma = Input::get('tipo_firma');
            // $tipo_firma = 2;
            return $this->firmaElectronicaController->firmaToken($nombre_documento, $rutafinal, $cedula, $tipo_firma);
        } catch (\Throwable $th) {
            // dd($th);
            Log::info($th);
            return ['estado' => false, 'documento' => null,  'msg' => 'Si el texto fue copiado de un documento de Word por favor dar click derecho, luego seleccionar pegar como texto sin formato, luego darle el formato correspondiente con el editor web de esta página.'];
        }
    }

    public function validarDocumento()
    {
        $input = Input::all();
        $carpeta  = public_path() . '/archivos_firmados/' . $input['documento'];
        if (file_exists($carpeta)) {
            $ruta = 'https://sistemasic.manta.gob.ec/archivos_firmados/' . $input['documento'];
            $ruta = URL::to('') . '/archivos_firmados/' . $input['documento'];
            return ['estado' => true, 'msg' => 'Documento encontrado', 'ruta' => $ruta, 'documento' => $input['documento']];
        } else {
            return ['estado' => false, 'msg' => 'No se encuentra el documento firmado en el servidor'];
        }
    }
    public function generarCodigo()
    {

        try {

            $datos = TramInternoAutorizacionesModel::where(['cedula' => Auth::user()->cedula, 'estado_autorizacion' => 'APROBADO'])->first();
            if (!$datos) {
                if (Auth::user()->cedula != '1310182470' && Auth::user()->cedula != '1314854918' && Auth::user()->cedula != '1312469651' && Auth::user()->cedula != '1309029708' && Auth::user()->cedula != '1306325018') {
                    return ['estado' => false, 'msg' => 'No tiene autorización, por favor acercarse a R.R.H.H para que sea habilitado.'];
                }
            }

            TramInternoCodigosModel::where('numtramite', Input::get('numtramite'))->update(['estado' => 'INA']);
            $codigo = new TramInternoCodigosModel;
            $codigo->codigo = Auth::user()->id . rand(1000, 5000);
            $codigo->numtramite = Input::get('numtramite');
            $codigo->id_usuario = Auth::user()->id;
            $codigo->save();
            $this->notificacion->EnviarEmail(Auth::user()->email, 'CÓDIGO DE AUTORIZACIÓN', $codigo->codigo, 'CÓDIGO DE AUTORIZACIÓN', '',  "vistas.emails.email", "NO");
            return ['estado' => true, 'msg' => 'Código enviado al correo ' . Auth::user()->email];
        } catch (\Throwable $th) {
            // return $th->getMessage();
            return ['estado' => false, 'msg' => 'No se pudo enviar el código de validación'];
        }
    }

    public function eliminarBorrador()
    {
        try {
            //code...
            $id = Input::get('id');
            $id = Crypt::decrypt($id);
            AsignadoTramiteInternoModel::where('id_tramite', $id)->delete();
            $tramite = TramiteInternoModel::find($id);
            $carpeta  = public_path() . '/archivos_firmados/' . $tramite->documento;
            if (is_file($carpeta)) {
                unlink($carpeta);
            }
            $tramite->delete();
            return ['estado' => true, 'msg' => 'Borrador eliminado correctamente'];
        } catch (\Throwable $th) {
            return ['estado' => false, 'msg' => 'No se pudo eliminar el borrador'];
        }
    }

    public function show($id)
    {
        $tabla = TramiteInternoModel::find($id);
        $archivados = AsignadoTramiteInternoModel::from('tma_tram_interno_asignados as asig')
        ->join('users as u', 'u.id', 'asig.id_usuario_asignado')
        ->select('u.name as usuario', 'asig.observacion', 'asig.updated_at as fecha')
        ->where(['asig.id_tramite' => $id, 'asig.estado' => 'ARCHIVADO'])->get();
        $historial = $this->obtenerHistorial($id);
        
        return view('vistas.historialtramitesinternos', [
            'timelineTramiteInternos' => $historial,
            'objetos' => null,
            'tabla' => $tabla,
            'archivados' => $archivados,
            'configuraciongeneral' => $this->configuraciongeneral
        ]);
    }

    private function guardarHistorial($data, $estado)
    {
        $historial = HistorialInternosModel::create([
            'tramite_id' => $data->id,
            'id_remitente' => $data->id_remitente,
            'id_usuario' => Auth::user()->id,
            'numtramite' => $data->numtramite,
            'tipo_tramite' => $data->tipo_tramite,
            'asunto' => $data->asunto,
            'peticion' => $data->peticion,
            'estado' => $estado,
            'delegados_para' => $data->delegados_para,
            'delegados_copia' => $data->delegados_copia,
            'tipo_firma' => $data->tipo_firma,
            'tipo_envio' => $data->tipo_envio,
            'numtramite_externo' => $data->numtramite_externo,
            'nombre_informe' => $data->nombre_informe
        ]);

        return $historial->id;
    }

    private function obtenerHistorial($id)
    {
        $tabla = HistorialInternosModel::from('tram_tmo_historial_tramites_internos as histo')
            ->join('users as remitente', 'histo.id_remitente', 'remitente.id')
            ->join('users as u', 'histo.id_usuario', 'u.id')
            ->select('histo.*', 'remitente.name as remitente', 'u.name as usuario')
            ->where('histo.tramite_id', $id)->orderBy('histo.updated_at', 'ASC')->get();
        return $tabla;
    }

    public function agregarHistorial()
    {
        if(Auth::check() && Auth::user()->id_perfil == 1)
        {
            $tramites = TramiteInternoModel::all();
            
            foreach($tramites as $tramite)
            {
                if($tramite->tipo_envio == 'ENVIAR')
                {
                    $idHistorial = $this->guardarHistorial($tramite, "{$tramite->tipo_tramite} creado correctamente");
                }
                else
                {
                    $idHistorial = $this->guardarHistorial($tramite, "{$tramite->tipo_tramite} guardado en borradores");
                }

                $historial = HistorialInternosModel::findOrFail($idHistorial);
                $historial->id_usuario = $tramite->id_remitente;
                $historial->created_at = $tramite->created_at;
                $historial->updated_at = $tramite->created_at;
                $historial->save();
            }

            $archivados =  AsignadoTramiteInternoModel::where(['estado' => 'ARCHIVADO'])->get();
            foreach($archivados as $archivado)
            {
                $idHistorial = $this->guardarHistorial($archivado->tramite, 'Trámite archivado');
                $historial = HistorialInternosModel::findOrFail($idHistorial);
                $historial->id_usuario = $archivado->id_usuario_asignado;
                $historial->created_at = $archivado->updated_at;
                $historial->updated_at = $archivado->updated_at;
                $historial->save();
            }
    
            return response()->json([
                'ok' => true,
                'message' => 'Historial creado exitosamente'
            ]);
        }
    }
}
