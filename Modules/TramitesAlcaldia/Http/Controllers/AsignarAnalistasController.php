<?php

namespace Modules\TramitesAlcaldia\Http\Controllers;

use App\ArchivosModel;
use App\User;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\NotificacionesController;
use App\SubDireccionModel;
use Illuminate\Support\Facades\Crypt;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabModel;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaAsiganrModel;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabDetaModel;

class AsignarAnalistasController extends Controller
{
    public $configuraciongeneral = ['Gestión de trámites internos', 'tramitesinternos.store', 'tramitesinternos.form', 'crear', '', 6 => ''];

    protected $Notificacion;

    public function __construct(NotificacionesController $Notificacion)
    {
        $this->middleware('auth');
        $this->Notificacion = $Notificacion;
    }

    public function asignados()
    {
        $this->configuraciongeneral[4] = 'Trámites asignados';
        return view('tramitesalcaldia::tramitesexternos.tramitesasignados', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'estado' => 'ANALISTA'
        ]);
    }

    public function asignadosContestados()
    {
        $this->configuraciongeneral[4] = 'Trámites contestados';
        return view('tramitesalcaldia::tramitesexternos.tramitesasignados', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'estado' => 'ANALISTA-CONTESTADO'
        ]);
    }

    public function asignadosAjax(Request $request)
    {
        $area = SubDireccionModel::where(['area' => Auth::user()->id_direccion])->first(['id_direccion']);
        $id_direccion = isset($area->id_direccion) ? $area->id_direccion : 0;
        $tramites = TramAlcaldiaAsiganrModel::with('cabecera.tipoTramite')->where([
            'id_direccion' => Auth::user()->id,
            'tram_peticiones_direccion_asignada.estado' => $request->estado
        ])->whereIn('direccion_solicitante', [$id_direccion, Auth::user()->id_direccion]);

        return DataTables::of($tramites)->editColumn('tipo_tramite', function($tramite) {
            return isset($tramite->cabecera->tipo_tramite) ? $tramite->cabecera->tipoTramite->valor : '';
        })->addColumn('atender', function($tramite) {
            $atender = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->cabecera->id])
            ->where('asig.estado', '<>', 'CON COPIA')->get();

            return $atender->map(function($asignado) {
                return $asignado->direccion;
            })->implode(' - ');
        })->addColumn('action', function($tramite) use($request) {
            $id = $tramite->cabecera->id;
            if($request->estado == 'ANALISTA')
            {
                return '<a href="' . route('tramites.show', ['id' => $id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
                <i class="fa fa-newspaper-o"></i></a> 
                <a href="' . route('tramite.editfinalizar') . "?id=$id " . '">
                <i class="fa fa-pencil-square-o"></i></a>';
            }
            else
            {
                return '<a href="' . route('tramites.show', ['id' => $id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
                <i class="fa fa-newspaper-o"></i></a>';
            }
        })->make(true);
    }

    public function guardarInformeAnalista(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($id);

            $analistaAsignado = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'id_direccion' => Auth::user()->id,
                'estado' => 'ANALISTA'
            ])->first();

            if(!$analistaAsignado)
            {
                $analistaAsignado = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id,
                    'id_direccion' => Auth::user()->id_direccion,
                    'estado' => 'ANALISTA'
                ])->first();
            }
            
            $archivo = ArchivosModel::where([
                'tipo' => 10,
                'id_referencia' => $tramite->id,
                'id_usuario' => Auth::user()->id
            ])->count();
            // dd($archivo);
            
            // if($archivo == 0 && isset(request()->observacion))
            // {
                $analistaAsignado->observacion = request()->observacion;
                $analistaAsignado->estado = 'ANALISTA-CONTESTADO';
                $analistaAsignado->respondido = 1;
                $analistaAsignado->save();
                
                $direccion = direccionesModel::find(Auth::user()->id_direccion);
                $this->guardarTimeline($id, $tramite, 'Respuesta del analista asignado: ' . $direccion->direccion);
                
                $subareas = SubDireccionModel::whereIn('id_direccion', [6, 21])->pluck('area');
                $analistaUnidad = collect($subareas)->contains(Auth::user()->id_direccion);

                // if(Auth::user()->id_direccion == 6 || Auth::user()->id_direccion == 21 || $analistaUnidad)
                if(Auth::user()->id_direccion == 6 || $analistaUnidad)
                {
                    if($tramite->tipo == 'TRAMITES MUNICIPALES')
                    {   
                        $perfiles = array_merge(UsuariosModel::ANALISTA_DIRECTOR, UsuariosModel::DIRECTORES);
                        $perfilesAD = collect($perfiles);
                        $director = UsuariosModel::where([
                            'id_direccion' => Auth::user()->id_direccion
                        ])->select('id', 'name', 'email')
                        ->whereIn('id_perfil', $perfilesAD)->get();
                    }
                    else
                    {
                        $subarea = SubDireccionModel::where(['area' => Auth::user()->id_direccion])->first(['id_direccion']);
                        $id_direccion = isset($subarea->id_direccion) ? $subarea->id_direccion : 0;
                        $director = UsuariosModel::where(function($q) use($id_direccion) {
                            $q->where(['id_direccion' => Auth::user()->id_direccion])
                            ->orWhere(['id_direccion' => $id_direccion]);
                        })->select('id', 'name', 'email')
                        ->whereIn('id_perfil', UsuariosModel::ANALISTA_DIRECTOR)->get();
                    }
                }
                else
                {
                    $director = UsuariosModel::directores()
                    ->where(['users.id_direccion' => $analistaAsignado->direccion_solicitante])->get();
                    
                    if(count($director) == 0)
                    {
                        $director = UsuariosModel::where(['id_perfil' => 54,'id_direccion' => $analistaAsignado->direccion_solicitante])->get();
                    }
                }
                
                $ruta = 'tramitesalcaldia/finalizartramite?id=' . $tramite->id;
                $mensaje = Auth::user()->name .', ha respondido al trámite asignado N° ' . $tramite->numtramite;

                foreach($director as $dir)
                {
                    $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $dir->id, '2c438f');
                    $this->Notificacion->EnviarEmail($dir->email, 'Respuesta del analista asignado', '', $mensaje, $ruta, 'vistas.emails.email');
                }
                
                $perfiles = collect(UsuariosModel::DIRECTORES);
                $esDirector = $perfiles->contains(Auth::user()->id_perfil);
                $redirect = $esDirector ? 'tramitesrespondidos' : 'asignados';

                DB::commit();
                return response()->json([
                    'ok' => true,
                    'message' => 'Su informe ha sido enviado correctamente',
                    'redirect' => $redirect
                ]);
            // }
            // else if(!isset(request()->observacion))
            // {
            //     return response()->json([
            //         'ok' => false,
            //         'message' => 'No se puede enviar hasta que suba su informe'
            //     ]);
            // }
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo guardar, inténtelo de nuevo.',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }
    
    public function asignarAnalista(Request $request, $id)
    {
        if($request->ajax())
        {
            DB::beginTransaction();
            try
            {
                $tramite = TramAlcaldiaCabModel::findOrFail($id);
                $tramite->observacion = $request->observacion;
                $tramite->save();

                $esCoodinacion = collect(UsuariosModel::COORDINACIONES)->contains(Auth::user()->id_direccion);
                $analistasInformar = $request->analistas;
                
                $direccionAsignada = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'id_direccion' => Auth::user()->id_direccion])->first();
                $direccionAsignada->observacion = $request->observacion;
                $direccionAsignada->save();
                
                $ruta = $esCoodinacion ? 'tramitesalcaldia/finalizartramite?id=' . $tramite->id . '&estado=' . Crypt::encrypt('ANALISTA') : 'tramitesalcaldia/finalizartramite?id=' . $tramite->id;
                $mensaje = 'Se le ha asignado el trámite N° ' . $tramite->numtramite;

                foreach($analistasInformar as $informar)
                {
                    if($esCoodinacion)
                    {
                        TramAlcaldiaAsiganrModel::updateOrCreate(['id_cab' => $id, 'id_direccion' => $informar], [
                            'direccion_solicitante' => Auth::user()->id_direccion,
                            'respondido' => 0,
                            'estado' => 'ANALISTA'
                        ]);

                        $director = UsuariosModel::directores()->where('users.id_direccion', $informar)->get();
                        foreach($director as $dir)
                        {
                            $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $dir->id, '2c438f');
                            $this->Notificacion->EnviarEmail($dir->email, 'Trámite asignado', '', $mensaje, $ruta, 'vistas.emails.email');
                        }
                    }
                    else
                    {
                        $analista = User::findOrFail($informar);
    
                        TramAlcaldiaAsiganrModel::updateOrCreate(['id_cab' => $id, 'id_direccion' => $analista->id], [
                            'direccion_solicitante' => Auth::user()->id_direccion,
                            'estado' => 'ANALISTA'
                        ]);

                        $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $analista->id, '2c438f');
                        $this->Notificacion->EnviarEmail($analista->email, 'Trámite asignado', '', $mensaje, $ruta, 'vistas.emails.email');
                    }

                }

                $direccion = direccionesModel::find(Auth::user()->id_direccion);
                $this->guardarTimeline($id, $tramite, 'Asignar trámite a analista: ' . $direccion->direccion);
    
                DB::commit();
                return response()->json([
                    'ok' => true,
                    'message' => 'Se ha notificado correctamente a los analistas seleccionados.'
                ]);
            }
            catch(\Exception $ex)
            {
                DB::rollback();
                return response()->json([
                    'ok' => false,
                    'message' => 'No se pudo notificar a las direcciones seleccionada, inténtelo de nuevo.',
                    'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
                ]);
            }
        }
        else abort(401);
    }

    public function guardarTimeline($id, $data, $observacion = null, $estado = 0, $disposicion = null):int
    {
        $timeline = new TramAlcaldiaCabDetaModel;
        $timeline->numtramite = $data->numtramite;
        // $timeline->recomendacion = 'Solicitud ingresada satisfactoriamente';
        $timeline->observacion = $observacion;
        $timeline->remitente = $data->remitente;
        $timeline->asunto = $data->asunto;
        $timeline->peticion = $data->peticion;
        $timeline->prioridad = $data->prioridad;
        $timeline->direccion_atender = $data->direccion_atender;
        $timeline->fecha_fin = $data->fecha_fin;
        $timeline->estado_proceso = $estado;
        $timeline->disposicion = $disposicion == null ? $data->disposicion : $disposicion;
        // $timeline->observacion = $data->observacion;
        $timeline->id_usuario = Auth::user()->id;
        $timeline->ip = request()->ip();
        $timeline->pc = request()->getHttpHost();
        $timeline->id_tram_cab = $id;
        $timeline->archivo = $data->archivo;
        $timeline->fecha_ingreso = $data->fecha_ingreso;
        if($observacion == 'Trámite en proceso de gestión')
        {
            $timeline->fecha_respuesta = now();
        }
        $timeline->save();

        return $timeline->id;
    }
}
