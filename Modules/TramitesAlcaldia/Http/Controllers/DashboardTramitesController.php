<?php namespace Modules\Tramitesalcaldia\Http\Controllers;

use App\ArchivosModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabModel;
use Modules\TramitesAlcaldia\Entities\AnexosModel;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaAsiganrModel;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabDetaModel;
use Modules\TramitesAlcaldia\Entities\TramiteInternoModel;
use Yajra\DataTables\DataTables;

class DashboardTramitesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	var $configuraciongeneral = ["DASHBOARD TRAMITES", "tramitesalcaldia/dashboard", "index"];

	public function index()
	{
		$externos = $this->tramitesExternos('null', 'null');
		$externos = json_decode($externos->content());
		
		$internos = $this->tramitesInternos('null', 'null');
		$internos = json_decode($internos->content());
		$date = Carbon::now();
        $endDate = $date->subDay(7);
        $fin = $endDate->toDateString();
        $date = Carbon::now();
        $inicio = $date->toDateString();
		$fin2 = date('Y-m-d',strtotime($inicio));
		$desde = date('2020-06-10');
        $hasta = date('Y-m-d');
		
		return view('tramitesalcaldia::dashboard',[
			"configuraciongeneral"=>$this->configuraciongeneral,
			'externos' => $externos,
			'internos' => $internos,
			"desde" => $desde,
			"hasta" => $hasta,
            "inicio_fin" => $fin2,
		]);
	}

	public function tramitesExternos($desde, $hasta)
	{
		if($desde == 'null')
		{
			$desde = date('2020-06-10');
			$hasta = date('Y-m-d');
		}

		$desdeHasta = ["{$desde} 00:00:00", "{$hasta} 23:59:59"];
		
		$ingresados = TramAlcaldiaCabModel::where(['estado' => 'ACT'])
			->where('disposicion', '<>', 'RESERVADO')->whereBetween('created_at', $desdeHasta)->count();

		$portalTE = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'tipo' => 'SECRETARIA GENERAL'])
			->where('disposicion', '<>', 'RESERVADO')->whereBetween('created_at', $desdeHasta)->count();
		$ventanilla = TramAlcaldiaCabModel::where(['tipo' => 'VENTANILLA', 'estado' => 'ACT'])
			->where('disposicion', '<>', 'RESERVADO')->whereBetween('created_at', $desdeHasta)->count();

		$portalPMCiudadano = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'tipo' => 'TRAMITES MUNICIPALES'])
			->whereBetween('created_at', $desdeHasta)->whereNull('id_usuario_externo')->count();
		$portalPMVentanilla = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'tipo' => 'TRAMITES MUNICIPALES'])
			->whereBetween('created_at', $desdeHasta)->whereNotNull('id_usuario_externo')->count();

		$pendientes = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'PENDIENTE'])
			->whereBetween('created_at', $desdeHasta)->count();

		$enProceso = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'EN PROCESO'])
			->whereBetween('created_at', $desdeHasta)->count();

		$aprobados = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'APROBADA'])
			->whereBetween('created_at', $desdeHasta)->count();

		$finalizados = TramAlcaldiaCabModel::where(['estado' => 'ACT'])->whereIn('disposicion', ['FINALIZADO', 'FINALIZADO PENDIENTE DE PAGO'])
		->whereBetween('created_at', $desdeHasta)->count();
			// ->where(function($q) use($desdeHasta) {
			// 	$q->whereBetween('created_at', $desdeHasta)
			// 	->orWhereBetween('fecha_respuesta', $desdeHasta);
			// })->count();
		
		$finalizadosPorPagar = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'FINALIZADO PENDIENTE DE PAGO'])
			->whereBetween('created_at', $desdeHasta)->count();

		$finalizadosFisico = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'FINALIZADO'])
			->whereBetween('created_at', $desdeHasta)->whereNull('correo_electronico')->count();

		$finalizadosDigital = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'FINALIZADO'])
			->whereBetween('created_at', $desdeHasta)->whereNotNull('correo_electronico')->count();

		$ids = TramAlcaldiaCabModel::where(['estado' => 'ACT'])->whereBetween('created_at', $desdeHasta)->pluck('id');
		$hojasAnexos = AnexosModel::where(['tipo' => 10])->where('hojas', '>', 0)
			->whereBetween('created_at', $desdeHasta)->sum('hojas');
		$hojasArchivos = ArchivosModel::where(['tipo' => 10])->whereIn('id_referencia', $ids)
			->whereBetween('created_at', $desdeHasta)->count();
		$nHojas = ($hojasAnexos + ($hojasArchivos * 2));
		
		return response()->json([
			'data' => [
				'ingresados' => [
					'total' => number_format($ingresados),
					'secretariaGeneral' => [
						'total' => number_format($portalTE + $ventanilla),
						'portal' => number_format($portalTE),
						'ventanilla' => number_format($ventanilla)
					],
					'permisosMunicipales' => [
						'total' => number_format($portalPMCiudadano + $portalPMVentanilla),
						'portal' => number_format($portalPMCiudadano),
						'ventanilla' => number_format($portalPMVentanilla),
					],
				],
				'estados' => [
					'enProceso' => number_format($enProceso),
					'aprobados' => number_format($aprobados),
					'pendientes' => number_format($pendientes),
					'finalizados' => [
						'total' => number_format($finalizados),
						'pendientePago' => number_format($finalizadosPorPagar),
						'respuestaFisica' => number_format($finalizadosFisico),
						'respuestaDigital' => number_format($finalizadosDigital)
					],
				],
				'hojasAhorradas' => number_format($nHojas),
				'sinformato' => [
					'ingresados' => [
						'total' => ($ingresados),
						'secretariaGeneral' => [
							'total' => ($portalTE + $ventanilla),
							'portal' => ($portalTE),
							'ventanilla' => ($ventanilla),
							'portal_x100' =>  ($portalTE + $ventanilla) == 0 ? 0 : round(($portalTE / ($portalTE + $ventanilla)) * 100, 2),
							'ventanilla_x100' => ($portalTE + $ventanilla) == 0 ? 0 : round(($ventanilla / ($portalTE + $ventanilla)) * 100, 2)
						],
						'permisosMunicipales' => [
							'total' => ($portalPMCiudadano + $portalPMVentanilla),
							'portal' => ($portalPMCiudadano),
							'ventanilla' => ($portalPMVentanilla),
							'portal_x100' => ($portalPMCiudadano + $portalPMVentanilla) == 0 ? 0 : round(($portalPMCiudadano / ($portalPMCiudadano + $portalPMVentanilla)) * 100, 2),
							'ventanilla_x100' => ($portalPMCiudadano + $portalPMVentanilla) == 0 ? 0 : round(($portalPMVentanilla / ($portalPMCiudadano + $portalPMVentanilla)) * 100, 2)
						],
					],
					'estados' => [
						'enProceso' => ($enProceso),
						'aprobados' => ($aprobados),
						'pendientes' => ($pendientes),
						'enProceso_x100' => ($enProceso + $aprobados + $pendientes + $finalizados) == 0 ? 0 : round(($enProceso / ($enProceso + $aprobados + $pendientes + $finalizados)) * 100, 2),
						'aprobados_x100' => ($enProceso + $aprobados + $pendientes + $finalizados) == 0 ? 0 : round(($aprobados / ($enProceso + $aprobados + $pendientes + $finalizados)) * 100, 2),
						'pendientes_x100' => ($enProceso + $aprobados + $pendientes + $finalizados) == 0 ? 0 : round(($pendientes / ($enProceso + $aprobados + $pendientes + $finalizados)) * 100, 2),
						'finalizados_x100' => ($enProceso + $aprobados + $pendientes + $finalizados) == 0 ? 0 : round(($finalizados / ($enProceso + $aprobados + $pendientes + $finalizados)) * 100, 2),
						'finalizados' => [
							'total' => ($finalizados),
							'pendientePago' => ($finalizadosPorPagar),
							'respuestaFisica' => ($finalizadosFisico),
							'respuestaDigital' => ($finalizadosDigital),
							'pendientePago_x100' => ($finalizados) == 0 ? 0 : round(($finalizadosPorPagar / ($finalizados)) * 100, 2),
							'respuestaFisica_x100' => ($finalizados) == 0 ? 0 : round(($finalizadosFisico / ($finalizados)) * 100, 2),
							'respuestaDigital_x100' => ($finalizados) == 0 ? 0 : round(($finalizadosDigital / ($finalizados)) * 100, 2)
						],
					],
					'hojasAhorradas' => $nHojas
				],
			]
		]);
	}

	public function tramitesInternos($desde, $hasta)
	{
		if($desde == 'null')
		{
			$desde = date('2020-06-10');
			$hasta = date('Y-m-d');
		}

		$desdeHasta = ["{$desde} 00:00:00", "{$hasta} 23:59:59"];

		$generados = TramiteInternoModel::whereBetween('created_at', $desdeHasta)->count();
		
		$memos = TramiteInternoModel::where(['tipo_tramite' => 'MEMO'])->whereBetween('created_at', $desdeHasta)->count();
		$oficios = TramiteInternoModel::where(['tipo_tramite' => 'OFICIO'])->whereBetween('created_at', $desdeHasta)->count();
		$informes = TramiteInternoModel::where(['tipo_tramite' => 'INFORME'])->whereBetween('created_at', $desdeHasta)->count();
		
		$enviados = TramiteInternoModel::where(['tipo_envio' => 'ENVIAR'])->count();
		$borrador = TramiteInternoModel::where(['tipo_envio' => 'BORRADOR'])->count();
		$archivados = TramiteInternoModel::whereHas('asignados', function($q) {
			$q->where(['estado' => 'ARCHIVADO']);
		})->count();

		$token = TramiteInternoModel::where(['tipo_firma' => 'TOKEN'])
			->whereBetween('created_at', $desdeHasta)->count();
		$qr = TramiteInternoModel::where(['tipo_firma' => 'CORREO'])
			->whereBetween('created_at', $desdeHasta)->count();
		$noFirmado = TramiteInternoModel::whereNull('tipo_firma')
			->whereBetween('created_at', $desdeHasta)->count();

		$hojasAnexos = AnexosModel::where(['tipo' => 11])->where('hojas', '>', 0)
			->whereBetween('created_at', $desdeHasta)->sum('hojas');
		$hojasArchivos = $generados * 2;
		$nHojas = number_format($hojasAnexos + $hojasArchivos);

		return response()->json([
			'data' => [
				'generados' => [
					'total' => number_format($generados),
					'memos' => number_format($memos),
					'oficios' => number_format($oficios),
					'informes' => number_format($informes),
				],
				'tipoFirma' => [
					'token' => number_format($token),
					'qr' => number_format($qr),
					'noFirmado' => number_format($noFirmado),
				],
				'hojasAhorradas' => $nHojas,
				'sinformato' => [
					'generados' => [
						'total' => ($generados),
						'memos' => ($memos),
						'oficios' => ($oficios),
						'informes' => ($informes),
						'memos_x100' => ($memos + $oficios + $informes) == 0 ? 0 : round(($memos / ($memos + $oficios + $informes)) * 100, 2),
						'oficios_x100' => ($memos + $oficios + $informes) == 0 ? 0 : round(($oficios / ($memos + $oficios + $informes)) * 100, 2),
						'informes_x100' => ($memos + $oficios + $informes) == 0 ? 0 : round(($informes / ($memos + $oficios + $informes)) * 100, 2)
					]	
				]
			]
		]);
	}

	public function tramitesEstadosAjax(Request $request)
	{
		$desde = $request->desde;
		$hasta = $request->hasta;
		$desdeHasta = ["{$desde} 00:00:00", "{$hasta} 23:59:59"];
		
		if($request->estado == 'FINALIZADOS')
		{
			$tramites = TramAlcaldiaCabModel::where(['estado' => 'ACT'])
				->whereIn('disposicion', ['FINALIZADO', 'FINALIZADO PENDIENTE DE PAGO'])
				->whereBetween('created_at', $desdeHasta);
		}
		else if($request->estado == 'FISICA')
		{
			$tramites = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'FINALIZADO'])
				->whereBetween('created_at', $desdeHasta)->whereNull('correo_electronico');
		}
		else if($request->estado == 'DIGITAL')
		{
			$tramites = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'FINALIZADO'])
				->whereBetween('created_at', $desdeHasta)->whereNotNull('correo_electronico');
		}
		else
		{
			$tramites = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => $request->estado])
			->whereBetween('created_at', $desdeHasta);
		}

		return DataTables::of($tramites)->addColumn('tipoTramite', function($tramite) {
            return isset($tramite->tipoTramite->valor) ? $tramite->tipoTramite->valor : '';
        })->addColumn('atender', function($tramite) {
            $atender = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->id])
			->where('asig.estado', '<>', 'CON COPIA')->get();
			
			$asignados_2 = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('users as u', 'asig.id_direccion', 'u.id')
            ->select('u.name as direccion')
			->where(['asig.id_cab' => $tramite->id, 'u.id_perfil' => 55])->get();
			if(count($asignados_2) > 0)
			{
				$atender = collect(array_merge($atender->toArray(), $asignados_2->toArray()));
			}

            return $atender->map(function($asignado) {
                return $asignado['direccion'];
            })->implode(' - ');
        })->addColumn('copia', function($tramite) {
            $copia = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->id, 'asig.estado' => 'CON COPIA'])->get();
			
			if(count($copia) == 0)
			{
				return 'NO ASIGNADOS';
			}
			else
			{
				return $copia->map(function($asignado) {
					return $asignado->direccion;
				})->implode(' - ');
			}
        })->addColumn('analistas', function($tramite) {
            $direcciones = explode('|', $tramite->direccion_atender);
            $analistas = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
            ->join('users as u', 'dira.id_direccion', 'u.id')
            ->where(['dira.id_cab' => $tramite->id])
            ->whereIn('dira.direccion_solicitante', $direcciones)
            ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
            ->select('u.name', 'dira.estado')->get();

			if(count($analistas) == 0)
			{
				return 'NO ASIGNADOS';
			}
			else
			{
				return $analistas->map(function($item, $key) {
					$estado = $item->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO';
					return "{$item->name} ({$estado})";
				})->implode(', ');
			}

        })->addColumn('adjunto', function($tramite) {
            $adjunto = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id])->first(['ruta']);
            
            if(isset($adjunto->ruta))
            {
                $archivos = '<a href="' . asset('archivos_sistema/' . $adjunto->ruta) . '" class="btn btn-success btn-sm dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>';
            }
            else $archivos = '';
            
            return $archivos;
        })->rawColumns(['adjunto'])->make(true);
	}


	public function getTimeLineCab($id,$tipo){
        $sw=0;
        switch($tipo){
            case 1:
            $tabla=TramAlcaldiaCabDetaModel::
            select("tram_peticiones_cab_deta.*","u.name")
            ->join("users as u","u.id","=","tram_peticiones_cab_deta.id_usuario")
			//->where('id_crono_cab',$id)
			->offset(1)
            ->limit(20)
			->orderBy("tram_peticiones_cab_deta.updated_at","DESC")
            ->get();
            return $tabla;
            break;
        }
	}

	public function getTramites($tipo,$datos,$total){

		if($tipo==1){
			$prioridad=explode("|",ConfigSystem("prioridadtramites"));
			$prioridadcolores=explode("|",ConfigSystem("prioridadtramitescolor"));
			foreach($datos as $item){
				switch($item->prioridad){

					case $prioridad[0]:
						$item->prioridad=$prioridad[0];
						$item->color=$prioridadcolores[0];
						$item->porcentaje=round((($item->total/$total->total)*100),2);

					break;
					case $prioridad[1]:
						$item->prioridad= $prioridad[1];
						$item->color=$prioridadcolores[1];
						$item->porcentaje=round((($item->total/$total->total)*100),2);
					break;
					case $prioridad[2]:
						$item->prioridad= $prioridad[2];
						$item->color=$prioridadcolores[2];
						$item->porcentaje=round((($item->total/$total->total)*100),2);

					break;

				}


			}

			return $datos;
		}else if($tipo==2){

			$disposicion=explode("|",ConfigSystem("disposicion"));
			$disposicioncolores=explode("|",ConfigSystem("disposicioncolor"));
			//show($disposicion);
			foreach($datos as $item){
				switch($item->disposicion){

					case $disposicion[0]:
						$item->disposicion=$disposicion[0];
						$item->color=$disposicioncolores[0];
						$item->porcentaje=round((($item->total/$total->total)*100),2);

					break;
					case $disposicion[1]:
						$item->disposicion= $disposicion[1];
						$item->color=$disposicioncolores[1];
						$item->porcentaje=round((($item->total/$total->total)*100),2);
					break;
					case $disposicion[2]:
						$item->disposicion= $disposicion[2];
						$item->color=$disposicioncolores[2];
						$item->porcentaje=round((($item->total/$total->total)*100),2);

					break;

				}


			}
			return $datos;
		}


	}

}
