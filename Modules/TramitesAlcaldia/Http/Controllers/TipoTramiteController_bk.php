<?php

namespace Modules\TramitesAlcaldia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\TramitesAlcaldia\Entities\TipoTramiteModel;

class TipoTramiteController_bk extends Controller
{
    var $configuraciongeneral = ["Tipos de trámites", "tramitesalcaldia/tipostramites", "index", 6 => 'tramitesalcaldia/tipostramitesajax'];

    var $objetos = '[ {"Tipo":"text","Descripcion":"Nombre","Nombre":"valor","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Área","Nombre":"id_area","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"text","Descripcion":"Días a despachar","Nombre":"dias_despachar","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
    ]';

    var $validarjs = [
        'valor' => 'valor: {
            required: true
        }',
        'dias_despachar' => 'dias_despachar: {
            required: true
        }'
    ];

    var $estado = [
        'ACT' => 'ACTIVO',
        'INA' => 'INACTIVO'
    ];

    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $objetos = json_decode($this->objetos);
        $tabla = [];

        return view('vistas.index', [
            'objetos' => $objetos,
            'tabla' => $tabla,
            'configuraciongeneral' => $this->configuraciongeneral,
            'delete' => 'si',
            'create' => 'si'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[2] = "crear";
        $tabla = new TipoTramiteModel;
        $objetos = json_decode($this->objetos);
        $objetos[2]->Valor = $this->estado;

        return view('vistas.create', [
            'configuraciongeneral' => $configuraciongeneral,
            'tabla' => $tabla,
            'objetos' => $objetos,
            'validarjs' => $this->validarjs
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->guardar($request);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[2] = "editar";
        $id = Crypt::decrypt($id);
        $tabla = TipoTramiteModel::findOrFail($id);
        $objetos = json_decode($this->objetos);
        $objetos[2]->Valor = $this->estado;

        return view('vistas.create', [
            'configuraciongeneral' => $configuraciongeneral,
            'tabla' => $tabla,
            'objetos' => $objetos,
            'validarjs' => $this->validarjs
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->guardar($request, $id);
    }

    public function tipostramitesajax(Request $request)
    {
        $columns = array(
            0 =>'id',
            1 =>'valor',
            2=> 'dias_despachar'
        );

        $totalData = TipoTramiteModel::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = TipoTramiteModel::offset($start)->limit($limit)
                ->orderBy($order,$dir)->get();
        }
        else
        {
            $search = $request->input('search.value');

            $posts = TipoTramiteModel::offset($start)->limit($limit)
                ->orderBy($order,$dir)->get();

            $totalFiltered = TipoTramiteModel::where('valor','LIKE',"%{$search}%")
                ->orWhere('dias_despachar', 'LIKE',"%{$search}%")->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $aciones = link_to_route('tipostramites.edit','', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o')).'';

                $nestedData['id'] = $post->id;
                $nestedData['valor'] = $post->valor;
                $nestedData['dias_despachar'] = $post->dias_despachar;
                $nestedData['estado'] = $post->estado == 'ACT' ? 'ACTIVO' : 'INACTIVO';
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }
        
        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        ];

        return response()->json($json_data);
    }

    public function guardar($request, $id = null)
    {
        DB::beginTransaction();
        try
        {
            // dd($request->all());
            $validator = Validator::make($request->all(), [
                'valor' => 'required',
                'dias_despachar' => 'required',
                'estado' => 'required'
            ]);

            if ($validator->fails()) return back()->withErrors($validator)->withInput();

            $message = $id == null ? 'Tipo de trámite creado correctamente.' : 'Tipo de trámite actualizado correctamente.';

            TipoTramiteModel::updateOrCreate(['id' => $id], [
                'valor' => $request->valor,
                'dias_despachar' => $request->dias_despachar,
                'estado' => $request->estado
            ]);

            DB::commit();
            return redirect()->route('tipostramites.index')->with('message', $message);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return back()->withErrors([$ex->getMessage() . ' - ' . $ex->getLine()])->withInput();
        }
    }
}
