<?php

namespace Modules\TramitesAlcaldia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Route;
use Auth;
use Illuminate\Routing\Controller;
use App\ModulosModel;
use App\MenuModulosModel;

class TramitesAlcaldiaController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
	public function index()
	{
		$rucu=Route::currentRouteName();//Route::getCurrentRoute()->getPath();
        $vermodulo=explode(".",$rucu);
        //show($vermodulo);
        $mod=ModulosModel::where("ruta",$vermodulo[0])->first();
        $usuario=Auth::user();
        $tbnivel=MenuModulosModel::join("menu as a","a.id","=","ad_menu_modulo.id_menu")
                ->join("ad_menu_perfil as b","b.id_menu_modulo","=","ad_menu_modulo.id")
                ->select("ad_menu_modulo.*","a.menu")
                ->where("b.id_perfil", Auth::user()->id_perfil);
                if($mod)
                    $tbnivel=$tbnivel->where("ad_menu_modulo.id_modulo",$mod->id);
        $tbnivel=$tbnivel->orderby("ad_menu_modulo.orden")->get();
        //show($tbnivel);
		return view('tramitesalcaldia::index',["modulo"=>$mod,"usuario"=>$usuario,"iconos"=>$tbnivel,"delete"=>"si",
        "create"=>"si"]);

		return view('tramitesalcaldia::index');
	}
}
