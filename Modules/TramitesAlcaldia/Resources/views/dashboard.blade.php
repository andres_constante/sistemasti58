<?php
if(!isset($nobloqueo))
   Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section('estilos')
<style>
    /* .modal-content {
        height:700px;
        overflow:auto;
    } */
    .font-sm {
        font-size: 1.2em;
        background-color: #0a3d62 !important;
    }

    .pointer:hover {
        cursor: pointer;
    }
    body.DTTT_Print {
        background: #fff;

    }
    .color_celeste{
        background-color: #0984e3;
        /*background-color: #1289A7;*/
        color: #fff;
    }
    .color_verde{
        background-color: #006266;
        color: #fff;
    }
    .gordito{
        font-weight: 500;
    }

    .color_turquesa{
        background-color: #1289A7;
        color: #fff;
    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }
    .ibox_gren{
        background-color: #6ab04c !important;
        color: #fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
@stop
@section ('contenido')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            @if (Session::has('message'))
                <script>
                    $(document).ready(function() {
                        toastr["success"]("{{ Session::get('message') }}");
                    });
                </script>
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            <div class="wrapper wrapper-content" >
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="date_added">Desde</label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input id="date_added" type="text" class="form-control" value="{{$desde}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="date_modified">Hasta</label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input id="date_modified" type="text" class="form-control" value="{{$hasta}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <button class="btn btn-success" onclick="filtrar(false)" style="margin-top: 1.7em;">Filtrar</button>
                            <button class="btn btn-info" onclick="filtrar(true)" style="margin-top: 1.7em;">Todos</button>
                            {{-- <div class="input-group" style="margin-top: 1.7em;">
                            </div> --}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title color_celeste">
                                <h3>TRÁMITES EXTERNOS INGRESADOS</h3>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title ibox_gren">
                                                <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                <h5>TOTAL</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <h1 class="no-margins gordito" id="total_ingresados">{{$externos->data->ingresados->total }}</h1>
                                                <small>TOTAL</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title ibox_gren">
                                                <span class="label label-primary pull-right font-sm" id="total_sg" style="background-color:#0c2461 !important;color:#fff;">{{ $externos->data->ingresados->secretariaGeneral->total }}</span>
                                                <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                <h5>SECRETARÍA GENERAL</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h1 class="no-margins" id="sg_ingresados_portal">{{$externos->data->ingresados->secretariaGeneral->portal }}</h1>
                                                        <small>P. CIUDADANO</small>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <h1 class="no-margins" id="sg_ingresados_ventanilla">{{$externos->data->ingresados->secretariaGeneral->ventanilla }}</h1>
                                                        <small>VENTANILLA</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title ibox_gren">
                                                <span class="label  pull-right font-sm" id="total_pm" style="background-color:#0c2461 !important;color:#fff;">{{ $externos->data->ingresados->permisosMunicipales->total }}</span>
                                                <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                <h5>PERMISOS MUNICIPALES</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h1 class="no-margins" id="pm_ingresados_portal">{{$externos->data->ingresados->permisosMunicipales->portal }}</h1>
                                                        <small>P. CIUDADANO</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h1 class="no-margins" id="pm_ingresados_ventanilla">{{$externos->data->ingresados->permisosMunicipales->ventanilla }}</h1>
                                                        <small>VENTANILLA</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title ibox_gren">
                                                <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                <h5>AHORRO DE HOJAS</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <h1 class="no-margins" id="ahorro_hojas">{{$externos->data->hojasAhorradas }}</h1>
                                                <small>HOJAS</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <figure class="highcharts-figure">
                            <div id="container"></div>
                        </figure>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title color_celeste">
                                <h3>POR ESTADOS</h3>
                            </div>
                            <input type="hidden" name="estado" id="estado">
                            @include('tramitesalcaldia::modaltramites')
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            {{-- <div class="col-lg-4">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title color_turquesa">
                                                        <span class="label pull-right" style="background-color:0;color:#FFFFFF;">     </span>
                                                        <h5>TOTAL</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <h1 class="no-margins gordito" id="finalizados_total">{{ $externos->data->estados->finalizados->total }}</h1>
                                                        <small>TOTAL</small>
                                                    </div>
                                                </div>
                                            </div> --}}
        
                                            <div class="col-sm-6">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title color_turquesa">
                                                        <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                        <h5 class="pointer" onclick="verTramites('PENDIENTE')">PENDIENTES</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <h1 class="no-margins" id="pendientes">{{$externos->data->estados->pendientes }}</h1>
                                                        <small>PENDIENTES</small>
                                                    </div>
                                                </div>
                                            </div>
        
                                            <div class="col-sm-6">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title color_turquesa">
                                                        <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                        <h5 class="pointer" onclick="verTramites('EN PROCESO')">EN PROCESO</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <h1 class="no-margins" id="en_proceso">{{$externos->data->estados->enProceso }}</h1>
                                                        <small>EN PROCESO</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title color_turquesa">
                                                        <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                        <h5 class="pointer" onclick="verTramites('APROBADA')">POR APROBAR (SECRETARÍA GENERAL)</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <h1 class="no-margins" id="aprobados">{{$externos->data->estados->aprobados }}</h1>
                                                        <small>APROBADOS</small>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-6">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title color_turquesa">
                                                        <span class="label label-primary pull-right font-sm" id="finalizados_total" style="background-color:#0c2461 !important;color:#fff;">{{ $externos->data->estados->finalizados->total }}</span>
                                                        <h5 class="pointer" onclick="verTramites('FINALIZADOS')">FINALIZADOS</h5>
                                                    </div>
                                                    <div class="ibox-content" style="font-size: 1em; color:#000">
                                                        <h3 class="no-margins">RESPUESTA DIGITAL - <b  id="respuesta_digital">{{$externos->data->estados->finalizados->respuestaDigital }}</b></h3>
                                                        <h3 class="no-margins" >RESPUESTA FÍSICA - <b id="respuesta_fisica"> {{$externos->data->estados->finalizados->respuestaFisica }}</b></h3>
                                                        <h3 class="no-margins" >PENDIENTE DE PAGO - <b id="pendiente_pago">{{$externos->data->estados->finalizados->pendientePago }}</b></h3>
                                                        {{-- <small>FINALIZADOS</small> --}}
                                                    </div>
                                                </div>
                                            </div>
        
                                            {{-- <div class="col-lg-4">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title ibox_gren">
                                                        <span class="label pull-right" style="background-color:0;color:#FFFFFF;">     </span>
                                                        <h5 class="pointer" onclick="verTramites('FISICA')">FINALIZADOS RESPUESTA FÍSICA</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <h1 class="no-margins" id="respuesta_fisica">{{$externos->data->estados->finalizados->respuestaFisica }}</h1>
                                                        <small>FINALIZADOS RESPUESTA FÍSICA</small>
                                                    </div>
                                                </div>
                                            </div>
        
                                            <div class="col-lg-4">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title ibox_gren">
                                                        <span class="label pull-right" style="background-color:0;color:#FFFFFF;">     </span>
                                                        <h5 class="pointer" onclick="verTramites('DIGITAL')">FINALIZADOS RESPUESTA DIGITAL</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <h1 class="no-margins" id="respuesta_digital">{{$externos->data->estados->finalizados->respuestaDigital }}</h1>
                                                        <small>FINALIZADOS RESPUESTA DIGITAL</small>
                                                    </div>
                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-lg-6 col-md-12">
                                        <figure class="highcharts-figure">
                                            <div id="piechart"></div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- <div class="col-lg-6">
                    <figure class="highcharts-figure">
                        <div id="piechart"></div>
                    </figure>
                </div>
                <div class="col-lg-6">
                    <figure class="highcharts-figure">
                        <div id="piechart2"></div>
                    </figure>
                </div> --}}
            </div>

            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title color_celeste">
                                <h3>TRÁMITES INTERNOS GENERADOS</h3>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title ibox_gren">
                                                <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                <h5>TOTAL</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <h1 class="no-margins gordito" id="total_internos">{{$internos->data->generados->total }}</h1>
                                                <small>TOTAL</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title ibox_gren">
                                                <span class="label label-primary pull-right font-sm" id="total_sg_memos" style="background-color:#0c2461 !important;color:#fff;">{{$internos->data->sinformato->generados->memos_x100}}%</span>
                                                <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                <h5>MEMOS</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <h1 class="no-margins" id="memos">{{$internos->data->generados->memos }}</h1>
                                                <small>MEMO</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title ibox_gren">
                                                <span class="label label-primary pull-right font-sm" id="total_sg_oficios" style="background-color:#0c2461 !important;color:#fff;">{{$internos->data->sinformato->generados->oficios_x100}}%</span>
                                                <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                <h5>OFICIOS</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <h1 class="no-margins" id="oficios">{{$internos->data->generados->oficios }}</h1>
                                                <small>OFICIOS</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title ibox_gren">
                                                <span class="label label-primary pull-right font-sm" id="total_sg_informes" style="background-color:#0c2461 !important;color:#fff;">{{$internos->data->sinformato->generados->informes_x100}}%</span>
                                                <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                <h5>INFORMES</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <h1 class="no-margins" id="informes">{{$internos->data->generados->informes }}</h1>
                                                <small>INFORMES</small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title ibox_gren">
                                                <span class="label pull-right" style="background-color:0;color:#FFFFFF;"></span>
                                                <h5>AHORRO DE HOJAS</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <h1 class="no-margins" id="ahorro_hojas_internos">{{$internos->data->hojasAhorradas }}</h1>
                                                <small>HOJAS</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12">
                    <figure class="highcharts-figure">
                        <div id="piechart3"></div>
                    </figure>
                </div>
            </div>
        </div>
    </div>
@stop

@section ('scripts')
 <script>
    $(document).ready(function() {
        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd',
            language: 'es'
        });

        ArrayPortal = [];
        ArrayVentanilla = [];


        ArrayPortal.push({ y:parseInt('{{$externos->data->sinformato->ingresados->secretariaGeneral->portal}}'), porcentaje:'{{$externos->data->sinformato->ingresados->secretariaGeneral->portal_x100}}'});
        ArrayPortal.push({ y:parseInt('{{$externos->data->sinformato->ingresados->permisosMunicipales->portal}}'), porcentaje: '{{$externos->data->sinformato->ingresados->permisosMunicipales->portal_x100}}'});
        ArrayVentanilla.push({ y:parseInt('{{$externos->data->sinformato->ingresados->secretariaGeneral->ventanilla}}'), porcentaje:'{{$externos->data->sinformato->ingresados->secretariaGeneral->ventanilla_x100}}'});
        ArrayVentanilla.push({ y:parseInt('{{$externos->data->sinformato->ingresados->permisosMunicipales->ventanilla}}'), porcentaje:'{{$externos->data->sinformato->ingresados->permisosMunicipales->ventanilla_x100}}'});

        dasboard();

        arrayDataPie = [];
        arrayDataPie.push({ name:'PENDIENTES', y:parseInt('{{$externos->data->sinformato->estados->pendientes}}'), porcentaje:'{{$externos->data->sinformato->estados->pendientes_x100}}'});
        arrayDataPie.push({ name:'EN PROCESO', y:parseInt('{{$externos->data->sinformato->estados->enProceso}}'), porcentaje:'{{$externos->data->sinformato->estados->enProceso_x100}}'});
        arrayDataPie.push({ name:'APROBADOS', y:parseInt('{{$externos->data->sinformato->estados->aprobados}}'), porcentaje:'{{$externos->data->sinformato->estados->aprobados_x100}}'});
        arrayDataPie.push({ name:'FINALIZADOS', y:parseInt('{{$externos->data->sinformato->estados->finalizados->total}}'), porcentaje:'{{$externos->data->sinformato->estados->finalizados_x100}}'});
        show_pie('piechart', arrayDataPie, 'TRÁMITES EXTERNOS');

        // arrayDataPie = [];
        // arrayDataPie.push({ name:'PENDIENTES DE PAGO', y:parseInt('{{$externos->data->sinformato->estados->finalizados->pendientePago}}'), porcentaje:'{{$externos->data->sinformato->estados->finalizados->pendientePago_x100}}'});
        // arrayDataPie.push({ name:'RESPUESTA FÍSICA', y:parseInt('{{$externos->data->sinformato->estados->finalizados->respuestaFisica}}'), porcentaje:'{{$externos->data->sinformato->estados->finalizados->respuestaFisica_x100}}'});
        // arrayDataPie.push({ name:'RESPUESTA DIGITAL', y:parseInt('{{$externos->data->sinformato->estados->finalizados->respuestaDigital}}'), porcentaje:'{{$externos->data->sinformato->estados->finalizados->respuestaDigital_x100}}'});
        // show_pie('piechart2', arrayDataPie, 'TRÁMITES EXTERNOS FINALIZADOS');

        arrayDataPie = [];
        arrayDataPie.push({ name:'MEMOS', y:parseInt('{{$internos->data->sinformato->generados->memos}}'), porcentaje: '{{$internos->data->sinformato->generados->memos_x100}}'});
        arrayDataPie.push({ name:'OFICIOS', y:parseInt('{{$internos->data->sinformato->generados->oficios}}'), porcentaje:'{{$internos->data->sinformato->generados->oficios_x100}}'});
        arrayDataPie.push({ name:'INFORMES', y:parseInt('{{$internos->data->sinformato->generados->informes}}'), porcentaje: '{{$internos->data->sinformato->generados->informes_x100}}'});
        show_pie('piechart3', arrayDataPie, 'TRÁMITES INTERNOS GENERADOS');
    })

    function verTramites(estado)
    {
        let desde = $('#date_added').val();
        let hasta = $('#date_modified').val();
        $('#estado').val(estado)
        loadTable();
        $('#modalTramites').modal()
    }

    function loadTable()
    {
        let desde = $('#date_added').val();
        let hasta = $('#date_modified').val();
        let estado = $('#estado').val();
        
        var table = $('#tabla-tramites').DataTable({
            processing: true,
            destroy: true,
            serverSide: true,
            responsive: true,
            searching: false,
            order: [[ 0, 'DESC' ]],
            ajax: '{!! route('tramites.estados') !!}?estado='+estado+'&desde='+desde+'&hasta='+hasta,
            language: { url: '../../language/es.json' },
            lengthMenu: [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, 'TODOS']],
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'pdfHtml5',
                    title: 'Permisos Municipales Solicitados',
                    text: '<i class="fa fa-file-pdf"></i> PDF'
                },
                {
                    extend: 'excelHtml5',
                    title: 'Permisos Municipales Solicitados',
                    footer: true
                },
            ],
            columns: [
                { data: 'id', name: 'id' },
                { data: 'numtramite', name: 'numtramite' },
                { data: 'fecha_ingreso', name: 'fecha_ingreso' },
                { data: 'tipoTramite', name: 'tipoTramite' },
                { data: 'remitente', name: 'remitente' },
                { data: 'atender', name: 'atender' },
                // { data: 'copia', name: 'copia' },
                { data: 'analistas', name: 'analistas' },
                { data: 'fecha_fin', name: 'fecha_fin' },
                // { data: 'adjunto', name: 'adjunto' },
                // { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    }

    function filtrar(todos)
    {
        let desde = todos ? 'null' : $('#date_added').val();
        let hasta = todos ? 'null' : $('#date_modified').val();
        
        if(todos)
        {
            $('#date_added').val('2020-06-10')
            $('#date_modified').val('2020-08-11')
        }

        tramitesExternos(desde, hasta)
        tramitesInternos(desde, hasta)
    }


    var arrayDataPie = [];
    var arrayDataPie2 = [];
    var ArrayPortal = [];
    var ArrayVentanilla = [];
    function tramitesExternos(desde, hasta) {
        $.ajax({
            type: 'GET',
            url: `/tramitesalcaldia/tramitesexternosdashboard/${desde}/${hasta}`
        }).done((data) => {
            $('#total_ingresados').text(data.data.ingresados.total);
            $('#sg_ingresados_portal').text(data.data.ingresados.secretariaGeneral.portal);
            $('#sg_ingresados_ventanilla').text(data.data.ingresados.secretariaGeneral.ventanilla);
            $('#ahorro_hojas').text(data.data.hojasAhorradas);
            $('#pm_ingresados_portal').text(data.data.ingresados.permisosMunicipales.portal);
            $('#pm_ingresados_ventanilla').text(data.data.ingresados.permisosMunicipales.ventanilla);
            $('#pendientes').text(data.data.estados.pendientes);
            $('#en_proceso').text(data.data.estados.enProceso);
            $('#aprobados').text(data.data.estados.aprobados);
            $('#finalizados_total').text(data.data.estados.finalizados.total);
            $('#pendiente_pago').text(data.data.estados.finalizados.pendientePago);
            $('#respuesta_fisica').text(data.data.estados.finalizados.respuestaFisica);
            $('#respuesta_digital').text(data.data.estados.finalizados.respuestaDigital);
            $('#total_sg').text(data.data.ingresados.secretariaGeneral.total);
            $('#total_pm').text(data.data.ingresados.permisosMunicipales.total);
            ArrayPortal = [];
            ArrayVentanilla = [];
            
            ArrayPortal.push({ y:parseInt(data.data.sinformato.ingresados.secretariaGeneral.portal), porcentaje: data.data.sinformato.ingresados.secretariaGeneral.portal_x100});
            ArrayPortal.push({ y:parseInt(data.data.sinformato.ingresados.permisosMunicipales.portal), porcentaje: data.data.sinformato.ingresados.permisosMunicipales.portal_x100});
            ArrayVentanilla.push({ y:parseInt(data.data.sinformato.ingresados.secretariaGeneral.ventanilla), porcentaje: data.data.sinformato.ingresados.secretariaGeneral.ventanilla_x100});
            ArrayVentanilla.push({ y:parseInt(data.data.sinformato.ingresados.permisosMunicipales.ventanilla), porcentaje: data.data.sinformato.ingresados.permisosMunicipales.ventanilla_x100});
            
            dasboard();
            arrayDataPie = [];

            arrayDataPie.push({ name:'PENDIENTES', y:parseInt(data.data.sinformato.estados.pendientes), porcentaje: data.data.sinformato.estados.pendientes_x100});
            arrayDataPie.push({ name:'EN PROCESO', y:parseInt(data.data.sinformato.estados.enProceso),  porcentaje: data.data.sinformato.estados.enProceso_x100});
            arrayDataPie.push({ name:'APROBADOS', y:parseInt(data.data.sinformato.estados.aprobados),  porcentaje: data.data.sinformato.estados.aprobados_x100});
            arrayDataPie.push({ name:'FINALIZADOS', y:parseInt(data.data.sinformato.estados.finalizados.total), porcentaje:data.data.sinformato.estados.finalizados_x100});
            show_pie('piechart', arrayDataPie, 'TRÁMITES EXTERNOS');
            
            arrayDataPie = [];
            arrayDataPie.push({ name:'PENDIENTES DE PAGO', y:parseInt(data.data.sinformato.estados.finalizados.pendientePago), porcentaje: data.data.sinformato.estados.finalizados.pendientePago_x100});
            arrayDataPie.push({ name:'RESPUESTA FÍSICA', y:parseInt(data.data.sinformato.estados.finalizados.respuestaFisica), porcentaje: data.data.sinformato.estados.finalizados.respuestaFisica_x100});
            arrayDataPie.push({ name:'RESPUESTA DIGITAL', y:parseInt(data.data.sinformato.estados.finalizados.respuestaDigital), porcentaje: data.data.sinformato.estados.finalizados.respuestaDigital_x100});
            show_pie('piechart2', arrayDataPie, 'TRÁMITES EXTERNOS FINALIZADOS');

        });
    }

    function tramitesInternos(desde, hasta) {
        $.ajax({
            type: 'GET',
            url: `/tramitesalcaldia/tramitesinternosdashboard/${desde}/${hasta}`
        }).done((data) => {
            $('#total_internos').text(data.data.generados.total)
            $('#memos').text(data.data.generados.memos)
            $('#oficios').text(data.data.generados.oficios)
            $('#informes').text(data.data.generados.informes)
            $('#ahorro_hojas_internos').text(data.data.hojasAhorradas)
            $('#total_sg_memos').text(`${data.data.sinformato.generados.memos_x100}%`)
            $('#total_sg_oficios').text(`${data.data.sinformato.generados.oficios_x100}%`)
            $('#total_sg_informes').text(`${data.data.sinformato.generados.informes_x100}%`)

            arrayDataPie = [];
            arrayDataPie.push({ name:'MEMOS', y:data.data.sinformato.generados.memos, porcentaje:data.data.sinformato.generados.memos_x100});
            arrayDataPie.push({ name:'OFICIOS', y:data.data.sinformato.generados.oficios, porcentaje:data.data.sinformato.generados.oficios_x100 });
            arrayDataPie.push({ name:'INFORMES', y:data.data.sinformato.generados.informes, porcentaje: data.data.sinformato.generados.informes_x100});
            show_pie('piechart3', arrayDataPie, 'TRÁMITES INTERNOS GENERADOS');

        })
    }

    function dasboard(){
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: null
            },
            subtitle: {
                text: '<b style="color: #000; font-weight: bold">TRÁMITES EXTERNOS INGRESADOS<b>'
            },
            xAxis: {
                categories: [
                    'SECRETARÍA GENERAL',
                    'PERMISOS MUNICIPALES'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Cantidad de documentos'
                }
            },
            colors: ['#A3CB38', '#0984e3'],
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f}  ({point.porcentaje}%)</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                pie: {
                    shadow: false,
                    center: ['50%', '50%']
                },
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        format:  '<span>{series.name}</span>:<br><b >{point.y}</b><br><b style="font-size:1.2em">({point.porcentaje}%)</b>',
                        enabled: true,
                        crop: false,
                        overflow: 'none'
                    }
                }
            },
            series: [{
                name: 'P. CIUDADANO',
                data: ArrayPortal

            }, {
                name: 'VENTANILLA',
                data: ArrayVentanilla

            }]
        });
    }

    function show_pie(Name, ArrayPie, Title) {
        Highcharts.chart(Name, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text:null
                
            },
            subtitle: {
                text:'<b style="color: #000; font-weight: bold">'+Title+'<br>'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b> <b>({point.porcentaje}%)</b>'
            },
            colors: ['#0d233a', '#1aadce', '#8bbc21', '#910000', '#1aadce',
                '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: <br><b>{point.y}</b> <br><b style="font-size:1.2em">({point.porcentaje}%)</b>'
                    },
                    showInLegend: true
                }
            },
            legend: {
                enabled: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Cantidad',
                data: ArrayPie
            }]
        });
    }
</script>
@stop
