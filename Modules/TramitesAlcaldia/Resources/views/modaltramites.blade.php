<div class="modal fade" tabindex="-1" role="dialog" id="modalTramites">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">TRÁMITES EXTERNOS</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover display" id="tabla-tramites" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Número de trámite</th>
                                <th>Fecha de ingreso</th>
                                <th>Tipo de trámite</th>
                                <th>Remitente</th>
                                <th>Dirección a atender</th>
                                {{-- <th>Con copia</th> --}}
                                <th>Analistas asignados</th>
                                <th>Fecha estimada de finalización</th>
                                {{-- <th>Adjunto</th> --}}
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>