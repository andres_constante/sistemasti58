<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Comprobante</title>
</head>

<body>


  <style>
    .x10 {
      font-size: 11px;
    }

    .x20 {
      font-size: 14px;
    }

    .x23 {
      font-size: 12px;
    }

    .centrado {
      text-align: center;
    }
  </style>

  <div id="general">
    <div class="centrado x23" style="padding-bottom: 10px;">
      <b>GOBIERNO AUTÓNOMO DESCENTRALIZADO
        MUNICIPAL DEL CANTÓN MANTA</b><br>



    </div>
    <div id="contenedor">
      <table width="100%" cellpadding="0" cellspacing="0" align="center" class="table">
        <tbody>
          <tr>
            <td align="center"> <span class="x20">No. Trámite asignado</span></td>
          </tr>
          <tr>
            <td align="center"><span class="x20"> <b>{{$tabla->numtramite}}</b></span></td>
          </tr>
          <tr>
            <td align="center"><span class="x20">
                @php
                if($tabla->tipo==null){
                echo 'Secretaria general';
                }else{
                echo 'Trámite municipal';
                }
                @endphp
              </span></td>
          <tr>
          <tr>
            <td align="center"><span class="x20"><br></span></td>
          </tr>
        </tbody>
      </table>
      <table width="100%" cellpadding="0" cellspacing="0" align="center" class="table">
        <tbody>
          <tr>
            <td align="center"> <span class="x20"><b>Remitente:</b></span></td>
          </tr>
          <tr>
            <td align="center"><span class="x20">{{$tabla->cedula_remitente}}</span></td>
          </tr>
          <tr>
            <td align="center"><span class="x10">{{$tabla->remitente}}</span></td>
          <tr>


          <tr>
            <td align="center"><span class="x20"><br></span></td>
          </tr>
          <tr>
            <td align="center"> <span class="x20"><b>Fecha y hora de entrega:</b></span></td>
          </tr>
          <tr>
            <td align="center"><span class="x20">{{fechas(600,$tabla->fecha_ingreso)}}</span></td>
          </tr>
          <tr>
            <td align="center"><span class="x20">{{fechas(1200,$tabla->create_at)}}</span></td>
          <tr>
          <tr>
            <td align="center"><span class="x20"><br></span></td>
          </tr>


          <?php
          if (isset($anexos[0])) {
            # code...
            echo '<tr><td align="center"><span class="x20"><b>Anexos:</b></span></td></tr>';
          }
          foreach ($anexos as $key => $value) {
            # code...
            echo  '<tr><td align="center"><span class="x20">' . $value->nombre . ' <b>(' . $value->hojas . ' h.)</b></span></td></tr>';
          }
          ?>
        </tbody>
      </table>

    </div>
    <div class="centrado">
      <p class="x20">
        Puede revisar la trazabilidad del su solicitud
        en: https://portalciudadano.manta.gob.ec/
      </p>
    </div>

    <script type="text/javascript">
      function printHTML() {
        if (window.print) {
          window.print();
          setTimeout(() => {

            window.close();
          }, 2000);
        }
      }
      document.addEventListener("DOMContentLoaded", function(event) {
        printHTML();
      });
    </script>
</body>

</html>