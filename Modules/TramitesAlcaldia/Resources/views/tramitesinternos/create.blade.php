@extends('layout')

@section ('titulo') {{ $configuraciongeneral[0] }} @stop

@section('contenido')
<div class="row animated fadeInRight">
    <div class="col-lg-12">
        <div class="imgback">
            <h1>{{ $configuraciongeneral[4] }}</h1>

            <div class="ibox float-e-margins">
                {{-- <div class="ibox-title">
                    <div>
                        <a href="{{ route('tramitesinternos.index') }}" class="btn btn-primary ">Todos</a>
                        <a href="{{ route('tramitesinternos.create') }}" class="btn btn-default ">Nuevo</a>
                    </div>
                </div> --}}
                
                <div class="ibox-content">
                    @include('tramitesalcaldia::tramitesinternos.errors')
                    @if ($configuraciongeneral[3] == 'crear')
                        {!! Form::open(['route' => $configuraciongeneral[1], 'method' => 'POST', 'id' => "form", 'class' => 'form-horizontal','files' => true]) !!}
                            @include('tramitesalcaldia::'.$configuraciongeneral[2], ['edicion' => false])
                    @elseif($configuraciongeneral[3] == 'editar')
                        {!! Form::model($tabla, ['route' => [$configuraciongeneral[1], Crypt::encrypt($tabla->id)], 'method' => 'PUT', 'id' => "form", 'class' => 'form-horizontal','files' => true]) !!}
                            @include('tramitesalcaldia::'.$configuraciongeneral[2], ['edicion' => true])
                    @endif
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@include("vistas.includes.mainjs")

@stop

@section('scripts')
    <script src="{{ asset('js/tramitesinternos.js').'?v='.rand(1,1000) }}"></script>
@endsection