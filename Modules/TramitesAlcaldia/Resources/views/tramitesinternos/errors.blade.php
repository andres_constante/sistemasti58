@if ($errors->any())
    <div class="row">
        <div class="col-12 col-md-6 col-md-offset-3">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li class="text-center">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif

@if(session('warning'))
    <div class="row ">
        <div class="col-12 col-md-6 col-md-offset-3">
            <div class="alert alert-warning alert-dismissible text-center">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('warning') }}
            </div>
        </div>
    </div>
@endif

@if (Session::has('message'))
<script>
    $(document).ready(function() {
        toastr["success"]("{{ Session::get('message') }}");
    });
</script>
@endif