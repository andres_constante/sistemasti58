<?php
if(!isset($nobloqueo))
?>
@extends(Input::has("menu") ? 'layout_basic_no_head':'layout' )

@section ('titulo') Agendar Trámite @stop

<style>
    .loader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 90%;
        z-index: 9999;
        opacity: .8;
        background: url({{ asset('img/loading35.gif') }}) 50% 50% no-repeat rgb(249,249,249);
    }

    .disabled {
        pointer-events: none !important;
        color: #AAA !important;
        background: #F5F5F5 !important;
    }
</style>

@section ('contenido')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="imgback">
                    <h1>Agendar trámite</h1>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="">
                            <a href="{{ route('tramites.index') }}" class="btn btn-primary "><i class="fa fa-align-justify"></i> Todos</a>
                            <a href="{{ route('tramites.create') }}" class="btn btn-default ">Volver</a>
                        </div>

                        @if($errors->all())
                            <script>
                            $(document).ready(function() {
                                toastr["error"]($("#diverror").html());    
                            });
                            </script>
                            <div class="alert alert-danger" style="text-align: left;" id='diverror'>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Error!</h4>
                            <div id="diverror">
                                <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                                </ul>
                            </div>
                            </div>
                        @endif

                        @if (Session::has("warning"))
                            <script>
                                $(document).ready(function() {
                                    toastr["warning"]("{{ Session::get('warning') }}");
                                });
                            </script>
                            <div class="alert alert-warning">⚠ Advertencia ⚠ <br>{{ Session::get('warning') }}</div>
                        @endif
                    </div>

                    <div class="ibox-content">
                        {!! Form::open(array('route' => 'agenda.store','role'=>'form' , 'id'=>'form-tram','class'=>'form-horizontal', 'files' => true)) !!}
                            <div class="form-group">
                                <div class="loader" id="position" style="diplay: none;"></div>
                                {!! Form::hidden('ubicacion', null, ['id' => 'ubicacion']) !!}
                            </div>
                        
                            @foreach($objetos as $i => $campos) 
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="form-group">
                                            {!! Form::label($campos->Nombre, $campos->Descripcion, []) !!}    
                                            @if($campos->Tipo=="select")
                                                {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) :
                                                ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' =>
                                                "form-control ".$campos->Clase)) !!}
                                            @elseif($campos->Tipo=="textarea2")
                                                {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' .
                                                $campos->Clase ,'placeholder'=>'Ingrese '.$campos->Descripcion, 'rows' => 3)) !!}
                                            @elseif($campos->Tipo=="datetimetext")
                                                <div id="date_{{$campos->Nombre}}" class="input-group datetime">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                    
                                                    {!! Form::text($campos->Nombre, $campos->Valor , array(
                                                    'data-placement' =>'top',
                                                    'data-toogle'=>'tooltip',
                                                    'class' => 'form-control datefecha '.$campos->Clase,
                                                    'placeholder'=>$campos->Descripcion,
                                                    $campos->Nombre,
                                                    )) !!}
                                                </div>
                                            @elseif($campos->Tipo=="file")
                                                {!! Form::file($campos->Nombre, array('class' => 'form-control '.$campos->Clase)) !!}
                                            @else
                                                {!! Form::text($campos->Nombre, $campos->ValorAnterior!="Null" ? $campos->ValorAnterior :
                                                Input::old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                                
                            <div class="row">
                                <div class="col-xs-10 col-md-8 col-md-offset-2">
                                    <div style="width: 600px; height: 400px;" id="map_canvas"></div>
                                </div>
                            </div>

                            {!! Form::submit("Guardar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include("vistas.includes.mainjs")
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy1ViaIr_ziZYThMeiNuvrZK7pUb7X_SI&libraries=places"></script>
    <script src="{{ asset('js/tramites-agenda.js') }}"></script>
@endsection