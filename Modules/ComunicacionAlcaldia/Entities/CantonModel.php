<?php

namespace Modules\ComunicacionAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;

class CantonModel extends Model
{
    protected $table = 'tmo_canton';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'nombre_canton'=>'required|unique:parroquia'. ($id ? ",id,$id" : '')
		], $merge);
    } 
}
