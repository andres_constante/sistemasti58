<?php

namespace Modules\ComunicacionAlcaldia\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AgendaVirtualModel extends Model
{
    protected $table = 'com_tmov_comunicacion_agenda';
    protected $hidden = [];
    public static function rules ($id=0, $merge=[]) {
       return array_merge(
       [                
           'nombre'=>'required',
           'descripcion'=>'required',
           'direccion'=>'required',
           'latitud'=>'required',
           'longitud'=>'required',
           'fecha_inicio'=>'required',
           'fecha_fin'=>'required',
           'barrio_id'=>'required'

       ], $merge);
   }
    public static function rules_interna ($id=0, $merge=[]) {
       return array_merge(
       [                
           'nombre'=>'required',
           'direccion'=>'required',
           'latitud'=>'required',
           'longitud'=>'required',
           'fecha_inicio'=>'required',
           'fecha_fin'=>'required'
       ], $merge);
   }

   public function usuario()
   {
        return $this->belongsTo(User::class);
   }
}
