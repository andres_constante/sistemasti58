<?php

namespace Modules\ComunicacionAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;

class AgendaEstadosModel extends Model
{
    protected $table = 'com_tma_estados_agenda';
    protected $hidden = [];
    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'nombre' => 'required'
            ],
            $merge
        );
    }
}
