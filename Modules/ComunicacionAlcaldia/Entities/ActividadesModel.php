<?php namespace Modules\Comunicacionalcaldia\Entities;
   
use Illuminate\Database\Eloquent\Model;

class actividadesModel extends Model {

    protected $table = 'com_tmae_actividades';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [   'id',       
			'nombre'=>'required'
		], $merge);
    } 

}