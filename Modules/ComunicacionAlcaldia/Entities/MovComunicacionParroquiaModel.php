<?php namespace Modules\Comunicacionalcaldia\Entities;
   
use Illuminate\Database\Eloquent\Model;

class MovComunicacionParroquiaModel extends Model {

    protected $fillable = [];

    protected $table = 'com_tmov_comunicacion_parroquia';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'id_com_cab'=>'required',
                'id_parroquia	'=>'required',                
                'avance'=>'required|numeric',
                'calles'=>'required'
            ], $merge);
        }
}