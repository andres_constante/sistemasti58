<?php

namespace Modules\ComunicacionAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;

class AgendaNotificacionesPersonalizadas extends Model
{
    protected $table = 'com_tmo_notifiacion_personalizada';
}
