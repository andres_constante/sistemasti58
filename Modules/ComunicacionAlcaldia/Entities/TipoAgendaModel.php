<?php

namespace Modules\ComunicacionAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;

class TipoAgendaModel extends Model
{
    protected $table = 'com_tma_tipo_agenda';
    protected $hidden = [];
    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'tipo' => 'required'
            ],
            $merge
        );
    }
}
