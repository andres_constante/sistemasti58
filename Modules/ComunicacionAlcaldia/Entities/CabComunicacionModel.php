<?php namespace Modules\Comunicacionalcaldia\Entities;
   
use Illuminate\Database\Eloquent\Model;

class cabComunicacionModel extends Model {

    protected $table = 'com_tmov_comunicacion_cab';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'id_tipo_actividad'=>'required|numeric',
            'nombre_actividad'=>'required',
            'id_direccion'=> 'required|numeric', 
            'zona'=> 'required', 
            'beneficiarios'=> 'required|numeric', 
            'observacion'=> 'required', 
            'fecha_inicio'=> 'required|date', 
            'fecha_fin'=> 'required|date', 
            'kilometros'=> 'required|numeric', 
            'idusuario'=> 'required',
            'txtpresupuesto'=>'required',
            'txtavancebapa'=>'required'
		], $merge);
    } 

    protected $tableCabPresupuesto = 'com_tmov_comunicacion_personas';
    public static function rulesCabPresupuesto ($id=0, $merge=[]) {
        return array_merge([
            'id_com_cab' => 'required',
            'monto_inicial' => 'required',
            'monto_final' => 'required',
            'idusuario' => 'required',
            'observacion' => 'required'
        ], $merge);
    }

}