<?php namespace Modules\Comunicacionalcaldia\Entities;
   
use Illuminate\Database\Eloquent\Model;

class parroquiaModel extends Model {

    protected $table = 'parroquia';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'parroquia'=>'required|unique:parroquia'. ($id ? ",id,$id" : ''),
			'zona'=>'required'
		], $merge);
    } 

}