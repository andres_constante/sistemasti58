<?php

namespace Modules\ComunicacionAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;

class AgendaVistaModel extends Model
{
    protected $table = 'com_tma_visto_agenda';
}
