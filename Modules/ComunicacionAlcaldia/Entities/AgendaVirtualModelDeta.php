<?php

namespace Modules\ComunicacionAlcaldia\Entities;

use Illuminate\Database\Eloquent\Model;

class AgendaVirtualModelDeta extends Model
{
    protected $table = 'com_tmov_comunicacion_agenda_deta';
}
