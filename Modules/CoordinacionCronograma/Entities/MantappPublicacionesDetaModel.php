<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class MantappPublicacionesDetaModel extends Model
{
    protected $fillable = [];
    protected $table="man_tmov_publicaciones_deta";
}
