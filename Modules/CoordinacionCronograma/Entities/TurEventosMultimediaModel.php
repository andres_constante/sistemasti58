<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class TurEventosMultimediaModel extends Model
{
    protected $table = 'tur_agenda_archivos';
}
