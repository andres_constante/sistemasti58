<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class MantappPublicacionesArchivosModel extends Model
{
    protected $fillable = [];
    protected $table="man_tmov_publicaciones_archivos";
     
}
