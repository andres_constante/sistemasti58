<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020SancionesTiposModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmae_tipo_sancion";
}
