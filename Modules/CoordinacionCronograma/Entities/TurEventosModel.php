<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class TurEventosModel extends Model
{
    protected $table = 'tur_agenda';
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                
            'evento'=>'required',
            'direccion'=>'required',
            'latitud'=>'required',
            'longitud'=>'required',
            'fecha_inicio'=>'required',
            'fecha_fin'=>'required'
        ], $merge);
    }
 
}
