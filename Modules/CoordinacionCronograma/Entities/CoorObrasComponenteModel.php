<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorObrasComponenteModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmov_obras_componente";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'id_obra_cab'=>'required',
                'id_objetivo_componente'=>'required'
            ], $merge);
    }
}
