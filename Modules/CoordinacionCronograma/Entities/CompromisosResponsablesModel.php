<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CompromisosResponsablesModel extends Model
{
    protected $fillable = [];
    protected $table ="coor_tmov_compromisos_resposables";
}
