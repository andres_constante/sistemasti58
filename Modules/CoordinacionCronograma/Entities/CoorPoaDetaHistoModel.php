<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorPoaDetaHistoModel extends Model
{
    protected $fillable = [];
    protected $table="poa_tmae_deta_histo";
}
