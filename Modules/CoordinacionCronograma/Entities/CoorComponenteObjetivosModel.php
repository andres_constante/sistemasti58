<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorComponenteObjetivosModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmae_cronograma_componente_objetivo";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                                
                'objetivo'=>'required|unique:coor_tmae_cronograma_componente_objetivo'. ($id ? ",id,$id" : ''),
                'id_componente'=>'required',
                'programa'=>'required'
            ], $merge);
        }  
}
