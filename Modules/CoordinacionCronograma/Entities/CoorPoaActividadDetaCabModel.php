<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorPoaActividadDetaCabModel extends Model
{
   
    protected $fillable = [];
    protected $table="poa_tmae_cab_actividad_deta";
}
