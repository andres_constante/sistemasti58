<?php namespace Modules\Coordinacioncronograma\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CoorContratistaModel extends Model {

    protected $fillable = [];
    protected $table="coor_tmae_contratista";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'ruc'=>'required|unique:coor_tmae_contratista'. ($id ? ",id,$id" : ''),
                'nombres'=>'required'/*,
                'direccion'=>'required',
                'telefono'=>'required',
                'correo'=>'required|numeric'*/
            ], $merge);
        }  

}