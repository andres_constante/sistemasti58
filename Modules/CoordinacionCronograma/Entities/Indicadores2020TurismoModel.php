<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020TurismoModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmov_turismo";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                                
            'n_habitantes'=>'required',
            'n_beneficiarios'=>'required',
            'id_tipo_servicio'=>'required',
            'inversion'=>'required'
        ], $merge);
    } 
}
