<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorIndicadorTipoFiltroModel extends Model
{
    protected $fillable = [];
    protected $table="ind_tmov_indicador_tipo_filtro";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'filtro'=>'required|unique:ind_tmov_indicador_tipo_filtro'. ($id ? ",id,$id" : ''),
                'id_campotxt'=>'required|unique:ind_tmov_indicador_tipo_filtro'. ($id ? ",id,$id" : ''),
                'tipo'=>'required',
                'id_indicador_mae'=>'required'
            ], $merge);
    }
}
