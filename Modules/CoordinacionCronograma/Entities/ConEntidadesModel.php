<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class ConEntidadesModel extends Model
{
    protected $table = 'con_entidades';
    protected $hidden = [];
    public static function rules ($id=0, $merge=[]) {
       return array_merge(
       [                
           'nombre'=>'required|unique:con_entidades'. ($id ? ",id,$id" : ''),
       ], $merge);
   } 
}
