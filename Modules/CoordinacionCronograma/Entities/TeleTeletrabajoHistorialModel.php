<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class TeleTeletrabajoHistorialModel extends Model
{
    protected $table = 'tele_tmov_teletrabajo_deta';
    protected $hidden = [];
    protected $fillable = [];
}
