<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class TurEventosCategoriasModel extends Model
{
    protected $table = 'tur_agenda_categoria';
}
