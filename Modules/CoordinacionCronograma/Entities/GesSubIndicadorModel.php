<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class GesSubIndicadorModel extends Model
{
    protected $fillable = [];
    protected $table="ges_tmov_gestion_indicador_sub";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                                
                // 'id_cab_ges'=>'required|unique:ges_tmov_gestion_indicador_sub'. ($id ? ",id,$id" : ''),
                'nombresub'=>'required',
                'medida'=>'required'
            ], $merge);
        } 
}
