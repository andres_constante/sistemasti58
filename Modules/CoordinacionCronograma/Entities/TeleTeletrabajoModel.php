<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class TeleTeletrabajoModel extends Model
{
    protected $table = 'tele_tmov_teletrabajo';
    protected $hidden = [];
    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'id_semana' => 'required',
                // 'observacion' => 'required'
            ],
            $merge
        );
    }
}
