<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class MultasUnaserDetaModel extends Model
{
    protected $fillable = [];
    protected $table="una_tmo_multas_deta";
}
