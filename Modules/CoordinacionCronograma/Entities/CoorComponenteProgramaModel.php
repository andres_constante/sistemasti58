<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorComponenteProgramaModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmae_cronograma_componente_programa";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [
                'programa'=>'required|unique:coor_tmae_cronograma_componente_programa'. ($id ? ",id,$id" : ''),
                'id_componente'=>'required',
                'programa'=>'required'
            ], $merge);
        }
}
