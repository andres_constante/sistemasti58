<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoordinacionDetaDireccionModel extends Model
{
    protected $fillable = [];
    protected $table = "coor_tmov_coordinacion_direccion_deta";
}
