<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020FaunaTipoAtencionModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmov_fauna_tipo_atencion";
}
