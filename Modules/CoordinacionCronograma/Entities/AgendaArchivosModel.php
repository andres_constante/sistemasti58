<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class AgendaArchivosModel extends Model
{
    protected $fillable = [];
    protected $table="com_tmov_comunicacion_agenda_archivos";
}
