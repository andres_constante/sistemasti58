<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorPoaActividadesTareasModel extends Model
{
    protected $fillable = [];
    protected $table="poa_tmae_cab_actividad_tarea";

    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                //'proyecto'=>'required|unique:poa_tmae_cab'. ($id ? ",id,$id" : ''),
                'tarea'=>'required',
                'fecha_fin'=>'required',
                'fecha_inicio'=>'required',
                'estado_tarea'=>'required',
                'avance'=>'required',
                'id_actividad'=>'required',

            ], $merge);
            
    }
    public static function rulesdir ($id=0, $merge=[]) {
        return array_merge(
            [
                'id'=>'required',
                'avance'=>'required'

            ], $merge);
            
    }
}
