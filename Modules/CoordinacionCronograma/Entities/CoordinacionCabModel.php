<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoordinacionCabModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmae_coordinacion_main";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'nombre'=>'required|unique:coor_tmae_coordinacion_main'. ($id ? ",id,$id" : ''),
                'id_responsable'=>'required',
                'id_direccion'=>'required'
            ], $merge);
    }
}
