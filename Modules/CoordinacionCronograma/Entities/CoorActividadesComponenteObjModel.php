<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorActividadesComponenteObjModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmov_actividades_componente";
}
