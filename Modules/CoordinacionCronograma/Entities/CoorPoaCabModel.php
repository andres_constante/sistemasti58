<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorPoaCabModel extends Model
{
    protected $fillable = [];
    protected $table="poa_tmae_cab";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                //'proyecto'=>'required|unique:poa_tmae_cab'. ($id ? ",id,$id" : ''),
                'proyecto'=>'required',
                'id_direccion'=>'required',
                //'monto_asignado'=>'required',
                'objetivo'=>'required'
            ], $merge);
    }
}
