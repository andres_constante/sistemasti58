<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020Model extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmov_cab";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                                
            'id_indicador'=>'required',
            'cedula'=>'required',
            'pais'=>'required',
            'nombres'=>'required',
            'apellidos'=>'required',
            'discapacidad'=>'required',
            'barrio_id'=>'required',
            'direccion'=>'required'
        ], $merge);
    } 
    public static function rules_2 ($id=0, $merge=[]) {
        return array_merge(
        [                                
            'id_indicador'=>'required',
            'barrio_id'=>'required'
        ], $merge);
    } 
}
