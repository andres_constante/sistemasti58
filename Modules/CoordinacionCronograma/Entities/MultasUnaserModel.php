<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class MultasUnaserModel extends Model
{
    protected $fillable = [];
    protected $table="una_tmo_multas";

    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'placa'=>'required',
                'nombre'=>'required',
                'cedula'=>'required',
                'valor'=>'required',
                'fecha'=>'required',
                'descripcion'=>'required'

            ], $merge);
            
    }
}
