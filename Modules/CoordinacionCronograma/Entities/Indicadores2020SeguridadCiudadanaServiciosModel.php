<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020SeguridadCiudadanaServiciosModel extends Model
{
    protected $table="ind2020_tmae_seguridad_ciudadana";
}
