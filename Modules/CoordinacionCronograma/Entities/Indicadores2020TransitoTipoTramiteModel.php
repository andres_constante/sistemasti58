<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020TransitoTipoTramiteModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmov_transito_tipo_tramite";
}
