<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class PlanCampaniaResponsableModel extends Model
{
    
    protected $fillable = [];
    protected $table="plan_tmov_responsable";
}
