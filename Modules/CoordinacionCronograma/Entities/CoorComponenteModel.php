<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorComponenteModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmae_cronograma_componente_cab";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                                
                'nombre'=>'required|unique:coor_tmae_cronograma_componente_cab'. ($id ? ",id,$id" : ''),
                'detalle'=>'required'
            ], $merge);
        }  
}
