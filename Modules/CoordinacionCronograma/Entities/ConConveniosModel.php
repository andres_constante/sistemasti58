<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class ConConveniosModel extends Model
{
    protected $fillable = [];
    protected $table ="con_convenios";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                                
            // 'nombre'=>'required|unique:coor_tmae_cronograma_componente_cab'. ($id ? ",id,$id" : ''),
            // 'detalle'=>'required'
        ], $merge);
    } 
}
