<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class ObraActividadDetaModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmov_obras_actividades_deta";
}
