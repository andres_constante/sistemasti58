<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorProyectoEstrategicoActividadesDeta extends Model
{
    protected $fillable = [];
    protected $table="coor_tmov_proyecto_estrategico_actividad_deta";
}
