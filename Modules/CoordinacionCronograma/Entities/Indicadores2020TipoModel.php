<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020TipoModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmae_tipo";

}
