<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020EventosModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmov_eventos";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                                
            'n_habitantes'=>'required',
            'n_beneficiarios'=>'required',
            'id_tipo_evento'=>'required',
            'inversion'=>'required',
            'id_tipo_artista'=>'required'
            // 'id_indicador_cab'=>'required'
        ], $merge);
    } 
}
