<?php namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CabRecomendacionContraModel extends Model
{
    protected $fillable = [];
    protected $table ="coor_tmae_recomendacion_contraloria";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'tipo_documento'=>'required',
                'numero_documento'=>'required',
                'recomendacion'=>'required|unique:coor_tmae_recomendacion_contraloria'. ($id ? ",id,$id" : ''),
                'periodo'=>'required',
                'estado_proceso'=>'required',
                'actividad'=>'required',
                'fecha_inicio'=>'required',
                'fecha_fin'=>'required'
            ], $merge);
        }  
}
