<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020TransitoModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmov_transito";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                                
            'id_tipo_tramite'=>'required',
            'cantidad'=>'required',
            // 'telefono'=>'required'
            // 'id_indicador_cab'=>'required'
        ], $merge);
    } 
}
