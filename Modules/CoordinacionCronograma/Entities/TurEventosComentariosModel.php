<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class TurEventosComentariosModel extends Model
{
    protected $table = 'tur_agenda_comentarios';
}
