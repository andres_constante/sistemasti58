<?php namespace Modules\Coordinacioncronograma\Entities;
   
use Illuminate\Database\Eloquent\Model;

class MovimientoCronogramaPersonasModel extends Model {

    protected $table = 'coor_tmov_cropnograma_resposables';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'id_crono_cab'=>'required',
            'id_usuario'=>'required'
		], $merge);
    } 

}