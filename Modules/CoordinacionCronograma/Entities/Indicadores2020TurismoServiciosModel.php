<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020TurismoServiciosModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmae_turismo_servicios";
}
