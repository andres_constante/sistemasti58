<?php namespace Modules\Coordinacioncronograma\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CoorCronogramaDetaActModel extends Model {

    protected $fillable = [];
    protected $table="coor_tmov_cronograma_deta";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                //'tema'=>'required|unique:coor_tmov_obras_listado'. ($id ? ",id,$id" : ''),
                'id_crono_deta'=>'required|numeric',
                'actividad'=>'required',
                'fecha_inicio'=>'required|date',
                'fecha_fin'=>'required|date'
            ], $merge);
        }

}