<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class GesIndicadorModel extends Model
{
    protected $fillable = [];
    protected $table="ges_tmae_gestion_indicador_main";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                                
                'nombre'=>'required',
                'id_direccion'=>'required'
            ], $merge);
        } 
}
