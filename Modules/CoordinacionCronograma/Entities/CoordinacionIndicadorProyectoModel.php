<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoordinacionIndicadorProyectoModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmae_indicador";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'indicador'=>'required|unique:coor_tmae_indicador'. ($id ? ",id,$id" : ''),
            ], $merge);
    }
}
