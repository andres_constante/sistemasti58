<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class MantappPublicacionesModel extends Model
{
    protected $fillable = [];
    protected $table="man_tmov_publicaciones";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                                
            'titulo'=>'required'
        ], $merge);
    } 
}
