<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorIndicadorTipoModel extends Model
{
    protected $fillable = [];
    protected $table="ind_tmae_indicador_tipo";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'tipo'=>'required|unique:ind_tmae_indicador_tipo'. ($id ? ",id,$id" : ''),
                'detalle'=>'required'
            ], $merge);
    }
}
