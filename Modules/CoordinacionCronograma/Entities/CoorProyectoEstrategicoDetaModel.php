<?php namespace Modules\Coordinacioncronograma\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CoorProyectoEstrategicoDetaModel extends Model {


        protected $fillable = [];
        protected $table="coor_tmov_proyecto_estrategico_deta";
        public static function rules ($id=0, $merge=[]) {
                return array_merge(
                [                
                    'id_tipo_proyecto'=>'required',
                    'nombre'=>'required',            
                    'estado_proyecto' => 'required'
                ], $merge);
            }

}