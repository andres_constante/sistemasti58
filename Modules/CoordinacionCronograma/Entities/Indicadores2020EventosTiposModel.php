<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020EventosTiposModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmov_eventos_tipo";
    
}
