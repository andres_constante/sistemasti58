<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020MantenimientoAreasServicioModel extends Model
{
    
    protected $table = "ind2020_tma_mantenimientos_areas_tipo_servicios";
}
