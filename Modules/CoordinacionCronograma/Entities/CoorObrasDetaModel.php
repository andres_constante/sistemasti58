<?php namespace Modules\Coordinacioncronograma\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CoorObrasDetaModel extends Model {

    protected $fillable = [];
    protected $table="coor_tmov_obras_listado_deta";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'id_tipo_obra'=>'required',
                'calle'=>'required',
                'nombre_obra'=>'required',
                'numero_proceso'=>'required',
                'estado_obra'=>'required',
                'id_contratista'=>'required',
                'fecha_inicio'=>'required',
                'fecha_final'=>'required',
                'estado'=>'required',
            ], $merge);
        }


}