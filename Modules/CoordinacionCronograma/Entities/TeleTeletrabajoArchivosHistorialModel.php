<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class TeleTeletrabajoArchivosHistorialModel extends Model
{
    protected $fillable = [];
    protected $table = 'tele_tmov_teletrabajo_archivo_deta';
}
