<?php namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CabRecomendacionContraHistoModel extends Model
{
    protected $fillable = [];
    protected $table ="coor_tmae_recomendacion_contraloria_deta";
}
