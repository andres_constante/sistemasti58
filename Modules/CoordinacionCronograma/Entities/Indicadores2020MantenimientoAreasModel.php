<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020MantenimientoAreasModel extends Model
{
    protected $fillable = [];
    protected $table = "ind2020_tmov_mantenimientos_areas";
    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'n_habitantes' => 'required',
                'n_beneficiarios' => 'required',
                'inversion' => 'required',
                'cantidad' => 'required'
            ],
            $merge
        );
    }
}
