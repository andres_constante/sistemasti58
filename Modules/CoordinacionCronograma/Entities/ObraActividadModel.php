<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class ObraActividadModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmov_obras_actividades";

    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'id_proyecto'=>'required',
                'actividad'=>'required',
                'fecha_inicio_actividad'=>'required',
                'fecha_fin_actividad'=>'required'

            ], $merge);
            
    }
    public static function rulesdir ($id=0, $merge=[]) {
        return array_merge(
            [
                'id'=>'required',
                'observacion_actividad'=>'required',
                'avance_actividad'=>'required'

            ], $merge);
            
    }

}
