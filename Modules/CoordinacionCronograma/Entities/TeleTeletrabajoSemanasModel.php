<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class TeleTeletrabajoSemanasModel extends Model
{
    protected $fillable = [];
    protected $table = 'tele_tmov_teletrabajo_semanas';
}
