<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CabCompromisoalcaldiaDetaModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmov_compromisos_deta";
}
