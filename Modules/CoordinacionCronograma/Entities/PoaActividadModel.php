<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class PoaActividadModel extends Model
{
    
    protected $table = 'coor_tmov_poa_actividad';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'id_cab_pro'=>'required',
			'financiamiento'=>'required',
			'costo_institucion'=>'required',
			'costo_prov'=>'required',
			'costo_inicial'=>'required',
			'costo_actual'=>'required',
			'diferencia'=>'required',
		], $merge);
    } 

    
}
