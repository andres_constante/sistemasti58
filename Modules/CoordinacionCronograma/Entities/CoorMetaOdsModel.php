<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorMetaOdsModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmae_meta_ods";
}
