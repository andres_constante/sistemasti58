<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020SancionesModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmov_sanciones";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                                
            'id_sancion'=>'required',
            'valor'=>'required'
        ], $merge);
    } 

}
