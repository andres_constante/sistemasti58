<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class EmergenciaSanitariaModel extends Model
{
    protected $fillable = [];
    protected $table ="emergencia_sanitaria";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'id_direccion'=>'required',
                'id_semana'=>'required',
                'archivo'=>'required|file'//|size:2000
            ], $merge);
    }
}
