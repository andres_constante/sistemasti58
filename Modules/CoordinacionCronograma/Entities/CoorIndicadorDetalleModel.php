<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorIndicadorDetalleModel extends Model
{
    protected $fillable = [];
    protected $table="ind_tmov_indicadores_deta";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                //'filtro'=>'required|unique:ind_tmov_indicadores_deta'. ($id ? ",id,$id" : ''),
                'id_parroquia'=>'required',
                'id_indicador_mae'=>'required'

            ], $merge);
    }
}
