<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class PlanesEmergenciasCoor extends Model
{
    protected $fillable = [];
    protected $table ="planes_emergencias";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'id_direccion'=>'required',
                'archivo'=>'required|file'//|size:2000
            ], $merge);
    }
}
