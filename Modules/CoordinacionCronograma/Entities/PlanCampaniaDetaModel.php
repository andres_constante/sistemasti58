<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class PlanCampaniaDetaModel extends Model
{
    
    protected $fillable = [];
    protected $table="plan_tmov_cab_plancampania_deta";
        
}
