<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorProyectoEstrategicoActividades extends Model
{
    protected $fillable = [];
    protected $table="coor_tmov_proyecto_estrategico_actividad";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                //'proyecto'=>'required|unique:poa_tmae_cab'. ($id ? ",id,$id" : ''),
                'id_proyecto'=>'required',
                'actividad'=>'required',
                'id_direccion_act'=>'required',
                'idusuario_act'=>'required',
                'fecha_inicio'=>'required',
                'fecha_fin'=>'required',
                // 'observacion'=>'required',
                // 'avance'=>'required'

            ], $merge);
            
    }
    public static function rulesdir ($id=0, $merge=[]) {
        return array_merge(
            [
                //'proyecto'=>'required|unique:poa_tmae_cab'. ($id ? ",id,$id" : ''),
                'id'=>'required',
                // 'actividad'=>'required',
                // 'id_direccion_act'=>'required',
                // 'idusuario_act'=>'required',
                // 'fecha_inicio'=>'required',
                // 'fecha_fin'=>'required',
                'observacion'=>'required',
                'avance'=>'required'

            ], $merge);
            
    }
}
