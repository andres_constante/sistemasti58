<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class GesCabIndicardorResponsableModel extends Model
{
    protected $fillable = [];
    protected $table="ges_tmov_gestion_indicador_reponsable";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                                
                'id_cab_indi'=>'required',
                'id_usuario'=>'required'
            ], $merge);
        } 


}
