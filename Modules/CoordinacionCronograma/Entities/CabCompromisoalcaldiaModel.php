<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CabCompromisoalcaldiaModel extends Model
{
    protected $fillable = [];
    protected $table = "coor_tmov_compromisos_cab";
     public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'estado_actividad'=>'required',                
                'actividad'=>'required|unique:coor_tmov_compromisos_cab'. ($id ? ",id,$id" : ''),
                'id_parroquia'=>'required',
                'id_direccion'=>'required',
                'actividad'=>'required',
                'fecha_inicio'=>'required',
                'fecha_fin'=>'required'
            ], $merge);
        }  
}
