<?php namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class TipoActividadPoaModel extends Model
{
    protected $table = 'coor_tmae_tipo_acti_poa';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'tipo_actividad'=>'required'
		], $merge);
    } 
}
