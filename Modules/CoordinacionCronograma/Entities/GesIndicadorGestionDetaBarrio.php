<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class GesIndicadorGestionDetaBarrio extends Model
{
    protected $fillable = [];
    protected $table="ges_tmov_gestion_indicador_parroquia_deta";
}
