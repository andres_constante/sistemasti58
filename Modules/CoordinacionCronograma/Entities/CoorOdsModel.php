<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class CoorOdsModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmae_ods";
}
