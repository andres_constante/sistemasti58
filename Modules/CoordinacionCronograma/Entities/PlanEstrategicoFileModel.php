<?php

namespace Modules\Coordinacioncronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class PlanEstrategicoFileModel extends Model
{
    protected $fillable = [];
    protected $table ="plan_estra_archivos";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'id_direccion'=>'required|unique:plan_estra_archivos'. ($id ? ",id,$id" : ''),
                'archivo'=>'required|file'//|size:2000
            ], $merge);
    }
}
