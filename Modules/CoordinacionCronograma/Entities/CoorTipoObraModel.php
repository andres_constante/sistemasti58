<?php namespace Modules\Coordinacioncronograma\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CoorTipoObraModel extends Model {

    protected $fillable = [];
    protected $table="coor_tmae_tipo_obra";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'tipo'=>'required|unique:coor_tmae_tipo_obra'. ($id ? ",id,$id" : '')
            ], $merge);
        }

}