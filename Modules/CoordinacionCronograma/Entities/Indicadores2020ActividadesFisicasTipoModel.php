<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020ActividadesFisicasTipoModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmov_actividades_fisicas_tipo";
}
