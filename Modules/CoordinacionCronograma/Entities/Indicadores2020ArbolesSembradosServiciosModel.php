<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020ArbolesSembradosServiciosModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmae_arboles_servicios";
}
