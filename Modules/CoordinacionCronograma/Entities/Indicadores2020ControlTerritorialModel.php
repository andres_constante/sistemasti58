<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class Indicadores2020ControlTerritorialModel extends Model
{
    protected $fillable = [];
    protected $table="ind2020_tmov_control_territorial";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                                
            'fecha_ingreso'=>'required',
            'id_inspector_tecnico'=>'required',
            // 'id_indicador_cab'=>'required',
            'numero_acta'=>'required'
        ], $merge);
    } 
    
}
