<?php namespace Modules\Coordinacioncronograma\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CoorTipoProyectoModel extends Model {

    protected $fillable = [];
    protected $table="coor_tmae_tipo_proyecto";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'tipo'=>'required|unique:coor_tmae_tipo_proyecto'. ($id ? ",id,$id" : '')
                
            ], $merge);
        }
}