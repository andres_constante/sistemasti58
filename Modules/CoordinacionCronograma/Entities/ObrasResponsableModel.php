<?php

namespace Modules\CoordinacionCronograma\Entities;

use Illuminate\Database\Eloquent\Model;

class ObrasResponsableModel extends Model
{
    protected $fillable = [];
    protected $table="coor_tmov_obras_actividades_responsables";

}
