<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionCabModel;
use Modules\CoordinacionCronograma\Entities\CoordinacionDetaDireccionModel;
use stdClass;
use DB;
use Auth;
use function GuzzleHttp\json_decode;
use Session;
class CoordinacionCabController extends Controller
{
    var $configuraciongeneral = array("Coordinaciones", "coordinacioncronograma/coordinaciones", "index", 6 => "coordinacioncronograma/coordinacionesajax", 7 => "coordinaciones");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Nombre Coordinación","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Responsable","Nombre":"id_responsable","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Direcciones","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "nombre" => "nombre: {
                            required: true
                        }",
        "id_responsable" => "id_responsable: {
                            required: true
                        }"
    );
    public function getdireccionescoordinacion()
    {
        $id=Input::get("id");
        $tabla=CoordinacionDetaDireccionModel::join("tmae_direcciones as a","a.id","=","coor_tmov_coordinacion_direccion_deta.id_direccion")
            ->where("coor_tmov_coordinacion_direccion_deta.id_coor_cab",$id)
            ->select("a.direccion","a.id")
            ->orderby("a.direccion")
            ->pluck("direccion","id")
            ->all();
        return $tabla;
    }
    public function verpermisos($objetos = array(), $tipo = "index")
    {
        //show($objetos);
        $tabla = CoordinacionCabModel::where("coor_tmae_coordinacion_main.estado", "ACT")
            ->join("users as a", "a.id", "coor_tmae_coordinacion_main.id_responsable")
            ->select("coor_tmae_coordinacion_main.*", "a.name as responsable",
                DB::raw("(select GROUP_CONCAT(x.direccion SEPARATOR ', ') AS id_direccion 
	from coor_tmov_coordinacion_direccion_deta AS y 
		inner join tmae_direcciones as x on x.id = y.id_direccion where y.id_coor_cab = coor_tmae_coordinacion_main.id limit 1) as id_direccion"));
        return array($objetos, $tabla);
    }
    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) {
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
        } else{
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        }
        return $object;
    }
    public function coordinacionesajax(Request $request)
    {
        $objetos = json_decode($this->objetos);
        $objetos[1]->Nombre = "responsable";
        $tabla = $this->verpermisos($objetos);
        $objetos = $tabla[0];

        //show($objetos);
        $tabla = $tabla[1];
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('coor_tmae_coordinacion_main.id', 'LIKE', "%{$search}%")
                    ->orWhere('coor_tmae_coordinacion_main.nombre', 'LIKE', "%{$search}%")
                    ->orWhere('a.name', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {
                $permisos= $this->perfil();

                $acciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;' .
                    link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o'));

                if($permisos->eliminar=='SI'){
                    $acciones.='&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                }

                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $nestedData['acciones'] = $acciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = json_decode($this->objetos);
        $objetos[1]->Nombre = "responsable";
        //$objetos = $this->verpermisos($objetos);
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si"
        ]);
        //return view('coordinacioncronograma::index');
    }
    public function guardar($id)
    {
        $input = Input::all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];

        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new CoordinacionCabModel;
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro de Datos";
        } else {
            $ruta .= "/$id/edit";
            $guardar = CoordinacionCabModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Actualización de Datos";
        }

        $input = Input::all();
        $arrapas = array();

        $validator = Validator::make($input, CoordinacionCabModel::rules($id));

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            DB::beginTransaction();
            try {
                foreach ($input as $key => $value) {

                    if ($key != "_method" && $key != "_token" && $key != "id_direccion") {
                        $guardar->$key = $value;
                    }
                }
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                $guardar->save();
                $id=$guardar->id;
                //show($id);
                /**/
                if(Input::has("id_direccion"))
                {
                    $direcciones=Input::get("id_direccion");
                    //show($direcciones);
                    CoordinacionDetaDireccionModel::where("id_coor_cab",$id)->delete();
                    foreach ($direcciones as $keydir => $valuedir)
                    {
                        //Aqui
                        $guardares=new CoordinacionDetaDireccionModel;
                        $guardares->id_coor_cab = $id;
                        $guardares->id_direccion=$valuedir;
                        $guardares->id_usuario=Auth::user()->id;
                        $guardares->ip=\Request::getClientIp();
                        $guardares->save();
                    }
                }
                Auditoria($msgauditoria . " - ID: " . $id . "-" . Input::get($guardar->nombre));
                DB::commit();
            } //Fin Try
            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage() . " - " . $e->getLine();
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }
    public function formularioscrear($id)
    {
        /*QUITAR CAMPOS*/
        $objetos=json_decode($this->objetos);
        $tbcom=UsuariosModel::where("estado","ACT")->select(DB::raw("concat(name,' - ',cargo) as name"),"id")->orderby("name")->pluck("name", "id")->all();
        $direcciones = direccionesModel::where("estado", "ACT")->orderby("direccion")->pluck("direccion", "id")->all();
        $objetos[1]->Valor=$this->escoja + $tbcom;
        $objetos[2]->Valor=$direcciones;
        $objetos = $this->verpermisos($objetos, "crear");
        $datos = array();
        if ($id == "") {
            $this->configuraciongeneral[2] = "crear";
            $datos=[
                "objetos" => $objetos[0],
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            ];
        }else{
            $this->configuraciongeneral[2] = "editar";
            $id = intval(Crypt::decrypt($id));
            $tabla = $objetos[1]->where("coor_tmae_coordinacion_main.id", $id)->first();
            $direcAntes=CoordinacionDetaDireccionModel::where("id_coor_cab",$id)->pluck("id_direccion","id_direccion")->all();
            $objetos[0][2]->ValorAnterior=$direcAntes;
            $datos=[
                "tabla"=>$tabla,
                "objetos" => $objetos[0],
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            ];
        }
        return view('vistas.create', $datos);
    }
        /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return $this->formularioscrear("");
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("coor_tmae_coordinacion_main.id", $id)->first();

        $this->configuraciongeneral[3] = "5"; //Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "si";
        $objetos = $objetos[0];
        $objetos[1]->Nombre = "responsable";
        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
        //return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
        //return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $tabla = CoordinacionCabModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
