<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Form;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Modules\Coordinacioncronograma\Entities\CoorIndicadorDetalleModel;
use Modules\Coordinacioncronograma\Entities\CoorIndicadorTipoFiltroModel;
use Modules\Coordinacioncronograma\Entities\CoorIndicadorTipoModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
class CoorIndicadorDetaController extends Controller
{
    var $configuraciongeneral = array("Detalles de Indicadores", "coordinacioncronograma/indicadoresdetalles", "index", 6 => "coordinacioncronograma/indicadoresdetallesajax", 7 => "indicadoresdetalles");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[        
        {"Tipo":"select","Descripcion":"Parroquia","Nombre":"id_parroquia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Indicador","Nombre":"id_indicador_mae","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },        
        {"Tipo":"divresul","Descripcion":"Valores","Nombre":"divresul","Clase":"Null","Valor":"","ValorAnterior" :"Null" }
                  ]';
//https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "id_indicador_mae" => "id_indicador_mae: {
                            required: true
                        }",
        "id_parroquia" => "id_parroquia: {
                            required: true
                        }"
    );
    public function getcamposdatos()
    {
        //return date("H:i:s");
        $id=intval(Input::get("id"));
        $idparroquia=intval(Input::get("id_parroquia"));
        $tabla=CoorIndicadorTipoFiltroModel::where("id_indicador_mae",$id)->get();
        $filtro=CoorIndicadorDetalleModel::where("id_indicador_mae",$id)
            ->where("id_parroquia",$idparroquia)
            ->where("estado","ACT")
            ->first();
        //show($filtro);
        $bus=array();
        if($filtro)
            $bus=json_decode($filtro->json,true);
        //show($bus);
        foreach ($tabla as $i => $campos)
        {
            $valor=0;
            if(count($bus))
                $valor=$bus[$campos->id_campotxt];
            echo Form::label($campos->id_campotxt,$campos->filtro.':',array("id"=>"label_".$campos->id_campotxt,"class"=>"col-lg-3 control-label"));
            echo Form::text($campos->id_campotxt, $valor , array('class' => 'form-control solonumeros','placeholder'=>'Ingrese total de '.$campos->filtro));

        }

    }
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function verpermisos($objetos = array(), $tipo = "index")
    {
        //show($objetos);
        $tabla = CoorIndicadorDetalleModel::where("ind_tmov_indicadores_deta.estado", "ACT")
            ->join("ind_tmae_indicador_tipo as a","a.id","=","ind_tmov_indicadores_deta.id_indicador_mae")
            ->join("parroquia as b","b.id","=","ind_tmov_indicadores_deta.id_parroquia")
            ->select("ind_tmov_indicadores_deta.*","a.detalle","b.parroquia")
            ->orderby("ind_tmov_indicadores_deta.id","desc");
        return array($objetos, $tabla);
    }

    public function indicadoresdetallesajax(Request $request)
    {
        $objetos=json_decode($this->objetos);
        $objetos[0]->Nombre="parroquia";
        $objetos[1]->Nombre="detalle";
        $objetos[2]->Nombre="json";
        $tabla = $this->verpermisos($objetos);
        $objetos = $tabla[0];

        //show($objetos);
        $tabla = $tabla[1];
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('ind_tmov_indicadores_deta.id', 'LIKE', "%{$search}%")
                    ->orWhere('ind_tmov_indicadores_deta.json', 'LIKE', "%{$search}%")
                    ->orWhere('b.parroquia', 'LIKE', "%{$search}%")
                    ->orWhere('a.detalle', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();

        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {


                $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;' .
                    link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o')) . '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;

            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos= json_decode($this->objetos);
        //$objetos[0]->Nombre="detalle";
        //$objetos = $this->verpermisos($objetos);
        $objetos[0]->Nombre="parroquia";
        $objetos[1]->Nombre="detalle";
        $objetos[2]->Nombre="json";
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si"
        ]);
        //return view('coordinacioncronograma::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        /*QUITAR CAMPOS*/
        $objetos=json_decode($this->objetos);
        $tbcom=CoorIndicadorTipoModel::where("estado","ACT")->pluck("detalle", "id")->all();
        $parroquia=parroquiaModel::where("estado","ACT")
            ->where("parroquia","not like","%ESPECIFICADO%")
            ->pluck("parroquia", "id")->all();
        $objetos[0]->Valor=$this->escoja + $parroquia;
        $objetos[1]->Valor=$this->escoja + $tbcom;
        $objetos = $this->verpermisos($objetos, "crear");
        $this->configuraciongeneral[2] = "crear";
        return view('vistas.create', [
            "objetos" => $objetos[0],
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs
        ]);
    }
    public function guardar($id)
    {
        $input = Input::all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];
        $veri= CoorIndicadorDetalleModel::where("id_parroquia",Input::get("id_parroquia"))
            ->where("id_indicador_mae",Input::get("id_indicador_mae"))
            ->first();
        if($veri)
            $id=$veri->id;
        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new CoorIndicadorDetalleModel;
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro Variable de Configuración";
        } else {
            //$ruta .= "/$id/edit";
            $ruta .= "/".Crypt::encrypt($id)."/edit";
            $guardar = CoorIndicadorDetalleModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Edición Variable de Configuración";
        }
        $arrapas = array();

        $validator = Validator::make($input, CoorIndicadorDetalleModel::rules($id));

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            $indatos=$input;
            unset($indatos["_token"]);
            unset($indatos["_method"]);
            unset($indatos["id_parroquia"]);
            unset($indatos["id_indicador_mae"]);
            foreach ($input as $key => $value) {
                if ($key == "id_parroquia" || $key == "id_indicador_mae") {
                    $guardar->$key = $value;
                }
            }
            $guardar->json=json_encode($indatos);
            $guardar->id_usuario = Auth::user()->id;
            $guardar->ip = \Request::getClientIp();
            $guardar->pc = \Request::getHost();
            $guardar->save();
            /**/

            Auditoria($msgauditoria . " - ID: " . $id );
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("ind_tmov_indicador_tipo_filtro.id", $id)->first();

        $this->configuraciongeneral[3] = "5";//Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "si";
        $objetos=$objetos[0];
        $objetos[0]->Nombre="detalle";
        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos), "editar");
        $tabla = $objetos[1]->where("ind_tmov_indicadores_deta.id", $id)->first();

        $tbcom=CoorIndicadorTipoModel::where("estado","ACT")->pluck("detalle", "id")->all();
        $parroquia=parroquiaModel::where("estado","ACT")
            ->where("parroquia","not like","%ESPECIFICADO%")
            ->pluck("parroquia", "id")->all();
        $objetos[0][0]->Valor=$this->escoja + $parroquia;
        $objetos[0][1]->Valor=$this->escoja + $tbcom;
        //show($tabla);
        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        $this->configuraciongeneral[2] = "editar";
        return view('vistas.create', [
            "objetos" => $objetos[0],
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
            "tabla" => $tabla/*,
            "dicecionesImagenes" => $dicecionesImagenes,
            "dicecionesDocumentos" => $dicecionesDocumentos*/
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $tabla = CoorIndicadorTipoFiltroModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
