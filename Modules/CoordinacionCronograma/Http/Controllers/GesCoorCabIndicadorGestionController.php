<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\ArchivosModel;
use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\ComunicacionAlcaldia\Entities\CantonModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteObjetivosModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionCabModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionDetaDireccionModel;
use Modules\CoordinacionCronograma\Entities\GesCabIndicadorGestionDetaModel;
use Modules\CoordinacionCronograma\Entities\GesCabIndicadorGestionModel;
use Modules\CoordinacionCronograma\Entities\GesCabIndicardorResponsableModel;
use Modules\CoordinacionCronograma\Entities\GesIndicadorGestionDetaBarrio;
use Modules\CoordinacionCronograma\Entities\GesSubIndicadorModel;


use stdClass;


class GesCoorCabIndicadorGestionController extends Controller
{

    var $configuraciongeneral = array("Indicadores de Gestión", "coordinacioncronograma/indicadoresgestion", "index", 6 => "coordinacioncronograma/indicadoresgestionajax", 7 => "indicadoresgestion");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Proyecto","Nombre":"proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Responsable(s)","Nombre":"idusuario","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Indicadores","Nombre":"json_indicadores","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" }

                  ]';

    /*COmponente Sostenibilidad*/
    var $objetoscomponentes = '
    [
        {"Tipo":"select","Descripcion":"Componente","Nombre":"componente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
        {"Tipo":"select","Descripcion":"Objetivo","Nombre":"objetivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Programa","Nombre":"programa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
    ]
';

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['getBarrios', 'getCantones']]);
    }
    var $validarjs = array(
        "proyecto" => "proyecto: {
                            required: true
                        }",
        "actividad" => "actividad: {
                            required: true
                        }",
        "id_objetivo_componente" => "id_objetivo_componente: {
            required: true
        }"
    );
    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'NO';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        }
        //show($object);
        return $object;
    }

    public function verpermisos($objetos = array(), $tipo = "index")
    {
        // show($objetos);


        if ($tipo == "index") {
            // dd($objetos);
            unset($objetos[4]);
            $tabla = GesCabIndicadorGestionModel::join("tmae_direcciones as a", "a.id", "=", "ges_tmov_gestion_indicador.id_direccion")
                ->select("ges_tmov_gestion_indicador.*", "a.direccion as id_direccion", DB::raw("(select GROUP_CONCAT(x.name SEPARATOR ', ') from ges_tmov_gestion_indicador_reponsable AS y inner join users as x on x.id = y.id_usuario where y.id_cab_indi = ges_tmov_gestion_indicador.id limit 1) as idusuario"))
                ->where("ges_tmov_gestion_indicador.estado", "ACT")
                ->groupby("ges_tmov_gestion_indicador.id");
            unset($objetos[0]);
            // dd($objetos);
            unset($objetos[1]);
        } else {
            $tabla = GesCabIndicadorGestionModel::where("estado", "ACT");
        }


        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        // $seleccionar_direccion = direccionesModel::where("estado", "ACT");
        if ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 2) {
            // $seleccionar_direccion = $seleccionar_direccion->where("id", Auth::user()->id_direccion);
            $tabla = $tabla->where("ges_tmov_gestion_indicador.id_direccion", Auth::user()->id_direccion);
        }
        // dd($objetos);
        // dd($tabla->get());
        return array($objetos, $tabla);
    }
    public function indicadoresgestionajax(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        $objetos = $tabla[0];
        //unset($objetos[4]);

        $tabla = $tabla[1];
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            if ($value->Nombre == 'id_direccion') {
                $columns[] = 'ges_tmov_gestion_indicador.id_direccion';
            } else {
                $columns[] = $value->Nombre;
            }
        }

        $columns[] = 'acciones';

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        // dd($tabla);
        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                // ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('ges_tmov_gestion_indicador.id', 'LIKE', "%{$search}%")
                    ->orWhere('a.direccion', 'LIKE', "%{$search}%")


                    // ->orWhere('detalle', 'LIKE', "%{$search}%")
                ;
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {

                $permisos = $this->perfil();

                //=============================================================
                $show = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                //'&nbsp;&nbsp;' .
                $edit = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o'));
                //. '&nbsp;&nbsp;
                $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                        <div style="display: none;">
                        <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                            <input name="_token" type="hidden" value="' . csrf_token() . '">
                            <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                        </form>
                        </div>';
                if ($permisos->ver == 'NO')
                    $show = "";
                if ($permisos->editar == 'NO')
                    $edit = "";
                if ($permisos->eliminar == 'NO')
                    $dele = "";
                $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                //=============================================================
                $nestedData['id'] = $post->id;

                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $valorc = $post->$campo;
                    $nestedData["$value->Nombre"] = "$valorc";
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = $this->verpermisos(json_decode($this->objetos));


        // dd($objetos);
        return view('vistas.index', [
            "objetos" => $objetos[0],
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si"
        ]);
    }





    public function formularioscrear($id)
    {


        $history = null;
        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 2) {
            $seleccionar_direccion = direccionesModel::where([["estado", "ACT"], ['id', Auth::user()->id_direccion]])->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        } else {
            $seleccionar_direccion = direccionesModel::where("estado", "ACT")->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        }
        $objetos[2]->Valor = $this->escoja + $seleccionar_direccion;
        $objetos[3]->Valor = [];
        $obaddjs = array();
        $objetosmain = $this->verpermisos($objetos, "crear");
        /*Componentes*/
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $com = CoorComponenteModel::where("estado", "ACT")->select(DB::raw("concat(nombre,' - ',detalle) as componente"), "id")
            ->orderBy('nombre', 'ASC')->pluck("componente", "id")->all();
        $objetoscomponentes[0]->Valor = $this->escoja + $com;
        $objetoscomponentes[1]->Valor = $this->escoja;
        $objetoscomponentes[2]->Valor = $this->escoja;
        $objetos = array_merge($objetoscomponentes, $objetosmain[0]);

        /*====================================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"id_coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        if ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 2) {
            $cmb = CoordinacionCabModel::join("users as a", "a.id", "=", "coor_tmae_coordinacion_main.id_responsable")
                ->join("coor_tmov_coordinacion_direccion_deta as d", "d.id_coor_cab", "=", "coor_tmae_coordinacion_main.id")
                //->select("coor_tmae_coordinacion_main.*","a.name as responsable")
                ->select("coor_tmae_coordinacion_main.id", DB::raw("concat(coor_tmae_coordinacion_main.nombre,': ',a.name) as nombre"))
                ->where([
                    ["coor_tmae_coordinacion_main.estado", "ACT"],
                    ['d.id_direccion', Auth::user()->id_direccion]
                ])
                ->pluck("nombre", "id")->all();
            $coordinacion->Valor = $cmb;
        } else {
            $cmb = CoordinacionCabModel::join("users as a", "a.id", "=", "coor_tmae_coordinacion_main.id_responsable")
                //->select("coor_tmae_coordinacion_main.*","a.name as responsable")
                ->select("coor_tmae_coordinacion_main.id", DB::raw("concat(coor_tmae_coordinacion_main.nombre,': ',a.name) as nombre"))
                ->where("coor_tmae_coordinacion_main.estado", "ACT")->pluck("nombre", "id")->all();
            $coordinacion->Valor = $this->escoja + $cmb;
        }


        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 5);
        // show($objetos);
        $js = '{"Tipo":"htmlplantilla","Descripcion":"Indicadores","Nombre":"js_indicadores_plantilla","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $js = json_decode($js);

        $html = '<script>  
        
                var n_filas=0;
                $(document).ready(function () {
                   
        
                    $("#label_js_indicadores_plantilla").hide();
                    $("#div-js_indicadores_plantilla").removeClass("col-lg-8");
                    $("#div-js_indicadores_plantilla").addClass("col-lg-12");
                    $("#id_direccion").change(function (e) { 
                            var id=this.value;
                            var ruta= "' . URL::to('coordinacioncronograma/getIndicadoresGestion') . '?id="+id+"&t=4";
                            $.get(ruta, function(data) {
                                $("#indicadores_gestion").empty();
                                $("#tabla_indicadores tbody").empty();
                                $.each(data, function(key, element) {
                                    $("#indicadores_gestion").append("<option value=\'" + element.id + "\'>" + element.nombre+"</option>");
                                });           
                                $("#indicadores_gestion").trigger("chosen:updated");
                            });
                    });
                    $("#indicadores_gestion").change(function (e) { 
                        crearTabla();
                    });
                    $("#mes_select").change(function (e) { 
                        crearTabla();
                    });





                    $(".fecha_input").datepicker({
                        todayBtn: "linked",
                        keyboardNavigation: false,
                        forceParse: false,
                        calendarWeeks: false, //True para mostrar Numero de semanas
                        autoclose: true,
                        format: "yyyy-mm-dd",
                        language: "es",
                        orientation: "auto bottom"
                    });
                    $("#mes_select").change(function(e) {
                        llenarfecha();
                    });
                });

                function crearTabla(){
                    var id=$("#indicadores_gestion").val();
                    var mes_fecha=$("#mes_select").val();
                    var ruta= "' . URL::to('coordinacioncronograma/getIndicadoresGestion') . '?id="+id+"&t=1&id_gestion=' . $id . '&mes_fecha="+mes_fecha+"";
                    $("#tabla_indicadores tbody").empty();
                        $.get(ruta, function(data) {
                            if($.isEmptyObject(data)==false){
                                $("#indicadores_gestion_parroquias").empty();
                                $("#indicadores_gestion_barrios").empty();
                                $.each(data, function(key, element) {
                                    console.log(element);
                                    if(element.tipo==78){
                                        console.log();
                                        $("#indicadores_gestion_parroquias").append("<option value=\'" + element.id + "\'>" + element.parroquia+"</option>");
                                    }else{
                                        if(\'' . $id . '\'==""){
                                        
                                            $("#tabla_indicadores").append(\'<tr id="fila_\'+n_filas+\'" ><td>\'+element.nombre+ \'<br></td><td><b>\'+element.nombresub+ \'</b></td><td  class="hidden"><input type="number" onkeyup="armar_json(\'+n_filas+\')" class="valores" id="linea_\'+element.id+\'"></td> <td><input hidden  id="indicador_id_\'+element.id+\'" value="\'+element.id+\'"><input type="number"  onkeyup="armar_json(\'+n_filas+\')" class="valores" id="indicador_\'+element.id+\'"><input type="number" hidden id="indicador_estado_\'+element.id+\'" value="0"></td><td><input type="text"  onkeyup="armar_json(\'+n_filas+\')" class="valores "  id="medida_\'+element.id+\'"></td><td><input type="number"  onkeyup="armar_json(\'+n_filas+\')"  class="valores" id="meta_\'+element.id+\'"></td><td><input type="number"  onkeyup="armar_json(\'+n_filas+\')" class="valores "  id="benficiarios_\'+element.id+\'"></td><td><input type="date"  onkeyup="armar_json(\'+n_filas+\')" class="valores date " disabled value="' . date('Y-m-d') . '"  id="fecha_\'+element.id+\'"></td><td><textarea type="text"  onkeyup="armar_json(\'+n_filas+\')" class="valores "  value=""  id="obs_\'+element.id+\'"></textarea></td><td><i  onclick="borrar(\'+n_filas+\')" class="fa fa-times-circle hidden" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
                                        }else{
                                            
                                            console.log(\'' . $id . '\');
                                            $("#tabla_indicadores").append(\'<tr><td>\'+element.nombre+ \'</td><td>\'+element.nombresub+ \'</td><td class="hidden"><input type="number"  value="\'+element.linea_base+ \'"  onkeyup="armar_json(\'+n_filas+\')" class="valores" id="linea_\'+element.id+\'"></td><td><input hidden value="\'+element.id_deta_ges+\'"><input type="number" onkeyup="armar_json(\'+n_filas+\')" value="\'+Math.round(element.valor)+\'"  ><input type="number" hidden value="0"></td><td><input type="text"  onkeyup="armar_json(\'+n_filas+\')" class="valores " value="\'+element.medida+ \'"  id="medida_\'+element.id+\'"></td><td><input type="number" onkeyup="armar_json(\'+n_filas+\')" value="\'+element.meta_planificada+ \'" class="valores"  id="meta_\'+element.id+\'"></td><td><input type="number"  onkeyup="armar_json(\'+n_filas+\')" class="valores " id="benficiarios_\'+element.id+\'"  value="\'+element.mes+ \'"> </td><td><input type="date" value="' . date('Y-m-d') . '"  disabled  onkeyup="armar_json(\'+n_filas+\')" class="valores date"  id="fecha_\'+element.id+\'"></td><td><textarea type="text"  onkeyup="armar_json(\'+n_filas+\')" class="valores "  value="\'+element.observaciones+ \'"  id="obs_\'+element.id+\'">\'+element.observaciones+ \'</textarea></td><td></td></tr>\');
                                        }
                                        n_filas++;
                                    }
                                    
                                }); 
                                mostrarBarrios();
                                llenarfecha();
                                $("#indicadores_gestion_parroquias").trigger("chosen:updated");
                             

                            }else{
                                toastr["error"]("No se encontró ningun indicador");
                            }
                        });
                }
                
                function borrar(id){
                    console.log(id);
                    $("#fila_"+id).remove();
                    armar_json(id);
                }
                function armar_json(id){
                    var all_says = [];
                    $("#tabla_indicadores tbody tr").each(function (index) { 
                        var id_indicador=0;
                        var valor_indicador=0;
                        var estado=0;
                        var linea_base=0;
                        var meta=0;
                        var fecha="";
                        var medida="";
                        var mes_fecha="";
                        var obs="";
                        $(this).children("td").each(function (index2) {
                    
                            if(index2==2){
                                $(this).children("input").each(function (index3) {
                                    if(index3==0){
                                        linea_base=$(this).val();
                                    }
                                });
                                $(this).children("select").each(function (index3) {
                                    if(index3==0){
                                        linea_base=$(this).val();
                                    }
                                });
                            }
                            if(index2==3){
                                console.log(this);
                                $(this).children("input").each(function (index3) {
                                    if(index3==0){
                                        id_indicador=$(this).val();
                                    }
                                    if(index3==1){
                                        valor_indicador=$(this).val();
                                    }
                                    
                                    if(index3==2){
                                        estado=$(this).val();
                                    
                                    }
                                });
                            }
                            if(index2==4){
                                console.log(this);
                                $(this).children("input").each(function (index3) {
                                    if(index3==0){
                                        medida=$(this).val();
                                    }
                                    
                                });
                            }
                            if(index2==5){
                                
                                $(this).children("input").each(function (index3) {
                                    if(index3==0){
                                        meta=$(this).val();
                                    }
                                });
                            }
                            if(index2==6){
                                $(this).children("input").each(function (index3) {
                                    if(index3==0){
                                        mes=$(this).val();
                                    }
                                    
                                });
                            }
                            
                            if(index2==7){
                                $(this).children("input").each(function (index3) {
                                    if(index3==0){
                                        mes_fecha=$(this).val();
                                    }
                                    
                                });
                            }
                            if(index2==8){
                                $(this).children("textarea").each(function (index3) {
                                    if(index3==0){
                                        obs=$(this).val();
                                    }
                                    
                                });
                            }
                        });
                        var obj = {
                            id: id_indicador,
                            valor: valor_indicador,
                            estado_indicador: estado,
                            linea_base: linea_base,
                            meta: meta,
                            mes: mes,
                            mes_fecha: mes_fecha,
                            medida: medida,
                            obs: obs,
                          };
                        
                            all_says.push(obj);
                            
                            console.log(obj);
                    
                    });
                    $("#json_indicadores").val(JSON.stringify(all_says));
                }

                function llenarfecha(){
                    // var id = document.getElementsByClassName("date");
                    var fecha=$("#mes_select").val();
                    $(".date").each(function() {
                        console.log(this);
                        $(this).val(fecha);
                    });
                }
                </script>
                <span>Indicador</span>
                <select id="indicadores_gestion" class="form-control chosen-select"></select>
                 <span class="hidden">Parroquia</span><select id="indicadores_gestion_parroquias" class="form-control hidden"></select>
               <span class="hidden">Barrio</span><select id="indicadores_gestion_barrios" class="form-control hidden"></select>
               <span>Mes</span>
               <select id="mes_select" class="form-control chosen-select">
               <option value="2020-01-01">Enero</option>
               <option value="2020-02-01">Febrero</option>
               <option value="2020-03-01">Marzo</option>
               <option value="2020-04-01">Abril</option>
               <option value="2020-05-01">Mayo</option>
               <option value="2020-06-01">Junio</option>
               <option value="2020-07-01">Julio</option>
               <option value="2020-08-01">Agosto</option>
               <option value="2020-09-01">Septiembre</option>
               <option value="2020-10-01">Octubre</option>
               <option value="2020-11-01">Noviembre</option>
               <option value="2020-12-01">Diciembre</option>
               </select>
                 <button class="hidden" id="agregar_barrio" type="button">ADD</button>
                <div>
                <br>
                
                <table class="table-striped table-responsive" id="tabla_indicadores">
                <thead>
                    <tr>
                    <th scope="col">Indicador</th>
                    <th scope="col">Parroquia/Detalle</th>
                    <th scope="col" class="hidden">Barrio</th>
                    <th scope="col">Número</th>
                    <th scope="col">Unidad de Medida</th>
                    <th scope="col">Inversión</th>
                    <th scope="col">Beneficiarios</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Observacion</th>
                    <th scope="col" class="hidden">Acción</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
                <span><b>Observación</b></span>
                <textarea class="form-control hidden" id="observacion" name="observacion"></textarea>
                </div>';


        $js->Valor = $html;
        $objetos = insertarinarray($objetos, $js, 8);
        // show($objetos);


        /*====================================================*/
        if ($id != "") {

            $this->configuraciongeneral[2] = "editar";
            $this->configuraciongeneral[3] = "11"; //Tipo de Referencia de Archivo
            $this->configuraciongeneral[4] = "si";
            $id = Crypt::decrypt($id);
            $tabla = GesCabIndicadorGestionModel::find($id);
            $objetos[8]->Valor = $objetos[8]->Valor . '
            <script>
            $(document).ready(function () {
                if(' . date('Y', strtotime($tabla->created_at)) . '==2020){
                    $("#mes_select").empty()
                    $("#mes_select").append($("<option>", {value:"2020-01-01", text:"Enero"}));
                    $("#mes_select").append($("<option>", {value:"2020-02-01", text:"Febrero"}));
                    $("#mes_select").append($("<option>", {value:"2020-03-01", text:"Marzo"}));
                    $("#mes_select").append($("<option>", {value:"2020-04-01", text:"Abril"}));
                    $("#mes_select").append($("<option>", {value:"2020-05-01", text:"Mayo"}));
                    $("#mes_select").append($("<option>", {value:"2020-06-01", text:"Junio"}));
                    $("#mes_select").append($("<option>", {value:"2020-07-01", text:"Julio"}));
                    $("#mes_select").append($("<option>", {value:"2020-08-01", text:"Agosto"}));
                    $("#mes_select").append($("<option>", {value:"2020-09-01", text:"Septiembre"}));
                    $("#mes_select").append($("<option>", {value:"2020-10-01", text:"Octubre"}));
                    $("#mes_select").append($("<option>", {value:"2020-11-01", text:"Noviembre"}));
                    $("#mes_select").append($("<option>", {value:"2020-12-01", text:"Diciembre"}));

                }else{
                  
                        $("#mes_select").empty()
                        $("#mes_select").append($("<option>", {value:"2019-01-01", text:"Enero"}));
                        $("#mes_select").append($("<option>", {value:"2019-02-01", text:"Febrero"}));
                        $("#mes_select").append($("<option>", {value:"2019-03-01", text:"Marzo"}));
                        $("#mes_select").append($("<option>", {value:"2019-04-01", text:"Abril"}));
                        $("#mes_select").append($("<option>", {value:"2019-05-01", text:"Mayo"}));
                        $("#mes_select").append($("<option>", {value:"2019-06-01", text:"Junio"}));
                        $("#mes_select").append($("<option>", {value:"2019-07-01", text:"Julio"}));
                        $("#mes_select").append($("<option>", {value:"2019-08-01", text:"Agosto"}));
                        $("#mes_select").append($("<option>", {value:"2019-09-01", text:"Septiembre"}));
                        $("#mes_select").append($("<option>", {value:"2019-10-01", text:"Octubre"}));
                        $("#mes_select").append($("<option>", {value:"2019-11-01", text:"Noviembre"}));
                        $("#mes_select").append($("<option>", {value:"2019-12-01", text:"Diciembre"}));
    
                  
                }
    
            });</script>';


            // show($tabla);
            //$objetos = $this->verpermisos($objetos, "editar");
            //$objetos=$objetos[0];
            $valanteriorobj = CoorComponenteObjetivosModel::find($tabla->id_objetivo_componente);
            if ($valanteriorobj) {
                //$valanteriorpro = clone $valanteriorobj;
                $valanteriorpro = $valanteriorobj;
                $valanteriorcom = CoorComponenteModel::find($valanteriorobj->id_componente);
                $objetos[0]->ValorAnterior = array($valanteriorcom->id => $valanteriorcom->id);
                $objetos[1]->Valor = array($valanteriorobj->id => $valanteriorobj->objetivo);
                $objetos[2]->Valor = array($valanteriorpro->id => $valanteriorpro->programa);
            }
            /*====================================*/
            /*====================================*/
            //COORDINACION
            //Valor Anterior
            $ante = CoordinacionDetaDireccionModel::where("id_direccion", $tabla->id_direccion)->first();
            if ($ante) {
                $ante = array($ante->id_coor_cab => $ante->id_coor_cab);
                $objetos[5]->ValorAnterior = $ante;
            }

            /*Responsables*/
            $valorAnteriorpersonas = GesCabIndicardorResponsableModel::join("users as a", "a.id", "=", "ges_tmov_gestion_indicador_reponsable.id_usuario")
                ->where("ges_tmov_gestion_indicador_reponsable.id_cab_indi", $id)
                ->select("a.*");
            // show($valorAnteriorpersonas);
            if (array_key_exists(7, $objetos)) {
                $objetos[7]->Valor = $valorAnteriorpersonas->pluck("name", "id")->all();
                $objetos[7]->ValorAnterior = $valorAnteriorpersonas->pluck("id", "id")->all();
            }
            $objetos = $this->asignaroles($objetos, $this->perfil(), $tabla);
            // dd($objetos);

            $objetos[8]->Valor = $objetos[8]->Valor . '  <script>
            $(document).ready(function () {
                   
                

                var id=$("#id_direccion").val();
                var ruta= "' . URL::to('coordinacioncronograma/getIndicadoresGestion') . '?id="+id+"&t=4";
                $.get(ruta, function(data) {
                    $("#indicadores_gestion").empty();
                    $.each(data, function(key, element) {
                        $("#indicadores_gestion").append("<option value=\'" + element.id + "\'>"+ element.nombre+"</option>");
                    });           
                    $("#indicadores_gestion").trigger("chosen:updated");
                });
            });
            </script>';

            // $indicador = GesCabIndicadorGestionDetaModel::select(DB::raw("DATE_FORMAT(ges_tmov_gestion_indicador_deta.mes, '%m-%Y') as mes"), 'id_cab_indi')
            //     ->where('id_cab_indi', $id)
            //     ->groupBy('mes')
            //     ->get();
            // $actividades_collect = collect($indicador);
            // $history = $actividades_collect->map(function ($item, $key) {
            //     // dd($item);
            //     $history_ = GesCabIndicadorGestionDetaModel::join('ges_tmov_gestion_indicador_sub as sub_ind', 'sub_ind.id', '=', 'id_deta_ges')
            //         ->join("ges_tmae_gestion_indicador_main as ind", "ind.id", "=", "sub_ind.id_cab_ges")
            //         ->join("users as u", "u.id", "=", "id_usuario")
            //         ->select("ges_tmov_gestion_indicador_deta.*", "u.name", "sub_ind.nombresub", "ind.nombre", "ind.id as id_main", DB::raw("DATE_FORMAT(ges_tmov_gestion_indicador_deta.mes, '%Y-%m-%d') as created_at2"), DB::raw("DATE_FORMAT(ges_tmov_gestion_indicador_deta.mes, '%Y-%m') as mes_2"))
            //         ->where(['id_cab_indi' => $item->id_cab_indi, 'ges_tmov_gestion_indicador_deta.estado' => 'ACT'])
            //         ->whereRaw('DATE_FORMAT(ges_tmov_gestion_indicador_deta.mes, "%m-%Y") = ?', [$item->mes])
            //         ->orderBy("ind.nombre", "ASC")
            //         ->orderBy("mes_2", "asc")
            //         ->get();
            //     return [
            //         'Mes' => $item->mes,
            //         'Detalle' => $history_

            //     ];
            // });
            // dd($history);

            $history = null
                //  GesCabIndicadorGestionDetaModel::join('ges_tmov_gestion_indicador_sub as sub_ind', 'sub_ind.id', '=', 'id_deta_ges')
                //     ->join("ges_tmae_gestion_indicador_main as ind", "ind.id", "=", "sub_ind.id_cab_ges")
                //     ->join("users as u", "u.id", "=", "id_usuario")
                //     ->select("ind.nombre", "ind.id", "id_cab_indi")
                //     ->where(['id_cab_indi' => $id, 'ges_tmov_gestion_indicador_deta.estado' => 'ACT'])
                //     ->groupBy('nombre')
                //     ->get()
            ;
            // dd($history);


            // dd($objetos);
            unset($objetos[3]);
            unset($objetos[4]);
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
                "history" => $history
                //"historialtable" => $historialtable,
                // "obadd" => $obaddjs
            );
            /*Setear*/
        } else {
            $this->configuraciongeneral[2] = "crear";
            //$objetos = $this->verpermisos($objetos, "crear")[0];
            /*Setear Chaleco Azul*/
            // show($objetos);
            unset($objetos[3]);
            unset($objetos[4]);
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            );
        }

        if ($this->configuraciongeneral[2] == "editar") {
            //            $this->configuraciongeneral[3] = "9"; //Tipo de Referencia de Archivo
            //            $this->configuraciongeneral[4] = "si";
            $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "=", "pdf"], ["tipo", "11"]])->get();
            $dicecionesImagenes = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "<>", "pdf"], ["tipo", "11"]])->get();


            // dd($history);


        } else {
            $dicecionesDocumentos = null;
            $dicecionesImagenes = null;
        }
        $files = array(
            'dicecionesImagenes' => $dicecionesImagenes,
            'dicecionesDocumentos' => $dicecionesDocumentos
        );
        $datos = array_merge($datos, $files);
        //show($datos);
        return view('vistas.create', $datos);
    }
    public function asignaroles($objetos, $permisos, $tabla)
    {
        if ($permisos->editar_parcialmente == 'SI') {
            //show($objetos);
            foreach ($objetos as $key => $value) {
                if ($value->Tipo == "select" && $value->Nombre != "avance") {
                    $objetos[$key]->Tipo = "selectdisabled";
                    //show($tabla);
                    if ($objetos[$key]->ValorAnterior != "Null") {

                        $pos = $objetos[$key]->ValorAnterior;
                        if (!is_array($pos))
                            $objetos[$key]->Valor = array($pos => $value->Valor[$pos]);
                        else
                            foreach ($pos as $keyaa => $vaa)
                                $objetos[$key]->Valor = array($keyaa => $value->Valor[$vaa]);
                    } else {
                        //
                        $in = $objetos[$key]->Nombre;
                        $kk = $tabla->$in;
                        $aar = $objetos[$key]->Valor;
                        if (array_key_exists($kk, $aar)) {
                            //$objetos[$key]->Tipo = "html2";
                            $objetos[$key]->Valor = array($kk => $aar[$kk]);
                        }
                    }
                } elseif ($value->Tipo == "textarea" || $value->Tipo == "text") {
                    if ($value->Nombre != "json_indicadores") {
                        $objetos[$key]->Tipo = "textdisabled";
                        $in = $objetos[$key]->Nombre;
                        $kk = $tabla->$in;
                        $objetos[$key]->Valor = "" . $kk;
                    }
                } elseif ($value->Tipo == "htmlplantilla") {

                    $in = $objetos[$key]->Nombre;

                    // dd($in);
                    if ($in == "js_indicadores_plantilla") {
                        $objetos[$key]->Valor = "" . $objetos[$key]->Valor . '  <script>
                        $(document).ready(function () {
                            $("#idusuario").prop("disabled", true).trigger("chosen:updated");
                        });
                        </script>';
                    }
                }
            }
        }
        // show($objetos);
        return $objetos;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        if ($this->perfil()->crear == "NO") {
            Session::flash('message', "Ud no tiene permisos para crear");
            return Redirect::to($this->configuraciongeneral[1]);
        };

        // $indicador_main=GesCabIndicadorGestionModel::where(['id_direccion'=>Auth::user()->id_direccion,'estado'=>'ACT'])->first();

        // if($indicador_main){
        //     Session::flash('error', "Ya cuenta con un registro en este año, por favor edite el que se encuentra registrado");
        //     return Redirect::to($this->configuraciongeneral[1]);
        // }
        return $this->formularioscrear("");
    }

    public function guardar($id)
    {
        //show($id);
        $input = Input::all();
        // show($input);
        $ruta = $this->configuraciongeneral[1];
        DB::beginTransaction();
        try {
            if ($id == 0) {
                $ruta .= "/create";
                $guardar = new GesCabIndicadorGestionModel;
                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro Compromiso Alcalde";
            } else {
                $ruta .= "/" . Crypt::encrypt($id) . "/edit";
                $guardar = GesCabIndicadorGestionModel::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Compromiso Alcalde modificado";
            }

            //para actualizar los indicadores por perfil 



            $validar = GesCabIndicadorGestionModel::rules($id);
            $validator = Validator::make($input, $validar);

            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $timeline = new GesCabIndicadorGestionModel;
                foreach ($input as $key => $value) {

                    if ($key != "_method" && $key != "_token" && $key != "observacion" && $key != "idusuario" && $key != "componente" && $key != "objetivo" && $key != "programa" && $key !=  "id_coordinacion" && $key != "json_indicadores") {
                        $guardar->$key = $value;
                    }
                }
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                //Componente
                $guardar->id_objetivo_componente = Input::get("objetivo");
                $guardar->save();
                $idcab = $guardar->id;


                /*Responsables*/
                if (Input::has("idusuario")) {
                    $multiple = Input::get("idusuario");
                    $json = [];
                    $guardarres = GesCabIndicardorResponsableModel::where("id_cab_indi", $idcab)
                        ->delete();
                    foreach ($multiple as $key => $value) {
                        # code...
                        //Responsables
                        // $guardarres = GesCabIndicardorResponsableModel::where("id_usuario", $value)
                        //     ->where("id_cab_indi", $idcab)
                        //     ->first();
                        // if (!$guardarres)
                        $guardarres = new GesCabIndicardorResponsableModel;
                        $guardarres->id_usuario = $value;
                        $guardarres->id_cab_indi = $idcab;
                        $guardarres->save();
                        $json[] = $value;
                        //
                        $usuario = UsuariosModel::select("users.*")
                            ->where("id", $guardarres->id_usuario)
                            ->first();
                        //show($actividad);
                        $notificacion = new NotificacionesController();
                        $notificacion->notificacionesweb("Se le ha asignado un nuevo indicador de gestión <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit', $usuario->id, "2c438f");
                        //$this->notificacion->EnviarEmail($usuario->email, "Nueva Actividad", "Se le ha asignado una nueva actividad", "<b>ACTIVIDAD: <b>#</b>" . $idcab . "</b> <br>" . $actividad, $configuraciongeneral[1] . '/' . $guardar->id . '/edit');
                    }
                    Auditoria($msgauditoria . " - ID: " . $id);
                }



                if (Input::has("json_indicadores")) {
                    $multiple = Input::get("json_indicadores");
                    $array_indicadores = json_decode($multiple);
                    // dd($array_indicadores);

                    $obs = Input::get('observacion');

                    if (is_array($array_indicadores)) {
                        // GesCabIndicadorGestionDetaModel::where('id_cab_indi', $id)->update(["estado" => "INA"]);
                        foreach ($array_indicadores as $key => $value) {
                            // dd($value->id);
                            if ($value->estado_indicador == 0) {
                                GesCabIndicadorGestionDetaModel::whereRaw('MONTH(mes_fecha) = ?', [date('m', strtotime($value->mes_fecha))])
                                    ->where('id_cab_indi', $idcab)
                                    ->where('id_deta_ges', $value->id)
                                    ->update(['estado' => 'INA']);

                                $indicador = new GesCabIndicadorGestionDetaModel;
                                $indicador->id_cab_indi = $idcab;
                                $indicador->id_deta_ges = $value->id;
                                $indicador->valor = $value->valor;
                                $indicador->linea_base = $value->linea_base;
                                $indicador->meta_planificada = $value->meta;
                                $indicador->observaciones = $value->obs;
                                $indicador->id_usuario = Auth::user()->id;
                                $indicador->mes = $value->mes;
                                $indicador->medida = $value->medida;
                                if ($value->mes_fecha == '') {
                                    $mes_fecha = date('Y-m-d');
                                } else {
                                    $mes_fecha = $value->mes_fecha;
                                }

                                $indicador->mes_fecha = $mes_fecha;
                                $indicador->ip = \Request::getClientIp();
                                $indicador->pc = \Request::getHost();
                                $indicador->save();
                            } elseif ($value->estado_indicador == 3) {
                                $indicador = GesCabIndicadorGestionDetaModel::whereRaw('MONTH(mes_fecha) = ?', [date('m', strtotime($value->mes_fecha))])
                                    ->where('id_cab_indi', $idcab)
                                    ->where('id_deta_ges', $value->id)
                                    ->update(['estado' => 'INA']);
                                if (isset($indicador)) {
                                    $indicador = GesCabIndicadorGestionDetaModel::find($indicador->id);
                                    $valor = $indicador->valor;
                                    $mes = $indicador->mes;
                                } else {
                                    $indicador = new GesCabIndicadorGestionDetaModel;
                                    $indicador->id_cab_indi = $idcab;
                                    $indicador->id_deta_ges = $value->id;
                                    $valor = 0;
                                    $mes = 0;
                                }



                                $indicador->valor = 0;
                                $indicador->linea_base = 0;
                                $indicador->meta_planificada = 0;
                                $indicador->id_usuario = Auth::user()->id;
                                $indicador->mes = 0;
                                $indicador->mes_fecha = '';
                                $indicador->ip = \Request::getClientIp();
                                $indicador->pc = \Request::getHost();
                                $indicador->observaciones = $value->obs; # / %
                                $indicador->save();
                                $id_incdicador_deta = $indicador->id;

                                $indicador_barrio = GesIndicadorGestionDetaBarrio::whereRaw('MONTH(mes_fecha) = ?', [date('m', strtotime($value->mes_fecha))])
                                    ->where('id_indicador_deta', $id_incdicador_deta)
                                    ->where('id_barrio', $value->linea_base)
                                    ->first();
                                if (isset($indicador_barrio)) {
                                    $indicador_barrio = GesIndicadorGestionDetaBarrio::find($indicador_barrio->id);
                                } else {
                                    $indicador_barrio = new GesIndicadorGestionDetaBarrio();
                                }

                                $indicador_barrio->id_indicador_deta = $id_incdicador_deta;
                                $indicador_barrio->id_barrio = $value->linea_base;
                                $indicador_barrio->valor = $value->valor;
                                $indicador_barrio->mes = $value->mes;
                                if ($value->mes_fecha == '') {
                                    $mes_fecha = date('Y-m-d');
                                } else {
                                    $mes_fecha = $value->mes_fecha;
                                }


                                $indicador_barrio->mes_fecha = $mes_fecha;
                                $indicador_barrio->beneficiarios = $value->mes;
                                $indicador_barrio->meta = $value->meta;
                                $indicador_barrio->medida = $value->medida;
                                $indicador_barrio->save();

                                $indicador = GesCabIndicadorGestionDetaModel::find($indicador_barrio->id_indicador_deta);
                                $indicador->valor = $valor + $value->valor;
                                $indicador->mes =  $mes + $value->mes;
                                $indicador->mes_fecha = $mes_fecha;
                                $indicador->medida = $value->medida;
                                $indicador->save();
                            }
                        }
                    }
                    Auditoria($msgauditoria . " - ID: " . $id);
                }
                DB::commit();
                Session::flash('message', $msg);
                return Redirect::to($this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit');
            } //
        } catch (\Exception $e) {
            // ROLLBACK
            DB::rollback();
            return $mensaje = $e->getMessage();
            return redirect()->back()->withErrors([$mensaje])->withInput();
        }
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("ges_tmov_gestion_indicador.id", $id)->first();
        $objetos = $objetos[0];
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);

        $js = '{"Tipo":"htmlplantilla","Descripcion":"Indicadores","Nombre":"js_indicadores_plantilla","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $js = json_decode($js);
        $html = ' <div>
        <br>
        
        <table class="table table-striped" id="tabla_indicadores">
        <thead>
            <tr>
            <th scope="col">Indicador</th>
            <th scope="col">Parroquia/Detalle</th>
            <th scope="col">Barrio</th>
            <th scope="col">Número</th>
            <th scope="col">Unidad de Medida</th>
            <th scope="col">Inversión</th>
            <th scope="col">Beneficiarios</th>
            <th scope="col">Mes</th>
            </tr>
        </thead>
        <tbody>';
        $indicadores = GesCabIndicadorGestionDetaModel::join('ges_tmov_gestion_indicador_sub as sub_ind', 'sub_ind.id', '=', 'id_deta_ges')
            ->join("ges_tmae_gestion_indicador_main as ind", "ind.id", "=", "sub_ind.id_cab_ges")
            ->join("users as u", "u.id", "=", "id_usuario")
            ->select("ges_tmov_gestion_indicador_deta.*", "u.name", "sub_ind.nombresub", "ind.nombre")
            ->where(['id_cab_indi' => $id, 'ges_tmov_gestion_indicador_deta.estado' => 'ACT'])
            ->orderBy('created_at', 'ASC')
            ->get();

        foreach ($indicadores as $key => $value) {
            $html .= '<tr>
            <td>' . $value->nombre . '</td>
            <td>' . $value->nombresub . '</td>
            <td>' . $value->linea_base . '</td>
            <td>' . number_format($value->valor, 0) . '</td>
            <td>' . $value->medida . '</td>
            <td>' . number_format($value->meta_planificada, 2) . '</td>
            <td>' . $value->mes . '</td>
            <td>' . $value->mes_fecha . '</td>
            </tr>';
        }

        $html .= '</tbody>
        </table></div>';
        $js->Valor = $html;
        // $objetos = insertarinarray($objetos, $coordinacion, 2);
        // $objetos = insertarinarray($js, $coordinacion, 8);        

        $tabla->idusuario = $tabla->idusuario . ' ' . $html;

        // dd($objetos);
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();
        try {
            $tabla = GesCabIndicadorGestionModel::find($id);
            $tabla->estado = 'INA';
            $tabla->id_usuario = Auth::user()->id;
            //$tabla->delete();
            // dd($tabla);
            $tabla->save();
            DB::commit();
            Session::flash('message', 'El registro se eliminó Exitosamente!');
            if (Input::has('vista')) {
                return 'El registro se eliminó Exitosamente!';
            }
            return Redirect::to($this->configuraciongeneral[1]);
        } //Try Transaction
        catch (\Exception $e) {
            //  // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);
        }
    }
    public function getIndicadoresGestion()
    {
        $id = Input::get("id");
        $tipo = Input::get("t");
        $id_gestion = Input::get("id_gestion");
        $mes = date('m', strtotime(Input::get("mes_fecha")));

        // dd($mes);
        if ($tipo == 1) {
            if ($id_gestion != null) {
                $indicadores = GesCabIndicadorGestionDetaModel::join('ges_tmov_gestion_indicador_sub as sub_ind', 'sub_ind.id', '=', 'id_deta_ges')
                    ->join("ges_tmae_gestion_indicador_main as ind", "ind.id", "=", "sub_ind.id_cab_ges")
                    ->join("users as u", "u.id", "=", "id_usuario")
                    ->leftjoin('parroquia as p', 'p.id', '=', 'sub_ind.id_parroquia')
                    ->select("ges_tmov_gestion_indicador_deta.*", "u.name", 'sub_ind.id_parroquia', 'sub_ind.tipo', 'p.parroquia', "sub_ind.nombresub", "ind.nombre", "ind.id as id_main", DB::raw("DATE_FORMAT(ges_tmov_gestion_indicador_deta.mes_fecha, '%Y-%m-%d') as created_at2"))
                    ->where(['id_cab_indi' => Crypt::decrypt($id_gestion), 'ind.id' => $id, 'ges_tmov_gestion_indicador_deta.estado' => 'ACT'])
                    ->whereRaw('MONTH(ges_tmov_gestion_indicador_deta.mes_fecha) = ?', [$mes])
                    ->orderBy("ind.nombre", "ASC")
                    ->orderBy("ind.created_at", "DESC")
                    ->get();

                // show($indicadores);
                if (!isset($indicadores[0])) {
                    $indicadores = GesSubIndicadorModel::join("ges_tmae_gestion_indicador_main as ind", "ind.id", "=", "ges_tmov_gestion_indicador_sub.id_cab_ges")
                        ->leftjoin('parroquia as p', 'p.id', '=', 'ges_tmov_gestion_indicador_sub.id_parroquia')
                        ->select('ges_tmov_gestion_indicador_sub.tipo', 'ges_tmov_gestion_indicador_sub.id_parroquia', DB::raw("' ' as medida"), DB::raw("'-' as observaciones"), 'p.parroquia', 'ind.nombre', 'ges_tmov_gestion_indicador_sub.nombresub', 'ges_tmov_gestion_indicador_sub.id', 'ges_tmov_gestion_indicador_sub.created_at', 'ges_tmov_gestion_indicador_sub.id as id_deta_ges')
                        ->where(["ind.id" => $id, "ges_tmov_gestion_indicador_sub.estado" => "ACT"])
                        // ->pluck("nombresub", "ges_tmov_gestion_indicador_sub.id")
                        ->orderBy("ind.nombre", "ASC")
                        ->orderBy("ind.created_at", "DESC")
                        ->get();
                }
                // dd($id);
            } else {
                $indicadores = GesSubIndicadorModel::join("ges_tmae_gestion_indicador_main as ind", "ind.id", "=", "ges_tmov_gestion_indicador_sub.id_cab_ges")
                    ->leftjoin('parroquia as p', 'p.id', '=', 'ges_tmov_gestion_indicador_sub.id_parroquia')
                    ->select('ges_tmov_gestion_indicador_sub.tipo', 'ges_tmov_gestion_indicador_sub.id_parroquia', 'p.parroquia', 'ind.nombre', 'ges_tmov_gestion_indicador_sub.nombresub', 'ges_tmov_gestion_indicador_sub.id')
                    ->where(["ind.id" => $id, "ges_tmov_gestion_indicador_sub.estado" => "ACT"])
                    // ->pluck("nombresub", "ges_tmov_gestion_indicador_sub.id")
                    ->orderBy("ind.nombre", "ASC")
                    ->orderBy("ind.created_at", "DESC")
                    ->get();
            }

            return response()->json($indicadores);
        } elseif ($tipo == 2) {

            $indicadores = GesSubIndicadorModel::join("ges_tmae_gestion_indicador_main as ind", "ind.id", "=", "ges_tmov_gestion_indicador_sub.id_cab_ges")
                ->select('ind.nombre', 'ges_tmov_gestion_indicador_sub.nombresub', 'ges_tmov_gestion_indicador_sub.id')
                ->where(["ges_tmov_gestion_indicador_sub.id" => $id, "ges_tmov_gestion_indicador_sub.estado" => "ACT"])->first();
            return response()->json($indicadores);
        } elseif ($tipo == 3) {

            try {
                // $id = Crypt::decrypt($id);


                $indicadores = GesCabIndicadorGestionDetaModel::join('ges_tmov_gestion_indicador_sub as sub_ind', 'sub_ind.id', '=', 'id_deta_ges')
                    ->join("ges_tmae_gestion_indicador_main as ind", "ind.id", "=", "sub_ind.id_cab_ges")
                    ->leftjoin('parroquia as p', 'p.id', '=', 'sub_ind.id_parroquia')
                    ->join("users as u", "u.id", "=", "id_usuario")
                    ->select("ges_tmov_gestion_indicador_deta.*", "u.name", "sub_ind.nombresub", "sub_ind.tipo", "p.parroquia", "ind.nombre")
                    ->where(['id_cab_indi' => $id, 'ges_tmov_gestion_indicador_deta.estado' => 'ACT', 'sub_ind.id_cab_ges' => $id_gestion])
                    ->orderBy("ind.nombre", "ASC")
                    ->orderBy("ind.created_at", "DESC")
                    ->orderBy("mes_fecha", "DESC")
                    ->orderBy("nombresub", "DESC")
                    ->get();

                // dd($indicadores);
                $ajaxtxt = "<p><table class='table'><tr><td>Indicador</td><td>Parroquia/Detalle</td><td>Barrio</td><td>Número</td><td>Inversión</td><td>Beneficiarios</td><td>Fecha de registro</td><td>Ultima actualización</td></tr>";
                foreach ($indicadores as $key => $value) {

                    if ($value->tipo == 2) {
                        $ajaxtxt .= "<tr><td>$value->nombre</td><td>$value->parroquia</td><td><button type='button' onclick='mostrarBarriosDeta($value->id)' class='btn btn-success'>Ver barrios</button></td><td>$value->valor</td><td>$value->meta_planificada</td><td>$value->mes</td><td>$value->mes_fecha</td><td>$value->updated_at</td></tr>";
                    } else {
                        $ajaxtxt .= "<tr><td>$value->nombre</td><td>$value->nombresub</td><td></td><td>" . number_format($value->valor, 0) . "</td><td>" . number_format($value->meta_planificada, 2) . "</td><td>$value->mes</td><td>$value->mes_fecha</td><td>$value->updated_at</td></tr>";
                    }
                }
                $ajaxtxt .= "</table></p>";
                return $ajaxtxt;
            } catch (Exception $e) {
                return $e;
            }
        } elseif ($tipo == 4) {
            $indicadores = GesSubIndicadorModel::join("ges_tmae_gestion_indicador_main as ind", "ind.id", "=", "ges_tmov_gestion_indicador_sub.id_cab_ges")
                ->select('ind.nombre', 'ind.id')
                ->where(["id_direccion" => $id, "ges_tmov_gestion_indicador_sub.estado" => "ACT"])
                ->groupBy('ind.id')
                ->orderBy("ind.nombre", "ASC")
                ->orderBy("ind.created_at", "DESC")
                ->get();
            // dd( $indicadores);
            // $indicadores=collect($indicadores);
            // $indicadores->push(['id'=>0,'nombre'=>'Escoja']);
            // $indicadores->all();
            return response()->json(array_merge([['id' => null, 'nombre' => 'Seleccione indicador']], $indicadores->toArray()));
            // dd($id);

        } elseif ($tipo == 5) {
            $indicadores = GesSubIndicadorModel::join("ges_tmae_gestion_indicador_main as ind", "ind.id", "=", "ges_tmov_gestion_indicador_sub.id_cab_ges")
                ->select('ind.nombre', 'ind.id')
                ->where(["id_direccion" => $id, "ges_tmov_gestion_indicador_sub.estado" => "ACT"])
                ->groupBy('ind.id')
                ->orderBy("ind.nombre", "ASC")
                ->orderBy("ind.created_at", "DESC")
                ->get();
            // dd( $indicadores);
            // $indicadores=collect($indicadores);
            // $indicadores->push(['id'=>0,'nombre'=>'Escoja']);
            // $indicadores->all();
            return response()->json(array_merge([['id' => 0, 'nombre' => 'TODOS']], $indicadores->toArray()));
        }
    }
    public function getBarrios($id_parroquia)
    {
        $tipo = Input::get('tipo');
        if ($tipo == 1) {
            $indicadores_barrios = GesIndicadorGestionDetaBarrio::leftjoin('barrio as p', 'p.id', '=', 'ges_tmov_gestion_indicador_parroquia_deta.id_barrio')
                ->where(['id_indicador_deta' => $id_parroquia])->get();
            $ajaxtxt = "<p><table class='table'><tr><td>Barrio</td><td>Número</td><td>Inversión</td><td>Beneficiarios</td><td>Fecha de registro</td><td>Ultima actualización</td></tr>";
            foreach ($indicadores_barrios as $key => $value) {
                # code...

                $ajaxtxt .= "<tr><td>$value->barrio</td></td><td>$value->valor</td><td>$value->meta</td><td>$value->beneficiarios</td><td>$value->created_at</td><td>$value->updated_at</td></tr>";
            }

            $ajaxtxt .= "</table></p>";

            // return $indicadores_barrios;
            return $ajaxtxt;
        } else {

            // $indicadores = GesSubIndicadorModel::select('id_parroquia')
            //     ->where(["ges_tmov_gestion_indicador_sub.id" => $id_parroquia, "ges_tmov_gestion_indicador_sub.estado" => "ACT"])->first();
            // if ($indicadores) {
            if ($tipo == 2) {
                if ($id_parroquia == 0)
                    return [0 => 'TODOS'] + BarrioModel::where('estado', 'ACT')->orderby("barrio", "asc")->get();

                $barrios = BarrioModel::where('id_parroquia', $id_parroquia)->orderby("barrio", "asc")->get();
                // asort($barrios);
                return $barrios;
            } else {
                return BarrioModel::where('id_parroquia', $id_parroquia)->orderby("barrio", "asc")->get();
            }
            // } else {
            //     return [];
            // }
        }
    }

    public function getCantones()
    {
        return CantonModel::select("id", "nombre_canton as canton")->orderby("nombre_canton", "asc")->get();
    }
}
