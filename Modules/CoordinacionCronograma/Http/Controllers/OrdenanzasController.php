<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\CoordinacionCronograma\Entities\PlanesEmergenciasCoor;
use stdClass;

/////// YA QUEDÓ CON S xD
class OrdenanzasController extends Controller
{
    var $configuraciongeneral = array("ORDENANZAS", "coordinacioncronograma/ordenanzas", "index", 6 => "coordinacioncronograma/ordenanzasajax", 7 => "ordenanzas", 20 => "coordinacioncronograma/ordenanzasajaxgetarchivos");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"select","Descripcion":"Dirección / Área","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Desctripción","Nombre":"detalle","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Archivo","Nombre":"archivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';
    var $validarjs = array();
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        $object->perfil = $id_tipo_pefil->tipo;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 7) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        }
        //show($object);
        return $object;
    }
    public function verpermisos($objetos = array(), $tipo = "index")
    {
        $tabla = PlanesEmergenciasCoor::join("tmae_direcciones as a", "a.id", "=", "planes_emergencias.id_direccion")
            ->select(
                "planes_emergencias.*",
                "a.direccion"
            )->where("planes_emergencias.estado", "ACT")
            ->where("planes_emergencias.tipo", "ORDENANZA")
            ->orderby("planes_emergencias.id", "desc");
        //show($tabla->get());
        $seleccionar_direccion = direccionesModel::where("estado", "ACT");
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $id_usuario = Auth::user()->id;
        if ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 2) {
            /*if (count($objetos)) {
                unset($objetos[9]);
                unset($objetos[10]);
            }*/
            $seleccionar_direccion = $seleccionar_direccion->where("id", Auth::user()->id_direccion);
            $tabla = $tabla->where("planes_emergencias.id_direccion", Auth::user()->id_direccion);
            //if($id_tipo_pefil->tipo == 2)
            //                $tabla = $tabla->whereRaw("$id_usuario in(select ff.id_usuario from coor_tmov_compromisos_resposables as ff where ff.id_crono_cab = coor_tmov_compromisos_cab.id)");
        }


        ////////////////
        if ($id_tipo_pefil->tipo == 10) {
            $direcciones = getDireccionesCoor(Auth::user()->id);
            $tabla = $tabla->whereIn("planes_emergencias.id_direccion", explode(",", $direcciones->id_direccion));
        }



        $seleccionar_direccion = $seleccionar_direccion->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        //show($seleccionar_direccion);
        if (count($objetos))
            $objetos[0]->Valor = $this->escoja + $seleccionar_direccion;
        if ($tipo == "index") {
            $objetos[0]->Nombre = "direccion";
        }
        //show($objetos);
        return array($objetos, $tabla);
    }
    public function ordenanzasajaxgetarchivos(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        //show($tabla);
        $objetos = $tabla[0];
        $tabla = $tabla[1]->get();
        //show($tabla->get());
        $resul = "";
        $permisos = $this->perfil();
        foreach ($tabla as $key => $value) {

            $dele = '<a onClick="eliminar(' . $value->id . ')"  style="color: red;"><i class="fa fa-trash"></i> ELIMINAR</a>
            <div style="display: none;">
            <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $value->id . '" accept-charset="UTF-8" id="frmElimina' . $value->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                <input name="_token" type="hidden" value="' . csrf_token() . '">
                <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
            </form>
            </div>';
            if ($permisos->eliminar == 'NO')
                $dele = "";


            //$edit = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($value->id),"menu"=>"no"), array('class' => 'fa fa-pencil-square-o divpopup', 'onclick' => 'popup(this)'));
            $editico = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', ' EDITAR ', array(Crypt::encrypt($value->id)), array('class' => 'fa fa-pencil-square-o divpopup', 'style' => 'color:white;'));
            // $editico = "";
            $edit = '<a href="' . URL::to("/") . '/archivos_sistema/' . $value->archivo . '" onclick="popup(this)">';
            $size = is_file(public_path() . "/archivos_sistema/" . $value->archivo) ? round(filesize(public_path() . "/archivos_sistema/" . $value->archivo) / 1024, 2) : "-";
            $size .= " (Kb/Mb)<br>" . $value->created_at;
            $resul .= '<div class="col-lg-6">
            <div class="file">
            
            
            <div class="col-lg-8 file-name">
            <span class="badge badge-success">' . $value->direccion . '</span><span class="badge badge-primary" >' . $editico . '</span> ' . $dele . '<br>
            
            <br>
            <b>Descripción: </b>' . $value->detalle . '
            <br>
            <small>' . $size . '</small>
            </div>
            ' . $edit . '
            
            <div class="col-lg-3 ">
            <img alt="image" class="img-responsive" src="' . asset("img/icons/pdf-icon.png") . '">
            </div>
            </a>
            
            </div>

                    </div>' . "\n\r";
            $resul .= '<script type="text/javascript">
        $(document).ready(function(){
            $(\'.file-box\').each(function() {
                animationHover(this, \'pulse\');
            });
        });
    </script>';
        }
        return $resul;
    }
    public function ordenanzasajax(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        //show($tabla);
        $objetos = $tabla[0];
        $tabla = $tabla[1];
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        //show($columns);
        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('planes_emergencias.id', 'LIKE', "%{$search}%")
                    ->orWhere('a.direccion', 'LIKE', "%{$search}%")
                    ->orWhere('planes_emergencias.detalle', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            $permisos = $this->perfil();
            foreach ($posts as $post) {
                //=============================================================
                $show = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                //'&nbsp;&nbsp;' .
                $edit = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id), "menu" => "no"), array('class' => 'fa fa-pencil-square-o divpopup', 'onclick' => 'popup(this)'));
                //. '&nbsp;&nbsp;
                $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                if ($permisos->ver == 'NO')
                    $show = "";
                if ($permisos->editar == 'NO')
                    $edit = "";
                if ($permisos->eliminar == 'NO')
                    $dele = "";
                $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                //=============================================================
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $valorc = $post->$campo;
                    $nestedData["$value->Nombre"] = "$valorc";
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = $this->verpermisos(json_decode($this->objetos));
        return view('vistas.indexarchivos', [
            "objetos" => $objetos[0],
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si",
            "permisos" => $this->perfil()
        ]);
        //return view('coordinacioncronograma::index');
    }
    public function formularioscrear($id)
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);

        /*Dirección*/
        $seleccionar_direccion = direccionesModel::where("estado", "ACT")->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        $objetos[0]->Valor = $this->escoja + $seleccionar_direccion;
        $objetosmain = $this->verpermisos($objetos, "crear");
        /*====================================================*/
        if ($id != "") {
            // dd($objetos);
            $id = Crypt::decrypt($id);
            $tabla = PlanesEmergenciasCoor::find($id);
            $this->configuraciongeneral[2] = "editar";
            //==========================================================
            /*
            $this->configuraciongeneral[2] = "editar";
            $this->configuraciongeneral[3] = "11"; //Tipo de Referencia de Archivo
            $this->configuraciongeneral[4] = "si";
*/
            //$objetos = $this->asignaroles($objetos, $this->perfil(), $tabla);
            //show($objetos);
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
                "permisos" => $this->perfil(),
                //"historialtable"=>$historialtable,
                //"obadd"=>$obaddjs,
                "botonguardaravance" => "no"
            );
            /*Setear*/
        } else {
            $this->configuraciongeneral[2] = "crear";
            //$objetos = $this->verpermisos($objetos, "crear")[0];
            /*Setear Chaleco Azul*/
            //show($objetos);
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => array()
            );
        }
        //show($objetos);
        /*
        if ($this->configuraciongeneral[2] == "editar") {
            //            $this->configuraciongeneral[3] = "9"; //Tipo de Referencia de Archivo
            //            $this->configuraciongeneral[4] = "si";
            $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "=", "pdf"], ["tipo", "11"]])->get();
            $dicecionesImagenes = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "<>", "pdf"], ["tipo", "11"]])->get();
        } else {
            $dicecionesDocumentos = null;
            $dicecionesImagenes = null;
        }

        $files = array(
            'dicecionesImagenes' => $dicecionesImagenes,
            'dicecionesDocumentos' => $dicecionesDocumentos
        );
        */
        $datos = array_merge($datos); //, $files);
        //show($datos);
        return view('vistas.create', $datos);
    }
    public function guardar($id)
    {
        //show($id);
        $input = Input::all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];
        $menuno = "";
        if (Input::has("menu")) {
            $menuno = "?menu=no";
            unset($input["menu"]);
        }
        DB::beginTransaction();
        try {
            if ($id == 0) {
                $ruta .= "/create";
                $guardar = new PlanesEmergenciasCoor;
                $guardar->tipo = 'ORDENANZA';
                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro Compromiso Alcalde";
            } else {
                $ruta .= "/" . Crypt::encrypt($id) . "/edit";
                $guardar = PlanesEmergenciasCoor::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Compromiso Alcalde modificado";
            }

            //$input = Input::all();
            $validar = PlanesEmergenciasCoor::rules($id);
            $validator = Validator::make($input, $validar);

            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            } else {
                //die("Hola");
                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token" && $key != "archivo") {
                        $guardar->$key = $value;
                    }
                }
                /*Subir Archivo*/
                $dir = public_path() . '/archivos_sistema/';
                $docs = Input::file("archivo");
                if ($docs) {
                    $dire = direccionesModel::find(Input::get("id_direccion"));
                    $id = $dire->id . "_" . strtotime(fechas(0));
                    $fileName = strtolower(reemplazaespeciales($dire->direccion)) . "_$id." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                    $ext = $docs->getClientOriginalExtension();
                    $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                    if (!in_array($ext, $perfiles)) {
                        DB::rollback();
                        $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                        return redirect()->back()->withErrors([$mensaje])->withInput();
                    }
                    //shoW();
                    /*
                    $oldfile = $dir . '' . $fileName;
                    if (is_file($oldfile)) {
                        if($id_tipo_pefil->tipo != 1)
                        {
                            if (file_exists($file_path))
                                unlink($file_path);
                        }else
                        {
                            DB::rollback();
                            $mensaje = "Ya se encuentra el archivo final registrado";
                            return redirect()->back()->withErrors([$mensaje])->withInput();
                        }
                    }
                    */
                    $docs->move($dir, $fileName);
                    $guardar->archivo = $fileName;
                    /**/
                    $guardar->id_usuario = Auth::user()->id;
                    $guardar->ip = \Request::getClientIp();
                    $guardar->pc = \Request::getHost();
                    $guardar->save();
                    //die("aqui");
                    Auditoria($msgauditoria . " - ID: " . $id);
                    DB::commit();
                    Session::flash('message', $msg);
                    $redir = $this->configuraciongeneral[1] . $menuno;
                    if ($id == 0)
                        $redir = $this->configuraciongeneral[1] . "/" . Crypt::encrypt($guardar->id) . "/edit" . $menuno;
                    return Redirect::to($redir);
                    //
                }
            }
        } catch (\Exception $e) {
            // ROLLBACK
            DB::rollback();
            $mensaje = $e->getMessage();
            return redirect()->back()->withErrors([$mensaje])->withInput();
        }
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return $this->formularioscrear("");
        //return view('coordinacioncronograma::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("planes_emergencias.id", $id)->first();
        $this->configuraciongeneral[3] = "11"; //Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "Si";
        $dicecionesImagenes = [];
        $dicecionesDocumentos = [];
        //$this->configuraciongeneral[0].="<p>".$tabla->direccion."</p>";
        $this->configuraciongeneral[0] = "<p>" . $tabla->direccion . "</p>";
        $objetos = $objetos[0];
        unset($objetos[0]);
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            'dicecionesImagenes' => $dicecionesImagenes,
            'dicecionesDocumentos' => $dicecionesDocumentos
            //"timeline" => $this->construyetimeline($id)
        ]);
        //return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
        //return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $tabla = PlanesEmergenciasCoor::find($id);
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
