<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\CoordinacionCronograma\Entities\TeleTeletrabajoArchivosModel;
use Modules\CoordinacionCronograma\Entities\TeleTeletrabajoModel;
use Modules\CoordinacionCronograma\Entities\TeleTeletrabajoSemanasModel;
//
use Modules\CoordinacionCronograma\Entities\TeleTeletrabajoHistorialModel;
use Modules\CoordinacionCronograma\Entities\TeleTeletrabajoArchivosHistorialModel;
use stdClass;

class TeleTeletrabajoSemanasController extends Controller
{
    var $configuraciongeneral = array("TELETRABAJO", "coordinacioncronograma/teletrabajo", "index", 6 => "coordinacioncronograma/teletrabajoajax", 7 => "teletrabajo", 20 => "coordinacioncronograma/teletrabajoajaxgetarchivos");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"select","Descripcion":"Semana","Nombre":"id_semana","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Entidad","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tipo de actividad","Nombre":"tipo_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"htmlplantilla","Descripcion":"Archivos","Nombre":"archivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Observación","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado_documentacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';
    var $validarjs = array();
    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }
    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        $object->perfil = $id_tipo_pefil->tipo;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 8 || $id_tipo_pefil->tipo == 10) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 7) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        }
        //show($object);
        return $object;
    }
    /*Mostrar Historial de Teletrabajo*/
    public function teletrabajohistorial($id)
    {
        //$id=Crypt::decrypt($id);
        //$tabla = $this->verpermisos(json_decode($this->objetos));
        // dd($tabla);

        //$objetos = $tabla[0];

        $tablahead = TeleTeletrabajoHistorialModel::join("tele_tmov_teletrabajo_semanas as a", "a.id", "=", "tele_tmov_teletrabajo_deta.id_semana")
            ->join("tmae_direcciones as b", "b.id", "=", "tele_tmov_teletrabajo_deta.id_direccion")
            ->join("users as c", "c.id", "=", "tele_tmov_teletrabajo_deta.id_usuario")
            ->select("tele_tmov_teletrabajo_deta.*", "a.semana", "b.direccion", "c.name")
            ->where("tele_tmov_teletrabajo_deta.id_cab_tele_trab", $id)
            ->orderBy("id");
        $tabladeta = $tablahead->get();
        //return $tabladeta;
        $tablahead = $tablahead->first();
        $this->configuraciongeneral[0] = "Historial";
        return view('vistas.show_files_teletrabajo', [
            "tablahead" => $tablahead,
            "tabladeta" => $tabladeta,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }
    public function verpermisos($objetos = array(), $tipo = "index")
    {
        $tabla = TeleTeletrabajoModel::join('tele_tmov_teletrabajo_semanas as sema', 'sema.id', '=', 'tele_tmov_teletrabajo.id_semana')
            ->select(
                "tele_tmov_teletrabajo.*",
                "sema.semana"
            )
            // ->
            ->where("tele_tmov_teletrabajo.estado", "ACT")
            ->orderby("tele_tmov_teletrabajo.created_at", "desc")
            ;
        // $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        // dd(Auth::user()->id_direccion);
        // if ($id_tipo_pefil->tipo == 3 && Auth::user()->id_direccion != 5) {
        //     $tabla = $tabla->where("tele_tmov_teletrabajo.id_direccion", Auth::user()->id_direccion);
        // }


        return array($objetos, $tabla);
    }
    public function teletrabajoajaxgetarchivos(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        // dd($tabla);

        $objetos = $tabla[0];

        // dd();
        $entidad = Input::get('entidad');
        if ($entidad == null) {
            $entidad = Auth::user()->id_direccion;
        }

        // if($entidad){

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4 && Auth::user()->id_direccion != 5) {
            // dd('sss');
            if (Auth::user()->id_direccion != $entidad) {
                $entidad = Auth::user()->id_direccion;
            }
        }
        $tabla = $tabla[1]->where('id_direccion', $entidad)
        // ->orderby('id_semana','DESC')
        // ->orderby('sema.semana','ASC')
        ->get();
        // if ($entidad == 20) {
        //     $tabla = $tabla
        //         ->where('id_semana', '<>', 9)
        //         // ->where('tipo_actividad', '<>', 'VERIFICABLES')
        //         ;
        // }

        // dd($tabla);
        // }else{

        //     $tabla = $tabla[1]->get();
        // }
        $direcion = direccionesModel::where('id', $entidad)->first();
        //show($tabla->get());
        $resul = "<div style='text-align: center;'><h3>" . $direcion->direccion . "</h3></div>";
        foreach ($tabla as $key => $value) {
            //$edit = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($value->id),"menu"=>"no"), array('class' => 'fa fa-pencil-square-o divpopup', 'onclick' => 'popup(this)'));
            $editico = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', ' EDITAR ', array(Crypt::encrypt($value->id)), array('class' => 'fa fa-pencil-square-o ', 'style' => 'color:#008605;'));
            $eliminar =  '&nbsp;&nbsp;<a class="fa fa-trash " style="color:#a93131;" onClick="eliminar(' . $value->id . ')"> ELIMINAR</a>
            <div style="display: none;">
            <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $value->id . '" accept-charset="UTF-8" id="frmElimina' . $value->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                <input name="_token" type="hidden" value="' . csrf_token() . '">
                <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
            </form>
            </div>';
            //$hitorialarchivos=


            $archivos = TeleTeletrabajoArchivosModel::where(['id_cab_teletrabajo' => $value->id, 'estado' => 'ACT'])->get();
            if (isset($archivos[0]->archivo)) {
            }
            $tabla = '
            <table style="width:100%">
            ';
            $fi = 0;
            $tatofile = 0;
            foreach ($archivos as $k => $vv) {
                $fi++;
                $tafile = (is_file(public_path() . '/teletrabajo/' . $value->id . '/' . $vv->archivo)) ? round(filesize(public_path() . '/teletrabajo/' . $value->id . '/' . $vv->archivo) / 1024, 2) : "0";
                $tatofile += intval($tafile);
                $tabla .= '<tr>
                        <td>' . $fi . '</td>
                       <td><a href="' . URL::to("/") . '/teletrabajo/' . $value->id . '/' . $vv->archivo . '" class="divpopup" target="_blank"  onclick="popup(this)"><i class="fa fa-file-pdf-o" aria-hidden="true"> ' . $vv->archivo . ' </i></a></td>
                       <td>Ingresado: ' . $vv->created_at . '</td>
                       <td style="text-align: right;">' . $tafile . ' Kb</td>
                </tr>';
                // dd($tabla);
            }
            $tatofilemb = 0;
            if ($tatofile >= 1024)
                $tatofilemb = round($tatofile / 1024, 2);
            $tatofilembtxt = ($tatofilemb > 0) ? "&nbsp;<span class='label label-warning-light'>$tatofilemb MB</span>" : "";
            $tabla .= "<tr><th colspan='3'><span class='label label-primary'>$fi archivo(s) cargado(s)</span>&nbsp;<span class='label label-warning'>$tatofile KB</span>$tatofilembtxt</th></tr>";

            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();


            $tabla .= '</table>';


            if ($value->estado_documentacion == 'REGISTRADO') {
                $color = 'secondary';
                if ($id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4) {
                    $editico = "";
                    $eliminar = "";
                }
            } elseif ($value->estado_documentacion == 'CON OBSERVACIONES') {
                $color = 'danger';
                if ($id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4 && Auth::user()->id_direccion != $value->id_direccion) {
                    $editico = "";
                }
                if ($id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4) {

                    $eliminar = "";
                }
            } else {

                if ($id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4) {
                    $editico = "";
                    $eliminar = "";
                }
                $color = 'success';
            }
            // dd();
            $resul .= '
            <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <span class="badge badge-' . $color . '">' . $value->estado_documentacion . '</span>
                        ' . $editico . '
                        ' . $eliminar . '
                        <span class="badge badge-success">
                        <a href="' . URL::to("coordinacioncronograma/teletrabajohistorial") . "/" . $value->id . '" target="_blank" class="divpopup" onclick="popup(this)">
                            <i class="far fa-clock"></i> HISTORIAL</span>
                        </a>

                    </div>
                    <div class="">
                        <b> Semana:</b> ' . $value->semana . ' 
                        <br>
                        <b> Tipo de actividad: </b>' . $value->tipo_actividad . '
                        <br>
                        <b> Observación: </b>' . $value->observacion . '
                        <br>
                        <span class="badge badge-primary">' . $value->nombre_archivo . '</span>
                    </div>

                </div>
                <div class="ibox-content">
                ' . $tabla  . '
                </div>
            </div>

        </div>' . "\n\r";



            $resul .= '<script type="text/javascript">
        $(document).ready(function(){
            $(\'.zoom\').each(function() {
                animationHover(this, \'pulse\');
            });
        });
    </script>';
        }
        return $resul;
    }
    public function teletrabajoajax(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        //show($tabla);
        $objetos = $tabla[0];
        $tabla = $tabla[1];

        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        //show($columns);
        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('plan_estra_archivos.id', 'LIKE', "%{$search}%")
                    ->orWhere('a.direccion', 'LIKE', "%{$search}%")
                    ->orWhere('plan_estra_archivos.observacion', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            $permisos = $this->perfil();
            foreach ($posts as $post) {
                //=============================================================
                $show = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                //'&nbsp;&nbsp;' .
                $edit = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id), "menu" => "no"), array('class' => 'fa fa-pencil-square-o divpopup', 'onclick' => 'popup(this)'));
                //. '&nbsp;&nbsp;
                $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                if ($permisos->ver == 'NO')
                    $show = "";
                if ($permisos->editar == 'NO')
                    $edit = "";
                if ($permisos->eliminar == 'NO')
                    $dele = "";
                $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                //=============================================================
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $valorc = $post->$campo;
                    $nestedData["$value->Nombre"] = "$valorc";
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $entidad = Input::get('entidad');
        $this->configuraciongeneral[20] = $this->configuraciongeneral[20] . "?entidad=" . $entidad;
        $entidades = TeleTeletrabajoModel::join('tmae_direcciones as en', 'en.id', '=', 'tele_tmov_teletrabajo.id_direccion')
            ->where("tele_tmov_teletrabajo.estado", "ACT")
            ->orderby("en.direccion", "asc")
            ->groupBy("tele_tmov_teletrabajo.id_direccion");
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 ||    Auth::user()->id_direccion == 5) {
            // dd($id_tipo_pefil->tipo);
            $entidades = $entidades->get();
        } else {
            $entidades = $entidades->where("tele_tmov_teletrabajo.id_direccion", Auth::user()->id_direccion)->get();
        }
        // $entidades = ConEntidadesModel::where('estado', 'ACT')->get();
        return view('vistas.indexarchivos', [
            "objetos" => $objetos[0],
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "entidad" => $entidad,
            "direciones" => $entidades,
            "convenios" => 1,
            "create" => "si",
            "permisos" => $this->perfil()
        ]);
        //return view('coordinacioncronograma::index');
    }
    public function formularioscrear($id)
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);

        /*Dirección*/
        $seleccionar_direccion = direccionesModel::where("estado", "ACT")->orderby("direccion")->pluck("direccion", "id")->all();
        $semanas = TeleTeletrabajoSemanasModel::select(DB::raw('concat(semana,":  ",fecha_inicio," - ",fecha_fin) as semana'), 'id')->where("estado", "ACT")->orderby("semana")->pluck("semana", "id")->all();
        $objetos[0]->Valor = $this->escoja + $semanas;
        $objetos[1]->Valor = $this->escoja + $seleccionar_direccion;

        $objetos[2]->Valor = ['PLANIFICADAS' => 'PLANIFICADAS', 'VERIFICABLES' => 'VERIFICABLES'];

        $objetos[3]->Valor = '<div style="text-align: end;"><button type="button" class="btn btn-info" id="add_archivo">
        <i class="fa fa-plus-circle" style="font-size: 1.5em;"> </i> Agregar Item
        </button></div>
        <table class="table table-striped" id="tabla_archivos">
            <thead>
                <tr>
                <th scope="col">Archivo</th>
                <th scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <script type="text/javascript">
        var n_filas_arr=0;
        
        $(function () {
            $("#add_archivo").click(function (e) { 
                $("#tabla_archivos tbody").prepend(\'<tr id="fila_ar\'+n_filas_arr+\'" ><td><input type="file" required accept="application/pdf" onChange="validarFile(this)" class="form-control" name="archivos[]"></td><td><i onclick="borrar_archivo(\'+n_filas_arr+\')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
                n_filas_arr++;
            });
            $("#add_archivo").trigger("click");
        });
        function borrar_archivo(id){

            $("#fila_ar"+id).remove();
        }


        function validarFile(all)
        {
            //EXTENSIONES Y TAMANO PERMITIDO.
            var extensiones_permitidas = [".pdf"];
            var tamano = 3; // EXPRESADO EN MB.
            var rutayarchivo = all.value;
            var ultimo_punto = all.value.lastIndexOf(".");
            var extension = rutayarchivo.slice(ultimo_punto, rutayarchivo.length);
            if(extensiones_permitidas.indexOf(extension) == -1)
            {
                toastr["error"]("Extensión de archivo no valida");
                 all.value = "";
                return; // Si la extension es no válida ya no chequeo lo de abajo.
            }
            if((all.files[0].size / 1048576) > tamano)
            {
                toastr["error"]("El archivo no puede superar los "+tamano+"MB");
                 all.value = "";
                return;
            }
        }
        </script>';
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4) {
            unset($objetos[1]);
            if ($id != "") {
                $id_c = Crypt::decrypt($id);
                $tabla = TeleTeletrabajoModel::find($id_c);
                if ($id_c != 0 && $tabla->estado_documentacion == 'CON OBSERVACIONES') {
                    // dd($tabla);
                    $objetos[4]->Tipo = 'html2';
                    $objetos[4]->Valor = $tabla->observacion;
                } else {
                    unset($objetos[4]);
                }
            } else {
                unset($objetos[4]);
            }
            unset($objetos[5]);
        } else {
            $objetos[5]->Valor = ['REGISTRADO' => 'REGISTRADO', 'CON OBSERVACIONES' => 'CON OBSERVACIONES', 'APROBADO' => 'APROBADO'];
            // dd($id);
            if ($id == "") {
                unset($objetos[4]);
                unset($objetos[5]);
            }
        }
        /*====================================================*/
        if ($id != "") {
            // dd($objetos);
            $id = Crypt::decrypt($id);
            $tabla = TeleTeletrabajoModel::find($id);

            // $objetos[0]->ValorAnterior = $tabla->id_semana;
            // $objetos[1]->ValorAnterior = $tabla->id_direccion;
            // $objetos[2]->ValorAnterior = $tabla->tipo_actividad;


            $objetos[4]->Valor = (isset($tabla->observacion)) ? $tabla->observacion : ' ';

            if ($id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4) {
                $objetos[3]->Tipo = 'html2';
            }

            $html = '
        <div style="text-align: end;"><button type="button" class="btn btn-info" id="add_archivo">
        <i class="fa fa-plus-circle" style="font-size: 1.5em;"> </i> Agregar Item
        </button></div>
        <table class="table table-striped" style="text-align: center;" id="tabla_archivos">
            <thead >
                <tr>
                <th scope="col" style="text-align: center;">Archivo</th>
                <th scope="col" style="text-align: center;">Acción</th>
                </tr>
            </thead>
            <tbody>
            ';
            $archivos = TeleTeletrabajoArchivosModel::where(['id_cab_teletrabajo' => $id, 'estado' => 'ACT'])->get();
            // dd( $archivos );
            $archivos_total = TeleTeletrabajoArchivosModel::where(['id_cab_teletrabajo' => $id, 'estado' => 'ACT'])->count();
            foreach ($archivos as $key => $value) {
                $html .= '<tr id="fila_ar' . ($key + 1) . '"><td><a   target="_blank" href="' . URL::to('') . '/teletrabajo/' . $id . '/' . $value->archivo . '" class="divpopup" target="_blank"  onclick="popup(this)" ><i class="fa fa-file-pdf-o" aria-hidden="true"> </i> ' . $value->archivo . '</td><td><i onclick=" borrar_bd(' . $value->id . ',' . ($key + 1) . ')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></td></tr>';
            }
            $html .= '</tbody></table><script type="text/javascript">
        var n_filas_arr=' . $archivos_total . ';
        $("#add_archivo").click(function (e) { 
            $("#tabla_archivos tbody").prepend(\'<tr id="fila_ar\'+n_filas_arr+\'" ><td><input type="file" accept="application/pdf" onChange="validarFile(this)" required class="form-control" name="archivos[]"></td><td><i onclick="borrar_archivo(\'+n_filas_arr+\')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
            n_filas_arr++;
        });
        function borrar_archivo(id){

            $("#fila_ar"+id).remove();
        }
        function borrar_bd(id,fila){
            var ruta_delete="' . URL::to('coordinacioncronograma/deleteArchivoTeletrabajo') . '?id="+id+"&t=1&_token=' . csrf_token() . '";
            $.post(ruta_delete, function(data) {
                    if(data.tipo){
                        $("#fila_ar"+fila).remove();
                        toastr["success"](data.mensaje);
                    }else{
                        toastr["error"](data.mensaje);
                    }
            });
        }

        function validarFile(all)
        {
            //EXTENSIONES Y TAMANO PERMITIDO.
            var extensiones_permitidas = [".pdf"];
            var tamano = 3; // EXPRESADO EN MB.
            var rutayarchivo = all.value;
            var ultimo_punto = all.value.lastIndexOf(".");
            var extension = rutayarchivo.slice(ultimo_punto, rutayarchivo.length);
            if(extensiones_permitidas.indexOf(extension) == -1)
            {
                toastr["error"]("Extensión de archivo no valida");
                all.value = "";
                return; // Si la extension es no válida ya no chequeo lo de abajo.
            }
            if((all.files[0].size / 1048576) > tamano)
            {
                toastr["error"]("El archivo no puede superar los "+tamano+"MB");
                all.value = "";
                return;
            }
        }
        </script>';
            // dd($objetos);

            // show($objetos);
            $objetos[3]->Valor = $html;
            $this->configuraciongeneral[2] = "editar";
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
                "permisos" => $this->perfil(),
                "botonguardaravance" => "no"
            );
            /*Setear*/
        } else {
            $this->configuraciongeneral[2] = "crear";
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => array()
            );
        }

        $datos = array_merge($datos);
        return view('vistas.create', $datos);
    }
    public function guardar($id)
    {
        // show($id);
        $input = Input::all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];


        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $tipo_actividad = Input::get('tipo_actividad');
        $editar = ConfigSystem('validacion_teletrabajo');


        DB::beginTransaction();
        try {
            if ($id == 0) {
                $ruta .= "/create";
                $guardar = new TeleTeletrabajoModel();
                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro Compromiso Alcalde";

                if ($editar == 'SI') {
                    if (permisoingresoplanificacion('Fry|18;Sat|-1;Sun|-1;Mon|-1;Tue|-1;Wed|-1;Thu|-1') && ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) && $tipo_actividad == 'VERIFICABLES') {
                        Session::flash('message', 'No está permitido realizar cambios por el momento. Se puede realizar ingresos el día viernes(hasta las 6PM)');
                        return Redirect::to($this->configuraciongeneral[1]);
                    }
                    if (permisoingresoplanificacion('Mon|18;Tue|-1;Wed|-1;Thu|-1;Sat|-1;Sun|-1;Fry|-1') && ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) && $tipo_actividad == 'PLANIFICADAS') {
                        // dd(permisoingresoplanificacion('Sun|19;Tue|-1;Wed|-1;Tur|-1'));
                        Session::flash('message', 'No está permitido realizar cambios por el momento, Se puede realizar ingresos el día lunes(hasta las 6PM).');
                        return Redirect::to($this->configuraciongeneral[1]);
                    }
                }


                if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) {
                    $verificacion = TeleTeletrabajoModel::join('tele_tmov_teletrabajo_semanas as sema', 'sema.id', '=', 'tele_tmov_teletrabajo.id_semana')
                        ->where(['id_semana' => Input::get('id_semana'), 'tele_tmov_teletrabajo.estado' => 'ACT', 'id_direccion' => Input::get('id_direccion'), 'tipo_actividad' => Input::get('tipo_actividad')])->first();
                } else {
                    $verificacion = TeleTeletrabajoModel::join('tele_tmov_teletrabajo_semanas as sema', 'sema.id', '=', 'tele_tmov_teletrabajo.id_semana')
                        ->where(['id_semana' => Input::get('id_semana'), 'tele_tmov_teletrabajo.estado' => 'ACT', 'id_direccion' => Auth::user()->id_direccion, 'tipo_actividad' => Input::get('tipo_actividad')])->first();
                }
                if ($verificacion) {
                    return Redirect::to("$ruta")
                        ->withErrors(['Ya tiene un registro activo de la semana ' . $verificacion->semana . ' con tipo de actividad  ' . $verificacion->tipo_actividad])
                        ->withInput();
                }
            } else {
                $ruta .= "/" . Crypt::encrypt($id) . "/edit";
                $guardar = TeleTeletrabajoModel::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Compromiso Alcalde modificado";
            }
            $timeline = new TeleTeletrabajoHistorialModel;
            //$input = Input::all();
            $validar = TeleTeletrabajoModel::rules($id);
            $validator = Validator::make($input, $validar);

            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            } else {
                //die("Hola");
                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token" && $key != "archivos") {
                        $guardar->$key = $value;
                        $timeline->$key = $value;
                    }
                }


                if ($guardar->id_direccion == null) {
                    $guardar->id_direccion = Auth::user()->id_direccion;
                    $timeline->id_direccion = $guardar->id_direccion;
                    // $guardar->id_direccion = 1;
                }

                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                $guardar->save();

                /**/
                $idCab = $guardar->id;
                /*Time Line*/
                $timeline->id_semana = $guardar->id_semana;
                $timeline->id_direccion = $guardar->id_direccion;
                $timeline->id_usuario = Auth::user()->id;
                $timeline->ip = \Request::getClientIp();
                $timeline->pc = \Request::getHost();
                $timeline->id_cab_tele_trab = $idCab;
                $timeline->save();
                $idCabHistorial = $timeline->id;
                $archivosjson = array();
                /**/
                $archivos = Input::file('archivos');
                // dd($archivos);
                if (is_array($archivos)) {
                    foreach ($archivos as $key => $arc) {
                        if (is_file($arc)) {
                            $archivo = new TeleTeletrabajoArchivosModel();
                            $dir = public_path() . '/teletrabajo';

                            if (!is_dir($dir)) {
                                mkdir($dir, 0777);
                            }
                            $dir = public_path() . '/teletrabajo/' . $idCab;

                            $fileName = $arc->getClientOriginalName();
                            $ext = $arc->getClientOriginalExtension();
                            $perfiles = ['pdf'];
                            if (!in_array($ext, $perfiles)) {
                                DB::rollback();
                                $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                                return redirect()->back()->withErrors([$mensaje])->withInput();
                            }

                            // dd($fileName);
                            $fileName = date('Y_m_h__H_i') . '_' . $fileName;
                            $fileName = str_replace(' ', '_', $fileName);
                            $fileName = str_replace('#', '_', $fileName);
                            $archivo->archivo = $fileName;
                            $oldfile = $dir . '/' . $fileName;
                            if (is_file($oldfile)) {
                                File::delete($oldfile);
                            }
                            $arc->move($dir, $fileName);

                            $archivo->id_cab_teletrabajo = $idCab;
                            $archivo->save();
                            $archivosjson[] = $archivo->id;
                        }
                    } //Fin For de Archivos
                    /*Grabar IDs de Archivos en Historial*/
                    $timelinefiles = new TeleTeletrabajoArchivosHistorialModel;
                    $timelinefiles->archivo = json_encode($archivosjson);
                    $timelinefiles->id_cab_teletrabajo = $idCabHistorial;
                    $timelinefiles->save();
                }
                // dd($input);


                DB::commit();


                if ($guardar->estado_documentacion == 'CON OBSERVACIONES') {

                    $usuario = UsuariosModel::select('users.*')->join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
                        ->where(["ap.tipo" => 3, "id_direccion" => $guardar->id_direccion, 'users.estado' => 'ACT'])
                        ->whereIn("users.id_perfil", [3, 5, 15, 22, 29, 9])
                        ->get();
                    $verificacion = TeleTeletrabajoModel::join('tele_tmov_teletrabajo_semanas as sema', 'sema.id', '=', 'tele_tmov_teletrabajo.id_semana')
                        ->where(['id_semana' => Input::get('id_semana'), 'tele_tmov_teletrabajo.estado' => 'ACT', 'tele_tmov_teletrabajo.id' => $guardar->id])->first();
                    // dd($usuario);
                    foreach ($usuario as $key => $value) {
                        # code...
                        $this->notificacion->EnviarEmail($value->email, "Teletrabajo con observaciones", "EL teletrabajo de la semana " . $verificacion->semana . " tiene observaciones.", $guardar->observacion_coordinacion, $this->configuraciongeneral[1] . '/' . Crypt::encrypt($guardar->id) . '/edit', "vistas.emails.email");
                        $this->notificacion->notificacionesweb("EL teletrabajo de la semana <b>" . $verificacion->semana . "</b> tiene observaciones.", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($guardar->id) . '/edit', $value->id, "2c438f");
                    }
                }
                Session::flash('message', $msg);
                $redir = $this->configuraciongeneral[1] . '?entidad=' . $guardar->id_direccion;;
                return Redirect::to($redir);
            }
        } catch (\Exception $e) {
            // ROLLBACK
            DB::rollback();
            $mensaje = $e->getMessage() . ' ' . $e->getLine();
            return redirect()->back()->withErrors([$mensaje])->withInput();
        }
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return $this->formularioscrear("");
        //return view('coordinacioncronograma::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        // $id = intval(Crypt::decrypt($id));
        // $objetos = $this->verpermisos(json_decode($this->objetos));
        // $tabla = $objetos[1]->where("plan_estra_archivos.id", $id)->first();
        // $this->configuraciongeneral[3] = "11"; //Tipo de Referencia de Archivo
        // $this->configuraciongeneral[4] = "Si";
        // $dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "11"]])->get();
        // $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "11"]])->get();
        // //$this->configuraciongeneral[0].="<p>".$tabla->direccion."</p>";
        // $this->configuraciongeneral[0] = "<p>" . $tabla->direccion . "</p>";
        // $objetos = $objetos[0];
        // unset($objetos[0]);
        // return view('vistas.show', [
        //     "objetos" => $objetos,
        //     "tabla" => $tabla,
        //     "configuraciongeneral" => $this->configuraciongeneral,
        //     'dicecionesImagenes' => $dicecionesImagenes,
        //     'dicecionesDocumentos' => $dicecionesDocumentos
        //     //"timeline" => $this->construyetimeline($id)
        // ]);
        //return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
        //return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        try {
            //code...

            // $id = Input::get('id');
            // dd($intpu);
            $tabla = TeleTeletrabajoModel::find($id);
            $ruta = public_path() . '/teletrabajo/' . $tabla->id_cab_teletrabajo;
            $direccion = $tabla->id_direccion;
            if (file_exists($ruta)) {
                // unlink($ruta);
                // rmdir($ruta);
            }
            $tabla->estado = 'INA';
            $tabla->save();
            $redir = $this->configuraciongeneral[1] . '?entidad=' . $direccion;
            return Redirect::to($redir);
        } catch (\Throwable $th) {
            // DB::rollback();
            $mensaje = $th->getMessage() . ' ' . $th->getLine();
            return redirect()->back()->withErrors([$mensaje])->withInput();
        }
    }
    public function deleteArchivoTeletrabajo()
    {
        try {
            //code...

            $id = Input::get('id');
            $tabla = TeleTeletrabajoArchivosModel::find($id);
            $ruta = public_path() . '/teletrabajo/' . $tabla->id_cab_teletrabajo . '/' . $tabla->archivo;
            if (file_exists($ruta)) {
                unlink($ruta);
            }
            $tabla->delete();
            return ['tipo' => true, 'mensaje' => 'Archivo eliminado'];
        } catch (\Throwable $th) {
            //throw $th;
            return ['tipo' => false, 'mensaje' => 'El archivo no pudo ser eliminado'];
        }
    }
}
