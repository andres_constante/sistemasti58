<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;
use Modules\CoordinacionCronograma\Entities\CoorIndicadorTipoModel;
/*Resusar Codigo*/
use App\Http\Controllers\WebApiController;
class CoorIndicadorTipoController extends Controller
{
    var $configuraciongeneral = array("Indicadores", "coordinacioncronograma/indicadoresmain", "index", 6 => "coordinacioncronograma/indicadoresmainajax", 7 => "indicadoresmain");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Tipo Indicador","Nombre":"tipo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Detalle","Nombre":"detalle","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Icono","Nombre":"icono_app","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tipo Gráfico","Nombre":"tipografico","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Medida","Nombre":"medida","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Asignar Color","Nombre":"color","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Fuente","Nombre":"fuente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Orden","Nombre":"orden","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
//https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "tipo" => "tipo: {
                            required: true
                        }",
        "detalle" => "detalle: {
                            required: true
                        }"
    );
    protected $webapi;
    public function __construct(WebApiController $webapi)
    {
        $this->middleware('auth');
        $this->webapi=$webapi;
    }
    public function estadisticaindicadoresget()
    {
        $resul=$this->webapi->getindicadoresmain();
        //dd($resul);
        $res=array();
        $this->configuraciongeneral[0]="Indicadores Territoriales";
        foreach ($resul as $key =>$value)
        {
            //show($value,0);
            $parro=$value["detalletotales"];
            $parroquia=array();
            $valores=array();
            foreach ($parro as $keya => $valuea)
            {
                $parroquia[]="'".$valuea["parroquia"]."'";
                //show($categoria,0);
                foreach ($valuea["indicador"] as $keyi =>$valuei)
                {
                    if(!array_key_exists($valuei["rubro"],$valores))
                        $valores[$valuei["rubro"]]=$valuei["valor"];
                    else
                        $valores[$valuei["rubro"]]=$valores[$valuei["rubro"]].",".$valuei["valor"];
                    //show($valores,0);
                }

            }
            $arson="";
            foreach ($valores as $keyt => $valt)
            {
                $arson.="{ name: '".$keyt."',\ndata: [".$valt."] },\n";
            }
            $res[]=array(
                "id"=>$value["id"],
                "detalle"=>$value["detalle"],
                "sumatotal"=>$value["sumatotal"],
                "icono_app"=>$value["icono_app"],
                "tipografico"=>$value["tipografico"],
                "medida"=>$value["medida"],
                "color"=>$value["color"],
                "fuente"=>$value["fuente"],
                "parroquias"=>implode(",",$parroquia),
                "valores"=>$arson
            );
            //$categoria=implode(",",$categoria);
            //show($valores);
            //$resul[$key]["categoria"]=$value["detalletotales"];
        }
        //show($res);
        return view('vistas.indicadoreschart', ["res" => $res,"configuraciongeneral" => $this->configuraciongeneral]);
    }

    public function verpermisos($objetos = array(), $tipo = "index")
    {
        //show($objetos);
        $tabla = CoorIndicadorTipoModel::where("estado", "ACT")->orderby("id","desc");
        return array($objetos, $tabla);
    }

    public function indicadoresmainajax(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        $objetos = $tabla[0];
        $tabla = $tabla[1];
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('id', 'LIKE', "%{$search}%")
                    ->orWhere('tipo', 'LIKE', "%{$search}%")
                    ->orWhere('detalle', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();

        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {


                $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;' .
                    link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o')) . '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;

            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = $this->verpermisos(json_decode($this->objetos));
        return view('vistas.index', [
            "objetos" => $objetos[0],
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si"
        ]);
        //return view('coordinacioncronograma::index');
    }
    public function formularioscrear($id)
    {
        $objetos = $this->verpermisos(json_decode($this->objetos), "crear");
        $tabla=$objetos[1];
        $objetos=$objetos[0];
        /**/
        $tipografico = explodewords(ConfigSystem("tipograficoestadistico"), "|");
        $objetos[3]->Valor=$tipografico;
        $objetos[5]->Valor =array("false"=>"false","true"=>"true");
        /**/
        if($id=="") {
            $this->configuraciongeneral[2] = "crear";
            $datos=[
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            ];
        }else{
            $tabla = $tabla->where("id", $id)->first();
            $this->configuraciongeneral[2] = "editar";
            $datos=[
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla
            ];
        }
        return view('vistas.create',$datos);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return $this->formularioscrear("");
    }
    public function guardar($id)
    {
        $input = Input::all();

        $ruta = $this->configuraciongeneral[1];

        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new CoorIndicadorTipoModel;
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro Variable de Configuración";
        } else {
            $ruta .= "/$id/edit";
            $guardar = CoorIndicadorTipoModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Edición Variable de Configuración";
        }

        $input = Input::all();
        $arrapas = array();

        $validator = Validator::make($input, CoorIndicadorTipoModel::rules($id));

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            foreach ($input as $key => $value) {

                if ($key != "_method" && $key != "_token") {
                    $guardar->$key = $value;
                }
            }
            $guardar->id_usuario = Auth::user()->id;
            $guardar->ip = \Request::getClientIp();
            $guardar->pc = \Request::getHost();
            $guardar->save();
            /**/

            Auditoria($msgauditoria . " - ID: " . $id . "-" . Input::get($guardar->tipo));
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("id", $id)->first();

        $this->configuraciongeneral[3] = "5";//Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "si";
        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        return view('vistas.show', [
            "objetos" => $objetos[0],
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $id = intval(Crypt::decrypt($id));
        return $this->formularioscrear($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $tabla = CoorIndicadorTipoModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
