<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Yajra\DataTables\Facades\DataTables;
use GuzzleHttp\Client as GuzzleHttpClient;
use App\Http\Controllers\ReportesJasperController;
use App\UsuariosModel;
use Auth;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;

class ReporteincidenciaController extends Controller
{
    var $escoja = array(0 => "TODOS");
    var $formatos = array("pdf" => "PDF", "xls" => "XLS");
    var $objects = '[
        {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "idtipo_entidad", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Tipo incidencia", "Nombre": "id_tipo_denuncia", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "fecha_ini", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "fecha_fin", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
        {"Tipo": "select", "Descripcion": "Responsable", "Nombre": "id_responsable", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
        {"Tipo": "select-multiple", "Descripcion": "Estado", "Nombre": "estado", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},				
        {"Tipo":"select","Descripcion":"Parroquia","Nombre":"parroquia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Condición","Nombre":"condicion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Delegados","Nombre":"delegado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';
    var $reportes = array(
        null => "Escoja el reporte",
        "coordinacioncronograma/resportes_estadisticos" => "REPORTES ESTADÍSTICOS",
        "coordinacioncronograma/reporteincidencia" => "REPORTE DE INCIDENCIAS TERRITORIALES",
        "reportescoordinacionactividades" => "REPORTE DE DISPOSICIONES GENERALES",
        "reportescompromisos" => "REPORTE COMPROMISOS TERRITORIALES",
        "reportespoa" => "REPORTE POA",
        "reportespoacertificaciones" => "REPORTE CERTIFICACIONES POA",
        "reportescoordinacionobras" => "REPORTE DE OBRAS",
        "reportesrecomendaciones" => "REPORTE RECOMENDACIONES DE CONTRALORIA",
        "reportesplanpropuesta" => "REPORTE PLAN DE PROPUESTA",
        "reportesindicadores" => "REPORTE INDICADORES DE GESTION",
        "coordinacioncronograma/coordinacionlista" => "Coordinación Actividades - Búsqueda",
        "coordinacioncronograma/coordinacionlistaobras" => "Coordinación Actividades - Búsqueda"
    );
    var $reportes_objeto = '[
            {"Tipo": "select", "Descripcion": "Tipo de reporte", "Nombre": "reporte", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null","Ancho":12}    
        ]';
    public function __construct(ReportesJasperController $ReportJasper)
    {
        $this->middleware('auth', ['except' => ['reportescoordinacionactividades', 'reporteincidencias']]);
        $this->ReportJasper = $ReportJasper;
    }


    public function reporteincidencias()
    {

        // if (Input::get("token"))
        return $this->generarreporte();
    }

    public function generarreporte()
    {
        // dd();
        $input = Input::all();
        // show($input);      
        $data = json_decode(Input::get("data"));
        //$namedata=Crypt::decrypt(Input::get("namedata"));
        $paramadi = Crypt::decrypt(Input::get("paramadi"));
        // show(array($paramadi,$data));        
        $paramsok = array();
        foreach ($data as $key => $value) {
            # code...
            //show($value);
            if ($value->name != "_token" && $value->name != "formato" && $value->name != "namereporte" && $value->name != "tituloreporte" && $value->name != "tiporeporte")
                $paramsok[$value->name] = $value->value;
            elseif ($value->name == "formato")
                $formato = $value->value;
            elseif ($value->name == "namereporte")
                $namedata = $value->value;
            elseif ($value->name == "tituloreporte")
                $paramadi["TITULO"] = mb_strtoupper($value->value);
            elseif ($value->name == "tiporeporte")
                if ($value->value == "informe" || $value->value == "observa")
                    $namedata .= "_" . $value->value;
        }
        // Filtros
        //show($paramsok);
        // dd();
        // show($paramadi);
        //$name, $params, $format, $filename, $download, $download_is_attachment = true
        $filename = "Actividades-" . str_random(6);
        //  show(array($namedata,$paramsok+$paramadi,$formato,$filename,true,false));
        //  show($namedata);
        $ruta = '';

        $param =  $paramsok + $paramadi;
        // if($param['idtipo_entidad'] != 0)
        //     $namedata = 'rpt_reporte_incidencias_agrupado_by_entidad';

        // show($namedata);
        foreach ($param as $key => $value) {
            # code...
            $ruta .= $key . '=' . $value . '&';
        }
        // dd($ruta);
        return $this->ReportJasper->generateReportByJson($namedata, $ruta, $formato, $filename, true, false);
    }

    public function getEntidad($cedula = null)
    {
        $client = new GuzzleHttpClient();
        // $resp_cliente = $client->request('GET', 'http://localhost:8091/mantaentusmanos/public/getEntidades/1?cedula=' . $cedula);
        $resp_cliente = $client->request('GET', 'https://portalciudadano.manta.gob.ec/getEntidades/1?cedula='.$cedula);
        $respuesta = json_decode($resp_cliente->getBody()->getContents());
        $array_resp = collect($respuesta)->pluck('nombre_entidad', 'id');
        $array_resp->put(0, 'TODOS');
        $sort = $array_resp->sortKeys();
        $sort->all();
        // $array_resp->all();
        return $sort;
    }


    public function conusultatipoincidenciabyidentidad($id)
    {
        $input = Input::all();
        // dd();
        // dd((($id!=0)?2:$id));
        $client = new GuzzleHttpClient();
        $resp_cliente = $client->request('GET', 'https://portalciudadano.manta.gob.ec/conusultatipoincidenciabyidentidad?id='.$id);
        // $resp_cliente = $client->request('GET', 'http://localhost:8091/mantaentusmanos/public/conusultatipoincidenciabyidentidad?id='.(($id!=0)?2:$id));
        // $resp_cliente = $client->request('GET', 'http://localhost/mantaentusmanos/public/conusultatipoincidenciabyidentidad?id=2');
        $respuesta = json_decode($resp_cliente->getBody()->getContents());
        $array_resp = collect($respuesta)->pluck('nombre_denuncia', 'id');
        $array_resp->put(0, 'TODOS');
        $sort = $array_resp->sortKeys();
        $sort->all();
        // show($sort);
        // $array_resp->all();
        return $sort;
    }


    public function getResponsableEntidad()
    {
        $client = new GuzzleHttpClient();
        $resp_cliente = $client->request('GET', 'https://portalciudadano.manta.gob.ec/responsablesConEntidadAndEntidad');
        $respuesta = json_decode($resp_cliente->getBody()->getContents());
        $array_resp = collect($respuesta)->pluck('responsable', 'id');
        $array_resp->put(0, 'TODOS');
        $sort = $array_resp->sortKeys();
        $sort->all();
        // $array_resp->all();
        return $sort;
    }

    public function getIncidenciasByIdEntidad($id)
    {
        $id = Input::get("entidad");
        $client = new GuzzleHttpClient();
        $resp_cliente = $client->request('GET', 'https://portalciudadano.manta.gob.ec/contar_incidencias?id_entidad=' . $id);
        $respuesta = json_decode($resp_cliente->getBody()->getContents());
        return $respuesta;
    }


    public function getDelegadosByDireccion()
    {
        $id = Input::get("entidad");
        // dd($id);
        if (request()->ajax()) {
            $url = config('app.env') == 'local' ? 'http://mantaentusmanos.test' : 'https://portalciudadano.manta.gob.ec';
            $client = new GuzzleHttpClient();
            $resp_cliente = $client->request('GET', $url.'/getDelegadosByDireccion?entidad=' . $id);
            $respuesta = json_decode($resp_cliente->getBody()->getContents());
            // dd($respuesta);
            if ($respuesta->data == true) {
                # code...
                return ["estado" => true, "data" => $respuesta->data];
            } 

            return ["estado" => false];

        } else abort(404);
    }

    public function ReporteincidenciaControllerajax()
    {
        $input = Input::all();

        $estado =  !isset($input['estado']) ? [0 => "0"] : $input['estado'];
        $parroquia = !isset($input['parroquia']) ? "" : $input['parroquia'];
        $delegado = !isset($input['delegado']) ? "0" : $input['delegado'];

        // dd($input);
        $client = new GuzzleHttpClient();
        $url = config('app.env') == 'local' ? 'http://mantaentusmanos.test' : 'https://portalciudadano.manta.gob.ec';
        // $resp_cliente = $client->request('GET', 'https://portalciudadano.manta.gob.ec/consultaReportes?fecha_ini='.$input['fecha_ini'].'&fecha_fin='.$input['fecha_fin'].'&id_tipo_denuncia='.$input['id_tipo_denuncia'].'&estado='.$input['estado']);
        $resp_cliente = $client->request('GET', $url.'/consultaReportes?fecha_ini=' . $input['fecha_ini'] . '&fecha_fin=' . $input['fecha_fin'] . '&idtipo_entidad=' . $input['idtipo_entidad'] . '&id_usuario_responsable=' . $input['id_responsable'] . '&estado=' . implode(',', $estado)   . '&id_tipo_denuncia=' . $input['id_tipo_denuncia'] . '&parroquia=' . $parroquia . '&condicion=' . $input['condicion'] . '&delegado=' .$delegado);
        $tabla = json_decode($resp_cliente->getBody()->getContents());

        // show($url.'/consultaReportes?fecha_ini=' . $input['fecha_ini'] . '&fecha_fin=' . $input['fecha_fin'] . '&idtipo_entidad=' . $input['idtipo_entidad'] . '&id_usuario_responsable=' . $input['id_responsable'] . '&estado=' . implode(',', $estado)   . '&id_tipo_denuncia=' . $input['id_tipo_denuncia'] . '&parroquia=' . $parroquia . '&condicion=' . $input['condicion'] . '&delegado=' .$delegado);
        // dd($tabla);
        $objetos = '[
		{"Tipo":"text","Descripcion":"Tipo incidencia","Nombre":"nombre_denuncia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Direccion","Nombre":"nombre_entidad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Detalle","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Denunciante","Nombre":"usuario_denunciante","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Telefono","Nombre":"telefono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Dirección de la denuncia","Nombre":"direccion_denuncia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Parroquia","Nombre":"parroquia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },			
		{"Tipo":"text","Descripcion":"Responsable","Nombre":"responsable_entidad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Código","Nombre":"codigo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Estado","Nombre":"estado_denuncia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Fecha de inicio","Nombre":"created_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Plazo de respuesta","Nombre":"fecha_solucion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },		
		{"Tipo":"text","Descripcion":"Fecha de culminación","Nombre":"tiempo_respuesta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Tiempo transcurrido","Nombre":"tiempo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Estado de ejecución","Nombre":"resultado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }			
    ]';

        $objetos = json_decode($objetos);
        $configuraciongeneral = ['Reporte de incidencias territoriales', 'coordinacioncronograma/reporteincidencia'];


        return view('vistas.reporteshtml.rendimientoactividades', [
            'edit' => 'no',
            "tabla" => $tabla->datos,
            "objetos" => $objetos,
            "configuraciongeneral" => $configuraciongeneral,
            "incidencias" => 'si'
        ]);
    }


    public function ajax_repors_indicencias(Request $request){
        $client = new GuzzleHttpClient();
        $resp_cliente = $client->request('GET', 'https://portalciudadano.manta.gob.ec/getIndicencias_Apiws?id_direccion='.$request->id_direccion.'&fecha_ini='.$request->fecha_ini.'&fecha_fin='.$request->fecha_fin);

        $respuesta = json_decode($resp_cliente->getBody()->getContents());
        return response()->json($respuesta[0]);
        // return DataTables::of($respuesta[0])->addIndexColumn()->make(true);
     }

    public function repors_indicencias(){
        // $client = new GuzzleHttpClient();
        // $resp_cliente = $client->request('POST', 'https://portalciudadano.manta.gob.ec/exportar_entidades');
        // $respuesta = json_decode($resp_cliente->getBody()->getContents());
        
        // $arraysito = array();
        // foreach ($respuesta as  $d) {
        //     array_push($arraysito, [ $d->nombre_entidad => $d->nombre_entidad ]);
        // }
        
        // return $arraysito;



        // $estados = [
        //     'POR ATENDER' => 'POR ATENDER',
        //     'ATENDIDO' => 'ATENDIDO',
        //     'NO ASISTIO' => 'NO ASISTIO',
        //     'PRESENTE' => 'PRESENTE',
        //     'ANULADO' => 'ANULADO',
        //     'CADUCADO' => 'CADUCADO'
        // ];
        //  return $estados;


        $general_config = ['REPORTE DE INCIDENCIAS TERRITORIALES '];
        return view('reports.repors_indicencias', ['general_config' => $general_config]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $validate_js = [];
        $general_config = ['REPORTE DE INCIDENCIAS TERRITORIALES', 'coordinacioncronograma/reporteincidencias', 'ReporteincidenciaControllerajax'];
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "
        <script type='text/javascript'>        
        function titurepor()
        {
            var entidad=$('#idtipo_entidad').find('option:selected').text()+':';
            var estado =  $('#estado').val();
            var fecha_ini = $('#fecha_ini').val();
            var fecha_fin = $('#fecha_fin').val();
            var id_responsable = $('#id_responsable').val();
            
            $('#tituloreporte').val('fdgfhgf');
            
        }             
          $(document).ready(function() {
                 $('#tituloreporte').val('fdgfhgf');
            $('#namereporte').change(function(event) {                
                return titurepor();
            });
            $('#namereporte').trigger('change');
          });
        </script>
        ";

        $formatosjs = array(
            //"rpt_rendimiento_actividad_noagrupado" => '{"html":"Tabla HTML","pdf":"PDF","xls":"XLS"}',
            "rpt_reporte_incidencias" => '{"pdf":"PDF","xls":"XLS"}',
            //"rpt_rendimiento_direccion" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_actividad_sin_avance" => '{"pdf":"PDF","xls":"XLS"}',
            //"rpt_direccion_sin_avance_detallado" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_actividad_vencidas" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_actividad_cien" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_graficos" => '{"graficos":"Gráficos"}'
            //,"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            //"rpt_rendimiento_actividad_noagrupado" => '{"completo":"Completo","informe":"Informe"}',
            //"rpt_reporte_incidencias" => '{"completo":"Completo","informe":"Informe","observa":"Informe con Observación"}',
            "rpt_reporte_incidencias_agrupado_entidad" => '{"pdf":"PDF"}',
            "rpt_reporte_incidencias" => '{"pdf":"PDF"}',
            //"rpt_rendimiento_direccion" => '{"pdf":"Completo"}',
            "rpt_rendimiento_actividad_sin_avance" => '{"completo":"Completo"}',
            //"rpt_direccion_sin_avance_detallado" => '{"completo":"Completo","informe":"Informe"}',
            "rpt_rendimiento_actividad_vencidas" => '{"completo":"Completo"}',
            "rpt_rendimiento_actividad_cien" => '{"completo":"Completo"}',
            "rpt_graficos" => '{"informe":"Informe"}'
        );

        $namerepor = array(
            "rpt_reporte_incidencias_agrupado_entidad" => "Reporte de incidencias detallado",
            "rpt_reporte_incidencias" => "Reporte general de incidencias",
            "rpt_reporte_incidencias_rendimiento" => "Reporte rendimiento de delegados",
        );
        $formatosjs = array(
            "rpt_reporte_incidencias_agrupado_entidad" => '{"html":"Tabla HTML","pdf":"PDF","xls":"XLS"}',
            "rpt_reporte_incidencias" => '{"html":"Tabla HTML","pdf":"PDF","xls":"XLS"}',
            "rpt_reporte_incidencias_rendimiento" => '{"html":"Tabla HTML","pdf":"PDF","xls":"XLS"}',

        );
        $tiporepor = array(
            "rpt_reporte_incidencias_agrupado_entidad" => '{"pdf":"PDF"}',
            "rpt_reporte_incidencias" => '{"pdf":"PDF"}',
            "rpt_reporte_incidencias_rendimiento" => '{"pdf":"PDF"}'
        );
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "users.id_perfil")->where("users.id", Auth::user()->id)->first();



        $fecha_ini = date('Y') . "-01-01";
        $fecha_fin = date('Y') . "-12-31";

        $objeto = json_decode($this->objects);
        // dd($this->getEntidad());
        if ($id_tipo_pefil->tipo == 3) {
            
            $respuesta=$this->getEntidad(Auth::user()->cedula);
            if($respuesta[0]=='TODOS'){
                unset($respuesta[0]);
            }
            unset($namerepor['rpt_reporte_incidencias_agrupado_entidad']);
            unset($formatosjs['rpt_reporte_incidencias_agrupado_entidad']);
            unset($tiporepor['rpt_reporte_incidencias_agrupado_entidad']);
            $respuesta_2=$this->conusultatipoincidenciabyidentidad((isset($respuesta[1])?$respuesta[1]:"null"));
            // dd($respuesta_2);
        } else {
            $respuesta=$this->getEntidad();
            $respuesta_2=$this->conusultatipoincidenciabyidentidad(2);
        }
        
        $objeto[0]->Valor = $respuesta;
        $objeto[1]->Valor = $respuesta_2;
        $objeto[2]->Valor = $fecha_ini;
        $objeto[3]->Valor = $fecha_fin;
        $objeto[4]->Valor = $namerepor;
        // $objeto[4]->Valor = ["html" => "Tabla HTML"] + $this->formatos;
        $objeto[5]->Valor = ["html" => "Tabla HTML"] + $this->formatos;
        $objeto[6]->Valor = ["pdf" => "PDF"];
        $objeto[7]->Valor = $this->getResponsableEntidad();
        $objeto[8]->Valor = $this->escoja + ["REVISIÓN" => "REVISIÓN", "EN PROCESO" => "EN PROCESO", "REPROGRAMADO" => "REPROGRAMADO", "FINALIZADO" => "FINALIZADO", "CIERRE CON OBSERVACIONES" => "CIERRE CON OBSERVACIONES"];
        $objeto[10]->Valor = ["" => "TODOS"] + parroquiaModel::where('estado', 'ACT')->wherenotin('id', [8, 9])->pluck('parroquia', 'parroquia')->all();
        $objeto[11]->Valor = ["" => "TODOS", "A TIEMPO" => "A TIEMPO", "RETRASADA" => "RETRASADA", "CULMINADA" => "CULMINADA"];
        $objeto[12]->Valor = ["" => "TODOS"];
        // unset($objeto[10]);
        $funcalljs = "titurepor();";
        // show($funcalljs);
        //show($objetos);
        $configuraciongeneral = ['Reporte de incidencias territoriales', 'coordinacioncronograma/reporteincidencia'];
        //  $objetos_reportes=json_decode($this->reportes_objeto);
        //  $objetos_reportes[0]->Valor=$this->reportes;
        //  $objeto=insertarinarray($objeto,$objetos_reportes[0],0);

        // show($objeto[0]);
        return view('reports.create', [
            'objects' => $objeto,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor,
            "funcalljs" => $funcalljs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('coordinacioncronograma::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        // return view('vistas.show');
        // https://portalciudadano.manta.gob.ec/incidenciasmanta/incidenciasrevision/3
        $client = new GuzzleHttpClient();
        $resp_cliente = $client->request('GET', 'https://portalciudadano.manta.gob.ec/incidenciasmanta/verdenuncia/' . $id);
        $respuesta = $resp_cliente->getBody()->getContents();

        // $array_resp->all();
        return $respuesta;
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
