<?php

namespace Modules\Coordinacioncronograma\Http\Controllers;

use App\Http\Controllers\NotificacionesController;
use App\Http\Controllers\WebApiAgendaController;
use Illuminate\Routing\Controller;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Map;
use Modules\ComunicacionAlcaldia\Entities\AgendaEstadosModel;
use Modules\ComunicacionAlcaldia\Entities\AgendaNotificacionesPersonalizadas;
use Modules\ComunicacionAlcaldia\Entities\AgendaVirtualModel;
use Modules\ComunicacionAlcaldia\Entities\AgendaVirtualModelDeta;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\ComunicacionAlcaldia\Entities\TipoAgendaModel;
use Modules\CoordinacionCronograma\Entities\AgendaArchivosModel;
use stdClass;

class AgendaVirtualController extends Controller
{
    var $configuraciongeneral = array("Agenda Virtual", "coordinacioncronograma/agendavirtual", "index", 6 => "coordinacioncronograma/agendavirtualajax", 7 => "agendavirtual");
    var $escoja = array(null => "Escoja opción...");

    var $objetos = '[
        {"Tipo":"select","Descripcion":"Tipo de actividad(*)","Nombre":"id_tipo","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Actividad(*)","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Descripción de la actividad(*)","Nombre":"descripcion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Parroquia","Nombre":"parroquia_id","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Barrio","Nombre":"barrio_id","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Dirección de la actividad(*)","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Lugar referencia","Nombre":"detalle_direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"mapa","Descripcion":"MAPA(*)","Nombre":"mapa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetimetext","Descripcion":"Fecha y hora de inicio(*)","Nombre":"fecha_inicio","Clase":"consultar_fecha","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetimetext","Descripcion":"Fecha y hora de fin(*)","Nombre":"fecha_fin","Clase":"consultar_fecha","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file-multiple","Descripcion":"Adjunto","Nombre":"adjunto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Latitud","Nombre":"latitud","Clase":"disabled hidden","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Longitud","Nombre":"longitud","Clase":"disabled hidden","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Notificaciones","Nombre":"notificaciones","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"checkbox","Descripcion":"Solo notificar a","Nombre":"otros","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Solo notificar a","Nombre":"id_usuario_masivo","Clase":"chosen-select disabled","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"checkbox","Descripcion":"Delegar a","Nombre":"delegaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Delegar a","Nombre":"delegacion_alcalde","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Mensaje para delegados","Nombre":"disposicion_alcalde","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado","Nombre":"id_estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Confirmación de asistencia del Sr. Alcalde","Nombre":"confirmar_asistencia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Asistencia Sr. Alcalde","Nombre":"asistencia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
		]';

    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "nombre" => "nombre: {
			required: true
		}"
    );

    const LUNES = 1;
    const MARTES = 2;
    const MIERCOLES = 3;
    const JUEVES = 4;
    const VIERNES = 5;
    const SABADO = 6;
    const DOMINGO = 7;
    const MIN_EVENTO = 1;
    const MAX_EVENTO = 23;
    const DURACION_ENTRE_EVENTOS = 15;
    const DURACION_MINIMA_EVENTOS = 15;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */



    public function agendavirtualajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'nombre',
            2 => 'descripcion',
            3 => 'direccion',
            4 => 'fecha_inicio',
            5 => 'fecha_fin',
            6 => 'estado_aprobacion',
            7 => 'observacion',
            8 => 'observacion_cancelacion',
            9 => 'tipo_agenda',
            10 => 'estado',
            11 => 'confirmar_asistencia',
            12 => 'asistencia',
            13 => 'adjunto',
            14 => 'acciones'
        );

        // dd($request->fecha_inicio_f);
        $fecha_inicio = $request->fecha_inicio_f;
        $fecha_fin = $request->fecha_fin_f;
        $estado = $request->estado_aprobacion;
        $confirmacion = $request->confirmacion;
        $carbon_fecha = new \Carbon\Carbon();
        if ($fecha_inicio == '') {
            $fecha_inicio = '2019-01-01 00:00:00';
        } else {
            $fecha_inicio =  $fecha_inicio . ' 00:00:00';
        }


        if ($fecha_fin == '') {
            $fecha_fin = $carbon_fecha->addYear();
        } else {
            $fecha_fin = $fecha_fin . ' 23:59:59';
        }
        // dd($estado);
        $query = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
            ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
            ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo as id_tipo')
            ->where(['com_tmov_comunicacion_agenda.estado' => 'ACT'])
            ->wherenotin('com_tmov_comunicacion_agenda.id_estado',[6])
            ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin]);


        if ($estado != null && $estado != '0') {
            // dd($estado );
            $query = $query->where(['id_estado' => $estado]);
        }
        if ($confirmacion != null) {
            // dd($estado );
            $query = $query->where(['confirmar_asistencia' => $confirmacion]);
        }




        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "users.id_perfil")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo != 8 && $id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4) {
            // $query = $query->where(['id_usuario' => Auth::user()->id]);
            ///

            $id = [];
            $query_delegadas = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo as id_tipo')
                ->wherenotin('com_tmov_comunicacion_agenda.id_estado',[6])
                ->where(['com_tmov_comunicacion_agenda.estado' => 'ACT'])
                ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin])->get();
            foreach ($query_delegadas as $kk => $vv) {
                # code...
                if ($vv->delegacion_alcalde != null) {
                    $responsables = DB::select('select id as delegacion_alcalde from users where id in (' . $vv->delegacion_alcalde . ')');
                    foreach ($responsables as $kkk => $vvv) {

                        # code...
                        // dd(Auth::user()->id); 
                        // dd($responsables);
                        if (Auth::user()->id == $vvv->delegacion_alcalde) {
                            // dd($responsables);
                            array_push($id, $vv->id);
                        }
                    }
                }
            }



            $guardarres = AgendaNotificacionesPersonalizadas::where("id_usuario", Auth::user()->id)
                ->get();
            // dd($guardarres);
            foreach ($guardarres as $key => $value) {
                # code...
                array_push($id, $value->id_agenda);
            }

            $query = $query->whereIn('com_tmov_comunicacion_agenda.id', $id);
            // dd($query->get());
            // id_agenda

            if ($id_tipo_pefil->id_perfil == 10) {
                $query = $query->orwhere('coordinadores', 1)->where('tipo_agenda', 0);
            } elseif ($id_tipo_pefil->id_perfil == 25) {
                // $misconcejales=[];
                $misconcejales = explode("|", ConfigSystem("misconcejales"));
                if (is_array($misconcejales) && in_array($id_tipo_pefil->id, $misconcejales)) {
                    $query = $query->where('tipo_agenda', 0)
                        ->where(function ($query) {
                            $query
                                ->orwhere('concejales', 1)
                                ->orwhere('misconcejales', 1);
                        });
                    // Log::info()
                } else {
                    $query = $query->where('concejales', 1)->where('tipo_agenda', 0);
                }
            } elseif ($id_tipo_pefil->id_perfil == 3 || $id_tipo_pefil->id_perfil == 15 || $id_tipo_pefil->id_perfil == 22 || $id_tipo_pefil->id_perfil == 9) {
                $query = $query->orwhere('directores', 1)->where('tipo_agenda', 0);
                // dd($eventos);
            }
        }
        // dd($query);

        $totalData = $query->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value')) && empty($request->input('campos'))) {
            $posts =  $query

                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            // $search = $request->input('search.value');
            $search = $request->input('campos');

            $posts =   $query

                ->where(function ($query) use ($search) {
                    $query->where('com_tmov_comunicacion_agenda.id', 'LIKE', "%{$search}%")
                        ->orWhere('com_tmov_comunicacion_agenda.nombre', 'LIKE', "%{$search}%")
                        ->orWhere('direccion', 'LIKE', "%{$search}%")
                        ->orWhere('responsable', 'LIKE', "%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered =  $query
                ->where(function ($query) use ($search) {
                    $query->where('com_tmov_comunicacion_agenda.id', 'LIKE', "%{$search}%")
                        ->orWhere('com_tmov_comunicacion_agenda.nombre', 'LIKE', "%{$search}%")
                        ->orWhere('direccion', 'LIKE', "%{$search}%")
                        ->orWhere('responsable', 'LIKE', "%{$search}%");
                })
                ->count();
        }

        $data = array();
        // dd($posts);
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {

                $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

                $delete = '';
                $create = '';
                $edit = '';
                $acciones = '&nbsp;&nbsp;' . link_to_route('' . $this->configuraciongeneral[7] . '.show', ' VER', array($post->id), array('class' => 'fa fa-newspaper-o divpopup btn btn-primary', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;';
                // $edit .= link_to_route('' . $this->configuraciongeneral[7] . '.edit', '', array($post->id), array('class' => 'fa fa-pencil-square-o'));
                $edit = link_to_route('' . $this->configuraciongeneral[7] . '.edit', ' EDITAR', array($post->id), array('class' => 'fa fa-pencil-square-o btn btn-light', 'style' => 'color: #fff;background-color: #17a2b8;border-color: #17a2b8; width: 150px;'));
                $reprogramar = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-calendar btn btn-warning" style="width: 150px;" onclick="atender(\'' . Crypt::encrypt($post->id) . '\',4,\'aaa\',\'' . $post->fecha_inicio . '\',\'' . $post->fecha_fin . '\')" > REPROGRAMAR</a>';
                $suspender = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-times btn btn-danger" style="width: 150px;" onclick="atender(\'' . Crypt::encrypt($post->id) . '\',3,\'aaa\')" > SUSPENDER</a>';
                $delegar = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-users btn btn-info"  style="width: 150px;" onclick="atender(\'' . Crypt::encrypt($post->id) . '\',7,\'aaa\')" > DELEGAR</a>';
                $alcaldia = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-users btn btn-info" style="width: 150px; font-size: x-small;" onclick="atender(\'' . Crypt::encrypt($post->id) . '\',6,\'aaa\')" > NOTIFICAR A ALCALDÍA</a>';

                $edit = $edit  . "&nbsp;&nbsp;" . $reprogramar . $delegar . $suspender;
                $asistir = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-check-circle btn btn-success" style="width: 150px;" onclick="atender(\'' . Crypt::encrypt($post->id) . '\',8,\'aaa\')" > ASISTIR</a>';
                $acompañar = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-user-secret btn btn-light" style="width: 150px; background-color: #697b42; color: white;"onclick="atender(\'' . Crypt::encrypt($post->id) . '\',9,\'aaa\')" > INTEGRARSE</a>';
                $confirmar = '&nbsp;&nbsp;<a id="aprobar" class="fa  fa-check-circle-o btn btn-secondary" style="  width: 150px;  background-color: #a04495; color: white; font-size: x-small;" onclick="atender(\'' . Crypt::encrypt($post->id) . '\',10,\'aaa\')" > ' . (($post->confirmar_asistencia == 1) ? 'RETIFICAR' : 'CONFIRMAR') . ' ASISTENCIA</a>';
                $confirmar_asistencia_finalizada = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-flag-checkered btn btn-secondary" style="   width: 150px; background-color: #5544a0; color: white;" onclick="atender(\'' . Crypt::encrypt($post->id) . '\',12,\'aaa\')" > FINALIZAR</a>';

                $acciones = $edit;
                if ($post->id_estado == 8 && (Auth::user()->id_perfil == 12 || Auth::user()->id_perfil == 1)) {
                    $acciones = $edit . $asistir . $acompañar;
                }
                if (Auth::user()->id_perfil == 12 || Auth::user()->id_perfil == 1 || Auth::user()->id_perfil == 16) {
                    $acciones .= $confirmar;
                }
                // dd($acciones);
                if (Auth::user()->id_perfil == 12) {
                    $acciones .= $alcaldia;
                }
                if ($post->id_estado == 5) {
                    $edit = link_to_route('' . $this->configuraciongeneral[7] . '.edit', ' EDITAR', array($post->id), array('class' => 'fa fa-pencil-square-o btn btn-light', 'style' => 'color: #fff;background-color: #17a2b8;border-color: #17a2b8; width: 150px'));

                    $acciones = $edit . $confirmar . $confirmar_asistencia_finalizada;
                }

                if ($id_tipo_pefil->tipo != 8 && $id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4) {
                    $acciones = '&nbsp;&nbsp;' . link_to_route('' . $this->configuraciongeneral[7] . '.show', ' VER', array($post->id), array('class' => 'fa fa-newspaper-o divpopup btn btn-primary', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;';
                }




                $adjunto = "<a href='" . URL::to('/archivos_sistema/' . $post->adjunto) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";




                // dd($post);

                $nestedData['id'] = $post->id;
                $nestedData['id_tipo'] = $post->id_tipo;
                $nestedData['nombre'] = $post->nombre;
                $nestedData['descripcion'] = $post->descripcion;
                $nestedData['direccion'] = $post->direccion;
                $nestedData['fecha_inicio'] = $post->fecha_inicio;
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['detalle_direccion'] = $post->detalle_direccion;
                $nestedData['id_estado'] = $post->estado_aprobacion;
                $nestedData['confirmar_asistencia'] = ($post->confirmar_asistencia) ? 'SI' : 'NO';
                $nestedData['adjunto'] = ($post->adjunto) ? $adjunto : '';
                $nestedData['asistencia'] = (($post->asistio == 1) ? 'ASISTIÓ' : (($post->asistio == 0 && $post->confirmar_asistencia == 1 && $post->id_estado == 5) ? 'NO ASISTIÓ' : ''));
                $nestedData['acciones'] = $acciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }

    function getActividades()
    {
        $filtros = Input::all();

        $fecha_inicio = '2019-01-01 00:00:00';
        
        $carbon_fecha = new \Carbon\Carbon();
        $fecha_fin = $carbon_fecha->addYear();
        if (isset($filtros["estado_aprobacion"])) {
            // dd($filtros);
            $carbon_fecha = new \Carbon\Carbon();
            $fecha_inicio = $filtros['fecha_inicio_f'];
            if ($fecha_inicio == '') {
                $fecha_inicio = '2019-01-01 00:00:00';
            } else {
                $fecha_inicio = $filtros['fecha_inicio_f'] . ' 00:00:00';
            }
            $fecha_fin = $filtros['fecha_fin_f'];

            if ($fecha_fin == '') {
                // dd('as');
                $fecha_fin = $carbon_fecha->addYear();
            } else {
                $fecha_fin = $filtros['fecha_fin_f'] . ' 23:59:59';
            }
            $actividades = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as id_estado', 'e.id as id_estado_2', 't.tipo')
                ->where(['com_tmov_comunicacion_agenda.estado' => 'ACT'])
                // ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                //     $query->whereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                //         ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                // })
                ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin])
                ->wherenotin('com_tmov_comunicacion_agenda.id_estado',[6])
                // ->whereIn('id_estado', [3, 4, 8])
                ->where(function ($query) use ($filtros) {
                    // dd(Carbon::parse($filtros['start']));
                    $query->where('com_tmov_comunicacion_agenda.id', 'LIKE', "%{$filtros["campos"]}%")
                        ->orWhere('com_tmov_comunicacion_agenda.nombre', 'LIKE', "%{$filtros["campos"]}%")
                        ->orWhere('direccion', 'LIKE', "%{$filtros["campos"]}%")
                        ->orWhere('responsable', 'LIKE', "%{$filtros["campos"]}%");

                    // if ($filtros["start"] !=null || $filtros["end"] != null) {
                    // dd('s');
                    // $query->whereBetween('fecha_inicio', array($filtros['fecha_inicio'], $filtros['fecha_fin']));
                    // ->orwhereBetween('fecha_fin', array($filtros['fecha_inicio'], $filtros['fecha_fin']));
                    // $query->whereBetween('fecha_inicio', [$filtros['start'], $filtros['end']]);
                    // }
                });
            // dd($actividades);
        } else {
            $actividades = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as id_estado', 'e.id as id_estado_2', 't.tipo')
                ->wherenotin('com_tmov_comunicacion_agenda.id_estado',[6])
                ->where(['com_tmov_comunicacion_agenda.estado' => 'ACT'])
                // ->whereIn('id_estado', [3, 4, 8])
            ;
            // dd('s');
        }

        if (isset($filtros["estado_aprobacion"])) {
            if ($filtros["estado_aprobacion"] == 30) {
                $actividades = $actividades->whereIn('id_estado', [3, 4, 8]);
            } else {
                if ($filtros["estado_aprobacion"] != 0) {
                    $actividades = $actividades->where('id_estado', $filtros["estado_aprobacion"]);
                }
            }
        }


        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo != 8 && $id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4) {
            $id = [];
            $query_delegadas = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo as id_tipo')
                ->wherenotin('com_tmov_comunicacion_agenda.id_estado',[6])
                ->where(['com_tmov_comunicacion_agenda.estado' => 'ACT'])
                ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin])->get();
            foreach ($query_delegadas as $kk => $vv) {
                # code...
                if ($vv->delegacion_alcalde != null) {
                    $responsables = DB::select('select id as delegacion_alcalde from users where id in (' . $vv->delegacion_alcalde . ')');
                    foreach ($responsables as $kkk => $vvv) {

                        # code...
                        // dd(Auth::user()->id); 
                        // dd($responsables);
                        if (Auth::user()->id == $vvv->delegacion_alcalde) {
                            // dd($responsables);
                            array_push($id, $vv->id);
                        }
                    }
                }
            }

            $guardarres = AgendaNotificacionesPersonalizadas::where("id_usuario", Auth::user()->id)
                ->get();
            // dd($guardarres);
            foreach ($guardarres as $key => $value) {
                # code...
                array_push($id, $value->id_agenda);
            }

            $actividades = $actividades->whereIn('com_tmov_comunicacion_agenda.id', $id);
            // dd($actividades->get());
            // id_agenda

            if ($id_tipo_pefil->id_perfil == 10) {
                $actividades = $actividades->orwhere('coordinadores', 1)->where('tipo_agenda', 0);
            } elseif ($id_tipo_pefil->id_perfil == 25) {
                // $misconcejales=[];
                $misconcejales = explode("|", ConfigSystem("misconcejales"));
                if (is_array($misconcejales) && in_array($id_tipo_pefil->id, $misconcejales)) {
                    $actividades = $actividades->where('tipo_agenda', 0)
                        ->where(function ($actividades) {
                            $actividades
                                ->orwhere('concejales', 1)
                                ->orwhere('misconcejales', 1);
                        });
                    // Log::info()
                } else {
                    $actividades = $actividades->where('concejales', 1)->where('tipo_agenda', 0);
                }
            } elseif ($id_tipo_pefil->id_perfil == 3 || $id_tipo_pefil->id_perfil == 15 || $id_tipo_pefil->id_perfil == 22 || $id_tipo_pefil->id_perfil == 9) {
                $actividades = $actividades->orwhere('directores', 1)->where('tipo_agenda', 0);
                // dd($eventos);
            }
            $actividades = $actividades->get();
        } else {
            $actividades = $actividades->get();
        }
        // dd($actividades);
        $actividades_collect = collect($actividades);
        $multiplied = $actividades_collect->map(function ($item, $key) {
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
            // dd($id_tipo_pefil);
            if (isset($id_tipo_pefil) && $id_tipo_pefil->tipo != 3 && $id_tipo_pefil->tipo != 2 && $id_tipo_pefil->tipo != 4 || Auth::user()->id_perfil == 12) {


                $edit = link_to_route('' . $this->configuraciongeneral[7] . '.edit', ' EDITAR', array($item->id), array('class' => 'fa fa-pencil-square-o btn btn-light', 'style' => 'color: #fff;background-color: #17a2b8;border-color: #17a2b8'));
                $reprogramar = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-calendar btn btn-danger" onclick="atender(\'' . Crypt::encrypt($item->id) . '\',4,\'aaa\',\'' . $item->fecha_inicio . '\',\'' . $item->fecha_fin . '\')" > REPROGRAMAR</a>';
                $delegar = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-users btn btn-info"  onclick="atender(\'' . Crypt::encrypt($item->id) . '\',7,\'aaa\')" > DELEGAR</a>';
                $suspender = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-times btn btn-danger" onclick="atender(\'' . Crypt::encrypt($item->id) . '\',3,\'aaa\')" > SUSPENDER</a>';
                $asistir = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-check-circle btn btn-success" onclick="atender(\'' . Crypt::encrypt($item->id) . '\',8,\'aaa\')" > ASISTIR</a>';
                $acompañar = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-times btn btn-light" style="background-color: #697b42; color: white;" onclick="atender(\'' . Crypt::encrypt($item->id) . '\',9,\'aaa\')" > INTEGRARSE</a>';
                $eliminar = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-trash btn btn-warning"  style="width: 150px;" onclick="atender(\'' . Crypt::encrypt($item->id) . '\',13,\'aaa\')" > ELIMINAR</a>';
                
                $acciones = $edit . $reprogramar . $delegar . $suspender . $eliminar;
                $confirmar = '&nbsp;&nbsp;<a id="aprobar" class="fa  fa-check-circle-o btn btn-secondary" style="    background-color: #a04495; color: white;" onclick="atender(\'' . Crypt::encrypt($item->id) . '\',10,\'aaa\')" > ' . (($item->confirmar_asistencia == 1) ? 'RETIFICAR' : 'CONFIRMAR') . ' ASISTENCIA</a>';
                $confirmar_asistencia_finalizada = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-flag-checkered btn btn-secondary" style="    background-color: #5544a0; color: white;" onclick="atender(\'' . Crypt::encrypt($item->id) . '\',12,\'aaa\')" > FINALIZAR</a>';

                if ($item->id_estado_2 == 8) {
                    $acciones = $acciones . $asistir . $acompañar;
                }
                if (Auth::user()->id_perfil == 12 || Auth::user()->id_perfil == 1 || Auth::user()->id_perfil == 16) {
                    $acciones .= $confirmar;
                }
                if ($item->id_estado_2 == 5) {
                    $edit = link_to_route('' . $this->configuraciongeneral[7] . '.edit', ' EDITAR', array($item->id), array('class' => 'fa fa-pencil-square-o btn btn-light', 'style' => 'color: #fff;background-color: #17a2b8;border-color: #17a2b8'));

                    $acciones = $edit . $confirmar . $confirmar_asistencia_finalizada;
                }
            } else {
                $acciones = '';
            }

            $color = '';
            if ($item->id_estado_2 == 3 || $item->id_estado_2 == 4 || $item->id_estado_2 == 8) {

                $color = '#4CAF50';
                if ($item->confirmar_asistencia == 0) {
                    $color = '#5544a0';
                }
            } elseif ($item->id_estado_2 == 5 || $item->id_estado_2 == 12) {
                $color = '#d4cece';
            } elseif ($item->id_estado_2 == 6 || $item->id_estado_2 == 7) {
                $color = '#dd4b39';
            } else {

                $color = '#2196F3';
            }



            return collect(
                [
                    'id' => $item->id,
                    'title' => substr($item->tipo, 0, 3) . '. ' . $item->nombre,
                    'start' => date('Y-m-d H:i:s', strtotime($item->fecha_inicio)),
                    'end' => date('Y-m-d H:i:s', strtotime($item->fecha_fin)),
                    'description' => str_replace(array("\r", "\n"), '', $item->descripcion),
                    'acciones' => str_replace(array("\r", "\n"), '', $acciones),
                    'color' => $color
                ]
            );
        });

        return $multiplied;
    }
    public function perfil($verificar = 'SI')
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "users.id_perfil")->where("users.id", Auth::user()->id)->first();

        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->id_perfil == 12 || $id_tipo_pefil->id_perfil == 16) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'NO';
            $object->guardar = 'SI';
        }

        return $object;
    }
    public function index()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $delete = 'si';
        $create = 'si';



        $objetos = json_decode($this->objetos);
        // show($objetos);
        unset($objetos[6]);
        unset($objetos[3]);
        unset($objetos[4]);
        unset($objetos[7]);
        unset($objetos[8]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[12]);
        unset($objetos[11]);
        unset($objetos[13]);
        unset($objetos[14]);
        unset($objetos[15]);
        unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[18]);
        // unset($objetos[21]);

        $filtros = '[
            {"Tipo":"select","Descripcion":"Cantidad a mostar","Nombre":"length","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"text","Descripcion":"Campo de busqueda","Nombre":"campos","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Estado","Nombre":"estado_aprobacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"date","Descripcion":"Inicio","Nombre":"fecha_inicio_f","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"date","Descripcion":"Fin","Nombre":"fecha_fin_f","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Confirmación","Nombre":"confirmacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';
        $filtros = json_decode($filtros);
        // dd($filtros);
        $estados = AgendaEstadosModel::where('estado', 'ACT')->wherenotin('id', [2, 7, 8, 9, 10, 11, 12])->orderby('nombre', 'asc')->pluck('nombre', 'id')->all();
        $filtros[0]->Valor = [10 => '10', 10 => '10', 50 => '50', 100 => '100', 10000 => 'TODOS'];
        $filtros[2]->Valor = [0 => 'TODOS'] + $estados;
        $filtros[5]->Valor = [null => 'TODOS', 0 => 'SIN CONFIRMAR', 1 => 'CONFIRMADA'];
        // dd($actividades);
        unset($filtros[0]);
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => [],
            "actividades_agenda" => [],
            "configuraciongeneral" => $this->configuraciongeneral,
            "create" => $create,
            "delete" => $delete,
            "permisos" => $this->perfil('NO'),
            "id_tipo_pefil" => $id_tipo_pefil,
            "filtros" => $filtros,
            "validar_Agenda" => "si"

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        //////// mapa //////
        $config['center'] = '-0.9484704, -80.7237787';
        //$config['center'] = 'auto';
        $config['zoom'] = 'auto';
        $config['onclick'] = '$("#latitud").val(event.latLng.lat()); $("#longitud").val(event.latLng.lng()); 
        marker_0.setOptions({
            position: new google.maps.LatLng(event.latLng.lat(),event.latLng.lng())
        });';


        //////// mapa //////

        Map::initialize($config);
        $marker = array();
        Map::add_marker($marker);
        $data['map'] = Map::create_map();
        $objetos = json_decode($this->objetos);

        $delegados = UsuariosModel::join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
            ->join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')
            ->select('users.id', DB::raw('CONCAT(users.name," - ",d.direccion) as name'))
            ->whereIn('tipo', [3, 4, 10])
            ->orWhere('p.id', 25)
            ->orWhereIn('users.id', [1648, 1809])
            ->orderBy('d.alias', 'ASC')
            ->pluck('name', 'id')
            ->all();


        $tipo = TipoAgendaModel::where(['estado' => 'ACT'])->pluck('tipo', 'id')->all();
        $parroquia = parroquiaModel::where(['estado' => 'ACT'])->wherenotin('id', [9])->orderby('parroquia', 'ASC')->pluck('parroquia', 'id')->all();
        $objetos[3]->Valor = $parroquia;
        $objetos[4]->Valor = barrioModel::where(['estado' => 'ACT', 'id_parroquia' => 1])->orderby('barrio', 'ASC')->pluck('barrio', 'id')->all();
        $objetos[0]->Valor = $tipo;
        // $objetos[12]->Valor = [0 => "LABORAL", 1 => "PERSONAL"];
        $objetos[13]->Valor = ['GRUPO' => 'GRUPO', 'COORDINADORES' => 'COORDINADORES', 'CONCEJALES' => 'CONCEJALES', 'DIRECTORES' => 'DIRECTORES'];
        $objetos[15]->Valor = $delegados;
        $objetos[17]->Valor = $delegados;
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);

        // show($objetos);
        $this->configuraciongeneral[2] = "crear";
        // show($objetos);
        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
            "mapa" => $data,
            "validar_Agenda" => "si"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        return $this->guardar(0);
        //
    }

    public function getCategoriaEvento()
    {
        $id = Input::get('id');
        $array = [];
        if ($id == "INTERNO") {
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
            if ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 4 || $id_tipo_pefil->tipo == 5) {
                $array = ["ACTIVIDADES"];
            } else {
                $array = ["ACTIVIDADES", "REUNIONES DE TRABAJO"];
            }
        } else {
            $array = ["EVENTOS SOCIALES"];
        }

        return $array;
        //
    }

    public function guardar($id)
    {
        try {
            DB::beginTransaction();
            //code...
            $notificacion = new NotificacionesController;

            $input = Input::all();
            // dd($input);

            $ruta = $this->configuraciongeneral[1];

            $bandera = true;

            // dd($validator);

            if ($id == 0) {
                $ruta .= "/create";
                $guardar = new AgendaVirtualModel();
                $msg = "Registro Creado Exitosamente...!";
                $msg2 = "Actividad registrada";
                $msgauditoria = "Registro Variable de Configuración";



                $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "users.id_perfil")->where("users.id", Auth::user()->id)->first();
                // $verificacion = $this->verficar_existente(Input::get('fecha_inicio'), Input::get('fecha_fin'), $id, 2);

                // if ($verificacion['estado'] == 'false') {
                //     return Redirect::to("$ruta")
                //         ->withWarning('Existe un actividad en este rango de fechas.')
                //         ->withInput();
                // }
                // if (Input::hasFile("adjunto")) {
                //     $docs = Input::file("adjunto");
                //     if ($docs) {
                //         $ext = $docs->getClientOriginalExtension();
                //         $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                //         if (!in_array($ext, $perfiles)) {
                //             DB::rollback();
                //             $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                //             return redirect()->back()->withErrors([$mensaje])->withInput();
                //         }
                //     }
                // }

                $guardar->id_usuario = Auth::user()->id;
                $guardar->metodo_regsitro = 'WEB';
            } else {
                $ruta .= "/$id/edit";
                $guardar = AgendaVirtualModel::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msg2 = "Actividad modificada";
                $msgauditoria = "Edición Variable de Configuración";
                $guardar->id_usuario = Auth::user()->id;
                // $verificacion = $this->verficar_existente(Input::get('fecha_inicio'), Input::get('fecha_fin'), $id, 2);
                // if ($verificacion['estado'] == 'false' && Input::get('id_estado') == 4) {
                //     return response()->json(['error' => 'Existe un evento en este rango de fechas.'], 200);
                // }
                $bandera = false;
            }


            $fecha = date('w', strtotime(Input::get('fecha_inicio')));
            $fin_fecha = date('w', strtotime(Input::get('fecha_fin')));

            if ($fecha != $fin_fecha) {
                return Redirect::to("$ruta")
                    ->withWarning('La duración de las actividades no pueden durar mas de un día.')
                    ->withInput();
            }

            $carbon1 = new \Carbon\Carbon(Input::get('fecha_inicio'));
            $carbon2 = new \Carbon\Carbon(Input::get('fecha_fin'));
            $minutesDiff = $carbon1->diffInMinutes($carbon2, true);
            // dd($minutesDiff);
            // dd($value);

            if ($minutesDiff <= 0) {
                return Redirect::to("$ruta")
                    ->withWarning('La hora de inicio debe ser mayor a la hora fin')
                    ->withInput();
            }



            $input = Input::all();
            $validator = Validator::make($input, AgendaVirtualModel::rules($id));

            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            } else {
                // dd($input);
                foreach ($input as $key => $value) {
                    if ($key == 'fecha_inicio') {
                        $carbon1 = new \Carbon\Carbon();
                        $carbon2 = new \Carbon\Carbon($value);
                        $minutesDiff = $carbon1->diffInMinutes($carbon2, true);
                        // dd($minutesDiff);
                        // dd($value);

                        if ($minutesDiff < 0) {
                            return Redirect::to("$ruta")
                                ->withWarning('La fecha de inicio debe ser mayor a la actual')
                                ->withInput();
                        }
                    }
                    $guardar->coordinadores = 0;
                    $guardar->concejales = 0;
                    $guardar->directores = 0;
                    // dd($guardar);



                    if ($key != "_method" && $key != "_token" && $key != "adjunto" && $key != "en_espera" && $key != "id_usuario_masivo" && $key != "otros" && $key != "delegaciones"  && $key != "parroquia_id") {
                        if ($key == "notificaciones") {
                        } elseif ($key == "delegacion_alcalde") {
                            if (is_array($input['delegacion_alcalde'])) {
                                $guardar->$key = implode(',', $input['delegacion_alcalde']);
                                // dd($guardar->$key);
                                $guardar->id_estado = 8;
                                // dd($input);

                            } else {
                                $guardar->$key = null;
                                $guardar->id_estado = 3;
                            }
                            // dd($guardar->delegacion_alcalde);
                        } elseif ($key == "id_tipo") {
                            $guardar->$key = $value;
                            if ($value == 18) {
                                $guardar->tipo_agenda = 1;
                            } else {
                                $guardar->tipo_agenda = 0;
                            }
                        } else {
                            // if($key == "delegacion_alcalde"){
                            //     dd($guardar->$key.'dd');
                            // }
                            $guardar->$key = $value;
                        }
                    }
                }

                if (!isset($input['delegacion_alcalde'])) {
                    $guardar->delegacion_alcalde = null;
                    $guardar->id_estado = 3;
                }
                $notificaciones = Input::get('notificaciones');
                if ($notificaciones != null) {
                    if (in_array('COORDINADORES', $notificaciones)) {
                        $guardar->coordinadores = 1;
                    }
                    if (in_array('CONCEJALES', $notificaciones)) {
                        $guardar->concejales = 1;
                    }
                    if (in_array('DIRECTORES', $notificaciones)) {
                        $guardar->directores = 1;
                    }
                    if (in_array('GRUPO', $notificaciones)) {
                        $guardar->misconcejales = 1;
                    }
                }

                if ($guardar->barrio_id == 276) {
                    $guardar->canton_id = 8;
                }



                // dd($guardar);

                $guardar->save();

                $idcab = $guardar->id;
                /*ARCHIVO*/


                if (isset($input['adjunto'])) {
                    $dir = public_path() . '/archivos_sistema/';
                    // AgendaArchivosModel
                    $archivos = $input['adjunto'];
                    foreach ($archivos as $key => $value) {
                        # code...
                        $docs = $value;
                        if (is_file($docs)) {
                            $fileName = "agenda-$idcab-$key" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                            $ext = $docs->getClientOriginalExtension();
                            $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                            if (!in_array($ext, $perfiles)) {
                                DB::rollback();
                                $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                                return redirect()->back()->withErrors([$mensaje])->withInput();
                            }


                            $file = new AgendaArchivosModel;
                            // $oldfile = $dir . $file->archivo;
                            // File::delete($oldfile);
                            $file->ruta = $fileName;
                            $file->id_agenda = $idcab;
                            $file->save();
                            $docs->move($dir, $fileName);
                        }
                    }
                }





                /*Detalle Personas a Cargo / Fiscalizadores*/
                if (Input::has("id_usuario_masivo")) {
                    $multiple = Input::get("id_usuario_masivo");
                    AgendaNotificacionesPersonalizadas::where("id_agenda", $idcab)
                        ->delete();
                    $json = [];
                    $players = [];
                    foreach ($multiple as $key => $value) {
                        # code...
                        //Responsables

                        $guardarres = AgendaNotificacionesPersonalizadas::where("id_usuario", $value)
                            ->where("id_agenda", $idcab)
                            ->first();
                        if (!$guardarres)
                            $guardarres = new AgendaNotificacionesPersonalizadas();
                        $guardarres->id_usuario = $value;
                        $guardarres->id_agenda = $idcab;
                        $guardarres->save();
                        $usuario = DB::table('tma_playerid')->where('users_id', $guardarres->id_usuario)->get();
                        // dd($usuario);
                        foreach ($usuario as $u => $u_) {
                            array_push($players, $u_->id_player);
                        }
                    }
                    $web_api = new WebApiAgendaController;
                    $notificacion->notificacion($players, $guardar->id, '🔔  ' . $msg2, $guardar->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($guardar->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($guardar->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($guardar->fecha_fin)), 2, $web_api->getActividad($guardar->id));
                } else {
                    AgendaNotificacionesPersonalizadas::where("id_agenda", $idcab)
                        ->delete();
                }


                DB::commit();

                if (isset($input['delegacion_alcalde']) && is_array($input['delegacion_alcalde'])) {
                    $web_api = new WebApiAgendaController;
                    foreach ($input['delegacion_alcalde'] as $key => $value) {
                        $usuario = UsuariosModel::where('id', $value)->first();
                        if ($usuario) {
                            $players = [];
                            $telefonos_personas = DB::table('tma_playerid')
                                ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                                ->where('u.id', $usuario->id)->get();
                            foreach ($telefonos_personas as $key => $v) {
                                array_push($players, $v->id_player);
                            }
                            //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $guardar->id, '🔔  Ha sido delegado a una actividad de la agenda virtual',  $guardar->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($guardar->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($guardar->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($guardar->fecha_fin)), 2, $guardar);
                            $notificacion->EnviarEmailAgenda($usuario->email, ' 🔔  Ha sido delegado a una actividad de la agenda virtual', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $guardar->nombre . ' <br> <b>Fecha:</b> ' . date('d/m/Y', strtotime($guardar->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($guardar->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($guardar->fecha_fin)) . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $guardar->descripcion . '<b> Observación: </b>' . ((isset($guardar->observacion)) ? '' . $guardar->observacion : 'Ninguna') .  '</div>', "coordinacioncronograma/agendavirtual");
                            $notificacion->notificacion($players, $guardar->id, '🔔  Ha sido delegado a una actividad de la agenda virtual',  $guardar->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($guardar->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($guardar->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($guardar->fecha_fin)), 2, $web_api->getActividad($guardar->id));
                        }
                    }
                }


                // dd('ssss');
                $this->notificarAlcalde('🔔  ' . $msg2, $guardar);
                if ($guardar->coordinadores == 1 || $guardar->coordinadores == true) {
                    //    dd(10);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 10);
                }
                if ($guardar->concejales == 1 || $guardar->concejales == true) {

                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 25);
                }

                if ($guardar->directores == 1 || $guardar->directores == true) {
                    // dd($guardar);

                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 3);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 5);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 15);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 22);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 29);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 9);
                }

                if ($guardar->misconcejales == 1 || $guardar->misconcejales == true) {

                    $this->notificarGrupito('🔔  ' . $msg2, $guardar);
                }




                insertarhistorial($guardar, $idcab, 1, Auth::user()->id);
                Auditoria($msgauditoria . " - ID: " . $id . "-" . Input::get($guardar->nombre));
            }
            Session::flash('message', $msg);
            return Redirect::to($this->configuraciongeneral[1]);
        } catch (\Throwable $th) {
            DB::rollback();
            Session::flash('error', 'Ocurrio un problema...');
            return $th;
            //throw $th;
        }
    }

    public function notificarAlcalde($titulo, $actividad)
    {
        $web_api = new WebApiAgendaController;
        $cedula_alcalde =  ConfigSystem('cedula_alcalde');
        $notificacion = new NotificacionesController;
        $telefonos_alacalde = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->where('cedula', $cedula_alcalde)->get();

        $players = array();
        foreach ($telefonos_alacalde as $key => $value) {
            array_push($players, $value->id_player);
        }
        $notificacion->notificacion($players, $actividad->id, $titulo, $actividad->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, $actividad->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));

        $usuario = User::where('cedula', $cedula_alcalde)->first();
        if ($usuario) {
            $notificacion->EnviarEmailAgenda($usuario->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha:</b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin)) . '<br><b> Responsable: </b> ' . $actividad->responsable . '</div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '</div>', "coordinacioncronograma/agendavirtual", "vistas.email", "NO");
            //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha: </b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin))  . '<br><b> Responsable: </b> ' . $actividad->responsable . '</div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '</div>',  "coordinacioncronograma/agendavirtual", "vistas.email", "NO");
        }
    }
    public function notificarTodos($titulo, $actividad, $perfil)
    {
        // dd($perfil);
        $web_api = new WebApiAgendaController;
        $notificacion = new NotificacionesController;
        $telefonos_personas = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->join('ad_perfil as p', 'p.id', '=', 'u.id_perfil')
            ->where('u.id_perfil', $perfil)
            ->get();

        $usuarios = UsuariosModel::where('id_perfil', $perfil)->get();
        foreach ($usuarios as $key => $value_) {
            $notificacion->EnviarEmailAgenda($value_->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");
        }

        $players = array();
        foreach ($telefonos_personas as $key => $value) {
            array_push($players, $value->id_player);
            // $notificacion->EnviarEmailAgenda($value->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");

        }
        $notificacion->notificacion($players, $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
    }



    public function notificarPersonalizada($titulo, $actividad)
    {
        // dd($perfil);
        $web_api = new WebApiAgendaController;
        $notificacion = new NotificacionesController;
        $telefonos_personas =  AgendaNotificacionesPersonalizadas::join('tma_playerid as p', 'p.users_id', '=', 'com_tmo_notifiacion_personalizada.id_usuario')
            // ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            // ->join('ad_perfil as p', 'p.id', '=', 'u.id_perfil')
            ->where("com_tmo_notifiacion_personalizada.id_agenda", $actividad->id)
            ->get();

        $usuarios =  AgendaNotificacionesPersonalizadas::
            // join('tma_playerid as p','p.users_id','=','com_tmo_notifiacion_personalizada.id_usuario')
            join('users as u', 'u.id', '=', 'com_tmo_notifiacion_personalizada.id_usuario')
            // ->join('ad_perfil as p', 'p.id', '=', 'u.id_perfil')
            ->where("com_tmo_notifiacion_personalizada.id_agenda", $actividad->id)
            ->get();
        foreach ($usuarios as $key => $value_) {
            $notificacion->EnviarEmailAgenda(
                $value_->email, 
                $titulo, 
                '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'),
                '',
                'coordinacioncronograma/agendavirtual',
                 "vistas.email");
        }

        $players = array();
        foreach ($telefonos_personas as $key => $value) {
            array_push($players, $value->id_player);
            // $notificacion->EnviarEmailAgenda($value->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");

        }


        $notificacion->notificacion($players, $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
    }
    public function notificarRegistra($titulo, $actividad)
    {
        $web_api = new WebApiAgendaController;
        $notificacion = new NotificacionesController;
        $telefonos = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->where('u.id', $actividad->id_usuario)->get();

        $usuario = User::where('id', $actividad->id_usuario)->first();
        if ($usuario) {
            $notificacion->EnviarEmailAgenda($usuario->email, $titulo, '<div style="padding: 5% 0 0 6%">Su actividad fué modificada <br><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha: </b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin))  . '<br><b> Responsable: </b> ' . $actividad->responsable . '</div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '</div>', 'coordinacioncronograma/agendavirtual', "vistas.email");
            //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', $titulo, '<div style="padding: 5% 0 0 6%">Su actividad fué modificada <br><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha: </b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin))  . '<br><b> Responsable: </b> ' . $actividad->responsable . '</div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '</div>', 'coordinacioncronograma/agendavirtual', " vistas.email");
        }
        $players = array();
        foreach ($telefonos as $key => $value) {
            array_push($players, $value->id_player);
        }
        $notificacion->notificacion($players, $actividad->id, $titulo, $actividad->nombre, 2, $web_api->getActividad($actividad->id));
        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, $actividad->nombre, 2, $web_api->getActividad($actividad->id));
    }
    public function notificarAlcaldia($titulo, $actividad)
    {
        $web_api = new WebApiAgendaController;
        $notificacion = new NotificacionesController;
        $telefonos = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->join('ad_perfil as p', 'p.id', '=', 'u.id_perfil')
            ->where('p.tipo', 8)->get();

        $usuario = User::join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
            ->select('users.*', 'p.tipo')
            ->where('p.tipo', 8)->get();

        foreach ($usuario as $key => $value) {
            # code...
            if ($value) {
                $notificacion->EnviarEmailAgenda($value->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre,  '<div style="padding: 1% 0 0 6%"><b> Dirección:</b> ' . $actividad->direccion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') . '</div>', "coordinacioncronograma/agendavirtual");
                //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre,  '<div style="padding: 1% 0 0 6%"><b> Dirección:</b> ' . $actividad->direccion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') . '</div>', " coordinacioncronograma/agendavirtual");
                $notificacion->notificacionesweb("Estimado(a) " . $value->name . ", existe una nueva actividad por revisar.", "coordinacioncronograma/agendavirtual", $value->id, "ed5565");
            }
        }
        $players = array();
        foreach ($telefonos as $key => $value) {
            array_push($players, $value->id_player);
        }
        $notificacion->notificacion($players, $actividad->id, $titulo, 'Actividad: ' . $actividad->nombre, 2, $web_api->getActividad($actividad->id));
        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, 'Actividad: ' . $actividad->nombre, 2, $web_api->getActividad($actividad->id));
    }


    public function notificarDelgados($titulo, $actividad)
    {
        $notificacion = new NotificacionesController;
        $web_api = new WebApiAgendaController;
        $delegados = $actividad->delegacion_alcalde;
        $array_delegados = explode(',', $delegados);
        if (!is_array($array_delegados) && $actividad->delegacion_alcalde != null) {
            $array_delegados = [$delegados];
        }

        if (is_array($array_delegados)) {
            foreach ($array_delegados as $key => $value) {
                $usuario = UsuariosModel::where('id', $value)->first();
                if ($usuario) {
                    $players = array();
                    $telefonos_personas = DB::table('tma_playerid')
                        ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                        ->where('u.id', $usuario->id)->get();
                    foreach ($telefonos_personas as $key => $v) {
                        array_push($players, $v->id_player);
                    }
                    //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, '🔔 ' . $titulo,  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $actividad);
                    $notificacion->EnviarEmailAgenda($usuario->email, ' 🔔 ' . $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha:</b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin)) . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') .  '</div>', "coordinacioncronograma/agendavirtual");
                    $notificacion->notificacion($players, $actividad->id, '🔔 ' . $titulo,  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
                }
            }
        }
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $objetos = json_decode($this->objetos);
        $tabla = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
            ->join('com_tma_tipo_agenda as t', 't.id', 'com_tmov_comunicacion_agenda.id_tipo')
            ->join('barrio as b', 'b.id', 'com_tmov_comunicacion_agenda.barrio_id')
            ->join('parroquia as p', 'p.id', 'b.id_parroquia')
            ->select(
                'com_tmov_comunicacion_agenda.*',
                't.tipo as id_tipo',
                'e.nombre as estado_aprobacion',
                'b.barrio as barrio_id',
                'p.parroquia as parroquia_id',
                DB::raw('IFNULL((select GROUP_CONCAT(u.name SEPARATOR ", ") as responsables from com_tmo_notifiacion_personalizada as per inner join users as u on u.id=per.id_usuario where id_agenda = com_tmov_comunicacion_agenda.id),"NO") as id_usuario_masivo'),
                DB::raw('IFNULL((select GROUP_CONCAT(u.name SEPARATOR ", ") as delegados from users as u  where u.id in  (com_tmov_comunicacion_agenda.delegacion_alcalde) ),"NO") as delegacion_alcalde'),
                DB::raw('IF(com_tmov_comunicacion_agenda.masivo=1, "SI", "NO") as masivo'),
                DB::raw('IF(com_tmov_comunicacion_agenda.confirmar_asistencia=1, "SI", "NO") as confirmar_asistencia'),
                DB::raw('IF(com_tmov_comunicacion_agenda.asistio=1, "SI", "NO") as asistencia')
            )
            ->where(["com_tmov_comunicacion_agenda.estado" => "ACT", "com_tmov_comunicacion_agenda.id" => $id])->first();

        // show($tabla);


        $latitud = $tabla->latitud;
        $longitud = $tabla->longitud;
        $autozoom = "18";

        if ($latitud == "") {
            $latitud = "-0.9484126160642922";
            //$autozoom="18";
        }
        if ($longitud == "")
            $longitud = "-80.72165966033936";
        $config['center'] = "$latitud,$longitud";
        $config['zoom'] = $autozoom; //'auto';

        Map::initialize($config);
        //Marker
        $marker = array();
        $marker['position'] = "$latitud,$longitud";
        $marker['draggable'] = false;

        Map::add_marker($marker);
        $data['map'] = Map::create_map();
        unset($objetos[7]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        unset($objetos[14]);
        unset($objetos[16]);
        unset($objetos[19]);
        // show($objetos);
        $tabla->coordinadores = ($tabla->coordinadores == 1) ? 'SI' : 'NO';
        $tabla->concejales = ($tabla->concejales == 1) ? 'SI' : 'NO';
        $tabla->directores = ($tabla->directores == 1) ? 'SI' : 'NO';

        $timeline_agenda = AgendaVirtualModelDeta::leftjoin('users as u', 'u.id', '=', 'com_tmov_comunicacion_agenda_deta.id_usuario_modi')
            ->join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda_deta.id_estado')
            ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda_deta.id_tipo')
            ->select('com_tmov_comunicacion_agenda_deta.*', 'u.name', 'e.nombre as estado_agenda')
            ->where('id_cab_agenda', $id)->get();

        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "timeline_agenda" => $timeline_agenda,
            "configuraciongeneral" => $this->configuraciongeneral,
            //"validararry"=>$validararry
            "mapa" => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {

        $objetos = json_decode($this->objetos);

        $this->configuraciongeneral[2] = "editar";
        $tabla = AgendaVirtualModel::find($id);

        // unset($objetos[9]);

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        $latitud = $tabla->latitud;
        $longitud = $tabla->longitud;
        $autozoom = "18";
        $actiondrag = '$("#latitud").val(event.latLng.lat()); $("#longitud").val(event.latLng.lng()); marker_0.setOptions({
            position: new google.maps.LatLng(event.latLng.lat(),event.latLng.lng())
        });';
        if ($latitud == "") {
            $latitud = "-0.9484126160642922";
            //$autozoom="18";
        }
        if ($longitud == "")
            $longitud = "-80.72165966033936";
        $config['center'] = "$latitud,$longitud";
        $config['zoom'] = $autozoom; //'auto';
        $config['onclick'] = $actiondrag;
        Map::initialize($config);
        //Marker
        $marker = array();
        $marker['position'] = "$latitud,$longitud";
        $marker['draggable'] = true;
        $marker['ondragend'] = $actiondrag;
        Map::add_marker($marker);
        $data['map'] = Map::create_map();


        $delegados = UsuariosModel::join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
            ->join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')
            ->select('users.id', DB::raw('CONCAT(users.name," - ",d.direccion) as name'))
            ->whereIn('tipo', [3, 4, 10])
            ->orWhere('p.id', 25)
            ->orWhereIn('users.id', [1648, 1809])
            ->orderBy('d.alias', 'ASC')
            ->pluck('name', 'id')
            ->all();

        $tipo = TipoAgendaModel::where(['estado' => 'ACT'])
            // ->whereIn('id', [10, 11])
            ->pluck('tipo', 'id')->all();
        $delegaciones = AgendaNotificacionesPersonalizadas::where('id_agenda', $id)->pluck('id_usuario', 'id_usuario')->all();
        $objetos[0]->Valor = $tipo;
        $objetos[0]->ValorAnterior = $tabla->id_tipo;
        // $objetos[12]->Valor = [0 => "LABORAL", 1 => "PERSONAL"];
        // $objetos[13]->Valor = ['COORDINADORES' => 'COORDINADORES', 'CONCEJALES' => 'CONCEJALES', 'DIRECTORES' => 'DIRECTORES'];
        $objetos[13]->Valor = ['GRUPO' => 'GRUPO', 'COORDINADORES' => 'COORDINADORES', 'CONCEJALES' => 'CONCEJALES', 'DIRECTORES' => 'DIRECTORES'];
       
        $parroquia = parroquiaModel::where(['estado' => 'ACT'])->wherenotin('id', [9])->pluck('parroquia', 'id')->all();
        $parroquia_id = barrioModel::where(['estado' => 'ACT', 'id' => $tabla->barrio_id])->first();
        $objetos[3]->Valor = $parroquia;
        $objetos[3]->ValorAnterior = $parroquia_id->id_parroquia;

        $objetos[4]->Valor = barrioModel::where(['estado' => 'ACT', 'id_parroquia' => $parroquia_id->id_parroquia])->pluck('barrio', 'id')->all();
        $objetos[4]->ValorAnterior = $tabla->barrio_id;
        $objetos[0]->Valor = $tipo;
        $notificaciones = [];
        if ($tabla->coordinadores == 1) {
            $notificaciones['COORDINADORES'] = 'COORDINADORES';
        }
        if ($tabla->concejales == 1) {
            $notificaciones['CONCEJALES'] = 'CONCEJALES';
        }
        if ($tabla->directores == 1) {
            $notificaciones['DIRECTORES'] = 'DIRECTORES';
        }
        $objetos[13]->ValorAnterior = $notificaciones;

        // dd($tabla);




        // $objetos[13]->ValorAnterior = $tabla->coordinadores;
        // $objetos[14]->ValorAnterior = $tabla->concejales;
        // $objetos[15]->ValorAnterior = $tabla->dírectores;

        $objetos[15]->Valor = $delegados;
        $objetos[15]->ValorAnterior = $delegaciones;

        $objetos[17]->Valor = $delegados;
        // dd($tabla->delegacion_alcalde);
        $objetos[17]->ValorAnterior = explode(',', $tabla->delegacion_alcalde);
        if ($tabla->delegacion_alcalde != null) {
            $objetos[16]->Valor = true;
            // $objetos[13]->Valor = false;
        } else {
            $objetos[16]->Valor = false;
        }

        // dd($noti);
        $noti = AgendaNotificacionesPersonalizadas::where("id_agenda", $tabla->id)->first();
        if ($noti) {
            // $objetos[15]->Valor = false;
            $objetos[14]->Valor = true;
        } else {
            $objetos[14]->Valor = false;
        }
        // $objetos[11]->Valor = $tipo;
        // unset($objetos[18]); 
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        $objetos[10]->Valor = AgendaArchivosModel::where('id_agenda', $tabla->id)->pluck('ruta', 'id')->all();
        // show($objetos[10]);
        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "tabla" => $tabla,
            "mapa" => $data,
            "validarjs" => $this->validarjs,
            "validar_Agenda" => "si"
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $tabla = AgendaVirtualModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }

    function atender(Request $request)
    {

        try {
            //code...

            $id = Crypt::decrypt(Input::get('id'));
            $observacion = Input::get('obs');
            //$responsable = Input::get('responsable');
            $tipo = Input::get('tipo');
            $inicio = Input::get('fecha_inicio');
            $fin = Input::get('fecha_fin');
            $delegacion_alcalde = Input::get("delegacion_alcalde");


            $actividad = AgendaVirtualModel::find($id);
            $notificacion = new NotificacionesController;
            $web_api = new WebApiAgendaController;

            DB::beginTransaction();

            if ($tipo == 1) {

                $verificacion = $this->verficar_existente($actividad->fecha_inicio, $actividad->fecha_fin, $id);

                if ($verificacion['estado'] == 'true' && $actividad->id_estado != 2) {

                    $carbon1 = new \Carbon\Carbon();
                    $carbon2 = new \Carbon\Carbon($actividad->fecha_fin);
                    $minutesDiff = $carbon1->diffInMinutes($carbon2, false);

                    if ($minutesDiff < 0) {
                        return ['estado' => 'error', 'msg' => 'La actividad ya finalizó'];
                    }
                    $actividad->id_estado = 2;
                    $actividad->observacion = $observacion;
                    $actividad->save();
                    insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);

                    DB::commit();
                    $cedula_alcalde =  ConfigSystem('cedula_alcalde');
                    $telefonos_alacalde = DB::table('tma_playerid')
                        ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                        ->where('cedula', $cedula_alcalde)->get();

                    $players = array();
                    foreach ($telefonos_alacalde as $key => $value) {
                        array_push($players, $value->id_player);
                    }



                    $notificacion->notificacion($players, $actividad->id, '🔔  Actividad Pre-Aprobada',  $actividad->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
                    //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, '🔔  Actividad Pre-Aprobada',  $actividad->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));

                    $usuario = User::where('cedula', $cedula_alcalde)->first();
                    if ($usuario) {
                        $notificacion->EnviarEmailAgenda($usuario->email, 'Actividad Pre-Aprobada', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha: </b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin))  . '<br><b> Responsable: </b> ' . $actividad->responsable . '</div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') . ' </div>', "coordinacioncronograma/agendavirtual", "vistas.email", "NO");
                        //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', 'Actividad Pre-Aprobada', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha: </b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin))  . '<br><b> Responsable: </b> ' . $actividad->responsable . '</div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') . ' </div>',  "coordinacioncronograma/agendavirtual", "vistas.email", "NO");
                    }
                } else {
                    return response()->json(['error' =>  $verificacion['msg'], 'actividades' => $verificacion['actividades']], 200);
                }
            } elseif ($tipo == 3) {


                if ($observacion == '' || $observacion == null) {
                    return ['estado' => 'error', 'msg' => 'Campo requerido'];
                }
                $actividad->id_estado = 6;
                $actividad->observacion = $observacion;
                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);
                DB::commit();
                $cedula_alcalde =  ConfigSystem('cedula_alcalde');
                $telefonos_alacalde = DB::table('tma_playerid')
                    ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                    ->where('cedula', $cedula_alcalde)->get();


                //alcalde
                $players = array();
                foreach ($telefonos_alacalde as $key => $value) {
                    array_push($players, $value->id_player);
                }

                $notificacion->notificacion($players, $actividad->id, '🔔  Actividad suspendida ❌',  $actividad->nombre . '   Motivo:' . $actividad->observacion, 2, $web_api->getActividad($actividad->id));
                //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, '🔔  Actividad suspendida ❌',  $actividad->nombre . '  Motivo:' . $actividad->observacion, 2, $web_api->getActividad($actividad->id));

                $usuario = User::where('cedula', $cedula_alcalde)->first();
                if ($usuario) {
                    $notificacion->EnviarEmailAgenda($usuario->email, 'Actividad suspendida', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '</div>', '<div style="padding: 1% 0 0 6%"><b> Motivo:</b> ' . $actividad->observacion . '</div>', "coordinacioncronograma/agendavirtual", "vistas.email", "NO");
                    //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', 'Actividad suspendida', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '</div>', '<div style="padding: 1% 0 0 6%"><b> Motivo:</b> ' . $actividad->observacion . '</div>',  "coordinacioncronograma/agendavirtual", "vistas.email", "NO");
                }



                $msg2 = 'Actividad suspendida';
                if ($actividad->coordinadores == 1 || $actividad->coordinadores == true) {
                    //    dd(10);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 10);
                }
                if ($actividad->concejales == 1 || $actividad->concejales == true) {

                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 25);
                }
                if ($actividad->directores == 1 || $actividad->directores == true) {
                    // dd($actividad);

                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 3);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 5);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 15);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 22);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 29);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 9);
                }
                if ($actividad->misconcejales == 1 || $actividad->misconcejales == true) {

                    $this->notificarGrupito('🔔  ' . $msg2, $actividad);
                }

                $this->notificarPersonalizada('🔔  ' . $msg2, $actividad);

                $this->notificarDelgados('🔔  ' . $msg2, $actividad);
            } elseif ($tipo == 2) {

                if ($observacion == '' || $observacion == null) {
                    return ['estado' => 'error', 'msg' => 'Campo requerido'];
                }
                $actividad->id_estado = 7;
                $actividad->observacion = $observacion;
                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);
                DB::commit();
            } elseif ($tipo == 4) {

                // dd($inicio);
                // $verificacion = $this->verficar_existente($inicio, $fin, $id, 4);
                // if ($verificacion['estado'] == 'false') {
                //     return response()->json(['error' =>  $verificacion['msg'], 'actividades' => $verificacion['actividades']], 200);
                // }

                $carbon1 = new \Carbon\Carbon($inicio);
                $carbon2 = new \Carbon\Carbon($fin);
                $minutesDiff = $carbon1->diffInMinutes($carbon2, true);
                // dd($minutesDiff);
                // dd($value);

                if ($minutesDiff <= 0) {

                    return ['estado' => 'error', 'error' => 'La hora de inicio debe ser mayor a la hora fin'];
                }

                if ($observacion == '' || $observacion == null) {
                    return ['estado' => 'error', 'error' => 'Campo requerido'];
                }


                if ($actividad->id_estado == 8) {

                    $actividad->id_estado = 8;
                } else {
                    $actividad->id_estado = 4;
                }
                $actividad->observacion = $observacion;
                $actividad->fecha_inicio = $inicio;
                $actividad->fecha_fin = $fin;
                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);
                $web_api = new WebApiAgendaController;

                $bandera = false;
                if ($actividad->delegacion_alcalde != null) {
                    $delegacion_alcalde_ = explode(',', $actividad->delegacion_alcalde);
                    $mensajes = '';
                    if (!is_array($delegacion_alcalde_)) {
                        $delegacion_alcalde_ = [$actividad->delegacion_alcalde];
                    }
                    foreach ($delegacion_alcalde_ as $key => $value) {
                        # code...
                        $verificacion_delegado = $web_api->verificarDisponibilidad2($value, $inicio, $fin, $actividad->id);

                        if ($verificacion_delegado['respuesta'] == true) {
                            $bandera = true;
                            $mensajes .= $verificacion_delegado['mensaje_app'] . '<br>';
                        }
                        Log::info('--------------1486');
                        Log::info($verificacion_delegado);
                    }
                }

                $notificaciones = AgendaNotificacionesPersonalizadas::where("id_agenda",  $actividad->id)->get();
                foreach ($notificaciones as $key => $value) {
                    # code...
                    $verificacion_delegado = $web_api->verificarDisponibilidad2($value->id_usuario, $inicio, $fin, $actividad->id);

                    if ($verificacion_delegado['respuesta'] == true) {
                        $bandera = true;
                        $mensajes .= $verificacion_delegado['mensaje_app'] . '<br>';
                    }
                    Log::info('--------------1500');
                    Log::info($verificacion_delegado);
                }

                if ($bandera) {
                    return ['estado' => 'error', 'msg' => $mensajes];
                }

                DB::commit();
                $cedula_alcalde =  ConfigSystem('cedula_alcalde');
                $telefonos_alacalde = DB::table('tma_playerid')
                    ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                    ->where('cedula', $cedula_alcalde)->get();

                $players = array();
                foreach ($telefonos_alacalde as $key => $value) {
                    array_push($players, $value->id_player);
                }

                $usuario = User::where('cedula', $cedula_alcalde)->first();
                if ($usuario) {
                    $notificacion->EnviarEmailAgenda($usuario->email, 'Actividad reprogramada', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha: </b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin))  . '<br><b> Responsable: </b> ' . $actividad->responsable . '</div>', '<div style="padding: 1% 0 0 6%"><b> Observación: </b> ' . $actividad->observacion . '</div>', "coordinacioncronograma/agendavirtual", "vistas.email", "NO");
                    //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', 'Actividad reprogramada', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha: </b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin))  . '<br><b> Responsable: </b> ' . $actividad->responsable . '</div>', '<div style="padding: 1% 0 0 6%"><b> Observación: </b> ' . $actividad->observacion . '</div>',  "coordinacioncronograma/agendavirtual", "vistas.email", "NO");
                }

                $msg2 = 'Actividad reprogramada';
                if ($actividad->coordinadores == 1 || $actividad->coordinadores == true) {
                    //    dd(10);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 10);
                }
                if ($actividad->concejales == 1 || $actividad->concejales == true) {

                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 25);
                }
                if ($actividad->misconcejales == 1 || $actividad->misconcejales == true) {

                    $this->notificarGrupito('🔔  ' . $msg2, $actividad);
                }

                if ($actividad->directores == 1 || $actividad->directores == true) {
                    // dd($actividad);

                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 3);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 5);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 15);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 22);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 29);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 9);
                }
                $this->notificarPersonalizada('🔔  ' . $msg2, $actividad);
                $this->notificarDelgados('🔔  ' . $msg2, $actividad);
            } elseif ($tipo == 5) { /////////aprobar el alcalde

                $verificacion = $this->verficar_existente($actividad->fecha_inicio, $actividad->fecha_fin, $id, 3);
                if ($verificacion['estado'] == 'true') {
                    $actividad->id_estado = 3;
                    $actividad->recordar = "NO";
                } else {
                    return response()->json(['error' =>  $verificacion['msg'], 'actividades' => $verificacion['actividades']], 200);
                }
                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);

                DB::commit();
                $players = array();


                $cedula_alcalde =  ConfigSystem('cedula_alcalde');
                $telefonos_alacalde = DB::table('tma_playerid')
                    ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                    ->where('cedula', $cedula_alcalde)->get();
                $players = array();
                foreach ($telefonos_alacalde as $key => $value) {
                    array_push($players, $value->id_player);
                }
                $notificacion->notificacion($players, $actividad->id, '🔔  Actividad Aprobada', $actividad->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
            } elseif ($tipo == 6) { /////////SOLICITAR INFOMACIÓN

                // $actividad->id_estado = 9;
                $actividad->observacion_cancelacion = $observacion;
                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);

                DB::commit();

                $this->notificarAlcaldia($observacion, $actividad);
            } elseif ($tipo == 7) { /////////DELEGAR

                $actividad->id_estado = 8;

                $actividad->delegacion_alcalde = implode(',', json_decode($delegacion_alcalde));
                $actividad->disposicion_alcalde = $observacion;
                $actividad->recordar = "NO";
                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);

                DB::commit();


                $delegacion_alcalde = json_decode($delegacion_alcalde);
                if (is_array($delegacion_alcalde)) {
                    // dd($delegacion_alcalde);
                    $players = array();
                    $dele_ = '';
                    foreach ($delegacion_alcalde as $key => $value) {
                        $usuario = UsuariosModel::where('id', $value)->first();
                        if ($usuario) {
                            // dd("vale verga");
                            $telefonos_personas = DB::table('tma_playerid')
                                ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                                ->where('u.id', $usuario->id)->get();
                            foreach ($telefonos_personas as $key => $v) {
                                array_push($players, $v->id_player);
                            }
                            //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, '🔔  Ha sido delegado a una actividad de la agenda virtual',  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2,  $web_api->getActividad($actividad->id));
                            $notificacion->EnviarEmailAgenda($usuario->email, ' 🔔  Ha sido delegado a una actividad de la agenda virtual', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha:</b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin)) . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') .  '</div>', "coordinacioncronograma/agendavirtual");
                            $notificacion->notificacion($players, $actividad->id, '🔔  Ha sido delegado a una actividad de la agenda virtual',  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
                            $dele_ .= $usuario->name . '<br>';
                        }
                    }
                    //$notificacion->EnviarEmailAgenda($actividad->email_solicitante, 'Estimado, ' . $actividad->responsable . ' su actividad ha sido delegada', '<div style="padding: 5% 0 0 6%">   <b>Delegados: </b>' . $dele_ . '  <b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha: </b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin))  . '<br><b> Responsable: </b> ' . $actividad->responsable . '</div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '</div><div style="padding: 1% 0 0 6%"><b> Observación: </b> ' . $actividad->observacion . '</div>', "coordinacioncronograma/agendavirtual", "vistas.email", "NO");
                    $msg2 = 'Actividad fue delegada';
                    if ($actividad->coordinadores == 1 || $actividad->coordinadores == true) {
                        //    dd(10);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 10);
                    }
                    if ($actividad->concejales == 1 || $actividad->concejales == true) {

                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 25);
                    }
                    if ($actividad->misconcejales == 1 || $actividad->misconcejales == true) {

                        $this->notificarGrupito('🔔  ' . $msg2, $actividad);
                    }
                    if ($actividad->directores == 1 || $actividad->directores == true) {
                        // dd($actividad);

                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 3);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 5);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 15);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 22);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 29);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 9);
                    }

                    $this->notificarPersonalizada('🔔  ' . $msg2, $actividad);
                }
            } elseif ($tipo == 11) { /////////avisarle mas luego

                $actividad->id_estado = 11;
                // dd("vale verga");
                $actividad->disposicion_alcalde = $observacion;
                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);
                DB::commit();
                $this->notificarAlcaldia('🔔  Actividad en lista de espera', $actividad);
                $this->notificarRegistra('🔔  Actividad en lista de espera', $actividad);
            } elseif ($tipo == 8) { /////////avisarle mas luego

                // $verificacion = $this->verficar_existente($actividad->fecha_inicio, $actividad->fecha_fin, $actividad->id, 3);

                // if ($verificacion['estado'] == 'false') {
                //     return response()->json(['error' => 'Existe una o varias actividades en este rango de fecha y hora. ', 'html' => $verificacion['actividades']], 200);
                // }
                $msg2 = "Queda sin efecto la delegación del Sr.Alcalde porque el asistirá";
                if ($actividad->coordinadores == 1 || $actividad->coordinadores == true) {
                    //    dd(10);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 10);
                }
                if ($actividad->concejales == 1 || $actividad->concejales == true) {

                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 25);
                }
                if ($actividad->misconcejales == 1 || $actividad->misconcejales == true) {

                    $this->notificarGrupito('🔔  ' . $msg2, $actividad);
                }
                if ($actividad->directores == 1 || $actividad->directores == true) {
                    // dd($actividad);

                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 3);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 5);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 15);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 22);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 29);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 9);
                }
                $this->notificarDelgados('🔔  ' . $msg2, $actividad);
                $this->notificarPersonalizada('🔔  ' . $msg2, $actividad);
                $actividad->id_estado = 3;
                $actividad->delegacion_alcalde = null;
                $actividad->disposicion_alcalde = '';
                $actividad->save();

                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);
                DB::commit();
            } elseif ($tipo == 9) { /////////avisarle mas luego

                $msg2 = "El Sr.Alcalde se integrará a la actividad ";

                if ($actividad->coordinadores == 1 || $actividad->coordinadores == true) {
                    //    dd(10);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 10);
                }
                if ($actividad->concejales == 1 || $actividad->concejales == true) {

                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 25);
                }
                if ($actividad->misconcejales == 1 || $actividad->misconcejales == true) {

                    $this->notificarGrupito('🔔  ' . $msg2, $actividad);
                }

                if ($actividad->directores == 1 || $actividad->directores == true) {
                    // dd($actividad);

                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 3);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 5);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 15);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 22);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 29);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 9);
                }
                $this->notificarDelgados('🔔  ' . $msg2, $actividad);
                $this->notificarPersonalizada('🔔  ' . $msg2, $actividad);
                $actividad->id_estado = 13;
                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);
                DB::commit();
            } elseif ($tipo == 10) { /////////avisarle mas luego

                if ($actividad->confirmar_asistencia == 1) {
                    $actividad->confirmar_asistencia = 0;
                    $msg2 = "El Sr.Alcalde no asistirá a la actividad ";
                } else {
                    $actividad->confirmar_asistencia = 1;
                    $msg2 = "El Sr.Alcalde confirmó que asistirá a la actividad ";
                }
                // $actividad->confirmar_asistencia = 1;
                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);
                DB::commit();


                if ($actividad->coordinadores == 1 || $actividad->coordinadores == true) {
                    //    dd(10);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 10);
                }
                if ($actividad->concejales == 1 || $actividad->concejales == true) {

                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 25);
                }
                if ($actividad->misconcejales == 1 || $actividad->misconcejales == true) {

                    $this->notificarGrupito('🔔  ' . $msg2, $actividad);
                }
                if ($actividad->directores == 1 || $actividad->directores == true) {
                    // dd($actividad);

                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 3);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 5);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 15);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 22);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 29);
                    $this->notificarTodos('🔔  ' . $msg2, $actividad, 9);
                }
                $this->notificarDelgados('🔔  ' . $msg2, $actividad);

                $this->notificarPersonalizada('🔔  ' . $msg2, $actividad);
            } elseif ($tipo == 12) { /////////avisarle mas luego
                $asistencia = Input::get('asistencia');
                // dd($asistencia);
                if ($asistencia == "true") {
                    $actividad->asistio = 1;
                } else {
                    $actividad->asistio = 0;
                }

                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);
                DB::commit();
            } elseif ($tipo == 13) { 
                // dd($actividad);

                // $actividad->id_estado = 6;
                $actividad->estado = 'INA';
                $actividad->save();
                insertarhistorial($actividad, $actividad->id, 1, Auth::user()->id);
                DB::commit();
            } else {
                DB::commit();
            }




            return ['estado' => 'ok', 'msg' => 'Registro actualizado correctamente'];
        } catch (\Exception $th) {
            DB::rollback();
            return ['estado' => 'error', 'msg' => $th->getMessage() . ' ' . $th->getLine() . ' Ocurrió un error intentelo nuevamente, por favor......'];
        }
    }

    function verficar_existente($fecha_inicio, $fecha_fin, $id, $tipo = 0)
    {

        $actividad_inicial = null;
        $actividades = null;
        $reprogramar = 0;
        if ($tipo == 0) {
            // dd($id);
            if ($id == 0) {
                // $actividades = AgendaVirtualModel::where([['id_usuario', Auth::user()->id], ['fecha_inicio', '<=', $fecha_inicio], ['fecha_fin', '>', $fecha_inicio], ['id_estado', '<>', 7]])
                //     ->first();
                $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "users.id_perfil")->where("users.id", Auth::user()->id)->first();

                $actividades = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                    ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                    ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo')
                    ->whereIn('id_estado', [2, 3, 4, 8])
                    ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                        $query->where('fecha_inicio', '<=', $fecha_inicio)
                            ->where('fecha_fin', '>=', $fecha_fin)

                            ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                            ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                    })->get();
            } else {

                $actividades = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                    ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                    ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo')
                    ->whereIn('id_estado', [2, 3, 4, 8])
                    ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                        $query->where('fecha_inicio', '<=', $fecha_inicio)
                            ->where('fecha_fin', '>=', $fecha_fin)

                            ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                            ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                    })->get();
            }
        } elseif ($tipo == 3) {
            $actividad_inicial = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo')
                ->where([['com_tmov_comunicacion_agenda.id', $id]])
                ->get();

            $actividades = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo')
                ->whereIn('id_estado', [3, 4])
                // where([['com_tmov_comunicacion_agenda.id', '<>', $id], ['fecha_inicio', '<=', $fecha_inicio], ['fecha_fin', '>', $fecha_inicio], ['id_estado', 3]])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })->get();
        } elseif ($tipo == 4) {

            $actividad_inicial = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo')
                ->where([['com_tmov_comunicacion_agenda.id', $id]])
                ->get();

            $actividades = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo')
                ->whereIn('id_estado', [3, 4])
                ->where([['com_tmov_comunicacion_agenda.id', '<>', $id]])
                // where([['com_tmov_comunicacion_agenda.id', '<>', $id], ['fecha_inicio', '<=', $fecha_inicio], ['fecha_fin', '>', $fecha_inicio], ['id_estado', 3]])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })->get();
            // dd($actividades);

            $reprogramar = 1;
        } else {

            $actividades = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo')
                ->where([['com_tmov_comunicacion_agenda.id', '<>', $id]])
                ->whereIn('id_estado', [3, 4])
                // where([['com_tmov_comunicacion_agenda.id', '<>', $id], ['fecha_inicio', '<=', $fecha_inicio], ['fecha_fin', '>', $fecha_inicio], ['id_estado', 3]])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })->get();
        }


        if (isset($actividades[0])) {

            $act = "";
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "users.id_perfil")->where("users.id", Auth::user()->id)->first();
            if ($id_tipo_pefil->id_perfil == 12 || $id_tipo_pefil->id_perfil == 16 || $id_tipo_pefil->id_perfil == 1) {
                $act = '<table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">ACTIVIDAD</th>
                <th scope="col">DESCRIPCIÓN</th>
                <th scope="col">DIRECCIÓN</th>
                <th scope="col">INICIO</th>
                <th scope="col">FIN</th>
              </tr>
            </thead>
            <tbody>';
                if ($actividad_inicial != null) {
                    foreach ($actividad_inicial as $key => $v) {
                        $act .= '<tr id="fila_' . $v->id . '">';
                        $act .= '<td style="text-align: justify;width: 25em;"><i class="fa fa-newspaper-o" aria-hidden="true"></i>  <b>' . $v->nombre . '</b><br><span class="badge badge-secondary">' . $v->tipo . '</span> </td>';
                        // if ($reprogramar == 1) {
                        //     $act .= '<th style="width: 18em;">  <div class="input-group datetime"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input data-placement="top" data-toogle="tooltip" class="form-control datefecha" placeholder="Inicio" value="' . date('Y-m-d H:i:s', strtotime($v->fecha_inicio)) . '" name="fecha_inicio" type="text" id="fecha_inicio_' . $v->id . '"></div></th>';
                        //     $act .= '<th style="width: 18em;"><div class="input-group datetime"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input data-placement="top" data-toogle="tooltip" class="form-control datefecha" placeholder="Fin" value="' . date('Y-m-d H:i:s', strtotime($v->fecha_fin)) . '" name="fecha_fin" type="text" id="fecha_fin_' . $v->id . '"></div></td>';
                        //     $act .= '<th style="width: 9em;">Ninguno</td>';
                        //     $act .= '<th  style="width: 10em;"><input id="obs_' . $v->id . '" style=" height: 2.5em;" class="swal2-text"></td>';
                        //     $act .= '<th style="width: 11em;"><a id="reprogramar_' . $v->id . '" class="fa fa-check btn btn-light" style="width: 100%; height: 2.5em; color: #fff;background-color: #387947;border-color: #387947;" onclick="atender_modal(\'' . $v->id . '\',4,\'aaa\',\'' . $v->fecha_inicio . '\',\'' . $v->fecha_fin . '\',\'' . Crypt::encrypt($v->id) . '\')" > REPROGRAMAR</a>';
                        // } else {
                        $act .= '<td  style="width: 15em; text-align: justify;">' . $v->descripcion . '</td>';
                        $act .= '<td  style="width: 15em; text-align: justify;">' . $v->direccion . '</td>';
                        $act .= '<td style="width: 9.5em; text-align: justify;">' . date('Y-m-d H:i:s', strtotime($v->fecha_inicio)) . '</td>';
                        $act .= '<td style="width: 9.5em; text-align: justify;">' . date('Y-m-d H:i:s', strtotime($v->fecha_fin)) . '</td>';
                        // $act .= '<th style="width: 9em;">Ninguno</td>';
                        // $act .= '<th style="width: 11em;"><a id="aprobar_' . $v->id . '" class="fa fa-check btn btn-light" style="width: 100%; height: 2.5em; color: #fff;background-color: #387947;border-color: #387947;" onclick="atender_modal(\'' . $v->id . '\',3,\'aaa\',\'' . $v->fecha_inicio . '\',\'' . $v->fecha_fin . '\',\'' . Crypt::encrypt($v->id) . '\')" > APROBAR</a>';
                        // }
                        $act .= '</tr>';
                    }
                }
                foreach ($actividades as $key => $value) {
                    $act .= '<tr id="fila_' . $value->id . '">';
                    $act .= '<td style="text-align: justify;width: 25em;"><i class="fa fa-newspaper-o" aria-hidden="true"></i>  <b>' . $value->nombre . '</b> <br><span class="badge badge-secondary">' . $value->tipo . '</span></td>';
                    // $act .= '<th style="width: 9.5em;"><div class="input-group datetime"><span class="input-group-addon" ><span class="glyphicon glyphicon-calendar"></span></span><input data-placement="top" style="width: 9.5em;" data-toogle="tooltip" class="form-control datefecha" placeholder="Inicio" value="' . date('Y-m-d H:i:s', strtotime($value->fecha_inicio)) . '" name="fecha_inicio" type="text" id="fecha_inicio_' . Crypt::encrypt($value->id) . '"></div></th>';
                    // $act .= '<th style="width: 9.5em;"><div class="input-group datetime"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input data-placement="top" style="width: 9.5em;" data-toogle="tooltip" class="form-control datefecha" placeholder="Inicio" value="' . date('Y-m-d H:i:s', strtotime($value->fecha_fin)) . '" name="fecha_inicio" type="text" id="fecha_inicio_' . Crypt::encrypt($value->id) . '"></div></td>';
                    $act .= '<td  style="width: 15em; text-align: justify;">' . $value->descripcion . '</td>';
                    $act .= '<td  style="width: 15em;text-align: justify;">' . $value->direccion . '</td>';
                    $act .= '<td style="width: 11em;text-align: justify;">' . date('Y-m-d H:i:s', strtotime($value->fecha_inicio)) . '</td>';
                    $act .= '<td style="width: 11em;text-align: justify;">' . date('Y-m-d H:i:s', strtotime($value->fecha_fin)) . '</td>';

                    // $act .= '<th style="width: 9em;"><select class="chosen-select" id="delegados_' . $value->id . '" multiple  name="delegados"></select></td>';
                    // $act .= '<th style="width: 11em;"><a id="lista_' . $value->id . '" class="fa fa-list-ul btn btn-dark" style="width: 100%; height: 2.5em;" onclick="atender_modal(\'' . $value->id . '\',1,\'aaa\',\'' . $value->fecha_inicio . '\',\'' . $value->fecha_fin . '\',\'' . Crypt::encrypt($value->id) . '\')" > LISTA DE ESPERA</a><br><br><a id="delegar_' . $value->id . '" class="fa fa-users btn btn-info" style="width: 100%;" onclick="atender_modal(\'' . $value->id . '\',2,\'aaa\',\'' . $value->fecha_inicio . '\',\'' . $value->fecha_fin . '\',\'' . Crypt::encrypt($value->id) . '\')" > DELEGAR</a></td>';
                    $act .= '</tr>';
                }
                $act .= '</tbody>
            </table>';
            }
            return ['estado' => 'false', 'msg' => 'Existe una actividad en este rango de fechas', 'actividades' => $act];
        } else {
            return ['estado' => 'true', 'msg' => 'ok...'];
        }
    }


    function verficar_existente_dos($fecha_inicio, $id, $tipo, $categoria)
    {
        if ($tipo == 1) {
            $actividades = AgendaVirtualModel::where([['id', '<>', $id], ['fecha_inicio', '<=', $fecha_inicio], ['fecha_fin', '>', $fecha_inicio]])
                ->whereIn('id_estado', [3, 4, 8])
                ->first();

            if ($actividades) {
                return ['estado' => 'false', 'msg' => 'Existe una actividad en este rango de fecha'];
            } else {
                return ['estado' => 'true', 'msg' => 'ok...'];
            }
        } elseif ($tipo == 2) {
            $fecha_inicio_ = date('Y-m-d', strtotime($fecha_inicio));
            $fecha_inicio = $fecha_inicio_ . ' 00:00:00';
            $fecha_fin = $fecha_inicio_ . ' 23:59:59';

            $barrios = AgendaVirtualModel::where([['id', '<>', $id], ['fecha_inicio', '>=', $fecha_inicio], ['fecha_inicio', '<=', $fecha_fin]])
                ->whereIn('id_estado', [2, 3, 4, 8])
                ->where('id_tipo', 4)
                ->count();

            // dd($barrios);
            $ciudadano = AgendaVirtualModel::where([['id', '<>', $id], ['fecha_inicio', '>=', $fecha_inicio], ['fecha_inicio', '<=', $fecha_fin]])
                ->whereIn('id_estado', [2, 3, 4, 8])
                ->where('id_tipo', 5)
                ->count();

            $gremios = AgendaVirtualModel::where([['id', '<>', $id], ['fecha_inicio', '>=', $fecha_inicio], ['fecha_inicio', '<=', $fecha_fin]])
                ->whereIn('id_estado', [2, 3, 4, 8])
                ->where('id_tipo', 6)
                ->count();

            // dd($barrios.$ciudadano.$gremios);

            if ($ciudadano == 4) {
                return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            }

            if ($barrios + $gremios == 4) {
                return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            }

            if ($ciudadano == 3 && ($barrios + $gremios) == 1) {
                return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            }

            if ($ciudadano == 2 && ($barrios + $gremios) == 2) {
                return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            }

            if ($ciudadano == 1 && ($barrios + $gremios) == 3) {
                return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            }

            return ['estado' => 'true', 'msg' => 'ok...'];
        }
    }

    public function eliminar_archivo($id)
    {
        try {
            DB::beginTransaction();
            $archivo = AgendaArchivosModel::find($id);
            $dir = public_path() . '/archivos_sistema/';
            File::delete($dir . '/' . $archivo->ruta);
            $archivo->delete();
            DB::commit();
            return ['estado' => 'ok', 'msg' => 'Registro actualizado correctamente'];
        } catch (\Exception $th) {
            DB::rollback();
            return ['estado' => 'error', 'msg' => $th->getMessage() . ' ' . $th->getLine() . ' Ocurrió un error intentelo nuevamente, por favor......'];
        }
    }

    public function notificarGrupito($titulo, $actividad)
    {
        // dd($perfil);
        $web_api = new WebApiAgendaController;
        $notificacion = new NotificacionesController;
        $misconcejales = explode("|", ConfigSystem("misconcejales"));

        $telefonos_personas = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->join('ad_perfil as p', 'p.id', '=', 'u.id_perfil')
            ->whereIn('u.id', $misconcejales)
            ->get();

        $usuarios = UsuariosModel::whereIn('id', $misconcejales)->get();
        foreach ($usuarios as $key => $value_) {
            $notificacion->EnviarEmailAgenda($value_->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");
        }

        $players = array();
        foreach ($telefonos_personas as $key => $value) {
            array_push($players, $value->id_player);
            // $notificacion->EnviarEmailAgenda($value->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");

        }
        $notificacion->notificacion($players, $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
    }
}
