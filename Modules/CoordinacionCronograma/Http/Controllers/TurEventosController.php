<?php

namespace Modules\Coordinacioncronograma\Http\Controllers;

use App\ArchivosModel;
use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use GeneaLabs\LaravelMaps\Facades\Map;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Modules\CoordinacionCronograma\Entities\TurEventosCategoriasModel;
use Modules\CoordinacionCronograma\Entities\TurEventosModel;

// use Illuminate\Http\Request;

class TurEventosController extends Controller
{

    var $configuraciongeneral = array("Agenda Cultural", "coordinacioncronograma/agendaturistica", "index", 6 => "coordinacioncronograma/agendaturisticaajax", 7 => "agendaturistica");
    var $escoja = array(null => "Escoja opción...");

    var $objetos = '[
        {"Tipo":"select","Descripcion":"Tipo de evento","Nombre":"id_categoria","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Actividad(*)","Nombre":"evento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Descripción del evento","Nombre":"descripcion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetimetext","Descripcion":"Fecha y hora de inicio(*)","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetimetext","Descripcion":"Fecha y hora de fin(*)","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Dirección del evento","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"mapa","Descripcion":"MAPA(*)","Nombre":"mapa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Latitud","Nombre":"latitud","Clase":"disabled ","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Longitud","Nombre":"longitud","Clase":"disabled ","Valor":"Null","ValorAnterior" :"Null" }
		]';

    var $validarjs = array(
        "nombre" => "nombre: {
                required: true
            }"
    );

    const LUNES = 1;
    const MARTES = 2;
    const MIERCOLES = 3;
    const JUEVES = 4;
    const VIERNES = 5;
    const SABADO = 6;
    const DOMINGO = 7;
    const MIN_EVENTO = 1;
    const MAX_EVENTO = 23;
    const DURACION_ENTRE_EVENTOS = 15;
    const DURACION_MINIMA_EVENTOS = 15;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $delete = 'si';
        $create = 'si';


        $objetos = json_decode($this->objetos);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[8]);

        $filtros = '[
            {"Tipo":"select","Descripcion":"Cantidad a mostar","Nombre":"length","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"text","Descripcion":"Campo de busqueda","Nombre":"campos","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"date","Descripcion":"Inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"date","Descripcion":"Fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';
        $filtros = json_decode($filtros);
        $filtros[0]->Valor = [10 => '10', 10 => '10', 50 => '50', 100 => '100', 10000 => 'TODOS'];
        // dd($actividades);
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => [],
            "actividades_agenda" => [],
            "configuraciongeneral" => $this->configuraciongeneral,
            "create" => $create,
            "delete" => $delete,
            "id_tipo_pefil" => $id_tipo_pefil,
            "filtros" => $filtros,
            "validar_AgendaTurismo" => 'SI'
        ]);
    }
    function getActividadesAgendaTuristica()
    {
        $filtros = Input::all();
        if (isset($filtros["estado_aprobacion"])) {
            $carbon_fecha = new \Carbon\Carbon();
            $fecha_inicio = $filtros['fecha_inicio'];
            if ($fecha_inicio == '') {
                $fecha_inicio = '2019-01-01 00:00:00';
            } else {
                $fecha_inicio = $filtros['fecha_inicio'] . ' 00:00:00';
            }
            $fecha_fin = $filtros['fecha_fin'];
            if ($fecha_fin == '') {
                $fecha_fin = $carbon_fecha->addYear();
            } else {
                $fecha_fin = $filtros['fecha_fin'] . ' 23:59:59';
            }

            $actividades = TurEventosModel::
            select('tur_agenda.id','tur_agenda.id_categoria',
            'tur_agenda.id_categoria',
            'c.categoria',
            'tur_agenda.evento',
            'tur_agenda.descripcion',
            'tur_agenda.fecha_inicio',
            'tur_agenda.fecha_fin',
            'tur_agenda.direccion',
            'tur_agenda.longitud',
            'tur_agenda.latitud')->join('tur_agenda_categoria as c', 'c.id', 'tur_agenda.id_categoria')
                ->where(['tur_agenda.estado' => 'ACT'])
                ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin])
                ->where(function ($query) use ($filtros) {
                    $query->where('tur_agenda.id', 'LIKE', "%{$filtros["campos"]}%")
                        ->orWhere('tur_agenda.nombre', 'LIKE', "%{$filtros["campos"]}%")
                        ->orWhere('direccion', 'LIKE', "%{$filtros["campos"]}%");
                });
        } else {
            $actividades = TurEventosModel::join('tur_agenda_categoria as c', 'c.id', 'tur_agenda.id_categoria')
                ->where(['tur_agenda.estado' => 'ACT']);
        }

        $actividades = $actividades->get();

        $actividades_collect = collect($actividades);
        $multiplied = $actividades_collect->map(function ($item, $key) {
            $edit = link_to_route('' . $this->configuraciongeneral[7] . '.edit', ' EDITAR', array($item->id), array('class' => 'fa fa-pencil-square-o btn btn-light', 'style' => 'color: #fff;background-color: #17a2b8;border-color: #17a2b8'));
            $reprogramar = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-calendar btn btn-danger" onclick="atender(\'' . Crypt::encrypt($item->id) . '\',4,\'aaa\',\'' . $item->fecha_inicio . '\',\'' . $item->fecha_fin . '\')" > REPROGRAMAR</a>';
            $suspender = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-times btn btn-danger" onclick="atender(\'' . Crypt::encrypt($item->id) . '\',3,\'aaa\')" > SUSPENDER</a>';
            $acciones = $edit;
            $color = '#2196F3';
            return collect(
                [
                    'id' => $item->id,
                    'title' => substr($item->tipo, 0, 3) . '. ' . $item->evento,
                    'start' => date('Y-m-d H:i:s', strtotime($item->fecha_inicio)),
                    'end' => date('Y-m-d H:i:s', strtotime($item->fecha_fin)),
                    'description' => str_replace(array("\r", "\n"), '', $item->descripcion),
                    'acciones' => str_replace(array("\r", "\n"), '', $acciones),
                    'color' => $color
                ]
            );
        });

        return $multiplied;
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        //////// mapa //////
        $config['center'] = '-0.9484704,-80.7237787';
        //$config['center'] = 'auto';
        $config['zoom'] = 'auto';
        $config['onclick'] = '$("#latitud").val(event.latLng.lat()); $("#longitud").val(event.latLng.lng()); 
        alert("sss");
        marker_0.setOptions({
            position: new google.maps.LatLng(event.latLng.lat(),event.latLng.lng())
        });';
        Map::initialize($config);
        $marker = array();
        Map::add_marker($marker);
        $data['map'] = Map::create_map();
        $objetos = json_decode($this->objetos);
        $categorias = TurEventosCategoriasModel::where('estado', 'ACT')->pluck('categoria', 'id')->all();
        $objetos[0]->Valor = $categorias;
        $objetos[0]->Adicional = '<button id="agregar_categoria" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar categoría </button>';
        $this->configuraciongeneral[2] = "crear";
        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
            "mapa" => $data,
            "validar_Agenda" => "si",
            "validar_AgendaTurismo"=>'si'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //

        return $this->guardar(0);
    }


    public function guardar($id)
    {
        try {
            DB::beginTransaction();
            //code...
            $notificacion = new NotificacionesController;
            $input = Input::all();
            $ruta = $this->configuraciongeneral[1];

            if ($id == 0) {
                $ruta .= "/create";
                $guardar = new TurEventosModel();
                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro Variable de Configuración";
            } else {
                $ruta .= "/$id/edit";
                $guardar = TurEventosModel::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Edición Variable de Configuración";
            }

            $validator = Validator::make($input, TurEventosModel::rules($id));

            if ($validator->fails()) {
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            } else {
                foreach ($input as $key => $value) {
                    if ($key == 'fecha_inicio') {
                        $carbon1 = new \Carbon\Carbon();
                        $carbon2 = new \Carbon\Carbon($value);
                        $minutesDiff = $carbon1->diffInMinutes($carbon2, true);
                        // dd($minutesDiff);
                        // dd($value);

                        if ($minutesDiff < 0) {
                            return Redirect::to("$ruta")
                                ->withWarning('La fecha de inicio debe ser mayor a la actual')
                                ->withInput();
                        }
                    }
                    if ($key != "_method" && $key != "_token") {
                        $guardar->$key = $value;
                    }
                }
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                $guardar->save();


                DB::commit();
                // insertarhistorial($guardar, $idcab, 1, Auth::user()->id);
                Auditoria($msgauditoria . " - ID: " . $id . "-" . Input::get($guardar->evento));
            }
            Session::flash('message', $msg);
            return Redirect::to($this->configuraciongeneral[1].'/'.$guardar->id.'/edit');
        } catch (\Throwable $th) {
            DB::rollback();
            Session::flash('error', 'Ocurrio un problema...');
            return $th;
            //throw $th;
        }
    }



    public function agendaturisticaajax(Request $request)
    {
        $columns = array(
            0 => 'tur_agenda.id',
            1 => 'evento',
            2 => 'descripcion',
            3 => 'direccion',
            4 => 'fecha_inicio',
            5 => 'fecha_fin',
            6 => 'acciones'
        );

        $fecha_inicio = $request->fecha_inicio;
        $fecha_fin = $request->fecha_fin;
        $estado = $request->estado_aprobacion;
        $carbon_fecha = new \Carbon\Carbon();
        if ($fecha_inicio == '') {
            $fecha_inicio = '2019-01-01 00:00:00';
        } else {
            $fecha_inicio =  $fecha_inicio . ' 00:00:00';
        }


        if ($fecha_fin == '') {
            $fecha_fin = $carbon_fecha->addYear();
        } else {
            $fecha_fin = $fecha_fin . ' 23:59:59';
        }
        // dd($estado);
        $query = TurEventosModel::
            select('tur_agenda.id','tur_agenda.id_categoria',
            'tur_agenda.id_categoria',
            'c.categoria',
            'tur_agenda.evento',
            'tur_agenda.descripcion',
            'tur_agenda.fecha_inicio',
            'tur_agenda.fecha_fin',
            'tur_agenda.direccion',
            'tur_agenda.longitud',
            'tur_agenda.latitud')->join('tur_agenda_categoria as c', 'c.id', 'tur_agenda.id_categoria')
            ->where(['tur_agenda.estado' => 'ACT'])
            ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin]);

        $totalData = $query->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value')) && empty($request->input('campos'))) {
            $posts =  $query

                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            // $search = $request->input('search.value');
            $search = $request->input('campos');

            $posts =   $query

                ->where(function ($query) use ($search) {
                    $query->where('tur_agenda.id', 'LIKE', "%{$search}%")
                        ->orWhere('tur_agenda.evento', 'LIKE', "%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered =  $query
                ->where(function ($query) use ($search) {
                    $query->where('tur_agenda.id', 'LIKE', "%{$search}%")
                        ->orWhere('tur_agenda.evento', 'LIKE', "%{$search}%");
                })
                ->count();
        }

        $data = array();
        // dd($posts);
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {

                $edit = '';
                $edit.= '&nbsp;&nbsp;' . link_to_route('' . $this->configuraciongeneral[7] . '.show', ' VER', array($post->id), array('class' => 'fa fa-newspaper-o divpopup btn btn-primary', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;';
                $edit.= link_to_route('' . $this->configuraciongeneral[7] . '.edit', ' EDITAR', array($post->id), array('class' => 'fa fa-pencil-square-o btn btn-light', 'style' => 'color: #fff;background-color: #17a2b8;border-color: #17a2b8'));
                $reprogramar = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-calendar btn btn-warning" onclick="atender(\'' . Crypt::encrypt($post->id) . '\',4,\'aaa\',\'' . $post->fecha_inicio . '\',\'' . $post->fecha_fin . '\')" > REPROGRAMAR</a>';
                $suspender = '&nbsp;&nbsp;<a id="aprobar" class="fa fa-times btn btn-danger" onclick="atender(\'' . Crypt::encrypt($post->id) . '\',3,\'aaa\')" > SUSPENDER</a>';
                // $edit = $edit  . "&nbsp;&nbsp;" . $reprogramar . $suspender;
                $acciones = $edit;
                $nestedData['id'] = $post->id;
                $nestedData['id_categoria'] = $post->id_categoria;
                $nestedData['evento'] = $post->evento;
                $nestedData['descripcion'] = $post->descripcion;
                $nestedData['direccion'] = $post->direccion;
                $nestedData['fecha_inicio'] = $post->fecha_inicio;
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['acciones'] = $acciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $objetos = json_decode($this->objetos);
        $tabla = TurEventosModel::join('tur_agenda_categoria as c', 'c.id', 'tur_agenda.id_categoria')
            ->where(['tur_agenda.estado' => 'ACT', "tur_agenda.id" => $id])->first();

        // dd($tabla);
        $latitud = $tabla->latitud;
        $longitud = $tabla->longitud;
        $autozoom = "18";

        if ($latitud == "") {
            $latitud = "-0.9484126160642922";
            //$autozoom="18";
        }
        if ($longitud == "")
            $longitud = "-80.72165966033936";
        $config['center'] = "$latitud,$longitud";
        $config['zoom'] = $autozoom; //'auto';

        Map::initialize($config);
        //Marker
        $marker = array();
        $marker['position'] = "$latitud,$longitud";
        $marker['draggable'] = false;

        Map::add_marker($marker);
        $data['map'] = Map::create_map();
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[8]);
        // show($objetos);


        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            //"validararry"=>$validararry
            "mapa" => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $objetos = json_decode($this->objetos);
        $categorias = TurEventosCategoriasModel::where('estado', 'ACT')->pluck('categoria', 'id')->all();
        $objetos[0]->Valor = $categorias;
        $this->configuraciongeneral[2] = "editar";
        $tabla = TurEventosModel::find($id);
        $objetos[0]->ValorAnterior = $tabla->id_categoria;
        $objetos[0]->Adicional = '<button id="agregar_categoria" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar categoría </button>';

        $latitud = $tabla->latitud;
        $longitud = $tabla->longitud;
        $autozoom = "18";
        $actiondrag = '$("#latitud").val(event.latLng.lat()); $("#longitud").val(event.latLng.lng()); marker_0.setOptions({
            position: new google.maps.LatLng(event.latLng.lat(),event.latLng.lng())
        });';
        if ($latitud == "") {
            $latitud = "-0.9484126160642922";
            //$autozoom="18";
        }
        if ($longitud == "")
            $longitud = "-80.72165966033936";
        $config['center'] = "$latitud,$longitud";
        $config['zoom'] = $autozoom; //'auto';
        $config['onclick'] = $actiondrag;
        Map::initialize($config);
        //Marker
        $marker = array();
        $marker['position'] = "$latitud,$longitud";
        $marker['draggable'] = true;
        $marker['ondragend'] = $actiondrag;
        Map::add_marker($marker);
        $data['map'] = Map::create_map();

        $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "=", "pdf"], ["tipo", "20"]])->get();
        $dicecionesImagenes = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "<>", "pdf"], ["tipo", "20"]])->get();
        $this->configuraciongeneral[3] = "20"; //Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "si";
        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "tabla" => $tabla,
            "mapa" => $data,
            "validarjs" => $this->validarjs,
            "validar_Agenda" => "si",
            'dicecionesImagenes' => $dicecionesImagenes,
            'dicecionesDocumentos' => $dicecionesDocumentos,
            "validar_AgendaTurismo"=>'si',
            'eliminar_archivo'=>'si'
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $tabla = TurEventosModel::find($id);
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
