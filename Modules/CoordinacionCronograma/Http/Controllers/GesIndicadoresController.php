<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\CoordinacionCronograma\Entities\GesIndicadorModel;
use stdClass;

class GesIndicadoresController extends Controller
{


    var $configuraciongeneral = array("Administración Indicadores de Gestión", "coordinacioncronograma/indicadoresgestionmain", "index", 6 => "coordinacioncronograma/indicadoresgestionmainajax", 7 => "indicadoresgestionmain");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Nombre","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }

                  ]';

    public function __construct()
    {
        $this->middleware('auth');
    }
    var $validarjs = array(
        "id_direccion" => "proyecto: {
                            required: true
                        }",
        "nombre" => "nombre: {
                            required: true
                        }"
    );

    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        }
        //show($object);
        return $object;
    }


    public function verpermisos($objetos = array(), $tipo = "index")
    {
        

        if ($tipo == "index") {
            unset($objetos[4]);
            $tabla = GesIndicadorModel::join("tmae_direcciones as a", "a.id", "=", "ges_tmae_gestion_indicador_main.id_direccion")
                ->select("ges_tmae_gestion_indicador_main.id","ges_tmae_gestion_indicador_main.nombre", "a.direccion as id_direccion")
                ->where("ges_tmae_gestion_indicador_main.estado", "ACT");
        } else {
            $tabla = GesIndicadorModel::where("ges_tmae_gestion_indicador_main.estado", "ACT");
        }
        return array($objetos, $tabla);
    }

    function indicadoresgestionmainajax(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        //show($tabla);
        $objetos = $tabla[0];
        $tabla = $tabla[1];

        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        //show($columns);
        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('ges_tmae_gestion_indicador_main.id', 'LIKE', "%{$search}%")
                    ->orWhere('ges_tmae_gestion_indicador_main.nombre', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            $permisos = $this->perfil();
            foreach ($posts as $post) {
                //=============================================================
                $show = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                //'&nbsp;&nbsp;' .
                $edit = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o'));
                //. '&nbsp;&nbsp;
                $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                if ($permisos->ver == 'NO')
                    $show = "";
                if ($permisos->editar == 'NO')
                    $edit = "";
                if ($permisos->eliminar == 'NO')
                    $dele = "";
                $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                //=============================================================
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $valorc = $post->$campo;
                    $nestedData["$value->Nombre"] = "$valorc";
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = $this->verpermisos(json_decode($this->objetos));

        return view('vistas.index', [
            "objetos" => $objetos[0],
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si",
            "permisos" => $this->perfil()
        ]);
    }

    public function asignaroles($objetos, $permisos, $tabla)
    {
        if ($permisos->editar_parcialmente == 'SI' && $permisos->editar == 'NO') {
            //show($objetos);
            foreach ($objetos as $key => $value) {
                if ($value->Tipo == "select" && $value->Nombre != "avance") {
                    $objetos[$key]->Tipo = "selectdisabled";
                    //show($tabla);
                    if ($objetos[$key]->ValorAnterior != "Null") {

                        $pos = $objetos[$key]->ValorAnterior;
                        if (!is_array($pos))
                            $objetos[$key]->Valor = array($pos => $value->Valor[$pos]);
                        else
                            foreach ($pos as $keyaa => $vaa)
                                $objetos[$key]->Valor = array($keyaa => $value->Valor[$vaa]);
                    } else {
                        //
                        $in = $objetos[$key]->Nombre;
                        $kk = $tabla->$in;
                        $aar = $objetos[$key]->Valor;
                        if (array_key_exists($kk, $aar)) {
                            //$objetos[$key]->Tipo = "html2";
                            $objetos[$key]->Valor = array($kk => $aar[$kk]);
                        }
                    }
                }
            }
        }
        return $objetos;
    }

    public function formularioscrear($id)
    {


        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);
        /*Dirección*/
        $seleccionar_direccion = direccionesModel::where("estado", "ACT")->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        $objetos[0]->Valor = $this->escoja + $seleccionar_direccion;
        if ($id != "") {
            $this->configuraciongeneral[2] = "editar";
            $id = Crypt::decrypt($id);
            $tabla = GesIndicadorModel::find($id);
            $objetos = $this->asignaroles($objetos, $this->perfil(), $tabla);
            //show($objetos);
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
            );
            /*Setear*/
        } else {
            $this->configuraciongeneral[2] = "crear";
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            );
        }

        //show($datos);
        return view('vistas.create', $datos);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return $this->formularioscrear("");
    }
    public function guardar($id)
    {
        //show($id);
        $input = Input::all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];
        DB::beginTransaction();
        try {
            if ($id == 0) {
                $ruta .= "/create";
                $guardar = new GesIndicadorModel;
                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro Compromiso Alcalde";
            } else {
                $ruta .= "/" . Crypt::encrypt($id) . "/edit";
                $guardar = GesIndicadorModel::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Compromiso Alcalde modificado";
            }

            //$input = Input::all();
            $validar = GesIndicadorModel::rules($id);
            $validator = Validator::make($input, $validar);

            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            } else {

                foreach ($input as $key => $value) {

                    if ($key != "_method" && $key != "_token") {
                        $guardar->$key = $value;
                    }
                }
                $guardar->save();
                DB::commit();
                Session::flash('message', $msg);
                return Redirect::to($this->configuraciongeneral[1]);
            } //
        } catch (\Exception $e) {
            // ROLLBACK
            DB::rollback();
            $mensaje = $e->getMessage();
            return redirect()->back()->withErrors([$mensaje])->withInput();
        }
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("ges_tmae_gestion_indicador_main.id", $id)->first();
        return view('vistas.show', [
            "objetos" => $objetos[0],
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //


        DB::beginTransaction();
        try {
            $tabla = GesIndicadorModel::find($id);
            $tabla->estado = 'INA';
            $tabla->save();
            DB::commit();
            Session::flash('message', 'El registro se eliminó Exitosamente!');
            return Redirect::to($this->configuraciongeneral[1]);
        } //Try Transaction
        catch (\Exception $e) {
            //  // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);
        }
    }
}
