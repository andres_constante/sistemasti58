<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\ArchivosModel;
use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020ActividadesFisicasModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020ActividadesFisicasTipoModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020ArbolesSembradosLugaresModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020ArbolesSembradosModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020ArbolesSembradosServiciosModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020ControlTerritorialModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020DetaModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020EventosModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020EventosTiposArtistaModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020EventosTiposModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020FaunaModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020FaunaRazaMascota;
use Modules\CoordinacionCronograma\Entities\Indicadores2020FaunaTipoAtencionModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020FaunaTipoMascota;
use Modules\CoordinacionCronograma\Entities\Indicadores2020MantenimientoAreasModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020MantenimientoAreasServicioModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020Model;
use Modules\CoordinacionCronograma\Entities\Indicadores2020SancionesModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020SancionesTiposModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020SeguridadCiudadanaModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020SeguridadCiudadanaServiciosModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020TipoModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020TransitoModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020TransitoTipoTramiteModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020TurismoModel;
use Modules\CoordinacionCronograma\Entities\Indicadores2020TurismoServiciosModel;
use stdClass;

class IndicadoresFichasController extends Controller
{

    var $configuraciongeneral = array("FICHAS DE INDICADORES", "coordinacioncronograma/indicadores", "index", 6 => "coordinacioncronograma/indicadoresajax", 7 => "indicadores");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"select","Descripcion":"Tipo indicador","Nombre":"id_indicador","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },               
        {"Tipo":"datetext","Descripcion":"Fecha de registro","Nombre":"fecha_registro","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },               
        {"Tipo":"text","Descripcion":"Cédula","Nombre":"cedula","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },               
        {"Tipo":"text","Descripcion":"Pais","Nombre":"pais","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Nombres","Nombre":"nombres","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Apellidos","Nombre":"apellidos","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Discapacidad","Nombre":"discapacidad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"html","Descripcion":"Lista de codigos","Nombre":"clave_catastral_html","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Parroquia","Nombre":"parroquia_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Barrio","Nombre":"barrio_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Dirección Domiciliaria","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Código catastral","Nombre":"clave_catastral","Clase":"solonumeros ","Valor":"Null","ValorAnterior" :"Null" }
        ]';

    var $validarjs = array(
        "id_direccion" => "id_direccion: {
                            required: true
                        }",
        "filtproyectoro" => "proyecto: {
                            required: true
                        }"
    );
    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }
    public function perfil($verificar = 'SI')
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        //show($id_tipo_pefil);
        // if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $verificar == 'NO') { //1 Administrador 4 Coordinador
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $id_tipo_pefil->tipo == 10) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';

            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        }

        return $object;
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = '[
            {"Tipo":"select","Descripcion":"Tipo indicador","Nombre":"id_indicador","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },               
            {"Tipo":"select","Descripcion":"Parroquia","Nombre":"parroquia_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Barrio","Nombre":"barrio_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Detalle","Nombre":"detalle","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Fecha de creación","Nombre":"created_at","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" }
            ]';
        $objetos = json_decode($objetos);
        $filtros = json_decode('[
            {"Tipo":"select","Descripcion":"Direccion","Nombre":"dieccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Tipo indicador","Nombre":"id_indicador","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },               
            {"Tipo":"select","Descripcion":"Parroquia","Nombre":"parroquia_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Barrio","Nombre":"barrio_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"text","Descripcion":"Cedula","Nombre":"cedula","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
            ]');

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'id_direccion')->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
            $filtros[0]->Valor = Indicadores2020TipoModel::from('ind2020_tmae_tipo as t')
                ->join("tmae_direcciones as d", "d.id", "=", "t.id_direccion")
                ->where(['t.estado' => 'ACT', 'd.id' => Auth::user()->id_direccion])->pluck('direccion', 'd.id')->all();


            $filtros[1]->Valor = [0 => 'TODOS'] + Indicadores2020TipoModel::from('ind2020_tmae_tipo as t')
                ->join("tmae_direcciones as d", "d.id", "=", "t.id_direccion")
                ->where(['t.estado' => 'ACT', 'd.id' => Auth::user()->id_direccion])->pluck('tipo', 't.id')->all();
        } else {
            $filtros[0]->Valor = [0 => 'TODOS'] + Indicadores2020TipoModel::from('ind2020_tmae_tipo as t')
                ->join("tmae_direcciones as d", "d.id", "=", "t.id_direccion")
                ->where(['t.estado' => 'ACT'])->pluck('direccion', 'd.id')->all();
            $filtros[1]->Valor = [0 => 'TODOS'] + Indicadores2020TipoModel::from('ind2020_tmae_tipo as t')
                ->where(['t.estado' => 'ACT'])->pluck('tipo', 'id')->all();
        }
        $filtros[2]->Valor =  [0 => 'TODOS'] +  parroquiaModel::where('estado', 'ACT')->pluck('parroquia', 'id')->all();
        $filtros[3]->Valor =  [0 => 'TODOS'] + barrioModel::where(['estado' => 'ACT'])->pluck('barrio', 'id')->all();
        // dd($filtros);

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "delete" => "si",
            "create" => "si",
            "configuraciongeneral" => $this->configuraciongeneral,
            "permisos" => $this->perfil(),
            'filtros' => $filtros
        ]);
    }

    public function formularioscrear($id = "")
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        /*QUITAR CAMPOS*/
        $objetos = json_decode($this->objetos);

        $objetosmain = $this->verpermisos($objetos, "crear");
        // show($objetos);
        // dd($id_tipo_pefil);
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) {
            $objetos[0]->Valor = Indicadores2020TipoModel::where('estado', 'ACT')->pluck('tipo', 'id')->all();
        } else {
            $objetos[0]->Valor = Indicadores2020TipoModel::where(['estado' => 'ACT', 'id_direccion' => Auth::user()->id_direccion])->pluck('tipo', 'id')->all();
        }

        $objetos[1]->Valor = date('Y-m-d');
        $objetos[3]->ValorAnterior = "ECUADOR";
        $objetos[6]->Valor = ['NO' => 'NO', 'SI' => 'SI'];
        $objetos[8]->Valor = $this->escoja + parroquiaModel::where('estado', 'ACT')->pluck('parroquia', 'id')->all();
        $objetos[9]->Valor = [];

        $tipo = Input::get('tipo');
        if ($tipo == null) {
            $tipo = 0;
        }
        // if($tipo==9){
        //     dd($tipo);

        // }
        if ($id != null || $id != "") {
            $id = intval(Crypt::decrypt($id));

            $tabla = $objetosmain[1]->where("ind2020_tmov_cab.id", $id)->first();
            $tipo = $tabla->id_indicador;
            $objetos[0]->Valor = Indicadores2020TipoModel::where(['estado' => 'ACT', 'id' => $tabla->id_indicador])->pluck('tipo', 'id')->all();
        } else {
            if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) {
                if ($tipo == 0) {
                    $tipo = 1;
                }
            } else {
                $xxx = Indicadores2020TipoModel::where(['estado' => 'ACT', 'id_direccion' => Auth::user()->id_direccion])->first();
                // dd($xxx);
                if (!$xxx) {
                    return redirect()->back()
                        ->withErrors(['Ud no tiene acceso a este indicador']);
                } else {
                    $xxx = Indicadores2020TipoModel::where(['estado' => 'ACT', 'id_direccion' => Auth::user()->id_direccion])->first();
                    if ($tipo == 0) {
                        $tipo = $xxx->id;
                    }
                }
            }
            $objetos[0]->ValorAnterior = $tipo;
        }


   


        switch ($tipo) {
                //transito
            case 1:
                # code...
                $objetos_add = '[
                    {"Tipo":"select","Descripcion":"Tipo de tramite","Nombre":"id_tipo_tramite","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },               
                    {"Tipo":"text","Descripcion":"Cantidad","Nombre":"cantidad","Clase":"Null","Valor":"solonumeros","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Tipo de vehiculo","Nombre":"tipo_vehiculo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Tipo de multa","Nombre":"tipo_multa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Valor trámite","Nombre":"valor_tramite","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"html2","Descripcion":"Html","Nombre":"html","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                    ]';
                $objetos_add = json_decode($objetos_add);


                $objetos_add[0]->Valor = $this->escoja + Indicadores2020TransitoTipoTramiteModel::where('estado', 'ACT')->pluck('tipo', 'id')->all();
                $objetos_add[2]->Valor = $this->escoja + [
                    'LIVIANO' => 'LIVIANO',
                    'PESADO' => 'PESADO'
                ];
                $objetos_add[3]->Valor = $this->escoja + ['PARQUEADERO' => 'PARQUEADERO', 'FOTO MULTA' => 'FOTO MULTA'];

                $objetos_add[5]->Valor = '
                
              
                <script>
                $(function () {
                    $("#div_tipo_multa").hide();
                    $("#div_tipo_vehiculo").hide();
                    $("#div_valor_tramite").hide();
                    $("#div_html").hide();
                    
                    validar($("#id_tipo_tramite"));
                    $("#id_tipo_tramite").change(function (e) { 
                        validar(this);
                    });
    
                    
                });
    
                function validar(c){
                    console.log(c.value);
                       if(c.value==6){
                        $("#div_tipo_vehiculo").show();
                       }else{
                        $("#div_tipo_vehiculo").hide();
                       }
                       
                       if(c.value==7){
                        $("#div_tipo_multa").show();
                       }else{
                        $("#div_tipo_multa").hide();
                       }
                       
                       if(c.value==1 || c.value==2){
                           $("#div_valor_tramite").hide();
                        }else{
                            $("#div_valor_tramite").show();
                       }    
                }
                
                </script>';

                $objetos = array_merge($objetos, $objetos_add);
                break;

            case 2:
                ////////////////////// inspecciones
                $objetos_add = '[
                    {"Tipo":"text","Descripcion":"Telefono","Nombre":"telefono","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Correo","Nombre":"correo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Tipo de incidencia","Nombre":"tipo_incidencia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Canal de denuncia","Nombre":"canal","Clase":"email","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Cedula infractor","Nombre":"cedula_infractor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Pais infractor","Nombre":"pais","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Nombres infractor","Nombre":"nombres_infractor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Apellidos infractor","Nombre":"apellidos_infractor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Discapacidad infractor","Nombre":"discapacidad_infractor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Código catastral infractor","Nombre":"codigo_catastral_infractor","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Teléfono infractor","Nombre":"telefono_infractor","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Correo infractor","Nombre":"correo_infractor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Inspección","Nombre":"inspeccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Estado","Nombre":"estado_indicador","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"textarea","Descripcion":"Justificación","Nombre":"observaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Acta emitida","Nombre":"acta_emitida","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Inspector / Técnico","Nombre":"id_inspector_tecnico","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"datetext","Descripcion":"Fecha de ingreso de denuncia","Nombre":"fecha_ingreso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"file","Descripcion":"Registro fotográfico","Nombre":"resgistro_fotografico","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Para efecto","Nombre":"para_efecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"N° de acta 1","Nombre":"numero_acta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"N° de acta 2","Nombre":"numero_acta_2","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"textarea","Descripcion":"Justificación","Nombre":"justificacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Condición","Nombre":"condicion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                    ]';
                $objetos_add = json_decode($objetos_add);
                //Ambiental Construcciones Turisticas
                //AMBIENTAL|CONSTRUCIONES|TURISTICAS
                $objetos_add[2]->Valor = [
                    '' => 'Sin especificar',
                    'Contaminación acustica' => 'Contaminación acustica',
                    'Descargas de agua' => 'Descargas de agua',
                    'Contaminación lumínica' => 'Contaminación lumínica',
                    'Descargas de desechos solidos' => 'Descargas de desechos solidos',
                    'Robo de arena' => 'Robo de arena',
                    'Tala de Arboles' => 'Tala de Arboles',
                    'Contaminación por animales domesticos' => 'Contaminación por animales domesticos',
                    'Solar lleno de desechos' => 'Solar lleno de desechos',
                    'Afectación a propiedad privada' => 'Afectación a propiedad privada',
                    'Costrucción en zona de riesgo' => 'Costrucción en zona de riesgo',
                    'Daño a espacios Publicos' => 'Daño a espacios Publicos',
                    'Infraestructura en mal estado' => 'Infraestructura en mal estado',
                    'Construcciones ilegales' => 'Construcciones ilegales',
                    'Ocupación de via Publica por escombros o materiales de construcción' => 'Ocupación de via Publica por escombros o materiales de construcción',
                    'Sin permisos de construcción' => 'Sin permisos de construcción',
                    'Falta de medidas de seguridad' => 'Falta de medidas de seguridad',
                    'Incumplimiento de la suspension o clausura' => 'Incumplimiento de la suspension o clausura',
                    'Ocupación de espacios publicos comercio informal' => 'Ocupación de espacios publicos comercio informal',
                    'Sin permisos de funcionamiento' => 'Sin permisos de funcionamiento',
                    'Producto deteriodado' => 'Producto deteriodado',
                    'Alteracion del orden publico' => 'Alteracion del orden publico',
                    'Falta de normas de bioseguridad' => 'Falta de normas de bioseguridad',
                    'Otros' => 'Otros'
                ];
                $objetos_add[3]->Valor = ['MantApp' => 'MantApp', 'Redes Sociales' => 'Redes Sociales', 'Secretarias' => 'Secretarias', 'Territoriales' => 'Territoriales'];
                $objetos_add[12]->Valor = ['AMBIENTAL' => 'AMBIENTAL', 'CONSTRUCIONES' => 'CONSTRUCIONES', 'TURISTICAS' => 'TURISTICAS', 'SERVICIOS PÚBLICOS' => 'SERVICIOS PÚBLICOS', 'OTROS' => 'OTROS'];


                $objetos_add[8]->Valor = ['NO' => 'NO', 'SI' => 'SI'];
                //EJECUTADO|EN PROCESO|REPROGRAMADA|FINALIZADA
                $objetos_add[13]->Valor = ['EJECUTADO' => 'EJECUTADO', 'EN PROCESO' => 'EN PROCESO', 'REPROGRAMADA' => 'REPROGRAMADA', 'FINALIZADA' => 'FINALIZADA'];
                //TERMINOS EN TIEMPOS|COMPARTIDA CON OTRAS DIRECCIONES
                //ADVERTENCIA|INFRACCION|CONFORMIDAD
                $objetos_add[15]->Valor = ['ADVERTENCIA 1' => 'ADVERTENCIA 1', 'ADVERTENCIA 2' => 'ADVERTENCIA 2', 'OBSTRUCCION 1' => 'OBSTRUCCION 1', 'OBSTRUCCION 2' => 'OBSTRUCCION 2', 'CONFORMIDAD' => 'CONFORMIDAD', 'INFRACCIÓN' => 'INFRACCIÓN'];
                $objetos_add[16]->Valor = UsuariosModel::where('estado', 'ACT')->pluck('name', 'id')->all();
                $objetos_add[19]->Valor = [
                    'INMEDIATO' => 'INMEDIATO',
                    '24 HORAS' => '24 HORAS',
                    '48 HORAS' => '48 HORAS',
                    '7 DIAS' => '7 DIAS',
                    '30 DIAS' => '30 DIAS'
                ];
                $objetos_add[23]->Valor = [
                    'A TIEMPO' => 'A TIEMPO',
                    'RETRASADA' => 'RETRASADA',
                    'CULMINADA' => 'CULMINADA'
                ];
                //

                // $objetos_add[8]->Valor = ['1' => '1', '2' => '2'];
                // show($objetos_add);



                $objetos = array_merge($objetos, $objetos_add);



                break;
            case 3:
                $objetos_add = '[
                    {"Tipo":"select","Descripcion":"Sanción","Nombre":"id_sancion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"textdisabled","Descripcion":"Valor","Nombre":"valor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                    ]';
                $objetos_add = json_decode($objetos_add);
                $tipos_eventos = Indicadores2020SancionesTiposModel::where('estado', 'ACT')->pluck('tipo', 'id')->all();
                $objetos_add[0]->Valor = $tipos_eventos;
                $objetos = array_merge($objetos, $objetos_add);

                break;
            case 4:
                //    show($objetos);
                unset($objetos[2]);
                unset($objetos[3]);
                unset($objetos[4]);
                unset($objetos[5]);
                unset($objetos[6]);
                unset($objetos[7]);
                unset($objetos[10]);
                unset($objetos[11]);
                $objetos_add = '[
                {"Tipo":"text","Descripcion":"N° de habitantes","Nombre":"n_habitantes","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },               
                {"Tipo":"text","Descripcion":"N° de beneficiarios","Nombre":"n_beneficiarios","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"select","Descripcion":"Tipo de eventos","Nombre":"id_tipo_evento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Inversión $","Nombre":"inversion","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"select","Descripcion":"Nacionalidad Artista","Nombre":"nacionalidad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"select","Descripcion":"Tipo artista","Nombre":"id_tipo_artista","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"C.I Artista","Nombre":"cedula_artista","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Apellidos artista","Nombre":"apellidos_artista","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Nombres artista","Nombre":"nombres_artista","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"textarea","Descripcion":"Observaciones","Nombre":"observaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                ]';
                $objetos_add = json_decode($objetos_add);
                $tipos_eventos = Indicadores2020EventosTiposModel::where('estado', 'ACT')->pluck('tipo', 'id')->all();
                $objetos_add[2]->Valor = $tipos_eventos;
                $objetos_add[2]->Adicional = '<button id="agregar_item_eventos" onClick="agregar(1,\'id_tipo_evento\')" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar tipo </button>';
                $objetos_add[4]->Valor = ['LOCAL' => 'LOCAL', 'NACIONAL' => 'NACIONAL', 'INTERNACIONAL' => 'INTERNACIONAL'];
                $tipos_artista = Indicadores2020EventosTiposArtistaModel::where('estado', 'ACT')->pluck('tipo_artista', 'id')->all();
                // dd($tipos_artista);
                $objetos_add[5]->Valor = $tipos_artista;
                $objetos_add[5]->Adicional = '<button id="agregar_item_tipo_artista" onClick="agregar(2,\'id_tipo_artista\')"type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar tipo de artista </button>';;
                $objetos = array_merge($objetos, $objetos_add);
                break;

            case 5:
                //    show($objetos);
                unset($objetos[2]);
                unset($objetos[3]);
                unset($objetos[4]);
                unset($objetos[5]);
                unset($objetos[6]);
                unset($objetos[7]);
                unset($objetos[10]);
                unset($objetos[11]);
                $objetos_add = '[
                    {"Tipo":"text","Descripcion":"N° de habitantes","Nombre":"n_habitantes","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },               
                    {"Tipo":"text","Descripcion":"N° de beneficiarios","Nombre":"n_beneficiarios","Clase":"solonumeros","Valor":"solonumeros","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Tipo de actividad","Nombre":"id_tipo_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Inversión $","Nombre":"inversion","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"C.I del instructor","Nombre":"cedula_instructor","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Apellidos del instructor","Nombre":"apellidos_instructor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Nombres del instructor","Nombre":"nombres_instructor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"textarea","Descripcion":"Observaciones","Nombre":"observaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                    ]';
                $objetos_add = json_decode($objetos_add);
                $tipos_eventos = Indicadores2020ActividadesFisicasTipoModel::where('estado', 'ACT')->pluck('tipo', 'id')->all();
                $objetos_add[2]->Valor = $tipos_eventos;
                $objetos_add[2]->Adicional = '<button id="agregar_item_eventos" onClick="agregar(3,\'id_tipo_actividad\')" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar tipo </button>';
                $objetos = array_merge($objetos, $objetos_add);
                break;
            case 6:
                //    show($objetos);
                unset($objetos[2]);
                unset($objetos[3]);
                unset($objetos[4]);
                unset($objetos[5]);
                unset($objetos[6]);
                unset($objetos[7]);
                unset($objetos[10]);
                unset($objetos[11]);
                $objetos_add = '[
                    {"Tipo":"text","Descripcion":"N° de habitantes","Nombre":"n_habitantes","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },               
                    {"Tipo":"text","Descripcion":"N° de beneficiarios","Nombre":"n_beneficiarios","Clase":"solonumeros","Valor":"solonumeros","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Tipo de servicio","Nombre":"id_tipo_servicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Lugar","Nombre":"id_lugar","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Inversión $","Nombre":"inversion","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":" Cantidad","Nombre":"cantidad","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Custodia","Nombre":"custodia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Cédula custodio","Nombre":"ci_custodio","Clase":"solonumeros cedula","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Apellidos Custodio","Nombre":"apellido_custodio","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Nombres Custodio","Nombre":"nombre_custodio","Clase":"null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"textarea","Descripcion":"Observaciones","Nombre":"observaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                    ]';
                $objetos_add = json_decode($objetos_add);
                $objetos_add[2]->Valor = Indicadores2020ArbolesSembradosServiciosModel::where('estado', 'ACT')->whereIn('id', [1])->pluck('tipo', 'id')->all();
                // $objetos_add[2]->Adicional = '<button id="agregar_item_eventos" onClick="agregar(4,\'id_tipo_servicio\')" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar servicio </button>';
                $objetos_add[3]->Valor = Indicadores2020ArbolesSembradosLugaresModel::where('estado', 'ACT')->orderBy('lugar', 'ASC')->pluck('lugar', 'id')->all();
                $objetos_add[3]->Adicional = '<button id="agregar_item_eventos" onClick="agregar(5,\'id_lugar\')" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar lugar </button>';
                $objetos_add[6]->Valor = ['NO' => 'NO', 'SI' => 'SI'];
                $objetos_add[10]->Adicional = '<div class="col-lg-3"><div class="alert alert-secondary " role="alert">
                <p style="text-align: justify;">🛑 Si es un parque aquí se debe mencionar que es el: <br>  PARQUE DE LA MADRE <br>🛑  Si son aceras o parterres indicar la DIRECCIÓN</p>
              </div></div>';
                $objetos = array_merge($objetos, $objetos_add);
                break;
            case 7:
                //    show($objetos);
                unset($objetos[2]);
                unset($objetos[3]);
                unset($objetos[4]);
                unset($objetos[5]);
                unset($objetos[6]);
                unset($objetos[7]);
                unset($objetos[10]);
                unset($objetos[11]);
                $objetos_add = '[
                    {"Tipo":"text","Descripcion":"N° de habitantes","Nombre":"n_habitantes","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },               
                    {"Tipo":"text","Descripcion":"N° de beneficiarios","Nombre":"n_beneficiarios","Clase":"solonumeros","Valor":"solonumeros","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Tipo de servicio","Nombre":"id_tipo_servicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Cantidad","Nombre":"cantidad","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Inversión $","Nombre":"inversion","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"textarea","Descripcion":"Observaciones","Nombre":"observaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                    ]';
                $objetos_add = json_decode($objetos_add);
                $objetos_add[2]->Valor =  Indicadores2020SeguridadCiudadanaServiciosModel::where('estado', 'ACT')->pluck('tipo', 'id')->all();
                $objetos_add[2]->Adicional = '<button id="agregar_item_eventos" onClick="agregar(6,\'id_tipo_servicio\')" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar servicio </button>';
                $objetos = array_merge($objetos, $objetos_add);
                break;
            case 8:
                //    show($objetos);
                unset($objetos[2]);
                unset($objetos[3]);
                unset($objetos[4]);
                unset($objetos[5]);
                unset($objetos[6]);
                unset($objetos[7]);
                unset($objetos[10]);
                unset($objetos[11]);
                $objetos_add = '[
                    {"Tipo":"text","Descripcion":"N° de habitantes","Nombre":"n_habitantes","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },               
                    {"Tipo":"text","Descripcion":"N° de beneficiarios","Nombre":"n_beneficiarios","Clase":"solonumeros","Valor":"solonumeros","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Tipo de servicio","Nombre":"id_tipo_servicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Inversión $","Nombre":"inversion","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"textarea","Descripcion":"Observaciones","Nombre":"observaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                    ]';
                $objetos_add = json_decode($objetos_add);
                $objetos_add[2]->Valor = Indicadores2020TurismoServiciosModel::where('estado', 'ACT')->pluck('tipo', 'id')->all();
                $objetos_add[2]->Adicional = '<button id="agregar_item_eventos" onClick="agregar(7,\'id_tipo_servicio\')" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar servicio </button>';;
                $objetos = array_merge($objetos, $objetos_add);
                break;
            case 9:
                //    show($objetos);
                // unset($objetos[1]);
                unset($objetos[3]);
                // unset($objetos[3]);
                // unset($objetos[4]);
                unset($objetos[6]);
                unset($objetos[7]);
                unset($objetos[10]);
                unset($objetos[11]);

                // dd($objetos);
                $sp = json_decode('{"Tipo":"select","Descripcion":"S/P","Nombre":"sp","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }');
                $sp->Valor = ['SI' => 'SI', 'NO' => 'NO'];
                // dd($sp);
                $objetos = insertarinarray($objetos, $sp, 1);
                $objetos_add = '[
                    {"Tipo":"text","Descripcion":"Telefono","Nombre":"telefono","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },               
                    {"Tipo":"select","Descripcion":"Tipo de atención","Nombre":"id_tipo_atencion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Inversión $","Nombre":"inversion","Clase":"disabled","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Vacunación antiRabia","Nombre":"anti_rabia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Tipo mascota","Nombre":"id_tipo_mascota","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Raza de mascota","Nombre":"id_raza","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Edad de mascota(Meses)","Nombre":"edad","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Sexo","Nombre":"sexo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"textarea","Descripcion":"Observaciones","Nombre":"observaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                    ]';
                $objetos_add = json_decode($objetos_add);
                $objetos_add[1]->Valor = Indicadores2020FaunaTipoAtencionModel::where('estado', 'ACT')->pluck('tipo', 'id')->all();
                $valor = Indicadores2020FaunaTipoAtencionModel::where('estado', 'ACT')->first();
                $objetos_add[2]->ValorAnterior = $valor->valor;
                $objetos_add[3]->Valor = ['NO' => 'NO', 'SI' => 'SI'];
                $objetos_add[4]->Valor = Indicadores2020FaunaTipoMascota::where('estado', 'ACT')->pluck('tipo', 'id')->all();;
                $objetos_add[5]->Valor = Indicadores2020FaunaRazaMascota::where(['estado' => 'ACT', 'id_tipo' => 1])->pluck('raza', 'id')->all();;;
                $objetos_add[7]->Valor = ['HEMBRA' => 'HEMBRA', 'MACHO' => 'MACHO'];
                $objetos = array_merge($objetos, $objetos_add);
                break;
            case 10:
                //    show($objetos);
                unset($objetos[2]);
                unset($objetos[3]);
                unset($objetos[4]);
                unset($objetos[5]);
                unset($objetos[6]);
                unset($objetos[7]);
                unset($objetos[10]);
                unset($objetos[11]);

                $objetos_add = '[
                    {"Tipo":"text","Descripcion":"N° de habitantes","Nombre":"n_habitantes","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },               
                    {"Tipo":"text","Descripcion":"N° de beneficiarios","Nombre":"n_beneficiarios","Clase":"solonumeros","Valor":"solonumeros","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Tipo de servicio","Nombre":"id_tipo_servicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Lugar","Nombre":"id_lugar","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Inversión $","Nombre":"inversion","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"text","Descripcion":"Cantidad en Metros Cuadrados","Nombre":"cantidad","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"textarea","Descripcion":"Observaciones","Nombre":"observaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                    ]';
                $objetos_add = json_decode($objetos_add);
                $objetos_add[2]->Valor = Indicadores2020MantenimientoAreasServicioModel::where('estado', 'ACT')->pluck('tipo', 'id')->all();
                // $objetos_add[2]->Adicional = '<button id="agregar_item_eventos" onClick="agregar(8,\'id_tipo_servicio\')" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar servicio </button>';
                $objetos_add[3]->Valor = Indicadores2020ArbolesSembradosLugaresModel::where('estado', 'ACT')->pluck('lugar', 'id')->all();
                $objetos_add[3]->Adicional = '<button id="agregar_item_eventos" onClick="agregar(5,\'id_lugar\')" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar lugar </button>';
                $objetos_add[6]->Adicional = '<div class="col-lg-3"><div class="alert alert-secondary " role="alert">
                <p style="text-align: justify;">🛑 Si es un parque aquí se debe mencionar que es el: <br>  PARQUE DE LA MADRE <br>🛑  Si son aceras o parterres indicar la DIRECCIÓN</p>
              </div></div>';
                $objetos = array_merge($objetos, $objetos_add);
                break;

            default:
                # code...
                break;
        }

        $datos = array();
        if ($id == "") {
            $this->configuraciongeneral[2] = "crear";


            $datos = [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            ];
        } else {
            $this->configuraciongeneral[2] = "editar";

            // dd($tabla);
            $dicecionesDocumentos = null;
            $dicecionesImagenes = null;


           
            if ($tipo == 1 || $tipo == 2 || $tipo == 3) {
                $parroquia_id = barrioModel::where(['estado' => 'ACT', 'id' => $tabla->barrio_id])->first();
                $objetos[8]->ValorAnterior = $parroquia_id->id_parroquia;
                $objetos[9]->Valor = barrioModel::where(['estado' => 'ACT', 'id_parroquia' => $parroquia_id->id_parroquia])->pluck('barrio', 'id')->all();
                $objetos[9]->ValorAnterior = $tabla->barrio_id;
            }
            if ($tipo == 9) {
                $parroquia_id = barrioModel::where(['estado' => 'ACT', 'id' => $tabla->barrio_id])->first();
                $objetos[6]->ValorAnterior = $parroquia_id->id_parroquia;
                $objetos[7]->Valor = barrioModel::where(['estado' => 'ACT', 'id_parroquia' => $parroquia_id->id_parroquia])->pluck('barrio', 'id')->all();
                $objetos[7]->ValorAnterior = $tabla->barrio_id;
            } else if ($tipo == 2) {
                // $parroquia_id = barrioModel::where(['estado' => 'ACT', 'id' => $tabla->barrio_id])->first();
                // $objetos[2]->ValorAnterior = $parroquia_id->id_parroquia;
                // $objetos[3]->Valor = barrioModel::where(['estado' => 'ACT', 'id_parroquia' => $parroquia_id->id_parroquia])->pluck('barrio', 'id')->all();
                // $objetos[3]->ValorAnterior = $tabla->barrio_id;
                // dd($objetos[2]);
            }else {
                $parroquia_id = barrioModel::where(['estado' => 'ACT', 'id' => $tabla->barrio_id])->first();
                $objetos[2]->ValorAnterior = $parroquia_id->id_parroquia;
                $objetos[3]->Valor = barrioModel::where(['estado' => 'ACT', 'id_parroquia' => $parroquia_id->id_parroquia])->pluck('barrio', 'id')->all();
                $objetos[3]->ValorAnterior = $tabla->barrio_id;
            }
            // show($tabla);
            // show($objetos);

            $datos_extras = [];
            switch ($tipo) {
                case 1:
                    $datos_extras = Indicadores2020TransitoModel::where('id_indicador_cab', $id)->get();
                    break;
                case 2:
                    $datos_extras = Indicadores2020ControlTerritorialModel::where('id_indicador_cab', $id)->get();
                    break;
                case 3:
                    $datos_extras = Indicadores2020SancionesModel::where('id_indicador_cab', $id)->get();
                    break;
                case 4:
                    $datos_extras = Indicadores2020EventosModel::where('id_indicador_cab', $id)->get();
                    break;
                case 5:
                    $datos_extras = Indicadores2020ActividadesFisicasModel::where('id_indicador_cab', $id)->get();
                    break;
                case 6:
                    $datos_extras = Indicadores2020ArbolesSembradosModel::where('id_indicador_cab', $id)->get();
                    break;
                case 7:
                    $datos_extras = Indicadores2020SeguridadCiudadanaModel::where('id_indicador_cab', $id)->get();
                    break;
                case 8:
                    $datos_extras = Indicadores2020TurismoModel::where('id_indicador_cab', $id)->get();
                    break;
                case 9:
                    $datos_extras = Indicadores2020FaunaModel::where('id_indicador_cab', $id)->get();
                    break;
                case 10:
                    $datos_extras = Indicadores2020MantenimientoAreasModel::where('id_indicador_cab', $id)->get();
                    break;

                default:
                    # code...
                    break;
            }

            foreach ($datos_extras as $key => $value) {
                foreach ($value->toarray() as $k => $v) {
                    if ($k != "id") {
                        $tabla->$k = $v;
                    }
                }
            }
           
            return view('vistas.create', [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
                'dicecionesImagenes' => $dicecionesImagenes,
                'dicecionesDocumentos' => $dicecionesDocumentos,
                "permisos" =>  $this->perfil(),
                "timeline" => null
            ]);
        }

        return view('vistas.create', $datos);
    }

    public function verpermisos($objetos = array(), $tipo = "index", $anio = '2019')
    {


        unset($objetos[7]);
        $tabla = Indicadores2020Model::select('ind2020_tmov_cab.*', 'p.parroquia', 'b.barrio', 'tt.tipo')
            ->join("ind2020_tmae_tipo as tt", "tt.id", "=", "ind2020_tmov_cab.id_indicador")
            ->join("tmae_direcciones as d", "d.id", "=", "tt.id_direccion")
            ->join("barrio as b", "b.id", "=", "ind2020_tmov_cab.barrio_id")
            ->join("parroquia as p", "p.id", "=", "b.id_parroquia")
            ->where('ind2020_tmov_cab.estado', 'ACT');

        // show($tabla->get()->toarray());
        return array($objetos, $tabla);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return $this->formularioscrear();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }


    public function guardar($id)
    {
        $input = Input::all();

        $ruta = $this->configuraciongeneral[1];

        if ($id == 0) {
            $ruta .= "/create?tipo=" . Input::get('id_indicador');
            $guardar = new Indicadores2020Model();
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro de Datos";
        } else {
            $ruta .= "/" . Crypt::encrypt($id) . "/edit";
            $guardar = Indicadores2020Model::find($id);
            // dd($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Actualización de Datos";
        }

        $input = Input::all();
        // show($input);
        $arrapas = array();

        switch (Input::get('id_indicador')) {
            case 4:
                # code...
                $validator = Validator::make($input, Indicadores2020Model::rules_2($id));
                break;
            case 5:
                # code...
                $validator = Validator::make($input, Indicadores2020Model::rules_2($id));
                break;
            case 6:
                # code...
                $validator = Validator::make($input, Indicadores2020Model::rules_2($id));
                break;
            case 7:
                # code...
                $validator = Validator::make($input, Indicadores2020Model::rules_2($id));
                break;
            case 8:
                # code...
                $validator = Validator::make($input, Indicadores2020Model::rules_2($id));
                break;
            case 9:
                # code...
                $validator = Validator::make($input, Indicadores2020Model::rules_2($id));
                break;
            case 10:
                # code...
                $validator = Validator::make($input, Indicadores2020Model::rules_2($id));
                break;

            default:
                # code...
                $validator = Validator::make($input, Indicadores2020Model::rules($id));
                break;
        }


        if ($validator->fails()) {
            //die($ruta);
            DB::rollback();
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            DB::beginTransaction();
            try {
                // $timeline=new Indicadores2020DetaModel();
                foreach ($input as $key => $value) {

                    if (
                        $key == "id_indicador"
                        || $key == "cedula"
                        || $key == "pais"
                        || $key == "nombres"
                        || $key == "apellidos"
                        || $key == "discapacidad"
                        || $key == "clave_catastral"
                        || $key == "barrio_id"
                        || $key == "direccion"
                        || $key == "fecha_registro"

                    ) {
                        $guardar->$key = $value;
                        // $timeline->$key=$value;
                    }
                }
                if ($id == 0) {
                    $guardar->id_usuario = Auth::user()->id;
                    $guardar->ip = Auth::user()->id;
                    $guardar->pc = Auth::user()->id;
                }

                $guardar->save();
                // dd($guardar);

                $idcab = $guardar->id;


                switch ($guardar->id_indicador) {

                    case 1:
                        $validator = Validator::make($input, Indicadores2020TransitoModel::rules($id));
                        if ($validator->fails()) {
                            DB::rollback();
                            return Redirect::to("$ruta")
                                ->withErrors($validator)
                                ->withInput();
                        }


                        if ($id == 0)
                            $extra = new Indicadores2020TransitoModel();
                        else
                            $extra = Indicadores2020TransitoModel::where('id_indicador_cab', $id)->first();



                        $extra->id_tipo_tramite = Input::get('id_tipo_tramite');
                        $extra->cantidad = Input::get('cantidad');
                        $extra->tipo_vehiculo = Input::get('tipo_vehiculo');
                        $extra->tipo_multa = Input::get('tipo_multa');
                        $extra->valor_tramite = str_replace(',', '', Input::get('valor_tramite'));
                        // $extra->valor_tramite = Input::get('valor_tramite');
                        $extra->id_indicador_cab = $idcab;
                        $extra->save();

                        break;
                    case 2:
                        $validator = Validator::make($input, Indicadores2020ControlTerritorialModel::rules($id));
                        if ($validator->fails()) {
                            DB::rollback();
                            return Redirect::to("$ruta")
                                ->withErrors($validator)
                                ->withInput();
                        }

                        if ($id == 0)
                            $extra = new Indicadores2020ControlTerritorialModel();
                        else
                            $extra = Indicadores2020ControlTerritorialModel::where('id_indicador_cab', $id)->first();

                        $extra->telefono = Input::get('telefono');
                        // $extra->correo = Input::get('correo');
                        $extra->inspeccion = Input::get('inspeccion');
                        $extra->estado_indicador = Input::get('estado_indicador');
                        $extra->observaciones = Input::get('observaciones');
                        $extra->acta_emitida = Input::get('acta_emitida');
                        $extra->id_inspector_tecnico = Input::get('id_inspector_tecnico');
                        $extra->fecha_ingreso = Input::get('fecha_ingreso');
                        $extra->numero_acta = Input::get('numero_acta');
                        $extra->correo = Input::get('correo');
                        $extra->canal = Input::get('canal');
                        $extra->numero_acta_2 = Input::get('numero_acta_2');
                        $extra->id_indicador_cab = $idcab;
                        
                        $extra->tipo_incidencia = Input::get('tipo_incidencia');
                        $extra->canal = Input::get('canal');
                        $extra->cedula_infractor = Input::get('cedula_infractor');
                        $extra->nombres_infractor = Input::get('nombres_infractor');
                        $extra->apellidos_infractor = Input::get('apellidos_infractor');
                        $extra->discapacidad_infractor = Input::get('discapacidad_infractor');
                        $extra->codigo_catastral_infractor = Input::get('codigo_catastral_infractor');
                        $extra->telefono_infractor = Input::get('telefono_infractor');
                        $extra->correo_infractor = Input::get('correo_infractor');
                        $extra->para_efecto = Input::get('para_efecto');
                        $extra->justificacion = Input::get('justificacion');
                        $extra->condicion = Input::get('condicion');
                        
                        

                        $dir = public_path() . '/archivos_sistema/';
                        // AgendaArchivosModel
                        $docs = Input::file('resgistro_fotografico');
                        if (is_file($docs)) {
                            $fileName = "indicador2020_resgistro_fotografico-$idcab-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                            $ext = $docs->getClientOriginalExtension();
                            $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                            if (!in_array($ext, $perfiles)) {
                                DB::rollback();
                                $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                                return redirect()->back()->withErrors([$mensaje])->withInput();
                            }

                            $extra->resgistro_fotografico = $fileName;
                            $docs->move($dir, $fileName);
                        }

                        $extra->save();

                        if ($extra->acta_emitida == 'INFRACCIÓN') {

                            $guardar_infraccion = new Indicadores2020Model();
                            $guardar_infraccion->id_usuario = Auth::user()->id;
                            $guardar_infraccion->ip = Auth::user()->id;
                            $guardar_infraccion->pc = Auth::user()->id;
                            $guardar_infraccion->id_indicador = 3;
                            $guardar_infraccion->cedula = Input::get("cedula");
                            $guardar_infraccion->pais = Input::get("pais");
                            $guardar_infraccion->nombres = Input::get("nombres");
                            $guardar_infraccion->apellidos = Input::get("apellidos");
                            $guardar_infraccion->discapacidad = Input::get("discapacidad");
                            $guardar_infraccion->clave_catastral = Input::get("clave_catastral");
                            $guardar_infraccion->barrio_id = Input::get("barrio_id");
                            $guardar_infraccion->direccion = Input::get("direccion");
                            $guardar_infraccion->save();

                            $extra_infraccion = new Indicadores2020SancionesModel();
                            $extra_infraccion->id_sancion = 1;
                            $extra_infraccion->valor = 0;
                            $extra_infraccion->id_indicador_cab =  $guardar_infraccion->id;
                            $extra_infraccion->save();
                        }


                        break;
                    case 3:
                        // dd(Input::all());
                        $validator = Validator::make($input, Indicadores2020SancionesModel::rules($id));
                        if ($validator->fails()) {
                            DB::rollback();
                            return Redirect::to("$ruta")
                                ->withErrors($validator)
                                ->withInput();
                        }

                        if ($id == 0)
                            $extra = new Indicadores2020SancionesModel();
                        else
                            $extra = Indicadores2020SancionesModel::where('id_indicador_cab', $id)->first();

                        if (!$extra) {
                            $extra = new Indicadores2020SancionesModel();
                        }

                        $extra->id_sancion = Input::get('id_sancion');
                        $extra->valor = Input::get('valor');

                        $extra->id_indicador_cab = $idcab;
                        $extra->save();

                        break;
                    case 4:
                        // dd(Input::all());
                        $validator = Validator::make($input, Indicadores2020EventosModel::rules($id));
                        if ($validator->fails()) {
                            DB::rollback();
                            return Redirect::to("$ruta")
                                ->withErrors($validator)
                                ->withInput();
                        }

                        if ($id == 0)
                            $extra = new Indicadores2020EventosModel();
                        else
                            $extra = Indicadores2020EventosModel::where('id_indicador_cab', $id)->first();

                        $extra->n_habitantes = Input::get('n_habitantes');
                        $extra->n_beneficiarios = Input::get('n_beneficiarios');
                        $extra->id_tipo_evento = Input::get('id_tipo_evento');
                        $extra->inversion = str_replace(',', '', Input::get('inversion'));
                        $extra->nacionalidad = Input::get('nacionalidad');
                        $extra->id_tipo_artista = Input::get('id_tipo_artista');
                        $extra->cedula_artista = Input::get('cedula_artista');
                        $extra->apellidos_artista = Input::get('apellidos_artista');
                        $extra->nombres_artista = Input::get('nombres_artista');
                        $extra->observaciones = Input::get('observaciones');
                        $extra->id_indicador_cab = $idcab;
                        $extra->save();

                        break;
                    case 5:
                        // dd(Input::all());
                        $validator = Validator::make($input, Indicadores2020ActividadesFisicasModel::rules($id));
                        if ($validator->fails()) {
                            DB::rollback();
                            return Redirect::to("$ruta")
                                ->withErrors($validator)
                                ->withInput();
                        }

                        if ($id == 0)
                            $extra = new Indicadores2020ActividadesFisicasModel();
                        else
                            $extra = Indicadores2020ActividadesFisicasModel::where('id_indicador_cab', $id)->first();

                        $extra->n_habitantes = Input::get('n_habitantes');
                        $extra->n_beneficiarios = Input::get('n_beneficiarios');
                        $extra->id_tipo_actividad = Input::get('id_tipo_actividad');
                        $extra->inversion = str_replace(',', '', Input::get('inversion'));
                        $extra->cedula_instructor = Input::get('cedula_instructor');
                        $extra->apellidos_instructor = Input::get('apellidos_instructor');
                        $extra->nombres_instructor = Input::get('nombres_instructor');
                        $extra->observaciones = Input::get('observaciones');
                        $extra->id_indicador_cab = $idcab;
                        $extra->save();

                        break;
                    case 6:
                        // dd(Input::all());
                        $validator = Validator::make($input, Indicadores2020ArbolesSembradosModel::rules($id));
                        if ($validator->fails()) {
                            DB::rollback();
                            return Redirect::to("$ruta")
                                ->withErrors($validator)
                                ->withInput();
                        }
                        if ($id == 0)
                            $extra = new Indicadores2020ArbolesSembradosModel();
                        else
                            $extra = Indicadores2020ArbolesSembradosModel::where('id_indicador_cab', $id)->first();
                        if (!$extra) {
                            $extra = new Indicadores2020ArbolesSembradosModel();
                        }

                        // dd($id);
                        $extra->n_habitantes = Input::get('n_habitantes');
                        $extra->n_beneficiarios = Input::get('n_beneficiarios');
                        $extra->id_tipo_servicio = Input::get('id_tipo_servicio');
                        $extra->inversion = str_replace(',', '', Input::get('inversion'));
                        $extra->id_lugar = Input::get('id_lugar');
                        $extra->cantidad = Input::get('cantidad');
                        $extra->custodia = Input::get('custodia');
                        $extra->apellido_custodio = Input::get('apellido_custodio');
                        $extra->nombre_custodio = Input::get('nombre_custodio');
                        $extra->observaciones = Input::get('observaciones');
                        $extra->id_indicador_cab = $idcab;
                        $extra->save();

                        break;
                    case 7:
                        // dd(Input::all());
                        $validator = Validator::make($input, Indicadores2020SeguridadCiudadanaModel::rules($id));
                        if ($validator->fails()) {
                            DB::rollback();
                            return Redirect::to("$ruta")
                                ->withErrors($validator)
                                ->withInput();
                        }
                        if ($id == 0)
                            $extra = new Indicadores2020SeguridadCiudadanaModel();
                        else
                            $extra = Indicadores2020SeguridadCiudadanaModel::where('id_indicador_cab', $id)->first();
                        if (!$extra) {
                            $extra = new Indicadores2020SeguridadCiudadanaModel();
                        }
                        $extra->n_habitantes = Input::get('n_habitantes');
                        $extra->n_beneficiarios = Input::get('n_beneficiarios');
                        $extra->id_tipo_servicio = Input::get('id_tipo_servicio');
                        $extra->inversion = str_replace(',', '', Input::get('inversion'));
                        $extra->cantidad = Input::get('cantidad');
                        $extra->observaciones = Input::get('observaciones');
                        $extra->id_indicador_cab = $idcab;
                        $extra->save();

                        break;
                    case 8:
                        // dd(Input::all());
                        $validator = Validator::make($input, Indicadores2020TurismoModel::rules($id));
                        if ($validator->fails()) {
                            DB::rollback();
                            return Redirect::to("$ruta")
                                ->withErrors($validator)
                                ->withInput();
                        }
                        if ($id == 0)
                            $extra = new Indicadores2020TurismoModel();
                        else
                            $extra = Indicadores2020TurismoModel::where('id_indicador_cab', $id)->first();
                        if (!$extra) {
                            $extra = new Indicadores2020TurismoModel();
                        }
                        $extra->n_habitantes = Input::get('n_habitantes');
                        $extra->n_beneficiarios = Input::get('n_beneficiarios');
                        $extra->id_tipo_servicio = Input::get('id_tipo_servicio');
                        $extra->inversion = str_replace(',', '', Input::get('inversion'));
                        $extra->cantidad = Input::get('cantidad');
                        $extra->observaciones = Input::get('observaciones');
                        $extra->id_indicador_cab = $idcab;
                        $extra->save();

                        break;
                    case 9:
                        // dd(Input::all());
                        $validator = Validator::make($input, Indicadores2020FaunaModel::rules($id));
                        if ($validator->fails()) {
                            DB::rollback();
                            return Redirect::to("$ruta")
                                ->withErrors($validator)
                                ->withInput();
                        }
                        if ($id == 0)
                            $extra = new Indicadores2020FaunaModel();
                        else
                            $extra = Indicadores2020FaunaModel::where('id_indicador_cab', $id)->first();
                        if (!$extra) {
                            $extra = new Indicadores2020FaunaModel();
                        }
                        $extra->telefono = Input::get('telefono');
                        $extra->id_tipo_atencion = Input::get('id_tipo_atencion');
                        $extra->inversion = str_replace(',', '', Input::get('inversion'));
                        $extra->anti_rabia = Input::get('anti_rabia');
                        $extra->id_raza = Input::get('id_raza');
                        $extra->edad = Input::get('edad');
                        $extra->sexo = Input::get('sexo');
                        $extra->sp = Input::get('sp');
                        $extra->observaciones = Input::get('observaciones');
                        $extra->id_indicador_cab = $idcab;
                        $extra->save();

                        break;
                    case 10:
                        // dd(Input::all());
                        $validator = Validator::make($input, Indicadores2020MantenimientoAreasModel::rules($id));
                        if ($validator->fails()) {
                            DB::rollback();
                            return Redirect::to("$ruta")
                                ->withErrors($validator)
                                ->withInput();
                        }
                        if ($id == 0)
                            $extra = new Indicadores2020MantenimientoAreasModel();
                        else
                            $extra = Indicadores2020MantenimientoAreasModel::where('id_indicador_cab', $id)->first();
                        if (!$extra) {
                            $extra = new Indicadores2020MantenimientoAreasModel();
                        }
                        $extra->n_habitantes = Input::get('n_habitantes');
                        $extra->n_beneficiarios = Input::get('n_beneficiarios');
                        $extra->id_tipo_servicio = Input::get('id_tipo_servicio');
                        $extra->inversion = str_replace(',', '', Input::get('inversion'));
                        $extra->id_lugar = Input::get('id_lugar');
                        $extra->cantidad = str_replace(',', '', Input::get('cantidad'));
                        $extra->observaciones = Input::get('observaciones');
                        $extra->id_indicador_cab = $idcab;
                        $extra->save();

                        break;
                    default:
                        # code...
                        break;
                }





                Auditoria($msgauditoria . " - ID: " . $id . "- indicadores");
                DB::commit();
            } //Fin Try


            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage() . " - " . $e->getLine();
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }

    public function indicadoresajax(Request $request)
    {
        $objetos = '[
            {"Tipo":"select","Descripcion":"Tipo indicador","Nombre":"id_indicador","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },               
            {"Tipo":"select","Descripcion":"Parroquia","Nombre":"parroquia_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Barrio","Nombre":"barrio_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Detalle","Nombre":"detalle","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Fecha de creación","Nombre":"created_at","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" }
            ]';
        $objetos = json_decode($objetos);
        $tabla = $this->verpermisos($objetos);
        $objetos = $tabla[0];

        // show($objetos);
        $tabla = $tabla[1];
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        // dd(Input::all());

        $dieccion = Input::get('dieccion');
        $cedula = Input::get('cedula');
        $id_indicador = Input::get('id_indicador');
        $parroquia_id = Input::get('parroquia_id');
        $barrio_id = Input::get('barrio_id');
        if ($cedula != "" && $cedula != null) {
            $tabla->where('cedula',  $cedula);
        }
        if ($dieccion != 0) {
            $tabla->where('id_direccion',  $dieccion);
        }
        if ($id_indicador != 0) {
            $tabla->where('tt.id',  $id_indicador);
        }
        if ($parroquia_id != 0) {
            $tabla->where('p.id',  $parroquia_id);
        }
        if ($barrio_id != 0) {
            $tabla->where('b.id',  $barrio_id);
        }



        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        // dd($tabla);
        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('tt.tipo', 'LIKE', "%{$search}%")
                    // ->where('d.direccion', 'LIKE', "%{$search}%")
                ;
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            // show($posts);
            foreach ($posts as $post) {
                // dd($post);
                $permisos = $this->perfil();
                $acciones = '';
                $edit = link_to_route('' . $this->configuraciongeneral[7] . '.edit', ' EDITAR', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o btn btn-light', 'style' => 'color: #fff;background-color: #17a2b8;border-color: #17a2b8'));
                if ($permisos->eliminar == 'SI') {
                    // $acciones= '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash  btn btn-danger"></i></a>
                    // <div style="display: none;">
                    // <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                    // <input name="_token" type="hidden" value="' . csrf_token() . '">
                    // <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    // </form>
                    // </div>';
                }
                $acciones = ' ' . $edit;

                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $detalle_html = '';

                switch ($post->id_indicador) {
                    case 1:
                        $detalle = Indicadores2020TransitoModel::join('ind2020_tmov_transito_tipo_tramite as ti', 'ti.id', '=', 'id_tipo_tramite')->where(['ind2020_tmov_transito.estado' => 'ACT', 'id_indicador_cab' => $post->id])->get();
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>C.I / Nombre</b>: ' . $post->cedula . ' / ';
                            $detalle_html .= '' . $post->nombres . ' ' . $post->apellidos . '<br>';
                            $detalle_html .= '<b>Tipo trámite:</b> ' . $value->tipo . '<br>';
                            $detalle_html .= '<b>Cantidad:</b> ' . $value->cantidad . '<br>';
                            $detalle_html .= '<b>Valor:</b> ' . ((isset($value->valor)) ? $value->valor : 0) . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                    case 2:
                        // dd($detalle);
                        $detalle = Indicadores2020ControlTerritorialModel::where(['ind2020_tmov_control_territorial.estado' => 'ACT', 'id_indicador_cab' => $post->id])->get();
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>C.I / Nombre</b>: ' . $post->cedula . ' / ';
                            $detalle_html .= '' . $post->nombres . ' ' . $post->apellidos . '<br>';
                            $detalle_html .= '<b>Tipo:</b> ' . $value->inspeccion . '<br>';
                            $detalle_html .= '<b>Estado:</b> ' . $value->estado_indicador . '<br>';
                            $detalle_html .= '<b>Observaciones:</b> ' . $value->observaciones . '<br>';
                            $detalle_html .= '<b>Acta Emitida:</b> ' . $value->acta_emitida . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                        break;
                    case 3:
                        $detalle = Indicadores2020SancionesModel::join('ind2020_tmae_tipo_sancion as ti', 'ti.id', 'id_sancion')->where(['ind2020_tmov_sanciones.estado' => 'ACT', 'id_indicador_cab' => $post->id])->get();
                        // dd($post);
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>C.I / Nombre</b>: ' . $post->cedula . ' / ';
                            $detalle_html .= '' . $post->nombres . ' ' . $post->apellidos . '<br>';
                            $detalle_html .= '<b>Dirección:</b> ' . $value->direccion . '<br>';
                            $detalle_html .= '<b>Sanción:</b> ' . $value->tipo . '<br>';
                            $detalle_html .= '<b>Valor:</b> ' . $value->valor . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                        break;
                    case 4:
                        $detalle = Indicadores2020EventosModel::join('ind2020_tmov_eventos_tipo as ti', 'ti.id', 'id_tipo_evento')->where(['ind2020_tmov_eventos.estado' => 'ACT', 'id_indicador_cab' => $post->id])->get();
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>Beneficiarios</b>: ' . $value->n_beneficiarios . '<br>';
                            $detalle_html .= '<b>Inversión:</b> ' . $value->inversion . '<br>';
                            $detalle_html .= '<b>Artista:</b> ' . $value->nombres_artista . '  ' . $value->apellidos_artista . '<br>';
                            $detalle_html .= '<b>Observaciones:</b> ' . $value->observaciones . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                        break;
                    case 5:
                        $detalle = Indicadores2020ActividadesFisicasModel::join('ind2020_tmov_actividades_fisicas_tipo as ti', 'ti.id', 'id_tipo_actividad')->where(['ind2020_tmov_actividades_fisicas.estado' => 'ACT', 'id_indicador_cab' => $post->id])->get();
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>Beneficiarios</b>: ' . $value->n_beneficiarios . '<br>';
                            $detalle_html .= '<b>Inversión:</b> ' . $value->inversion . '<br>';
                            $detalle_html .= '<b>Instructor:</b> ' . $value->nombres_instructor . '  ' . $value->apellidos_instructor . '<br>';
                            $detalle_html .= '<b>Tipo actividad:</b> ' . $value->tipo . '<br>';
                            $detalle_html .= '<b>Observaciones:</b> ' . $value->observaciones . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                        break;
                    case 5:
                        $detalle = Indicadores2020ActividadesFisicasModel::join('ind2020_tmov_actividades_fisicas_tipo as ti', 'ti.id', 'id_tipo_actividad')->where(['ind2020_tmov_actividades_fisicas.estado' => 'ACT', 'id_indicador_cab' => $post->id])->get();
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>Beneficiarios</b>: ' . $value->n_beneficiarios . '<br>';
                            $detalle_html .= '<b>Inversión:</b> ' . $value->inversion . '<br>';
                            $detalle_html .= '<b>Instructor:</b> ' . $value->nombres_instructor . '  ' . $value->apellidos_instructor . '<br>';
                            $detalle_html .= '<b>Tipo actividad:</b> ' . $value->tipo . '<br>';
                            $detalle_html .= '<b>Observaciones:</b> ' . $value->observaciones . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                        break;
                    case 6:
                        $detalle = Indicadores2020ArbolesSembradosModel::join('ind2020_tmae_arboles_lugares as ti', 'ti.id', 'id_lugar')
                            ->join('ind2020_tmae_arboles_servicios as ser', 'ser.id', 'id_tipo_servicio')
                            ->where(['ind2020_tmov_arboles.estado' => 'ACT', 'id_indicador_cab' => $post->id])
                            ->get();
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>Beneficiarios</b>: ' . $value->n_beneficiarios . '<br>';
                            $detalle_html .= '<b>Inversión:</b> ' . $value->inversion . '<br>';
                            $detalle_html .= '<b>Tipo lugar:</b> ' . $value->lugar . '<br>';
                            $detalle_html .= '<b>Tipo servicio:</b> ' . $value->tipo . '<br>';
                            $detalle_html .= '<b>Observaciones:</b> ' . $value->observaciones . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                        break;
                    case 7:
                        $detalle = Indicadores2020SeguridadCiudadanaModel::join('ind2020_tmae_seguridad_ciudadana as ti', 'ti.id', 'id_tipo_servicio')
                            ->where(['ind2020_tmov_seguridad_ciudadana.estado' => 'ACT', 'id_indicador_cab' => $post->id])
                            ->get();
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>Beneficiarios</b>: ' . $value->n_beneficiarios . '<br>';
                            $detalle_html .= '<b>Inversión:</b> ' . $value->inversion . '<br>';
                            $detalle_html .= '<b>Tipo servicio:</b> ' . $value->tipo . '<br>';
                            $detalle_html .= '<b>Cantidad:</b> ' . $value->cantidad . '<br>';
                            $detalle_html .= '<b>Observaciones:</b> ' . $value->observaciones . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                        break;
                    case 8:
                        $detalle = Indicadores2020TurismoModel::join('ind2020_tmae_turismo_servicios as ti', 'ti.id', 'id_tipo_servicio')
                            ->where(['ind2020_tmov_turismo.estado' => 'ACT', 'id_indicador_cab' => $post->id])
                            ->get();
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>Beneficiarios</b>: ' . $value->n_beneficiarios . '<br>';
                            $detalle_html .= '<b>Inversión:</b> ' . $value->inversion . '<br>';
                            $detalle_html .= '<b>Tipo servicio:</b> ' . $value->tipo . '<br>';
                            $detalle_html .= '<b>Observaciones:</b> ' . $value->observaciones . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                        break;
                    case 9:
                        $detalle = Indicadores2020FaunaModel::join('ind2020_tmov_fauna_tipo_atencion as ti', 'ti.id', 'id_tipo_atencion')
                            ->join('ind2020_tmov_fauna_tipo_raza as ra', 'ra.id', 'id_raza')
                            ->join('ind2020_tmov_fauna_tipo_mascota as ma', 'ma.id', 'ra.id_tipo')
                            ->where(['ind2020_tmov_fauna.estado' => 'ACT', 'id_indicador_cab' => $post->id])
                            ->get();
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>S/P</b>: ' . $value->sp . '<br>';
                            if ($value->sp == 'SI') {
                                $detalle_html .= '<b>C.I / Nombre</b>: ' . $post->cedula . ' / ';
                                $detalle_html .= '' . $post->nombres . ' ' . $post->apellidos . '<br>';
                            }
                            $detalle_html .= '<b>Tipo de atención:</b> ' . $value->tipo . '<br>';
                            $detalle_html .= '<b>Anti Rabia:</b> ' . $value->anti_rabia . '<br>';
                            $detalle_html .= '<b>Tipo mascota:</b> ' . $value->tipo . '<br>';
                            $detalle_html .= '<b>Raza:</b> ' . $value->raza . '<br>';
                            $detalle_html .= '<b>Edad:</b> ' . $value->edad . '<br>';
                            $detalle_html .= '<b>Observaciones:</b> ' . $value->observaciones . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                        break;
                    case 10:
                        $detalle = Indicadores2020MantenimientoAreasModel::join('ind2020_tmae_arboles_lugares as ti', 'ti.id', 'id_lugar')
                            ->join('ind2020_tma_mantenimientos_areas_tipo_servicios as ser', 'ser.id', 'id_tipo_servicio')
                            ->where(['ind2020_tmov_mantenimientos_areas.estado' => 'ACT', 'id_indicador_cab' => $post->id])
                            ->get();
                        foreach ($detalle as $key => $value) {
                            # code...
                            $detalle_html .= '<b>Beneficiarios</b>: ' . $value->n_beneficiarios . '<br>';
                            $detalle_html .= '<b>Inversión:</b> ' . $value->inversion . '<br>';
                            $detalle_html .= '<b>Tipo lugar:</b> ' . $value->lugar . '<br>';
                            $detalle_html .= '<b>Tipo servicio:</b> ' . $value->tipo . '<br>';
                            $detalle_html .= '<b>Cantidad m<sup>2</sup>:</b> ' . $value->cantidad . '<br>';
                            $detalle_html .= '<b>Observaciones:</b> ' . $value->observaciones . '<br>';
                            $detalle_html .= '<b>Fecha:</b> ' .  $post->fecha_registro . '<br>';
                        }
                        break;
                    default:
                        # code...
                        break;
                }
                $nestedData['acciones'] = $acciones;
                $nestedData['barrio_id'] = $post->barrio;
                $nestedData['id_indicador'] = $post->tipo;
                $nestedData['parroquia_id'] = $post->parroquia;
                $nestedData['detalle'] = $detalle_html;
                $nestedData['created_at'] = date('Y-m-d H:m:s', strtotime($post->created_at));
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        // $id = intval(Crypt::decrypt($id));

        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function consultar_sancion()
    {
        try {
            //code...
            $id = Input::get('id');
            $sancion = Indicadores2020SancionesTiposModel::where(['estado' => 'ACT', 'id' => $id])->first();
            if ($sancion)
                return $sancion->valor;
            else
                return '';
        } catch (\Throwable $th) {

            return '';
        }
    }
    public function consultar_mascotas()
    {
        try {
            //code...
            $id = Input::get('id');
            $sancion = Indicadores2020FaunaRazaMascota::where(['estado' => 'ACT', 'id_tipo' => $id])->orderBy('raza', 'ASC')->get();
            return $sancion;
        } catch (\Throwable $th) {

            return [];
        }
    }
    public function getInicadoresDireccion($id)
    {
        try {
            //code...
            $sancion = Indicadores2020TipoModel::where(['estado' => 'ACT', 'id_direccion' => $id])->orderBy('tipo', 'ASC')->get();
            return $sancion;
        } catch (\Throwable $th) {

            return [];
        }
    }
    public function consultarvalorAtencion()
    {
        try {
            //code...
            $id = Input::get('tipo');
            $sancion = Indicadores2020FaunaTipoAtencionModel::where(['estado' => 'ACT', 'id' => $id])->first();
            if ($sancion)
                return $sancion->valor;
            else
                return '';
        } catch (\Throwable $th) {

            return '';
        }
    }

    public function agregar_item()
    {
        //
        try {
            $tipo = Input::get('tipo');
            $categoria = Input::get('nombre');

            // dd(Input::all());
            switch ($tipo) {
                case 1:
                    $item = Indicadores2020EventosTiposModel::where('tipo', $categoria)->first();
                    if ($item) {
                        $datos = Indicadores2020EventosTiposModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => $categoria . ", ya se encuentra registrado", "datos" => $datos], 200);
                    }
                    if ($categoria == null ||  $categoria == "") {
                        $datos = Indicadores2020EventosTiposModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => "No se permite agregar campos vacios", "datos" => $datos], 200);
                    }

                    $item = new Indicadores2020EventosTiposModel();
                    $item->tipo = $categoria;
                    $item->save();
                    $datos = Indicadores2020EventosTiposModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                    break;
                case 2:
                    $item = Indicadores2020EventosTiposArtistaModel::where('tipo_artista', $categoria)->first();
                    if ($item) {
                        $datos = Indicadores2020EventosTiposArtistaModel::select('tipo_artista as nombre', 'id')->orderby('tipo_artista', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => $categoria . ", ya se encuentra registrado", "datos" => $datos], 200);
                    }
                    if ($categoria == null ||  $categoria == "") {
                        $datos = Indicadores2020EventosTiposArtistaModel::select('tipo_artista as nombre', 'id')->orderby('tipo_artista', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => "No se permite agregar campos vacios", "datos" => $datos], 200);
                    }
                    $item = new Indicadores2020EventosTiposArtistaModel();
                    $item->tipo_artista = $categoria;
                    $item->save();
                    $datos = Indicadores2020EventosTiposArtistaModel::select('tipo_artista as nombre', 'id')->orderby('tipo_artista', 'ASC')->get();
                    break;
                case 3:
                    $item = Indicadores2020ActividadesFisicasTipoModel::where('tipo', $categoria)->first();
                    if ($item) {
                        $datos = Indicadores2020ActividadesFisicasTipoModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => $categoria . ", ya se encuentra registrado", "datos" => $datos], 200);
                    }
                    if ($categoria == null ||  $categoria == "") {
                        $datos = Indicadores2020ActividadesFisicasTipoModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => "No se permite agregar campos vacios", "datos" => $datos], 200);
                    }
                    $item = new Indicadores2020ActividadesFisicasTipoModel();
                    $item->tipo = $categoria;
                    $item->save();
                    $datos = Indicadores2020ActividadesFisicasTipoModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                    break;
                case 4:
                    $item = Indicadores2020ArbolesSembradosServiciosModel::where('tipo', $categoria)->first();
                    if ($item) {
                        $datos = Indicadores2020ArbolesSembradosServiciosModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => $categoria . ", ya se encuentra registrado", "datos" => $datos], 200);
                    }
                    if ($categoria == null ||  $categoria == "") {
                        $datos = Indicadores2020ArbolesSembradosServiciosModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => "No se permite agregar campos vacios", "datos" => $datos], 200);
                    }
                    $item = new Indicadores2020ArbolesSembradosServiciosModel();
                    $item->tipo = $categoria;
                    $item->save();
                    $datos = Indicadores2020ArbolesSembradosServiciosModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                    break;
                case 5:
                    $item = Indicadores2020ArbolesSembradosLugaresModel::where('lugar', $categoria)->first();
                    if ($item) {
                        $datos = Indicadores2020ArbolesSembradosLugaresModel::select('lugar as nombre', 'id')->orderby('lugar', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => $categoria . ", ya se encuentra registrado", "datos" => $datos], 200);
                    }
                    if ($categoria == null ||  $categoria == "") {
                        $datos = Indicadores2020ArbolesSembradosLugaresModel::select('lugar as nombre', 'id')->orderby('lugar', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => "No se permite agregar campos vacios", "datos" => $datos], 200);
                    }
                    $item = new Indicadores2020ArbolesSembradosLugaresModel();
                    $item->lugar = $categoria;
                    $item->save();
                    $datos = Indicadores2020ArbolesSembradosLugaresModel::select('lugar as nombre', 'id')->orderby('lugar', 'ASC')->get();
                    break;
                case 6:
                    $item = Indicadores2020SeguridadCiudadanaServiciosModel::where('tipo', $categoria)->first();
                    if ($item) {
                        $datos = Indicadores2020SeguridadCiudadanaServiciosModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => $categoria . ", ya se encuentra registrado", "datos" => $datos], 200);
                    }
                    if ($categoria == null ||  $categoria == "") {
                        $datos = Indicadores2020SeguridadCiudadanaServiciosModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => "No se permite agregar campos vacios", "datos" => $datos], 200);
                    }
                    $item = new Indicadores2020SeguridadCiudadanaServiciosModel();
                    $item->tipo = $categoria;
                    $item->save();
                    $datos = Indicadores2020SeguridadCiudadanaServiciosModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                    break;
                case 7:
                    $item = Indicadores2020TurismoServiciosModel::where('tipo', $categoria)->first();
                    if ($item) {
                        $datos = Indicadores2020TurismoServiciosModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => $categoria . ", ya se encuentra registrado", "datos" => $datos], 200);
                    }
                    if ($categoria == null ||  $categoria == "") {
                        $datos = Indicadores2020TurismoServiciosModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => "No se permite agregar campos vacios", "datos" => $datos], 200);
                    }
                    $item = new Indicadores2020TurismoServiciosModel();
                    $item->tipo = $categoria;
                    $item->save();
                    $datos = Indicadores2020TurismoServiciosModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                    break;
                case 8:
                    $item = Indicadores2020MantenimientoAreasServicioModel::where('tipo', $categoria)->first();
                    if ($item) {
                        $datos = Indicadores2020MantenimientoAreasServicioModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => $categoria . ", ya se encuentra registrado", "datos" => $datos], 200);
                    }
                    if ($categoria == null ||  $categoria == "") {
                        $datos = Indicadores2020MantenimientoAreasServicioModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                        return response()->json(["estado" => "error", "msg" => "No se permite agregar campos vacios", "datos" => $datos], 200);
                    }
                    $item = new Indicadores2020MantenimientoAreasServicioModel();
                    $item->tipo = $categoria;
                    $item->save();
                    $datos = Indicadores2020MantenimientoAreasServicioModel::select('tipo as nombre', 'id')->orderby('tipo', 'ASC')->get();
                    break;

                default:
                    # code...
                    break;
            }
            return response()->json(["estado" => "ok", "msg" => "Agregado Exitosamente", "datos" => $datos, "seleccionado" => $item->id], 200);
        } catch (\Throwable $th) {
            $datos = [];
            //throw $th;
            return response()->json(["estado" => "error", "msg" => "No se pudo agregar" . ' ' . $th->getLine() . ' ' . $th->getMessage(), "datos" => $datos], 200);
        }
    }
}
