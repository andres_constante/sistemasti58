<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\Http\Controllers\ReportesJasperController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;

class ReporteCirculacionController extends Controller
{
    var $escoja = array(0 => "TODOS");
    var $formatos = array("pdf" => "PDF", "xls" => "XLS");
    var $objects = '[
        {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "fecha_ini", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "fecha_fin", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "text", "Descripcion": "RUC", "Nombre": "ruc", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "text", "Descripcion": "Placa", "Nombre": "placa_c", "Clase": "mayuscula", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Actividad", "Nombre": "actividad", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Tipo de vehiculo", "Nombre": "tipo_vehiculo", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
        {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
        {"Tipo": "select", "Descripcion": "Estado", "Nombre": "estado", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]';

    public function __construct(ReportesJasperController $ReportJasper)
    {
        $this->middleware('auth', ['except' => ['reportescoordinacionactividades', 'reporteincidencias']]);
        $this->ReportJasper = $ReportJasper;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $validate_js = [];
        $general_config = ['REPORTE DE REGISTRO DE CIRCULACION', 'coordinacioncronograma/reportecirculaciones', ''];
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);

        $funcionjs = "
        <script type='text/javascript'>        
        function titurepor()
        {
            var entidad=$('#idtipo_entidad').find('option:selected').text()+':';
            var estado =  $('#estado').val();
            var fecha_ini = $('#fecha_ini').val();
            var fecha_fin = $('#fecha_fin').val();
            var id_responsable = $('#id_responsable').val();
            
            $('#tituloreporte').val('fdgfhgf');
            
        }             
          $(document).ready(function() {
                 $('#tituloreporte').val('fdgfhgf');
            $('#namereporte').change(function(event) {                
                return titurepor();
            });
            $('#namereporte').trigger('change');
          });
        </script>
        ";
        $formatosjs = array(
            "rpt_reporte_permisos_circulacion" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_reporte_permisos_circulacion" => '{"pdf":"PDF"}'
        );

        $namerepor = array(
            "rpt_reporte_permisos_circulacion" => "Reporte de incidencias detallado"
        );
        $formatosjs = array(
            "rpt_reporte_permisos_circulacion" => '{"html":"Tabla HTML","pdf":"PDF","xls":"XLS"}'

        );
        $tiporepor = array(
            "rpt_reporte_permisos_circulacion" => '{"pdf":"PDF"}'
        );

        $objeto = json_decode($this->objects);
        $fecha_ini = "2020-05-04";
        $objeto[0]->Valor = $fecha_ini;
        $fecha_fin = date('Y-m-d');
        $objeto[1]->Valor = $fecha_fin;
        // dd($objeto);

        $tipos = explodewords('Entrega de comida,Entrega de materiales de salud,Insumos de oficina,Mecánica,Otros', ',');
        // $objeto[2]->Valor = $this->escoja;
        $objeto[4]->Valor = $this->escoja + $tipos;
        $objeto[5]->Valor =  $this->escoja + [
            'Automóvil' => 'Automóvil',
             'Moto' => 'Moto',
             'Camioneta' => 'Camioneta',
             'Camión (furgón)' => 'Camión (furgón)'
            ];
        $objeto[6]->Valor = $namerepor;
        $objeto[7]->Valor =  $this->formatos;
        $objeto[8]->Valor = ["pdf" => "PDF"];
        $objeto[9]->Valor = [
            0 => 'TODOS',
            126 => 'EN REVISIÓN',
            127 => 'FINALIZADO / APROBADO',
            128 => 'FINALIZADO / NEGADO'
        ];
        $funcalljs = "titurepor();";

        return view('reports.create', [
            'objects' => $objeto,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor,
            "funcalljs" => $funcalljs
        ]);
    }
    public function reportecirculaciones()
    {

        return $this->generarreporte();
    }

    public function generarreporte()
    {
        $data = json_decode(Input::get("data"));
        $paramadi = Crypt::decrypt(Input::get("paramadi"));
        $paramsok = array();
        foreach ($data as $key => $value) {
            # code...
            //show($value);
            if ($value->name != "_token" && $value->name != "formato" && $value->name != "namereporte" && $value->name != "tituloreporte" && $value->name != "tiporeporte")
                $paramsok[$value->name] = $value->value;
            elseif ($value->name == "formato")
                $formato = $value->value;
            elseif ($value->name == "namereporte")
                $namedata = $value->value;
            elseif ($value->name == "tituloreporte")
                $paramadi["TITULO"] = mb_strtoupper($value->value);
            elseif ($value->name == "tiporeporte")
                if ($value->value == "informe" || $value->value == "observa")
                    $namedata .= "_" . $value->value;
        }
        $filename = "Registros-" . str_random(6);
        $ruta = '';

        $paramsok['cedula_usuario'] = Auth::user()->cedula;
        $param =  $paramsok + $paramadi;
        foreach ($param as $key => $value) {
            # code...
            $ruta .= $key . '=' . $value . '&';
        }
        return $this->ReportJasper->generateReportByJson($namedata, $ruta, $formato, $filename, true, false, 10);
    }
}
