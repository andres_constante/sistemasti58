<?php namespace Modules\Coordinacioncronograma\Http\Controllers;


use Illuminate\Routing\Controller;
use App\ModulosModel;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Route;
use Auth;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaDetaModel;
use DB;
use App\UsuariosModel;
use DateTime;


class CoordinacionChartsController extends Controller
{
    var $configuraciongeneral = array("Reporte Estadístico de Actividades", "coordinacioncronograma/coordinaciongraficos", "index");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"actividadespordireccion","Descripcion":"ACTIVIDADES","Nombre":"actividadespordireccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"chart2","Descripcion":"GRÁFICO DE ACTIVIDADES POR DIRECCIÓN","Nombre":"chart4","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"chart","Descripcion":"GRÁFICO DE ACTIVIDADES POR DIRECCIONES","Nombre":"chart3","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        
        ]';


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $objetos = json_decode($this->objetos);
        $objetos = array_values($objetos);

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $id_usuario = Auth::user()->id;
        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {

            $direcciones = direccionesModel::
            join("users as us", "us.id_direccion", "=", "tmae_direcciones.id")
                ->select('tmae_direcciones.id', 'direccion')->where([['tmae_direcciones.estado', '=', 'ACT'], ["us.id", $id_usuario]])->orderby("direccion", "asc")->get();
            unset($objetos[2]);

        } else {
            $objetos[2]->Nombre = "chart5";
            $direcciones = CoorCronogramaCabModel::
            join("coor_tmov_cropnograma_resposables as cr", "cr.id_crono_cab", "=", "coor_tmov_cronograma_cab.id")
                ->join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                ->select('a.id', 'a.direccion')
                ->groupBy('a.direccion')
                ->get();
            //show($direcciones);
        }

        //show($modificacionesHoy);
        $datos = CoorCronogramaCabModel::select(DB::raw("count(id) as total,estado_actividad,'0' as color,0 as porcentaje"))
            ->groupBy("estado_actividad")
            ->where("estado", "ACT")
            ->get();
        $total = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
            ->where("estado", "ACT")
            ->first();
        $iboxActividades = $this->getActividades($datos, $total);
        $totalacti = $total;
        unset($datos);
        unset($total);


        $datos = CoorObrasModel::select(DB::raw("count(id) as total,estado_obra"))
            ->groupBy("estado_obra")
            ->get();
        $total = CoorObrasModel::select(DB::raw("count(id) as total"))
            ->groupBy("estado_obra")
            ->first();
        $iboxObras = $this->getObras($datos, $total);


        $actividadesActualizadas = $this->getTimeLineCab(0, 1);

        //show($objetos);


        return view('vistas.indexchart', [
            "objetos" => $objetos,
            "direcciones" => $direcciones,
            "iboxActividades" => $iboxActividades,
            "iboxObras" => $iboxObras,
            "actividadesActualizadas" => $actividadesActualizadas,
            "configuraciongeneral" => $this->configuraciongeneral,
            "totalacti" => $totalacti,
            "delete" => "si",
            "create" => "si"
        ]);
    }

    public function getTimeLineCab($id, $tipo)
    {
        $sw = 0;
        switch ($tipo) {
            case 1:
                $tabla = CoorCronogramaDetaModel::
                select("coor_tmov_cronograma_deta.*", "u.name", "dir.direccion")
                    ->join("users as u", "u.id", "=", "coor_tmov_cronograma_deta.id_usuario")
                    ->join("tmae_direcciones as dir", "dir.id", "=", "coor_tmov_cronograma_deta.id_direccion")
                    //->where('id_crono_cab',$id)
                    ->offset(1)
                    ->limit(5)
                    ->orderBy("coor_tmov_cronograma_deta.updated_at", "DESC")
                    ->get();
                return $tabla;
                break;
        }
    }

    public function getActividades($datos, $total)
    {

        $estadoobras = explode("|", ConfigSystem("estadoobras"));
        $estadoobrascolores = explode("|", ConfigSystem("estadoobrascolor"));


        foreach ($datos as $item) {
            switch ($item->estado_actividad) {

                case $estadoobras[0]:
                    $item->estado_actividad = 'EJECUTADO VENCIDO';
                    $item->color = $estadoobrascolores[0];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);

                    break;
                case $estadoobras[1]:
                    $item->estado_actividad = 'EJECUTADO';
                    $item->color = $estadoobrascolores[1];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);
                    break;
                case $estadoobras[2]:
                    $item->estado_actividad = 'EJECUCIÓN';
                    $item->color = $estadoobrascolores[2];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);

                    break;
                case $estadoobras[3]:
                    $item->estado_actividad = 'POR EJECUTAR';
                    $item->color = $estadoobrascolores[3];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);

                    break;
                case $estadoobras[4]:
                    $item->color = $estadoobrascolores[4];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);
                    break;

            }


        }
        return $datos;

    }

    public function getObras($datos, $total)
    {

        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $estadoobrascolores = explode("|", ConfigSystem("estadoobrascolor"));


        foreach ($datos as $item) {
            switch ($item->estado_obra) {
                case 'EJECUTADO_VENCIDO':
                    $item->estado_obra = 'EJECUTADO VENCIDO';
                    $item->color = $estadoobrascolores[0];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);

                    break;
                case 'EJECUTADO':
                    $item->estado_obra = 'EJECUTADO';
                    $item->color = $estadoobrascolores[1];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);
                    break;
                case 'EJECUCION':
                    $item->estado_obra = 'EJECUCIÓN';
                    $item->color = $estadoobrascolores[2];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);

                    break;
                case 'POR_EJECUTAR':
                    $item->estado_obra = 'POR EJECUTAR';
                    $item->color = $estadoobrascolores[3];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);

                    break;
                case 'SUSPENDIDO':
                    $item->color = $estadoobrascolores[4];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);
                    break;

            }


        }
        return $datos;

    }


    public function getChartLine()
    {
        $id_direccion = Input::get('id');
        $tipo = Input::get('tipo');
        //show($tipo);
        $datos = null;
        if ($tipo == 1) {
            $direcciones = direccionesModel::select("id", "direccion")->where("estado", "ACT")->get();
            foreach ($direcciones as $direccion) {
                $EJECUCION = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
                    ->where([["estado", "ACT"], ["estado_actividad", "EJECUCION"], ["id_direccion", $direccion->id]])
                    ->first();

                $EJECUTADO = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
                    ->where([["estado", "ACT"], ["estado_actividad", "EJECUTADO"], ["id_direccion", $direccion->id]])
                    ->first();
                $EJECUTADO_VENCIDO = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
                    ->where([["estado", "ACT"], ["estado_actividad", "EJECUTADO_VENCIDO"], ["id_direccion", $direccion->id]])
                    ->first();
                $POR_EJECUTAR = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
                    ->where([["estado", "ACT"], ["estado_actividad", "POR_EJECUTAR"], ["id_direccion", $direccion->id]])
                    ->first();
                $SUSPENDIDO = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
                    ->where([["estado", "ACT"], ["estado_actividad", "SUSPENDIDO"], ["id_direccion", $direccion->id]])
                    ->first();

                $datos[] = [$direccion->direccion, $EJECUCION->total, $EJECUTADO->total, $EJECUTADO_VENCIDO->total, $POR_EJECUTAR->total, $SUSPENDIDO->total];

            }

        } else if ($tipo == 2) {
            $direcciones = direccionesModel::select("id", "direccion")->where([["estado", "ACT"], ["id", $id_direccion]])->get();
            $EJECUCION = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
                ->where([["estado", "ACT"], ["estado_actividad", "EJECUCION"], ["id_direccion", $id_direccion]])
                ->first();

            $EJECUTADO = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
                ->where([["estado", "ACT"], ["estado_actividad", "EJECUTADO"], ["id_direccion", $id_direccion]])
                ->first();
            $EJECUTADO_VENCIDO = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
                ->where([["estado", "ACT"], ["estado_actividad", "EJECUTADO_VENCIDO"], ["id_direccion", $id_direccion]])
                ->first();
            $POR_EJECUTAR = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
                ->where([["estado", "ACT"], ["estado_actividad", "POR_EJECUTAR"], ["id_direccion", $id_direccion]])
                ->first();
            $SUSPENDIDO = CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
                ->where([["estado", "ACT"], ["estado_actividad", "SUSPENDIDO"], ["id_direccion", $id_direccion]])
                ->first();

            $datos[] = [$direcciones[0]->direccion, $EJECUCION->total, $EJECUTADO->total, $EJECUTADO_VENCIDO->total, $POR_EJECUTAR->total, $SUSPENDIDO->total];

        } else if ($tipo == 3) {

            $avances = CoorCronogramaCabModel::select(DB::raw("dir.id"))
                ->join("tmae_direcciones as dir", "dir.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                ->where([["coor_tmov_cronograma_cab.estado", "ACT"], ["estado_actividad", "EJECUCION"]])
                ->groupBy("dir.id")
                ->get();

            foreach ($avances as $item) {
                //show($item->id);
                $valor = CoorCronogramaCabModel::select(DB::raw("dir.direccion, avg(avance) as porcentaje"))
                    ->join("tmae_direcciones as dir", "dir.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                    ->where([["coor_tmov_cronograma_cab.estado", "ACT"], ["dir.id", $item->id]])
                    ->groupBy('dir.direccion')
                    ->first();
                if ($valor->porcentaje == 0) {
                    $datos[] = [$valor->direccion, null];
                } else {
                    $datos[] = [$valor->direccion, round($valor->porcentaje, 2)];
                }


            }


        } else if ($tipo == 4) {
            $fechainicial = date("Y-m-d") . " 00:00:00";
            //$fechainicial="2019-06-01 00:00:00";
            $fechafinal = date("Y-m-d") . " 23:59:59";
            $modificacionesHoy = CoorCronogramaCabModel::
            join("tmae_direcciones as dir", "dir.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                ->join("users as u", "u.id", "=", "coor_tmov_cronograma_cab.id_usuario")
                ->join("ad_perfil as p", "u.id_perfil", "=", "p.id")
                ->select("dir.direccion", DB::raw("count(*) as total"))
                ->whereBetween("coor_tmov_cronograma_cab.updated_at", [$fechainicial, $fechafinal])
                ->where("p.tipo", "<>", 3)
                ->where("p.tipo", "<>", 1)
                ->groupBy("dir.direccion")
                ->get();


            foreach ($modificacionesHoy as $item) {

                $datos[] = [$item["direccion"], $item["total"]];
            }


        }


        return response()->json($datos);

    }

    public function getValoresCharts()
    {
        $tipo_chart = Input::get("tipo");
        $direccion = Input::get("direccion");
        $valores;
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $id_usuario = Auth::user()->id;
        $fecha = new DateTime();
        $fecha->modify('this week');
        $fechadesde = $fecha->format('Y-m-d');
        $fecha->modify('this week +4 days');
        $fechahasta = $fecha->format('Y-m-d');
        $querygeneral = queryrendimiento($fechadesde, $fechahasta);
        switch ($tipo_chart) {
            case '1':


                $valores = CoorObrasModel::select(DB::raw('count(estado_obra) as valor, estado_obra'))
                    ->where("coor_tmov_obras_listado.estado", "ACT")->groupby("estado_obra")->get();
                return $valores;

                break;
            case '2':
                switch ($id_tipo_pefil->tipo) {
                    case 1:
                        $valores = CoorCronogramaCabModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
                            ->where("coor_tmov_cronograma_cab.estado", "ACT")->groupby("estado_actividad")->get();
                        return $valores;
                        break;
                    case 2:

                        $valores = CoorCronogramaCabModel::
                        join("coor_tmov_cropnograma_resposables as cr", "cr.id_crono_cab", "=", "coor_tmov_cronograma_cab.id")
                            ->join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                            ->select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
                            ->where([["coor_tmov_cronograma_cab.estado", "ACT"], ["cr.id_usuario", $id_usuario]])
                            ->get();
                        return $valores;

                        break;
                    case 3:
                        $valores = CoorCronogramaCabModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
                            ->join("users as u", "u.id_direccion", "=", "coor_tmov_cronograma_cab.id_direccion")
                            ->where([["coor_tmov_cronograma_cab.estado", "ACT"], ["u.id", $id_usuario]])
                            ->groupby("estado_actividad")->get();
                        return $valores;
                        return $valores;
                        break;
                    case 4:
                        $valores = CoorCronogramaCabModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
                            ->where("coor_tmov_cronograma_cab.estado", "ACT")->groupby("estado_actividad")->get();
                        return $valores;
                        break;

                }
                break;
            case '3':
                switch ($id_tipo_pefil->tipo) {
                    case 1:
                        $valores = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                            ->select(DB::raw('count(id_direccion) as valor, a.direccion'))
                            ->where("coor_tmov_cronograma_cab.estado", "ACT")->groupby("a.direccion")->get();
                        //
                        $valores = DB::select($querygeneral);


                        $newValore = [];


                        foreach ($valores as $key => $valor) {
                            $newValore[$key] = [$valor->direccion, floatval($valor->valor)];

                            // for ($i=0; $i < count($valores); $i++) {
                            //     if($i==$key){
                            //         $newValore[$key][$i+1]=intVal($valor->valor);
                            //     }else{
                            //         $newValore[$key][$i+1]=null;
                            //     }


                            // }

                        }
                        //show("Hol");
                        return $newValore;

                        break;
                    case 2:
                        $valores = CoorCronogramaCabModel::
                        join("coor_tmov_cropnograma_resposables as cr", "cr.id_crono_cab", "=", "coor_tmov_cronograma_cab.id")
                            ->join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                            ->select(DB::raw('count(coor_tmov_cronograma_cab.id) as valor, a.direccion'))
                            ->where([["coor_tmov_cronograma_cab.estado", "ACT"], ["cr.id_usuario", $id_usuario]])
                            ->groupby("a.direccion")
                            ->get();
                        return $valores;

                        break;
                    case 3:
                        //show("estoy aqui");
                        $valores = CoorCronogramaCabModel::
                        join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                            ->join("users as u", "u.id_direccion", "=", "coor_tmov_cronograma_cab.id_direccion")
                            ->select(DB::raw('count(*) as valor, a.direccion'))
                            ->where([["coor_tmov_cronograma_cab.estado", "ACT"], ["u.id", $id_usuario]])
                            ->groupby("a.direccion")->get();
                        return $valores;
                        break;
                    case 4:
                        $valores = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                            ->select(DB::raw('count(id_direccion) as valor, a.direccion'))
                            ->where("coor_tmov_cronograma_cab.estado", "ACT")->groupby("a.direccion")->get();
                        $valores = DB::select($querygeneral);

                        $newValore = [];

                        foreach ($valores as $key => $valor) {
                            $newValore[$key] = [$valor->alias, floatval($valor->Rendimiento)];

                            // for ($i=0; $i < count($valores); $i++) {
                            //     if($i==$key){
                            //         $newValore[$key][$i+1]=intVal($valor->valor);
                            //     }else{
                            //         $newValore[$key][$i+1]=null;
                            //     }


                            // }


                        }
                        //show($newValore);
                        return $newValore;
                        break;

                }
                break;
            case '4':
                $valores = CoorCronogramaCabModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
                    ->where([["coor_tmov_cronograma_cab.estado", "ACT"], ["coor_tmov_cronograma_cab.id_direccion", $direccion]])->groupby("estado_actividad")->get();
                return $valores;
                break;

                return response()->json($valores);
        }
    }

    /*Funciones Filtro Detalle Ajax*/
    public function ajaxreportehtmlfiltrosactividades($semanaactual="no")
    {
        $sql = queryactividades();
        $tabla = DB::select($sql);
        $fechaactudesde = Input::get("FECHAACTUDESDE");
        $fechaactuhasta = Input::get("FECHAACTUHASTA");
        //show($fechaactudesde);
        //show(queryrendimiento($fechaactudesde, $fechaactuhasta));
        $rendimiento = DB::select(queryrendimiento($fechaactudesde, $fechaactuhasta,$semanaactual));
        //show($rendimiento);
        $objetos = '[
		{"Tipo":"text","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Estado","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Avance %","Nombre":"avance","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Rendimiento %","Nombre":"Rendimiento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },		
		{"Tipo":"text","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },				
		{"Tipo":"text","Descripcion":"Última Modificación","Nombre":"fecha_update","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
	    ]';
        $objetos = json_decode($objetos);
        $configuraciongeneral = ['Reporte general de Actividades por Rendimiento', 'coordinacioncronograma/cronogramacompromisos'];
        return view('vistas.reporteshtml.rendimientoactividades', [
            "tabla" => $tabla,
            "objetos" => $objetos,
            "configuraciongeneral" => $configuraciongeneral,
            "rendimiento" => $rendimiento
        ]);
    }
}
