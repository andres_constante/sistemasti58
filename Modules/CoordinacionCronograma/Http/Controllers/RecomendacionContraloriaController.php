<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Crypt;


use Modules\Coordinacioncronograma\Entities\CabRecomendacionContraModel;
use Modules\Coordinacioncronograma\Entities\CabRecomendacionContraHistoModel;


use App\ArchivosModel;
use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use stdClass;

class RecomendacionContraloriaController extends Controller
{
    var $configuraciongeneral = array("Recomendaciones Contraloría", "coordinacioncronograma/recomendaciones", "index", 6 => "coordinacioncronograma/recomendacionesajax", 7 => "recomendaciones");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"select","Descripcion":"Tipo Documento","Nombre":"tipo_documento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Nombre de Examen","Nombre":"nombre_examen","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"# Documento","Nombre":"numero_documento","Clase":"numeros-letras-guion mayuscula","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Recomendación","Nombre":"recomendacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Periodo","Nombre":"periodo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado_proceso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Actividad","Nombre":"actividad","Clase":"maxlength:350","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Entrega","Nombre":"fecha_entrega","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Aprobación","Nombre":"fecha_aprobacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },     
        {"Tipo":"select","Descripcion":"Verificación","Nombre":"verificacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Observación de la verificación","Nombre":"observacion_verificacion","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" }        
                  ]';
    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "recomendacion" => "recomendacion: {
                            required: true
                        }",
        "estado_proceso" => "estado_proceso: {
                            required: true
                        }",
        "actividad" => "actividad: {
                            required: true
                        }",
        "fecha_inicio" => "fecha_inicio: {
                            required: true
                        }",
        "fecha_fin" => "fecha_fin: {
                            required: true
                        }"
    );

    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }

    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        $object->perfil = $id_tipo_pefil->tipo;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 7) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        }
        //show($object);
        return $object;
    }
    public function asignaroles($objetos, $permisos, $tabla)
    {
        //show($permisos);
        if ($permisos->perfil == 1 || $permisos->perfil == 4 || $permisos->perfil == 10 || Auth::user()->id == 1864)
            return $objetos;
        //if ($permisos->editar_parcialmente == 'SI' && $permisos->guardar == 'SI') {
        //show($objetos);
        $tipo = "text";
        foreach ($objetos as $key => $value) {
            if ($value->Tipo == "select" && $value->Nombre != "avance") {
                $tipo = "selectdisabled";
                if ($objetos[$key]->ValorAnterior != "Null") {
                    $pos = $objetos[$key]->ValorAnterior;
                    if (!is_array($pos))
                        $objetos[$key]->Valor = array($pos => $value->Valor[$pos]);
                    else
                        foreach ($pos as $keyaa => $vaa)
                            $objetos[$key]->Valor = array($keyaa => $value->Valor[$vaa]);
                } else {
                    //
                    $in = $objetos[$key]->Nombre;
                    $kk = $tabla->$in;
                    $aar = $objetos[$key]->Valor;
                    if (array_key_exists($kk, $aar)) {
                        //$objetos[$key]->Tipo = "html2";
                        $objetos[$key]->Valor = array($kk => $aar[$kk]);
                    }
                }
            } elseif ($value->Tipo == "text") {
                $tipo = "textdisabled";
                //$objetos[$key]->Valor ="";
            } elseif ($value->Tipo == "textarea"  && $value->Nombre != "actividad") {
                $tipo = "textarea-disabled";
                //$objetos[$key]->Valor ="Hola";
            } elseif ($value->Tipo == "datetext") {
                $tipo = "textdisabled";
                //$objetos[$key]->Valor ="Hola";
            } else
                $tipo = $objetos[$key]->Tipo;
            $objetos[$key]->Tipo = $tipo;
            //show($tabla);

        }
        //}
        //show($objetos);
        return $objetos;
    }
    public function verpermisos($objetos = array(), $tipo = "index")
    {
        //show($objetos);
        $tabla = CabRecomendacionContraModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmae_recomendacion_contraloria.id_direccion")
            ->select("coor_tmae_recomendacion_contraloria.*", "a.direccion")
            ->where("coor_tmae_recomendacion_contraloria.estado", "ACT");
        $seleccionar_direccion = direccionesModel::where("estado", "ACT");
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $id_usuario = Auth::user()->id;
        $sw = 0;
        if ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 2) {
            if (Auth::user()->id != 1864) {
                if (count($objetos)) {
                    unset($objetos[10]);
                    unset($objetos[11]);
                }
                $seleccionar_direccion = $seleccionar_direccion->where("id", Auth::user()->id_direccion);
                $tabla = $tabla->where("coor_tmae_recomendacion_contraloria.id_direccion", Auth::user()->id_direccion);
            }
        }
        $seleccionar_direccion = $seleccionar_direccion->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        if (count($objetos))
            $objetos[7]->Valor = $this->escoja + $seleccionar_direccion;
        if ($tipo == "index")
            $objetos[7]->Nombre = "direccion";
        //show(array($objetos, $tabla->get()));
        return array($objetos, $tabla->orderby("coor_tmae_recomendacion_contraloria.id", "desc"));
    }

    function recomendacionesajax(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        $objetos = $tabla[0];
        $tabla = $tabla[1];
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('coor_tmae_recomendacion_contraloria.id', 'LIKE', "%{$search}%")
                    ->orWhere('coor_tmae_recomendacion_contraloria.recomendacion', 'LIKE', "%{$search}%")
                    ->orWhere('coor_tmae_recomendacion_contraloria.estado_proceso', 'LIKE', "%{$search}%")
                    ->orWhere('coor_tmae_recomendacion_contraloria.actividad', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            $permisos = $this->perfil();
            foreach ($posts as $post) {
                $show = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                $edit = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id), "menu" => "no"), array('class' => 'fa fa-pencil-square-o', 'onclick' => 'popup(this)'));
                $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                if ($permisos->ver == 'NO')
                    $show = "";
                if ($permisos->editar == 'NO')
                    $edit = "";
                if ($permisos->eliminar == 'NO')
                    $dele = "";
                $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public
    function index()
    {
        $tabla = [];
        $objetos = $this->verpermisos(json_decode($this->objetos));
        return view('vistas.index', [
            "objetos" => $objetos[0],
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si",
            "permisos" => $this->perfil()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public
    function create()
    {
        $tipodocu = explodewords(ConfigSystem("documentocontraloria"), "|");

        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);
        $objetos[0]->Valor = $this->escoja + $tipodocu;
        $estado = $this->escoja + $estadoobras;
        $objetos[5]->Valor = $estado;
        $objetos[4]->Descripcion = "Periodo (Ejm. 2019-01-01 hasta " . date("Y-m-d") . ")";

        // show($objetos);
        $verificacion = explodewords(ConfigSystem("verificacion"), "|");
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $objetos[12]->Valor = $this->escoja + $verificacion;
        if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4 && Auth::user()->id != 1864) {
            unset($objetos[12]);
            unset($objetos[13]);
        }

        /**/
        $this->configuraciongeneral[3] = "4";
        $this->configuraciongeneral[4] = "si";
        /*QUITAR CAMPOS*/
        $objetos = $this->verpermisos($objetos, "crear");
        return view('vistas.create', [
            "objetos" => $objetos[0],
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs
        ]);
    }

    public
    function guardar($id)
    {
        $input = Input::all();

        $ruta = $this->configuraciongeneral[1];
        $menuno = "";
        if (Input::has("menu")) {
            $menuno = "?menu=no";
            unset($input["menu"]);
        }
        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new CabRecomendacionContraModel;
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro Variable de Configuración";
        } else {
            $ruta .= "/$id/edit";
            $guardar = CabRecomendacionContraModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Edición Variable de Configuración";
        }
        $arrapas = array();

        $validator = Validator::make($input, CabRecomendacionContraModel::rules($id));

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            $timeline = new CabRecomendacionContraHistoModel;
            foreach ($input as $key => $value) {

                if ($key != "_method" && $key != "_token") {
                    $guardar->$key = $value;
                    $timeline->$key = $value;
                }
            }
            $guardar->id_usuario = Auth::user()->id;
            $guardar->ip = \Request::getClientIp();
            $guardar->pc = \Request::getHost();
            $guardar->save();
            /**/
            $timeline->id_cab_reco = $guardar->id;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->save();


            $idcab = $guardar->id;

            if ($guardar->verificacion == 'VERIFICADO CON OBSERVACIONES') {
                $guardar_verificacion = CabRecomendacionContraModel::find($idcab);
                $guardar_verificacion->estado_proceso = 'EJECUCION_VENCIDO';
                $guardar_verificacion->save();

                $usuario = UsuariosModel::select("users.*")
                    ->join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
                    ->where(["id_direccion" => $guardar->id_direccion])
                    ->whereIn("p.tipo", [3, 10])
                    ->get();
                // dd($usuario);
                foreach ($usuario as $key => $value_) {
                    # code...
                    $this->notificacion->notificacionesweb("Recomendación ha sido verificado con observaciones <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit', $value_->id, "2c438f");
                    $this->notificacion->EnviarEmail($value_->email, "Recomendación estratégico", "Recomendación ha sido verificado con observaciones", $guardar->observacion_verificacion,  $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit');
                }
            }

            Auditoria($msgauditoria . " - ID: " . $id . "-" . Input::get($guardar->recomendacion));
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1] . $menuno);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public
    function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public
    function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("coor_tmae_recomendacion_contraloria.id", $id)->first();

        $this->configuraciongeneral[3] = "5"; //Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "si";
        $dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        return view('vistas.show', [
            "objetos" => $objetos[0],
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public
    function edit($id)
    {
        $tipodocu = explodewords(ConfigSystem("documentocontraloria"), "|");

        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $this->configuraciongeneral[2] = "editar";
        $objetos = json_decode($this->objetos);
        $objetos[0]->Valor = $this->escoja + $tipodocu;
        $estado = $this->escoja + $estadoobras;
        $objetos[5]->Valor = $estado;
        $id = intval(Crypt::decrypt($id));
        //show($tabla);
        //show($objetos);
        $this->configuraciongeneral[3] = "5"; //Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "si";

        $verificacion = explodewords(ConfigSystem("verificacion"), "|");


        /*QUITAR CAMPOS*/
        $objetos = $this->verpermisos($objetos, "crear");
        $tabla = $objetos[1]->where("coor_tmae_recomendacion_contraloria.id", $id)->first();
        $dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        $objetos[0][12]->Valor = $this->escoja + $verificacion;
        $objetos[0][12]->ValorAnterior = $tabla->verificacion;
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $objetos[0][12]->Valor = $this->escoja + $verificacion;
        if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4 && Auth::user()->id != 1864) {
            unset($objetos[0][12]);
            unset($objetos[0][13]);
        }

        $objetos = $this->asignaroles($objetos[0], $this->perfil(), $tabla);
        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
            "tabla" => $tabla,
            "dicecionesImagenes" => $dicecionesImagenes,
            "dicecionesDocumentos" => $dicecionesDocumentos,
            "permisos" => $this->perfil()
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public
    function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public
    function destroy($id)
    {
        //
        //show($id);
        $tabla = CabRecomendacionContraModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
