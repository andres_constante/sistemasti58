<?php namespace Modules\Coordinacioncronograma\Http\Controllers;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use Modules\Coordinacioncronograma\Entities\CoorTipoObraModel;
class CoorTipoObraController extends Controller {
	var $configuraciongeneral = array ("Tipo de Obra", "coordinacioncronograma/tipoobra", "index",6=>"coordinacioncronograma/tipoobraajax",7=>"tipoobra");
    var $objetos = '[
    	{"Tipo":"text","Descripcion":"Tipo de Obra","Nombre":"tipo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
            "tipo"=>"ruc: {
                            required: true
                        }"
        );
    public function __construct() {
        $this->middleware('auth');
    }
	public function index()
	{
        //$tabla=CoorTipoObraModel::where("estado","ACT")->orderby("id","desc")->get();//->paginate(500);
        $tabla=[];
		return view('vistas.index',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>"no",
                "create"=>"si"
                ]);
		//return view('coordinacioncronograma::index');
    }

    public function tipoobraajax(Request $request)
    {
      $columns = array(
                            0 =>'id',
                            1 =>'tipo',
                            7=> 'acciones',
                        );

        $totalData = CoorTipoObraModel::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = CoorTipoObraModel::where("estado","ACT")
                        ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  CoorTipoObraModel::where("estado","ACT")
                            
                            ->where(function($query)use($search){
                                $query->where('id','LIKE',"%{$search}%")
                                ->orWhere('tipo', 'LIKE',"%{$search}%")  ;
                                }) 
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = CoorTipoObraModel::where("estado","ACT")
                            ->where(function($query)use($search){
                                $query->where('id','LIKE',"%{$search}%")
                                ->orWhere('tipo', 'LIKE',"%{$search}%")  ;
                                }) 
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            //show($posts);
            foreach ($posts as $post)
            {


                    $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[7]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o divpopup','target'=>'_blank', 'onclick'=>'popup(this)')).'&nbsp;&nbsp;'.
                    link_to_route(str_replace("/",".",$this->configuraciongeneral[7]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="'.$this->configuraciongeneral[7].'/'.$post->id.'" accept-charset="UTF-8" id="frmElimina'.$post->id.'" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="'.csrf_token().'">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';


                $nestedData['id'] = $post->id;
                $nestedData['tipo'] = $post->tipo;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;

            }
        }
        //show($data);
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );

        return response()->json($json_data);
    }

    






	public function create()
    {
        //
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
	public function guardar($id)
    {
           $input=Input::all();

            $ruta=$this->configuraciongeneral[1];

            if($id==0)
            {
                $ruta.="/create";
                $guardar= new CoorTipoObraModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= CoorTipoObraModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }

            $input=Input::all();
            $arrapas=array();

            $validator = Validator::make($input, CoorTipoObraModel::rules($id));

            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {

                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }
                 }

                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->tipo));
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
  	public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }
    public function show($id)
    {
        //
        $tabla = CoorTipoObraModel::find($id);
        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $tabla = CoorTipoObraModel::find($id);
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "tabla"=>$tabla,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=CoorTipoObraModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
