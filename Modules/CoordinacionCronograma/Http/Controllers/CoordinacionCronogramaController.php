<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Routing\Controller;
use App\ModulosModel;
use App\MenuModulosModel;
use Route;
use Auth;

// use Illuminate\Routing\Route;

class CoordinacionCronogramaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $rucu = Route::currentRouteName();//Route::getCurrentRoute()->getPath();
        $vermodulo = explode(".", $rucu);

        // $rucu=Request::path(); //Route::getCurrentRoute()->getPrefix(); //Route::getCurrentRoute()->getPath();
        // $vermodulo=explode("/",$rucu);
        // show($mod);
        $mod = ModulosModel::where("ruta", $vermodulo[0])->first();
        $usuario = Auth::user();
        $tbnivel = MenuModulosModel::join("menu as a", "a.id", "=", "ad_menu_modulo.id_menu")
            ->join("ad_menu_perfil as b", "b.id_menu_modulo", "=", "ad_menu_modulo.id")
            ->select("ad_menu_modulo.*", "a.menu", "a.icono as iconpng")
            ->where("b.id_perfil", Auth::user()->id_perfil)
            ->where("ad_menu_modulo.adicional", "<>", "hidden");
        if ($mod)
            $tbnivel = $tbnivel->where("ad_menu_modulo.id_modulo", $mod->id);
        $tbnivel = $tbnivel->orderby("ad_menu_modulo.orden")->get();
        //show($tbnivel);
        return view('coordinacioncronograma::index', ["modulo" => $mod, "usuario" => $usuario, "iconos" => $tbnivel, "delete" => "si",
            "create" => "si"]);

        // return view('coordinacioncronograma::index');
    }
}
