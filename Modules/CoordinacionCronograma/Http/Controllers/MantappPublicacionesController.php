<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Modules\CoordinacionCronograma\Entities\MantappPublicacionesArchivosModel;
use Modules\CoordinacionCronograma\Entities\MantappPublicacionesModel;
use GuzzleHttp\Client as GuzzleHttpClient;
use Modules\CoordinacionCronograma\Entities\MantappPublicacionesDetaModel;

class MantappPublicacionesController extends Controller
{
    var $configuraciongeneral = array("PUBLICACIONES MANTAPP", "coordinacioncronograma/publicaciones", "index", 6 => "coordinacioncronograma/publicacionesajax", 7 => "publicaciones");
    var $escoja = array(null => "Escoja opción...");

    var $objetos = '[
        {"Tipo":"text","Descripcion":"Título de la publicación","Nombre":"titulo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Descripción de la publicación","Nombre":"descripcion","Clase":"editor_html","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Link de Facebook","Nombre":"link_facebook","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Link de Instagram","Nombre":"link_instagram","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Link de Twitter","Nombre":"link_twitter","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
		]';

    var $validarjs = array(
        "titulo" => "titulo: {
                required: true
            }",
        "descripcion" => "descripcion: {
                required: true
            }"
    );


    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        $delete = 'si';
        $create = 'si';
        $objetos = json_decode($this->objetos);
        $coordinacion = '{"Tipo":"htmlplantilla","Descripcion":"Fecha de creación","Nombre":"created_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $usuario = '[{"Tipo":"htmlplantilla","Descripcion":"Publicado por ","Nombre":"name","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
        $usuario = json_decode($usuario);
        unset($objetos[1]);
        unset($objetos[2]);
        unset($objetos[3]);
        unset($objetos[4]);
        $objetos = insertarinarray($objetos, $coordinacion, 0);
        $objetos = array_merge($objetos, $usuario);


        // dd($objetos);
        return view('vistas.index', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "create" => $create,
            "delete" => $delete,
            'tabla' => [],
            "id_tipo_pefil" => $id_tipo_pefil
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);

        $coordinacion = '{"Tipo":"htmlplantilla","Descripcion":"Imagenes","Nombre":"imagenes_html","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);

        $coordinacion->Valor = '<div style="text-align: end;"><button type="button" class="btn btn-info" id="add_archivo">
        <i class="fa fa-plus-circle" style="font-size: 1.5em;"> </i> Agregar Item
        </button></div>
        <table class="table table-striped" id="tabla_archivos">
            <thead>
                <tr>
                <th scope="col">Archivo</th>
                <th scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <script type="text/javascript">
        var n_filas_arr=0;
        
        $(function () {
            $("#add_archivo").click(function (e) { 
                $("#tabla_archivos tbody").prepend(\'<tr id="fila_ar\'+n_filas_arr+\'" ><td><input type="file" required accept="image/*" class="form-control" name="archivos[]"></td><td><i onclick="borrar_archivo(\'+n_filas_arr+\')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
                n_filas_arr++;
            });
            $("#add_archivo").trigger("click");
        });
        function borrar_archivo(id){

            $("#fila_ar"+id).remove();
        }
        </script>
';
        // show($coordinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 2);


        $coordinacion = '{"Tipo":"htmlplantilla","Descripcion":"Videos","Nombre":"videos_html","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);

        $coordinacion->Valor = '<div style="text-align: end;"><button type="button" class="btn btn-success" id="add_video">
        <i class="fa fa-plus-circle" style="font-size: 1.5em;"> </i> Agregar Item
        </button></div>
        <table class="table table-striped" id="tabla_videos">
            <thead>
                <tr>
                <th scope="col">Archivo</th>
                <th scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <script type="text/javascript">
        var n_filas_arr_videos=0;
        
        $(function () {
            $("#add_video").click(function (e) { 
                $("#tabla_videos tbody").prepend(\'<tr id="fila_ar_videos\'+n_filas_arr_videos+\'" ><td><input type="file" required accept="video/*" class="form-control" name="videos[]"></td><td><i onclick="borrar_archivo_video(\'+n_filas_arr_videos+\')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
                n_filas_arr_videos++;
            });
        });
        function borrar_archivo_video(id){

            $("#fila_ar_videos"+id).remove();
        }

        function validarPeso(input){
            console.log(input.files[0].size);
            if(input.files[0].size > 47185920){
                toastr["error"]("El peso del archivo debe ser menor o igual que 45MB.");
                input.value = "";
            };
        }
        </script>
';
        // show($coordinacion);
        // $objetos = insertarinarray($objetos, $coordinacion, 2);



        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //


        return $this->guardar(0);
    }


    public function guardar($id)
    {

        try {
            $execution_time_limit = 500;
            set_time_limit($execution_time_limit);

            DB::beginTransaction();
            //code...
            $notificacion = new NotificacionesController;
            $input = Input::all();
            // dd($input);
            $ruta = $this->configuraciongeneral[1];

            if ($id == 0) {
                $ruta .= "/create";
                $guardar = new MantappPublicacionesModel();
                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro Variable de Configuración";
                $guardar->id_usuario = Auth::user()->id;
            } else {
                $ruta .= "/$id/edit";
                $guardar = MantappPublicacionesModel::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Edición Variable de Configuración";
            }
            
            $validator = Validator::make($input, MantappPublicacionesModel::rules($id));

            if ($validator->fails()) {
                return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
            } else {
                $guardar_deta = new MantappPublicacionesDetaModel();
                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token" && $key != "archivos" && $key != "videos") {
                        $guardar->$key = $value;
                        $guardar_deta->$key=$value;
                    }
                }
                $guardar->save();
                $idCab = $guardar->id;
                $guardar_deta->id_usuario = Auth::user()->id;
                $guardar_deta->id_cab_publicacion = $idCab;
                $guardar_deta->save();
                ////arechivos
                $archivos = Input::file('archivos');
                // dd($archivos);
                if (is_array($archivos)) {
                    foreach ($archivos as $key => $value) {
                        if (is_file($value)) {
                            $archivo = new MantappPublicacionesArchivosModel();
                            $dir = public_path() . '/publicaciones_manta_app';

                            if (!is_dir($dir)) {
                                mkdir($dir, 0777);
                            }
                            $dir = public_path() . '/publicaciones_manta_app/' . $idCab;
                            $archivo->id_publicacion = $idCab;

                            $name = 'Publicacion_' . $idCab . '_' . rand(1, 100000);
                            $resize_name = $name . '.' . $value->getClientOriginalExtension();
                            $value->move($dir, $resize_name);
                            $archivo->nombre = $resize_name;
                            $archivo->tipo = 'IMG';
                            $archivo->save();
                        }
                    }
                }
                ////arechivos
                $archivos = Input::file('videos');
                // dd($archivos);
                if (is_array($archivos)) {
                    foreach ($archivos as $key => $value) {
                        if (is_file($value)) {
                            $archivo = new MantappPublicacionesArchivosModel();
                            $dir = public_path() . '/publicaciones_manta_app';

                            if (!is_dir($dir)) {
                                mkdir($dir, 0777);
                            }
                            $dir = public_path() . '/publicaciones_manta_app/' . $idCab;
                            $archivo->id_publicacion = $idCab;

                            $name = 'Publicacion_' . $idCab . '_' . rand(1, 100000);
                            $resize_name = $name . '.' . $value->getClientOriginalExtension();
                            $value->move($dir, $resize_name);
                            $archivo->nombre = $resize_name;
                            $archivo->tipo = 'VIDEO';
                            $archivo->save();
                        }
                    }
                }


                DB::commit();


                // $url =  "https://portalciudadano.manta.gob.ec/playerid";
                // $client = new GuzzleHttpClient();
                try {
                    if ($id == 0) {

                        // $respuesta = $client->request('GET', $url);
                        // $consultawebserviceobs = json_decode($respuesta->getBody()->getContents(), true);
                        // dd($consultawebserviceobs);
                        $notificacion->notificacionmantapp(["276e28fc-25ad-459c-ae2b-cdd67d18c3b4"], $guardar->id, '🏢 Gad Manta 🔔', ' 📩 ' . $guardar->titulo,null, ['tipo' => 1, 'ruta' => '/publicidad']);
                        // $notificacion->notificacionmantapp($consultawebserviceobs, $guardar->id, '🏢 Gad Manta 🔔', ' 📩 '.$guardar->titulo, $guardar, ['tipo' => 1, 'ruta' => '/publicidad']);
                    }
                } catch (\Exception $e) {
                }
            }
            Session::flash('message', $msg);
            return Redirect::to($this->configuraciongeneral[1]);
        } catch (\Throwable $th) {
            DB::rollback();
            Session::flash('error', 'Ocurrio un problema...');
            return redirect()->back()->withErrors([$th->getMessage()])->withInput();
        }
    }



    public function publicacionesajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'created_at',
            2 => 'titulo',
            3 => 'name'
        );


        $query = MantappPublicacionesModel::select('man_tmov_publicaciones.*','u.name')->
        join('users as u','u.id','=','man_tmov_publicaciones.id_usuario')->where(['man_tmov_publicaciones.estado' => 'ACT']);

        $totalData = $query->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value')) && empty($request->input('campos'))) {
            $posts =  $query

                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts =   $query

                ->where(function ($query) use ($search) {
                    $query->where('man_tmov_publicaciones.id', 'LIKE', "%{$search}%")
                        ->orWhere('man_tmov_publicaciones.titulo', 'LIKE', "%{$search}%")
                        ->orWhere('man_tmov_publicaciones.descripcion', 'LIKE', "%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered =  $query
                ->where(function ($query) use ($search) {
                    $query->where('man_tmov_publicaciones.id', 'LIKE', "%{$search}%")
                        ->orWhere('man_tmov_publicaciones.titulo', 'LIKE', "%{$search}%")
                        ->orWhere('man_tmov_publicaciones.descripcion', 'LIKE', "%{$search}%");
                })
                ->count();
        }

        $data = array();
        // dd($posts);
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {

                $edit = '';
                $edit .= link_to_route('' . $this->configuraciongeneral[7] . '.edit', ' EDITAR', array($post->id), array('class' => 'fa fa-pencil-square-o btn btn-light', 'style' => 'color: #fff;background-color: #17a2b8;border-color: #17a2b8'));
                $edit .= '&nbsp;&nbsp;<a class="fa fa-trash btn btn-danger " onClick="eliminar(' . $post->id . ')"> ELIMINAR</a>
                <div style="display: none;">
                <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                    <input name="_token" type="hidden" value="' . csrf_token() . '">
                    <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                </form>
                </div>';
                // $edit = $edit  . "&nbsp;&nbsp;" . $reprogramar . $suspender;
                $acciones = $edit;
                $nestedData['id'] = $post->id;
                $nestedData['titulo'] = $post->titulo;
                $nestedData['name'] = $post->name;
                $nestedData['created_at'] = fechas(2, $post->created_at);
                $nestedData['acciones'] = $acciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $objetos = json_decode($this->objetos);
        $tabla = [];


        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $objetos = json_decode($this->objetos);
        $this->configuraciongeneral[2] = "editar";
        $tabla = MantappPublicacionesModel::find($id);

        $archivos = MantappPublicacionesArchivosModel::where('id_publicacion', $id)->get();
        $archivos_total = MantappPublicacionesArchivosModel::where('id_publicacion', $id)->count();
        $coordinacion = '{"Tipo":"htmlplantilla","Descripcion":"Imagenes","Nombre":"imagenes_html","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $html = '
        <div style="text-align: end;"><button type="button" class="btn btn-info" id="add_archivo">
        <i class="fa fa-plus-circle" style="font-size: 1.5em;"> </i> Agregar Item
        </button></div>
        <table class="table table-striped" style="text-align: center;" id="tabla_archivos">
            <thead >
                <tr>
                <th scope="col" style="text-align: center;">Archivo</th>
                <th scope="col" style="text-align: center;">Acción</th>
                </tr>
            </thead>
            <tbody>
            ';
        foreach ($archivos as $key => $value) {
            if ($value->tipo == 'IMG') {
                $html .= '<tr id="fila_ar' . ($key + 1) . '"><td><a  class="divpopup" target="_blank" onclick="popup(this)" href="' . URL::to('') . '/publicaciones_manta_app/' . $id . '/' . $value->nombre . '" > <img src="' . URL::to('') . '/publicaciones_manta_app/' . $id . '/' . $value->nombre . '" height="40" width="40"></td><td><i onclick=" borrar_bd(' . $value->id . ',' . ($key + 1) . ')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></td></tr>';
            }
        }
        $html .= '</tbody></table><script type="text/javascript">
        var n_filas_arr=' . $archivos_total . ';
        $("#add_archivo").click(function (e) { 
            $("#tabla_archivos tbody").prepend(\'<tr id="fila_ar\'+n_filas_arr+\'" ><td><input type="file" accept="image/*" onChange="validarPeso(this)" required class="form-control" name="archivos[]"></td><td><i onclick="borrar_archivo(\'+n_filas_arr+\')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
            n_filas_arr++;
        });
        function borrar_archivo(id){

            $("#fila_ar"+id).remove();
        }
        function borrar_bd(id,fila){
            var ruta_delete="' . URL::to('coordinacioncronograma/deleteMultimediapublicaciones') . '?id="+id+"&t=1&_token=' . csrf_token() . '";
            $.post(ruta_delete, function(data) {
                    if(data.tipo){
                        $("#fila_ar"+fila).remove();
                        toastr["success"](data.mensaje);
                    }else{
                        toastr["error"](data.mensaje);
                    }
            });
        }
        </script>';
        $coordinacion->Valor = $html;
        // show($coordinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 2);
        $coordinacion = '{"Tipo":"htmlplantilla","Descripcion":"Videos","Nombre":"videos_html","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $html = '
        <div style="text-align: end;"><button type="button" class="btn btn-success" id="add_videos">
        <i class="fa fa-plus-circle" style="font-size: 1.5em;"> </i> Agregar Item
        </button></div>
        <table class="table table-striped" style="text-align: center;" id="tabla_videos">
            <thead >
                <tr>
                <th scope="col" style="text-align: center;">Archivo</th>
                <th scope="col" style="text-align: center;">Acción</th>
                </tr>
            </thead>
            <tbody>
            ';
        foreach ($archivos as $key => $value) {
            if ($value->tipo == 'VIDEO') {
                $html .= '<tr id="fila_ar_videos' . ($key + 1) . '"><td><a  class="divpopup" target="_blank", onclick="popup(this)" href="' . URL::to('') . '/publicaciones_manta_app/' . $id . '/' . $value->nombre . '" > <video src="' . URL::to('') . '/publicaciones_manta_app/' . $id . '/' . $value->nombre . '" height="40" width="40"></td><td><i onclick=" borrar_bd_videos(' . $value->id . ',' . ($key + 1) . ')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></td></tr>';
            }
        }
        $html .= '</tbody></table><script type="text/javascript">
        var n_filas_arr_videos=' . $archivos_total . ';
        $("#add_videos").click(function (e) { 
            $("#tabla_videos tbody").prepend(\'<tr id="fila_ar_videos\'+n_filas_arr_videos+\'" ><td><input type="file" onChange="validarPeso(this)" accept="video/*" required class="form-control" name="videos[]"></td><td><i onclick="borrar_archivo_video(\'+n_filas_arr_videos+\')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
            n_filas_arr_videos++;
        });
        function borrar_archivo_video(id){

            $("#fila_ar_videos"+id).remove();
        }
        function borrar_bd_videos(id,fila){
            var ruta_delete="' . URL::to('coordinacioncronograma/deleteMultimediapublicaciones') . '?id="+id+"&t=1&_token=' . csrf_token() . '";
            $.post(ruta_delete, function(data) {
                    if(data.tipo){
                        $("#fila_ar_videos"+fila).remove();
                        toastr["success"](data.mensaje);
                    }else{
                        toastr["error"](data.mensaje);
                    }
            });
        }

        function validarPeso(input){
            console.log(input.files[0].size);
            if(input.files[0].size > 47185920){
                toastr["error"]("El peso del archivo debe ser menor o igual que 45MB.");
                input.value = "";
            };
        }
        </script>';
        $coordinacion->Valor = $html;
        // show($coordinacion);
        // $objetos = insertarinarray($objetos, $coordinacion, 2);

        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "tabla" => $tabla,
            "validarjs" => $this->validarjs
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $tabla = MantappPublicacionesModel::find($id);
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
    public function deleteMultimediapublicaciones()
    {
        try {
            //code...

            $id = Input::get('id');
            $tabla = MantappPublicacionesArchivosModel::find($id);
            $ruta = public_path() . '/publicaciones_manta_app/' . $tabla->id_publicacion . '/' . $tabla->nombre;
            if (file_exists($ruta)) {
                // dd($ruta);
                unlink($ruta);
            }
            $tabla->delete();
            return ['tipo' => true, 'mensaje' => 'Archivo eliminado'];
        } catch (\Throwable $th) {
            //throw $th;
            return ['tipo' => false, 'mensaje' => 'El archivo no pudo ser eliminado'];
        }
    }
}
