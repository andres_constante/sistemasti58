<?php

namespace Modules\Coordinacioncronograma\Http\Controllers;

use App\ArchivosModel;
use App\Http\Controllers\NotificacionesController;
use App\Http\Requests;
use App\SubDireccionModel;
use App\UsuariosModel;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\actividadesModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\CoordinacionCronograma\Entities\CoorComponenteModel;
use Modules\CoordinacionCronograma\Entities\CoorComponenteObjetivosModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionCabModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionDetaDireccionModel;
use Modules\CoordinacionCronograma\Entities\PoaActividadModel;
use Modules\CoordinacionCronograma\Entities\TipoActividadPoaModel;
use Modules\Coordinacioncronograma\Entities\CoorActividadesComponenteObjModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteProgramaModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaDetaActModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaDetaModel;
use Modules\Coordinacioncronograma\Entities\MovimientoCronogramaPersonasModel;
use Session;
use stdClass;

class CoorCronogramaCabController extends Controller
{
    var $configuraciongeneral = array(
        "Disposiciones generales",
        "coordinacioncronograma/cronogramacompromisos",
        "index",
        6 => "coordinacioncronograma/cronogramaajax",
        7 => "cronogramacompromisos"
    );
    var $escoja = array(null => "Escoja opción...");

    var $objetos = '[
        {"Tipo":"select","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tipo actividad","Nombre":"id_tipo_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"div","Descripcion":"Detalle Actividad POA","Nombre":"detalle_poa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Responsable","Nombre":"idusuario","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Explicación Actividad","Nombre":"observacion","Clase":"mostrarobservaciones","Valor":"Null","ValorAnterior" :"Null" } ,
        {"Tipo":"select","Descripcion":"Avance %","Nombre":"avance","Clase":"porcentaje","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Última Modificación","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Archivo Final","Nombre":"archivo_final","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" },     
        {"Tipo":"select","Descripcion":"Verificación","Nombre":"verificacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Observación de la verificación","Nombre":"observacion_verificacion","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" }     
        ]';

    var $objetosdetalle = '[
            {"Tipo":"text","Descripcion":"Responsable","Nombre":"responsable","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"text","Descripcion":"Correo","Nombre":"correo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
    var $botonesindex = '
        [
            {"Descripcion":"Mostrar","Ruta":"","Tipo":"Editar" },
            {"Descripcion":"Editar","Ruta":"","Tipo":"Mostrar" }
        ]
    ';
    /*COmponente Sostenibilidad*/
    var $objetoscomponentes = '
        [
            {"Tipo":"select","Descripcion":"Componente","Nombre":"componente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
            {"Tipo":"select","Descripcion":"Objetivos","Nombre":"objetivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Programa","Nombre":"programa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]
    ';
    var $columns = array(
        0 => 'id',
        1 => 'actividad',
        2 => 'tipo_actividad',
        3 => 'estado_actividad',
        4 => 'fecha_inicio',
        5 => 'fecha_fin',
        6 => 'direccion',
        7 => 'avance',
        8 => 'prioridad',
        9 => 'updated_at'
    );


    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "estado_actividad" => "estado_actividad: {
                            required: true
                        }",
        "actividad" => "actividad: {
                            required: true
                        }",

        "id_tipo_actividad" => "id_tipo_actividad: {
                            required: true
                        }",
        "fecha_inicio" => "fecha_inicio: {
                            required: true
                        }",
        "fecha_fin" => "fecha_fin: {
                            required: true
                        }",
        "idusuario" => "idusuario: {
                            required: true
                        }",


        "observacion" => "observacion: {
                            required: false
                        }"
    );


    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }
    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        }

        return $object;
    }
    public function traspasoactividad()
    {
        $tabla = CoorCronogramaCabModel::all();
        $pro = 0;
        $nopro = 0;
        foreach ($tabla as $key => $value) {
            # code...
            $guardar = CoorCronogramaDetaModel::where("id_crono_cab", $value->id)->first();
            if (!$guardar) {
                $timeline = new CoorCronogramaDetaModel;
                $timeline->estado_actividad = $value->estado_actividad;
                $timeline->actividad = $value->actividad;
                $timeline->fecha_inicio = $value->fecha_inicio;
                $timeline->fecha_fin = $value->fecha_fin;
                $timeline->observacion = $value->observacion;
                $timeline->id_usuario = $value->id_usuario;
                //$timeline->created_at=$value->created_at;
                //$timeline->updated_at=$value->updated_at;
                $timeline->ip = $value->ip;
                $timeline->pc = $value->pc;
                $timeline->estado = "INA";
                $timeline->id_direccion = $value->id_direccion;
                $timeline->avance = $value->avance;
                $timeline->id_crono_cab = $value->id;
                $personas = MovimientoCronogramaPersonasModel::where("id_crono_cab", $value->id)->get();
                $json = [];
                foreach ($personas as $key => $value) {
                    # code...
                    $json[] = $value->id_usuario;
                }
                $timeline->personas_json = json_encode($json);
                $timeline->save();
                $pro++;
            } else
                $nopro++;
        }
        return array("PROCESADOS" => $pro, "NO PROCESADOS" => $nopro);
    }

    public function quitarcampos($array, $idacti)
    {
        $tbcamposocultos = actividadesModel::find($idacti);
        if ($tbcamposocultos) {
            $camposocultos = explode("|", $tbcamposocultos->camposocultos);
            if (!count($array))
                return $camposocultos;
            foreach ($camposocultos as $key => $value) {
                # code...
                $valor = str_replace("div-", "", $value);
                unset($array[$valor]);
            }
        }
        return $array;
    }

    public function verificarPermiso()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        $id_usuario = Auth::user()->id;
        switch ($id_tipo_pefil->tipo) {
            case 1:

                $tabla = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                    ->select("coor_tmov_cronograma_cab.*", "a.direccion")
                    ->where("coor_tmov_cronograma_cab.estado", "ACT")
                    ->get();
                $create = 'si';
                $delete = 'si';
                break;
            case 2:

                $tabla = CoorCronogramaCabModel::join("coor_tmov_cropnograma_resposables as cr", "cr.id_crono_cab", "=", "coor_tmov_cronograma_cab.id")
                    ->join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                    ->select("coor_tmov_cronograma_cab.*", "a.direccion")
                    ->where([["coor_tmov_cronograma_cab.estado", "ACT"], ["cr.id_usuario", $id_usuario]])
                    ->get();

                //show($tabla);
                $create = 'no';
                $delete = 'no';
                break;
            case 3:
                $tabla = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                    ->join("users as u", "u.id_direccion", "=", "a.id")
                    ->select("coor_tmov_cronograma_cab.*", "a.direccion")
                    ->where([["coor_tmov_cronograma_cab.estado", "ACT"], ["u.id", $id_usuario]])
                    ->get();
                $create = 'no';
                $delete = 'no';
                break;
            case 4:
                $tabla = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                    ->select("coor_tmov_cronograma_cab.*", "a.direccion")
                    ->where("coor_tmov_cronograma_cab.estado", "ACT")
                    ->get();
                $create = 'si';
                $delete = 'si';
                break;
            case 10:
                $direciones = getDireccionesCoor(Auth::user()->id);
                // dd($direciones);
                $tabla = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
                    ->select("coor_tmov_cronograma_cab.*", "a.direccion")
                    ->where("coor_tmov_cronograma_cab.estado", "ACT")
                    ->get();
                $create = 'si';
                $delete = 'si';
                break;
        }
    }

    public function cronogramaajax(Request $request, $filtro = [], $columns = [], $rutaaccion = "")
    {

        if (!count($columns))
            $columns = $this->columns;
        if ($rutaaccion == "")
            $rutaaccion = $this->configuraciongeneral[7];
        $tablamain = CoorCronogramaCabModel::where("coor_tmov_cronograma_cab.estado", "ACT");
        $composentesql = "SELECT a.nombre AS componente from coor_tmae_cronograma_componente_cab a INNER JOIN coor_tmae_cronograma_componente_objetivo b on a.id=b.id_componente
            INNER JOIN coor_tmov_actividades_componente c ON c.id_objetivo_componente=b.id
            WHERE c.id_crono_cab=coor_tmov_cronograma_cab.id";
        $objetivosql = "SELECT b.objetivo from coor_tmae_cronograma_componente_cab a INNER JOIN coor_tmae_cronograma_componente_objetivo b on a.id=b.id_componente
            INNER JOIN coor_tmov_actividades_componente c ON c.id_objetivo_componente=b.id
            WHERE c.id_crono_cab=coor_tmov_cronograma_cab.id";
        $programasql = str_replace("b.objetivo", "b.programa", $objetivosql);
        //$tablamain = $tablamain->select(DB::raw("ifnull(($composentesql),'') as componente,ifnull(($objetivosql),'') as objetivo,ifnull(($programasql),'') as programa"));
        //show($tablamain->get()->toarray());
        /*============================*/
        $coordinacion = "SELECT nombre from coor_tmae_coordinacion_main AS a INNER JOIN coor_tmov_coordinacion_direccion_deta AS b
	                    WHERE a.id=b.id_coor_cab AND b.id_direccion=coor_tmov_cronograma_cab.id_direccion
            ";
        /*============================*/
        $filraw = DB::raw("ifnull(($composentesql),'') as componente,ifnull(($objetivosql),'') as objetivo,ifnull(($programasql),'') as programa,ifnull(($coordinacion),'') as coordinacion");
        //show($filraw);
        if (count($filtro)) {
            if (array_key_exists("actividades", $filtro)) {
                $tablamain = $tablamain->where(function ($query) use ($filtro) {
                    $query->whereIn("coor_tmov_cronograma_cab.id", $filtro["actividades"]);
                });
            } else
                $tablamain = $tablamain->where($filtro);
        }
        $tablamainsub = $tablamain->join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
            ->join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad");
        //show($tablamainsub->get());
        /*Componente, Objetivo, Programa*/



        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $id_usuario = Auth::user()->id;
        if (empty($request->input('search.value'))) {
            switch ($id_tipo_pefil->tipo) {
                case 1:
                    $posts = $tablamainsub
                        ->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw);
                    break;
                case 6:
                case 2:
                    $posts = $tablamainsub
                        ->join("coor_tmov_cropnograma_resposables as cr", "cr.id_crono_cab", "=", "coor_tmov_cronograma_cab.id")
                        ->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw)
                        ->where("cr.id_usuario", $id_usuario);
                    break;

                case 3:
                    $posts = $tablamainsub
                        ->join("users as u", "u.id_direccion", "=", "a.id")
                        ->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw)
                        ->where("u.id", $id_usuario);

                    break;

                case 4:
                    //return $limit;                    
                    $posts = $tablamainsub->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw);
                    /*if($limit>=0)
                                $posts =$posts ->offset($start);
                            $posts =$posts ->limit($limit)
                            ->orderBy($order,$dir);
                            */
                    break;
                case 5:
                    $posts = $tablamainsub->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw);
                    break;

                case 10:
                    $direcciones = getDireccionesCoor(Auth::user()->id);
                    //show($direcciones);
                    $posts = $tablamainsub->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw)
                        ->whereIn("a.id", explode(",", $direcciones->id_direccion . "," . Auth::user()->id_direccion));
                    //show($posts->get());
                    break;
            }
        } else {
            $search = $request->input('search.value');
            switch ($id_tipo_pefil->tipo) {
                case 1:
                    $posts = $tablamainsub
                        ->select("coor_tmov_cronograma_cab.*", "a.direccion", $filraw)
                        ->where(function ($query) use ($search) {
                            $query->where('coor_tmov_cronograma_cab.id', 'LIKE', "%{$search}%")
                                ->orWhere('coor_tmov_cronograma_cab.estado_actividad', 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("coor_tmov_cronograma_cab.actividad"), 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("a.direccion"), 'LIKE', "%{$search}%");
                        });

                    break;
                case 6:
                case 2:
                    $posts = $tablamainsub
                        //->join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
                        ->join("coor_tmov_cropnograma_resposables as cr", "cr.id_crono_cab", "=", "coor_tmov_cronograma_cab.id")
                        ->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw)
                        ->where("cr.id_usuario", $id_usuario)
                        ->where(function ($query) use ($search) {
                            $query->where('coor_tmov_cronograma_cab.id', 'LIKE', "%{$search}%")
                                ->orWhere('coor_tmov_cronograma_cab.estado_actividad', 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("coor_tmov_cronograma_cab.actividad"), 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("a.direccion"), 'LIKE', "%{$search}%");
                        });
                    break;

                case 3:
                    $posts = $tablamainsub
                        //->join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
                        ->join("users as u", "u.id_direccion", "=", "a.id")
                        ->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw)
                        ->where("u.id", $id_usuario)
                        ->where(function ($query) use ($search) {
                            $query->where('coor_tmov_cronograma_cab.id', 'LIKE', "%{$search}%")
                                ->orWhere('coor_tmov_cronograma_cab.estado_actividad', 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("coor_tmov_cronograma_cab.actividad"), 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("a.direccion"), 'LIKE', "%{$search}%");
                        });
                    break;

                case 4:

                    $posts = $tablamainsub
                        //->join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
                        ->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw)
                        ->where(function ($query) use ($search) {
                            $query->where('coor_tmov_cronograma_cab.id', 'LIKE', "%{$search}%")
                                ->orWhere('coor_tmov_cronograma_cab.estado_actividad', 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("coor_tmov_cronograma_cab.actividad"), 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("a.direccion"), 'LIKE', "%{$search}%");
                        });
                    break;

                case 5:

                    $posts = $tablamainsub
                        //->join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
                        ->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw)
                        ->where(function ($query) use ($search) {
                            $query->where('coor_tmov_cronograma_cab.id', 'LIKE', "%{$search}%")
                                ->orWhere('coor_tmov_cronograma_cab.estado_actividad', 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("coor_tmov_cronograma_cab.actividad"), 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("a.direccion"), 'LIKE', "%{$search}%");
                        });

                    break;
                case 10:
                    $direcciones = getDireccionesCoor(Auth::user()->id);
                    $posts = $tablamainsub
                        //->join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
                        ->select("coor_tmov_cronograma_cab.*", "a.direccion", "tipo.tipo_actividad", $filraw)
                        ->where(function ($query) use ($search) {
                            $query->where('coor_tmov_cronograma_cab.id', 'LIKE', "%{$search}%")
                                ->orWhere('coor_tmov_cronograma_cab.estado_actividad', 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("coor_tmov_cronograma_cab.actividad"), 'LIKE', "%{$search}%")
                                ->orWhere(DB::raw("a.direccion"), 'LIKE', "%{$search}%");
                        })
                        ->whereIn("a.id", explode(",", $direcciones->id_direccion . "," . Auth::user()->id_direccion));

                    break;
            }
        }
        /**/
        $totalData = $tablamain->count();

        // $totalFiltered = $totalData;
        $totalFiltered = $posts->count();
        $posts = $posts->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
        //show($posts);
        $data = array();
        if (!empty($posts)) {
            // dd($posts);
            foreach ($posts as $post) {
                switch ($id_tipo_pefil->tipo) {
                    case 1:
                        $show = link_to_route(str_replace("/", ".", $rutaaccion) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                        $edit = link_to_route(str_replace("/", ".", $rutaaccion) . '.edit', '', array($post->id, "menu" => "no"), array('class' => 'fa fa-pencil-square-o divpopup', 'onclick' => 'popup(this)'));
                        $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                        <div style="display: none;">
                        <form method="POST" action="' . $rutaaccion . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                            <input name="_token" type="hidden" value="' . csrf_token() . '">
                            <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                        </form>
                        </div>';
                        $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                        break;
                    case 6:
                    case 2:
                        $show = link_to_route(str_replace("/", ".", $rutaaccion) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                        $edit = link_to_route(str_replace("/", ".", $rutaaccion) . '.edit', '', array($post->id, "menu" => "no"), array('class' => 'fa fa-pencil-square-o divpopup', 'onclick' => 'popup(this)'));
                        $aciones = "$show&nbsp;&nbsp;$edit";
                        break;
                    case 3:
                        /*$datosFinalizacon = explode("|", ConfigSystem("quitareditar"));
                        // show($datosFinalizacon);
                        $fechabd = $datosFinalizacon[0];
                        $horabd = $datosFinalizacon[1];;

                        $fechaactual = date("D");
                        $horaactual = date("H");
                        if ($fechaactual == $fechabd && intval($horabd) < intval($horaactual)) {
                            $aciones = '' . link_to_route(str_replace("/", ".", $rutaaccion) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                        } else {
                            */
                        $show = link_to_route(str_replace("/", ".", $rutaaccion) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                        $edit = link_to_route(str_replace("/", ".", $rutaaccion) . '.edit', '', array($post->id, "menu" => "no"), array('class' => 'fa fa-pencil-square-o divpopup', 'onclick' => 'popup(this)'));
                        //}
                        $aciones = "$show&nbsp;&nbsp;$edit";
                        break;
                    case 4:
                        /*Agregar Un Style a Icono FontAwesome para Semáforo*/
                        $estiloimg = '<img src="' . asset("img/semaforo/azul.png") . '" style="width: 18px; height: auto;"/>';
                        $estilo = 'style="color: #1565C0;  font-size: larger;"';
                        $fechainisem = strtotime($post->fecha_fin); //strtotime($post->fecha_inicio);
                        $fechafinsem = strtotime(date("Y-m-d")); //strtotime($post->fecha_fin);
                        //$fechaactsem=strtotime(date("Y-m-d"));
                        $dif = $fechainisem - $fechafinsem;
                        if ($dif < 0 && intval($post->avance < 100)) {
                            $estilo = 'style=" color: #ff0808; font-size: larger;"';
                            $estiloimg = '<img src="' . asset("img/semaforo/rojo.png") . '" style="width: 18px; height: auto;"/>';
                        }
                        if (date("d", $dif) <= 3) {
                            $estilo = 'style=" color: #FFEB3B; font-size: larger;"';
                            $estiloimg = '<img src="' . asset("img/semaforo/amarillo.png") . '" style="width: 18px; height: auto;"/>';
                        }
                        if ($post->avance == 100) {
                            $estilo = 'style=" color: #71bf1d; font-size: larger;"';
                            $estiloimg = '<img src="' . asset("img/semaforo/verde.png") . '" style="width: 18px; height: auto;"/>';
                        }
                        //$dif=date("d",$dif);
                        //show(array($post->id,$post->fecha_inicio,$post->fecha_fin,$dif,date("Y-m-d",$fechainisem),date("Y-m-d",$fechafinsem),$estilo));

                        $show = link_to_route(str_replace("/", ".", $rutaaccion) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                        $edit = link_to_route(str_replace("/", ".", $rutaaccion) . '.edit', '', array($post->id, "menu" => "no"), array('class' => 'fa fa-pencil-square-o divpopup', 'onclick' => 'popup(this)'));
                        $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                        <div style="display: none;">
                        <form method="POST" action="' . $rutaaccion . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                            <input name="_token" type="hidden" value="' . csrf_token() . '">
                            <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                        </form>
                        </div><i>' . $estiloimg . '</i>';
                        $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                        //</div><i class="fa fa-circle" '.$estilo.'></i><span>'.$estiloimg.'</span>';

                        break;

                    case 5:
                        $show = link_to_route(str_replace("/", ".", $rutaaccion) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                        $aciones = "$show";
                        break;

                    case 10:
                        /*Agregar Un Style a Icono FontAwesome para Semáforo*/
                        $estiloimg = '<img src="' . asset("img/semaforo/azul.png") . '" style="width: 18px; height: auto;"/>';
                        $estilo = 'style="color: #1565C0;  font-size: larger;"';
                        $fechainisem = strtotime($post->fecha_fin); //strtotime($post->fecha_inicio);
                        $fechafinsem = strtotime(date("Y-m-d")); //strtotime($post->fecha_fin);
                        //$fechaactsem=strtotime(date("Y-m-d"));
                        $dif = $fechainisem - $fechafinsem;
                        if ($dif < 0 && intval($post->avance < 100)) {
                            $estilo = 'style=" color: #ff0808; font-size: larger;"';
                            $estiloimg = '<img src="' . asset("img/semaforo/rojo.png") . '" style="width: 18px; height: auto;"/>';
                        }
                        if (date("d", $dif) <= 3) {
                            $estilo = 'style=" color: #FFEB3B; font-size: larger;"';
                            $estiloimg = '<img src="' . asset("img/semaforo/amarillo.png") . '" style="width: 18px; height: auto;"/>';
                        }
                        if ($post->avance == 100) {
                            $estilo = 'style=" color: #71bf1d; font-size: larger;"';
                            $estiloimg = '<img src="' . asset("img/semaforo/verde.png") . '" style="width: 18px; height: auto;"/>';
                        }
                        $show = link_to_route(str_replace("/", ".", $rutaaccion) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;' .
                            '</div><i>' . $estiloimg . '</i>';
                        $aciones = "$show";

                        break;
                }

                $nestedData['id'] = $post->id;
                $nestedData['componente'] = $post->componente;
                $nestedData['objetivo'] = $post->objetivo;
                $nestedData['programa'] = $post->programa;
                $nestedData['actividad'] = $post->actividad;
                $nestedData['tipo_actividad'] = $post->tipo_actividad;
                $nestedData['estado_actividad'] = $post->estado_actividad;
                $nestedData['fecha_inicio'] = $post->fecha_inicio;
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['direccion'] = $post->direccion;
                $nestedData['observacion'] = $post->observacion;
                $nestedData['avance'] = $post->avance;
                $nestedData['prioridad'] = $post->prioridad;
                $nestedData['updated_at'] = date("Y-m-d H:i:s", strtotime($post->updated_at));
                $nestedData['acciones'] = $aciones;
                $nestedData['coordinacion'] = $post->coordinacion;
                $nestedData['verificacion'] = $post->verificacion;
                $nestedData['observacion_verificacion'] = $post->observacion_verificacion;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    public function index($rutaajax = "", $objetosadmin = [], $configuraciongeneraladmin = [])
    {
        $objetos = json_decode($this->objetos);
        //unset($objetos[7]);
        //show($objetos);        
        $objetos[6]->Nombre = "direccion";
        $objetos[2]->Nombre = "tipo_actividad";
        unset($objetos[3]);
        //$objetos[4]->Value=$this->escoja +$seleccionar_direccion;

        $objetos = array_values($objetos);
        //show($objetos);


        // show($fechaactual);
        $create = '';
        $delete = '';
        $edit = null;
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        $id_usuario = Auth::user()->id;
        switch ($id_tipo_pefil->tipo) {
            case 1:

                $create = 'si';
                $delete = 'si';
                break;
            case 2:
                $create = 'no';
                $delete = 'no';
                break;
            case 3:
                $create = 'no';
                $delete = 'no';

                break;
            case 4:
                $create = 'si';
                $delete = 'si';
                break;
            case 5:
                $create = 'no';
                $delete = 'no';
                $edit = 'no';
                break;
            case 10:
                $create = 'no';
                $delete = 'no';
                $edit = 'no';
                break;
        }
        unset($objetos[6]);
        unset($objetos[11]);
        // unset($objetos[13]);
        /**/
        $js = '[
        {"Tipo":"funcionjs","Descripcion":"scriptjs","Nombre":"scriptjs","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';
        /**/
        if ($rutaajax != "")
            $this->configuraciongeneral[6] = $rutaajax;
        $tabla = [];
        if (count($objetosadmin))
            $objetos = $objetosadmin;
        $configuraciongeneral = $this->configuraciongeneral;
        if (count($configuraciongeneraladmin))
            $configuraciongeneral = $configuraciongeneraladmin;
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos);
        /*====================================*/
        /*====================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 8);
        //show($objetos);
        /*====================================*/
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => $create,
            "create" => $delete,
            "edit" => $edit
        ]);
    }


    public function create($objetosadmin = [], $configuraciongeneraladmin = [])
    {
        //    
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $this->configuraciongeneral[2] = "crear";
        /*Componentes*/
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $com = CoorComponenteModel::where("estado", "ACT")->select(DB::raw("concat(nombre,' - ',detalle) as componente"), "id")
        ->orderBy('nombre','ASC')->pluck("componente", "id")->all();
        $objetoscomponentes[0]->Valor = $this->escoja + $com;
        $objetoscomponentes[1]->Valor = $this->escoja;
        $objetoscomponentes[2]->Valor = $this->escoja;
        //show($objetoscomponentes);
        /**/
        $objetos = json_decode($this->objetos);

        //$seleccionar_personas_cargo=UsuariosModel::where("estado", "ACT")->pluck("name","id")->all();
        $seleccionar_direccion = direccionesModel::where("estado", "ACT")->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        $seleccionar_tipo = TipoActividadPoaModel::where("estado", "ACT")->pluck("tipo_actividad", "id")->all();
        $objetos[6]->Valor = $this->escoja + $seleccionar_direccion;
        $objetos[7]->Valor = $this->escoja;
        $objetos[2]->Valor = $this->escoja + $seleccionar_tipo;
        //Estado
        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        unset($estadoobras['EJECUCION_VENCIDO']);
        unset($estadoobras['EJECUTADO']);
        unset($estadoobras['SUSPENDIDO']);
        $prioridad = explodewords(ConfigSystem("prioridad"), "|");
        $Avance = explodewords(ConfigSystem("avance"), "|");
        $tipopoa = explodewords(ConfigSystem("tipopoa"), "|");
        // show($objetos);
        $verificacion = explodewords(ConfigSystem("verificacion"), "|");
        $objetos[13]->Valor = $this->escoja + $verificacion;
        if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4) {
            unset($objetos[13]);
            unset($objetos[14]);
        }

        // $objetos[13]->ValorAnterior = $tabla->verificacion;  

        $objetos[0]->Valor = $this->escoja + $estadoobras;
        $objetos[9]->Valor = $this->escoja + $Avance;
        $objetos[10]->Valor = $prioridad;
        $objetos[12]->Valor = $this->escoja + $prioridad;


        unset($objetos[11]);
        unset($objetos[12]);
        if (count($objetosadmin))
            $objetos = $objetosadmin;
        $configuraciongeneral = $this->configuraciongeneral;
        if (count($configuraciongeneraladmin))
            $configuraciongeneral = $configuraciongeneraladmin;
        $objetos = array_merge($objetoscomponentes, $objetos);
        /*====================================================*/
        /*====================================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"id_coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $cmb = CoordinacionCabModel::join("users as a", "a.id", "=", "coor_tmae_coordinacion_main.id_responsable")
            //->select("coor_tmae_coordinacion_main.*","a.name as responsable")
            ->select("coor_tmae_coordinacion_main.id", DB::raw("concat(coor_tmae_coordinacion_main.nombre,': ',a.name) as nombre"))
            ->where("coor_tmae_coordinacion_main.estado", "ACT")->pluck("nombre", "id")->all();

        $coordinacion->Valor = $this->escoja + $cmb;
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 9);
        /*====================================================*/

        /*====================================================*/
        /*====================================================*/
        //subdirecion
        $coordinacion = '{"Tipo":"select","Descripcion":"Sub dirección","Nombre":"id_sub_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);

        $coordinacion->Valor = [];
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 11);
        /*====================================================*/
        unset($objetos[14]);
        unset($objetos[16]);
        unset($objetos[17]);
        // show($objetos);
        return view('vistas.create', [
            "objetos" => $objetos,
            "edit" => null,
            "tipopoa" => $tipopoa,
            "configuraciongeneral" => $configuraciongeneral,
            "validarjs" => $this->validarjs,
            "permisos" => $this->perfil() //Add 01/10/2019
        ]);
    }

    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }


    public function guardar($id, $objetosadmin = [], $configuraciongeneraladmin = [], $validararray = [], $inputadd = [])
    {
        //show($id);
        $input = Input::all() + $inputadd;
        // show($input);
        $configuraciongeneral = $this->configuraciongeneral;
        if (Input::has("menu")) {
            $configuraciongeneral[1] .= "?menu=no";
            unset($input["menu"]);
        }

        if (count($configuraciongeneraladmin))
            $configuraciongeneral = $configuraciongeneraladmin;
        $ruta = $configuraciongeneral[1];
        /*Validacion de No Ingreso para Directores*/
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if (permisoingresoactividades() && ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3)) {
            Session::flash('message', 'No está permitido realizar cambios por el momento, disculpe las molestias.');
            return Redirect::to($this->configuraciongeneral[1]);
        }
        /**/
        DB::beginTransaction();
        try {
            if ($id == 0) {
                $ruta .= "/create";
                $guardar = new CoorCronogramaCabModel;
                $guardarPoa = new CoorCronogramaCabModel;

                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro de Comunicación";
            } else {
                $ruta .= "/$id/edit";
                $guardar = CoorCronogramaCabModel::find($id);
                $guardarPoa = CoorCronogramaCabModel::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Edición Comunicación";
            }
            //$input = Input::all();
            $arrapas = array();
            $validararry = CoorCronogramaCabModel::rules($id);
            if (count($validararray))
                $validararry = $validararray;
            //show($validararry,0);                
            //$validararry=$this->quitarcampos($validararry,Input::get("tema_principal"));
            //$input=$this->quitarcampos($input,Input::get("tema_principal"));                
            //show($validararry,0);
            //show($input);

            //$validator = Validator::make($input);
            /*Componente de Sostenibilidad*/
            $componenteval = [];
            if (Input::has("componente")) {
                $componenteval = array(
                    "componente" => "required",
                    "objetivo" => "required",
                    "programa" => "required"
                );
                if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
                    $veri = CoorActividadesComponenteObjModel::where("id_crono_cab", $id)->first();
                    if (!$veri) {
                        $componenteval = [];
                        unset($input["componente"]);
                        unset($input["objetivo"]);
                        unset($input["programa"]);
                    }
                }
            }
            /**/
            //show($input);
            $validator = Validator::make($input, $validararry + $componenteval);
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            } else {
                foreach ($input as $key => $value) {

                    if ($key != "_method" && $key != "_token" && $key != "idusuario" && $key != "financiamiento" && $key != "costo_prov" && $key != "costo_institucion" && $key != "costo_inicial" && $key != "costo_actual" && $key != "diferencia" && $key != "componente" && $key != "objetivo" && $key != "programa" && $key != "id_coordinacion") {

                        if ($key == 'archivo_final') {
                            $dir = public_path() . '/archivos_sistema/';
                            $docs = Input::file("archivo_final");
                            if ($docs) {
                                $fileName = "Archivo_Final-tramite-$id" . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                                $ext = $docs->getClientOriginalExtension();
                                $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                                if (!in_array($ext, $perfiles)) {
                                    DB::rollback();
                                    $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                                    return redirect()->back()->withErrors([$mensaje])->withInput();
                                }


                                //shoW();
                                $oldfile = $dir . '' . $fileName;
                                if (is_file($oldfile)) {
                                    if ($id_tipo_pefil->tipo != 1) {
                                        if (file_exists($oldfile))
                                            unlink($oldfile);
                                    } else {
                                        DB::rollback();
                                        $mensaje = "Ya se encuentra el archivo final registrado";
                                        return redirect()->back()->withErrors([$mensaje])->withInput();
                                    }
                                }
                                $docs->move($dir, $fileName);
                                $guardar->$key = $fileName;
                            }
                        } elseif ($key == 'id_sub_direccion') {
                            if (array_key_exists('id_direccion', $input)) {
                                if ($input['id_direccion'] == 27) {
                                    $guardar->id_direccion = $value;
                                }

                                //$sub=SubDireccionModel::where('id_direccion',$value);

                                // $timeline->id_direccion = $value;
                                // dd('aqui');
                            }
                        } else {
                            $guardar->$key = $value;

                            if ($key == "id_tipo_actividad")
                                $guardar->$key = Input::get('id_tipo_actividad'); //3;

                        }
                    }
                }
                $guardar->avance = intval(Input::get('avance'));
                if (Auth::user()->id == 1182  && date('w') == 5) {
                    $guardar->updated_at = date('Y-m-d') . ' 11:' . date('i:s');
                    // dd($guardar);
                }
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                $guardar->save();
                $idcab = $guardar->id;
                /*Componente de Sostenibilidad*/
                if (Input::has("componente")) {
                    if (array_key_exists("componente", $input)) {
                        $guacticom = CoorActividadesComponenteObjModel::where("id_crono_cab", $idcab)->first();
                        //show($guacticom);
                        if (!$guacticom)
                            $guacticom = new CoorActividadesComponenteObjModel;
                        $guacticom->id_crono_cab = $idcab;
                        $guacticom->id_objetivo_componente = Input::get("objetivo");
                        //$guacticom->id_programa_componente = Input::get("programa");
                        $guacticom->id_usuario = Auth::user()->id;
                        $guacticom->ip = \Request::getClientIp();
                        $guacticom->pc = \Request::getHost();
                        $guacticom->save();
                    }
                    //show("1");
                }
                /**/
                if ($guardar->avance == 100) {
                    $estadoobras = explode("|", ConfigSystem("estadoobras"));
                    $actualizar_estado = CoorCronogramaCabModel::find($idcab);
                    $actualizar_estado->estado_actividad = $estadoobras[1];
                    $actualizar_estado->save();
                }
                //show($input['financiamiento']);
                if ($guardar->id_tipo_actividad == 2) {
                    $validarPoa = PoaActividadModel::rules(0);
                    $detallepoa = ["id_cab_pro" => $idcab, "financiamiento" => $input['financiamiento'], "costo_institucion" => $input['costo_institucion'], "costo_prov" => $input['costo_prov'], "costo_inicial" => $input['costo_inicial'], "costo_actual" => $input['costo_actual'], "diferencia" => $input['diferencia']];
                    $validatorPoa = Validator::make($detallepoa, $validarPoa);
                    //$validator = Validator::make($input);

                    if ($validatorPoa->fails() && $guardar->id_tipo_actividad == 2) {
                        //die($ruta);
                        DB::rollback();
                        return Redirect::to("$ruta")
                            ->withErrors($validatorPoa)
                            ->withInput();
                    }
                    $tabla3 = PoaActividadModel::where("id_cab_pro", $idcab)->get();
                    foreach ($tabla3 as $key1 => $value) {
                        # code...
                        $dele = PoaActividadModel::find($value->id);
                        $dele->estado = 'INA';
                        $dele->save();
                    }

                    $guardardeta = new PoaActividadModel;
                    $guardardeta->financiamiento = $input['financiamiento'];
                    $guardardeta->costo_institucion = $input['costo_institucion'];
                    $guardardeta->costo_institucion = $input['costo_institucion'];
                    $guardardeta->costo_prov = $input['costo_prov'];
                    $guardardeta->costo_inicial = $input['costo_inicial'];
                    $guardardeta->costo_actual = $input['costo_actual'];
                    $guardardeta->diferencia = $input['diferencia'];
                    $guardardeta->id_cab_pro = $idcab;
                    $guardardeta->save();
                }


                //
                $timeline = new CoorCronogramaDetaModel;
                if ($guardar->avance == 100) {
                    $estadoobras = explode("|", ConfigSystem("estadoobras"));
                    $timeline->estado_actividad = $estadoobras[1];
                } else {
                    $timeline->estado_actividad = $guardar->estado_actividad;
                }
                $timeline->actividad = $guardar->actividad;
                $timeline->fecha_inicio = $guardar->fecha_inicio;
                $timeline->fecha_fin = $guardar->fecha_fin;
                $timeline->fecha_fin = $guardar->fecha_fin;
                $timeline->observacion = $guardar->observacion;
                $timeline->id_usuario = $guardar->id_usuario;
                if (Auth::user()->id == 1182  && date('w') == 5) {
                    $timeline->created_at = $guardar->updated_at;
                    $timeline->updated_at = $guardar->updated_at;
                }

                $timeline->ip = \Request::getClientIp();
                $timeline->pc = \Request::getHost();
                $timeline->id_direccion = $guardar->id_direccion;

                $timeline->id_direccion = $guardar->id_direccion;
                $timeline->avance = $guardar->avance;
                $timeline->prioridad = $guardar->prioridad;
                $timeline->id_crono_cab = $idcab;

                $timeline->verificacion = $guardar->verificacion;
                $timeline->observacion_verificacion = $guardar->observacion_verificacion;
                //show($emails);


                //////////////////////para enviar alerta cuando se completa la actividad ////////////////
                if ($guardar->avance == 100) {
                    $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
                    if ($id_tipo_pefil->tipo != 4) {
                        $emails = explode("|", ConfigSystem("correoscoordinacion"));

                        $usuario = UsuariosModel::where("id", $guardar->id_usuario)->first();
                        $direccion = direccionesModel::where("id", $usuario->id_direccion)->first();
                        if ($direccion) {
                            $this->notificacion->EnviarEmail($emails, "Actividad completada", "<b>" . $usuario->name . "</b> de la dirección de " . $direccion->direccion . " completó la actividad <b>#</b>" . $guardar->id . "", "Esta actividad tiene un porcentaje de avance del 100%. <br><br> <b>Observación Registrada:</b> " . $guardar->observacion, $this->configuraciongeneral[1] . '/' . $guardar->id . '/edit', "vistas.emails.email");

                            for ($i = 0; $i < count($emails); $i++) {
                                $user = UsuariosModel::where("email", $emails[$i])->first();
                                if ($user) {
                                    $this->notificacion->notificacionesweb("<b>" . $usuario->name . "</b> de la dirección de " . $direccion->direccion . " completó la actividad <b>#</b>" . $guardar->id . "", $this->configuraciongeneral[1] . '/' . $guardar->id . '/edit', $user->id, "2c438f");
                                }
                            }
                        }
                    } //Fin IF

                }
                $actividad = $guardar->actividad;


                ///unset($guardar);


                /*Detalle Personas a Cargo / Fiscalizadores*/
                if (Input::has("idusuario")) {
                    $multiple = Input::get("idusuario");
                    $json = [];
                    $guardarres = MovimientoCronogramaPersonasModel::where("id_crono_cab", $idcab)
                        ->delete();
                    foreach ($multiple as $key => $value) {
                        # code...
                        //Responsables
                        $guardarres = MovimientoCronogramaPersonasModel::where("id_usuario", $value)
                            ->where("id_crono_cab", $idcab)
                            ->first();
                        if (!$guardarres)
                            $guardarres = new MovimientoCronogramaPersonasModel;
                        $guardarres->id_usuario = $value;
                        $guardarres->id_crono_cab = $idcab;
                        $guardarres->save();
                        $json[] = $value;
                        //
                        $usuario = UsuariosModel::select("users.*")
                            ->where("id", $guardarres->id_usuario)
                            ->first();
                        //show($actividad);
                        $this->notificacion->notificacionesweb("Se le ha asignado una nueva actividad <b>#</b>" . $idcab . "", $configuraciongeneral[1] . '/' . $idcab . '/edit', $usuario->id, "2c438f");
                        $this->notificacion->EnviarEmail($usuario->email, "Nueva Actividad", "Se le ha asignado una nueva actividad", "<b>ACTIVIDAD: <b>#</b>" . $idcab . "</b> <br>" . $actividad, $configuraciongeneral[1] . '/' . $guardar->id . '/edit');
                    }


                    //$json[] = $value;
                }

                if ($guardar->verificacion == 'VERIFICADO CON OBSERVACIONES'&& $id_tipo_pefil->tipo==4 ) {
                    $multiple = MovimientoCronogramaPersonasModel::where("id_crono_cab", $idcab)
                        ->get();
                    $guardar_verificacion = CoorCronogramaCabModel::find($idcab);
                    $guardar_verificacion->avance = 60;
                    $guardar_verificacion->estado_actividad = 'EJECUCION_VENCIDO';
                    $guardar_verificacion->save();

                    foreach ($multiple as $key => $value) {

                        $usuario = UsuariosModel::select("users.*")
                            ->where("id", $value->id_usuario)
                            ->first();
                        //show($actividad);


                        $this->notificacion->notificacionesweb("Actividad ha sido verificado con observaciones <b>#</b>" . $idcab . "", $configuraciongeneral[1] . '/' . $idcab . '/edit', $usuario->id, "2c438f");
                        $this->notificacion->EnviarEmail($usuario->email, "Nueva Actividad", "Actividad ha sido verificado con observaciones", "<b>ACTIVIDAD: <b>#</b>" . $idcab . "</b> <br>" . $actividad . "<br>" . $guardar_verificacion->observacion_verificacion, $configuraciongeneral[1] . '/' . $guardar->id . '/edit');
                    }
                }



                $timeline->personas_json = json_encode($json);
                //show($timeline);
                if (Auth::user()->id_perfil != 1) {

                    $timeline->save();
                }
                // dd($timeline);
            }

            // DB::rollback();
            DB::commit();
        } //Try Transaction
        catch (\Exception $e) {
            // ROLLBACK
            DB::rollback();
            $mensaje = $e->getMessage() . " - " . $e->getLine();
            return redirect()->back()->withErrors([$mensaje])->withInput();
        }
        Session::flash('message', $msg);
        return Redirect::to($configuraciongeneral[1]);
    }

    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }


    public function edit($id, $objetosadmin = [], $configuraciongeneraladmin = [])
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        /*Componentes*/
        $veri = CoorActividadesComponenteObjModel::where("id_crono_cab", $id)->first();
        //show($veri->toarray());
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $com = CoorComponenteModel::where("estado", "ACT")->orderBy('nombre','ASC')->select(DB::raw("concat(nombre,' - ',detalle) as componente"), "id");
        if ($veri) {
            $valanteriorobj = CoorComponenteObjetivosModel::find($veri->id_objetivo_componente);
            $valanteriorpro = clone $valanteriorobj;
            $valanteriorcom = CoorComponenteModel::find($valanteriorobj->id_componente);
            //show($valanteriorobj);
            //show(array($veri->toarray(),$valanteriorobj->toarray(),$valanteriorcom->toarray()));
            $objetoscomponentes[0]->ValorAnterior = $valanteriorcom->id;
            $objetoscomponentes[1]->Valor = array($valanteriorobj->id => $valanteriorobj->objetivo);
            $objetoscomponentes[2]->Valor = array($valanteriorpro->id => $valanteriorpro->programa);
            //$objetoscomponentes[1]->ValorAnterior=$valanteriorobj->id;
            if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
                $com = $com->where("id", $valanteriorobj->id_componente)->pluck("componente", "id")->all();
                $objetoscomponentes[0]->Valor = $com;
            } else {
                $com = $com->pluck("componente", "id")->all();
                $objetoscomponentes[0]->Valor = $this->escoja + $com;
            }
        } else {
            $com = $com->pluck("componente", "id")->all();
            $objetoscomponentes[0]->Valor = $this->escoja + $com;
            if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3)
                $objetoscomponentes[0]->Valor = $this->escoja;
            $objetoscomponentes[1]->Valor = $this->escoja;
            $objetoscomponentes[2]->Valor = $this->escoja;
        }
        //show($objetoscomponentes);
        /**/
        $variableControl = null;
        $verObservaciones = "si";
        $tabla = CoorCronogramaCabModel::find($id);
        //show($tabla);    
        //$zona=array("URBANA"=>"URBANA","RURAL"=>"RURAL");
        $this->configuraciongeneral[2] = "editar";
        $objetos = json_decode($this->objetos);
        //$seleccionar_personas_cargo=UsuariosModel::where("estado", "ACT")->pluck("name","id")->all();
        $seleccionar_direccion = direccionesModel::where("estado", "ACT")->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        $objetos[6]->Valor = $this->escoja + $seleccionar_direccion;
        $objetos[7]->Valor = $this->escoja;

        $verificacion = explodewords(ConfigSystem("verificacion"), "|");
        $objetos[13]->Valor = $this->escoja + $verificacion;
        $objetos[13]->ValorAnterior = $tabla->verificacion;
        if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4) {
            unset($objetos[13]);
            unset($objetos[14]);
        }
        // show($objetos);

        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {

            unset($seleccionar_direccion);
            $id_usuario = Auth::user()->id;
            $seleccionar_direccion = direccionesModel::join("users as u", "u.id_direccion", "=", "tmae_direcciones.id")
                ->where([["tmae_direcciones.estado", "ACT"], ["u.id", $id_usuario]])->orderby("direccion", "asc")->pluck("direccion", "tmae_direcciones.id")->all();
            $objetos[6]->Valor = $seleccionar_direccion;
            /*

            $datosFinalizacon = explode("|", ConfigSystem("quitareditar"));
            // show($datosFinalizacon);
            $fechabd = $datosFinalizacon[0];
            $horabd = $datosFinalizacon[1];;

            $fechaactual = date("D");
            $horaactual = date("H");

            if ($fechaactual == $fechabd && intval($horabd) < intval($horaactual)) {

                Session::flash('message', 'Solo puede realizar cambios hasta las '.$horabd);
                return Redirect::to($this->configuraciongeneral[1]);
            }*/
        }

        //Estado
        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $objetos[0]->Valor = $this->escoja + $estadoobras;

        $prioridades = explodewords(ConfigSystem("prioridad"), "|");
        $objetos[10]->Valor = $this->escoja + $prioridades;
        //show($objetos[8]);


        //Personas a cargo
        $valorAnteriorpersonas = MovimientoCronogramaPersonasModel::join("users as a", "a.id", "=", "coor_tmov_cropnograma_resposables.id_usuario")
            ->where("coor_tmov_cropnograma_resposables.id_crono_cab", $id)
            ->select("a.*");
        //show($valorAnteriorpersonas);
        $objetos[7]->Valor = $valorAnteriorpersonas->pluck("name", "id")->all();
        $objetos[7]->ValorAnterior = $valorAnteriorpersonas->pluck("id", "id")->all();
        //show($valotAnterior);
        $btnguardar = 0;
        if (Input::has("list"))
            $btnguardar = 1;

        $timelineActividades = $this->getTimeLineCab($id, 1);
        $timelineActividadesResp = $this->getTimeLineCab($id, 2);
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $id_usuario = Auth::user()->id;
        $edit = null;


        $seleccionar_tipo = TipoActividadPoaModel::where("estado", "ACT")->pluck("tipo_actividad", "id")->all();
        $objetos[2]->Valor = $this->escoja + $seleccionar_tipo;
        $tipopoa = explodewords(ConfigSystem("tipopoa"), "|");

        $valoranteriortipo = CoorCronogramaCabModel::join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
            ->where("coor_tmov_cronograma_cab.id", $id)->get();
        // show($valoranteriortipo);
        $objetos[2]->ValorAnterior = $valoranteriortipo->pluck("id_tipo_actividad", "tipo_actividad")->all();
        // unset($objetos[2]);
        // show($objetos);
        unset($objetos[11]);

        $poaact = PoaActividadModel::where([["id_cab_pro", $id], ["estado", "ACT"]])->first();

        /*====================================*/
        /*====================================*/
        //dsub direcion
        //Valor Anterior
        $ante = SubDireccionModel::join('tmae_direcciones as d', 'd.id', '=', 'tmae_direccion_subarea.area')->select('tmae_direccion_subarea.id_direccion')->where("area", $tabla->id_direccion)->first();
        // show($ante);
        // show($objetos);
        $id_direccion = null;
        $id_sub_direccion = $tabla->id_direccion;
        if ($ante) {
            $id_direccion = $ante->id_direccion;
            $objetos[6]->ValorAnterior = $id_direccion;
            $tabla->id_direccion = $id_direccion;
        }



        //show($objetos);
        switch ($id_tipo_pefil->tipo) {

            case 2: //usuario
                /*OB COmponentes*/
                $objetoscomponentes[0]->Tipo = "selectdisabled";
                $objetoscomponentes[1]->Tipo = "selectdisabled";
                $objetoscomponentes[2]->Tipo = "selectdisabled";
                /**/
                $objetos[0]->Tipo = "selectdisabled";
                $objetos[1]->Tipo = "textdisabled2";
                $objetos[6]->Tipo = "selectdisabled";
                $objetos[4]->Tipo = "textdisabled2";
                $objetos[5]->Tipo = "textdisabled2";
                $objetos[10]->Tipo = "selectdisabled";
                unset($seleccionar_direccion);
                unset($estadoobras);

                //estados
                $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
                $valor = $estadoobras[$tabla->estado_actividad];
                unset($estadoobras);
                $estadoobras = [$valor => $valor];
                $objetos[0]->Valor = $estadoobras;

                //prioridades
                unset($prioridades);
                $prioridades = explodewords(ConfigSystem("prioridad"), "|");
                if ($tabla->prioridad != "") {
                    $valor2 = $prioridades[$tabla->prioridad];
                    unset($prioridades);
                    $prioridades = [$valor2 => $valor2];
                } else {
                    $prioridades = array();
                }
                $objetos[10]->Valor = $prioridades;
                //estados


                $seleccionar_direccion = direccionesModel::where("id", $tabla->id_direccion)->orderby("direccion", "asc")->pluck("direccion", "id")->all();
                $objetos[6]->Valor = $seleccionar_direccion;
                $edit = "no";
                $variableControl = "no";


                $seleccionar_tipo = CoorCronogramaCabModel::join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
                    ->where("coor_tmov_cronograma_cab.id", $id)->pluck("tipo_actividad", "id_tipo_actividad")->all();
                $objetos[2]->Valor = $seleccionar_tipo;
                $tipopoa = explodewords(ConfigSystem("tipopoa"), "|");

                $valoranteriortipo = CoorCronogramaCabModel::join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
                    ->where("coor_tmov_cronograma_cab.id", $id)->get();
                // show($valoranteriortipo);
                $objetos[2]->ValorAnterior = $valoranteriortipo->pluck("id_tipo_actividad", "tipo_actividad")->all();
                break;

            case 3: //Director
                /*OB COmponentes*/
                $objetoscomponentes[0]->Tipo = "selectdisabled";
                $objetoscomponentes[1]->Tipo = "selectdisabled";
                $objetoscomponentes[2]->Tipo = "selectdisabled";
                /**/
                $objetos[0]->Tipo = "selectdisabled";
                $objetos[1]->Tipo = "textdisabled2";
                $objetos[6]->Tipo = "selectdisabled";
                $objetos[4]->Tipo = "textdisabled2";
                $objetos[5]->Tipo = "textdisabled2";
                $objetos[10]->Tipo = "selectdisabled";
                unset($seleccionar_direccion);
                unset($estadoobras);
                //estados
                $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
                $valor = $estadoobras[$tabla->estado_actividad];
                unset($estadoobras);
                $estadoobras = [$valor => $valor];
                $objetos[0]->Valor = $estadoobras;
                //estados

                //prioridades
                unset($prioridades);
                $prioridades = explodewords(ConfigSystem("prioridad"), "|");
                //show($prioridades);
                $valor2 = "";
                if ($tabla->prioridad) {
                    if(isset( $prioridades[$tabla->prioridad])){
                        $valor2 = $prioridades[$tabla->prioridad];
                    }else{
                        $valor2='ALTA';
                    }
                }

                unset($prioridades);
                $prioridades = [$valor2 => $valor2];
                $objetos[10]->Valor = $prioridades;
                //estados

                $seleccionar_direccion = direccionesModel::where("id", $tabla->id_direccion)->orderby("direccion", "asc")->pluck("direccion", "id")->all();
                $objetos[6]->Valor = $seleccionar_direccion;
                $edit = "no";
                $variableControl = "no";

                $seleccionar_tipo = CoorCronogramaCabModel::join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
                    ->where("coor_tmov_cronograma_cab.id", $id)->pluck("tipo_actividad", "id_tipo_actividad")->all();
                $objetos[2]->Valor = $seleccionar_tipo;
                $tipopoa = explodewords(ConfigSystem("tipopoa"), "|");

                $valoranteriortipo = CoorCronogramaCabModel::join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
                    ->where("coor_tmov_cronograma_cab.id", $id)->get();
                // show($valoranteriortipo);
                $objetos[2]->ValorAnterior = $valoranteriortipo->pluck("id_tipo_actividad", "tipo_actividad")->all();
                break;
        }
        $Avance = explodewords(ConfigSystem("avance"), "|");
        $objetos[9]->Valor = $this->escoja + $Avance;
        $objetos[9]->ValorAnterior = intval($tabla->avance);

        if ($id_tipo_pefil->tipo != 4) {
            $avance = [];
            $valor = $tabla->avance;
            if ($valor == null) {
                $avance + $this->escoja;
                $valor = 0;
            }
            for ($i = $valor; $i <= 100; $i++) {

                $avance = $avance + [$i => $i];
                $i = $i + 4;
            }
            $objetos[9]->Valor = $avance;
        }


        //  unset($objetos[10]);

        $dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "4"]])->get();
        $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "4"]])->get();


        $this->configuraciongeneral[3] = "4";
        $this->configuraciongeneral[4] = "si";
        /*MaxLecht TextArea*/
        if ($id_tipo_pefil->tipo != 4) {
            //$objetos[8]->Clase="Null";
            $objetos[8]->Clase = $objetos[8]->Clase . " maxlength:350";
        }
        //show($objetos);
        //show($tabla->toarray());
        if (count($objetosadmin))
            $objetos = $objetosadmin;
        $configuraciongeneral = $this->configuraciongeneral;
        if (count($configuraciongeneraladmin))
            $configuraciongeneral = $configuraciongeneraladmin;
        $objetos = array_merge($objetoscomponentes, $objetos);
        //show($objetos);
        /*====================================*/
        /*====================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"id_coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $cmb = CoordinacionCabModel::join("users as a", "a.id", "=", "coor_tmae_coordinacion_main.id_responsable")
            //->select("coor_tmae_coordinacion_main.*","a.name as responsable")
            ->select("coor_tmae_coordinacion_main.id", DB::raw("concat(coor_tmae_coordinacion_main.nombre,': ',a.name) as nombre"))
            ->where("coor_tmae_coordinacion_main.estado", "ACT")->pluck("nombre", "id")->all();
        //Valor Anterior
        $ante = CoordinacionDetaDireccionModel::where("id_direccion", $tabla->id_direccion)->first();
        if ($ante) {
            $ante = array($ante->id_coor_cab);
            $coordinacion->ValorAnterior = $ante;
        }
        $coordinacion->Valor = $this->escoja + $cmb;
        // show($ante);
        $objetos = insertarinarray($objetos, $coordinacion, 9);
        /*====================================*/
        // show($objetos);

        /*====================================================*/
        /*====================================================*/
        //subdirecion
        $coordinacion = '{"Tipo":"select","Descripcion":"Sub dirección","Nombre":"id_sub_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);

        $coordinacion->Valor = [];
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 11);
        $ante_sub = SubDireccionModel::join('tmae_direcciones as d', 'd.id', '=', 'tmae_direccion_subarea.area')->select('d.id', 'd.direccion')
            ->where("id_direccion", $id_direccion)->pluck('direccion', 'id')->all();

        $objetos[11]->Valor = $ante_sub;
        $objetos[11]->ValorAnterior = $id_sub_direccion;

        // dd($id_direccion);
        /*====================================================*/
        if(Auth::user()->id==1654){

            $botonguardaravance='SI';
        }else{
            $botonguardaravance=null;
        }
        return view('vistas.create', [
            "dicecionesImagenes" => $dicecionesImagenes,
            "dicecionesDocumentos" => $dicecionesDocumentos,
            "timelineActividades" => $timelineActividades,
            "timelineActividadesResp" => $timelineActividadesResp,
            "objetos" => $objetos,
            "variableControl" => $variableControl,
            "tabla" => $tabla,
            "edit" => $edit,
            "poaact" => $poaact,
            "tipopoa" => $tipopoa,
            "botonguardaravance" => $botonguardaravance,
            "configuraciongeneral" => $configuraciongeneral,
            "validarjs" => $this->validarjs,
            //"btnguardar" => $btnguardar,//Comentado 01/10/2019
            "verObservaciones" => $verObservaciones,
            "permisos" => $this->perfil() //Add 01/10/2019
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objetos = json_decode($this->objetos);
        $objetosdetalle = json_decode($this->objetosdetalle);

        unset($objetos[7]);
        //show($this->objetos);
        $objetos[0]->Nombre = "estado_actividad";
        $objetos[1]->Nombre = "actividad";
        $objetos[2]->Nombre = "tipo_actividad";
        $objetos[4]->Nombre = "fecha_inicio";
        $objetos[5]->Nombre = "fecha_fin";
        $objetos[6]->Nombre = "dias_restantes";
        $objetos[6]->Descripcion = "Dias restantes";
        $objetos[8]->Nombre = "observacion";


        unset($objetos[3]);

        $objetos = array_values($objetos);
        //show($objetos);


        $tabla = CoorCronogramaCabModel::join("coor_tmae_tipo_acti_poa as tipo", "tipo.id", "=", "coor_tmov_cronograma_cab.id_tipo_actividad")
            ->select(DB::raw("coor_tmov_cronograma_cab.id,actividad,estado_actividad,fecha_inicio,fecha_fin,CONCAT(DATEDIFF(fecha_fin,fecha_inicio),' días') as dias_restantes,observacion,tipo.tipo_actividad,avance,prioridad,coor_tmov_cronograma_cab.updated_at"))
            ->where("coor_tmov_cronograma_cab.id", $id)
            ->first();
        $tabla2 = MovimientoCronogramaPersonasModel::join('coor_tmov_cronograma_cab as cro', 'coor_tmov_cropnograma_resposables.id_crono_cab', '=', 'cro.id')
            ->join('users as us', 'coor_tmov_cropnograma_resposables.id_usuario', '=', 'us.id')
            ->select("us.name", "us.email")
            ->where("cro.id", $id)
            ->get();

        $poaact = PoaActividadModel::where([["id_cab_pro", $id], ["estado", "ACT"]])->first();
        $timelineActividades = $this->getTimeLineCab($id, 1);
        $timelineActividadesResp = $this->getTimeLineCab($id, 2);

        //show($poaact);
        // IMAGENES Y ARCHIVOS
        $dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"]])->get();
        $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"]])->get();
        //
        return view('vistas.show', [
            "dicecionesImagenes" => $dicecionesImagenes,
            "dicecionesDocumentos" => $dicecionesDocumentos,
            "timelineActividades" => $timelineActividades,
            "timelineActividadesResp" => $timelineActividadesResp,
            "objetos" => $objetos,
            "objetosdetalle" => $objetosdetalle,
            "tabla" => $tabla,
            "tabla2" => $tabla2,
            "poaact" => $poaact,
            "configuraciongeneral" => $this->configuraciongeneral
            //"validararry"=>$validararry
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $configuraciongeneraladmin = [])
    {
        //
        $configuraciongeneral = $this->configuraciongeneral;
        if (count($configuraciongeneraladmin))
            $configuraciongeneral = $configuraciongeneraladmin;
        DB::beginTransaction();
        try {
            $tabla = CoorCronogramaCabModel::find($id);
            $tabla->estado = 'INA';
            $tabla->id_usuario = Auth::user()->id;
            //$tabla->delete();
            $tabla->save();
            // demas modelos
            //$tabla2->delete();
            $tabla3 = CoorCronogramaDetaActModel::where("id_crono_cab", $id)->get();
            foreach ($tabla3 as $key => $value) {
                # code...
                $dele = CoorCronogramaDetaActModel::find($value->id);
                $dele->estado = 'INA';
                $dele->save();
            }
            // MovimientoComunicacionPersonasModel
            // CabPresupuestoModel
            // PresupuestoDetalleModel
            // MovComunicacionParroquiaModel

            //->update(array('estado' => 'INACTIVO'));
            DB::commit();
            Session::flash('message', 'El registro se eliminó Exitosamente!');
            if (Input::has('vista')) {
                return 'El registro se eliminó Exitosamente!';
            }
            return Redirect::to($configuraciongeneral[1]);
        } //Try Transaction
        catch (\Exception $e) {
            //  // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($configuraciongeneral[1]);
        }
    }


    //timeline edit and show cronograma
    public function getTimeLineCab($id, $tipo)
    {
        $sw = 0;
        switch ($tipo) {
            case 1:
                $tabla = CoorCronogramaDetaModel::select("coor_tmov_cronograma_deta.*", "u.name", "dir.direccion")
                    ->join("users as u", "u.id", "=", "coor_tmov_cronograma_deta.id_usuario")
                    ->join("tmae_direcciones as dir", "dir.id", "=", "coor_tmov_cronograma_deta.id_direccion")
                    ->where('id_crono_cab', $id)
                    ->orderBy("coor_tmov_cronograma_deta.updated_at", "ASC")
                    ->get();
                return $tabla;
                break;
            case 2:
                $personas[] = [];
                $tabla = CoorCronogramaDetaModel::select("personas_json")->where('id_crono_cab', $id)->get();
                foreach ($tabla as $valor) {

                    $personas = json_decode($valor->personas_json);

                    unset($tabla);
                    foreach ($personas as $p) {
                        $tabla[] = DB::table("users")->where("id", $p)->select("name")->first();
                        $sw = 1;
                    }
                }
                if ($sw == 0) {
                    $tabla = array();
                }

                return $tabla;
                break;
        }
    }

    public function actividadesvencidasindex(Request $request)
    {
        //show(Input::all());
        $acti = "";
        if (Input::has("actividades"))
            $acti = "&actividades=" . Input::get("actividades");
        if (!Input::has("ruta"))
            return $this->index("coordinacioncronograma/actividadesvencidasindex?ruta=ajax$acti");
        //show($acti);
        //show(Input::all());
        //$filtro = [["coor_tmov_cronograma_cab.estado_actividad", "EJECUCION"], ["coor_tmov_cronograma_cab.avance", "<", 100], ["fecha_fin", "<", date("Y-m-d")]];
        $filtro = [["coor_tmov_cronograma_cab.avance", "<", 100], ["fecha_fin", "<", date("Y-m-d")]];
        if (Input::has("actividades")) {
            //$acti=Crypt::decrypt(Input::get("actividades"));
            $acti = Input::get("actividades");
            $filtro = array("actividades" => json_decode($acti, TRUE));
            //$acti=implode(",",$acti);
            /*$filtro=function($query)use($acti){
                $query->whereIn("coor_tmov_cronograma_cab.id",$acti);
            };*/
            //$filtro=[]
        }
        //show($filtro);
        return $this->cronogramaajax($request, $filtro);
        //return $this->actividadesvencidasindexajax();
        //if (count($filtro))
        //$tablamain->where($filtro);
    }
}
