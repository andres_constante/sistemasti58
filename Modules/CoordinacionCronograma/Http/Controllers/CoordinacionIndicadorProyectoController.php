<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Modules\CoordinacionCronograma\Entities\CoordinacionIndicadorProyectoModel;

class CoordinacionIndicadorProyectoController extends Controller
{
    var $configuraciongeneral = array ("Indicador de proyecto", "coordinacioncronograma/indicadoresproyecto", "index",6=>"null", 7=>"indicadoresproyecto");
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Indicador","Nombre":"indicador","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
            "tipo"=>"indcador: {
                            required: true
                        }"
        );
    public function __construct() {
        $this->middleware('auth');
    }

	public function index()
	{
		$tabla=CoordinacionIndicadorProyectoModel::where("estado","ACT")->orderby("id","desc")->get();//->paginate(500);
		return view('vistas.index',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>"si",
                "create"=>"si"
                ]);
		//return view('coordinacioncronograma::index');
	}

	public function create()
    {
        //
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
	}

	public function guardar($id)
    {
           $input=Input::all();

        //    \dd($input);

            $ruta=$this->configuraciongeneral[1];

            if($id==0)
            {
                $ruta.="/create";
                $guardar= new CoordinacionIndicadorProyectoModel();
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= CoordinacionIndicadorProyectoModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }

            $input=Input::all();
            $arrapas=array();

            $validator = Validator::make($input, CoordinacionIndicadorProyectoModel::rules($id));

            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {

                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }
                 }

                 $guardar->save();
                 //Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->tipo));
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }

  public function store(Request $request)
    {
        //
        return $this->guardar(0);
	}

	public function show($id)
    {
        //
        $tabla = CoordinacionIndicadorProyectoModel::find($id);
        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
	}

	public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $tabla = CoordinacionIndicadorProyectoModel::find($id);
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "tabla"=>$tabla,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }


	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=CoordinacionIndicadorProyectoModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
