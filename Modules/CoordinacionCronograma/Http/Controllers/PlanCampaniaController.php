<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\ArchivosModel;
use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionCabModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionDetaDireccionModel;
use Modules\CoordinacionCronograma\Entities\PlanCampaniaDetaModel;
use Modules\CoordinacionCronograma\Entities\PlanCampaniaModel;
use Modules\CoordinacionCronograma\Entities\PlanCampaniaResponsableModel;
use stdClass;

class PlanCampaniaController extends Controller
{
    var $configuraciongeneral = array("Plan de Propuestas", "coordinacioncronograma/plancampania", "index", 6 => "coordinacioncronograma/plancampaniaajax", 7 => "plancampania");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        
        {"Tipo":"textarea2","Descripcion":"Objetivo general","Nombre":"objetivo_general","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },        
        {"Tipo":"textarea2","Descripcion":"Componente","Nombre":"componente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea2","Descripcion":"Objetivos Específicos","Nombre":"objetivo_especificos","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea2","Descripcion":"Actividades","Nombre":"actividades","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea2","Descripcion":"Actividades Propuestas","Nombre":"actividades_propuestas","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Verificable","Nombre":"verificable","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea2","Descripcion":"Observaciones","Nombre":"observaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },        
        {"Tipo":"select","Descripcion":"Dirección a Cargo","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Responsable(s)","Nombre":"idusuario","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Años","Nombre":"json_anios","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado_plan","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Avance (%)","Nombre":"avance","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';


    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }

    var $validarjs = array(
        "diagnostico" => "diagnostico: {
                            required: true
                        }",
        "objetivo_general" => "objetivo_general: {
                            required: true
                        }",
        "componente" => "componente: {
                            required: true
                        }",
        "fecha_inicio" => "fecha_inicio: {
                            required: true
                        }",
        "fecha_fin" => "fecha_fin: {
                            required: true
                        }",
        "id_direccion" => "id_direccion: {
                            required: true
                        }",
        "objetivo_especifico" => "objetivo_especifico: {
                            required: true
                        }",
        "actividades_propuestas" => "actividades_propuestas: {
                            required: true
                        }",
        "verificable" => "verificable: {
                            required: true
                        }"
    );

    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "users.id_perfil")->where("users.id", Auth::user()->id)->first();

        $object = new stdClass;
        // show($id_tipo_pefil);
        if ($id_tipo_pefil->tipo == 1  || $id_tipo_pefil->tipo == 4 || $id_tipo_pefil->id_perfil == 22) {
            //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'NO';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        }
        //show($object);
        return $object;
    }

    public function verpermisos($objetos = array(), $tipo = "index")
    {
        //show($objetos);
        $coordinacion = "SELECT nombre from coor_tmae_coordinacion_main AS a INNER JOIN coor_tmov_coordinacion_direccion_deta AS b
	                    WHERE a.id=b.id_coor_cab AND b.id_direccion=plan_tmov_cab_plancampania.id_direccion
            ";
        $tabla = PlanCampaniaModel::join("tmae_direcciones as a", "a.id", "=", "plan_tmov_cab_plancampania.id_direccion")
            ->select("plan_tmov_cab_plancampania.*", "a.direccion", DB::raw("ifnull(($coordinacion),'') as coordinacion"))
            ->where("plan_tmov_cab_plancampania.estado", "ACT")
            ->orderby("plan_tmov_cab_plancampania.id", "desc");
        $seleccionar_direccion = direccionesModel::where("estado", "ACT");

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 2) {
            if (Auth::user()->id != 1764) {
                $tabla = PlanCampaniaModel::where("plan_tmov_cab_plancampania.estado", "ACT")
                    ->orderby("plan_tmov_cab_plancampania.id", "desc");

                $seleccionar_direccion = $seleccionar_direccion->where("id", Auth::user()->id_direccion);

                $tabla = $tabla->where("plan_tmov_cab_plancampania.id_direccion", Auth::user()->id_direccion);
            }
        }
        ////////////////
        if ($id_tipo_pefil->tipo == 10) {
            $direcciones = getDireccionesCoor(Auth::user()->id);
            $tabla = $tabla->whereIn("plan_tmov_cab_plancampania.id_direccion", explode(",", $direcciones->id_direccion . "," . Auth::user()->id_direccion));
        }
        if ($tipo == "index") {
            $objetos[10]->Nombre = "direccion";
            unset($objetos[1]);
            unset($objetos[2]);
            // unset($objetos[3]);
            unset($objetos[11]);
            unset($objetos[12]);
            unset($objetos[13]);
        }
        if ($tipo == "show") {

            // show($objetos);
            unset($objetos[11]);
            $objetos[9]->Nombre = "direccion";
        }

        // show($tabla->get());
        return array($objetos, $tabla);
    }

    function plancampaniaajax(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        //show($tabla);
        $objetos = $tabla[0];
        $tabla = $tabla[1];
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 10);
        // show($objetos);
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        // show($columns);
        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('plan_tmov_cab_plancampania.id', 'LIKE', "%{$search}%")
                    ->orWhere('plan_tmov_cab_plancampania.diagnostico', 'LIKE', "%{$search}%")
                    // ->orWhere('a.direccion', 'LIKE', "%{$search}%")
                ;
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            $permisos = $this->perfil();
            foreach ($posts as $post) {
                //=============================================================
                $show = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                //'&nbsp;&nbsp;' .
                $edit = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id), "menu" => "no"), array('class' => 'fa fa-pencil-square-o', 'onclick' => 'popup(this)'));
                //. '&nbsp;&nbsp;
                $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                if ($permisos->ver == 'NO')
                    $show = "";
                if ($permisos->editar == 'NO')
                    $edit = "";
                if ($permisos->eliminar == 'NO')
                    $dele = "";
                $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                //=============================================================
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $valorc = $post->$campo;
                    $nestedData["$value->Nombre"] = "$valorc";
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = $this->verpermisos(json_decode($this->objetos));

        if (Auth::user()->cedula == '1003596176' || Auth::user()->cedula == '1312006610') {
            return back();
        }
        //show($objetos);
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $objetos = insertarinarray($objetos[0], $coordinacion, 10);
        // show($objetos);
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si",
            "permisos" => $this->perfil()
        ]);
    }


    public function formularioscrear($id)
    {


        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);
        /*Dirección*/
        $seleccionar_direccion = direccionesModel::orderby("direccion", "asc")->pluck("direccion", "id")->all();
        $objetos[9]->Valor = $this->escoja + $seleccionar_direccion;
        $objetos[10]->Valor = [];
        $obaddjs = array();
        $objetosmain = $this->verpermisos($objetos, "crear");
        $objetos =  $objetosmain[0];
        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $objetos[12]->Valor = $this->escoja + $estadoobras;

        $avance = [];
        for ($i = 0; $i <= 20; $i++) {
            $avance = $avance + [$i => $i];
        }
        $objetos[13]->Valor = $this->escoja + $avance;
        // dd($objetos);

        /*====================================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"id_coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $cmb = CoordinacionCabModel::join("users as a", "a.id", "=", "coor_tmae_coordinacion_main.id_responsable")
            //->select("coor_tmae_coordinacion_main.*","a.name as responsable")
            ->select("coor_tmae_coordinacion_main.id", DB::raw("concat(coor_tmae_coordinacion_main.nombre,': ',a.name) as nombre"))
            ->where("coor_tmae_coordinacion_main.estado", "ACT")->pluck("nombre", "id")->all();
        $coordinacion->Valor = $this->escoja + $cmb;
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 9);
        // show($objetos);
        $js = '{"Tipo":"htmlplantilla","Descripcion":"Plan Plurianual","Nombre":"js_indicadores_plantilla","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $js = json_decode($js);

        $html = '<script>  
                $(document).ready(function () {
                    var data_=["2019","2020","2021","2022","2023"];
                    $("#label_json_anios").hide();
                    $("#div_json_anios").hide();
                    $("#label_suma").hide();
                    $.each(data_, function(key, element) {
                        $("#tabla_indicadores").append(\'<tr id="fila_\'+element+\'" ><td>\'+element+\'</td><td><input value="\'+element+\'" hidden></input> <input type="number" id="input_anio_\'+element+\'" onkeyup="armar_json(\'+element+\')"></input></td></tr>\');
                    });                    
                });
                function armar_json(id){
                    var all_says = [];
                    var suma =0;
                    $("#tabla_indicadores tbody tr").each(function (index) { 
                        $(this).children("td").each(function (index2) {
                            if(index2==1){
                                var anio=0;
                                var valor=0;
                                $(this).children("input").each(function (index3) {
                                    if(index3==0){
                                        anio=$(this).val();
                                    }
                                    if(index3==1){
                                        valor=$(this).val();
                                    }
                                });  
                                var obj = {
                                    anio: anio,
                                    valor: valor
                                  };
                                  
                                  suma=suma+Number(valor);        
                                  all_says.push(obj);
                                
                            }
                        });
                    });
                    $("#json_anios").val(JSON.stringify(all_says));
                    
                }
                </script>
                 
                <div>
                <br>
                
                <table class="table table-striped" id="tabla_indicadores">
                <thead>
                    <tr>
                    <th scope="col">Año</th>
                    <th scope="col">Valor</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                </table></div>';


        $js->Valor = $html;
        $objetos = insertarinarray($objetos, $js, 12);
        // show($objetos);


        /*====================================================*/
        if ($id != "") {
            $this->configuraciongeneral[2] = "editar";
            $this->configuraciongeneral[3] = "12"; //Tipo de Referencia de Archivo
            $this->configuraciongeneral[4] = "si";
            $id = Crypt::decrypt($id);
            $tabla = PlanCampaniaModel::find($id);
            /*====================================*/
            //COORDINACION
            //Valor Anterior
            $ante = CoordinacionDetaDireccionModel::where("id_direccion", $tabla->id_direccion)->first();
            if ($ante) {
                $ante = array($ante->id_coor_cab => $ante->id_coor_cab);
                $objetos[9]->ValorAnterior = $ante;
            }
            $objetos[14]->ValorAnterior = $tabla->estado_plan;
            $objetos[15]->ValorAnterior = $tabla->avance;
            $objetos[10]->ValorAnterior = $tabla->id_direccion;
            /*Responsables*/
            $valorAnteriorpersonas = PlanCampaniaResponsableModel::join("users as a", "a.id", "=", "plan_tmov_responsable.id_usuario")
                ->where("plan_tmov_responsable.id_cab_plan", $id)
                ->select("a.*");
            // show($valorAnteriorpersonas);
            if (array_key_exists(12, $objetos)) {
                $objetos[11]->Valor = $valorAnteriorpersonas->pluck("name", "id")->all();
                $objetos[11]->ValorAnterior = $valorAnteriorpersonas->pluck("id", "id")->all();
            }
            $objetos = $this->asignaroles($objetos, $this->perfil(), $tabla);
            $anios = json_decode($tabla->json_anios);
            $valor = 0;
            $avance_anterior = 0;
            foreach ($anios as $key => $value) {
                # code...
                $value->anio;
                if (date('Y') - 1 == $value->anio && $tabla->avance <= $value->valor) {
                    $valor = $value->valor;

                    // dd($tabla->avance);

                } elseif (date('Y') == $value->anio &&  $value->valor != '') {
                    $valor = $value->valor;
                    // if($valor==100){
                    //     $valor=99;
                    // }

                }
                if ($value->anio <= date('Y') &&  $value->valor < 100) {
                    if ($value->valor != '') {
                        $valor = $valor + $value->valor;
                    }
                }
            }
            $avance = [];
            $valor_inicial = null;
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
            if ($id_tipo_pefil->tipo != 4) {
                $valor_inicial = $tabla->avance;
            }

            if ($valor_inicial == null) {
                $valor_inicial = 0;
            }


            for ($i = $valor_inicial; $i <= $valor; $i++) {
                $avance = $avance + [$i => $i];
            }


            $objetos[15]->Valor = $this->escoja + $avance;

            $objetos[12]->Valor = $objetos[12]->Valor . '
            <script> 
            
            $(document).ready(function () {
                var data_json=' . $tabla->json_anios . ';
                $.each(data_json, function(key, element) {                
                    document.getElementById("input_anio_"+element.anio+"").value=element.valor;
                });
            });
              
            </script>';
            // show($objetos);
            if (Auth::user()->cedula == 'admin' || Auth::user()->cedula == "1311879165") {
                $eliminar = 'SI';
            } else {
                $eliminar = null;
            }
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
                "eliminar_archivo" => $eliminar,
                "botonguardaravance" => 'no',
            );
            /*Setear*/
            // show($objetos);
        } else {
            $this->configuraciongeneral[2] = "crear";
            //$objetos = $this->verpermisos($objetos, "crear")[0];
            /*Setear Chaleco Azul*/
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            );
        }

        if ($this->configuraciongeneral[2] == "editar") {
            //            $this->configuraciongeneral[3] = "9"; //Tipo de Referencia de Archivo
            //            $this->configuraciongeneral[4] = "si";
            $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "=", "pdf"], ["tipo", "12"]])->get();
            $dicecionesImagenes = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "<>", "pdf"], ["tipo", "12"]])->get();
        } else {
            $dicecionesDocumentos = null;
            $dicecionesImagenes = null;
        }
        $files = array(
            'dicecionesImagenes' => $dicecionesImagenes,
            'dicecionesDocumentos' => $dicecionesDocumentos
        );
        $datos = array_merge($datos, $files);
        //show($datos);
        return view('vistas.create', $datos);
    }
    public function asignaroles($objetos, $permisos, $tabla)
    {
        if ($permisos->editar_parcialmente == 'SI') {
            //show($objetos);
            foreach ($objetos as $key => $value) {
                if ($value->Tipo == "select" && $value->Nombre != "avance") {
                    $objetos[$key]->Tipo = "selectdisabled";
                    //show($tabla);
                    if ($objetos[$key]->ValorAnterior != "Null") {

                        $pos = $objetos[$key]->ValorAnterior;
                        if (!is_array($pos)) {
                            // dd( $value->Valor[$pos]);
                            $objetos[$key]->Valor = array($pos => $value->Valor[$pos]);
                        } else
                            foreach ($pos as $keyaa => $vaa)
                                $objetos[$key]->Valor = array($keyaa => $value->Valor[$vaa]);
                    } else {
                        //
                        $in = $objetos[$key]->Nombre;
                        $kk = $tabla->$in;
                        $aar = $objetos[$key]->Valor;
                        if (array_key_exists($kk, $aar)) {
                            //$objetos[$key]->Tipo = "html2";
                            $objetos[$key]->Valor = array($kk => $aar[$kk]);
                        }
                    }
                } elseif ($value->Tipo == "textarea-2" || $value->Tipo == "textarea" || $value->Tipo == "text") {
                    if ($value->Nombre != "json_indicadores" && $value->Nombre != "observaciones") {
                        $objetos[$key]->Tipo = "html2";
                        $in = $objetos[$key]->Nombre;
                        $kk = $tabla->$in;
                        $objetos[$key]->Valor = "" . $kk;
                    }
                }
            }
        }
        // show($objetos);
        return $objetos;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        if ($this->perfil()->crear == "NO") {
            Session::flash('message', "Ud no tiene permisos para crear");
            return Redirect::to($this->configuraciongeneral[1]);
        };
        return $this->formularioscrear("");
    }

    public function guardar($id)
    {
        //show($id);
        $input = Input::all();
        // show($input);
        $ruta = $this->configuraciongeneral[1];
        $menuno = "";
        if (Input::has("menu")) {
            $menuno = "?menu=no";
            unset($input["menu"]);
        }
        DB::beginTransaction();
        try {
            if ($id == 0) {
                $ruta .= "/create";
                $guardar = new PlanCampaniaModel;
                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro Compromiso Alcalde";
            } else {
                $ruta .= "/" . Crypt::encrypt($id) . "/edit";
                $guardar = PlanCampaniaModel::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Compromiso Alcalde modificado";
            }


            $validar = PlanCampaniaModel::rules($id);
            $validator = Validator::make($input, $validar);

            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $timeline = new PlanCampaniaDetaModel;
                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token" && $key != "idusuario"  && $key !=  "id_coordinacion") {
                        $guardar->$key = $value;
                        $timeline->$key = $value;
                    }
                }
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                $guardar->save();
                $idcab = $guardar->id;

                $timeline->id_plan_cab = $idcab;
                $timeline->id_usuario = Auth::user()->id;
                $timeline->ip = \Request::getClientIp();
                $timeline->pc = \Request::getHost();
                $timeline->save();




                /*Responsables*/
                if (Input::has("idusuario")) {
                    $multiple = Input::get("idusuario");
                    $json = [];
                    foreach ($multiple as $key => $value) {
                        # code...
                        //Responsables
                        $guardarres = PlanCampaniaResponsableModel::where("id_usuario", $value)
                            ->where("id_cab_plan", $idcab)
                            ->first();
                        if (!$guardarres)
                            $guardarres = new PlanCampaniaResponsableModel;
                        $guardarres->id_usuario = $value;
                        $guardarres->id_cab_plan = $idcab;
                        $guardarres->save();
                        $json[] = $value;
                        //
                        $usuario = UsuariosModel::select("users.*")
                            ->where("id", $guardarres->id_usuario)
                            ->first();
                        //show($actividad);
                        $notificacion = new NotificacionesController();
                        $notificacion->notificacionesweb("Se le ha asignado un plan de campaña <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit', $usuario->id, "2c438f");
                        $this->notificacion->EnviarEmail($usuario->email, "Plan de propuesta", "Se le ha asignado plan de propuesta", "<b>ACTIVIDAD: <b>#</b>" . $idcab . "</b> <br>" . $guardar->actividades, $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit');
                    }
                    Auditoria($msgauditoria . " - ID: " . $id);
                }

                $ususrio = UsuariosModel::find(1764);
                $this->notificacion->notificacionesweb("Se le ha modificado un plan de campaña <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit', $ususrio->id, "2c438f");
                $this->notificacion->EnviarEmail($ususrio->email, "Plan de propuesta", "Se le ha modificado un plan de campaña", "<b>ACTIVIDAD: <b>#</b>" . $idcab . "</b> <br>" . $guardar->actividades, $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit');

                DB::commit();
                Session::flash('message', $msg);
                return Redirect::to($this->configuraciongeneral[1] . $menuno);
            } //
        } catch (\Exception $e) {
            // ROLLBACK
            DB::rollback();
            $mensaje = $e->getMessage();
            return redirect()->back()->withErrors([$mensaje])->withInput();
        }
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos), "show");
        $tabla = $objetos[1]->where("plan_tmov_cab_plancampania.id", $id)->first();
        $objetos = $objetos[0];
        // dd($objetos);
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        // show($tabla);
        $objetos = insertarinarray($objetos, $coordinacion, 9);




        $valorAnteriorpersonas = PlanCampaniaResponsableModel::join("users as a", "a.id", "=", "plan_tmov_responsable.id_usuario")
            ->select(DB::raw("GROUP_CONCAT(a.name SEPARATOR ',') as responsable"))
            ->where("plan_tmov_responsable.id_cab_plan", $id)
            ->first();


        // dd($tabla);



        $js = '{"Tipo":"htmlplantilla","Descripcion":"Indicadores","Nombre":"js_indicadores_plantilla","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $js = json_decode($js);
        $html = ' <div>
        <br>
        
        <table class="table table-striped" id="tabla_indicadores">
        <thead>
            <tr>
            <th scope="col">Año</th>
            <th scope="col">Valor</th>
            </tr>
        </thead>
        <tbody>';
        $anios = json_decode($tabla->json_anios);

        foreach ($anios as $key => $value) {
            $html .= '<tr>
            <td>' . $value->anio . '</td>
            <td>' . $value->valor . '</td>
            </tr>';
        }

        $html .= '</tbody>
        </table></div>';
        $js->Valor = $html;
        // $objetos = insertarinarray($objetos, $coordinacion, 2);
        // $objetos = insertarinarray($js, $coordinacion, 8);        

        $tabla->idusuario = ((isset($valorAnteriorpersonas)) ? $valorAnteriorpersonas->responsable : '') . ' ' . $html;
        unset($objetos[13]);
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();
        try {
            $tabla = PlanCampaniaModel::find($id);
            $tabla->estado = 'INA';
            $tabla->id_usuario = Auth::user()->id;
            //$tabla->delete();
            // dd($tabla);
            $tabla->save();
            DB::commit();
            Session::flash('message', 'El registro se eliminó Exitosamente!');
            if (Input::has('vista')) {
                return 'El registro se eliminó Exitosamente!';
            }
            return Redirect::to($this->configuraciongeneral[1]);
        } //Try Transaction
        catch (\Exception $e) {
            //  // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);
        }
    }
}
