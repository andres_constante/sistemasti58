<?php namespace Modules\Coordinacioncronograma\Http\Controllers;


use Illuminate\Routing\Controller;
use App\ModulosModel;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Route;
use Auth;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use DB;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
/**/

use Modules\Coordinacioncronograma\Http\Controllers\CoordinacionListObrasController;

class CoordinacionCharsObrasController extends Controller
{
    var $configuraciongeneral = array("Reporte Estadístico de Obras", "coordinacioncronograma/coordinaciongraficosobras", "index");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
		{"Tipo":"chartnuevo","Descripcion":"GRÁFICO DE OBRAS","Nombre":"chartnuevo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"chart2","Descripcion":"GRÁFICO DE OBRAS POR PARROQUIA","Nombre":"chart4","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"chart","Descripcion":"MAPA DEL CANTÓN","Nombre":"chart5","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
	]';

    protected $CoordinacionLista;

    public function __construct(CoordinacionListObrasController $CoordinacionLista)
    {
        $this->middleware('auth');
        $this->CoordinacionLista = $CoordinacionLista;
    }

    public function index()
    {
        $direcciones = parroquiaModel::select('id', 'parroquia')->where('estado', '=', 'ACT')->get();
        $objetos = json_decode($this->objetos);
        $objetos = array_values($objetos);

        //show($objetos);


        $datos = CoorObrasModel::select(DB::raw("count(id) as total,estado_obra"))
            ->groupBy("estado_obra")
            ->where("estado", "ACT")
            ->get();
        $total = CoorObrasModel::select(DB::raw("count(*) as total"))
            ->where("estado", "ACT")
            ->first();
        $iboxObras = $this->getObras($datos, $total);
        // show($total);
        $objetosTable = json_decode($this->CoordinacionLista->objetosTable);
        return view('vistas.indexchart', [
            "objetos" => $objetos,
            "direcciones" => $direcciones,
            "iboxObras" => $iboxObras,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si",
            "total" => $total,
            "objetosTable" => $objetosTable
        ]);
    }

    public function getObras($datos, $total)
    {

        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $estadoobrascolores = explode("|", ConfigSystem("estadoobrascolor"));


        foreach ($datos as $item) {
            switch ($item->estado_obra) {
                case 'EJECUCION_VENCIDO':
                    $item->estado_obra = 'EJECUTADO VENCIDO';
                    $item->color = $estadoobrascolores[0];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);

                    break;
                case 'EJECUTADO':
                    $item->estado_obra = 'EJECUTADO';
                    $item->color = $estadoobrascolores[1];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);
                    break;
                case 'EJECUCION':
                    $item->estado_obra = 'EJECUCIÓN';
                    $item->color = $estadoobrascolores[2];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);

                    break;
                case 'POR_EJECUTAR':
                    $item->estado_obra = 'POR EJECUTAR';
                    $item->color = $estadoobrascolores[3];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);

                    break;
                case 'SUSPENDIDO':
                    $item->color = $estadoobrascolores[4];
                    $item->porcentaje = round((($item->total / $total->total) * 100), 2);
                    break;

            }


        }
        return $datos;

    }

    public function getValoresChartsObras()
    {
        $tipo_chart = Input::get("tipo");
        $parroquia = Input::get("direccion");
        $valores;

        switch ($tipo_chart) {
            case '2':

                $valores = CoorObrasModel::select(DB::raw("count(nombre_obra) as valor, estado_obra"))
                    ->where("estado", "ACT")->groupby("estado_obra")->get();
                return $valores;
                break;
            case '3':

                $valores = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,pa.parroquia"))
                    ->where("coor_tmov_obras_listado.estado", "ACT")->groupby("pa.parroquia")
                    ->orderby(DB::raw("count(coor_tmov_obras_listado.nombre_obra)"))
                    ->get();

                return $valores;
                break;
            case '4':
                $valores = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["coor_tmov_obras_listado.id_parroquia", $parroquia]])->groupby("coor_tmov_obras_listado.estado_obra")
                    ->orderby(DB::raw("count(coor_tmov_obras_listado.nombre_obra)"))
                    ->get();
                return $valores;
                break;
            case '5':


                $EJECUCION = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["estado_obra", "EJECUCION"], ["pa.id", $parroquia]])
                    ->first();

                $EJECUTADO = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["estado_obra", "EJECUTADO"], ["pa.id", $parroquia]])
                    ->first();
                $EJECUCION_VENCIDO = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["estado_obra", "EJECUCION_VENCIDO"], ["pa.id", $parroquia]])
                    ->first();
                $POR_EJECUTAR = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["estado_obra", "POR_EJECUTAR"], ["pa.id", $parroquia]])
                    ->first();
                $SUSPENDIDO = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["estado_obra", "SUSPENDIDO"], ["pa.id", $parroquia]])
                    ->first();

                $valores[] = ['EJECUCION', $EJECUCION->valor, 0, 0, 0, 0];
                $valores[] = ['EJECUTADO', 0, $EJECUTADO->valor, 0, 0, 0];
                $valores[] = ['EJECUCION_VENCIDO', 0, 0, $EJECUCION_VENCIDO->valor, 0, 0];
                $valores[] = ['POR_EJECUTAR', 0, 0, 0, $POR_EJECUTAR->valor, 0];
                $valores[] = ['SUSPENDIDO', 0, 0, 0, 0, $SUSPENDIDO->valor];


                return $valores;

                break;
            case '6':


                $EJECUCION = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["estado_obra", "EJECUCION"]])
                    ->first();
                $valores[] = ['EJECUCION', $EJECUCION->valor, 0, 0, 0, 0];


                $EJECUTADO = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["estado_obra", "EJECUTADO"]])
                    ->first();
                $valores[] = ['EJECUTADO', 0, $EJECUTADO->valor, 0, 0, 0];
                $EJECUCION_VENCIDO = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["estado_obra", "EJECUCION_VENCIDO"]])
                    ->first();

                $valores[] = ['EJECUCION_VENCIDO', 0, 0, $EJECUCION_VENCIDO->valor, 0, 0];
                $POR_EJECUTAR = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["estado_obra", "POR_EJECUTAR"]])
                    ->first();
                $valores[] = ['POR_EJECUTAR', 0, 0, 0, $POR_EJECUTAR->valor, 0];
                $SUSPENDIDO = CoorObrasModel::
                join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
                    ->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
                    ->where([["coor_tmov_obras_listado.estado", "ACT"], ["estado_obra", "SUSPENDIDO"]])
                    ->first();
                $valores[] = ['SUSPENDIDO', 0, 0, 0, 0, $SUSPENDIDO->valor];

                return $valores;

                break;


                return response()->json($valores);
        }


    }

}
