<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;

class MascotasReporteController extends Controller
{
    var $escoja = array(0 => "TODOS");
    var $formatos = array("pdf" => "PDF", "xls" => "XLS");
    var $objects = '[
        {"Tipo": "select", "Descripcion": "Parroquia", "Nombre": "id_parroquia", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Responsable", "Nombre": "responsable", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "fecha_ini", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "fecha_fin", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
        {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
        {"Tipo":"select","Descripcion":"Sexo de la mascota","Nombre":"sexo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado de la mascota","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $validate_js = [];
        $general_config = ['REPORTE DE INCIDENCIAS TERRITORIALES', 'coordinacioncronograma/reporteincidencias', 'ReporteincidenciaControllerajax'];
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "
        <script type='text/javascript'>        
        function titurepor()
        {
            var entidad=$('#idtipo_entidad').find('option:selected').text()+':';
            var estado =  $('#estado').val();
            var fecha_ini = $('#fecha_ini').val();
            var fecha_fin = $('#fecha_fin').val();
            var id_responsable = $('#id_responsable').val();
            
            $('#tituloreporte').val('fdgfhgf');
            
        }             
          $(document).ready(function() {
                 $('#tituloreporte').val('fdgfhgf');
            $('#namereporte').change(function(event) {                
                return titurepor();
            });
            $('#namereporte').trigger('change');
          });
        </script>
        ";

        $formatosjs = array(
            //"rpt_rendimiento_actividad_noagrupado" => '{"html":"Tabla HTML","pdf":"PDF","xls":"XLS"}',
            "rpt_reporte_incidencias" => '{"pdf":"PDF","xls":"XLS"}',
            //"rpt_rendimiento_direccion" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_actividad_sin_avance" => '{"pdf":"PDF","xls":"XLS"}',
            //"rpt_direccion_sin_avance_detallado" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_actividad_vencidas" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_actividad_cien" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_graficos" => '{"graficos":"Gráficos"}'
            //,"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            //"rpt_rendimiento_actividad_noagrupado" => '{"completo":"Completo","informe":"Informe"}',
            //"rpt_reporte_incidencias" => '{"completo":"Completo","informe":"Informe","observa":"Informe con Observación"}',
            "rpt_reporte_incidencias_agrupado_entidad" => '{"pdf":"PDF"}',
            "rpt_reporte_incidencias" => '{"pdf":"PDF"}',
            //"rpt_rendimiento_direccion" => '{"pdf":"Completo"}',
            "rpt_rendimiento_actividad_sin_avance" => '{"completo":"Completo"}',
            //"rpt_direccion_sin_avance_detallado" => '{"completo":"Completo","informe":"Informe"}',
            "rpt_rendimiento_actividad_vencidas" => '{"completo":"Completo"}',
            "rpt_rendimiento_actividad_cien" => '{"completo":"Completo"}',
            "rpt_graficos" => '{"informe":"Informe"}'
        );

        $namerepor = array(
            "rpt_reporte_incidencias_agrupado_entidad" => "Reporte de incidencias detallado",
            "rpt_reporte_incidencias" => "Reporte general de incidencias",
        );
        $formatosjs = array(
            "rpt_reporte_incidencias_agrupado_entidad" => '{"html":"Tabla HTML","pdf":"PDF","xls":"XLS"}',
            "rpt_reporte_incidencias" => '{"html":"Tabla HTML","pdf":"PDF","xls":"XLS"}',

        );
        $tiporepor = array(
            "rpt_reporte_incidencias_agrupado_entidad" => '{"pdf":"PDF"}',
            "rpt_reporte_incidencias" => '{"pdf":"PDF"}'
        );

        $fecha_ini = date('Y') . "-01-01";
        $fecha_fin = date('Y') . "-12-31";

        $objeto = json_decode($this->objects);
        $objeto[0]->Valor = $this->getEntidad();
        $objeto[1]->Valor = $this->conusultatipoincidenciabyidentidad(0);
        $objeto[2]->Valor = $fecha_ini;
        $objeto[3]->Valor = $fecha_fin;
        $objeto[4]->Valor = $namerepor;
        // $objeto[4]->Valor = ["html" => "Tabla HTML"] + $this->formatos;
        $objeto[5]->Valor = ["html" => "Tabla HTML"] + $this->formatos;
        $objeto[6]->Valor = ["pdf" => "PDF"];
        $objeto[7]->Valor = $this->getResponsableEntidad();
        $objeto[8]->Valor = $this->escoja + ["REVISIÓN" => "REVISIÓN", "EN PROCESO" => "EN PROCESO", "REPROGRAMADO" => "REPROGRAMADO", "FINALIZADO" => "FINALIZADO"];
        $objeto[10]->Valor = ["" => "TODOS"] + parroquiaModel::where('estado', 'ACT')->wherenotin('id', [8, 9])->pluck('parroquia', 'parroquia')->all();
        $objeto[11]->Valor = ["" => "TODOS", "A TIEMPO" => "A TIEMPO", "RETRASADA" => "RETRASADA", "CULMINADA" => "CULMINADA"];
        // unset($objeto[10]);
        $funcalljs = "titurepor();";
        $configuraciongeneral = ['Reporte de incidencias territoriales', 'coordinacioncronograma/reporteincidencia'];
        
        return view('reports.create', [
            'objects' => $objeto,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor,
            "funcalljs" => $funcalljs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('coordinacioncronograma::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
