<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Modules\CoordinacionCronograma\Entities\MultasUnaserDetaModel;
use Modules\CoordinacionCronograma\Entities\MultasUnaserModel;
use Modules\CoordinacionCronograma\Entities\MultasUnaserTipoModel;

use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\Log;
use stdClass;

class MultasUnaserController extends Controller
{
    var $configuraciongeneral = array("MULTAS UNASER", "coordinacioncronograma/unaser", "index", 6 => "coordinacioncronograma/unaserajax", 7 => "unaser");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Placa","Nombre":"placa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Cédula","Nombre":"cedula","Clase":"solonumeros cedularuc","Valor":"Null","ValorAnterior" :"Null" },               
        {"Tipo":"text","Descripcion":"Nombre","Nombre":"nombre","Clase":"disabled","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tipo de multa","Nombre":"id_tipo_multa","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },               
        {"Tipo":"text","Descripcion":"Valor de la multa","Nombre":"valor","Clase":"moneda disabled","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetimetext","Descripcion":"Fecha","Nombre":"fecha","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Descripción","Nombre":"descripcion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Calle","Nombre":"calle","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Avenida","Nombre":"avenida","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Boleta","Nombre":"boleta","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Estado de la multa","Nombre":"estado_multa","Clase":"disabled","Valor":"Null","ValorAnterior" :"Null"}
        ]';

    var $validarjs = array(
        "cedula" => "cedula: {
                            required: true
                        }",
        "placa" => "placa: {
                            required: true
                        }",
        "valor" => "valor: {
                            required: true
                        }",
        "fecha" => "fecha: {
                            required: true
                        }",
        "descripcion" => "descripcion: {
                            required: true
                        }"
    );
    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }
    public function perfil($verificar = 'SI')
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $id_tipo_pefil->tipo == 10) { //1 Administrador 4 Coordinador
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';

            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'NO';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        }

        return $object;
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = json_decode($this->objetos);
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "delete" => "si",
            "create" => "si",
            "configuraciongeneral" => $this->configuraciongeneral,
            "permisos" => $this->perfil()
        ]);
    }

    public function formularioscrear($id = "")
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        /*QUITAR CAMPOS*/
        $objetos = json_decode($this->objetos);
        $objetos[3]->Valor = $this->escoja + MultasUnaserTipoModel::select('id', DB::raw('CONCAT(id," - ",tipo) as tipo'))->where('estado', 'ACT')->pluck('tipo', 'id')->all();
        $objetos[1]->Adicional = '<img src="' . asset('img/loading35.gif') . '" hidden  height="50" width="60" class="multas_consulta">';
        $objetos[2]->Adicional = '<img src="' . asset('img/loading35.gif') . '"  hidden height="50" width="60" class="multas_consulta">';

        // dd( $objetos[0]);
        if ($id_tipo_pefil->tipo != 1) {
        }
        unset($objetos[10]);

        $objetosmain = $this->verpermisos($objetos, "crear");

        $datos = array();
        if ($id == "") {
            $this->configuraciongeneral[2] = "crear";


            $datos = [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            ];
        } else {
            $this->configuraciongeneral[2] = "editar";

            // dd($tabla);
            $dicecionesDocumentos = null;
            $dicecionesImagenes = null;

            if ($id != null || $id != "") {
                $id = intval(Crypt::decrypt($id));
                $tabla = $objetosmain[1]->where("una_tmo_multas.id", $id)->first();
                // dd($tabla);
                $objetos[3]->ValorAnterior = $tabla->id_tipo_multa;
                // $objetos[0]->ValorAnterior = $tabla->id_tipo_multa;
            }


            // dd($id);
            return view('vistas.create', [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
                'dicecionesImagenes' => $dicecionesImagenes,
                'dicecionesDocumentos' => $dicecionesDocumentos,
                "permisos" =>  $this->perfil(),
                "timeline" => null
            ]);
        }

        return view('vistas.create', $datos);
    }

    public function verpermisos($objetos = array(), $tipo = "index", $anio = '2019')
    {


        $tabla = MultasUnaserModel::select('tt.tipo', 'una_tmo_multas.*')
            ->join('una_tmo_multas_tipo as tt', 'tt.id', '=', 'una_tmo_multas.id_tipo_multa')->where('una_tmo_multas.estado', 'ACT');

        // show($tabla->get()->toarray());
        return array($objetos, $tabla);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return $this->formularioscrear();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }


    public function guardar($id)
    {
        $input = Input::all();

        $ruta = $this->configuraciongeneral[1];

        $boleta = Input::get('boleta');
        $placa = Input::get('placa');

        if ($id == 0) {
            $multa = MultasUnaserModel::where(['boleta' => $boleta, 'placa' => $placa, 'estado' => 'ACT'])->first();
            // dd($multa);
            if ($multa) {
                $mensaje = 'La placa ' . $placa . ', tiene ya tiene registrada la boleta ' . $boleta . '.';
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
            $ruta .= "/create";
            $guardar = new MultasUnaserModel();
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro de Datos";
        } else {
            $ruta .= "/$id/edit";
            $guardar = MultasUnaserModel::find($id);
            // dd($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Actualización de Datos";
        }

        $input = Input::all();
        // show($input);
        $arrapas = array();

        $validator = Validator::make($input, MultasUnaserModel::rules($id));

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                DB::beginTransaction();
                $timeline = new MultasUnaserDetaModel();
                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token") {
                        $guardar->$key = $value;
                        $timeline->$key = $value;
                    }
                }
                if ($id == 0) {
                    $guardar->id_usuario = Auth::user()->id;
                    $guardar->ip = FacadesRequest::getClientIp();
                    $guardar->pc = FacadesRequest::getHost();
                }

                $guardar->save();

                $idcab = $guardar->id;

                $timeline->id_unaser_cab = $idcab;
                $timeline->id_usuario = Auth::user()->id;
                $timeline->ip = FacadesRequest::getClientIp();
                $timeline->pc = FacadesRequest::getHost();
                $timeline->save();

                $multa = MultasUnaserTipoModel::where('id', $guardar->id_tipo_multa)->first();


                Auditoria($msgauditoria . " - ID: " . $id . "- multa UNASER");
                DB::commit();
                if ($id == 0 || $guardar->estado_multa == 'NO LIQUIDADA') {
                    $liquitacion = $this->generarLiquidacion("FECHA : " . $guardar->fecha . ", MULTA UNASER : " . $multa->tipo . ', PLACA: ' . $guardar->placa . ', VALOR: ' . $guardar->valor . ',   CALLE:' . $guardar->calle . ' AV: ' . $guardar->avenida . ' BOLETA: ' . $guardar->boleta, 246, $guardar->valor, $guardar->cedula, $guardar->nombre, $guardar->id, $guardar->placa, $guardar->fecha);
                    if ($liquitacion['estado'] == 'false') {
                        // $guardar_ = MultasUnaserModel::find($id);
                        // // $guardar_->estado = 'INA';
                        // $guardar_->save();
                        Log::info($liquitacion);
                        return redirect()->back()->withErrors(['No se pudo liquidar'])->withInput();
                    }
                }
            } //Fin Try


            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage() . " - " . $e->getLine();
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }

    public function unaserajax(Request $request)
    {
        $objetos = json_decode($this->objetos);
        $tabla = $this->verpermisos($objetos);
        $objetos = $tabla[0];

        // show($objetos);
        $tabla = $tabla[1];
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('una_tmo_multas.cedula', 'LIKE', "%{$search}%")
                    ->orWhere('una_tmo_multas.nombre', 'LIKE', "%{$search}%")
                    ->orWhere('una_tmo_multas.placa', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            // show($posts);
            foreach ($posts as $post) {
                $permisos = $this->perfil();

                $acciones = '';
                $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

                $edit = '';
                $edit = link_to_route('' . $this->configuraciongeneral[7] . '.edit', ' EDITAR', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o btn btn-light', 'style' => 'color: #fff;background-color: #17a2b8;border-color: #17a2b8'));
                if ($id_tipo_pefil->tipo != 1) {
                    if ($post->estado_multa != 'NO LIQUIDADA') {
                        $edit = '';
                    }
                }
                $acciones = ' ' . $edit;

                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }


                $nestedData['acciones'] = $acciones;
                $nestedData['id_tipo_multa'] = $post->tipo;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

        return redirect()->back();
        ///return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        // return redirect()->back();
        return $this->formularioscrear($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        // $id = intval(Crypt::decrypt($id));

        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }



    public function contribullenteMantaGis($cedula, $nombres)
    {
        $webservice = ConfigSystem("webservice");
        $url_consulta = "http://$webservice/api/Operaciones/ConsultaContribuyenteRentas/" . $cedula;
        $client_c = new GuzzleHttpClient();
        $respuesta_c = $client_c->request('GET', $url_consulta);
        $consultawebservice_c = json_decode($respuesta_c->getBody()->getContents(), true);

        if ($consultawebservice_c[0]['estado'] == 'ok') {
            # code...
            return ['estado' => true, 'mensaje' => $consultawebservice_c[0]['detalle']];
        } else {
            # code...

            $url = "http://$webservice/api/Operaciones/InsertarContribuyente";

            $form_params = [
                "par_tipo_documento_id" => 1,
                "par_documento" => $cedula,
                "par_nombre" => $nombres,
                "par_empresa" => 'SD',
                "par_direccion" => 'SD',
                "par_telefono" => 'SD',
                "par_email" => 'SD',
                "par_fecha_ingreso" => date('Y-m-d'),
                "par_tipo_persona" => 'Natutal',
                "par_UsuarioID" => 18,
                "par_obligado_contabilidad" => 'NO',
                "par_fecha_contabilidad" => date('Y-m-d')
            ];


            $token = GetToken("http://$webservice/api/login/authenticate");

            if ($token[0] == "ok") $token = $token[1];
            else {
                $resul["message"] = "Autenticación Fallida: " . $token[1];
                $resul["statusCode"] = 2;
                Log::info("PlacetoPay 569: " . $resul["message"] . " statusCode: " . $resul["statusCode"]);
                // return $resul;
            }

            $client = new GuzzleHttpClient();
            $respuesta = $client->request('POST', $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => $form_params
            ]);
            $consultawebservice = json_decode($respuesta->getBody()->getContents(), true);


            // Log::info('...........');
            // Log::info($consultawebservice);
            // Log::info('...........');



            if ($consultawebservice[0]['estado'] == 'ok' || $consultawebservice[0]['detalle'] == 'Ya existe el contribuyente') {
                return ['estado' => true, 'mensaje' => $consultawebservice[0]['detalle']];
            } else {
                return ['estado' => false, 'mensaje' => $consultawebservice[0]['detalle']];
            }
        }
    }


    public function generarLiquidacion($detalle_pago, $rubro, $valor, $cedula = null, $nombres = null, $id, $placa, $fecha)
    {


        $user_manta_gis = $this->contribullenteMantaGis($cedula, $nombres);
        // dd($user_manta_gis);


        $resul = array("statusCode" => 2, "message" => "El RequestId no Existe");
        $webservice = ConfigSystem("webservice");
        $pc = request()->getHost();
        $ip = request()->ip();
        // Log::info($user_manta_gis);

        // dd($user_manta_gis);
        if ($user_manta_gis['estado'] == true) {
            # code...
            $url = "http://$webservice/api/Rentas/InsertarLiquidacionFun";
            $form_params = [
                "tipo" => 'rentas',
                "documento" => $cedula,
                "tipo_renta" => 6,
                "valortotal" => floatval($valor),
                "detalle" => $detalle_pago,
                "detallerubros" => $rubro . ',' . floatval($valor),
                "ip" => $ip,
                "pc" => $pc,
                "placa" => $placa,
                "fecha_liquidacion" => date('Ymd', strtotime($fecha))
            ];
            $token = GetToken("http://$webservice/api/login/authenticate");
            Log::info($token);
            if ($token[0] == "ok") $token = $token[1];

            else {
                $resul["message"] = "Autenticación Fallida: " . $token[1];
                $resul["statusCode"] = 2;
                Log::info("Liquidación MantaGis 623: " . $resul["message"] . " statusCode: " . $resul["statusCode"]);
            }
            $client = new GuzzleHttpClient();
            DB::beginTransaction();
            try {
                $respuesta = $client->request('POST', $url, [
                    'headers' => [

                        'Authorization' => 'Bearer ' . $token,
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'form_params' => $form_params
                ]);
                $consultawebservice = json_decode($respuesta->getBody()->getContents(), true);
                Log::info($consultawebservice);
                if ($consultawebservice[0]['estado'] > 0 && $consultawebservice[0]['detalle'] != 'Error la liquidación ya existe y está pendiente de pago.') {

                    $guardar_ = MultasUnaserModel::find($id);
                    $guardar_->mantagis = json_encode($consultawebservice);
                    $guardar_->estado_multa = 'PENDIENTE';
                    $guardar_->save();

                    DB::commit();
                    return ['estado' => 'ok', 'datos' => $consultawebservice];
                } else {
                    return ['estado' => 'false', 'datos' => $consultawebservice];
                }
            } catch (\Exception $e) {

                Log::info("Liquidacion PlacetoPay 656: " . $resul["message"] . " statusCode: " . $resul["statusCode"]);
                DB::rollback();
                $guardar_ = MultasUnaserModel::find($id);
                $guardar_->estado_multa = 'NO LIQUIDADA';
                $guardar_->save();
                return ['estado' => 'false', 'datos' => []];
            }
        } else {
            $guardar_ = MultasUnaserModel::find($id);
            $guardar_->estado_multa = 'NO LIQUIDADA';
            $guardar_->save();
            return ['estado' => 'false', 'datos' => []];
        }
    }


    public function consultar_placa($placa)
    {


        try {
            //code...
            $start_time = time();
            $contador = 0;
            $respuesta = [];
            while ($contador < 4) {
                // dd('ss');
                try {
                    //code...
                    $client = new GuzzleHttpClient();
                    $consulta = $client->request(
                        'GET',
                        'http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral2020/' . $placa . '/2717/PLACA',
                        ['timeout' => 3, 'http_errors' => false]
                    );
                    $respuesta = $consulta->getBody()->getContents();
                    $respuesta = json_decode($respuesta, true);
                    // dd($consulta->getStatusCode());
                } catch (\Throwable $th) {
                    //throw $th;

                }

                if (isset($respuesta[0]['placa'])) {
                    $contador == 4;
                }
                $contador++;
            }
            return response()->json($respuesta);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json($th);
        }
    }
}
