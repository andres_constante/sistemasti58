<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Routing\Controller;
use GuzzleHttp\Client as GuzzleHttpClient;

use Carbon\Carbon;
use App\UsuariosModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CabCompromisoalcaldiaModel;
use Modules\Coordinacioncronograma\Entities\CabRecomendacionContraModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\CoordinacionCronograma\Entities\CoorPoaActividadCabModel;
use Modules\Coordinacioncronograma\Entities\CoorProyectoEstrategicoModel;
use Modules\CoordinacionCronograma\Entities\PlanCampaniaModel;

class DasboardPoaController extends Controller
{
    var $configuraciongeneral = array("", "coordinacioncronograma/dashboardpoa", "index", "coordinacioncronograma/dashboardpoa/poadatos");
    var $escoja = array(0 => "TODOS");
    var $objetos = '[{"Tipo":"select","Descripcion":"Tipo de Filtro","Nombre":"tipofiltro","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';

    var $objetosTable = '[


        {"Tipo":"text","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Avance","Nombre":"avance","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Ultima Modificacion","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }

                ]';




    public function __construct()
    {
        $this->middleware('auth');
    }



    var $general = 'https://portalciudadano.manta.gob.ec/';


    public function resportes_estadisticos(){
        $datos=[
            ['id'=>1,'nombre'=>'POA','icono_app'=>asset('img/app/certificadosolvencia-min.png') ,'ruta'=>URL::to('').'/coordinacioncronograma/poa'],
            ['id'=>2,'nombre'=>'OBRAS','icono_app'=>asset('img/icons/maquina.png'),'ruta'=>URL::to('').'/coordinacioncronograma/obras'],
            ['id'=>3,'nombre'=>'DISPOSICIONES GENERALES','icono_app'=>asset('img/icons/user-laptop.png'),'ruta'=>URL::to('').'/coordinacioncronograma/actividades'],
            ['id'=>4,'nombre'=>'COMPROMISOS TERRITORIALES','icono_app'=>asset('img/icons/compromisos.png'),'ruta'=>URL::to('').'/coordinacioncronograma/compromisos_territoriales'],
            ['id'=>5,'nombre'=>'PLAN DE PROPUESTA','icono_app'=>asset('img/icons/plan.png'),'ruta'=>URL::to('').'/coordinacioncronograma/plan_propuesta'],
            ['id'=>5,'nombre'=>'PROYECTOS ESTRATÉGICOS','icono_app'=>asset('img/icons/estrategico.png'),'ruta'=>URL::to('').'/coordinacioncronograma/proyecto_estrategicos'],
            ['id'=>6,'nombre'=>'RECOMENCAIONES CONTROLARÍA','icono_app'=>asset('img/icons/contraloria.png'),'ruta'=>URL::to('').'/coordinacioncronograma/recomendaciones_contraloria'],
            ['id'=>7,'nombre'=>'MANTAPP','icono_app'=>asset('img/mantapp.png'),'ruta'=>URL::to('').'/coordinacioncronograma/mantapp'],
        
        ];
        $this->configuraciongeneral[0]="REPORTES ESTADÍSTICOS";
        $reportes = array(
            null => "Escoja el reporte",
            "coordinacioncronograma/resportes_estadisticos" => "REPORTES ESTADÍSTICOS",
            // "coordinacioncronograma/reporteincidencia" => "REPORTE DE INCIDENCIAS TERRITORIALES",
            "reportescoordinacionactividades" => "REPORTE DE DISPOSICIONES GENERALES",
            "reportescompromisos" => "REPORTE COMPROMISOS TERRITORIALES",
            "reportespoa" => "REPORTE POA",
            "reportespoacertificaciones" => "REPORTE CERTIFICACIONES POA",
            "reportescoordinacionobras" => "REPORTE DE OBRAS",
            "reportesrecomendaciones" => "REPORTE RECOMENDACIONES DE CONTRALORIA",
            "reportesplanpropuesta" => "REPORTE PLAN DE PROPUESTA",
            "reportesindicadores" => "REPORTE INDICADORES DE GESTION",
        );
        $reportes_objeto = '[
            {"Tipo": "select", "Descripcion": "Tipo de reporte", "Nombre": "reporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Ancho":12}    
        ]';
        $objetos_reportes=json_decode($reportes_objeto);
        $objetos_reportes[0]->Valor=$reportes;

        return view('vistas.reporteschart', ["res" => $datos,"configuraciongeneral" => $this->configuraciongeneral,'objetos_reportes'=>[]]);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////// POA ////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    public function obras()
    {
        $configuraciongeneral = array("REPORTE ESTADÍSTICO DE OBRAS", "coordinacioncronograma/obras", "index", "obrasdatos");

        $objetos = json_decode($this->objetos);
        //show($this->objetos);
        $objetos[0]->Nombre = "tipofiltro";
        $tipoFiltro = array(1 => "DIRECCION", 2 => "ESTADO", 3 => "PRIORIDAD");

        $objetos[0]->Valor = $this->escoja + $tipoFiltro;
        $objetosTable = json_decode($this->objetosTable);
        $objetos = array_values($objetos);
        $delete = 'si';
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
            $delete = null;
        }

        $date = Carbon::now();
        $endDate = $date->subDay(7);
        $fin = $endDate->toDateString();
        $date = Carbon::now();
        $inicio = $date->toDateString();
        $fin2 = date('Y-m-d', strtotime($inicio));
        $inicio2 = date('Y-m-d', strtotime($fin));

        $filtros = '[


                    {"Tipo":"fecha","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"fecha","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                
    
                    ]';

        $filtros = json_decode($filtros);
        // dd($filtros);
        $direciones = CoorObrasModel::join('tmae_direcciones as d', 'd.id', '=', 'coor_tmov_obras_listado.id_direccion')
            ->select(
                'd.id',
                'd.direccion'
            )

            ->where(['coor_tmov_obras_listado.estado' => 'ACT'])
            // ->wherein(1,[])
            ->groupby('d.id')
            ->pluck('direccion', 'id')
            ->all();



        // dd($direciones);


        $filtros[0]->Valor = '2017-01-01';
        $filtros[1]->Valor = date('Y-m-d');
        $filtros[2]->Valor = $direciones;
        $estados = explodewords(ConfigSystem("estadoobras"), "|");
        $filtros[3]->Valor = $estados;


        return view('vistas.dashboard.dasboardpoa', [
            "objetos" => $objetos,
            "estados" => $estados,
            "filtros" => $filtros,
            "objetosTable" => $objetosTable,
            "inicio_date" => $inicio2,
            "inicio_fin" => $fin2,

            "configuraciongeneral" => $configuraciongeneral,
            "create" => 'si',
            "delete" => $delete,
        ]);



        // } catch (\Exception $e) {
        //     return 'No fue posible contactar con https://portalciudadano.manta.gob.ec';
        //     return $e->getMessage();
        // }

    }


    public function obrasdatos()
    {
        $id_direccion = Input::get('direccion');
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');
        $estado = Input::get('estado');
        $detalle = '';

        $actividades_actuales = CoorObrasModel::
        
        join('tmae_direcciones as d', 'd.id', '=', 'coor_tmov_obras_listado.id_direccion')
            ->select(
                'coor_tmov_obras_listado.*',
                'd.direccion',
                'd.alias',
                DB::raw('ifnull(estado_obra,"SIN_ESTADO") as estado_obra')
            )
            ->where(['coor_tmov_obras_listado.estado' => 'ACT'])
            ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin]);


        if ($id_direccion != 0) {

            if ($estado != "0") {
                $detalle = 'Obras en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['coor_tmov_obras_listado.id_direccion' => $id_direccion, 'estado_obra' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->where('coor_tmov_obras_listado.id_direccion', $id_direccion)->get();
                $detalle = 'Obras ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        } else {
            //   dd($estado);
            if ($estado != "0") {
                $detalle = 'Obras en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['estado_obra' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->get();
                $detalle = 'Obras ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        }


        $estados = collect($actividades_actuales_);
        $estados_group = $estados->groupBy('estado_obra');
        $groupCount = $estados_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $doreciones = collect($actividades_actuales_);
        $doreciones_group = $doreciones->groupBy('alias');
        $direcciones_Count = $doreciones_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $direciones_estados = collect($actividades_actuales_);
        $direciones_group = $direciones_estados->groupBy('alias');
        $direcciones_Count_estados = $direciones_group->map(function ($item, $key) {
            $datos = collect($item)->groupBy('estado_obra');
            $groupCount = $datos->map(function ($item, $key) {
                return collect($item)->count();
            });
            return $groupCount;
        });
        // dd($direcciones_Count_estados);

        return ['actividades' => $actividades_actuales_, "estados" => $groupCount, "direciones_poa" => $direcciones_Count->sort(), 'detalle' => $detalle, 'direciones_por_estados' => $direcciones_Count_estados, 'titulo' => 'OBRAS', 'tipo' => 1];
    }

    ////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////// POA ////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    public function poa()
    {
        $configuraciongeneral = array("REPORTE ESTADÍSTICO DE LAS ACTIVIDADES DEL POA", "coordinacioncronograma/dashboardpoa", "index", "poadatos");

        $objetos = json_decode($this->objetos);
        //show($this->objetos);
        $objetos[0]->Nombre = "tipofiltro";
        $tipoFiltro = array(1 => "DIRECCION", 2 => "ESTADO", 3 => "PRIORIDAD");

        $objetos[0]->Valor = $this->escoja + $tipoFiltro;
        $objetosTable = json_decode($this->objetosTable);
        $objetos = array_values($objetos);
        $delete = 'si';
        $tipo_fecha = "Busqueda por Fecha de Última Modificación";
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
            $delete = null;
        }

        $date = Carbon::now();
        $endDate = $date->subDay(7);
        $fin = $endDate->toDateString();
        $date = Carbon::now();
        $inicio = $date->toDateString();
        $fin2 = date('Y-m-d', strtotime($inicio));
        $inicio2 = date('Y-m-d', strtotime($fin));

        $filtros = '[


                    {"Tipo":"fecha","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"fecha","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                
    
                    ]';

        $filtros = json_decode($filtros);
        // dd($filtros);
        $direciones = CoorPoaActividadCabModel::join('tmae_direcciones as d', 'd.id', '=', 'poa_tmae_cab_actividad.id_direccion_act')
            ->select(
                'd.id',
                'd.direccion'
            )

            ->where(['poa_tmae_cab_actividad.estado' => 'ACT'])
            ->groupby('d.id')
            ->pluck('direccion', 'id')
            ->all();



        // dd($direciones);


        $filtros[0]->Valor = '2019-01-01';
        $filtros[1]->Valor = date('Y-m-d');
        $filtros[2]->Valor = $direciones;
        $estados = explodewords(ConfigSystem("estadoobras"), "|");
        $filtros[3]->Valor = $estados;


        return view('vistas.dashboard.dasboardpoa', [
            "objetos" => $objetos,
            "estados" => $estados,
            "filtros" => $filtros,
            "objetosTable" => $objetosTable,
            "inicio_date" => $inicio2,
            "inicio_fin" => $fin2,

            "configuraciongeneral" => $configuraciongeneral,
            "create" => 'si',
            "delete" => $delete,
        ]);



        // } catch (\Exception $e) {
        //     return 'No fue posible contactar con https://portalciudadano.manta.gob.ec';
        //     return $e->getMessage();
        // }

    }

    public function poadatos()
    {
        $id_direccion = Input::get('direccion');
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');
        $estado = Input::get('estado');
        $detalle = '';

        $actividades_actuales = CoorPoaActividadCabModel::
        
        join('tmae_direcciones as d', 'd.id', '=', 'poa_tmae_cab_actividad.id_direccion_act')
        ->join('poa_tmae_cab as act', 'act.id', '=', 'poa_tmae_cab_actividad.id_poa')
            ->select(
                'poa_tmae_cab_actividad.*',
                'd.direccion',
                'd.alias',
                DB::raw("(SELECT sum(monto_solicita) FROM  poa_tmov_autorizacion as xx where xx.id_actividad=poa_tmae_cab_actividad.id) as monto_solicitado"),
                DB::raw('ifnull(estado_actividad,"SIN_ESTADO") as estado_actividad')
            )
            ->where(['poa_tmae_cab_actividad.estado' => 'ACT','act.estado' =>"ACT"])
            ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin]);


        if ($id_direccion != 0) {

            if ($estado != "0") {
                $detalle = 'Actividades en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['poa_tmae_cab_actividad.id_direccion_act' => $id_direccion, 'estado_actividad' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->where('poa_tmae_cab_actividad.id_direccion_act', $id_direccion)->get();
                $detalle = 'Actividades ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        } else {
            //   dd($estado);
            if ($estado != "0") {
                $detalle = 'Actividades en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['estado_actividad' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->get();
                $detalle = 'Actividades ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        }


        $estados = collect($actividades_actuales_);
        $estados_group = $estados->groupBy('estado_actividad');
        $groupCount = $estados_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $doreciones = collect($actividades_actuales_);
        $doreciones_group = $doreciones->groupBy('alias');
        $direcciones_Count = $doreciones_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $direciones_estados = collect($actividades_actuales_);
        $direciones_group = $direciones_estados->groupBy('alias');
        $direcciones_Count_estados = $direciones_group->map(function ($item, $key) {
            $datos = collect($item)->groupBy('estado_actividad');
            $groupCount = $datos->map(function ($item, $key) {
                return collect($item)->count();
            });
            return $groupCount;
        });
        // dd($direcciones_Count_estados);

        return ['actividades' => $actividades_actuales_, "estados" => $groupCount, "direciones_poa" => $direcciones_Count->sort(), 'detalle' => $detalle, 'direciones_por_estados' => $direcciones_Count_estados, 'titulo' => 'Actividades del POA', 'tipo' => 1];
    }


    /////////////////////////////////////////// ACTIVIDADES ///////////////////////////////////////////////////



    ////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////// actividades ////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    public function actividades()
    {
        $configuraciongeneral = array("REPORTE ESTADÍSTICO DE DISPOSICIONES GENERALES", "coordinacioncronograma/actividades", "index", "actividadesdatos");

        $objetos = json_decode($this->objetos);
        //show($this->objetos);
        $objetos[0]->Nombre = "tipofiltro";
        $tipoFiltro = array(1 => "DIRECCION", 2 => "ESTADO", 3 => "PRIORIDAD");

        $objetos[0]->Valor = $this->escoja + $tipoFiltro;
        $objetosTable = json_decode($this->objetosTable);
        $objetos = array_values($objetos);
        $delete = 'si';
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
            $delete = null;
        }

        $date = Carbon::now();
        $endDate = $date->subDay(7);
        $fin = $endDate->toDateString();
        $date = Carbon::now();
        $inicio = $date->toDateString();
        $fin2 = date('Y-m-d', strtotime($inicio));
        $inicio2 = date('Y-m-d', strtotime($fin));

        $filtros = '[


                    {"Tipo":"fecha","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"fecha","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                
    
                    ]';

        $filtros = json_decode($filtros);
        // dd($filtros);
        $direciones = CoorCronogramaCabModel::select(DB::raw("dir.id,direccion"))
            ->join("tmae_direcciones as dir", "dir.id", "=", "coor_tmov_cronograma_cab.id_direccion")
            ->where([["coor_tmov_cronograma_cab.estado", "ACT"]])
            ->groupBy("coor_tmov_cronograma_cab.id_direccion")
            ->orderby("direccion", "asc")
            ->pluck('direccion', 'id')
            ->all();



        // dd($direciones);


        $filtros[0]->Valor = '2019-01-01';
        $filtros[1]->Valor = date('Y-m-d');
        $filtros[2]->Valor = $direciones;
        $estados = explodewords(ConfigSystem("estadoobras"), "|");
        $filtros[3]->Valor = $estados;


        return view('vistas.dashboard.dasboardpoa', [
            "objetos" => $objetos,
            "estados" => $estados,
            "filtros" => $filtros,
            "objetosTable" => $objetosTable,
            "inicio_date" => $inicio2,
            "inicio_fin" => $fin2,

            "configuraciongeneral" => $configuraciongeneral,
            "create" => 'si',
            "delete" => $delete,
        ]);



        // } catch (\Exception $e) {
        //     return 'No fue posible contactar con https://portalciudadano.manta.gob.ec';
        //     return $e->getMessage();
        // }

    }



    public function actividadesdatos()
    {
        $id_direccion = Input::get('direccion');
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');
        $estado = Input::get('estado');
        $detalle = '';

        $actividades_actuales = CoorCronogramaCabModel::join('tmae_direcciones as d', 'd.id', '=', 'coor_tmov_cronograma_cab.id_direccion')
            ->select(
                'coor_tmov_cronograma_cab.*',
                'd.direccion',
                'd.alias',
                DB::raw('ifnull(estado_actividad,"SIN_ESTADO") as estado_actividad')
            )->where(["coor_tmov_cronograma_cab.estado" => "ACT"])
            ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin]);


        if ($id_direccion != 0) {

            if ($estado != "0") {
                $detalle = 'Disposiciones Generales en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['coor_tmov_cronograma_cab.id_direccion' => $id_direccion, 'estado_actividad' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->where('coor_tmov_cronograma_cab.id_direccion', $id_direccion)->get();
                $detalle = 'Disposiciones Generales ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        } else {
            //   dd($estado);
            if ($estado != "0") {
                $detalle = 'Disposiciones Generales en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['estado_actividad' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->get();
                $detalle = 'Disposiciones Generales ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        }


        // return $actividades_actuales_;
        $estados = collect($actividades_actuales_);
        $estados_group = $estados->groupBy('estado_actividad');
        $groupCount = $estados_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $doreciones = collect($actividades_actuales_);
        $doreciones_group = $doreciones->groupBy('alias');
        $direcciones_Count = $doreciones_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $direciones_estados = collect($actividades_actuales_);
        $direciones_group = $direciones_estados->groupBy('alias');
        $direcciones_Count_estados = $direciones_group->map(function ($item, $key) {
            $datos = collect($item)->groupBy('estado_actividad');
            $groupCount = $datos->map(function ($item, $key) {
                return collect($item)->count();
            });
            return $groupCount;
        });
        // dd($direcciones_Count_estados);

        return ['actividades' => $actividades_actuales_, "estados" => $groupCount, "direciones_poa" => $direcciones_Count->sort(), 'detalle' => $detalle, 'direciones_por_estados' => $direcciones_Count_estados, 'titulo' => 'Actividades', 'tipo' => 2];
    }
    ////////////////////////////////////////////////////////////////////////////////
    //////////////////////////// Compromisos Territoriales /////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    public function compromisos_territoriales()
    {
        $configuraciongeneral = array("REPORTE ESTADÍSTICO COMPROMISOS TERRITORIALES", "coordinacioncronograma/compromisos_territoriales", "index", "compromisos_territoriales_datos");

        $objetos = json_decode($this->objetos);
        //show($this->objetos);
        $objetos[0]->Nombre = "tipofiltro";
        $tipoFiltro = array(1 => "DIRECCION", 2 => "ESTADO", 3 => "PRIORIDAD");

        $objetos[0]->Valor = $this->escoja + $tipoFiltro;
        $objetosTable = json_decode($this->objetosTable);
        $objetos = array_values($objetos);
        $delete = 'si';
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
            $delete = null;
        }

        $date = Carbon::now();
        $endDate = $date->subDay(7);
        $fin = $endDate->toDateString();
        $date = Carbon::now();
        $inicio = $date->toDateString();
        $fin2 = date('Y-m-d', strtotime($inicio));
        $inicio2 = date('Y-m-d', strtotime($fin));

        $filtros = '[


                    {"Tipo":"fecha","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"fecha","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                
    
                    ]';

        $filtros = json_decode($filtros);
        // dd($filtros);
        $direciones = CabCompromisoalcaldiaModel::select(DB::raw("dir.id,direccion"))
            ->join("tmae_direcciones as dir", "dir.id", "=", "coor_tmov_compromisos_cab.id_direccion")
            ->where([["coor_tmov_compromisos_cab.estado", "ACT"]])
            ->groupBy("coor_tmov_compromisos_cab.id_direccion")
            ->orderby("direccion", "asc")
            ->pluck('direccion', 'id')
            ->all();



        // dd($direciones);


        $filtros[0]->Valor = '2019-01-01';
        $filtros[1]->Valor = date('Y-m-d');
        $filtros[2]->Valor = $direciones;
        $estados = explodewords(ConfigSystem("estadoobras"), "|");
        $filtros[3]->Valor = $estados;


        return view('vistas.dashboard.dasboardpoa', [
            "objetos" => $objetos,
            "estados" => $estados,
            "filtros" => $filtros,
            "objetosTable" => $objetosTable,
            "inicio_date" => $inicio2,
            "inicio_fin" => $fin2,

            "configuraciongeneral" => $configuraciongeneral,
            "create" => 'si',
            "delete" => $delete,
        ]);



        // } catch (\Exception $e) {
        //     return 'No fue posible contactar con https://portalciudadano.manta.gob.ec';
        //     return $e->getMessage();
        // }

    }



    public function compromisos_territoriales_datos()
    {
        $id_direccion = Input::get('direccion');
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');
        $estado = Input::get('estado');
        $detalle = '';

        $actividades_actuales = CabCompromisoalcaldiaModel::join('tmae_direcciones as d', 'd.id', '=', 'coor_tmov_compromisos_cab.id_direccion')
            ->select(
                'coor_tmov_compromisos_cab.*',
                'd.direccion',
                'd.alias',
                DB::raw('ifnull(estado_actividad,"SIN_ESTADO") as estado_actividad')
            )->where(["coor_tmov_compromisos_cab.estado" => "ACT"])
            ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin]);


        if ($id_direccion != 0) {

            if ($estado != "0") {
                $detalle = 'Compromisos territoriales en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['coor_tmov_compromisos_cab.id_direccion' => $id_direccion, 'estado_actividad' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->where('coor_tmov_compromisos_cab.id_direccion', $id_direccion)->get();
                $detalle = 'Compromisos territoriales ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        } else {
            //   dd($estado);
            if ($estado != "0") {
                $detalle = 'Compromisos territoriales en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['estado_actividad' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->get();
                $detalle = 'Compromisos territoriales ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        }


        // return $actividades_actuales_;
        $estados = collect($actividades_actuales_);
        $estados_group = $estados->groupBy('estado_actividad');
        $groupCount = $estados_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $doreciones = collect($actividades_actuales_);
        $doreciones_group = $doreciones->groupBy('alias');
        $direcciones_Count = $doreciones_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $direciones_estados = collect($actividades_actuales_);
        $direciones_group = $direciones_estados->groupBy('alias');
        $direcciones_Count_estados = $direciones_group->map(function ($item, $key) {
            $datos = collect($item)->groupBy('estado_actividad');
            $groupCount = $datos->map(function ($item, $key) {
                return collect($item)->count();
            });
            return $groupCount;
        });
        // dd($direcciones_Count_estados);

        return ['actividades' => $actividades_actuales_, "estados" => $groupCount, "direciones_poa" => $direcciones_Count->sort(), 'detalle' => $detalle, 'direciones_por_estados' => $direcciones_Count_estados, 'titulo' => 'Compromisos territoriales', 'tipo' => 3];
    }

    ////////////////////////////////////////////////////////////////////////////////
    //////////////////////////// recomendaciones_contraloria /////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    public function recomendaciones_contraloria()
    {
        $configuraciongeneral = array("REPORTE ESTADÍSTICO RECOMENDACIONES CONTRALORÍA", "coordinacioncronograma/recomendaciones_contraloria", "index", "recomendaciones_contraloria_datos");

        $objetos = json_decode($this->objetos);
        //show($this->objetos);
        $objetos[0]->Nombre = "tipofiltro";
        $tipoFiltro = array(1 => "DIRECCION", 2 => "ESTADO", 3 => "PRIORIDAD");

        $objetos[0]->Valor = $this->escoja + $tipoFiltro;
        $objetosTable = json_decode($this->objetosTable);
        $objetos = array_values($objetos);
        $delete = 'si';
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
            $delete = null;
        }

        $date = Carbon::now();
        $endDate = $date->subDay(7);
        $fin = $endDate->toDateString();
        $date = Carbon::now();
        $inicio = $date->toDateString();
        $fin2 = date('Y-m-d', strtotime($inicio));
        $inicio2 = date('Y-m-d', strtotime($fin));

        $filtros = '[


                    {"Tipo":"fecha","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"fecha","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                
    
                    ]';

        $filtros = json_decode($filtros);
        // dd($filtros);
        $direciones = CabRecomendacionContraModel::select(DB::raw("dir.id,direccion"))
            ->join("tmae_direcciones as dir", "dir.id", "=", "coor_tmae_recomendacion_contraloria.id_direccion")
            ->where([["coor_tmae_recomendacion_contraloria.estado", "ACT"]])
            ->groupBy("coor_tmae_recomendacion_contraloria.id_direccion")
            ->orderby("direccion", "asc")
            ->pluck('direccion', 'id')
            ->all();

        $filtros[0]->Valor = '2019-01-01';
        $filtros[1]->Valor = date('Y-m-d');
        $filtros[2]->Valor = $direciones;
        $estados = explodewords(ConfigSystem("estadoobras"), "|");
        $filtros[3]->Valor = $estados;


        return view('vistas.dashboard.dasboardpoa', [
            "objetos" => $objetos,
            "estados" => $estados,
            "filtros" => $filtros,
            "objetosTable" => $objetosTable,
            "inicio_date" => $inicio2,
            "inicio_fin" => $fin2,

            "configuraciongeneral" => $configuraciongeneral,
            "create" => 'si',
            "delete" => $delete,
        ]);



        // } catch (\Exception $e) {
        //     return 'No fue posible contactar con https://portalciudadano.manta.gob.ec';
        //     return $e->getMessage();
        // }

    }



    public function recomendaciones_contraloria_datos()
    {
        $id_direccion = Input::get('direccion');
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');
        $estado = Input::get('estado');
        $detalle = '';

        $actividades_actuales = CabRecomendacionContraModel::join('tmae_direcciones as d', 'd.id', '=', 'coor_tmae_recomendacion_contraloria.id_direccion')
            ->select(
                'coor_tmae_recomendacion_contraloria.*',
                'd.direccion',
                'd.alias',
                DB::raw('ifnull(estado_proceso,"SIN_ESTADO") as estado_proceso')
            )->where(["coor_tmae_recomendacion_contraloria.estado" => "ACT"])
            ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin]);


        if ($id_direccion != 0) {

            if ($estado != "0") {
                $detalle = 'Recomendaciones de controlaría en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['coor_tmae_recomendacion_contraloria.id_direccion' => $id_direccion, 'estado_proceso' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->where('coor_tmae_recomendacion_contraloria.id_direccion', $id_direccion)->get();
                $detalle = 'Recomendaciones de controlaría' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        } else {
            //   dd($estado);
            if ($estado != "0") {
                $detalle = 'Recomendaciones de controlaría en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['estado_proceso' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->get();
                $detalle = 'Recomendaciones de controlaría  ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        }


        // return $actividades_actuales_;
        $estados = collect($actividades_actuales_);
        $estados_group = $estados->groupBy('estado_proceso');
        $groupCount = $estados_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $doreciones = collect($actividades_actuales_);
        $doreciones_group = $doreciones->groupBy('alias');
        $direcciones_Count = $doreciones_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $direciones_estados = collect($actividades_actuales_);
        $direciones_group = $direciones_estados->groupBy('alias');
        $direcciones_Count_estados = $direciones_group->map(function ($item, $key) {
            $datos = collect($item)->groupBy('estado_proceso');
            $groupCount = $datos->map(function ($item, $key) {
                return collect($item)->count();
            });
            return $groupCount;
        });
        // dd($direcciones_Count_estados);

        return ['actividades' => $actividades_actuales_, "estados" => $groupCount, "direciones_poa" => $direcciones_Count->sort(), 'detalle' => $detalle, 'direciones_por_estados' => $direcciones_Count_estados, 'titulo' => 'Recomendaciones de controlaria', 'tipo' => 4];
    }
    ////////////////////////////////////////////////////////////////////////////////
    //////////////////////////// plan de campaña /////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    public function plan_campana()
    {
        $configuraciongeneral = array("REPORTE ESTADÍSTICO PLAN DE PROPUESTA", "coordinacioncronograma/plan_propuesta", "index", "plan_campana_datos");

        $objetos = json_decode($this->objetos);
        //show($this->objetos);
        $objetos[0]->Nombre = "tipofiltro";
        $tipoFiltro = array(1 => "DIRECCION", 2 => "ESTADO", 3 => "PRIORIDAD");

        $objetos[0]->Valor = $this->escoja + $tipoFiltro;
        $objetosTable = json_decode($this->objetosTable);
        $objetos = array_values($objetos);
        $delete = 'si';
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
            $delete = null;
        }

        $date = Carbon::now();
        $endDate = $date->subDay(7);
        $fin = $endDate->toDateString();
        $date = Carbon::now();
        $inicio = $date->toDateString();
        $fin2 = date('Y-m-d', strtotime($inicio));
        $inicio2 = date('Y-m-d', strtotime($fin));

        $filtros = '[


                    {"Tipo":"fecha","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"fecha","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                
    
                    ]';

        $filtros = json_decode($filtros);
        // dd($filtros);
        $direciones = PlanCampaniaModel::select(DB::raw("dir.id,direccion"))
            ->join("tmae_direcciones as dir", "dir.id", "=", "plan_tmov_cab_plancampania.id_direccion")
            ->where([["plan_tmov_cab_plancampania.estado", "ACT"]])
            ->groupBy("plan_tmov_cab_plancampania.id_direccion")
            ->orderby("direccion", "asc")
            ->pluck('direccion', 'id')
            ->all();

        $filtros[0]->Valor = '2019-01-01';
        $filtros[1]->Valor = date('Y-m-d');
        $filtros[2]->Valor = $direciones;
        $estados = explodewords(ConfigSystem("estadoobras"), "|");
        $filtros[3]->Valor = $estados;


        return view('vistas.dashboard.dasboardpoa', [
            "objetos" => $objetos,
            "estados" => $estados,
            "filtros" => $filtros,
            "objetosTable" => $objetosTable,
            "inicio_date" => $inicio2,
            "inicio_fin" => $fin2,

            "configuraciongeneral" => $configuraciongeneral,
            "create" => 'si',
            "delete" => $delete,
        ]);



        // } catch (\Exception $e) {
        //     return 'No fue posible contactar con https://portalciudadano.manta.gob.ec';
        //     return $e->getMessage();
        // }

    }



    public function plan_campana_datos()
    {
        $id_direccion = Input::get('direccion');
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');
        $estado = Input::get('estado');
        $detalle = '';

        $actividades_actuales = PlanCampaniaModel::join('tmae_direcciones as d', 'd.id', '=', 'plan_tmov_cab_plancampania.id_direccion')
            ->select(
                'plan_tmov_cab_plancampania.*',
                'd.direccion',
                'd.alias',
                DB::raw('ifnull(estado_plan,"SIN_ESTADO") as estado_plan')
            )->where(["plan_tmov_cab_plancampania.estado" => "ACT"])
            ->whereBetween('fecha_inicio', [$fecha_inicio, $fecha_fin]);


        if ($id_direccion != 0) {

            if ($estado != "0") {
                $detalle = 'Actividades del Plan de propuesta en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['plan_tmov_cab_plancampania.id_direccion' => $id_direccion, 'estado_plan' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->where('plan_tmov_cab_plancampania.id_direccion', $id_direccion)->get();
                $detalle = 'Actividades ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        } else {
            //   dd($estado);
            if ($estado != "0") {
                $detalle = 'Actividades del Plan de propuesta en estado <b>' . $estado . '</b> desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
                $actividades_actuales_ = $actividades_actuales->where(['estado_plan' => $estado])->get();
            } else {
                $actividades_actuales_ = $actividades_actuales->get();
                $detalle = 'Actividades del Plan de propuesta ' . ' desde ' . $fecha_inicio . ' hasta ' . $fecha_fin;
            }
        }


        // return $actividades_actuales_;
        $estados = collect($actividades_actuales_);
        $estados_group = $estados->groupBy('estado_plan');
        $groupCount = $estados_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $doreciones = collect($actividades_actuales_);
        $doreciones_group = $doreciones->groupBy('alias');
        $direcciones_Count = $doreciones_group->map(function ($item, $key) {
            return collect($item)->count();
        });
        $direciones_estados = collect($actividades_actuales_);
        $direciones_group = $direciones_estados->groupBy('alias');
        $direcciones_Count_estados = $direciones_group->map(function ($item, $key) {
            $datos = collect($item)->groupBy('estado_plan');
            $groupCount = $datos->map(function ($item, $key) {
                return collect($item)->count();
            });
            return $groupCount;
        });
        // dd($direcciones_Count_estados);

        return ['actividades' => $actividades_actuales_, "estados" => $groupCount, "direciones_poa" => $direcciones_Count->sort(), 'detalle' => $detalle, 'direciones_por_estados' => $direcciones_Count_estados, 'titulo' => 'Plan de propuesta', 'tipo' => 5];
    }
    ////////////////////////////////////////////////////////////////////////////////
    //////////////////////////// proyecto estrategicos /////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    public function proyecto_estrategicos()
    {
        $configuraciongeneral = array("REPORTE ESTADÍSTICO PROYECTOS ESTRATÉGICOS", "coordinacioncronograma/proyecto_estrategicos", "index", "proyecto_estrategicos_datos");

        $objetos = json_decode($this->objetos);
        //show($this->objetos);
        $objetos[0]->Nombre = "tipofiltro";
        $tipoFiltro = array(1 => "DIRECCION", 2 => "ESTADO", 3 => "PRIORIDAD");

        $objetos[0]->Valor = $this->escoja + $tipoFiltro;
        $objetosTable = json_decode($this->objetosTable);
        $objetos = array_values($objetos);
        $delete = 'si';
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
            $delete = null;
        }

        $date = Carbon::now();
        $endDate = $date->subDay(7);
        $fin = $endDate->toDateString();
        $date = Carbon::now();
        $inicio = $date->toDateString();
        $fin2 = date('Y-m-d', strtotime($inicio));
        $inicio2 = date('Y-m-d', strtotime($fin));

        $filtros = '[
                    {"Tipo":"select","Descripcion":"Fase","Nombre":"fase","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                
    
                    ]';

        $filtros = json_decode($filtros);
        $direciones = CoorProyectoEstrategicoModel::where('estado', 'ACT')->get();
        $datos = collect($direciones);
        $datos_collecte = $datos->map(function ($item, $key) {
            $direcciones = str_replace('|', ',', $item->responsables);
            if ($direcciones) {
                $direcciones_ = DB::select('select id ,direccion from tmae_direcciones where id in (' . $direcciones . ') ');
                return $direcciones_;
            }
        })->filter(function ($user, $key) {
            return $user != null;
        });

        $direcciones_select = [];
        foreach ($datos_collecte as $key => $value) {
            # code...datos_collecte
            // dd($value);
            foreach ($value as $key_ => $value_) {
                # code...
                if (!in_array($value_->id, $direcciones_select)) {
                    // dd(array_key_exists($value_->id, $direcciones_select));
                    array_push($direcciones_select, $value_->id);
                }
            }
        }

        $direciones = direccionesModel::whereIn('id', $direcciones_select)->pluck('direccion', 'id')->all();


        // dd($direciones);
        $filtros[0]->Valor = [
            '2020' => '2020-RUPTURA',
            '2023' => '2023-FORTALECIMIENTO',
            '2030' => '2030-CONSOLIDACION',
            '2035' => '2035-DIVERSIFICACION',
        ];
        // $filtros[1]->Valor = date('Y-m-d');
        $filtros[1]->Valor = $direciones;
        $estados = explodewords(ConfigSystem("estadoproyecto"), "|");
        $filtros[2]->Valor = $estados;

        // dd($estados);


        return view('vistas.dashboard.dasboardpoa', [
            "objetos" => $objetos,
            "estados" => $estados,
            "filtros" => $filtros,
            "objetosTable" => $objetosTable,
            "inicio_date" => $inicio2,
            "inicio_fin" => $fin2,

            "configuraciongeneral" => $configuraciongeneral,
            "create" => 'si',
            "delete" => $delete,
        ]);



        // } catch (\Exception $e) {
        //     return 'No fue posible contactar con https://portalciudadano.manta.gob.ec';
        //     return $e->getMessage();
        // }

    }



    public function proyecto_estrategicos_datos()
    {
        $id_direccion = Input::get('direccion');
        $fase = Input::get('fase');
        $estado = Input::get('estado');
        $detalle = '';


        $actividades_actuales = CoorProyectoEstrategicoModel::
            //join('tmae_direcciones as d', 'd.id', '=', 'plan_tmov_cab_plancampania.id_direccion')
            select(
                'coor_tmov_proyecto_estrategico.*',
                // 'd.direccion',
                // 'd.alias',
                DB::raw('ifnull(estado_proyecto,"SIN_ESTADO") as estado_proyecto')
            )->where(["coor_tmov_proyecto_estrategico.estado" => "ACT"]);
        if ($fase) {
            $actividades_actuales = CoorProyectoEstrategicoModel::
                //join('tmae_direcciones as d', 'd.id', '=', 'plan_tmov_cab_plancampania.id_direccion')
                select(
                    'coor_tmov_proyecto_estrategico.*',
                    // 'd.direccion',
                    // 'd.alias',
                    DB::raw('ifnull(estado_proyecto,"SIN_ESTADO") as estado_proyecto')
                )->where(["coor_tmov_proyecto_estrategico.estado" => "ACT", 'fase' => $fase]);
        }

        if ($id_direccion != 0) {

            if ($estado != "0") {

                $actividades_actuales_ = $actividades_actuales->where(['estado_proyecto' => $estado])->get();
                $datos = collect($actividades_actuales_);
                // dd($datos);
                $datos_collecte = $datos->map(function ($item, $key) use ($id_direccion) {

                    $direcciones = str_replace('|', ',', $item->responsables);
                    if ($direcciones) {
                        $direcciones_ = DB::select('select id ,direccion,alias, \'' . $item->estado_proyecto . '\' as estado_proyecto from tmae_direcciones where id in (' . $direcciones . ') ');
                        $direcciones_colect = collect($direcciones_);
                        $direcciones_colect_ = collect($direcciones_colect->where('id', $id_direccion));
                        // dd($direcciones_colect_->all());
                        if (isset($direcciones_colect_[0])) {
                            return collect($direcciones_colect_)->first();
                        }
                    }
                })->filter(function ($user, $key) {
                    return $user != null;
                });
                $actividades_actuales_ = $datos_collecte;
                $detalle = 'Proyecto estratégicos ' . $estado;
            } else {

                $datos = collect($actividades_actuales->get());
                $datos_collecte = $datos->map(function ($item, $key) use ($id_direccion) {
                    $direcciones = str_replace('|', ',', $item->responsables);
                    if ($direcciones) {
                        $direcciones_ = DB::select('select id ,direccion,alias, \'' . $item->estado_proyecto . '\' as estado_proyecto from tmae_direcciones where id in (' . $direcciones . ') ');
                        $direcciones_colect = collect($direcciones_);
                        $direcciones_colect_ = collect($direcciones_colect->where('id', $id_direccion));
                        // dd($direcciones_colect_->all());
                        if (isset($direcciones_colect_[0])) {
                            return collect($direcciones_colect_)->first();
                        }
                    }
                })->filter(function ($user, $key) {
                    return $user != null;
                });
                // dd(collect($    ));
                $actividades_actuales_ = $datos_collecte;
                $detalle = 'Proyecto estratégicos ' . $estado;
            }
        } else {
            //   dd($estado);
            $array_direciones = [];
            if ($estado != "0") {
                if ($fase != '0') {
                    $detalle = 'Proyecto estratégicos en estado <b>' . $estado . '</b> fase ' . $fase;
                } else {
                    $detalle = 'Proyecto estratégicos en estado <b>' . $estado . '</b>';
                }
                $actividades_actuales_ = $actividades_actuales->where(['estado_proyecto' => $estado])->get();


                $datos = collect($actividades_actuales_);

                foreach ($datos as $key => $value) {
                    # code...
                    // dd($value);
                    $direcciones = str_replace('|', ',', $value->responsables);
                    if ($direcciones) {
                        $direcciones_ = DB::select('select id ,direccion,alias, \'' . $value->estado_proyecto . '\' as estado_proyecto from tmae_direcciones where id in (' . $direcciones . ') ');
                        foreach ($direcciones_ as $k => $v) {
                            $array_direciones = array_merge($array_direciones, [$v]);
                            // dd($array_direciones);
                            # code...
                        }
                    }
                }
                $actividades_actuales_ = collect($array_direciones);
            } else {

                $datos = collect($actividades_actuales->get());

                foreach ($datos as $key => $value) {
                    # code...
                    $direcciones = str_replace('|', ',', $value->responsables);
                    if ($direcciones) {
                        $direcciones_ = DB::select('select id ,direccion,alias, \'' . $value->estado_proyecto . '\' as estado_proyecto from tmae_direcciones where id in (' . $direcciones . ') ');
                        foreach ($direcciones_ as $k => $v) {
                            $array_direciones = array_merge($array_direciones, [$v]);
                            // dd($array_direciones);
                            # code...
                        }
                    }
                }
                // dd();

                $actividades_actuales_ = collect($array_direciones);

                if ($fase != '0') {

                    $detalle = 'Proyecto estratégicos ' . ' fase ' . $fase;
                } else {
                    $detalle = 'Proyecto estratégicos';
                }
            }
        }


        // return $actividades_actuales_;
        $estados = collect($actividades_actuales_);
        $estados_group = $estados->groupBy('estado_proyecto');
        $groupCount = $estados_group->map(function ($item, $key) {
            return collect($item)->count();
        });

        $doreciones = collect($actividades_actuales_);
        $doreciones_group = $doreciones->groupBy('alias');
        $direcciones_Count = $doreciones_group->map(function ($item, $key) {
            return collect($item)->count();
        });


        $direciones_estados = collect($actividades_actuales_);
        $direciones_group = $direciones_estados->groupBy('alias');
        $direcciones_Count_estados = $direciones_group->map(function ($item, $key) {
            $datos = collect($item)->groupBy('estado_proyecto');
            $groupCount = $datos->map(function ($item, $key) {
                return collect($item)->count();
            });
            return $groupCount;
        });
        // dd($direciones_group);

        return ['actividades' => $actividades_actuales_, "estados" => $groupCount, "direciones_poa" => $direcciones_Count->sort(), 'detalle' => $detalle, 'direciones_por_estados' => $direcciones_Count_estados, 'titulo' => 'Proyectos estrategicos', 'tipo' => 6];
    }



    public function detalledasboard()
    {
        $tipo = Input::get('tipo');
        $estado = Input::get('estado');
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');
        $fase = Input::get('fase');
        $id_direccion = Input::get('direccion');
        $categoria = Input::get('categoria');
        $tipo_grafico = Input::get('tipo_grafico');
        // dd(Input::all());
        $columnas = [];
        $datos = [];
        switch ($tipo) {
            case 1:
                # code...
                // dd($categoria.$id_direccion);
                $datos = DB::select("select act.id_poa,act.id, d.alias, act.actividad,ifnull(estado_actividad,\"SIN_ESTADO\") as estado_actividad 
                            from poa_tmae_cab_actividad  as act
                            inner join tmae_direcciones as d on d.id = act.id_direccion_act 
                            inner join poa_tmae_cab as poa on poa.id = act.id_poa
                            where 
                                act.estado_actividad LIKE case when '" . $estado . "'='0' then '%' else  '" . $estado . "' end
                                AND act.id_direccion_act LIKE case when '" . $id_direccion . "'='0' then '%' else  '" . $id_direccion . "' end
                                AND act.fecha_inicio BETWEEN  '" . $fecha_inicio . " 00:00:00' AND '" . $fecha_fin . " 23:59:59'
                              and
                              act.estado = 'ACT' and poa.estado='ACT'");

                if ($tipo_grafico == 'direccion') {
                    $datos = collect($datos)->where('alias', $categoria)->all();
                } else {
                    $datos = collect($datos)->where('estado_actividad', $categoria)->all();
                }
                $columnas = ['POA', 'ID ACTIVIDAD', 'DIRECION', 'DESCRIPCIÓN', 'ESTADO'];
                break;
            case 2:
                # code...
                // dd($categoria.$id_direccion);
                $datos = DB::select("select  act.id, d.alias, act.actividad,ifnull(estado_actividad,\"SIN_ESTADO\") as estado_actividad 
                            from coor_tmov_cronograma_cab  as act
                            inner join tmae_direcciones as d on d.id = act.id_direccion
                            where 
                                act.estado_actividad LIKE case when '" . $estado . "'='0' then '%' else  '" . $estado . "' end
                                AND act.id_direccion LIKE case when '" . $id_direccion . "'='0' then '%' else  '" . $id_direccion . "' end
                                AND act.fecha_inicio BETWEEN  '" . $fecha_inicio . " 00:00:00' AND '" . $fecha_fin . " 23:59:59'
                              and
                              act.estado = 'ACT'");

                if ($tipo_grafico == 'direccion') {
                    $datos = collect($datos)->where('alias', $categoria)->all();
                } else {
                    $datos = collect($datos)->where('estado_actividad', $categoria)->all();
                }
                $columnas = ['ID ACTIVIDAD', 'DIRECION', 'DESCRIPCIÓN', 'ESTADO'];
                break;
            case 3:
                # code...
                // dd($categoria.$id_direccion);
                $datos = DB::select("select  act.id, d.alias, act.actividad,ifnull(estado_actividad,\"SIN_ESTADO\") as estado_actividad 
                            from coor_tmov_compromisos_cab  as act
                            inner join tmae_direcciones as d on d.id = act.id_direccion
                            where 
                                act.estado_actividad LIKE case when '" . $estado . "'='0' then '%' else  '" . $estado . "' end
                                AND act.id_direccion LIKE case when '" . $id_direccion . "'='0' then '%' else  '" . $id_direccion . "' end
                                AND act.fecha_inicio BETWEEN  '" . $fecha_inicio . " 00:00:00' AND '" . $fecha_fin . " 23:59:59'
                              and
                              act.estado = 'ACT'");

                if ($tipo_grafico == 'direccion') {
                    $datos = collect($datos)->where('alias', $categoria)->all();
                } else {
                    $datos = collect($datos)->where('estado_actividad', $categoria)->all();
                }
                $columnas = ['ID ACTIVIDAD', 'DIRECION', 'DESCRIPCIÓN', 'ESTADO'];
                break;
            case 4:
                # code...
                // dd($categoria.$id_direccion);
                $datos = DB::select("select  act.id, d.alias, act.actividad,ifnull(estado_proceso,\"SIN_ESTADO\") as estado_proceso 
                            from coor_tmae_recomendacion_contraloria  as act
                            inner join tmae_direcciones as d on d.id = act.id_direccion
                            where 
                                act.estado_proceso LIKE case when '" . $estado . "'='0' then '%' else  '" . $estado . "' end
                                AND act.id_direccion LIKE case when '" . $id_direccion . "'='0' then '%' else  '" . $id_direccion . "' end
                                AND act.fecha_inicio BETWEEN  '" . $fecha_inicio . " 00:00:00' AND '" . $fecha_fin . " 23:59:59'
                              and
                              act.estado = 'ACT'");

                if ($tipo_grafico == 'direccion') {
                    $datos = collect($datos)->where('alias', $categoria)->all();
                } else {
                    $datos = collect($datos)->where('estado_proceso', $categoria)->all();
                }
                $columnas = ['ID RECOMENDACIÓN', 'DIRECION', 'DESCRIPCIÓN', 'ESTADO'];
                break;
            case 5:
                # code...
                // dd($categoria.$id_direccion);
                $datos = DB::select("select  act.id, d.alias, act.actividades_propuestas,ifnull(estado_plan,\"SIN_ESTADO\") as estado_plan 
                            from plan_tmov_cab_plancampania  as act
                            inner join tmae_direcciones as d on d.id = act.id_direccion
                            where 
                                act.estado_plan LIKE case when '" . $estado . "'='0' then '%' else  '" . $estado . "' end
                                AND act.id_direccion LIKE case when '" . $id_direccion . "'='0' then '%' else  '" . $id_direccion . "' end
                                AND act.fecha_inicio BETWEEN  '" . $fecha_inicio . " 00:00:00' AND '" . $fecha_fin . " 23:59:59'
                              and
                              act.estado = 'ACT'");

                if ($tipo_grafico == 'direccion') {
                    $datos = collect($datos)->where('alias', $categoria)->all();
                } else {
                    $datos = collect($datos)->where('estado_plan', $categoria)->all();
                }
                $columnas = ['ID PLAN', 'DIRECION', 'DESCRIPCIÓN', 'ESTADO'];
                break;
            case 6:
                # code...
                // dd($categoria.$id_direccion);
                $datos = DB::select("select  act.id,act.nombre,act.responsables,ifnull(estado_proyecto,\"SIN_ESTADO\") as estado_proyecto 
                            from coor_tmov_proyecto_estrategico  as act
                            
                            where 
                                act.estado_proyecto LIKE case when '" . $estado . "'='0' then '%' else  '" . $estado . "' end AND
                                act.fase LIKE case when '" . $fase . "'='0' then '%' else  '" . $fase . "' end
                              and
                              act.estado = 'ACT'");

                //   dd($datos);
                $datos =  collect($datos);
                $datos_collecte = $datos->map(function ($item, $key) use ($id_direccion) {
                    $direcciones = str_replace('|', ',', $item->responsables);

                    if ($direcciones) {
                        $direcciones_ = DB::select('select id ,direccion,alias, \'' . $item->nombre . '\' , \'' . $item->estado_proyecto . '\' as estado_proyecto from tmae_direcciones where id in (' . $direcciones . ') ');
                        $direcciones_colect = collect($direcciones_);
                        if ($id_direccion != '0') {
                            $direcciones_colect_ = collect($direcciones_colect->where('id', $id_direccion));
                            // dd($direcciones_colect_);
                            if (isset($direcciones_colect_[0])) {
                                return collect($direcciones_colect_)->first();
                            }
                        } else {
                            return $direcciones_colect;
                        }
                    }
                })->filter(function ($user, $key) {
                    return $user != null;
                });

                
                $array_direciones = [];
                foreach ($datos_collecte as $k => $v) {
                    foreach ($v as $key => $value) {
                        # code...
                        $array_direciones = array_merge($array_direciones, [$value]);
                    }
                    // dd($array_direciones);
                    # code...
                }

                // dd($array_direciones);

                if ($tipo_grafico == 'direccion') {

                    $datos = collect($array_direciones)->where('alias', $categoria)->all();
                } else {
                    $datos = collect($array_direciones)->where('estado_proyecto', $categoria)->all();
                }
                $columnas = ['ID PROYECTO', 'DIRECION', 'DESCRIPCIÓN', 'ESTADO'];
                break;

            default:
                # code...
                break;
        }


        $html = '<table class="table"><thead><tr>';
        foreach ($columnas as $key => $value) {
            $html .= '<th>' . $value . '</th>';
        }
        $html .= '</tr></thead>';
        foreach ($datos as $k => $v) {

            $html .= '<tr>';
            foreach ($v as $s => $s_v) {
                $html .= '<td>' . $s_v . '</td>';
            }
            $html .= '</tr>';
            # code...
        }


        $html .= '</table>';


        return $html;
    }
}
