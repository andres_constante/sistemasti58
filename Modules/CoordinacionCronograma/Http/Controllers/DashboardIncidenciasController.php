<?php
namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use GuzzleHttp\Client as GuzzleHttpClient;
use Auth;
use Carbon\Carbon;
use App\UsuariosModel;
use DB;

class DashboardIncidenciasController extends Controller
{

    var $configuraciongeneral = array ("", "coordinacioncronograma/coordinacionlista", "index");
    var $escoja=array(0=>"TODOS");
    var $objetos = '[{"Tipo":"select","Descripcion":"Tipo de Filtro","Nombre":"tipofiltro","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';

    var $objetosTable = '[


        {"Tipo":"text","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Avance","Nombre":"avance","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                {"Tipo":"text","Descripcion":"Ultima Modificacion","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }

                ]';



    public function __construct() {
                $this->middleware('auth');
    }



    var $general = 'https://portalciudadano.manta.gob.ec/';
    // var $general = 'https://mantaentusmanos.test/';
    
    public function mantapp()
    {
        ini_set("memory_limit",-1);

        $configuraciongeneral = array ("", "coordinacioncronograma/coordinacionlista", "index");

    // try {
        $client = new GuzzleHttpClient();
        $respuesta = $client->request('POST',  $this->general.'/exportar_denuncias', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],'verify'=>false
        ]);
        $queryTotales = json_decode($respuesta->getBody()->getContents());
        $client = new GuzzleHttpClient();
        $respuesta = $client->request('POST',  $this->general.'/exportar_entidades', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],'verify'=>false
        ]);
        $entidades = json_decode($respuesta->getBody()->getContents());
        $objetos = json_decode($this->objetos);
        //show($this->objetos);
        $objetos[0]->Nombre="tipofiltro";
        $tipoFiltro=array(1=>"DIRECCION",2=>"ESTADO",3=>"PRIORIDAD");

        $objetos[0]->Valor=$this->escoja + $tipoFiltro;
        $objetosTable = json_decode($this->objetosTable);
        $objetos=array_values($objetos);
        $delete='si';
        $tipo_fecha="Busqueda por Fecha de Última Modificación";
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
        if($id_tipo_pefil->tipo==2||$id_tipo_pefil->tipo==3){
            $delete=null;
        }

        $date = Carbon::now();
        $endDate = $date->subDay(7);
        $fin = $endDate->toDateString();
        $date = Carbon::now();
        $inicio = $date->toDateString();
        $fin2 = date('Y-m-d',strtotime($inicio));
        $inicio2 = date('Y-m-d',strtotime($fin));


        return view('mantapp_denuncias.dashboard_denuncias',[
                "objetos"=>$objetos,
                 "objetosTable"=>$objetosTable,
                "inicio_date"=>$inicio2,
                "inicio_fin"=>$fin2,
            
                "denuncia"=>$queryTotales,  
                "entidades"=>$entidades,
              
                "configuraciongeneral"=>$configuraciongeneral,
                "create"=>'si',
                "delete"=>$delete,
                ]);

        // } catch (\Exception $e) {
        //     return 'No fue posible contactar con https://portalciudadano.manta.gob.ec';
        //     return $e->getMessage();
        // }

    }



    public function index()
    {
        return 'hola bebe';
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('coordinacioncronograma::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
