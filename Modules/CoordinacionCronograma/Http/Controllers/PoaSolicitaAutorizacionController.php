<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Modules\CoordinacionCronograma\Entities\CoorPoaActividadCabModel;
use Modules\CoordinacionCronograma\Entities\PoaAutorizacionActiModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Auth;
use Session;
use DB;
use URL;
use stdClass;
use Barryvdh\DomPDF\Facade as PDF;

class PoaSolicitaAutorizacionController extends Controller
{
    var $configuraciongeneral = array("Certificación POA", "coordinacioncronograma/solicitapoa", "index", 6 => "coordinacioncronograma/solicitapoaajax", 7 => "solicitapoa");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Proyecto POA","Nombre":"proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },                        
        {"Tipo":"text","Descripcion":"Actividad POA","Nombre":"id_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Objeto de la Contratación","Nombre":"objeto_contrato","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Dirección requiriente","Nombre":"direccion_adquiriente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Solicitado por","Nombre":"solicitado_por","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Aprobado por","Nombre":"aprobado_por","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Solicitud","Nombre":"fecha_solicita","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Aprobación","Nombre":"fecha_aprueba","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Observación","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Estado","Nombre":"estado_solicitud","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textdisabled","Descripcion":"Monto Total Actividad","Nombre":"monto_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Monto","Nombre":"monto_solicita","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
    var $objetosedit = '[
        {"Tipo":"datetext","Descripcion":"Fecha Solicitud","Nombre":"fecha_solicita","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },        
        {"Tipo":"textdisabled","Descripcion":"Proyecto POA","Nombre":"proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },                        
        {"Tipo":"select","Descripcion":"Actividad POA","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Objeto de la Contratación","Nombre":"objeto_contrato","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textdisabled","Descripcion":"Monto Total Actividad","Nombre":"monto_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Monto a Solicitar","Nombre":"monto_solicita","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
    protected $notificacion;
    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth', ["except" => ["printautorizacionpoa"]]);
        $this->notificacion = $notificacion;
    }
    var $validarjs = array();
    public function tmpEnviarNotificacionBorrador()
    {
        $total = 0;
        $acti = PoaAutorizacionActiModel::where("estado", "ACT")->where("estado_solicitud", "SOLICITADA")->get();
        foreach ($acti as $key => $value) {
            $montoact = number_format(floatval($value->monto_solicita), 2);
            $idcab = Crypt::encrypt($value->id);
            $direcciontxt = $value->direccion_adquiriente;
            $msgcer = "ha solicitado una Certificación POA de la Actividad #" . $value->id_actividad . " por un monto de $montoact."; //." del proyecto ".Input::get("proyecto");
            $userRe = UsuariosModel::find($value->id_usuario);
            $this->notificacion->notificacionesweb("Usted $msgcer", URL::to('coordinacioncronograma/printautorizacionpoa') . "/" . $idcab, $userRe->id, "2c438f");
            $this->notificacion->EnviarEmail(Auth::user()->email, "Nueva Certificación POA", "Certificación POA", "<b>CERTIFICACIÓN #" . $value->id . "</b><br>Usted $msgcer", $this->configuraciongeneral[1] . '/' . $idcab . '/edit');
            //Notificación Directora Ligia
            $msgcer = $userRe->name . " de $direcciontxt $msgcer";
            $userDir = UsuariosModel::find(1663);
            $this->notificacion->notificacionesweb($msgcer, $this->configuraciongeneral[1] . '/' . $idcab . '/edit', $userDir->id, "2c438f");
            $this->notificacion->EnviarEmail($userDir->email, "Nueva Certificación POA", "Certificación POA", "<b>CERTIFICACIÓN #" . $value->id . "</b><br>$msgcer", $this->configuraciongeneral[1] . '/' . $idcab . '/edit');
            $total++;
        }
        return "Se procesaron un total de $total Actividades POA";
    }
    public function perfil()
    {
        //$id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        //show($id_tipo_pefil);
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            if (Auth::user()->id == 1663) {
                $object->crear = 'SI';
                $object->ver = 'SI';
                $object->editar = 'SI';
                $object->eliminar = 'SI';
                $object->editar_parcialmente = 'SI';
                $object->guardar = 'SI';
            } else {

                $object->crear = 'NO';
                $object->ver = 'SI';
                $object->editar = 'SI';

                $object->eliminar = 'NO';
                $object->editar_parcialmente = 'SI';
                $object->guardar = 'NO';
            }
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        }
        $object->perfil = $id_tipo_pefil->tipo;
        return $object;
    }

    public function verpermisos($objetos = array(), $tipo = "index")
    {
        /*============================*/
        $tabla = PoaAutorizacionActiModel::where("poa_tmov_autorizacion.estado", "ACT")
            ->join("poa_tmae_cab_actividad as a", "a.id", "poa_tmov_autorizacion.id_actividad")
            ->join("poa_tmae_cab as b", "b.id", "=", "a.id_poa")
            ->select(
                "poa_tmov_autorizacion.*",
                "a.actividad",
                "b.proyecto",
                "b.anio"

            )
            ->orderBy("poa_tmov_autorizacion.id", "desc");
        //show($tabla->get()->toarray());
        $objetos[1]->Nombre = "actividad";
        $perfil = $this->perfil();
        //show($perfil);
        if ($perfil->perfil == 3 && Auth::user()->id != 1663)
            $tabla = $tabla->where("a.id_direccion_act", Auth::user()->id_direccion);
        return array($objetos, $tabla);
    }
    public function solicitapoaajax(Request $request)
    {

        // dd(Input::all());
        $objetos = json_decode($this->objetos);
        /*====================================*/
        $tabla = $this->verpermisos($objetos);
        $objetos = $tabla[0];

        $tabla = $tabla[1];
        $anio = Input::get('anio');
        if ($anio) {
            $tabla->where('anio', $anio);
        } else {
            $tabla->where('anio', 2019);
        }
        // dd($tabla);
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'id_direccion')->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
            if (Auth::user()->id != 1663) {
                $tabla->where('id_direccion', $id_tipo_pefil->id_direccion);
            }
        }

        ////////
        if ($id_tipo_pefil->tipo == 10) {
            $direcciones = getDireccionesCoor(Auth::user()->id);
            $tabla = $tabla->whereIn("id_direccion", explode(",", $direcciones->id_direccion));
        }

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('poa_tmov_autorizacion.id', 'LIKE', "%{$search}%")
                    ->orWhere('poa_tmov_autorizacion.objeto_contrato', 'LIKE', "%{$search}%")
                    ->orWhere('poa_tmov_autorizacion.direccion_adquiriente', 'LIKE', "%{$search}%")
                    ->orWhere('poa_tmov_autorizacion.solicitado_por', 'LIKE', "%{$search}%")
                    ->orWhere('a.actividad', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {
                $permisos = $this->perfil();
                $show = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                $edit =   link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o'));
                if (($permisos->perfil == 3 && Auth::user()->id != 1663) && $post->estado_solicitud != 'NEGADA')
                    $edit = '<a href="' . URL::to("coordinacioncronograma/printautorizacionpoa") . "/" . Crypt::encrypt($post->id) . '" class="fa fa-print" target="_blank">';

                if (Auth::user()->id == 1) {
                    $edit = '<a href="' . URL::to("coordinacioncronograma/printautorizacionpoa") . "/" . Crypt::encrypt($post->id) . '" class="fa fa-print" target="_blank">';
                }
                $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';

                // dd($post->estado_solicitud);
                if ($permisos->ver == 'NO')
                    $show = "";
                if ($permisos->editar == 'NO'  && $post->estado_solicitud != 'NEGADA')
                    $edit = "";
                if ($permisos->eliminar == 'NO')
                    $dele = "";
                $acciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    // dd($objetos);
                    if ($value->Nombre == "monto_solicita" || $value->Nombre == "monto_actividad") {
                        $campo = $value->Nombre;
                        $nestedData["$value->Nombre"] = "$" . number_format($post->$campo, 2);
                    } else {

                        $campo = $value->Nombre;
                        $nestedData["$value->Nombre"] = $post->$campo;
                    }
                }
                // dd($edit);
                $nestedData['acciones'] = $acciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = json_decode($this->objetos);
        $objetos[1]->Nombre = "actividad";
        /*====================================*/
        $this->configuraciongeneral[6] = "coordinacioncronograma/solicitapoaajax" . "?anio=" . Input::get('anio');
        $this->configuraciongeneral[0] = "Certificación POA - " . Input::get('anio');
        // dd(Input::all());

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si",
            "permisos" => $this->perfil()
        ]);
        //return view('coordinacioncronograma::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return redirect()->back()->withInput(Input::all());
        //return $this->index();
        //return view('coordinacioncronograma::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("poa_tmov_autorizacion.id", $id)->orderby("poa_tmov_autorizacion.id", "desc")->first();

        // $this->configuraciongeneral[3] = "5"; //Tipo de Referencia de Archivo
        // $this->configuraciongeneral[4] = "si";
        $objetos = $objetos[0];
        $objetos[1]->Nombre = "actividad";
        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral

        ]);
        //return view('coordinacioncronograma::show');
    }
    public function pideautorizacionpoa($id)
    {
        $id = intval(Crypt::decrypt($id));
        //show($id);
        //Ver si ya tiene Aprobación
        $verifica = PoaAutorizacionActiModel::where(["id_actividad" => $id, "estado_solicitud" => "APROBADA", 'estado' => 'ACT'])->orderby("id", "desc");
        // dd($toac);

        $toac = clone $verifica;
        $toac = $toac->select(DB::raw("sum(monto_solicita) as monto_total"))->first();
        $verifica = $verifica->first();
        //show($verifica->toarray());
        $saldo = 0;
        if ($verifica) {
            $saldo = floatval($toac->monto_total);
            $actividad = floatval($verifica->monto_actividad);
            if ($saldo == $actividad)
                return $this->show(Crypt::encrypt($verifica->id));
        }
        //
        $this->configuraciongeneral[2] = "editar";
        $tabla = CoorPoaActividadCabModel::join("poa_tmae_cab as a", "a.id", "=", "poa_tmae_cab_actividad.id_poa")
            ->select("poa_tmae_cab_actividad.*", "a.proyecto")
            ->where("poa_tmae_cab_actividad.id", $id);
        $combo = $tabla->pluck("actividad", "id")->all();
        $comboante = $tabla->pluck("id", "id")->all();
        //show($combo);
        $tabla = $tabla->first();
        if ($tabla->monto_actividad == 0)
            return view('vistas.showmensajes', [
                "configuraciongeneral" => $this->configuraciongeneral,
                "msg" => "El monto de la Actividad es cero.",
                "msgar" => array()
            ]);
        //show($tabla->toarray());
        $objetos = json_decode($this->objetosedit);
        $objetos[0]->Valor = date("Y-m-d");
        $objetos[2]->Valor = $combo;
        $objetos[2]->ValorAnterior = $comboante;
        $objetos[5]->ValorAnterior = floatval($tabla->monto_actividad) - $saldo;
        $link = json_decode('[
                {"Tipo":"link","Descripcion":"Detalle Actividad","Nombre":"detalleacti","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]');
        $link2 = json_decode('[
                {"Tipo":"link","Descripcion":"Certificaciones","Nombre":"detallecerti","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]');
        if ($verifica) {
            $objetos = array_merge($objetos, $link);
            $objetos[6]->Valor = URL::to($this->configuraciongeneral[1] . "/" . Crypt::encrypt($verifica->id));
            $objetos = array_merge($objetos, $link2);
            $objetos[7]->Valor = URL::to("coordinacioncronograma/solicitapoa");
        }

        //show($objetos);
        //$this->configuraciongeneral[1].="?menu=no";
        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "tabla" => $tabla,
            "validarjs" => $this->validarjs
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //$this->configuraciongeneral[2]="editar";
        //$tabla = PerfilModel::find($id);
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("poa_tmov_autorizacion.id", $id)->first();
        //show($tabla->toarray(),0);
        $objetos = $objetos[0];
        //show($objetos);

        foreach ($objetos as $key => $value) {
            //     if($key=="monto_solicita"){

            // }
            if ($objetos[$key]->Tipo == "text") {
                if ($value->Nombre != "monto_solicita") {
                    // show($key);
                    $objetos[$key]->Tipo = "textdisabled";
                }
            }
        }
        $estados = array("APROBADA" => "APROBADA", "NEGADA" => "NEGADA",  "ANULADA" => "ANULADA");
        if ($tabla->estado_solicitud == "APROBADA") {
            //show($objetos);
            $objetos[7]->Tipo = "textdisabled";
            $objetos[8]->Tipo = "textarea-disabled";
            $estados = array($tabla->estado_solicitud => $tabla->estado_solicitud);
        }
        unset($objetos[5]);
        $objetos[6]->Tipo = "textdisabled";
        //show($objetos);
        $objetos[9]->Tipo = "select";

        $objetos[9]->Valor = $estados;
        // dd($objetos);
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'id_direccion')->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 3 && Auth::user()->id != 1663) {
            unset($objetos[8]);
            unset($objetos[7]);
            // unset($objetos[9]);
            // dd($tabla);
            $objetos[9]->Valor = [$tabla->estado_solicitud => $tabla->estado_solicitud];
            $objetos[2]->Tipo = 'textarea';

            $objetos[6]->Tipo = "datetext";
            $objetos[11]->Tipo = "text";
        } elseif ($id_tipo_pefil->tipo == 4) {

            $objetos[2]->Tipo = "textarea";
            $objetos[6]->Tipo = "datetext";
            $objetos[7]->Tipo = "datetext";
            $objetos[8]->Tipo = "textarea";
            $estados = array("APROBADA" => "APROBADA", "NEGADA" => "NEGADA", "ANULADA" => "ANULADA");
            $objetos[9]->Valor = $estados;
            $objetos[9]->ValorAnterior = $tabla->estado_solicitud;
        }
        $this->configuraciongeneral[2] = "edit";

        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "tabla" => $tabla,
            "validarjs" => $this->validarjs
        ]);
        //return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try {
            $tabla = PoaAutorizacionActiModel::find($id);
            $tabla->estado = 'INA';
            $tabla->id_usuario = Auth::user()->id;
            //$tabla->delete();
            // dd($tabla);
            $tabla->save();
            DB::commit();
            Session::flash('message', 'El registro se eliminó Exitosamente!');
            if (Input::has('vista')) {
                return 'El registro se eliminó Exitosamente!';
            }
            return Redirect::to($this->configuraciongeneral[1]);
        } //Try Transaction
        catch (\Exception $e) {
            //  // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);
        }
    }
    public function guardar($id)
    {
        $input = Input::all();

        $ruta = $this->configuraciongeneral[1];
        $arrapas = array(
            'fecha_solicita' => 'required',
            'objeto_contrato' => 'required',
            'monto_solicita' => 'required|numeric'
        );
        $veracti = PoaAutorizacionActiModel::where("id_actividad", $id)
            ->where("estado", "ACT");

        $toac = PoaAutorizacionActiModel::where(["id_actividad" => $id, "estado_solicitud" => "APROBADA", 'estado' => 'ACT'])->orderby("id", "desc");
        $veracti = $veracti->first();
        $id_actividad = $id;
        $id_poa = 0;
        if (Input::has("estado_solicitud")) {
            //show($id);
            if (Input::has("estado_solicitud") != 'NEGADA') {
                $arrapas = array(
                    'fecha_aprueba' => 'required',
                    'observacion' => 'required',
                    'estado_solicitud' => 'required'
                );
            }

            $veracti = PoaAutorizacionActiModel::find($id);
            if ($veracti) {
                $id_poa = $veracti->id;
            }
        }
        $validator = Validator::make($input, PoaAutorizacionActiModel::rules($id, $arrapas));

        if ($validator->fails()) {
            //die($ruta);
            return redirect()->back()->withInput(Input::all())->withErrors($validator);
        } else {
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
                ->select("ap.tipo")
                ->where("users.id", Auth::user()->id)
                ->first();
            $direccion = direccionesModel::find(Auth::user()->id_direccion);
            $direcciontxt = "";
            if ($direccion)
                $direcciontxt = $direccion->direccion;
            $msgauditoria = "";
            $msg = "";
            //show($veracti);
            $solicitar = 1;
            if ($veracti)
                // dd(Auth::user()->id);
                if (($id_tipo_pefil->tipo == 1 ||  Auth::user()->id == 1663 ||  Auth::user()->id == 1663)) {
                    $solicitar = 0;
                    // show($solicitar);
                }
            // show($veracti);
            if ($solicitar == 1) //No Existe
            {
                if (($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 3) && Auth::user()->id != 1663) { //1 Administrador 3 Director
                    $monso = floatval(Input::get("monto_solicita"));
                    $monto = floatval(Input::get("monto_actividad"));
                    if ($monso > $monto)
                        return redirect()->back()->withErrors(["No se puede generar la Certificación, el monto es superior al monto de la Actividad."])->withInput();
                    /*===================================*/
                    /*Comprobar Saldos*/
                    $toac = $toac->select(DB::raw("sum(monto_solicita) as monto_total"))->first();
                    $verifica = null;
                    if ($veracti) {
                        $verifica = $veracti->first();
                    }
                    //show($verifica->toarray());
                    $saldo = 0;
                    if ($verifica) {
                        $ocupado = floatval($toac->monto_total);
                        $saldo = $ocupado + $monso;
                        $dispo = $monto - $ocupado;
                        if ($saldo > $monto)
                            return redirect()->back()->withErrors(["No se puede generar la Certificación, el monto es superior al monto de la Actividad, Monto Dispopnible: " . $dispo])->withInput();
                    }
                    /*===================================*/
                    if ($id_poa != 0) {
                        $guardar = PoaAutorizacionActiModel::find($id_poa);
                        // dd($id_auto);
                        //show('si');
                    } else {
                        // show('no');
                        $guardar = new PoaAutorizacionActiModel;
                        $guardar->id_actividad = Input::get("actividad");
                    }

                    $guardar->objeto_contrato = Input::get("objeto_contrato");
                    $guardar->direccion_adquiriente = $direcciontxt;
                    $guardar->solicitado_por = Auth::user()->name;
                    $guardar->cargo_solicita = Auth::user()->cargo;
                    $guardar->fecha_solicita = Input::get("fecha_solicita");
                    $guardar->estado_solicitud = "SOLICITADA";
                    $guardar->monto_solicita = Input::get("monto_solicita");
                    $guardar->monto_actividad = Input::get("monto_actividad");
                    $guardar->id_usuario = Auth::user()->id;
                    $guardar->ip = \Request::getClientIp();
                    $guardar->pc = \Request::getHost();
                    // dd($guardar);
                    // dd(Input::all());
                    $guardar->save();
                    $idcab = Crypt::encrypt($guardar->id);
                    $montoact = number_format(floatval($guardar->monto_solicita), 2);
                    $msgauditoria = "Solicitud de Certificado POA creada";
                    $msgcer = "ha solicitado una Certificación POA de la Actividad #" . $guardar->id_actividad . " del proyecto " . Input::get("proyecto") . " por un monto de $montoact. (" . str_pad($guardar->id, 5, "0", STR_PAD_LEFT) . ")";
                    //Notificación Usuario Solicita
                    $this->notificacion->notificacionesweb("Usted $msgcer", URL::to('coordinacioncronograma/printautorizacionpoa') . "/" . $idcab, Auth::user()->id, "2c438f");
                    $this->notificacion->EnviarEmail(Auth::user()->email, "Nueva Certificación POA", "Certificación POA", "<b>CERTIFICACIÓN #" . str_pad($guardar->id, 5, "0", STR_PAD_LEFT) . "</b><br>Usted $msgcer", $this->configuraciongeneral[1] . '/' . $idcab . '/edit');
                    //Notificación Directora Ligia
                    $msgcer = Auth::user()->name . " de $direcciontxt $msgcer";
                    $userDir = UsuariosModel::find(1663);
                    $this->notificacion->notificacionesweb($msgcer, $this->configuraciongeneral[1] . '/' . $idcab . '/edit', $userDir->id, "2c438f");
                    $this->notificacion->EnviarEmail($userDir->email, "Nueva Certificación POA", "Certificación POA", "<b>CERTIFICACIÓN #" . str_pad($guardar->id, 5, "0", STR_PAD_LEFT) . "</b><br>$msgcer", $this->configuraciongeneral[1] . '/' . $idcab . '/edit');
                } else
                    return redirect()->back()->withErrors(["No se puede generar la Certificación, usuario incorrecto."])->withInput();
            } else {
                //Usuario Soliicta
                $guardar = $veracti;
                $useSol = $guardar->id_usuario;

                $guardar->aprobado_por = Auth::user()->name;
                $guardar->cargo_aprobado = Auth::user()->cargo;

                $guardar->fecha_solicita = Input::get("fecha_solicita");
                $guardar->fecha_aprueba = Input::get("fecha_aprueba");
                $guardar->observacion = Input::get("observacion");
                $guardar->estado_solicitud = Input::get("estado_solicitud");
                $guardar->objeto_contrato = Input::get("objeto_contrato");
                $guardar->monto_solicita = Input::get("monto_solicita");
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                $guardar->codigobarras = $guardar->id . strtoupper(str_random(9)); // . $guardar->id . str_random(6);

                $msgauditoria = "Solicitud de Certificado POA editada: " . $id;
                $idcab = Crypt::encrypt($guardar->id);
                //
                $montoact = number_format(floatval($guardar->monto_solicita), 2);
                $msgcer = "Certificación POA de la Actividad #" . $guardar->id_actividad . " cambió a estado " . Input::get("estado_solicitud") . ". Monto Solicitado: $montoact. (" . str_pad($guardar->id, 5, "0", STR_PAD_LEFT) . ")";;
                $msgcer .= " por " . Auth::user()->name;
                //Notificación Usuario Solicita
                $this->notificacion->notificacionesweb("$msgcer", URL::to('coordinacioncronograma/printautorizacionpoa') . "/" . $idcab, Auth::user()->id, "2c438f");
                //$this->notificacion->EnviarEmail(Auth::user()->email, "Nueva Certificación POA", "Certificación POA", "<b>CERTIFICACIÓN #" . $idcab."</b><br>Usted $msgcer", $this->configuraciongeneral[1] . '/' . $idcab. '/edit');
                //Notificación Directora Ligia
                $userDir = UsuariosModel::find($useSol);
                $this->notificacion->notificacionesweb("$msgcer", URL::to('coordinacioncronograma/printautorizacionpoa') . "/" . $idcab, $userDir->id, "2c438f");
                //$this->notificacion->EnviarEmail($userDir->email, "Nueva Certificación POA", "Certificación POA", "<b>CERTIFICACIÓN #" . $idcab."</b><br>$msgcer", $this->configuraciongeneral[1] . '/' . $idcab. '/edit');

                $guardar->save();
            }
            $msg = $msgauditoria;
            $guardar->save();
            Auditoria($msgauditoria . " - ID: " . $id . "-" . Input::get("actividad"));
        }
        Session::flash('message', $msg);

        if ($id_actividad != 0) {

            $tabla = PoaAutorizacionActiModel::where("poa_tmov_autorizacion.estado", "ACT")
                ->join("poa_tmae_cab_actividad as a", "a.id", "poa_tmov_autorizacion.id_actividad")
                ->join("poa_tmae_cab as b", "b.id", "=", "a.id_poa")
                ->select(
                    "poa_tmov_autorizacion.*",
                    "a.actividad",
                    "b.proyecto",
                    "b.anio"

                )
                ->where('id_actividad', $id_actividad)->first();
            if ($tabla) {
                return Redirect::to($this->configuraciongeneral[1] . '?anio=' . $tabla->anio);
            }
        }
        return Redirect::to($this->configuraciongeneral[1]);
    }
    public function printautorizacionpoa($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = json_decode($this->objetos);
        $objetos[1]->Nombre = "actividad";
        $tabla = PoaAutorizacionActiModel::where("poa_tmov_autorizacion.estado", "ACT")
            ->join("poa_tmae_cab_actividad as a", "a.id", "poa_tmov_autorizacion.id_actividad")
            ->join("poa_tmae_cab as b", "b.id", "=", "a.id_poa")
            ->select(
                "poa_tmov_autorizacion.*",
                "a.actividad",
                "b.proyecto"
            )
            ->where("poa_tmov_autorizacion.id", $id)
            ->orderBy("poa_tmov_autorizacion.id", "desc")
            ->orderby("poa_tmov_autorizacion.id", "desc")->first();

        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        ini_set('memory_limit', '3000M'); //This might be too large, but depends on the data set
        $pdf = \App::make('dompdf.wrapper');
        // dd($tabla);
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            ]
        ]);
        $pdf->getDompdf()->setHttpContext($context);
        
        $pdf->loadView('vistas.certificadopoa', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ])->setPaper('a4', 'portrait'); //])->setPaper('a4','landscape');

        return $pdf->stream('certificadopoa.pdf');
        /*
        return view('vistas.certificadopoa', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "pdf"=>"no"
        ]);*/
    }
}
