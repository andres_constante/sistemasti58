<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Coordinacioncronograma\Http\Controllers\CoorCronogramaCabController;

class CoorArchivosActividadController extends Controller
{
    var $configuraciongeneral = array(
        "Plan 100 Días - Archivos",
        "coordinacioncronograma/archivosactividades",
        "index",
        6 => "coordinacioncronograma/archivosactividadesajax",
        7 => "archivosactividades"
    );
    var $columns = array(
        0 => 'id',
        1 => 'actividad',
        2 => 'tipo_actividad',
        3 => 'estado_actividad',
        4 => 'fecha_inicio',
        5 => 'fecha_fin',
        6 => 'direccion',
        7 => 'avance',
        8 => 'prioridad',
        9 => 'updated_at'
    );
    protected $Coordinacion;
    public function __construct(CoorCronogramaCabController $Coordinacion)
    {
        $this->middleware('auth');
        $this->Coordinacion = $Coordinacion;
    }
    public function archivosactividadesajax(Request $request)
    {
        $filtro = [ ["coor_tmov_cronograma_cab.id_tipo_actividad", 5]];
        return $this->Coordinacion->cronogramaajax($request, $filtro,$this->columns,$this->configuraciongeneral[7]);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return $this->Coordinacion->index($this->configuraciongeneral[6],[],$this->configuraciongeneral);
        //return $this->Coordinacion->index();

        //return view('coordinacioncronograma::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return $this->index();
        return view('coordinacioncronograma::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return $this->Coordinacion->show($id);
        //return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->Coordinacion->edit($id,[],$this->configuraciongeneral);
        //return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->Coordinacion->guardar($id,[],$this->configuraciongeneral,[],[]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
