<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\ArchivosModel;
use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteObjetivosModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionCabModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionDetaDireccionModel;
use Modules\Coordinacioncronograma\Entities\CoorPoaCabModel;
use Modules\Coordinacioncronograma\Entities\CoorPoaDetaHistoModel;
use Auth;
use Session;
use DB;
use Exception;
use Illuminate\Support\Facades\URL;
use Modules\CoordinacionCronograma\Entities\CoorPoaActividadCabModel;
use Modules\CoordinacionCronograma\Entities\CoorPoaActividadDetaCabModel;
use App\SubDireccionModel;
use Modules\CoordinacionCronograma\Entities\CoorPoaActividadesTareasDetaModel;
use Modules\CoordinacionCronograma\Entities\CoorPoaActividadesTareasModel;
use stdClass;
use Illuminate\Support\Facades\Log;

class CoorPoaCabController extends Controller
{
    var $configuraciongeneral = array("POA", "coordinacioncronograma/poamain", "index", 6 => "coordinacioncronograma/poamainajax", 7 => "poamain", 8 => '?anio=2019', 9 => "add_act_poa", 10 => "get_actividad_poa", 11 => "eliminar_actividad");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Proyecto","Nombre":"proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },                
        {"Tipo":"text","Descripcion":"Total Asignado por Dirección","Nombre":"monto_asignado","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Total Asignado por Proyecto","Nombre":"total_asignado","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado POA","Nombre":"estado_poa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Línea Base","Nombre":"linea_base","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Descripción del Proyecto","Nombre":"descripcion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Meta","Nombre":"meta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Año","Nombre":"anio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tipo","Nombre":"tipo_proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
    /*COmponente Sostenibilidad*/
    var $objetoscomponentes = '
        [
            {"Tipo":"select","Descripcion":"Componente","Nombre":"componente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
            {"Tipo":"select","Descripcion":"Objetivos","Nombre":"objetivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Programa","Nombre":"programa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]
    ';

    var $anio = '2019';



    var $actividades = '
        [            
            {"Tipo":"text","Descripcion":"Poa","Nombre":"id_poa","Clase":" hidden","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"textarea","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion_act","Clase":"12","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select-multiple","Descripcion":"Responsable","Nombre":"idusuario_act","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"datetext","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"datetext","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"textarea","Descripcion":"Explicación Actividad","Nombre":"observacion","Clase":"mostrarobservaciones maxlength:350","Valor":"Null","ValorAnterior" :"Null" } ,
            {"Tipo":"select","Descripcion":"Avance %","Nombre":"avance","Clase":"avance","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"text","Descripcion":"Monto Actividad","Nombre":"monto_actividad","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },      
            {"Tipo":"file","Descripcion":"Archivo final","Nombre":"archivo_final","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" },      
            {"Tipo":"html","Descripcion":"Tareas","Nombre":"tareas","Clase":"12","Valor":"Null","ValorAnterior" :"Null" },      
            {"Tipo":"select","Descripcion":"Verificación","Nombre":"verificacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },      
            {"Tipo":"textarea","Descripcion":"Observacion de verificación","Nombre":"observacion_verificacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }      
        ]
    ';
    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "id_direccion" => "id_direccion: {
                            required: true
                        }",
        "filtproyectoro" => "proyecto: {
                            required: true
                        }"
    );
    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }
    public function perfil($verificar = 'SI')
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        //show($id_tipo_pefil);
        // if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $verificar == 'NO') { //1 Administrador 4 Coordinador
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $id_tipo_pefil->tipo == 10) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            // dd('aqs');s
            if (Auth::user()->id == 1764) {
                $object->crear = 'SI';
                $object->ver = 'SI';
                $object->editar = 'SI';
                $object->eliminar = 'SI';
                $object->editar_parcialmente = 'SI';
                $object->guardar = 'SI';
            } else {

                $object->crear = 'NO';
                $object->ver = 'SI';
                $object->editar = 'SI';

                $object->eliminar = 'NO';
                $object->editar_parcialmente = 'SI';
                $object->guardar = 'NO';
            }
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        }

        return $object;
    }
    public function construyetimeline($id)
    {
        /*Historial Tabla*/
        $historialtable = CoorPoaDetaHistoModel::join("users as a", "a.id", "=", "poa_tmae_deta_histo.id_usuario")
            ->join("tmae_direcciones as d", "d.id", "=", "poa_tmae_deta_histo.id_direccion")
            ->where("poa_tmae_deta_histo.id_poa_cab", $id)
            ->select("poa_tmae_deta_histo.*", "a.name as usuario", "d.direccion")
            ->orderby("poa_tmae_deta_histo.updated_at")
            ->get();
        //show($historialtable);
        $obaddjs = json_decode($this->objetos);
        //            $obaddjs=$objetos;
        //show($obaddjs);
        //$obaddjs[2]->Nombre="parroquia";
        $obaddjs[1]->Nombre = "direccion";
        $object = new stdClass;
        $object->Tipo = "text";
        $object->Descripcion = "Modificado por";
        $object->Nombre = "usuario";
        $obaddjs[] = $object;
        return array($historialtable, $obaddjs);
    }
    public function verpermisos($objetos = array(), $tipo = "index", $anio = '2019')
    {
        //show($objetos);
        $composentesql = "SELECT aa.nombre AS componente from coor_tmae_cronograma_componente_cab aa 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bb on aa.id=bb.id_componente
            where bb.id=poa_tmae_cab.id_objetivo_componente";
        $objetivosql = "SELECT bx.objetivo from coor_tmae_cronograma_componente_cab ax 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bx on ax.id=bx.id_componente
            WHERE bx.id=poa_tmae_cab.id_objetivo_componente";
        $programasql = str_replace("bx.objetivo", "bx.programa", $objetivosql);
        /*============================*/
        $coordinacion = "SELECT nombre from coor_tmae_coordinacion_main AS a INNER JOIN coor_tmov_coordinacion_direccion_deta AS b
	                    WHERE a.id=b.id_coor_cab AND b.id_direccion=poa_tmae_cab.id_direccion
            ";
        /*============================*/
        $tabla = CoorPoaCabModel::where("poa_tmae_cab.estado", "ACT")
            ->join("tmae_direcciones as a", "a.id", "poa_tmae_cab.id_direccion")
            ->select(
                "poa_tmae_cab.*",
                "a.direccion",
                DB::raw("ifnull(($composentesql),'') as componente"),
                DB::raw("ifnull(($objetivosql),'') as objetivo"),
                DB::raw("ifnull(($programasql),'') as programa"),
                DB::raw("ifnull(($coordinacion),'') as coordinacion")
            )
            ->where('anio', $anio)
            ->orderBy("poa_tmae_cab.id", "desc");

        // dd($anio);
        $tabla = $tabla->where('anio', $anio);

        // show($tabla->get()->toarray());
        return array($objetos, $tabla);
    }

    public function poamainajax(Request $request)
    {
        // dd(Input::all());
        $anio = Input::get('anio');
        // dd($anio);
        if ($anio == null) {
            $anio = 2019;
        }
        $objetos = json_decode($this->objetos);
        $objetos[1]->Nombre = "direccion";
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos);
        /*====================================*/
        /*====================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 4);
        //show($objetos);
        /*====================================*/
        $tabla = $this->verpermisos($objetos, null, $anio);
        $objetos = $tabla[0];

        //show($objetos);
        $tabla = $tabla[1];
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'id_direccion')->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
            if (Auth::user()->id != 1764) {
                $subarea = SubDireccionModel::where("area", $id_tipo_pefil->id_direccion)->first();
                if ($subarea)
                    $tabla->where('id_direccion', $subarea->id_direccion);
                else
                    $tabla->where('id_direccion', $id_tipo_pefil->id_direccion);
            }
        }

        ////////
        if ($id_tipo_pefil->tipo == 10) {
            $direcciones = getDireccionesCoor(Auth::user()->id);
            $tabla = $tabla->whereIn("id_direccion", explode(",", $direcciones->id_direccion));
        }

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('poa_tmae_cab.id', 'LIKE', "%{$search}%")
                    ->orWhere('poa_tmae_cab.proyecto', 'LIKE', "%{$search}%")
                    ->orWhere('poa_tmae_cab.descripcion', 'LIKE', "%{$search}%")
                    ->orWhere('a.direccion', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {
                $permisos = $this->perfil();

                $acciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;' .
                    link_to_route(str_replace("/", ".",  $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id), "menu" => "no", "anio" => $anio), array('class' => 'fa fa-pencil-square-o divpopup', 'target' => '_blank'/*, 'onclick' => 'popup(this)'*/));
                //link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o'));

                if ($permisos->eliminar == 'SI') {
                    $acciones .= '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                }

                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $nestedData['acciones'] = $acciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = json_decode($this->objetos);
        $objetos[1]->Nombre = "direccion";
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos);
        //$objetos = $this->verpermisos($objetos);
        //show($objetos );
        /*====================================*/
        /*====================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 4);
        //show($objetos);
        /*====================================*/

        // dd(Input::all());
        $anio = Input::get('anio');
        if ($anio == null) {
            $anio = 2019;
        }
        $this->configuraciongeneral[6] = "coordinacioncronograma/poamainajax?anio=" . $anio;
        $this->configuraciongeneral[8] = (isset($anio)) ? "?anio=" . $anio : "?anio=2019";
        $this->configuraciongeneral[0] = 'POA - ' . $anio;


        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si",
            "cerpoa" => $anio,
            "permisos" => (($anio == 2020) ? $this->perfil('NO') : $this->perfil())
        ]);
        //return view('coordinacioncronograma::index');
    }
    public function formularioscrear($id = "")
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        /*QUITAR CAMPOS*/
        $objetos = json_decode($this->objetos);
        $tbcom = direccionesModel::where("estado", "ACT")->orderby("direccion")->pluck("direccion", "id")->all();
        //precontractual, contractual,ejecución
        $estadopoa = explodewords(ConfigSystem("estadopoa"), "|");
        $estado_act = explodewords(ConfigSystem("estadoobras"), "|");
        $tipo_proyecto = explodewords(ConfigSystem("proyecto_poa"), "|");
        $avance = explodewords(ConfigSystem("avance"), "|");
        $objetos[1]->Valor = $this->escoja + $tbcom;
        $objetos[4]->Valor = $this->escoja + $estadopoa;

        ///anio poa//
        // dd(Input::all())
        $anio = Input::get('anio');
        if (Input::get('anio') == null) {
            $objetos[8]->Valor = [2019 => 2019];
            $anio = 2019;
            unset($objetos[9]);
        } else {
            $objetos[8]->Valor = [$anio =>  $anio];
            if ($anio == 2019) {
                unset($objetos[9]);
            } else {
                $objetos[9]->Valor = $this->escoja + $tipo_proyecto;
            }
        }
        // $objetos[8]->ValoAnterior=Input::get('anio');
        // $objetos[8];
        // dd($anio);
        $objetosmain = $this->verpermisos($objetos, "crear", $anio);
        /*Componentes*/
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $com = CoorComponenteModel::where("estado", "ACT")->select(DB::raw("concat(nombre,' - ',detalle) as componente"), "id")
            ->orderBy('nombre', 'ASC')->pluck("componente", "id")->all();
        $objetoscomponentes[0]->Valor = $this->escoja + $com;
        $objetoscomponentes[1]->Valor = $this->escoja;
        $objetoscomponentes[2]->Valor = $this->escoja;
        $objetos = array_merge($objetoscomponentes, $objetosmain[0]);
        /*====================================================*/
        /*====================================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"id_coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $cmb = CoordinacionCabModel::join("users as a", "a.id", "=", "coor_tmae_coordinacion_main.id_responsable")
            //->select("coor_tmae_coordinacion_main.*","a.name as responsable")
            ->select("coor_tmae_coordinacion_main.id", DB::raw("concat(coor_tmae_coordinacion_main.nombre,': ',a.name) as nombre"))
            ->where("coor_tmae_coordinacion_main.estado", "ACT")->pluck("nombre", "id")->all();
        $coordinacion->Valor = $this->escoja + $cmb;
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 4);
        /*====================================================*/
        // dd($objetos);
        $datos = array();
        if ($id == "") {
            $this->configuraciongeneral[2] = "crear";

            $anio = Input::get('anio');
            if ($anio == 2020) {
                $ante = CoordinacionDetaDireccionModel::where("id_direccion", Auth::user()->id_direccion)->first();
                if ($ante) {
                    $ante = array($ante->id_coor_cab);
                    $objetos[4]->ValorAnterior = $ante;
                }
                $objetos[5]->ValorAnterior = Auth::user()->id_direccion;
            }

            // dd($objetos);
            $this->configuraciongeneral[6] = "coordinacioncronograma/poamainajax?anio=" . $anio;
            $this->configuraciongeneral[8] = (isset($anio)) ? "?anio=" . $anio : "?anio=2019";

            $datos = [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            ];
        } else {
            $this->configuraciongeneral[2] = "editar";
            $id = intval(Crypt::decrypt($id));
            //return $this->construyetimeline($id);

            $tabla = $objetosmain[1]->where("poa_tmae_cab.id", $id)->first();
            // dd($id);
            // dd($objetosmain[1]->where("poa_tmae_cab.id", $id));
            $anio = $tabla->anio;
            //show($tabla);
            $valanteriorobj = CoorComponenteObjetivosModel::find($tabla->id_objetivo_componente);
            if ($valanteriorobj) {
                //$valanteriorpro = clone $valanteriorobj;
                $valanteriorpro = $valanteriorobj;
                $valanteriorcom = CoorComponenteModel::find($valanteriorobj->id_componente);
                $objetos[0]->ValorAnterior = $valanteriorcom->id;
                $objetos[1]->Valor = array($valanteriorobj->id => $valanteriorobj->objetivo);
                $objetos[2]->Valor = array($valanteriorpro->id => $valanteriorpro->programa);
            }
            $objetos[8]->ValorAnterior = $tabla->estado_poa;

            $objetos[12]->Valor = [$tabla->anio => $tabla->anio];
            /*====================================*/
            /*====================================*/
            //COORDINACION
            //Valor Anterior
            $ante = CoordinacionDetaDireccionModel::where("id_direccion", $tabla->id_direccion)->first();
            if ($ante) {
                $ante = array($ante->id_coor_cab);
                $objetos[4]->ValorAnterior = $ante;
            }
            //show($coorinacion);
            //show($objetos);
            //////////////////////////////////////////////////CRHISTIAN///////////////////////////////////
            $actividades = json_decode($this->actividades);
            $actividades[0]->Valor = $id;
            $direccion = direccionesModel::where('id', $tabla->id_direccion)->first();
            //show($direccion);
            $actividades[0]->ValorAnterior = $id;
            //Subdirecciones
            $subdireccion = SubDireccionModel::join("tmae_direcciones as a", "a.id", "=", "tmae_direccion_subarea.area")
                ->select("a.direccion", "a.id")
                ->where("tmae_direccion_subarea.id_direccion", $tabla->id_direccion)
                ->orderby("tmae_direccion_subarea.id", "desc")
                ->pluck("direccion", "id")->all();

            //show($subdireccion);
            //Array de las Direcciones para Popup
            $dircombo = ["$direccion->id" => $direccion->direccion] + $subdireccion;
            $actividades[2]->Valor = $dircombo;

            $actividades[2]->ValorAnterior = $direccion->id;
            $actividades[7]->Valor = $this->escoja + $avance;
            $verificacion = explodewords(ConfigSystem("verificacion"), "|");
            $actividades[12]->Valor = $this->escoja + $verificacion;
            if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4) {
                unset($objetos[12]);
                unset($objetos[13]);
            }

            // show($actividades);



            $actividades[8]->Valor = $this->escoja + $estado_act;
            $responsables = UsuariosModel::select(DB::raw("concat(name,' - ',cargo) as name"), "id")->where("estado", "ACT")->where("id_direccion", $tabla->id_direccion)->pluck("name", "id")->all();
            $actividades[3]->Valor = $responsables;

            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

            $actividades_actuales = CoorPoaActividadCabModel::join('tmae_direcciones as d', 'd.id', '=', 'poa_tmae_cab_actividad.id_direccion_act')
                ->select(
                    'poa_tmae_cab_actividad.*',
                    'd.direccion',
                    //Monto Solicitado de Actividad
                    DB::raw("(SELECT sum(monto_solicita) FROM  poa_tmov_autorizacion as xx where xx.id_actividad=poa_tmae_cab_actividad.id and xx.estado_solicitud<>'NEGADA' and xx.estado_solicitud<>'ANULADA') as monto_solicitado")
                )
                ->where(['poa_tmae_cab_actividad.estado' => 'ACT', 'poa_tmae_cab_actividad.id_poa' => $id]);
            //Filtrar Actividades/Hitos de Acuerdo al usuario Logueado
            if ($id_tipo_pefil->tipo == 3) {
                if (Auth::user()->id != 1764) {
                    $actividades_actuales = $actividades_actuales->where("id_direccion_act", Auth::user()->id_direccion);
                }
            }
            $actividades_actuales = $actividades_actuales->get();
            //$subarea = SubDireccionModel::where("area",$id_tipo_pefil->id_direccion)->first();
            //
            $actividades_actuales_collect = collect($actividades_actuales);
            $mult_acttividades = $actividades_actuales_collect->map(function ($actividad) use ($id_tipo_pefil) {
                $responsables = DB::select('select GROUP_CONCAT(name SEPARATOR ", ") as responsables from users where id in (' . $actividad->idusuario_act  . ') ');
                // dd($responsables);
                if (isset($responsables[0])) {
                    $txt_responsable = $responsables[0]->responsables;
                } else {
                    $txt_responsable = '';
                }
                if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || Auth::user()->id == 1764) {
                    $show = '<a href="' . URL::to('') . '/coordinacioncronograma/show_actividad/' . Crypt::encrypt($actividad->id) . '" class="fa fa-newspaper-o divpopup" target="_blank" onclick="popup(this)"></a>';
                    $edit = '<a  onclick="editar(\'' . Crypt::encrypt($actividad->id) . '\')" class="fa fa-pencil-square-o"></a>';
                    $dele = '<a onclick="eliminar(\'' . Crypt::encrypt($actividad->id) . '\')" class="fa fa-trash"></a>';
                    $modalsubir = '<a type="button" class="fa fa-cloud-upload" onClick="modal_subir(' . $actividad->id . ',8)"></a>';
                    $solicitapoa = '<a href="' . URL::to('') . '/coordinacioncronograma/pideautorizacionpoa/' . Crypt::encrypt($actividad->id) . '?menu=no" class="fa fa-file-code-o divpopup" target="_blank" onclick="popup(this)"></a>';
                    if ($actividad->estado_actividad == "SUSPENDIDO") {
                        $solicitapoa = "";
                    }
                    $accion = "$show&nbsp;$edit&nbsp;$dele&nbsp;&nbsp;$modalsubir&nbsp;&nbsp;$solicitapoa";
                } else {
                    $show = '<a href="' . URL::to('') . '/coordinacioncronograma/show_actividad/' . Crypt::encrypt($actividad->id) . '" class="fa fa-newspaper-o divpopup" target="_blank" onclick="popup(this)"></a>';
                    $edit = '<a   onclick="editar(\'' . Crypt::encrypt($actividad->id) . '\')"  class="fa fa-pencil-square-o"></a>';
                    $modalsubir = '<a type="button" class="fa fa-cloud-upload" onClick="modal_subir(' . $actividad->id . ',8)"></a>';

                    $solicitapoa = '<a href="' . URL::to('') . '/coordinacioncronograma/pideautorizacionpoa/' . Crypt::encrypt($actividad->id) . '?menu=no" class="fa fa-file-code-o divpopup" target="_blank" onclick="popup(this)"></a>';
                    if ($actividad->estado_actividad == "SUSPENDIDO") {
                        $solicitapoa = "";
                    }
                    $accion = "$show&nbsp;$edit&nbsp;&nbsp;$modalsubir&nbsp;&nbsp;$solicitapoa";
                }
                //Este Array permite Dibujar la Tabla HTML, LOS Key son los Titulos HEAD del Table
                return [
                    'ID'            => $actividad->id,
                    'Actividad'            => $actividad->actividad,
                    'Dirección'         => $actividad->direccion,
                    'Responsables'     => $txt_responsable,
                    'Fecha Inicio'     => date('Y-m-d', strtotime($actividad->fecha_inicio)),
                    'Fecha Fin'     => date('Y-m-d', strtotime($actividad->fecha_fin)),
                    'Observación'     => $actividad->observacion,
                    'Avance'     => $actividad->avance . ' %',
                    'Estado Actividad'     => $actividad->estado_actividad,
                    'Monto Actividad'     => number_format($actividad->monto_actividad, 2),
                    'Monto Solicitado' => number_format($actividad->monto_solicitado, 2),
                    'Saldo Actual' => number_format(floatval($actividad->monto_actividad) - floatval($actividad->monto_solicitado), 2),
                    'Archivo final' => (isset($actividad->archivo_final)) ? '<a href="' . URL::to('') . '/archivos_sistema/' . $actividad->archivo_final . '" type="button" class="btn btn-success dropdown-toggle divpopup cboxElement" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 2em;"></i></a>' : '',
                    'Verificación'     => $actividad->verificacion,
                    'Observacion de verificación'     => $actividad->observacion_verificacion,
                    'Acción'     => $accion

                ];
            });


            if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 10) {
                // if (Auth::user()->id != 1764 && $anio!=2020) {
                if (Auth::user()->id != 1764) {
                    foreach ($objetos as $key => $value) {
                        foreach ($value as $k => $v) {
                            if ($value->Tipo == 'select' || $value->Tipo == 'select-multiple') {
                                if ($k == 'Nombre') {
                                    $objetos[$key]->Valor = (isset($tabla[$objetos[$key]->Nombre])) ? $tabla[$objetos[$key]->Nombre] : 'Sin datos';
                                }
                            } else {
                                if ($k == 'Nombre') {
                                    $objetos[$key]->Valor = (isset($tabla[$v])) ? $tabla[$v] : 'Sin datos';
                                }
                            }
                        }
                        $objetos[$key]->Tipo = 'html2';
                    }

                    unset($actividades[0]);
                    unset($actividades[1]);
                    unset($actividades[2]);
                    unset($actividades[3]);
                    unset($actividades[4]);
                    unset($actividades[5]);
                    unset($actividades[8]);
                    unset($actividades[9]);
                    unset($actividades[12]);
                    unset($actividades[13]);
                    // dd($actividades);
                }
            }

            if ($anio == 2019 || $anio == null) {
                unset($actividades[11]);
                // dd($actividades);
            } else {

                $estados_tareas = $this->escoja + $estado_act;
                $select_estados = '';
                foreach ($estados_tareas as $key => $value) {
                    # code...
                    $select_estados .= '<option value=\'' . $key . '\'>' . $value . '</option>';
                }

                $avence_estados = '';
                foreach ($avance as $key => $value) {
                    # code...
                    $avence_estados .= '<option value=\'' . $key . '\'>' . $value . '</option>';
                }



                $actividades[11]->Valor = '
                <div style="text-align:end;">
                <button type="button" class="btn btn-primary" disabled id="boton_agregar_tarea" onclick="agregartarea(' . $id . ')"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar tarea</button></div>
               
                    <table class="table" id="tareas_poa_act">
                    <thead>
                        <tr>
                    <th>TAREA</th>
                    <th>FECHA INICIO</th>
                    <th>FECHA FIN</th>
                    <th>AVANCE</th>
                    <th>ESTADO</th>
                    <th>ACCIÓN</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                    </table>
                
                <br>
                <br>
                <br>
              
                <script type="text/javascript">
                var contados_tareas=0;
                $(function () {
                  
                });

                function agregartarea(id){
                    $("#tareas_poa_act").append("<tr id=\'fila_tarea"+contados_tareas+"\' style=\'background: lightgoldenrodyellow;\'><td><textarea style=\'width: 100%;\'></textarea></td><td style=\'width: 10%;\'> <input class=\'datetime_act\'></td><td style=\'width: 10%;\' > <input class=\'datetime_act\'></td><td style=\'width: 1%;\' ><select>' . $avence_estados . '</select></td><td style=\'width: 10%;\'><select>' . $select_estados . '</select></td><td style=\'width: 10%;\'> <button  type=\'button\' onclick=\'guardartarea(' . $id . ',0,"+contados_tareas+")\' class=\'btn btn-secondary\'><i class=\'fa fa-floppy-o\' aria-hidden=\'true\'></i> </button> <button  type=\'button\'  class=\'btn btn-danger\' onclick=\'borrar_tarea("+contados_tareas+")\'><i class=\'fa fa-trash\' aria-hidden=\'true\'></i></button>  </td></tr>");

                   $(".datetime_act").datepicker({ 
                                    todayBtn: "linked",
                                keyboardNavigation: false,
                                forceParse: false,
                                calendarWeeks: false, //True para mostrar Numero de semanas
                                autoclose: true,
                                format: "yyyy-mm-dd",
                                language: "es",
                                // orientation: "auto bottom"
                            });
                             
                    contados_tareas++;
                }
                function agregartareas(){

                    $("#boton_agregar_tarea").attr("disabled",false)
                    $.ajax({
                        type: "GET",
                        url: "' . URL::to('') . '/coordinacioncronograma/get_tareas_poa?id="+' . $id . ',
                        contentType: false,
                        enctype: "multipart/form-data",
                        data: null,
                        processData: false,
                        cache: false,
                        success: function (response) {
                            $("#tareas_poa_act tbody").empty();
                            $.each(response, function(key, element) {
                                $("#tareas_poa_act").append("<tr id=\'fila_tarea"+contados_tareas+"\' style=\'\'><td><textarea style=\'width: 100%;\'>"+element.tarea+"</textarea></td><td style=\'width: 10%;\'> <input class=\'datetime_act\' disabled  value=\'"+element.fecha_inicio+"\'></td><td style=\'width: 10%;\'> <input class=\'datetime_act\' disabled value=\'"+element.fecha_fin+"\'></td><td style=\'width: 1%;\' ><select  id=\'select_avance"+element.id+"\'>' . $avence_estados . '</select></td><td style=\'width: 10%;\'><select disabled id=\'select_estado"+element.id+"\'>' . $select_estados . '</select></td><td style=\'width: 10%;\'> <button  type=\'button\' class=\'btn btn-secondary\' onclick=\'guardartarea(' . $id . ',"+element.id+","+contados_tareas+")\'><i class=\'fa fa-floppy-o\' aria-hidden=\'true\'></i></button> <button class=\'btn btn-danger\' type=\'button\' onclick=\'borrar_tarea("+contados_tareas+")\'><i class=\'fa fa-trash\' aria-hidden=\'true\'></i></button>  </td></tr>");             
                                $(".datetime_act").datepicker({ 
                                    todayBtn: "linked",
                                keyboardNavigation: false,
                                forceParse: false,
                                calendarWeeks: false, //True para mostrar Numero de semanas
                                autoclose: true,
                                format: "yyyy-mm-dd",
                                language: "es",
                                // orientation: "auto bottom"
                            });
                                contados_tareas++;
                                $("#select_avance"+element.id).val(element.avance);
                                $("#select_estado"+element.id).val(element.estado_tarea);
                               
                            }); 
                        }
                      });    
                }

                function borrar_tarea(id){
                    $("#fila_tarea"+id).remove();
                }
                function guardartarea(id,tarea_id,fila){

                    var tarea;
                    var inicio;
                    var fin;
                    var avance;
                    var estado;
                    $("#fila_tarea"+fila).children("td").each(function (index2) {
                        if(index2==0){
                            $(this).children("textarea").each(function (index3) {
                                if(index3==0){
                                  tarea=$(this).val();
                                }
                            });
                        }
                        if(index2==1){
                            $(this).children("input").each(function (index3) {
                                if(index3==0){
                                 inicio=$(this).val();
                                }
                            });
                        }
                        if(index2==2){
                            $(this).children("input").each(function (index3) {
                                if(index3==0){
                                  fin=$(this).val();
                                }
                            });
                        }
                        if(index2==3){
                            $(this).children("select").each(function (index3) {
                                if(index3==0){
                                  avance=$(this).val();
                                }
                            });
                        }
                        if(index2==4){
                            $(this).children("select").each(function (index3) {
                                if(index3==0){
                                  estado=$(this).val();
                                }
                            });
                        }
                    
                    });
                    var data = new FormData();
                      data.append("id",tarea_id);
                      data.append("_token", "' . csrf_token() . '");
                      data.append("id_actividad",' . $id . ');
                      data.append("fecha_inicio", inicio);
                      data.append("fecha_fin", fin);
                      data.append("avance", avance);
                      data.append("tarea", tarea);
                      data.append("estado_tarea", estado);
                     
                      $.ajax({
                          type: "POST",
                          url: "' . URL::to('') . '/coordinacioncronograma/add_act_tarea_poa",
                          data: data,
                          contentType: false,
                          enctype: "multipart/form-data",
                          data: data,
                          processData: false,
                          cache: false,
                          success: function (response) {
                            if(response.estado=="ok"){
                              toastr["success"](response.mensaje);
                            }else{
                              toastr["error"](response.mensaje);
                            }

                          }
                        });    
        

                }

            </script>
                
                ';
            }

            // dd($actividades);
            //////////////////////////////////////////////////CRHISTIAN///////////////////////////////////

            if ($this->configuraciongeneral[2] == "editar") {
                $this->configuraciongeneral[3] = "7"; //Tipo de Referencia de Archivo
                $this->configuraciongeneral[4] = "si";
                $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "=", "pdf"], ["tipo", "7"]])->get();
                $dicecionesImagenes = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "<>", "pdf"], ["tipo", "7"]])->get();
            } else {
                $dicecionesDocumentos = null;
                $dicecionesImagenes = null;
            }
            //Teomporal
            // dd($actividades);


            if (Auth::user()->id == 1654) {

                $botonguardaravance = 'SI';
            } else {
                $botonguardaravance = null;
            }

            return view('vistas.create', [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
                'actividades' => $actividades,
                'mult_acttividades' => $mult_acttividades,
                // 'permisos' => $this->perfil(),
                'dicecionesImagenes' => $dicecionesImagenes,
                'dicecionesDocumentos' => $dicecionesDocumentos,
                'botonguardaravance' => $botonguardaravance,
                // "permisos" => $this->perfil(),
                "permisos" => (($anio == 2020) ? $this->perfil('NO') : $this->perfil()),
                "timeline" => $this->construyetimeline($id)
            ]);
        }

        return view('vistas.create', $datos);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if ($this->perfil()->crear == 'SI' || Input::get('anio') == 2020) {
            return $this->formularioscrear();
        } else {
            Session::flash('error', ' No tiene permiso para crear');
            return Redirect::to($this->configuraciongeneral[1]);
        };
    }


    public function guardar($id)
    {
        $input = Input::all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];
        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new CoorPoaCabModel;
            $msg = "😊 Registro Creado Exitosamente...! ";
            $msgauditoria = "Registro Satisfactorio " . $this->configuraciongeneral[0];
        } else {
            //$ruta .= "/$id/edit";
            $ruta .= "/" . Crypt::encrypt($id) . "/edit";
            $guardar = CoorPoaCabModel::find($id);
            $msg = "😊 Registro Actualizado Exitosamente...!";
            $msgauditoria = "Edición Satisfactoria " . $this->configuraciongeneral[0];
        }

        $input = Input::all();
        $arrapas = array();

        $validator = Validator::make($input, CoorPoaCabModel::rules($id));

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            DB::beginTransaction();
            try {
                $timeline = new CoorPoaDetaHistoModel;
                foreach ($input as $key => $value) {

                    if ($key != "_method" && $key != "_token" && $key != "componente" && $key != "objetivo" && $key != "programa" && $key != "id_coordinacion" && $key != "menu" && $key != "ruta") {
                        $guardar->$key = $value;
                        $timeline->$key = $value;
                    }
                }
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                //Componente
                $guardar->id_objetivo_componente = Input::get("objetivo");
                $guardar->save();
                $idcab = $guardar->id;
                /**/
                $timeline->id_poa_cab = $idcab;
                $timeline->id_usuario = $guardar->id_usuario;
                $timeline->ip = $guardar->ip;
                $timeline->pc = $guardar->pc;
                $timeline->id_objetivo_componente = Input::get("objetivo");
                $timeline->save();
                Auditoria($msgauditoria . " - ID: " . $id);
                DB::commit();
            } //Fin Try
            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage() . " - " . $e->getLine();
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
            Session::flash('message', $msg);
            $anio = $guardar->anio;

            $this->configuraciongeneral[6] = "coordinacioncronograma/poamainajax?anio=" . $anio;
            $this->configuraciongeneral[8] = "?anio=" . $anio;
            $this->configuraciongeneral[1] = $this->configuraciongeneral[1] . $this->configuraciongeneral[8];
            return Redirect::to($this->configuraciongeneral[1]);
        }
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $anio = CoorPoaCabModel::where("id", $id)->first();
        $objetos = $this->verpermisos(json_decode($this->objetos),null,$anio->anio);
        $tabla = $objetos[1]->where("poa_tmae_cab.id", $id)->first();
        // dd($id);

        // $this->configuraciongeneral[3] = "5"; //Tipo de Referencia de Archivo
        // $this->configuraciongeneral[4] = "si";
        $objetos = $objetos[0];
        $objetos[0]->Nombre = "direccion";

        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "timeline" => $this->construyetimeline($id)
        ]);
    }

    public function show_actividad($id)
    {
        $id = intval(Crypt::decrypt($id));
        $actividades_actuales = CoorPoaActividadCabModel::join('tmae_direcciones as d', 'd.id', '=', 'poa_tmae_cab_actividad.id_direccion_act')
            ->select(
                'poa_tmae_cab_actividad.*',
                'd.direccion',
                DB::raw("(SELECT sum(monto_solicita) FROM  poa_tmov_autorizacion as xx where xx.id_actividad=poa_tmae_cab_actividad.id) as monto_solicitado")
            )
            ->where(['poa_tmae_cab_actividad.estado' => 'ACT', 'poa_tmae_cab_actividad.id' => $id])->first();
        // $actividades_actuales_collect = collect($actividades_actuales);
        // $mult_acttividades = $actividades_actuales_collect->map(function ($actividad) {
        //     $responsables = DB::select('select GROUP_CONCAT(name SEPARATOR ", ") as responsables from users where id in (' . $actividad->idusuario_act  . ') ');
        //     if (isset($responsables[0])) {
        //         $txt_responsable = $responsables[0]->responsables;
        //     } else {
        //         $txt_responsable = '';
        //     }
        //     return [
        //         'id'         => $actividad->id,
        //         'actividad'         => $actividad->actividad,
        //         'id_responsable'     => $txt_responsable,
        //         'fecha_inicio'     => date('Y-m-d', strtotime($actividad->fecha_inicio)),
        //         'fecha_fin'     => date('Y-m-d', strtotime($actividad->fecha_fin)),
        //         'observacion'     => $actividad->observacion,
        //         'avance'     => $actividad->avance . ' %',
        //         'estado_actividad'     => $actividad->estado_actividad,

        //     ];
        // });

        $this->configuraciongeneral[3] = "8"; //Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "si";
        // $objetos =  $this->actividades;
        // dd($mult_acttividades[0]['']);
        $dicecionesImagenes = ArchivosModel::where([["id_referencia", $actividades_actuales['id']], ["tipo_archivo", "<>", "pdf"], ["tipo", "8"]])->get();
        $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $actividades_actuales['id']], ["tipo_archivo", "=", "pdf"], ["tipo", "8"]])->get();

        $timelineProyecto = CoorPoaActividadDetaCabModel::select("poa_tmae_cab_actividad_deta.*")
            // ->join("poa_tmae_cab_actividad as p", "poa_tmae_cab_actividad.id_poa", "=", "p.id")
            // ->where("coor_tmov_proyecto_estrategico_deta.estado","ACT")
            ->where("poa_tmae_cab_actividad_deta.id_poa_act_cab", $id)
            ->orderBy("poa_tmae_cab_actividad_deta.updated_at", "ASC")
            ->get();
        // dd($timelineProyecto);
        $objetos = json_decode($this->actividades);
        unset($objetos[0]);
        unset($objetos[3]);
        unset($objetos[2]);
        $obj = json_decode('[{"Tipo":"text","Descripcion":"Monto Solicitado","Nombre":"monto_solicitado","Clase":" Null","Valor":"Null","ValorAnterior" :"Null" }]');
        $objetos = array_merge($objetos, $obj);
        //show($objetos);
        return view('vistas.show', [
            //"timelineProyecto" => null,
            'objetos' => $objetos,
            'tabla' => $actividades_actuales,
            "configuraciongeneral" => $this->configuraciongeneral,
            'dicecionesImagenes' => $dicecionesImagenes,
            'dicecionesDocumentos' => $dicecionesDocumentos

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
        //        $id = intval(Crypt::decrypt($id));
        //        $objetos = $this->verpermisos(json_decode($this->objetos), "editar");
        //        $tabla = $objetos[1]->where("poa_tmae_cab.id", $id)->first();
        //
        //        $tbcom=direccionesModel::where("estado","ACT")->orderby("direccion")->pluck("direccion", "id")->all();
        //        $estadopoa = explodewords(ConfigSystem("estadopoa"), "|");
        //        $objetos[0][0]->Valor=$this->escoja + $tbcom;
        //        $objetos[0][4]->Valor=$this->escoja + $estadopoa;
        //        $objetos[0][4]->ValorAnterior=$tabla->estado_poa;
        //        //show($tabla);
        //        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        //        $this->configuraciongeneral[2] = "editar";
        //        return view('vistas.create', [
        //            "objetos" => $objetos[0],
        //            "configuraciongeneral" => $this->configuraciongeneral,
        //            "validarjs" => $this->validarjs,
        //            "tabla" => $tabla/*,
        //            "dicecionesImagenes" => $dicecionesImagenes,
        //            "dicecionesDocumentos" => $dicecionesDocumentos*/
        //        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $tabla = CoorPoaCabModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        CoorPoaDetaHistoModel::where('id_poa_cab', $id)
            ->update(['estado' => 'INA']);
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroyactividad($id)
    {
        //
        /*
         use Modules\CoordinacionCronograma\Entities\CoorPoaActividadCabModel;
         use Modules\CoordinacionCronograma\Entities\CoorPoaActividadDetaCabModel;
         * */
        $tabla = CoorPoaActividadCabModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        CoorPoaActividadDetaCabModel::where('id_poa_cab', $id)
            ->update(['estado' => 'INA']);
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }






    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////   poa actividad ////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    public function store_actividad()
    {
        $id_encriptado = Input::get('id');
        $id = 0;
        if ($id_encriptado != null) {
            try {
                $id = Crypt::decrypt($id_encriptado);
            } catch (Exception $e) {
                $id = 0;
            };
        }
        return $this->guardar_act($id);
    }

    public function guardar_act($id)
    {
        $input = Input::all();
        // show($input);
        $ruta = $this->configuraciongeneral[1];
        if ($id == 0) {
            $guardar = new CoorPoaActividadCabModel;
            $msg = "😊 Registro Creado Exitosamente...! ";
            $msgauditoria = "Registro Satisfactorio " . $this->configuraciongeneral[0];
        } else {
            //$ruta .= "/$id/edit";
            $ruta .= "/" . Crypt::encrypt($id) . "/edit";
            $guardar = CoorPoaActividadCabModel::find($id);
            $msg = "😊 Registro Actualizado Exitosamente...!";
            $msgauditoria = "Edición Satisfactoria " . $this->configuraciongeneral[0];
        }
        $input = Input::all();
        // dd($input);
        // $anio = CoorPoaCabModel::where(['estado' => 'ACT', 'id' => $input['id_poa']])->first();
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        // if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3  || $id_tipo_pefil->tipo == 10) && Auth::user()->id != 1764 && $anio->anio!=2020) {
        if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3  || $id_tipo_pefil->tipo == 10) && Auth::user()->id != 1764) {
            // if ($id != 0 && Input::get('avance') < $guardar->avance) {
            if ($id != 0 && Input::get('avance') < $guardar->avance) {
                return  response()->json(['estado' => 'false', 'mensaje' => 'El avance no puede ser menor de ' . $guardar->avance . '%'], 200);
            }

            // dd($anio);

            $validator = Validator::make($input, CoorPoaActividadCabModel::rulesdir($id));
        } else {
            $validator = Validator::make($input, CoorPoaActividadCabModel::rules($id));
        }

        if ($validator->fails()) {
            $mensajes = $validator->getMessageBag()->getMessages();
            $mensaje = '';
            foreach ($mensajes as $key => $value) {

                $mensaje .= $value[0] . '<br>';
            }


            return  response()->json(['estado' => 'false', 'mensaje' => $mensaje], 200);
        } else {
            DB::beginTransaction();
            try {
                $timeline = new CoorPoaActividadDetaCabModel();
                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token" && $key != 'id') {

                        if ($key == 'archivo_final') {

                            $dir = public_path() . '/archivos_sistema/';
                            $docs = Input::file("archivo_final");
                            if ($docs) {
                                // dd($key);
                                $fileName = "Archivo_Final-poa_actividades-$id" . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                                $ext = $docs->getClientOriginalExtension();
                                $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                                if (!in_array($ext, $perfiles)) {
                                    DB::rollback();
                                    $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                                    return  response()->json(['estado' => 'false', 'mensaje' => 'No se admite el tipo de archivo'], 200);
                                }


                                //shoW();
                                $oldfile = $dir . '' . $fileName;
                                if (is_file($oldfile)) {
                                    // dd($id_tipo_pefil->tipo);
                                    Log::info($oldfile);
                                    if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $guardar->verificacion == "VERIFICADO CON OBSERVACIONES") {
                                        if (file_exists($oldfile)) {
                                            unlink($oldfile);
                                            Log::info('Borrado');
                                        }
                                    } else {
                                        DB::rollback();
                                        $mensaje = "Ya se encuentra el archivo final registrado";
                                        Log::info($mensaje . ' ' . $oldfile);
                                        return  response()->json(['estado' => 'false', 'mensaje' => $mensaje], 200);
                                        // return redirect()->back()->withErrors([$mensaje])->withInput();
                                    }
                                }
                                $docs->move($dir, $fileName);
                                $guardar->$key = $fileName;
                                $timeline->$key = $fileName;
                            }
                            // dd('paso');
                        } else {
                            $guardar->$key = $value;
                            $timeline->$key = $value;
                        }
                    }
                }
                if (Input::get('avance') == 100) {
                    $guardar->estado_actividad = 'EJECUTADO';
                }
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                $guardar->save();
                $idcab = $guardar->id;
                /**/

                ////// porque en actualizar observacion solo actualizo la obeservacion y el avance y en
                ///  el for no se halan
                $timeline->id_poa = $guardar->id_poa;
                $timeline->actividad = $guardar->actividad;
                $timeline->id_direccion_act = $guardar->id_direccion_act;
                $timeline->idusuario_act = $guardar->idusuario_act;
                $timeline->fecha_inicio = $guardar->fecha_inicio;
                $timeline->fecha_fin = $guardar->fecha_fin;
                $timeline->observacion = $guardar->observacion;
                $timeline->avance = $guardar->avance;
                $timeline->estado_actividad = $guardar->estado_actividad;
                $timeline->id_poa_act_cab = $idcab;
                $timeline->id_usuario = $guardar->id_usuario;
                $timeline->ip = $guardar->ip;
                $timeline->pc = $guardar->pc;




                if ($guardar->verificacion == 'VERIFICADO CON OBSERVACIONES' && $id_tipo_pefil->tipo == 4) {


                    $usuario = UsuariosModel::select("users.*")
                        ->join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
                        ->where(["id_direccion" => $guardar->id_direccion])
                        ->whereIn("p.tipo", [3, 10])
                        ->get();
                    // dd($usuario);

                    $guardar_verificacion = CoorPoaActividadCabModel::find($idcab);
                    $guardar_verificacion->avance = 60;
                    $guardar_verificacion->estado_actividad = 'EJECUCION_VENCIDO';
                    $guardar_verificacion->save();

                    $this->configuraciongeneral[6] = "coordinacioncronograma/poamainajax?anio=" .  $guardar->anio;
                    $this->configuraciongeneral[8] = "?anio=" .  $guardar->anio;
                    $this->configuraciongeneral[1] = $this->configuraciongeneral[1] . $this->configuraciongeneral[8];
                    foreach ($usuario as $key => $value_) {
                        # code...
                        $this->notificacion->notificacionesweb("Actividad del POA " . $guardar->anio . " <b>#</b>" . $idcab . "", $this->configuraciongeneral[1], $value_->id, "2c438f");
                        $this->notificacion->EnviarEmail($value_->email, "Actividad del POA " . $guardar->anio, "Actividad del POA ha sido verificado con observaciones", $guardar->observacion_verificacion,  $this->configuraciongeneral[1]);
                    }
                }



                $timeline->save();
                DB::commit();
            } //Fin Try
            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                // return  $e->getMessage();
                return  response()->json(['estado' => 'false', 'mensaje' => 'No se pudo grabar intente de nuevo ' . $e->getMessage()], 200);
            }

            return  response()->json(['estado' => 'ok', 'mensaje' => $msg], 200);
        }
    }
    public function eliminar_actividad_poa($id)
    {
        //

        try {
            $id = Crypt::decrypt($id);

            $tabla = CoorPoaActividadCabModel::find($id);
            $tabla->estado = 'INA';
            $tabla->save();
            CoorPoaActividadDetaCabModel::where('id_poa_act_cab', $id)
                ->update(['estado' => 'INA']);
            return response()->json(['estado' => 'ok', 'msg' => 'Registro dado de Baja!']);
        } catch (Exception $e) {
            return response()->json(['estado' => 'error', 'msg' => 'No se pudo completar la acción....']);
        }
    }
    public function get_actividad_poa($id)
    {
        //

        try {
            $id = Crypt::decrypt($id);
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
            $avance = explodewords(ConfigSystem("avance"), "|");
            $anio = CoorPoaActividadCabModel::select('anio')->join('poa_tmae_cab as poa', 'poa.id', '=', 'poa_tmae_cab_actividad.id_poa')->where(['poa_tmae_cab_actividad.estado' => 'ACT', 'poa_tmae_cab_actividad.id' => $id])->first();
            // dd('ss');
            // if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) && Auth::user()->id != 1764 && $anio->anio!=2020) {
            if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) && Auth::user()->id != 1764) {
                $actividad = CoorPoaActividadCabModel::select('observacion', 'avance')->where(['poa_tmae_cab_actividad.estado' => 'ACT', 'id' => $id])->first();
                return response()->json(['datos' => $actividad, 'avance' => $avance, 'alto_obs' => '300px']);
            } else {
                $actividad = CoorPoaActividadCabModel::where(['poa_tmae_cab_actividad.estado' => 'ACT', 'id' => $id])->first();
                $actividad->fecha_inicio = date('Y-m-d', strtotime($actividad->fecha_inicio));
                $actividad->fecha_fin = date('Y-m-d', strtotime($actividad->fecha_fin));
                $responsables = UsuariosModel::select(DB::raw("concat(name,' - ',cargo) as name"), "id")->where("estado", "ACT")->where("id_direccion", $actividad->id_direccion_act)->pluck("name", "id")->all();
                return response()->json(['datos' => json_decode($actividad), 'responsables' => $responsables, 'avance' => $avance, 'alto_obs' => '80px']);
            }
        } catch (Exception $e) {
            // dd($e->getMessage());
            return response()->json(['estado' => 'error', 'msg' => 'No se pudo completar la acción....']);
        }
    }


    public function guardar_act_tarea()
    {
        $input = Input::all();

        $id = Input::get('id');
        $ruta = $this->configuraciongeneral[1];
        if ($id == 0) {
            $guardar = new CoorPoaActividadesTareasModel();
            $msg = "😊 Registro Creado Exitosamente...! ";
            $msgauditoria = "Registro Satisfactorio " . $this->configuraciongeneral[0];
        } else {
            //$ruta .= "/$id/edit";
            $ruta .= "/" . Crypt::encrypt($id) . "/edit";
            $guardar = CoorPoaActividadesTareasModel::find($id);
            $msg = "😊 Registro Actualizado Exitosamente...!";
            $msgauditoria = "Edición Satisfactoria " . $this->configuraciongeneral[0];
        }
        $input = Input::all();
        // dd($input);

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3  || $id_tipo_pefil->tipo == 10) && Auth::user()->id != 1764) {
            if ($id != 0 && Input::get('avance') < $guardar->avance) {
                return  response()->json(['estado' => 'false', 'mensaje' => 'El avance no puede ser menor de ' . $guardar->avance . '%'], 200);
            }




            $validator = Validator::make($input, CoorPoaActividadesTareasModel::rulesdir($id));
        } else {
            $validator = Validator::make($input, CoorPoaActividadesTareasModel::rules($id));
        }

        if ($validator->fails()) {
            $mensajes = $validator->getMessageBag()->getMessages();
            $mensaje = '';
            foreach ($mensajes as $key => $value) {

                $mensaje .= $value[0] . '<br>';
            }


            return  response()->json(['estado' => 'false', 'mensaje' => $mensaje], 200);
        } else {
            DB::beginTransaction();
            try {
                $timeline = new CoorPoaActividadesTareasDetaModel();
                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token" && $key != 'id') {


                        $guardar->$key = $value;
                        $timeline->$key = $value;
                    }
                }
                if (Input::get('avance') == 100) {
                    $guardar->estado_tarea = 'EJECUTADO';
                }
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                $guardar->save();
                $idcab = $guardar->id;
                $timeline->id_tarea_cab = $idcab;


                $timeline->save();
                DB::commit();
            } //Fin Try
            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                return  $e->getMessage();
                return  response()->json(['estado' => 'false', 'mensaje' => 'No se pudo grabar intente de nuevo'], 200);
            }

            return  response()->json(['estado' => 'ok', 'mensaje' => $msg], 200);
        }
    }

    public function get_tareas_poa()
    {
        $id = Input::get('id');
        $actividades = CoorPoaActividadesTareasModel::where(['id_actividad' => $id, 'estado' => 'ACT'])->get();
        return $actividades;
    }
}
