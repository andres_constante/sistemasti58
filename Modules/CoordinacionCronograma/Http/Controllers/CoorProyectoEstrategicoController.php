<?php

namespace Modules\Coordinacioncronograma\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Modules\Coordinacioncronograma\Entities\CoorTipoProyectoModel;
use Modules\Coordinacioncronograma\Entities\CoorTipoProyectoDetalleModel;
use Modules\Coordinacioncronograma\Entities\CoorProyectoEstrategicoModel;
use App\ArchivosModel;
use App\Http\Controllers\NotificacionesController;
use app\SubDireccionModel;
use App\UsuariosModel;
use Auth;
use Exception;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\URL;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteObjetivosModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionCabModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionDetaDireccionModel;
use Modules\CoordinacionCronograma\Entities\CoordinacionIndicadorProyectoModel;
use Modules\CoordinacionCronograma\Entities\CoorMetaOdsModel;
use Modules\CoordinacionCronograma\Entities\CoorOdsModel;
use Modules\CoordinacionCronograma\Entities\CoorProyectoEstrategicoActividades;
use Modules\CoordinacionCronograma\Entities\CoorProyectoEstrategicoActividadesDeta;
use Modules\CoordinacionCronograma\Entities\ObraActividadModel;
use Modules\CoordinacionCronograma\Http\Controllers\CoorComponentesProgramaController;
use stdClass;

class CoorProyectoEstrategicoController extends Controller
{

	var $configuraciongeneral = array("Proyectos Estratégicos", "coordinacioncronograma/proyecto", "index", 6 => "null", 7 => "proyecto", 9 => "add_act_proyecto", 10 => "get_actividad_proyecto", 11 => "eliminar_actividad_proyecto");
	var $escoja = array(null => "Escoja opción...");
	// var $avance= array(0 => "0%" , 10 => "10%", 20 => "20%", 30 => "30%", 40 => "40%", 50 => "50%", 60 => "60%", 70 => "70%", 80 => "80%", 90 => "90%", 100 => "100%");
	var $objetos = '[
        {"Tipo":"select","Descripcion":"Componente","Nombre":"id_componente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"html","Descripcion":"Visión de desarrollo","Nombre":"vision","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Objetivos","Nombre":"id_objetivo_estra","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"ODS","Nombre":"id_ods","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Meta ODS","Nombre":"id_meta_ods","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"html","Descripcion":"Programa","Nombre":"programa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"html","Descripcion":"Conceptualización","Nombre":"conceptualizacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Indicador","Nombre":"indicador","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Código programa","Nombre":"codigo_programa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Plan","Nombre":"id_tipo_proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Código proyecto","Nombre":"codigo_proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Proyecto estratégico","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },	
        {"Tipo":"select","Descripcion":"Estado del proyecto","Nombre":"estado_proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Monto","Nombre":"monto","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"select","Descripcion":"Fase","Nombre":"fase","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';

	var $actividades = '
        [            
            {"Tipo":"text","Descripcion":"Poa","Nombre":"id_proyecto","Clase":" hidden","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"textarea","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion_act","Clase":"12","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select-multiple","Descripcion":"Responsable","Nombre":"idusuario_act","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"datetext","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"datetext","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"textarea","Descripcion":"Explicación Actividad","Nombre":"observacion","Clase":"mostrarobservaciones maxlength:350","Valor":"Null","ValorAnterior" :"Null" } ,
            {"Tipo":"select","Descripcion":"Avance %","Nombre":"avance","Clase":"avance","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"file","Descripcion":"Archivo final","Nombre":"archivo_final","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" },    
            {"Tipo":"select","Descripcion":"Verificación","Nombre":"verificacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"textarea","Descripcion":"Observación de la verificación","Nombre":"observacion_verificacion","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" }   
        ]
    ';
	//https://jqueryvalidation.org/validate/
	var $validarjs = array(
		"id_tipo_proyecto" => "id_tipo_proyecto: {
						required: true
					}",
		"estado_proyecto" => "estado_proyecto: {
						required: true
					}"

	);


	protected $notificacion;

	public function __construct(NotificacionesController $notificacion)
	{
		$this->middleware('auth');
		$this->notificacion = $notificacion;
	}

	public function index()
	{
		// $tabla=CoorProyectoEstrategicoModel::where("estado","ACT")->orderby("id","desc")->get();//->paginate(500);
		$objetos = json_decode($this->objetos);
		$objetos[0]->Nombre = "componente";
		$objetos[2]->Nombre = "objetivo";
		$objetos[3]->Nombre = "ods";
		$objetos[4]->Nombre = "meta_ods";
		$objetos[5]->Nombre = "programa";
		$objetos[9]->Nombre = "tipo";
		$objetos = array_values($objetos);
		//$this->calcularMeta();
		//show($objetos);
		// $tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as tp", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "tp.id")
		// 	->select("coor_tmov_proyecto_estrategico.*", "tp.tipo")
		// 	->where("coor_tmov_proyecto_estrategico.estado", "ACT")
		// 	->get();

		unset($objetos[15]);
		unset($objetos[18]);
		unset($objetos[7]);
		unset($objetos[22]);

		// show($objetos);

		$tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as tp", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "tp.id")
			->join("coor_tmae_cronograma_componente_cab as c", "coor_tmov_proyecto_estrategico.id_componente", "=", "c.id")
			->join("coor_tmae_cronograma_componente_objetivo as c_o", "coor_tmov_proyecto_estrategico.id_objetivo_estra", "=", "c_o.id")
			->join("coor_tmae_ods as o", "coor_tmov_proyecto_estrategico.id_ods", "=", "o.id")
			->join("coor_tmae_meta_ods as m_o", "coor_tmov_proyecto_estrategico.id_meta_ods", "=", "m_o.id")
			->select(
				"coor_tmov_proyecto_estrategico.*",
				"tp.tipo",
				"c.vision",
				"c.nombre as componente",
				"c_o.objetivo as objetivo",
				"c_o.conceptualizacion",
				"o.ods",
				"m_o.meta_ods",
				"m_o.meta_ods",
				"c_o.programa",
				"codigo_programa as codigo ",
				'tp.codigo as codigo_plan'
			)
			->where(["coor_tmov_proyecto_estrategico.estado" => "ACT"])
			->get();
		// $parroquias = str_replace('|', ',', $tabla->id_parroquia);

		// $parroquias = DB::select('select GROUP_CONCAT(parroquia SEPARATOR ", ") as id_parroquia from parroquia where id in (' . $parroquias . ') ');
		// $tabla['id_parroquia'] = $parroquias[0]->id_parroquia;
		// $direciones = str_replace('|', ',', $tabla->responsables);
		// $direciones = DB::select('select GROUP_CONCAT(direccion SEPARATOR ", ") as responsables from tmae_direcciones where id in (' . $direciones . ') ');
		// $tabla['responsables'] = $direciones[0]->responsables;




		return view('vistas.index', [
			"objetos" => $objetos,
			"tabla" => $tabla,
			"configuraciongeneral" => $this->configuraciongeneral,
			"delete" => "si",
			"create" => "si"
		]);
	}


	public function perfil()
	{
		$id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
		//show($id_tipo_pefil);
		$object = new stdClass;
		$object->perfil = $id_tipo_pefil->tipo;
		if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4|| $id_tipo_pefil->tipo == 10) { //1 Administrador 4 Coordinador
			// dd($id_tipo_pefil->tipo);
			$object->crear = 'SI';
			$object->ver = 'SI';
			$object->editar = 'SI';
			$object->eliminar = 'SI';
			$object->editar_parcialmente = 'SI';
			$object->guardar = 'SI';
		} else if ($id_tipo_pefil->tipo == 3) {
			$object->crear = 'SI';
			$object->ver = 'SI';
			$object->editar = 'SI';
			$object->eliminar = 'NO';
			$object->editar_parcialmente = 'SI';
			$object->guardar = 'SI';
		} else if ($id_tipo_pefil->tipo == 7) {
			$object->crear = 'SI';
			$object->ver = 'SI';
			$object->editar = 'SI';
			$object->eliminar = 'NO';
			$object->editar_parcialmente = 'SI';
			$object->guardar = 'SI';
		} else {
			$object->crear = 'NO';
			$object->ver = 'SI';
			$object->editar = 'NO';
			$object->eliminar = 'NO';
			$object->editar_parcialmente = 'SI';
		}
		//show($object);
		return $object;
	}
	public function create()
	{
		//
		$this->configuraciongeneral[2] = "crear";
		$seleccionar_tipo = CoorTipoProyectoModel::where(["estado" => "ACT", 'tipo_componente' => 2])->pluck("tipo", "id")->all();
		$seleccionar_componente = CoorComponenteModel::where("estado", "ACT")->orderBy('nombre','ASC')->pluck("nombre", "id")->all();
		$objetos = json_decode($this->objetos);
		$objetos[0]->Valor = $this->escoja + $seleccionar_componente;
		$objetos[2]->Valor = [];
		$objetos[3]->Valor = [];
		$objetos[4]->Valor = [];
		$objetos[9]->Valor = $this->escoja + $seleccionar_tipo;

		$indicadores = CoordinacionIndicadorProyectoModel::where('estado', 'ACT')->orderBy('indicador','ASC')->pluck("indicador", "id")->all();
		$objetos[7]->Valor = $indicadores;


		$avance = explodewords(ConfigSystem("estado_proyetos"), "|");
		$objetos[10]->Valor = [];
		$objetos[13]->Valor = $this->escoja + $avance;



		$objetos[15]->Valor = [
			'2020' => '2020-RUPTURA',
			'2023' => '2023-FORTALECIMIENTO',
			'2030' => '2030-CONSOLIDACION',
			'2035' => '2035-DIVERSIFICACION',
		];

		//COORDINACION
		$coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"id_coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
		$coordinacion = json_decode($coordinacion);
		//show($coorinacion);

		$cmb = CoordinacionCabModel::join("users as a", "a.id", "=", "coor_tmae_coordinacion_main.id_responsable")
			//->select("coor_tmae_coordinacion_main.*","a.name as responsable")
			->select("coor_tmae_coordinacion_main.id", DB::raw("concat(coor_tmae_coordinacion_main.nombre,': ',a.name) as nombre"))
			->where("coor_tmae_coordinacion_main.estado", "ACT")->pluck("nombre", "id")->all();

		$coordinacion->Valor = $this->escoja + $cmb;
		$objetos = insertarinarray($objetos, $coordinacion, 10);
		// show($objetos);
		return view('vistas.create', [
			"objetos" => $objetos,
			"configuraciongeneral" => $this->configuraciongeneral,
			"validarjs" => $this->validarjs
		]);
	}

	public function store(Request $request)
	{
		//
		return $this->guardar(0);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
		$objetos = json_decode($this->objetos);
		$objetos[0]->Nombre = "componente";
		$objetos[2]->Nombre = "objetivo";
		$objetos[3]->Nombre = "ods";
		$objetos[4]->Nombre = "meta_ods";
		$objetos[9]->Nombre = "tipo";
		$objetos = array_values($objetos);
		$timelineProyecto = $this->getTimeLineCab($id);

		$tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as tp", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "tp.id")
			->join("coor_tmae_cronograma_componente_cab as c", "coor_tmov_proyecto_estrategico.id_componente", "=", "c.id")
			->join("coor_tmae_cronograma_componente_objetivo as c_o", "coor_tmov_proyecto_estrategico.id_objetivo_estra", "=", "c_o.id")
			->join("coor_tmae_ods as o", "coor_tmov_proyecto_estrategico.id_ods", "=", "o.id")
			->join("coor_tmae_meta_ods as m_o", "coor_tmov_proyecto_estrategico.id_meta_ods", "=", "m_o.id")
			->select(
				"coor_tmov_proyecto_estrategico.*",
				"tp.tipo",
				"c.nombre as componente",
				"c.vision",
				"c_o.objetivo as objetivo",
				"c_o.programa",
				"o.ods",
				"m_o.meta_ods",
				"c_o.conceptualizacion",
				"codigo_programa as codigo",
				'tp.codigo as codigo_plan'
			)
			->where(["coor_tmov_proyecto_estrategico.estado" => "ACT", 'coor_tmov_proyecto_estrategico.id' => $id])
			->first();
		$parroquias = str_replace('|', ',', $tabla->id_parroquia);

		if ($parroquias) {
			$parroquias = DB::select('select GROUP_CONCAT(parroquia SEPARATOR ", ") as id_parroquia from parroquia where id in (' . $parroquias . ') ');
			$tabla['id_parroquia'] = $parroquias[0]->id_parroquia;
		}
		$direcciones = str_replace('|', ',', $tabla->responsables);
		if ($direcciones) {
			$direcciones = DB::select('select GROUP_CONCAT(direccion SEPARATOR ", ") as responsables from tmae_direcciones where id in (' . $direcciones . ') ');
			$tabla['responsables'] = $direcciones[0]->responsables;
		}
		$indicadores = str_replace('|', ',', $tabla->indicador);
		if ($indicadores) {
			$indicadores = DB::select('select GROUP_CONCAT(indicador SEPARATOR ", ") as indicadores from coor_tmae_indicador where id in (' . $indicadores . ') ');
			$tabla['indicador'] = $indicadores[0]->indicadores;
			// dd($indicadores);
		}
		return view('vistas.show', [
			"timelineProyecto" => $timelineProyecto,
			"objetos" => $objetos,
			"tabla" => $tabla,
			"configuraciongeneral" => $this->configuraciongeneral
		]);
	}

	public function guardar($id)
	{
		$input = Input::all();
		// dd($input);
		//$this->calcularMeta();
		//show($this->calcularMeta());
		$ruta = $this->configuraciongeneral[1];
		DB::beginTransaction();
		try {
			if ($id == 0) {
				$ruta .= "/create";
				$guardar = new CoorProyectoEstrategicoModel;
				$msg = "Registro Creado Exitosamente...!";
				$msgauditoria = "Registro Variable de Configuración";
			} else {
				$ruta .= "/$id/edit";
				$guardar = CoorProyectoEstrategicoModel::find($id);
				$msg = "Registro Actualizado Exitosamente...!";
				$msgauditoria = "Edición Variable de Configuración";
			}

			$input = Input::all();
			$arrapas = array();

			$validator = Validator::make($input, CoorProyectoEstrategicoModel::rules($id));

			if ($validator->fails()) {
				//die($ruta);
				return Redirect::to("$ruta")
					->withErrors($validator)
					->withInput();
			} else {
				$timeLine = new CoorTipoProyectoDetalleModel;

				foreach ($input as $key => $value) {

					if ($key != "_method" && $key != "_token" && $key != "id_coordinacion" && $key != "ruta") {
						if ($key == "monto")
							$value = strval(floatval(str_replace(",", "", $value)));

						if ($key == "responsables" || $key == 'id_parroquia' || $key == 'indicador')
							$value = strval(implode('|', $value));
						// dd($value);


						$guardar->$key = $value;

						$timeLine->$key = $value;
					}
				}



				$guardar->id_usuario = Auth::user()->id;
				$guardar->ip = \Request::getClientIp();
				$guardar->pc = \Request::getHost();

				$guardar->save();

				$idcab = $guardar->id;
				$timeLine->ip = \Request::getClientIp();
				$timeLine->pc = \Request::getHost();
				$timeLine->id_cab_pro = $idcab;
				$timeLine->id_usuario = $guardar->id_usuario;
				$timeLine->save();


				


				//Auditoria($msgauditoria . " - ID: " . $id . "-" . Input::get($guardar->nombre));
			}
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			return $mensaje = $e;
			return redirect()->back()->withErrors([$mensaje])->withInput();
		}
		Session::flash('message', $msg);
		return Redirect::to($this->configuraciongeneral[1].'/'.$idcab.'/edit');
	}

	public function edit($id)
	{
		//

		$id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
		if ($id_tipo_pefil->tipo == 5  || $id_tipo_pefil->tipo == 6) {
			return Redirect::to($this->configuraciongeneral[1]);
		}

		$this->configuraciongeneral[2] = "editar";
		$tabla = CoorProyectoEstrategicoModel::find($id);
		$objetos = json_decode($this->objetos);

		$avance = explodewords(ConfigSystem("estado_proyetos"), "|");

		$seleccionar_tipo = CoorTipoProyectoModel::where(["estado" => "ACT", 'tipo_componente' => 2])->pluck("tipo", "id")->all();
		$seleccionar_componente = CoorComponenteModel::where("estado", "ACT")->orderBy('nombre','ASC')->pluck("nombre", "id")->all();
		$objetos = json_decode($this->objetos);
		$objetos[0]->Valor = $this->escoja + $seleccionar_componente;
		$objetos[0]->ValorAnterior = $tabla->id_componente;

		$objetivos = CoorComponenteObjetivosModel::where(['estado' => 'ACT', 'id_componente' => $tabla->id_componente])->pluck('objetivo', 'id')->all();
		$objetos[2]->Valor = $objetivos;
		$objetos[2]->ValorAnterior = $tabla->id_objetivo_estra;

		$ods = CoorOdsModel::where(['estado' => 'ACT', 'id_componente' => $tabla->id_componente])->pluck('ods', 'id')->all();
		$objetos[3]->Valor = $ods;
		$objetos[3]->ValorAnterior = $tabla->id_ods;

		$meta_ods = CoorMetaOdsModel::where(['estado' => 'ACT', 'id_ods' => $tabla->id_ods])->pluck('meta_ods', 'id')->all();
		$objetos[4]->Valor = $meta_ods;
		$objetos[4]->ValorAnterior = $tabla->id_meta_ods;
		$objetos[9]->Valor = $this->escoja + $seleccionar_tipo;
		$objetos[9]->ValorAnterior = $tabla->id_tipo_proyecto;
		$avance = explodewords(ConfigSystem("estado_proyetos"), "|");


		$objetos[10]->Valor = ['NO' => 'NO', 'SI' => 'SI'];
		$objetos[10]->ValorAnterior = $tabla->plan_centenario;
		$objetos[13]->Valor = $this->escoja + $avance;
		$objetos[13]->ValorAnterior = $tabla->estado_proyecto;

		$objetos[15]->Valor = [
			'2020' => '2020-RUPTURA',
			'2023' => '2023-FORTALECIMIENTO',
			'2030' => '2030-CONSOLIDACION',
			'2035' => '2035-DIVERSIFICACION',
		];


		$indicadores = CoordinacionIndicadorProyectoModel::where('estado', 'ACT')->pluck("indicador", "id")->all();
		$objetos[7]->Valor = $indicadores;
		$indicadores = array();
		$indicadores = explode('|', $tabla->indicador);
		// if($tabla->indicadores !=null){
		$indicadores_v = array();
		foreach ($indicadores as $key => $value) {
			$indicadores_v[$value] = $value;
		}
		$objetos[7]->ValorAnterior =  $indicadores_v;
		// }
		// dd($objetos[7]);


		$direcciones = direccionesModel::where('estado', 'ACT')->pluck('direccion', 'id')->all();
		$objetos[10]->Valor = $direcciones;

		$direcciones_v[$value] = $value;

		$objetos[10]->ValorAnterior =   $tabla->id_direccion;


		//COORDINACION
		$coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"id_coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
		$coordinacion = json_decode($coordinacion);
		//show($coorinacion);

		$cmb = CoordinacionCabModel::join("users as a", "a.id", "=", "coor_tmae_coordinacion_main.id_responsable")
			//->select("coor_tmae_coordinacion_main.*","a.name as responsable")
			->select("coor_tmae_coordinacion_main.id", DB::raw("concat(coor_tmae_coordinacion_main.nombre,': ',a.name) as nombre"))
			->where("coor_tmae_coordinacion_main.estado", "ACT")->pluck("nombre", "id")->all();

		$coordinacion->Valor = $this->escoja + $cmb;
		$objetos = insertarinarray($objetos, $coordinacion, 10);
		// show($objetos);
		$ante = CoordinacionDetaDireccionModel::where("id_direccion", $tabla->id_direccion)->first();
		if ($ante) {
			$ante = array($ante->id_coor_cab);
			$objetos[10]->ValorAnterior = $ante;
		}


		$this->configuraciongeneral[3] = "3";
		$this->configuraciongeneral[4] = "si";




		//////////////////////////////////////////////////CRHISTIAN///////////////////////////////////
		$Avance = explodewords(ConfigSystem("avance"), "|");
		$estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
		$actividades = json_decode($this->actividades);
		$actividades[0]->Valor = $id;
		$actividades[0]->ValorAnterior = $id;
		$direccion = direccionesModel::where('id', $tabla->id_direccion)->first();
		//show($direccion);
		$actividades[0]->ValorAnterior = $id;
		//Subdirecciones
		$subdireccion = DB::table('tmae_direccion_subarea')->join("tmae_direcciones as a", "a.id", "=", "tmae_direccion_subarea.area")
			->select("a.direccion", "a.id")
			->where("tmae_direccion_subarea.id_direccion", $tabla->id_direccion)
			->orderby("tmae_direccion_subarea.id", "desc")
			->pluck("direccion", "id")->all();

		$dircombo = ["$direccion->id" => $direccion->direccion] + $subdireccion;
		$actividades[2]->Valor = $dircombo;

		$actividades[2]->ValorAnterior = $direccion->id;
		$responsables = UsuariosModel::select(DB::raw("concat(name,' - ',cargo) as name"), "id")->where("estado", "ACT")->where("id_direccion", $tabla->id_direccion)->pluck("name", "id")->all();
		$actividades[3]->Valor = $responsables;


		$actividades[7]->Valor = $this->escoja + $Avance; /// + $estado_act;
		$actividades[8]->Valor = $this->escoja + $estadoobras; /// + $estado_act;

		// show($actividades);
		$verificacion = explodewords(ConfigSystem("verificacion"), "|");
		$actividades[10]->Valor = $this->escoja + $verificacion;
		if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4) {
			unset($actividades[10]);
			unset($actividades[11]);
		}


		$actividades_actuales = CoorProyectoEstrategicoActividades::join('tmae_direcciones as d', 'd.id', '=', 'coor_tmov_proyecto_estrategico_actividad.id_direccion_act')->select(
			'coor_tmov_proyecto_estrategico_actividad.*',
			'd.direccion'
		)
			->where(['coor_tmov_proyecto_estrategico_actividad.estado' => 'ACT', 'coor_tmov_proyecto_estrategico_actividad.id_proyecto' => $id]);

		$actividades_actuales = $actividades_actuales->get();
		//$subarea = SubDireccionModel::where("area",$id_tipo_pefil->id_direccion)->first();
		//
		$actividades_actuales_collect = collect($actividades_actuales);
		$mult_acttividades = $actividades_actuales_collect->map(function ($actividad) use ($id_tipo_pefil) {
			$responsables = DB::select('select GROUP_CONCAT(name SEPARATOR ", ") as responsables from users where id in (' . $actividad->idusuario_act  . ') ');
			// dd($responsables);
			if (isset($responsables[0])) {
				$txt_responsable = $responsables[0]->responsables;
			} else {
				$txt_responsable = '';
			}
			if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || Auth::user()->id == 1764) {
				$show = '<a href="' . URL::to('') . '/coordinacioncronograma/show_actividad_proyecto/' . Crypt::encrypt($actividad->id) . '" class="fa fa-newspaper-o divpopup" target="_blank" onclick="popup(this)"></a>';
				$edit = '<a  onclick="editar(\'' . Crypt::encrypt($actividad->id) . '\')" class="fa fa-pencil-square-o"></a>';
				$dele = '<a onclick="eliminar(\'' . Crypt::encrypt($actividad->id) . '\')" class="fa fa-trash"></a>';
				$modalsubir = '<a type="button" class="fa fa-cloud-upload" onClick="modal_subir(' . $actividad->id . ',30)"></a>';

				$accion = "$show&nbsp;$edit&nbsp;$dele&nbsp;&nbsp;$modalsubir&nbsp;&nbsp;";
			} else {
				$show = '<a href="' . URL::to('') . '/coordinacioncronograma/show_actividad_proyecto/' . Crypt::encrypt($actividad->id) . '" class="fa fa-newspaper-o divpopup" target="_blank" onclick="popup(this)"></a>';
				$edit = '<a   onclick="editar(\'' . Crypt::encrypt($actividad->id) . '\')"  class="fa fa-pencil-square-o"></a>';
				$modalsubir = '<a type="button" class="fa fa-cloud-upload" onClick="modal_subir(' . $actividad->id . ',30)"></a>';


				$accion = "$show&nbsp;$edit&nbsp;&nbsp;$modalsubir&nbsp;&nbsp;";
			}
			//Este Array permite Dibujar la Tabla HTML, LOS Key son los Titulos HEAD del Table
			return [
				'ID'            => $actividad->id,
				'Actividad'            => $actividad->actividad,
				'Dirección'         => $actividad->direccion,
				'Responsables'     => $txt_responsable,
				'Fecha Inicio'     => date('Y-m-d', strtotime($actividad->fecha_inicio)),
				'Fecha Fin'     => date('Y-m-d', strtotime($actividad->fecha_fin)),
				'Observación'     => $actividad->observacion,
				'Avance'     => $actividad->avance . ' %',
				'Archivo final' => (isset($actividad->archivo_final)) ? '<a href="' . URL::to('') . '/archivos_sistema/' . $actividad->archivo_final . '" type="button" class="btn btn-success dropdown-toggle divpopup cboxElement" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 2em;"></i></a>' : '',
				'Verificación'     => $actividad->verificacion,
				'Observacion de verificación'     => $actividad->observacion_verificacion,
				'Acción'     => $accion

			];
		});



		$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ['estado', '<>', 'DEL'], ["tipo_archivo", "<>", "pdf"], ["tipo", "=", "3"]])->get();
		$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ['estado', '<>', 'ACT'], ["tipo_archivo", "=", "pdf"], ["tipo", "=", "3"]])->get();
		return view('vistas.create', [
			// "timelineProyecto"=>$timelineProyecto,
			"dicecionesImagenes" => $dicecionesImagenes,
			"dicecionesDocumentos" => $dicecionesDocumentos,
			"objetos" => $objetos,
			"configuraciongeneral" => $this->configuraciongeneral,
			"tabla" => $tabla,
			"validarjs" => $this->validarjs,
			'mult_acttividades' => $mult_acttividades,
			"permisos" => $this->perfil('NO'),
			'actividades' => $actividades,
			'eliminar_archivo' => 'si'
		]);
	}

	public function update(Request $request, $id)
	{
		//
		return $this->guardar($id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
		$tabla = CoorProyectoEstrategicoModel::find($id);
		//->update(array('estado' => 'INACTIVO'));
		$tabla->estado = 'INA';
		$tabla->save();
		Session::flash('message', 'Registro dado de Baja!');
		return Redirect::to($this->configuraciongeneral[1]);
	}

	public function getTimeLineCab($id)
	{

		$tabla = CoorTipoProyectoDetalleModel::select("coor_tmov_proyecto_estrategico_deta.*", "p.id")
			->join("coor_tmov_proyecto_estrategico as p", "coor_tmov_proyecto_estrategico_deta.id_cab_pro", "=", "p.id")
			// ->where("coor_tmov_proyecto_estrategico_deta.estado","ACT")
			->where("id_cab_pro", $id)
			->orderBy("coor_tmov_proyecto_estrategico_deta.updated_at", "ASC")
			->get();
		return $tabla;
	}
	public function combos($id, $tipo)
	{
		$tabla = array();
		if ($tipo == 1) {
			$tabla = CoorComponenteObjetivosModel::join('coor_tmae_cronograma_componente_cab as com', 'com.id', '=', 'coor_tmae_cronograma_componente_objetivo.id_componente')
				->select('coor_tmae_cronograma_componente_objetivo.*', 'com.vision')
				->where('id_componente', $id)->orderby('objetivo','ASC')->get();
		} elseif ($tipo == 2) {
			$tabla = CoorOdsModel::where('id_componente', $id)->orderby('ods','ASC')->get();
		} elseif ($tipo == 3) {
			$tabla = CoorMetaOdsModel::where('id_ods', $id)->orderby('meta_ods','ASC')->get();
		} elseif ($tipo == 4) {
			$tabla = CoorComponenteObjetivosModel::where('id', $id)->first();
		} elseif ($tipo == 5) {
			$tabla = CoorTipoProyectoModel::where(["estado" => "ACT", 'tipo_componente' => 2, 'id' => $id])->first();
		} elseif ($tipo == 6) {
			$tabla = CoorComponenteObjetivosModel::where('id', $id)->first();
		}

		return $tabla;
	}


	// public function estado($fi,$ff){
	// 	$fecha_actual = date("Y-m-d");
	// 	$fecha_act=Carbon::parse($fecha_actual);
	// 	$fecha_f=Carbon::parse($ff);
	// 	$fecha_ini=Carbon::parse($fi);

	// 	$diferencia = $fecha_act->diffInDays($fecha_f);
	// 	$dfinicial = $fecha_act->diffInDays($fecha_ini);

	// 	if($diferencia == 0){
	// 		return $estado = "EJECUTADO";
	// 	}

	// 	if($fecha_act >= $fecha_ini){
	// 		return $estado = "EJECUCION";
	// 	}

	// 	if ($fecha_act <= $fecha_ini) {
	// 		return $estado = "POR_EJECUTAR";
	// 	}
	// }





	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////   proyecto actividad ////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	public function store_actividad_proyecto()
	{
		$id_encriptado = Input::get('id');
		$id = 0;
		if ($id_encriptado != null) {
			try {
				$id = Crypt::decrypt($id_encriptado);
			} catch (Exception $e) {
				$id = 0;
			};
		}
		return $this->guardar_act_proyecto($id);
	}

	public function guardar_act_proyecto($id)
	{
		$input = Input::all();
		// show($input);
		$ruta = $this->configuraciongeneral[1];
		if ($id == 0) {
			$guardar = new CoorProyectoEstrategicoActividades;
			$msg = "😊 Registro Creado Exitosamente...! ";
			$msgauditoria = "Registro Satisfactorio " . $this->configuraciongeneral[0];
		} else {
			//$ruta .= "/$id/edit";
			$ruta .= "/" . Crypt::encrypt($id) . "/edit";
			$guardar = CoorProyectoEstrategicoActividades::find($id);
			$msg = "😊 Registro Actualizado Exitosamente...!";
			$msgauditoria = "Edición Satisfactoria " . $this->configuraciongeneral[0];
		}
		$input = Input::all();
		// dd($input);
		// $anio = CoorPoaCabModel::where(['estado' => 'ACT', 'id' => $input['id_poa']])->first();
		$id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
		// if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3  || $id_tipo_pefil->tipo == 10) && Auth::user()->id != 1764 && $anio->anio!=2020) {
		if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3  || $id_tipo_pefil->tipo == 10) && Auth::user()->id != 1764) {
			// if ($id != 0 && Input::get('avance') < $guardar->avance) {
			if ($id != 0 && Input::get('avance') < $guardar->avance) {
				return  response()->json(['estado' => 'false', 'mensaje' => 'El avance no puede ser menor de ' . $guardar->avance . '%'], 200);
			}

			// dd($anio);

			$validator = Validator::make($input, CoorProyectoEstrategicoActividades::rulesdir($id));
		} else {
			$validator = Validator::make($input, CoorProyectoEstrategicoActividades::rules($id));
		}

		if ($validator->fails()) {
			$mensajes = $validator->getMessageBag()->getMessages();
			$mensaje = '';
			foreach ($mensajes as $key => $value) {

				$mensaje .= $value[0] . '<br>';
			}


			return  response()->json(['estado' => 'false', 'mensaje' => $mensaje], 200);
		} else {
			DB::beginTransaction();
			try {
				$timeline = new CoorProyectoEstrategicoActividadesDeta();
				foreach ($input as $key => $value) {
					if ($key != "_method" && $key != "_token" && $key != 'id') {

						if ($key == 'archivo_final') {

							$dir = public_path() . '/archivos_sistema/';
							$docs = Input::file("archivo_final");
							if ($docs) {
								// dd($key);
								$fileName = "Archivo_Final-proyecto_actividad-$id" . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
								$ext = $docs->getClientOriginalExtension();
								$perfiles = explode("|", ConfigSystem("archivospermitidos"));
								if (!in_array($ext, $perfiles)) {
									DB::rollback();
									$mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
									return  response()->json(['estado' => 'false', 'mensaje' => 'No se admite el tipo de archivo'], 200);
								}


								//shoW();
								$oldfile = $dir . '' . $fileName;
								if (is_file($oldfile)) {
									// dd($id_tipo_pefil->tipo);
									if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $guardar->verificacion == "VERIFICADO CON OBSERVACIONES") {
										if (file_exists($oldfile))
											unlink($oldfile);
									} else {
										DB::rollback();
										$mensaje = "Ya se encuentra el archivo final registrado";
										return  response()->json(['estado' => 'false', 'mensaje' => $mensaje], 200);
										// return redirect()->back()->withErrors([$mensaje])->withInput();
									}
								}
								$docs->move($dir, $fileName);
								$guardar->$key = $fileName;
								$timeline->$key = $fileName;
							}
							// dd('paso');
						} else {
							$guardar->$key = $value;
							$timeline->$key = $value;
						}
					}
				}
				if (Input::get('avance') == 100) {
					$guardar->estado_actividad = 'EJECUTADO';
				}
				$guardar->id_usuario = Auth::user()->id;
				$guardar->ip = \Request::getClientIp();
				$guardar->pc = \Request::getHost();
				$guardar->save();
				$idcab = $guardar->id;
				/**/

				////// porque en actualizar observacion solo actualizo la obeservacion y el avance y en
				///  el for no se halan
				$timeline->id_proyecto = $guardar->id_proyecto;
				$timeline->actividad = $guardar->actividad;
				$timeline->id_direccion_act = $guardar->id_direccion_act;
				$timeline->idusuario_act = $guardar->idusuario_act;
				$timeline->fecha_inicio = $guardar->fecha_inicio;
				$timeline->fecha_fin = $guardar->fecha_fin;
				$timeline->observacion = $guardar->observacion;
				$timeline->avance = $guardar->avance;
				$timeline->estado_actividad = $guardar->estado_actividad;
				$timeline->id_actividad_cab = $idcab;
				$timeline->id_usuario = $guardar->id_usuario;
				$timeline->ip = $guardar->ip;
				$timeline->pc = $guardar->pc;




				if ($guardar->verificacion == 'VERIFICADO CON OBSERVACIONES' && $id_tipo_pefil->tipo==4 ) {


					$usuario = UsuariosModel::select("users.*")
						->join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
						->where(["id_direccion" => $guardar->id_direccion])
						->whereIn("p.tipo", [3, 10])
						->get();
					// dd($usuario);

					$guardar_verificacion = CoorProyectoEstrategicoActividades::find($idcab);
					$guardar_verificacion->avance = 60;
					$guardar_verificacion->estado_actividad = 'EJECUCION_VENCIDO';
					$guardar_verificacion->save();

					$this->configuraciongeneral[1] = $this->configuraciongeneral[1];
					foreach ($usuario as $key => $value_) {
						# code...
						$this->notificacion->notificacionesweb("Actividad de Proyecto Estratégico " . $guardar->anio . " <b>#</b>" . $idcab . "", $this->configuraciongeneral[1], $value_->id, "2c438f");
						$this->notificacion->EnviarEmail($value_->email, "Actividad de Proyecto Estratégico " . $guardar->anio, "Actividad de Proyecto Estratégico ha sido verificado con observaciones", $guardar->observacion_verificacion,  $this->configuraciongeneral[1]);
					}
				}



				$timeline->save();
				DB::commit();
			} //Fin Try
			catch (\Exception $e) {
				// ROLLBACK
				DB::rollback();
				return  $e->getMessage();
				return  response()->json(['estado' => 'false', 'mensaje' => 'No se pudo grabar intente de nuevo'], 200);
			}

			return  response()->json(['estado' => 'ok', 'mensaje' => $msg], 200);
		}
	}
	public function eliminar_actividad_proyecto($id)
	{
		//

		try {
			$id = Crypt::decrypt($id);

			$tabla = CoorProyectoEstrategicoActividades::find($id);
			$tabla->estado = 'INA';
			$tabla->save();
			CoorProyectoEstrategicoActividadesDeta::where('id_actividad_cab', $id)
				->update(['estado' => 'INA']);
			return response()->json(['estado' => 'ok', 'msg' => 'Registro dado de Baja!']);
		} catch (Exception $e) {
			return response()->json(['estado' => 'error', 'msg' => 'No se pudo completar la acción....']);
		}
	}


	public function show_actividad_proyecto($id)
	{
		$id = intval(Crypt::decrypt($id));
		$actividades_actuales = CoorProyectoEstrategicoActividades::join('tmae_direcciones as d', 'd.id', '=', 'coor_tmov_proyecto_estrategico_actividad.id_direccion_act')
			->select(
				'coor_tmov_proyecto_estrategico_actividad.*',
				'd.direccion'
			)
			->where(['coor_tmov_proyecto_estrategico_actividad.estado' => 'ACT', 'coor_tmov_proyecto_estrategico_actividad.id' => $id])->first();

		$this->configuraciongeneral[3] = "30"; //Tipo de Referencia de Archivo
		$this->configuraciongeneral[4] = "si";

		$dicecionesImagenes = ArchivosModel::where([["id_referencia", $actividades_actuales['id']], ["tipo_archivo", "<>", "pdf"], ["tipo", "30"]])->get();
		$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $actividades_actuales['id']], ["tipo_archivo", "=", "pdf"], ["tipo", "30"]])->get();

		$objetos = json_decode($this->actividades);
		// show($objetos);
		unset($objetos[0]);
		unset($objetos[3]);
		unset($objetos[2]);
		unset($objetos[9]);
		unset($objetos[10]);
		unset($objetos[11]);
	
		return view('vistas.show', [
			//"timelineProyecto" => null,
			'objetos' => $objetos,
			'tabla' => $actividades_actuales,
			"configuraciongeneral" => $this->configuraciongeneral,
			'dicecionesImagenes' => $dicecionesImagenes,
			'dicecionesDocumentos' => $dicecionesDocumentos

		]);
	}
	public function get_actividad_proyecto($id)
	{
		//

		try {
			$id = Crypt::decrypt($id);
			$id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
			$avance = explodewords(ConfigSystem("avance"), "|");
			if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) && Auth::user()->id != 1764) {
				$actividad = CoorProyectoEstrategicoActividades::select('observacion', 'avance')->where(['coor_tmov_proyecto_estrategico_actividad.estado' => 'ACT', 'id' => $id])->first();
				return response()->json(['datos' => $actividad, 'avance' => $avance, 'alto_obs' => '300px']);
			} else {
				$actividad = CoorProyectoEstrategicoActividades::where(['coor_tmov_proyecto_estrategico_actividad.estado' => 'ACT', 'id' => $id])->first();
				$actividad->fecha_inicio = date('Y-m-d', strtotime($actividad->fecha_inicio));
				$actividad->fecha_fin = date('Y-m-d', strtotime($actividad->fecha_fin));
				$responsables = UsuariosModel::select(DB::raw("concat(name,' - ',cargo) as name"), "id")->where("estado", "ACT")->where("id_direccion", $actividad->id_direccion_act)->pluck("name", "id")->all();
				return response()->json(['datos' => json_decode($actividad), 'responsables' => $responsables, 'avance' => $avance, 'alto_obs' => '80px']);
			}
		} catch (Exception $e) {
			// dd($e->getMessage());
			return response()->json(['estado' => 'error', 'msg' => 'No se pudo completar la acción....']);
		}
	}
}
