<?php

namespace Modules\Coordinacioncronograma\Http\Controllers;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Modules\Coordinacioncronograma\Entities\CoorActividadesComponenteObjModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteObjetivosModel;
use Session;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\ArchivosModel;
use App\Http\Controllers\NotificacionesController;
use app\SubDireccionModel;
use Modules\CoordinacionCronograma\Entities\CoorObrasDetaModel;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\Coordinacioncronograma\Entities\CoorContratistaModel;
use Modules\Coordinacioncronograma\Entities\CoorTipoObraModel;
use Modules\Coordinacioncronograma\Entities\CoorObrasComponenteModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Map;

use App\UsuariosModel;
use Exception;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\URL;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\CoordinacionCronograma\Entities\ObraActividadDetaModel;
use Modules\CoordinacionCronograma\Entities\ObraActividadModel;
use Modules\CoordinacionCronograma\Entities\ObrasResponsableModel;
use stdClass;

class CoorObrasController extends Controller
{
    var $configuraciongeneral = array("Obras", "coordinacioncronograma/listobras", "index", 6 => "coordinacioncronograma/obrasajax", 7 => "listobras", 9 => "add_act_obra", 10 => "get_actividad_obra", 11 => "eliminar_actividad_obra");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
    	{"Tipo":"select","Descripcion":"Parroquia","Nombre":"id_parroquia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tipo Obra","Nombre":"id_tipo_obra","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Actividad","Nombre":"nombre_obra","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Calle","Nombre":"calle","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Número de Proceso / Contrato","Nombre":"numero_proceso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Monto ($)","Nombre":"monto","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Direccion","Nombre":"id_direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Responsable","Nombre":"idusuario","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"select","Descripcion":"Estado Obra","Nombre":"estado_obra","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Contratista","Nombre":"id_contratista","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Año","Nombre":"anio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Avance %","Nombre":"avance","Clase":"porcentaje solonumeros","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"datetext","Descripcion":"Fecha Inicial","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Final","Nombre":"fecha_final","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Ultima Modificación","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Fiscalizador","Nombre":"fiscalizador","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Plazo (DÍAS)","Nombre":"plazo","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },

        {"Tipo":"textarea","Descripcion":"Observacion","Nombre":"observacion","Clase":"maxlength:350","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Latitud","Nombre":"latitud","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Longitud","Nombre":"longitud","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"mapa","Descripcion":"Mapa","Nombre":"mapa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },     
        {"Tipo":"select","Descripcion":"Verificación","Nombre":"verificacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Observación de la verificación","Nombre":"observacion_verificacion","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" }   
                  ]';


    var $actividades = '
        [            
            {"Tipo":"text","Descripcion":"Poa","Nombre":"id_proyecto","Clase":" hidden","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"textarea","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Parroquia","Nombre":"parroquia_id","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Barrio","Nombre":"barrio_id","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"datetext","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"datetext","Descripcion":"Fecha fin","Nombre":"fecha_fin_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"textarea","Descripcion":"Explicación Actividad","Nombre":"observacion_actividad","Clase":"mostrarobservaciones maxlength:350","Valor":"Null","ValorAnterior" :"Null" } ,
            {"Tipo":"select","Descripcion":"Avance %","Nombre":"avance_actividad","Clase":"avance","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"file","Descripcion":"Archivo final","Nombre":"archivo_final","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" },    
            {"Tipo":"select","Descripcion":"Verificación","Nombre":"verificacion_obra_act","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"textarea","Descripcion":"Observación de la verificación","Nombre":"observacion_verificacion_obra_act","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" }   
        ]
    ';
    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "id_parroquia" => "id_parroquia: {
                            required: true
                        }",
        "id_tipo_obra" => "id_tipo_obra: {
                            required: true
                        }",
        "nombre_obra" => "nombre_obra: {
                            required: true
                        }",
        "calle" => "calle: {
                            required: true
                        }",
        "monto" => "monto: {
                            required: true
                        }",
        // "estado_obra"=>"estado_obra: {
        //                 required: true
        //             }",
        "id_contratista" => "id_contratista: {
                            required: true
                        }",
        "avance" => "avance: {
                            required: true
                        }",
        "fecha_inicio" => "fecha_inicio: {
                            required: true
                        }",
        "fecha_final" => "fecha_final: {
                            required: true
                        }",
        "fiscalizador" => "fiscalizador: {
                            required: true
                        }",
        "plazo" => "plazo: {
                            required: true
                        }",
        "observacion" => "observacion: {
                            required: true
                        }"
    );
    /*COmponente Sostenibilidad*/
    var $objetoscomponentes = '
        [
            {"Tipo":"select","Descripcion":"Componente","Nombre":"componente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
            {"Tipo":"select","Descripcion":"Objetivos","Nombre":"objetivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Programa","Nombre":"programa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]
    ';



    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }


    public function perfil($verificar = 'SI')
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        //show($id_tipo_pefil);
        // if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $verificar == 'NO') { //1 Administrador 4 Coordinador
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            // dd('aqs');s
            if (Auth::user()->id == 1706 || Auth::user()->id == 1641  || Auth::user()->id == 1671) {
                $object->crear = 'SI';
                $object->ver = 'SI';
                $object->editar = 'SI';
                $object->eliminar = 'SI';
                $object->editar_parcialmente = 'SI';
                $object->guardar = 'SI';
            } else {

                $object->crear = 'NO';
                $object->ver = 'SI';
                $object->editar = 'SI';

                $object->eliminar = 'NO';
                $object->editar_parcialmente = 'SI';
                $object->guardar = 'NO';
            }
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        }

        return $object;
    }
    public function traspasoobras()
    {
        $tabla = CoorObrasModel::all();
        $pro = 0;
        $nopro = 0;
        foreach ($tabla as $key => $value) {
            # code...
            $ver = coorObrasDetaModel::where("id_cab_obra", $value->id)->first();
            if (!$ver) {
                $timeline = new coorObrasDetaModel;
                $timeline->id_cab_obra = $value->id;
                $timeline->id_tipo_obra = $value->id_tipo_obra;
                $timeline->calle = $value->calle;
                $timeline->nombre_obra = $value->nombre_obra;
                $timeline->numero_proceso = $value->numero_proceso;
                $timeline->monto = $value->monto;
                $timeline->estado_obra = $value->estado_obra;
                $timeline->id_contratista = $value->id_contratista;
                $timeline->avance = $value->avance;
                $timeline->fecha_inicio = $value->fecha_inicio;
                $timeline->fecha_final = $value->fecha_final;
                //$timeline->created_at=$value->created_at;
                //$timeline->updated_at=$value->updated_at;
                $timeline->id_usuario = $value->id_usuario;
                $timeline->id_parroquia = $value->id_parroquia;
                $timeline->ip = $value->ip;
                $timeline->pc = $value->pc;
                $timeline->save();
                $pro++;
            } else
                $nopro++;
        }
        return array("PROCESADOS" => $pro, "NO PROCESADOS" => $nopro);
    }
    public function index()
    {
        $objetos = json_decode($this->objetos);
        $objetos[8]->Nombre = "contratista";
        $objetos[0]->Nombre = "parroquia";
        $objetos[1]->Nombre = "tipoobra";
        $objetos[1]->Nombre = "tipoobra";

        unset($objetos[17]);
        unset($objetos[18]);
        unset($objetos[19]);
        // $tabla=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        // ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        // ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
        // ->select("coor_tmov_obras_listado.*", "a.nombres as contratista","b.tipo as tipoobra","c.parroquia")
        // ->where("coor_tmov_obras_listado.estado","ACT")->orderby("id","desc")->get();//->paginate(500);

        $tabla = [];
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $delete = "si";
        $create = "si";
        $edit = null;
        // $id_usuario=Auth::user()->id;
        // if($id_tipo_pefil->tipo==5){
        //     $create='no';
        //     $delete='no';
        //     $edit='no';

        // }
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos);
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => $delete,
            "create" => $create,
            "edit" => $edit

        ]);
        //return view('coordinacioncronograma::index');
    }

    public function obrasajax(Request $request)
    {
        $composentesql = "SELECT aa.nombre AS componente from coor_tmae_cronograma_componente_cab aa 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bb on aa.id=bb.id_componente
            inner join coor_tmov_obras_componente cc on cc.id_objetivo_componente=bb.id
            where cc.id_obra_cab=coor_tmov_obras_listado.id";
        $objetivosql = "SELECT bx.objetivo from coor_tmae_cronograma_componente_cab ax 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bx on ax.id=bx.id_componente
            inner join coor_tmov_obras_componente cx on cx.id_objetivo_componente=bx.id
            where cx.id_obra_cab=coor_tmov_obras_listado.id";
        $programasql = str_replace("bx.objetivo", "bx.programa", $objetivosql);
        ///////////////////
        /*============================*/
        $objetos = json_decode($this->objetos);
        $objetos[0]->Nombre = "parroquia";
        $objetos[1]->Nombre = "tipoobra";
        $objetos[8]->Nombre = "contratista";
        //show($objetos);
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos);
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';

        $totalData = CoorObrasModel::where("estado", "ACT")->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        $query = CoorObrasModel::join("coor_tmae_contratista as a", "a.id", "=", "coor_tmov_obras_listado.id_contratista")
            ->join("coor_tmae_tipo_obra as b", "b.id", "=", "coor_tmov_obras_listado.id_tipo_obra")
            ->join("parroquia as c", "c.id", "=", "coor_tmov_obras_listado.id_parroquia")
            ->select(
                "coor_tmov_obras_listado.*",
                "a.nombres as contratista",
                "b.tipo as tipoobra",
                "c.parroquia",
                DB::raw("ifnull(($composentesql),'') as componente"),
                DB::raw("ifnull(($objetivosql),'') as objetivo"),
                DB::raw("ifnull(($programasql),'') as programa")
            )
            ->where("coor_tmov_obras_listado.estado", "ACT")
            ->orderBy("coor_tmov_obras_listado.id", "desc");
        if ($id_tipo_pefil->tipo == 2  || $id_tipo_pefil->tipo == 3) {
            $query = $query->where("coor_tmov_obras_listado.id_direccion", Auth::user()->id_direccion);
        }

        if (empty($request->input('search.value'))) {
            $posts = $query->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $query->where(function ($query) use ($search) {
                $query->where('coor_tmov_obras_listado.id', 'LIKE', "%{$search}%")
                    ->orWhere('c.parroquia', 'LIKE', "%{$search}%")
                    ->orWhere('b.tipo', 'LIKE', "%{$search}%")
                    ->orWhere('a.nombres', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = $query->where(function ($query) use ($search) {
                $query->where('coor_tmov_obras_listado.id', 'LIKE', "%{$search}%")
                    ->orWhere('c.parroquia', 'LIKE', "%{$search}%")
                    ->orWhere('b.tipo', 'LIKE', "%{$search}%")
                    ->orWhere('a.nombres', 'LIKE', "%{$search}%");
            })
                ->count();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts->toarray());
            foreach ($posts as $post) {


                if ($id_tipo_pefil->tipo == 5  || $id_tipo_pefil->tipo == 6) {

                    $acciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '';
                } else {
                    $show = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                    $edit = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array($post->id, "menu" => "no"), array('class' => 'fa fa-pencil-square-o', 'target' => '_blank' /*'onclick' => 'popup(this)'*/));
                    $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
            <div style="display: none;">
            <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                <input name="_token" type="hidden" value="' . csrf_token() . '">
                <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
            </form>
            </div>';
                    $acciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                }
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $nestedData['acciones'] = $acciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }
















    public function create()
    {
        //

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 5 || $id_tipo_pefil->tipo == 6) {
            return Redirect::to($this->configuraciongeneral[1]);
        }

        $tipoobra = CoorTipoObraModel::where("estado", "ACT")->pluck("tipo", "id")->all();
        $Avance = explodewords(ConfigSystem("avance"), "|");
        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $contratista = CoorContratistaModel::pluck("nombres", "id")->all();
        $seleccionar_parroquia = parroquiaModel::where("estado", "ACT")->pluck("parroquia", "id")->all();
        $direcciones = direccionesModel::where(["estado" => "ACT"])->wherein("id", [16, 26, 33])->pluck("direccion", "id")->all();
        $objetos = json_decode($this->objetos);
        $objetos[0]->Valor = $this->escoja + $seleccionar_parroquia;
        $objetos[1]->Valor = $this->escoja + $tipoobra;
        $objetos[6]->Valor = $this->escoja + $direcciones;
        $objetos[7]->Valor = [];
        $objetos[8]->Valor = $this->escoja + $estadoobras;
        $objetos[9]->Valor = $this->escoja + $contratista;
        $objetos[10]->Valor = $this->escoja + [2017 => 2017, 2019 => 2019, 2020 => 2020];

        // show($objetos);
        $verificacion = explodewords(ConfigSystem("verificacion"), "|");
        $objetos[21]->Valor = $this->escoja + $verificacion;
        if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4) {
            unset($objetos[21]);
            unset($objetos[22]);
        }
        // $objetos[13]->ValorAnterior = $tabla->verificacion;
        // $objetos[8]->Valor=$this->escoja + $Avance;
        // show($objetos);
        unset($objetos[14]);
        $this->configuraciongeneral[2] = "crear";


        //////// mapa //////
        $config['center'] = '-0.9484704, -80.7237787';
        $config['zoom'] = 'auto';
        $config['onclick'] = '$("#latitud").val(event.latLng.lat()); $("#longitud").val(event.latLng.lng());';
        Map::initialize($config);
        $data['map'] = Map::create_map();

        // show($data);
        /*Componentes*/
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $com = CoorComponenteModel::where("estado", "ACT")->select(DB::raw("concat(nombre,' - ',detalle) as componente"), "id")
        ->orderBy('nombre','ASC')->pluck("componente", "id")->all();
        $objetoscomponentes[0]->Valor = $this->escoja + $com;
        $objetoscomponentes[1]->Valor = $this->escoja;
        $objetoscomponentes[2]->Valor = $this->escoja;
        $objetos = array_merge($objetoscomponentes, $objetos);
        /*====================================================*/
        /*====================================================*/
        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
            "mapa" => $data
        ]);
    }
    public function guardar($id)
    {

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 5 || $id_tipo_pefil->tipo == 6) {
            return Redirect::to($this->configuraciongeneral[1]);
        }
        $input = Input::all();

        $ruta = $this->configuraciongeneral[1];
        $menuno = "";
        if (Input::has("menu")) {
            $menuno = "?menu=no";
            unset($input["menu"]);
        }
        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new CoorObrasModel;
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro Variable de Configuración";
        } else {
            $ruta .= "/$id/edit";
            $guardar = CoorObrasModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Edición Variable de Configuración";
        }

        //$input = Input::all();
        $arrapas = array();
        $validarcampos = CoorObrasModel::rules($id);
        //show($validarcampos);
        //show($id_tipo_pefil->tipo);
        if ($id_tipo_pefil->tipo == 4) {
            unset($validarcampos["calle"]);
            unset($validarcampos["monto"]);
            unset($validarcampos["estado_obra"]);
            unset($validarcampos["avance"]);
            unset($validarcampos["id_contratista"]);
            unset($validarcampos["fiscalizador"]);
            unset($validarcampos["plazo"]);
            unset($validarcampos["observacion"]);
        }
        $validator = Validator::make($input, $validarcampos);
        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            DB::beginTransaction();
            try {
                foreach ($input as $key => $value) {

                    if ($key != "_method" && $key != "_token" && $key != "componente" && $key != "objetivo" && $key != "programa" && $key != "ruta" && $key != "idusuario") {
                        $guardar->$key = $value;
                    }
                }


                $guardar->id_usuario = Auth::user()->id;
                if(Input::get('avance')==100){
                    $guardar->estado_obra = 'EJECUTADO';
                }
                

                $guardar->save();
                $idcab = $guardar->id;

                $timeline = new CoorObrasDetaModel;
                //show($timeline);
                $timeline->id_tipo_obra = $guardar->id_tipo_obra;
                $timeline->calle = $guardar->calle;
                $timeline->nombre_obra = $guardar->nombre_obra;
                $timeline->numero_proceso = $guardar->numero_proceso;
                $timeline->monto = $guardar->monto;
                $timeline->estado_obra = $guardar->estado_obra;
                $timeline->id_contratista = $guardar->id_contratista;
                $timeline->avance = $guardar->avance;
                $timeline->fecha_inicio = $guardar->fecha_inicio;
                $timeline->fecha_final = $guardar->fecha_final;
                //$timeline->created_at=$guardar->created_at;
                //$timeline->updated_at=$guardar->updated_at;
                $timeline->id_usuario = Auth::user()->id;
                $timeline->id_parroquia = $guardar->id_parroquia;
                $timeline->id_cab_obra = $guardar->id;
                $timeline->fiscalizador = $guardar->fiscalizador;
                $timeline->plazo = $guardar->plazo;
                $timeline->observacion = $guardar->observacion;

                $timeline->verificacion = $guardar->verificacion;
                $timeline->observacion_verificacion = $guardar->observacion_verificacion;


                $timeline->longitud = $guardar->longitud;
                $timeline->latitud = $guardar->latitud;
                $timeline->ip = \Request::getClientIp();
                $timeline->pc = \Request::getHost();


                $timeline->save();
                /*Componente*/
                if (Input::has("objetivo")) {
                    $compo = CoorObrasComponenteModel::where("id_obra_cab", $guardar->id)->first();
                    if (!$compo)
                        $compo = new CoorObrasComponenteModel;

                    $compo->id_obra_cab = $guardar->id;
                    $compo->id_objetivo_componente = Input::get("objetivo");
                    $compo->id_usuario = Auth::user()->id;
                    $compo->ip = \Request::getClientIp();
                    $compo->pc = \Request::getHost();
                    $compo->save();
                }



                if (Input::has("idusuario")) {
                    $multiple = Input::get("idusuario");
                    $json = [];
                    $guardarres = ObrasResponsableModel::where("id_obra_cab", $idcab)
                        ->delete();
                    foreach ($multiple as $key => $value) {
                        # code...
                        //Responsables
                        $guardarres = ObrasResponsableModel::where("id_usuario", $value)
                            ->where("id_obra_cab", $idcab)
                            ->first();
                        if (!$guardarres)
                            $guardarres = new ObrasResponsableModel;
                        $guardarres->id_usuario = $value;
                        $guardarres->id_obra_cab = $idcab;
                        $guardarres->save();
                        $json[] = $value;
                        //
                        $usuario = UsuariosModel::select("users.*")
                            ->where("id", $guardarres->id_usuario)
                            ->first();
                        //show($actividad);
                        $this->notificacion->notificacionesweb("Se le ha asignado ser responsable a la obra <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit', $usuario->id, "2c438f");
                    }
                    /**/
                }



                if ($guardar->verificacion == 'VERIFICADO CON OBSERVACIONES') {
                    $multiple = ObrasResponsableModel::where("id_obra_cab", $idcab)
                        ->get();
                    $guardar_verificacion = CoorObrasModel::find($idcab);
                    $guardar_verificacion->avance = 60;
                    $guardar_verificacion->estado_obra = 'EJECUCION_VENCIDO';
                    $guardar_verificacion->save();

                    foreach ($multiple as $key => $value) {

                        $usuario = UsuariosModel::select("users.*")
                            ->where("id", $value->id_usuario)
                            ->first();
                        //show($actividad);


                        $this->notificacion->notificacionesweb("Actividad ha sido verificado con observaciones <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit', $usuario->id, "2c438f");
                        $this->notificacion->EnviarEmail($usuario->email, "Obra", "Actividad ha sido verificado con observaciones", $guardar_verificacion->observacion_verificacion,  $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit');
                    }
                }
                /**/
                Auditoria($msgauditoria . " - ID: " . $id . "-" . Input::get($guardar->nombre_obra));
                DB::commit();
            } //Try Transaction
            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage() . " - " . $e->getLine();
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
            Session::flash('message', $msg);
            return Redirect::to($this->configuraciongeneral[1] . '/' . $guardar->id . '/edit');
        }
    }
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }
    public function show($id)
    {
        //
        $objetos = json_decode($this->objetos);
        $objetos[9]->Nombre = "contratista";
        //$objetos[8]->Nombre="avance";
        $objetos[0]->Nombre = "parroquia";
        $objetos[1]->Nombre = "tipoobra";
        /*Componentes*/
        $composentesql = "SELECT aa.nombre AS componente from coor_tmae_cronograma_componente_cab aa 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bb on aa.id=bb.id_componente
            inner join coor_tmov_obras_componente cc on cc.id_objetivo_componente=bb.id
            where cc.id_obra_cab=coor_tmov_obras_listado.id";
        $objetivosql = "SELECT bx.objetivo from coor_tmae_cronograma_componente_cab ax 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bx on ax.id=bx.id_componente
            inner join coor_tmov_obras_componente cx on cx.id_objetivo_componente=bx.id
            where cx.id_obra_cab=coor_tmov_obras_listado.id";
        $programasql = str_replace("bx.objetivo", "bx.programa", $objetivosql);

        $tabla = CoorObrasModel::join("coor_tmae_contratista as a", "a.id", "=", "coor_tmov_obras_listado.id_contratista")
            ->join("coor_tmae_tipo_obra as b", "b.id", "=", "coor_tmov_obras_listado.id_tipo_obra")
            ->join("parroquia as c", "c.id", "=", "coor_tmov_obras_listado.id_parroquia")
            ->select(
                "coor_tmov_obras_listado.*",
                "a.nombres as contratista",
                "b.tipo as tipoobra",
                "c.parroquia",
                DB::raw("ifnull(($composentesql),'') as componente"),
                DB::raw("ifnull(($objetivosql),'') as objetivo"),
                DB::raw("ifnull(($programasql),'') as programa")
            )
            ->where("coor_tmov_obras_listado.estado", "ACT")
            ->where("coor_tmov_obras_listado.id", $id)
            ->first(); //->paginate(500);

        $latitud = $tabla->latitud;
        $longitud = $tabla->longitud;
        $autozoom = "18";
        $actiondrag = '$("#latitud").val(event.latLng.lat()); $("#longitud").val(event.latLng.lng());';
        if ($latitud == "") {
            $latitud = "-0.9484126160642922";
            //$autozoom="18";
        }
        if ($longitud == "")
            $longitud = "-80.72165966033936";
        $config['center'] = "$latitud,$longitud";
        $config['zoom'] = $autozoom; //'auto';
        $config['onclick'] = $actiondrag;
        Map::initialize($config);
        //Marker
        $marker = array();
        $marker['position'] = "$latitud,$longitud";
        $marker['draggable'] = true;
        $marker['ondragend'] = $actiondrag;
        Map::add_marker($marker);
        $data['map'] = Map::create_map();

        //show($data);
        $timeline_obras = $this->getTimeLine($id);
        unset($objetos[14]);
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos);
        return view('vistas.show', [
            "timeline_obras" => $timeline_obras,
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "mapa" => $data
        ]);
    }
    public function edit($id)
    {
        //

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 5  || $id_tipo_pefil->tipo == 6) {
            return Redirect::to($this->configuraciongeneral[1]);
        }

        $tabla = CoorObrasModel::find($id);
        $tipoobra = CoorTipoObraModel::where("estado", "ACT")->pluck("tipo", "id")->all();
        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $Avance = explodewords(ConfigSystem("avance"), "|");
        $contratista = CoorContratistaModel::pluck("nombres", "id")->all();
        $seleccionar_parroquia = parroquiaModel::where("estado", "ACT")->pluck("parroquia", "id")->all();
        $objetos = json_decode($this->objetos);
        $objetos[0]->Valor = $this->escoja + $seleccionar_parroquia;
        $objetos[1]->Valor = $this->escoja + $tipoobra;
        $objetos[8]->Valor = $this->escoja + $estadoobras;
        $objetos[9]->Valor = $this->escoja + $contratista;

        $direcciones = direccionesModel::where(["estado" => "ACT"])->wherein("id", [16, 26, 33])->pluck("direccion", "id")->all();
        $objetos[7]->Valor = [];
        $objetos[6]->Valor = $this->escoja + $direcciones;
        $objetos[6]->ValorAnterior = $tabla->id_direccion;

        // $objetos[8]->Valor=$this->escoja + $Avance;
        $objetos[10]->Valor = $this->escoja + [2019 => 2019, 2020 => 2020];
        $objetos[10]->ValorAnterior = $tabla->anio;
        // show($objetos);
        $verificacion = explodewords(ConfigSystem("verificacion"), "|");
        $objetos[21]->Valor = $this->escoja + $verificacion;
        $objetos[21]->ValorAnterior = $tabla->verificacion;
        if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4) {
            unset($objetos[20]);
            unset($objetos[21]);
        }


        $valorAnteriorpersonas = ObrasResponsableModel::join("users as a", "a.id", "=", "coor_tmov_obras_actividades_responsables.id_usuario")
            ->where("coor_tmov_obras_actividades_responsables.id_obra_cab", $id)
            ->select("a.*");
        // show($objetos);
        if (array_key_exists(7, $objetos)) {
            $objetos[7]->Valor = $valorAnteriorpersonas->pluck("name", "id")->all();
            $objetos[7]->ValorAnterior = $valorAnteriorpersonas->pluck("id", "id")->all();
        }
        $this->configuraciongeneral[2] = "editar";

        $timeline_obras = $this->getTimeLine($id);
        // show($objetos);
        unset($objetos[14]);

        $dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "1"]])->get();
        $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "1"]])->get();
        $latitud = $tabla->latitud;
        $longitud = $tabla->longitud;
        $autozoom = "18";
        $actiondrag = '$("#latitud").val(event.latLng.lat()); $("#longitud").val(event.latLng.lng());';
        if ($latitud == "") {
            $latitud = "-0.9484126160642922";
            //$autozoom="18";
        }
        if ($longitud == "")
            $longitud = "-80.72165966033936";
        $config['center'] = "$latitud,$longitud";
        $config['zoom'] = $autozoom; //'auto';
        $config['onclick'] = $actiondrag;

        Map::initialize($config);
        //Marker
        $marker = array();
        $marker['position'] = "$latitud,$longitud";
        $marker['draggable'] = true;
        $marker['ondragend'] = $actiondrag;
        Map::add_marker($marker);
        $data['map'] = Map::create_map();
        //show($config);
        //show($dicecionesImagenes);
        $this->configuraciongeneral[3] = "1";
        $this->configuraciongeneral[4] = "si";
        /*Componentes*/
        $veri = CoorObrasComponenteModel::where("id_obra_cab", $id)->first();
        //show($veri->toarray());
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $com = CoorComponenteModel::where("estado", "ACT")->orderBy('nombre','ASC')
            ->select(DB::raw("concat(nombre,' - ',detalle) as componente"), "id");
        if ($veri) {
            $valanteriorobj = CoorComponenteObjetivosModel::find($veri->id_objetivo_componente);
            $valanteriorpro = clone $valanteriorobj;
            $valanteriorcom = CoorComponenteModel::find($valanteriorobj->id_componente);
            //show($valanteriorobj);
            //show(array($veri->toarray(),$valanteriorobj->toarray(),$valanteriorcom->toarray()));
            $objetoscomponentes[0]->ValorAnterior = $valanteriorcom->id;
            $objetoscomponentes[1]->Valor = array($valanteriorobj->id => $valanteriorobj->objetivo);
            $objetoscomponentes[2]->Valor = array($valanteriorpro->id => $valanteriorpro->programa);
            //$objetoscomponentes[1]->ValorAnterior=$valanteriorobj->id;
            if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
                $com = $com->where("id", $valanteriorobj->id_componente)->pluck("componente", "id")->all();
                $objetoscomponentes[0]->Valor = $com;
            } else {
                $com = $com->pluck("componente", "id")->all();
                $objetoscomponentes[0]->Valor = $this->escoja + $com;
            }
        } else {
            $com = $com->pluck("componente", "id")->all();
            $objetoscomponentes[0]->Valor = $this->escoja + $com;
            if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3)
                $objetoscomponentes[0]->Valor = $this->escoja;
            $objetoscomponentes[1]->Valor = $this->escoja;
            $objetoscomponentes[2]->Valor = $this->escoja;
        }
        /**/
        $objetos = array_merge($objetoscomponentes, $objetos);
        //////////////////////////////////////////////////CRHISTIAN///////////////////////////////////
        $actividades = json_decode($this->actividades);
        $actividades[0]->Valor = $id;
        $actividades[0]->ValorAnterior = $id;
        $parroquias = parroquiaModel::where('estado', 'ACT')->pluck('parroquia', 'id')->all();
        $actividades[2]->Valor = $this->escoja + $parroquias; /// + $estado_act;
        $actividades[3]->Valor = []; /// + $estado_act;
        $actividades[7]->Valor = $this->escoja + $Avance; /// + $estado_act;
        $actividades[8]->Valor = $this->escoja + $estadoobras; /// + $estado_act;

        // show($actividades);
        $verificacion = explodewords(ConfigSystem("verificacion"), "|");
        $actividades[10]->Valor = $this->escoja + $verificacion;
        if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4) {
            unset($actividades[10]);
            unset($actividades[11]);
        }


        $actividades_actuales = ObraActividadModel::select(
            'coor_tmov_obras_actividades.*'
        )
            ->where(['coor_tmov_obras_actividades.estado' => 'ACT', 'coor_tmov_obras_actividades.id_proyecto' => $id]);

        $actividades_actuales = $actividades_actuales->get();
        //$subarea = SubDireccionModel::where("area",$id_tipo_pefil->id_direccion)->first();
        //
        $actividades_actuales_collect = collect($actividades_actuales);
        $mult_acttividades = $actividades_actuales_collect->map(function ($actividad) use ($id_tipo_pefil) {

            if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || Auth::user()->id == 1706 || Auth::user()->id == 1641 || Auth::user()->id == 1671) {
                $show = '<a href="' . URL::to('') . '/coordinacioncronograma/show_actividad_obra/' . Crypt::encrypt($actividad->id) . '" class="fa fa-newspaper-o divpopup" target="_blank" onclick="popup(this)"></a>';
                $edit = '<a  onclick="editar(\'' . Crypt::encrypt($actividad->id) . '\')" class="fa fa-pencil-square-o"></a>';
                $dele = '<a onclick="eliminar(\'' . Crypt::encrypt($actividad->id) . '\')" class="fa fa-trash"></a>';
                $modalsubir = '<a type="button" class="fa fa-cloud-upload" onClick="modal_subir(' . $actividad->id . ',14)"></a>';
                $accion = "$show&nbsp;$edit&nbsp;$dele&nbsp;&nbsp;$modalsubir";
            } else {
                $show = '<a href="' . URL::to('') . '/coordinacioncronograma/show_actividad_obra/' . Crypt::encrypt($actividad->id) . '" class="fa fa-newspaper-o divpopup" target="_blank" onclick="popup(this)"></a>';
                $edit = '<a   onclick="editar(\'' . Crypt::encrypt($actividad->id) . '\')"  class="fa fa-pencil-square-o"></a>';
                $modalsubir = '<a type="button" class="fa fa-cloud-upload" onClick="modal_subir(' . $actividad->id . ',14)"></a>';
                $accion = "$show&nbsp;$edit&nbsp;&nbsp;$modalsubir";
            }
            //Este Array permite Dibujar la Tabla HTML, LOS Key son los Titulos HEAD del Table
            $barrio = barrioModel::join('parroquia as p', 'p.id', '=', 'barrio.id_parroquia')
                ->where('barrio.id', $actividad->barrio_id)->first();

            return [
                'ID'            => $actividad->id,
                'Actividad'            => $actividad->actividad,
                'Parroquia'            => (isset($barrio->parroquia)) ? $barrio->parroquia : '',
                'Barrio'            => (isset($barrio->barrio)) ? $barrio->barrio : '',
                'Fecha Inicio'     => date('Y-m-d', strtotime($actividad->fecha_inicio_actividad)),
                'Fecha Fin'     => date('Y-m-d', strtotime($actividad->fecha_fin_actividad)),
                'Observación'     => $actividad->observacion_actividad,
                'Avance'     => $actividad->avance_actividad . ' %',
                'Estado Actividad'     => $actividad->estado_actividad,
                'Archivo final' => (isset($actividad->archivo_final)) ? '<a href="' . URL::to('') . '/archivos_sistema/' . $actividad->archivo_final . '" type="button" class="btn btn-success dropdown-toggle divpopup cboxElement" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 2em;"></i></a>' : '',
                'Acción'     => $accion

            ];
        });


        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 10) {
            // if (Auth::user()->id != 1706 && $anio!=2020) {
            if (Auth::user()->id != 1706 && Auth::user()->id != 1641 && Auth::user()->id != 1671) {
                foreach ($objetos as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($value->Tipo == 'select' || $value->Tipo == 'select-multiple') {
                            if ($k == 'Nombre') {
                                $objetos[$key]->Valor = (isset($tabla[$objetos[$key]->Nombre])) ? $tabla[$objetos[$key]->Nombre] : 'Sin datos';
                            }
                        } else {
                            if ($k == 'Nombre') {
                                $objetos[$key]->Valor = (isset($tabla[$v])) ? $tabla[$v] : 'Sin datos';
                            }
                        }
                    }
                    $objetos[$key]->Tipo = 'html2';
                }

                unset($actividades[0]);
                unset($actividades[1]);
                unset($actividades[2]);
                unset($actividades[3]);
                unset($actividades[4]);
                unset($actividades[5]);
                unset($actividades[8]);
                unset($actividades[9]);
            }
        }



        return view('vistas.create', [
            "dicecionesImagenes" => $dicecionesImagenes,
            "dicecionesDocumentos" => $dicecionesDocumentos,
            "timeline_obras" => $timeline_obras,
            "tabla" => $tabla,
            "objetos" => $objetos,
            'actividades' => $actividades,
            "permisos" => $this->perfil('NO'),
            'mult_acttividades' => $mult_acttividades,
            'botonguardaravance' => 'NO',
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
            "mapa" => $data
        ]);
    }
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 5  || $id_tipo_pefil->tipo == 6) {
            return Redirect::to($this->configuraciongeneral[1]);
        }

        DB::beginTransaction();
        try {
            $tabla = CoorObrasModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
            $tabla->estado = 'INA';
            $tabla->save();

            DB::commit();
            Session::flash('message', 'El registro se eliminó Exitosamente!');
            if (Input::has('vista')) {
                return 'El registro se eliminó Exitosamente!';
            }
            return Redirect::to($this->configuraciongeneral[1]);
        } //Try Transaction
        catch (\Exception $e) {
            //  // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);
        }
    }




    public function getTimeLine($id)
    {
        $tabla = CoorObrasDetaModel::select("coor_tmov_obras_listado_deta.*", "p.parroquia", "c.nombres as contratista", "to.tipo", "u.name")
            ->join("coor_tmae_tipo_obra as to", "to.id", "=", "coor_tmov_obras_listado_deta.id_tipo_obra")
            ->join("parroquia as p", "p.id", "=", "coor_tmov_obras_listado_deta.id_parroquia")
            ->join("coor_tmae_contratista as c", "c.id", "=", "coor_tmov_obras_listado_deta.id_contratista")
            ->join("users as u", "u.id", "=", "coor_tmov_obras_listado_deta.id_usuario")
            ->where('id_cab_obra', $id)
            ->orderBy("coor_tmov_obras_listado_deta.updated_at", "ASC")
            ->get();
        return $tabla;
    }










    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////   poa actividad ////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    public function store_actividad_obras()
    {
        $id_encriptado = Input::get('id');
        $id = 0;
        if ($id_encriptado != null) {
            try {
                $id = Crypt::decrypt($id_encriptado);
            } catch (Exception $e) {
                $id = 0;
            };
        }
        return $this->guardar_act($id);
    }

    public function guardar_act($id)
    {
        $input = Input::all();
        // show($input);
        $ruta = $this->configuraciongeneral[1];
        if ($id == 0) {
            $guardar = new ObraActividadModel();
            $msg = "😊 Registro Creado Exitosamente...! ";
            $msgauditoria = "Registro Satisfactorio " . $this->configuraciongeneral[0];
        } else {
            //$ruta .= "/$id/edit";
            $ruta .= "/" . Crypt::encrypt($id) . "/edit";
            $guardar = ObraActividadModel::find($id);
            $msg = "😊 Registro Actualizado Exitosamente...!";
            $msgauditoria = "Edición Satisfactoria " . $this->configuraciongeneral[0];
        }
        $input = Input::all();
        // dd($input);
        // $anio = CoorPoaCabModel::where(['estado' => 'ACT', 'id' => $input['id_poa']])->first();
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        // if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3  || $id_tipo_pefil->tipo == 10) && Auth::user()->id != 1706 && $anio->anio!=2020) {
        if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3  || $id_tipo_pefil->tipo == 10) && Auth::user()->id != 1706 &&  Auth::user()->id != 1641 && Auth::user()->id != 1671) {
            // if ($id != 0 && Input::get('avance') < $guardar->avance) {
            if ($id != 0 && Input::get('avance') < $guardar->avance) {
                return  response()->json(['estado' => 'false', 'mensaje' => 'El avance no puede ser menor de ' . $guardar->avance . '%'], 200);
            }

            // dd($anio);

            $validator = Validator::make($input, ObraActividadModel::rulesdir($id));
        } else {
            $validator = Validator::make($input, ObraActividadModel::rules($id));
        }

        if ($validator->fails()) {
            $mensajes = $validator->getMessageBag()->getMessages();
            $mensaje = '';
            foreach ($mensajes as $key => $value) {

                $mensaje .= $value[0] . '<br>';
            }


            return  response()->json(['estado' => 'false', 'mensaje' => $mensaje], 200);
        } else {
            DB::beginTransaction();
            try {
                $timeline = new ObraActividadDetaModel();
                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token" && $key != 'id' && $key != 'parroquia_id') {

                        if ($key == 'archivo_final') {

                            $dir = public_path() . '/archivos_sistema/';
                            $docs = Input::file("archivo_final");
                            if ($docs) {
                                // dd($key);
                                $fileName = "Archivo_Final-obras_actividad-$id" . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                                $ext = $docs->getClientOriginalExtension();
                                $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                                if (!in_array($ext, $perfiles)) {
                                    DB::rollback();
                                    $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                                    return  response()->json(['estado' => 'false', 'mensaje' => 'No se admite el tipo de archivo'], 200);
                                }


                                //shoW();
                                $oldfile = $dir . '' . $fileName;
                                if (is_file($oldfile)) {
                                    // dd($id_tipo_pefil->tipo);
                                    if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) {
                                        if (file_exists($oldfile))
                                            unlink($oldfile);
                                    } else {
                                        DB::rollback();
                                        $mensaje = "Ya se encuentra el archivo final registrado";
                                        return  response()->json(['estado' => 'false', 'mensaje' => $mensaje], 200);
                                        // return redirect()->back()->withErrors([$mensaje])->withInput();
                                    }
                                }
                                $docs->move($dir, $fileName);
                                $guardar->$key = $fileName;
                                $timeline->$key = $fileName;
                            }
                            // dd('paso');
                        } else {
                            $guardar->$key = $value;
                            $timeline->$key = $value;
                        }
                    }
                }
                if (Input::get('avance') == 100) {
                    $guardar->estado_actividad = 'EJECUTADO';
                }
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                $guardar->save();
                $idcab = $guardar->id;
                /**/

                ////// porque en actualizar observacion solo actualizo la obeservacion y el avance y en
                ///  el for no se halan
                $timeline->id_proyecto = $guardar->id_proyecto;
                $timeline->actividad = $guardar->actividad;
                $timeline->fecha_inicio_actividad = $guardar->fecha_inicio_actividad;
                $timeline->fecha_fin_actividad = $guardar->fecha_fin_actividad;
                $timeline->observacion_actividad = $guardar->observacion_actividad;
                $timeline->avance_actividad = $guardar->avance_actividad;
                $timeline->estado_actividad = $guardar->estado_actividad;
                $timeline->id_act_cab = $idcab;
                $timeline->id_usuario = $guardar->id_usuario;
                $timeline->ip = $guardar->ip;
                $timeline->pc = $guardar->pc;


                $timeline->save();



                if ($guardar->verificacion_obra_act == 'VERIFICADO CON OBSERVACIONES' && $id_tipo_pefil->tipo==4 ) {
                    $multiple = ObrasResponsableModel::where("id_obra_cab", $guardar->id_proyecto)
                        ->get();
                    $guardar_verificacion = ObraActividadModel::find($guardar->id);
                    $guardar_verificacion->avance_actividad = 60;
                    $guardar_verificacion->estado_actividad = 'EJECUCION_VENCIDO';
                    $guardar_verificacion->save();
                    foreach ($multiple as $key => $value) {

                        $usuario = UsuariosModel::select("users.*")
                            ->where("id", $value->id_usuario)
                            ->first();
                        //show($actividad);

                        $this->notificacion->notificacionesweb("Actividad ha sido verificado con observaciones <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($guardar->id_proyecto) . '/edit', $usuario->id, "2c438f");
                        $this->notificacion->EnviarEmail($usuario->email, "Obra", "Actividad ha sido verificado con observaciones", $guardar_verificacion->observacion_verificacion,  $this->configuraciongeneral[1] . '/' . Crypt::encrypt($guardar->id_proyecto) . '/edit');
                    }
                }

                DB::commit();
            } //Fin Try
            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                return  $e->getMessage();
                return  response()->json(['estado' => 'false', 'mensaje' => 'No se pudo grabar intente de nuevo'], 200);
            }

            return  response()->json(['estado' => 'ok', 'mensaje' => $msg], 200);
        }
    }
    public function eliminar_actividad_obra($id)
    {
        //

        try {
            $id = Crypt::decrypt($id);

            $tabla = ObraActividadModel::find($id);
            $tabla->estado = 'INA';
            $tabla->save();
            ObraActividadDetaModel::where('id_act_cab', $id)
                ->update(['estado' => 'INA']);
            return response()->json(['estado' => 'ok', 'msg' => 'Registro dado de Baja!']);
        } catch (Exception $e) {
            return response()->json(['estado' => 'error', 'msg' => 'No se pudo completar la acción....']);
        }
    }
    public function get_actividad_obra($id)
    {
        //

        try {
            $id = Crypt::decrypt($id);
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
            $avance = explodewords(ConfigSystem("avance"), "|");

            if (($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) && Auth::user()->id != 1706   && Auth::user()->id != 1641 && Auth::user()->id != 1671) {
                $actividad = ObraActividadModel::select('observacion_actividad', 'avance_actividad')
                    ->join('barrio as b', 'b.id', 'coor_tmov_obras_actividades.barrio_id')
                    ->where(['coor_tmov_obras_actividades.estado' => 'ACT', 'coor_tmov_obras_actividades.id' => $id])->first();
                return response()->json(['datos' => $actividad, 'avance' => $avance, 'alto_obs' => '300px']);
            } else {
                $actividad = ObraActividadModel::join('barrio as b', 'b.id', 'coor_tmov_obras_actividades.barrio_id')
                    ->select('b.id_parroquia as parroquia_id', 'coor_tmov_obras_actividades.*')
                    ->where(['coor_tmov_obras_actividades.estado' => 'ACT', 'coor_tmov_obras_actividades.id' => $id])
                    ->first();
                $actividad->fecha_inicio = date('Y-m-d', strtotime($actividad->fecha_inicio));
                $actividad->fecha_fin = date('Y-m-d', strtotime($actividad->fecha_fin));
                return response()->json(['datos' => json_decode($actividad), 'responsables' => [], 'avance' => $avance, 'alto_obs' => '80px']);
            }
        } catch (Exception $e) {
            dd($e->getMessage());
            return response()->json(['estado' => 'error', 'msg' => 'No se pudo completar la acción....']);
        }
    }


    public function show_actividad_obra($id)
    {
        $id = intval(Crypt::decrypt($id));
        $actividades_actuales = ObraActividadModel::select(
            'coor_tmov_obras_actividades.*'
        )
            ->where(['coor_tmov_obras_actividades.estado' => 'ACT', 'coor_tmov_obras_actividades.id' => $id])->first();

        $this->configuraciongeneral[3] = "8"; //Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "si";
        // $objetos =  $this->actividades;
        // dd($mult_acttividades[0]['']);
        $dicecionesImagenes = ArchivosModel::where([["id_referencia", $actividades_actuales['id']], ["tipo_archivo", "<>", "pdf"], ["tipo", "14"]])->get();
        $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $actividades_actuales['id']], ["tipo_archivo", "=", "pdf"], ["tipo", "14"]])->get();

        $timelineProyecto = ObraActividadDetaModel::select("coor_tmov_obras_actividades_deta.*")
            // ->join("poa_tmae_cab_actividad as p", "poa_tmae_cab_actividad.id_poa", "=", "p.id")
            // ->where("coor_tmov_proyecto_estrategico_deta.estado","ACT")
            ->where("coor_tmov_obras_actividades_deta.id_act_cab", $id)
            ->orderBy("coor_tmov_obras_actividades_deta.updated_at", "ASC")
            ->get();

        $objetos = json_decode($this->actividades);
        unset($objetos[0]);
        unset($objetos[3]);
        unset($objetos[2]);

        return view('vistas.show', [
            //"timelineProyecto" => null,
            'objetos' => $objetos,
            'tabla' => $actividades_actuales,
            "configuraciongeneral" => $this->configuraciongeneral,
            'dicecionesImagenes' => $dicecionesImagenes,
            'dicecionesDocumentos' => $dicecionesDocumentos

        ]);
    }
}
