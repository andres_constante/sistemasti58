<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Crypt;
use DB;

use Modules\Coordinacioncronograma\Entities\CoorComponenteModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteProgramaModel;

class CoorComponentesProgramaController extends Controller
{
    var $configuraciongeneral = array("Programas de Componentes", "coordinacioncronograma/componentesprograma", "index", 6 => "coordinacioncronograma/componentesprogramaajax", 7 => "componentesprograma");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"select","Descripcion":"Componente","Nombre":"id_componente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },        
        {"Tipo":"text","Descripcion":"Programa","Nombre":"programa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" } ,
        {"Tipo":"text","Descripcion":"Código","Nombre":"codigo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" } 
                  ]';
//https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "id_componente" => "id_componente: {
                            required: true
                        }",
        "programa" => "programa: {
                            required: true
                        }"
    );
   public function getcomponenteprograma()
    {
        $id=intval(Input::get("id"));
        $tabla = CoorComponenteProgramaModel::where("coor_tmae_cronograma_componente_programa.estado", "ACT")
        ->join("coor_tmae_cronograma_componente_cab as a","a.id","coor_tmae_cronograma_componente_programa.id_componente")
        ->select("coor_tmae_cronograma_componente_programa.id","coor_tmae_cronograma_componente_programa.programa")
        ->where("coor_tmae_cronograma_componente_programa.id_componente",$id)
        ->pluck("programa","id")
        ->all();
        return $tabla;
    }
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function verpermisos($objetos = array(), $tipo = "index")
    {
        //show($objetos);
        $tabla = CoorComponenteProgramaModel::where("coor_tmae_cronograma_componente_programa.estado", "ACT")
        ->join("coor_tmae_cronograma_componente_cab as a","a.id","coor_tmae_cronograma_componente_programa.id_componente")
        ->select("coor_tmae_cronograma_componente_programa.*",DB::raw("concat(a.nombre,' - ',a.detalle) as componente"))
        ->orderby("coor_tmae_cronograma_componente_programa.id","desc");
        return array($objetos, $tabla);
    }

    public function componentesprogramaajax(Request $request)
    {
        //show(Input::all());
        $objetos=json_decode($this->objetos);
        $objetos[0]->Nombre="componente";
        $tabla = $this->verpermisos($objetos);
        $objetos = $tabla[0];

        //show($objetos);
        $tabla = $tabla[1];
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        //show($start);
        if (empty($request->input('search.value'))) {
            //if($start)
            $posts = $tabla;
                if($limit>=0)
                    $posts = $posts->offset($start)->limit($limit);
            $posts = $posts->orderBy($order, $dir)->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('coor_tmae_cronograma_componente_programa.id', 'LIKE', "%{$search}%")
                    ->orWhere('coor_tmae_cronograma_componente_programa.objetivo', 'LIKE', "%{$search}%")
                    ->orWhere('coor_tmae_cronograma_componente_programa.programa', 'LIKE', "%{$search}%")
                    ->orWhere('coor_tmae_cronograma_componente_programa.codigo', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();

        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {


                $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;' .
                    link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o')) . '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;

            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos= json_decode($this->objetos);
        $objetos[0]->Nombre="componente";
        //$objetos = $this->verpermisos($objetos);
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si"
        ]);
        //return view('coordinacioncronograma::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        /*QUITAR CAMPOS*/
        $objetos=json_decode($this->objetos);
        $tbcom=CoorComponenteModel::where("estado","ACT")->pluck("nombre", "id")->all();
        $objetos[0]->Valor=$this->escoja + $tbcom;
        $objetos = $this->verpermisos($objetos, "crear");
        $this->configuraciongeneral[2] = "crear";
        return view('vistas.create', [
            "objetos" => $objetos[0],
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs
        ]);
    }
    public function guardar($id)
    {
        $input = Input::all();

        $ruta = $this->configuraciongeneral[1];

        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new CoorComponenteProgramaModel;
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro Variable de Configuración";
        } else {
            $ruta .= "/$id/edit";
            $guardar = CoorComponenteProgramaModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Edición Variable de Configuración";
        }

        $input = Input::all();
        $arrapas = array();

        $validator = Validator::make($input, CoorComponenteProgramaModel::rules($id));

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            foreach ($input as $key => $value) {

                if ($key != "_method" && $key != "_token") {
                    $guardar->$key = $value;
                }
            }
            $guardar->id_usuario = Auth::user()->id;
            $guardar->ip = \Request::getClientIp();
            $guardar->pc = \Request::getHost();
            $guardar->save();
            /**/

            Auditoria($msgauditoria . " - ID: " . $id . "-" . Input::get($guardar->programa));
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("coor_tmae_cronograma_componente_programa.id", $id)->first();

        $this->configuraciongeneral[3] = "5";//Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "si";
        $objetos=$objetos[0];
        $objetos[0]->Nombre="componente";
        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos), "editar");
        $tabla = $objetos[1]->where("coor_tmae_cronograma_componente_programa.id", $id)->first();

        $tbcom=CoorComponenteModel::where("estado","ACT")->pluck("nombre", "id")->all();
        $objetos[0][0]->Valor=$this->escoja + $tbcom;
        //show($tabla);
        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        $this->configuraciongeneral[2] = "editar";
        return view('vistas.create', [
            "objetos" => $objetos[0],
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
            "tabla" => $tabla/*,
            "dicecionesImagenes" => $dicecionesImagenes,
            "dicecionesDocumentos" => $dicecionesDocumentos*/
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $tabla = CoorComponenteProgramaModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
