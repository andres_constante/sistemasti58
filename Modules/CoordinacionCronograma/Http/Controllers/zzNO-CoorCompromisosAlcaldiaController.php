<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;

use Modules\Coordinacioncronograma\Http\Controllers\CoorCronogramaCabController;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use App\UsuariosModel;
use Modules\Coordinacioncronograma\Entities\MovimientoCronogramaPersonasModel; 
class CoorCompromisosAlcaldiaController extends Controller
{
    var $configuraciongeneral = array(
        "Compromisos Alcaldía",
        "coordinacioncronograma/compromisosalcaldia",
        "index",
        6 => "coordinacioncronograma/compromisosalcaldiaajax",
        7 => "compromisosalcaldia"
    );
    var $escoja = array(null => "Escoja opción...");

    var $objetos = '[
        {"Tipo":"select","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Responsable","Nombre":"idusuario","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Explicación Actividad","Nombre":"observacion","Clase":"mostrarobservaciones","Valor":"Null","ValorAnterior" :"Null" } ,
        {"Tipo":"text","Descripcion":"Última Modificación","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }    
        ]';

    var $botonesindex = '
        [
            {"Descripcion":"Mostrar","Ruta":"","Tipo":"Editar" },
            {"Descripcion":"Editar","Ruta":"","Tipo":"Mostrar" }
        ]
    ';
    var $columns = array(
        0 => 'id',
        1 => 'actividad',
        4 => 'fecha_inicio',
        5 => 'fecha_fin',
        6 => 'direccion',        
        9 => 'updated_at'
    );


    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "estado_actividad" => "estado_actividad: {
                            required: true
                        }",
        "actividad" => "actividad: {
                            required: true
                        }",
        "fecha_inicio" => "fecha_inicio: {
                            required: true
                        }",
        "fecha_fin" => "fecha_fin: {
                            required: true
                        }",
        "idusuario" => "idusuario: {
                            required: true
                        }",

        "observacion" => "observacion: {
                            required: false
                        }"
    );
    protected $Coordinacion;
    public function __construct(CoorCronogramaCabController $Coordinacion)
    {
        $this->middleware('auth');
        $this->Coordinacion = $Coordinacion;
    }
    public function compromisosalcaldiaajax(Request $request)
    {
        $filtro = [ ["coor_tmov_cronograma_cab.prioridad", "COMPROMISOS_ALCALDIA"]];
        return $this->Coordinacion->cronogramaajax($request, $filtro,$this->columns,$this->configuraciongeneral[7]);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $objetos = json_decode($this->objetos);
        $objetos[4]->Nombre = "direccion";
        unset($objetos[5]);
        return $this->Coordinacion->index($this->configuraciongeneral[6],$objetos,$this->configuraciongeneral);        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $objetos = json_decode($this->objetos);
        $seleccionar_direccion = direccionesModel::where("estado", "ACT")->orderby("direccion", "asc")->pluck("direccion", "id")->all();        
        //Estado
        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $prioridad = explodewords(ConfigSystem("prioridad"), "|");                
        //show($prioridad);

        $objetos[0]->Valor = $this->escoja + $estadoobras;        
        $objetos[4]->Valor = $this->escoja + $seleccionar_direccion;
        $objetos[5]->Valor = $this->escoja;        
        //show($objetos);
        unset($objetos[7]); 
        $this->configuraciongeneral[2] = "crear";       
        return $this->Coordinacion->create($objetos,$this->configuraciongeneral);
        //return view('coordinacioncronograma::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $objetos = json_decode($this->objetos);
        $validararray = CoorCronogramaCabModel::rules(0);
        unset($validararray["id_tipo_actividad"]);
        //show($validararry);
        return $this->Coordinacion->guardar(0,$objetos,$this->configuraciongeneral,$validararray,["prioridad"=>"COMPROMISOS_ALCALDIA"]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $objetos = json_decode($this->objetos);
        $seleccionar_direccion = direccionesModel::where("estado", "ACT")->orderby("direccion", "asc")->pluck("direccion", "id")->all();        
        //Estado
        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $prioridad = explodewords(ConfigSystem("prioridad"), "|");                
        //show($prioridad);
        $valorAnteriorpersonas = MovimientoCronogramaPersonasModel::join("users as a", "a.id", "=", "coor_tmov_cropnograma_resposables.id_usuario")
            ->where("coor_tmov_cropnograma_resposables.id_crono_cab", $id)
            ->select("a.*");
        //show($valorAnteriorpersonas);
        $objetos[5]->Valor = $valorAnteriorpersonas->pluck("name", "id")->all();
        $objetos[5]->ValorAnterior = $valorAnteriorpersonas->pluck("id", "id")->all();

        $objetos[0]->Valor = $this->escoja + $estadoobras;        
        $objetos[4]->Valor = $this->escoja + $seleccionar_direccion;
        //$objetos[5]->Valor = $this->escoja;        
        //show($objetos);
        unset($objetos[7]); 
        $this->configuraciongeneral[2] = "editar";       
        return $this->Coordinacion->edit($id,$objetos,$this->configuraciongeneral);
        return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $objetos = json_decode($this->objetos);
        $validararray = CoorCronogramaCabModel::rules(0);
        unset($validararray["id_tipo_actividad"]);
        //show($validararry);
        return $this->Coordinacion->guardar($id,$objetos,$this->configuraciongeneral,$validararray,["prioridad"=>"COMPROMISOS_ALCALDIA"]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        return $this->Coordinacion->destroy($id,$this->configuraciongeneral);
    }
}
