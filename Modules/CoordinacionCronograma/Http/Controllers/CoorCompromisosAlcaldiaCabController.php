<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\ArchivosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use Modules\Coordinacioncronograma\Entities\CoorComponenteModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteObjetivosModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionCabModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionDetaDireccionModel;
use Session;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Crypt;
/*Models*/

use Modules\Coordinacioncronograma\Entities\CabCompromisoalcaldiaModel;
use Modules\Coordinacioncronograma\Entities\CabCompromisoalcaldiaDetaModel;
use App\UsuariosModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\Coordinacioncronograma\Entities\CompromisosResponsablesModel;

use App\Http\Controllers\NotificacionesController;
use App\SubDireccionModel;
use Illuminate\Support\Facades\DB as FacadesDB;
use stdClass;

class CoorCompromisosAlcaldiaCabController extends Controller
{
    var $configuraciongeneral = array("Compromisos Territoriales", "coordinacioncronograma/compromisosalcaldiablue", "index", 6 => "coordinacioncronograma/compromisosalcaldiablueajax", 7 => "compromisosalcaldiablue");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Actividad / Requerimiento","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },        
        {"Tipo":"select","Descripcion":"Parroquia","Nombre":"id_parroquia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Ubicación / Dirección","Nombre":"ubicacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },        
        {"Tipo":"select","Descripcion":"Dirección a Cargo","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Responsable(s)","Nombre":"idusuario","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Explicación Actividad / Observación","Nombre":"observacion","Clase":"mostrarobservaciones  maxlength:350","Valor":"Null","ValorAnterior" :"Null" } ,
        {"Tipo":"select","Descripcion":"Avance %","Nombre":"avance","Clase":"porcentaje","Valor":"Null","ValorAnterior" :"Null" },      
        {"Tipo":"file","Descripcion":"Archivo Final","Nombre":"archivo_final","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" },  
        {"Tipo":"text","Descripcion":"Última Modificación","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Modificado por","Nombre":"usuario","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"porcentaje","Valor":"Null","ValorAnterior" :"Null" },     
        {"Tipo":"select","Descripcion":"Verificación","Nombre":"verificacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Observación de la verificación","Nombre":"observacion_verificacion","Clase":"hidden","Valor":"Null","ValorAnterior" :"Null" }   
        ]';
    /*COmponente Sostenibilidad*/
    var $objetoscomponentes = '
        [
            {"Tipo":"select","Descripcion":"Componente","Nombre":"componente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
            {"Tipo":"select","Descripcion":"Objetivos","Nombre":"objetivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Programa","Nombre":"programa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]
    ';
    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "estado_actividad" => "estado_actividad: {
                            required: true
                        }",
        "actividad" => "actividad: {
                            required: true
                        }",
        "parroquia" => "parroquia: {
                            required: true
                        }",
        "fecha_inicio" => "fecha_inicio: {
                            required: true
                        }",
        "fecha_fin" => "fecha_fin: {
                            required: true
                        }",
        "id_direccion" => "id_direccion: {
                            required: true
                        }"
    );

    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }
    public function pruebajsoncompromisos()
    {
        $tabla = CabCompromisoalcaldiaModel::get();
        $res = response()->json($tabla->toarray());
        $res->header('Content-Type', "application/json");
        return $res;
        //echo json_encode($tabla->toarray());
    }
    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        $object->perfil = $id_tipo_pefil->tipo;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $id_tipo_pefil->tipo == 10) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 7) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        }
        //show($object);
        return $object;
    }
    public function verpermisos($objetos = array(), $tipo = "index")
    {
        //show($objetos);
        $composentesql = "SELECT aa.nombre AS componente from coor_tmae_cronograma_componente_cab aa 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bb on aa.id=bb.id_componente
            where bb.id=coor_tmov_compromisos_cab.id_objetivo_componente";
        $objetivosql = "SELECT bx.objetivo from coor_tmae_cronograma_componente_cab ax 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bx on ax.id=bx.id_componente
            WHERE bx.id=coor_tmov_compromisos_cab.id_objetivo_componente";
        $programasql = str_replace("bx.objetivo", "bx.programa", $objetivosql);
        /*============================*/
        $coordinacion = "SELECT nombre from coor_tmae_coordinacion_main AS a INNER JOIN coor_tmov_coordinacion_direccion_deta AS b
	                    WHERE a.id=b.id_coor_cab AND b.id_direccion=coor_tmov_compromisos_cab.id_direccion
            ";
        $tabla = CabCompromisoalcaldiaModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_compromisos_cab.id_direccion")
            ->join("parroquia as p", "p.id", "=", "coor_tmov_compromisos_cab.id_parroquia")
            ->join("users as u", "u.id", "=", "coor_tmov_compromisos_cab.id_usuario")
            ->select(
                "coor_tmov_compromisos_cab.*",
                "a.direccion",
                "p.parroquia",
                "u.name as usuario",
                DB::raw("(select GROUP_CONCAT(x.name SEPARATOR ', ') from coor_tmov_compromisos_resposables AS y inner join users as x on x.id = y.id_usuario where y.id_crono_cab = coor_tmov_compromisos_cab.id limit 1) as idusuario"),
                DB::raw("ifnull(($composentesql),'') as componente"),
                DB::raw("ifnull(($objetivosql),'') as objetivo"),
                DB::raw("ifnull(($programasql),'') as programa"),
                DB::raw("ifnull(($coordinacion),'') as coordinacion")
            )
            ->where("coor_tmov_compromisos_cab.estado", "ACT")
            ->orderby("coor_tmov_compromisos_cab.id", "desc");
        //show($tabla->get());
        $seleccionar_direccion = direccionesModel::where("estado", "ACT");
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $id_usuario = Auth::user()->id;
        if ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 2) {
            /*if (count($objetos)) {
                unset($objetos[9]);
                unset($objetos[10]);
            }*/
            $seleccionar_direccion = $seleccionar_direccion->where("id", Auth::user()->id_direccion);
            $tabla = $tabla->where("coor_tmov_compromisos_cab.id_direccion", Auth::user()->id_direccion);
            if ($id_tipo_pefil->tipo == 2)
                $tabla = $tabla->whereRaw("$id_usuario in(select ff.id_usuario from coor_tmov_compromisos_resposables as ff where ff.id_crono_cab = coor_tmov_compromisos_cab.id)");
        }


        ////////////////
        if ($id_tipo_pefil->tipo == 10) {
            $direcciones = getDireccionesCoor(Auth::user()->id);
            $tabla = $tabla->whereIn("coor_tmov_compromisos_cab.id_direccion", explode(",", $direcciones->id_direccion . "," . Auth::user()->id_direccion));
        }



        $seleccionar_direccion = $seleccionar_direccion->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        //show($seleccionar_direccion);
        if (count($objetos))
            $objetos[6]->Valor = $this->escoja + $seleccionar_direccion;
        if ($tipo == "index") {
            $objetos[6]->Nombre = "direccion";
            $objetos[2]->Nombre = "parroquia";
        }
        //show(array($objetos, $tabla->get()));
        //Perfil Chaleco Azul
        if ($id_tipo_pefil->tipo == 7) {
            unset($objetos[0]);
            unset($objetos[4]);
            unset($objetos[5]);
            unset($objetos[9]);
            unset($objetos[12]);
        }/*elseif ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 2)
        {

        }*/
        //show($objetos);
        return array($objetos, $tabla);
    }

    public function compromisosalcaldiablueajax(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        //show($tabla);
        $objetos = $tabla[0];
        $tabla = $tabla[1];
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos);
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 9);
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        //show($columns);
        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('coor_tmov_compromisos_cab.id', 'LIKE', "%{$search}%")
                    ->orWhere('coor_tmov_compromisos_cab.estado_actividad', 'LIKE', "%{$search}%")
                    ->orWhere('a.direccion', 'LIKE', "%{$search}%")
                    ->orWhere('coor_tmov_compromisos_cab.actividad', 'LIKE', "%{$search}%")
                    ->orWhere('a.direccion', 'LIKE', "%{$search}%")
                    ->orWhere('p.parroquia', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            $permisos = $this->perfil();
            foreach ($posts as $post) {
                //=============================================================
                $show = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                //'&nbsp;&nbsp;' .
                $edit = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id), "menu" => "no"), array('class' => 'fa fa-pencil-square-o divpopup', 'onclick' => 'popup(this)'));
                //. '&nbsp;&nbsp;
                $dele = '<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                if ($permisos->ver == 'NO')
                    $show = "";
                if ($permisos->editar == 'NO')
                    $edit = "";
                if ($permisos->eliminar == 'NO')
                    $dele = "";
                $aciones = "$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                //=============================================================
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $valorc = $post->$campo;
                    $nestedData["$value->Nombre"] = "$valorc";
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos[0]);
        //show($objetos);
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 9);
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si",
            "permisos" => $this->perfil()
        ]);
        //return view('coordinacioncronograma::index');
    }
    public function asignaroles($objetos, $permisos, $tabla)
    {
        //show($permisos);
        if ($permisos->perfil == 1 || $permisos->perfil == 4 || $permisos->perfil == 10)
            return $objetos;
        //if ($permisos->editar_parcialmente == 'SI' && $permisos->guardar == 'SI') {
        //show($objetos);
        $tipo = "text";
        foreach ($objetos as $key => $value) {
            // show($objetos);
            if ($value->Tipo == "select" && $value->Nombre != "avance") {
                $tipo = "selectdisabled";
                if ($objetos[$key]->ValorAnterior != "Null") {
                    $pos = $objetos[$key]->ValorAnterior;
                    if ($pos == 27) {
                        // dd($value);
                    };
                    if (!is_array($pos))
                        $objetos[$key]->Valor = array($pos => $value->Valor[$pos]);
                    else
                        foreach ($pos as $keyaa => $vaa)
                            $objetos[$key]->Valor = array($keyaa => $value->Valor[$vaa]);
                } else {
                    //
                    $in = $objetos[$key]->Nombre;
                    $kk = $tabla->$in;
                    $aar = $objetos[$key]->Valor;
                    if (array_key_exists($kk, $aar)) {
                        //$objetos[$key]->Tipo = "html2";
                        $objetos[$key]->Valor = array($kk => $aar[$kk]);
                    }
                }
            } elseif ($value->Tipo == "text") {
                $tipo = "textdisabled";
                //$objetos[$key]->Valor ="Hola";
            } elseif ($value->Tipo == "textarea"  && $value->Nombre != "observacion") {
                $tipo = "textarea-disabled";
                //$objetos[$key]->Valor ="Hola";
            } elseif ($value->Tipo == "datetext") {
                $tipo = "textdisabled";
                //$objetos[$key]->Valor ="Hola";
            } else
                $tipo = $objetos[$key]->Tipo;
            $objetos[$key]->Tipo = $tipo;
            //show($tabla);

        }
        //}
        //show($objetos);
        return $objetos;
    }
    public function formularioscrear($id)
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        if($id==""){
            unset($estadoobras['EJECUCION_VENCIDO']);
            unset($estadoobras['EJECUTADO']);
            unset($estadoobras['SUSPENDIDO']);
        }
        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);
        $estado = $this->escoja + $estadoobras;
        $objetos[0]->Valor = $estado;
        //verificacion

        // show($objetos);
        $verificacion = explodewords(ConfigSystem("verificacion"), "|");
        $objetos[14]->Valor = $this->escoja + $verificacion;
        if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4) {
            unset($objetos[14]);
            unset($objetos[15]);
        }

        //Parroquia
        $seleccionar_parroquia = parroquiaModel::where("estado", "ACT")->pluck("parroquia", "id")->all();
        $objetos[2]->Valor = $this->escoja + $seleccionar_parroquia;
        //========================================================
        //Fechas
        $fecha = date("Y-m-d");
        $objetos[4]->Valor = $fecha;
        $objetos[5]->Valor = $fecha;
        /*Dirección*/
        $seleccionar_direccion = direccionesModel::where("estado", "ACT")->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        $objetos[6]->Valor = $this->escoja + $seleccionar_direccion;
        $objetos[7]->Valor = $this->escoja;
        /*Avance*/
        $Avance = explodewords(ConfigSystem("avance"), "|");
        $objetos[9]->Valor = $this->escoja + $Avance;
        /*Prioridad*/
        $prioridad = explodewords(ConfigSystem("prioridad"), "|");
        unset($prioridad["COMPROMISOS_ALCALDIA"]);
        unset($prioridad["COMPROMISOS_PLANIFICACION"]);
        $objetos[13]->Valor = $prioridad;
        //unset($objetos[11]);
        /*QUITAR CAMPOS*/
        unset($objetos[11]);
        unset($objetos[12]);
        //show($objetos);
        //show($objetos[0]);
        /*Lo siguiente permite acargar archivos en Editar*/
        /*$this->configuraciongeneral[3] = "4";
        $this->configuraciongeneral[4] = "si";
        */
        $obaddjs = array();
        $objetosmain = $this->verpermisos($objetos, "crear");
        /*Componentes*/
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $com = CoorComponenteModel::where("estado", "ACT")->select(DB::raw("concat(nombre,' - ',detalle) as componente"), "id")
        ->orderBy('nombre','ASC')->pluck("componente", "id")->all();
        $objetoscomponentes[0]->Valor = $this->escoja + $com;
        $objetoscomponentes[1]->Valor = $this->escoja;
        $objetoscomponentes[2]->Valor = $this->escoja;
        $objetos = array_merge($objetoscomponentes, $objetosmain[0]);
        //show($objetos);
        /*====================================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"id_coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $cmb = CoordinacionCabModel::join("users as a", "a.id", "=", "coor_tmae_coordinacion_main.id_responsable")
            //->select("coor_tmae_coordinacion_main.*","a.name as responsable")
            ->select("coor_tmae_coordinacion_main.id", DB::raw("concat(coor_tmae_coordinacion_main.nombre,': ',a.name) as nombre"))
            ->where("coor_tmae_coordinacion_main.estado", "ACT")->pluck("nombre", "id")->all();
        $coordinacion->Valor = $this->escoja + $cmb;
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 9);



        /*====================================================*/
        /*====================================================*/
        //subdirecion
        $coordinacion = '{"Tipo":"select","Descripcion":"Sub dirección","Nombre":"id_sub_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);

        $coordinacion->Valor = [];

        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 11);
        /*====================================================*/


        if ($id != "") {
            // dd($objetos);
            $id = Crypt::decrypt($id);
            $tabla = CabCompromisoalcaldiaModel::find($id);

            if ($id_tipo_pefil->tipo != 4) {
                $avance = [];
                $valor = $tabla->avance;
                if ($valor == null) {
                    $avance + $this->escoja;
                    $valor = 0;
                }
                for ($i = $valor; $i <= 100; $i++) {
                    $avance = $avance + [$i => $i];
                    $i = $i + 4;
                }
                $objetos[14]->Valor = $avance;
            }
            //========================================================
            //Fechas
            $fecha = date("Y-m-d");
            $objetos[7]->Valor = $tabla->fecha_inicio;
            $objetos[8]->Valor = $tabla->fecha_fin;

            if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) {
                $objetos[14]->ValorAnterior = $tabla->verificacion;
            }
            //==========================================================
            $this->configuraciongeneral[2] = "editar";
            $this->configuraciongeneral[3] = "9"; //Tipo de Referencia de Archivo
            $this->configuraciongeneral[4] = "si";
            //show($tabla);
            //$objetos = $this->verpermisos($objetos, "editar");
            //$objetos=$objetos[0];
            $valanteriorobj = CoorComponenteObjetivosModel::find($tabla->id_objetivo_componente);
            if ($valanteriorobj) {
                //$valanteriorpro = clone $valanteriorobj;
                $valanteriorpro = $valanteriorobj;
                $valanteriorcom = CoorComponenteModel::find($valanteriorobj->id_componente);
                $objetos[0]->ValorAnterior = array($valanteriorcom->id => $valanteriorcom->id);
                $objetos[1]->Valor = array($valanteriorobj->id => $valanteriorobj->objetivo);
                $objetos[2]->Valor = array($valanteriorpro->id => $valanteriorpro->programa);
            }

            /*====================================*/
            /*====================================*/
            //dsub direcion
            //Valor Anterior
            $ante = SubDireccionModel::join('tmae_direcciones as d', 'd.id', '=', 'tmae_direccion_subarea.area')->select('tmae_direccion_subarea.id_direccion','d.direccion')->where("area", $tabla->id_direccion)->first();
            // show($tabla);
            // show($tabla->id_direccion);
            if ($ante) {
                // dd($objetos[10]);
                $id_sub = $tabla->id_direccion;
                $objetos[11]->ValorAnterior = $id_sub;
                $objetos[10]->ValorAnterior = $id_sub;
                $objetos[11]->Valor =[$id_sub=>$ante->direccion];
                $tabla->id_direccion = $ante->id_direccion;
                // if (array_key_exists(11, $objetos))
                $ante_sub = SubDireccionModel::join('tmae_direcciones as d', 'd.id', '=', 'tmae_direccion_subarea.area')->select('d.id', 'd.direccion')
                    ->where("id_direccion", $ante->id_direccion)->pluck('direccion', 'id')->all();

                $objetos[12]->Valor = $ante_sub;
                $objetos[12]->ValorAnterior = $id_sub;
                // dd($objetos);
                // dd($objetos[11]);
                // dd($tabla);
                // show($objetos);
            }




            /*====================================*/
            /*====================================*/
            //COORDINACION
            //Valor Anterior
            $ante = CoordinacionDetaDireccionModel::where("id_direccion", $tabla->id_direccion)->first();
            // show($ante);
            //show($objetos);
            if ($ante) {
                $ante = array($ante->id_coor_cab => $ante->id_coor_cab);
                //                show($ante);
                if ($id_tipo_pefil->tipo != 7)
                    $objetos[9]->ValorAnterior = $ante;
                /*else
                    $objetos[6]->ValorAnterior=array();*/
                //else
                //                    $objetos[9]->ValorAnterior=$ante;
            }






            //show($objetos);
            if (array_key_exists(15, $objetos))
                $objetos[15]->ValorAnterior = intval($tabla->avance);

            /*Responsables*/
            $valorAnteriorpersonas = CompromisosResponsablesModel::join("users as a", "a.id", "=", "coor_tmov_compromisos_resposables.id_usuario")
                ->where("coor_tmov_compromisos_resposables.id_crono_cab", $id)
                ->select("a.*");
            //show($valorAnteriorpersonas);
            if (array_key_exists(12, $objetos)) {
                $objetos[12]->Valor = $valorAnteriorpersonas->pluck("name", "id")->all();
                $objetos[12]->ValorAnterior = $valorAnteriorpersonas->pluck("id", "id")->all();
            }

            //show($obaddjs);
            // show($objetos);
            //$objetos[0]->Tipo="selectdisabled";
            //array_merge($obaddjs,$obadd);
            //$obadd=$obaddjs;
            // show($historialtable);
            $objetos = $this->asignaroles($objetos, $this->perfil(), $tabla);
            
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
                "permisos" => $this->perfil(),
                //"historialtable"=>$historialtable,
                //"obadd"=>$obaddjs,
                "botonguardaravance" => null,
                "timeline" => $this->construyetimeline($id)
            );
            /*Setear*/
        } else {
            $this->configuraciongeneral[2] = "crear";
            //$objetos = $this->verpermisos($objetos, "crear")[0];
            /*Setear Chaleco Azul*/
            // show($objetos);
            unset($objetos[14]);
            unset($objetos[17]);
            unset($objetos[18]);
            
            $datos = array(
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            );
        }


        //show($objetos);
        if ($this->configuraciongeneral[2] == "editar") {
            //            $this->configuraciongeneral[3] = "9"; //Tipo de Referencia de Archivo
            //            $this->configuraciongeneral[4] = "si";
            $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "=", "pdf"], ["tipo", "9"]])->get();
            $dicecionesImagenes = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "<>", "pdf"], ["tipo", "9"]])->get();
        } else {
            $dicecionesDocumentos = null;
            $dicecionesImagenes = null;
        }
        $files = array(
            'dicecionesImagenes' => $dicecionesImagenes,
            'dicecionesDocumentos' => $dicecionesDocumentos
        );
        $datos = array_merge($datos, $files);
        //show($datos);
        return view('vistas.create', $datos);
    }
    public function construyetimeline($id)
    {
        /*Historial Tabla*/
        $historialtable = CabCompromisoalcaldiaDetaModel::join("users as a", "a.id", "=", "coor_tmov_compromisos_deta.id_usuario")
            ->join("parroquia as p", "p.id", "=", "coor_tmov_compromisos_deta.id_parroquia")
            ->join("tmae_direcciones as d", "d.id", "=", "coor_tmov_compromisos_deta.id_direccion")
            ->where("coor_tmov_compromisos_deta.id_crono_cab", $id)
            ->select(
                "coor_tmov_compromisos_deta.*",
                "a.name as usuario",
                "p.parroquia",
                "d.direccion",
                DB::raw("(select GROUP_CONCAT(x.name SEPARATOR ', ') from coor_tmov_compromisos_resposables AS y inner join users as x on x.id = y.id_usuario where y.id_crono_cab = coor_tmov_compromisos_deta.id_crono_cab) AS idusuario")
            )
            ->orderby("coor_tmov_compromisos_deta.updated_at")
            ->get();
        //show($historialtable);
        $obaddjs = json_decode($this->objetos);
        //            $obaddjs=$objetos;
        //show($obaddjs);
        $obaddjs[2]->Nombre = "parroquia";
        $obaddjs[6]->Nombre = "direccion";
        return array($historialtable, $obaddjs);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return $this->formularioscrear("");
    }

    public function guardar($id)
    {
        //show($id);
        $input = Input::all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];
        $menuno = "";
        if (Input::has("menu")) {
            $menuno = "?menu=no";
            unset($input["menu"]);
        }
        DB::beginTransaction();
        try {
            if ($id == 0) {
                $ruta .= "/create";
                $guardar = new CabCompromisoalcaldiaModel;
                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro Compromiso Alcalde";
            } else {
                $ruta .= "/" . Crypt::encrypt($id) . "/edit";
                $guardar = CabCompromisoalcaldiaModel::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Compromiso Alcalde modificado";
            }

            //$input = Input::all();
            $validar = CabCompromisoalcaldiaModel::rules($id);
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
            if ($id_tipo_pefil->tipo == 3 || $id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 7) {
                unset($validar["estado_actividad"]);
                unset($validar["fecha_inicio"]);
                unset($validar["fecha_fin"]);
            }

            $validator = Validator::make($input, $validar);

            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $timeline = new CabCompromisoalcaldiaDetaModel;
                foreach ($input as $key => $value) {

                    if ($key != "_method" && $key != "_token" && $key != "idusuario" && $key != "componente" && $key != "objetivo" && $key != "programa" && $key !=  "id_coordinacion") {
                        if ($key == 'id_sub_direccion') {
                            if (array_key_exists('id_direccion', $input)) {
                                if ($input['id_direccion'] == 27) {
                                    $guardar->id_direccion = $value;
                                    $timeline->id_direccion = $value;
                                }

                                // dd('aqui');
                            }
                        } elseif ($key == 'archivo_final') {
                            $dir = public_path() . '/archivos_sistema/';
                            $docs = Input::file("archivo_final");
                            if ($docs) {
                                $fileName = "Archivo_Final-compromiso-$id" . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                                $ext = $docs->getClientOriginalExtension();
                                $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                                if (!in_array($ext, $perfiles)) {
                                    DB::rollback();
                                    $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                                    return redirect()->back()->withErrors([$mensaje])->withInput();
                                }


                                //shoW();
                                $oldfile = $dir . '' . $fileName;
                                if (is_file($oldfile)) {
                                    if ($id_tipo_pefil->tipo != 1   || $guardar->verificacion == "VERIFICADO CON OBSERVACIONES") {
                                        if (file_exists($oldfile))
                                            unlink($oldfile);
                                    } else {
                                        DB::rollback();
                                        $mensaje = "Ya se encuentra el archivo final registrado";
                                        return redirect()->back()->withErrors([$mensaje])->withInput();
                                    }
                                }
                                $docs->move($dir, $fileName);
                                $guardar->$key = $fileName;
                            }
                        } else {
                            $guardar->$key = $value;
                            $timeline->$key = $value;
                        }
                    }
                }
                // dd($guardar);
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                if($guardar->avance==100){
                    $guardar->estado_actividad = 'EJECUTADO';
                }
                //Componente
                $guardar->id_objetivo_componente = Input::get("objetivo");
                $guardar->save();
                $idcab = $guardar->id;
                /*Responsables*/
                if (Input::has("idusuario")) {
                    $multiple = Input::get("idusuario");
                    $json = [];
                    $guardarres = CompromisosResponsablesModel::where("id_crono_cab", $idcab)
                        ->delete();
                    foreach ($multiple as $key => $value) {
                        # code...
                        //Responsables
                        $guardarres = CompromisosResponsablesModel::where("id_usuario", $value)
                            ->where("id_crono_cab", $idcab)
                            ->first();
                        if (!$guardarres)
                            $guardarres = new CompromisosResponsablesModel;
                        $guardarres->id_usuario = $value;
                        $guardarres->id_crono_cab = $idcab;
                        $guardarres->save();
                        $json[] = $value;
                        //
                        $usuario = UsuariosModel::select("users.*")
                            ->where("id", $guardarres->id_usuario)
                            ->first();
                        //show($actividad);
                        $this->notificacion->notificacionesweb("Se le ha asignado un nuevo compromiso de alcaldía <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit', $usuario->id, "2c438f");
                        //$this->notificacion->EnviarEmail($usuario->email, "Nueva Actividad", "Se le ha asignado una nueva actividad", "<b>ACTIVIDAD: <b>#</b>" . $idcab . "</b> <br>" . $actividad, $configuraciongeneral[1] . '/' . $guardar->id . '/edit');
                    }
                    /**/
                    $timeline->id_crono_cab = $idcab;
                    $timeline->id_usuario = $guardar->id_usuario;
                    $timeline->persona_json = json_encode($json);
                    $timeline->ip = $guardar->ip;
                    $timeline->pc = $guardar->pc;
                    $timeline->id_objetivo_componente = Input::get("objetivo");
                    $timeline->save();

                    if ($guardar->verificacion == 'VERIFICADO CON OBSERVACIONES' && $id_tipo_pefil->tipo==4 ) {
                        $multiple = CompromisosResponsablesModel::where("id_crono_cab", $idcab)
                            ->get();
                        $guardar_verificacion = CabCompromisoalcaldiaModel::find($idcab);
                        $guardar_verificacion->avance = 60;
                        $guardar_verificacion->estado_actividad = 'EJECUCION_VENCIDO';
                        $guardar_verificacion->save();

                        foreach ($multiple as $key => $value) {
                            $usuario = UsuariosModel::select("users.*")
                                ->where("id", $value->id_usuario)
                                ->first();
                            $this->notificacion->notificacionesweb("Compromisos territoriales <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit', $usuario->id, "2c438f");
                            $this->notificacion->EnviarEmail($usuario->email, "Compromisos territoriales", "Compromisos territoriales", "<b>ACTIVIDAD: <b>#</b>" . $idcab . "</b> <br>" . $guardar_verificacion->actividad . "<br>" . $guardar_verificacion->observacion_verificacion, $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit');
                        }
                    }

                    Auditoria($msgauditoria . " - ID: " . $id);
                }
                DB::commit();
                Session::flash('message', $msg);
                return Redirect::to($this->configuraciongeneral[1] . $menuno);
            } //
        } catch (\Exception $e) {
            // ROLLBACK
            DB::rollback();
            $mensaje = $e->getMessage();
            return redirect()->back()->withErrors([$mensaje])->withInput();
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public
    function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public
    function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("coor_tmov_compromisos_cab.id", $id)->first();
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos[0]);
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 9);
        //show($tabla);
        //$this->configuraciongeneral[3] = "5";//Tipo de Referencia de Archivo
        //$this->configuraciongeneral[4] = "si";
        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "timeline" => $this->construyetimeline($id)
        ]);
        //return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public
    function edit($id)
    {
        return $this->formularioscrear($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public
    function update(Request $request, $id)
    {
        //
        //show($id);
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public
    function destroy($id)
    {
        //
        /*
     use Modules\Coordinacioncronograma\Entities\CabCompromisoalcaldiaModel;
use Modules\Coordinacioncronograma\Entities\CabCompromisoalcaldiaDetaModel;
     **/
        DB::beginTransaction();
        try {
            $tabla = CabCompromisoalcaldiaModel::find($id);
            $tabla->estado = 'INA';
            $tabla->id_usuario = Auth::user()->id;
            //$tabla->delete();
            $tabla->save();
            // demas modelos
            //$tabla2->delete();
            $tabla3 = CabCompromisoalcaldiaDetaModel::where("id_crono_cab", $id)->get();
            foreach ($tabla3 as $key => $value) {
                # code...
                $dele = CabCompromisoalcaldiaDetaModel::find($value->id);
                $dele->estado = 'INA';
                $dele->save();
            }
            // MovimientoComunicacionPersonasModel
            // CabPresupuestoModel
            // PresupuestoDetalleModel
            // MovComunicacionParroquiaModel

            //->update(array('estado' => 'INACTIVO'));
            DB::commit();
            Session::flash('message', 'El registro se eliminó Exitosamente!');
            if (Input::has('vista')) {
                return 'El registro se eliminó Exitosamente!';
            }
            return Redirect::to($this->configuraciongeneral[1]);
        } //Try Transaction
        catch (\Exception $e) {
            //  // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);
        }
    }
}
