<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use App\ArchivosModel;
use App\Http\Controllers\NotificacionesController;
use app\SubDireccionModel;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\CoordinacionCronograma\Entities\ConConveniosDetaModel;
use Modules\CoordinacionCronograma\Entities\ConConveniosModel;
use Modules\CoordinacionCronograma\Entities\ConEntidadesModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteModel;
use Modules\Coordinacioncronograma\Entities\CoorComponenteObjetivosModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionCabModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionDetaDireccionModel;
use Modules\Coordinacioncronograma\Entities\CoorPoaCabModel;
use stdClass;
// use Illuminate\Http\Request;
class ConConveniosDetaController extends Controller
{

    var $configuraciongeneral = array("CONVENIOS", "coordinacioncronograma/convenios", "index", 6 => "coordinacioncronograma/conveniosajax", 7 => "convenios");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Proyecto","Nombre":"proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Descripción del Proyecto","Nombre":"descripcion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },                
        {"Tipo":"select","Descripcion":"Estado convenio","Nombre":"estado_convenio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Entidad","Nombre":"id_entidad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Verificación","Nombre":"verificacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },      
        {"Tipo":"textarea","Descripcion":"Observacion de verificación","Nombre":"observacion_verificacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }    
                  ]';
    /*COmponente Sostenibilidad*/
    var $objetoscomponentes = '
        [
            {"Tipo":"select","Descripcion":"Componente","Nombre":"componente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
            {"Tipo":"select","Descripcion":"Objetivos","Nombre":"objetivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Programa","Nombre":"programa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]
    ';


    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "id_direccion" => "id_direccion: {
                            required: true
                        }",
        "proyecto" => "proyecto: {
                            required: true
                        }"
    );
    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }
    public function perfil($verificar = 'SI')
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        //show($id_tipo_pefil);
        $object = new stdClass;
        //show($id_tipo_pefil);
        // if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $verificar == 'NO') { //1 Administrador 4 Coordinador
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) { //1 Administrador 4 Coordinador
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            // dd('aqs');s

            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';

            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'NO';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        }

        return $object;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = json_decode($this->objetos);
        $objetos[2]->Nombre = "direccion";
        $objetos[4]->Nombre = "nombre";
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos);
        //$objetos = $this->verpermisos($objetos);
        //show($objetos );
        /*====================================*/
        /*====================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 4);
        //show($objetos);
        /*====================================*/


        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si",
            "permisos" =>  $this->perfil()
        ]);
        //return view('coordinacioncronograma::index');
    }



    public function verpermisos($objetos = array(), $tipo = "index")
    {
        //show($objetos);
        $composentesql = "SELECT aa.nombre AS componente from coor_tmae_cronograma_componente_cab aa 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bb on aa.id=bb.id_componente
            where bb.id=con_convenios.id_objetivo_componente";
        $objetivosql = "SELECT bx.objetivo from coor_tmae_cronograma_componente_cab ax 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bx on ax.id=bx.id_componente
            WHERE bx.id=con_convenios.id_objetivo_componente";
        $programasql = str_replace("bx.objetivo", "bx.programa", $objetivosql);
        /*============================*/
        $coordinacion = "SELECT nombre from coor_tmae_coordinacion_main AS a INNER JOIN coor_tmov_coordinacion_direccion_deta AS b
	                    WHERE a.id=b.id_coor_cab AND b.id_direccion=con_convenios.id_direccion
            ";
        /*============================*/
        $tabla = ConConveniosModel::
            where("con_convenios.estado", "ACT")
            ->join("tmae_direcciones as a", "a.id", "con_convenios.id_direccion")
            ->join("con_entidades as e", "e.id", "con_convenios.id_entidad")
            ->select(
                "con_convenios.*",
                "a.direccion",
                "e.nombre",
                DB::raw("ifnull(($composentesql),'') as componente"),
                DB::raw("ifnull(($objetivosql),'') as objetivo"),
                DB::raw("ifnull(($programasql),'') as programa"),
                DB::raw("ifnull(($coordinacion),'') as coordinacion")
            )
            ->orderBy("con_convenios.id", "desc");

        // dd($anio);
        // $tabla = $tabla->where('anio', $anio);

        // show($tabla->get()->toarray());
        return array($objetos, $tabla);
    }


    public function formularioscrear($id = "")
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        /*QUITAR CAMPOS*/
        $objetos = json_decode($this->objetos);
        $tbcom = direccionesModel::where("estado", "ACT")->orderby("direccion")->pluck("direccion", "id")->all();
        $entidades = ConEntidadesModel::where("estado", "ACT")->orderby("nombre")->pluck("nombre", "id")->all();
        //precontractual, contractual,ejecución
        // $estado_act = explodewords(ConfigSystem("estadopoa"), "|");
        $estado_act = explodewords(ConfigSystem("estadoobras"), "|");
        $avance = explodewords(ConfigSystem("avance"), "|");
        $verificacion = explodewords(ConfigSystem("verificacion"), "|");
        // show($objetos);
        $objetos[2]->Valor = $this->escoja + $tbcom;
        $objetos[3]->Valor = $this->escoja + $estado_act;
        $objetos[4]->Valor = $this->escoja + $entidades;
        $objetos[5]->Valor = $this->escoja + $verificacion;

        $objetosmain = $this->verpermisos($objetos, "crear");
        /*Componentes*/
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $com = CoorComponenteModel::where("estado", "ACT")->select(DB::raw("concat(nombre,' - ',detalle) as componente"), "id")
            ->pluck("componente", "id")->all();
        $objetoscomponentes[0]->Valor = $this->escoja + $com;
        $objetoscomponentes[1]->Valor = $this->escoja;
        $objetoscomponentes[2]->Valor = $this->escoja;
        $objetos = array_merge($objetoscomponentes, $objetosmain[0]);
        /*====================================================*/
        /*====================================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"id_coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        $cmb = CoordinacionCabModel::join("users as a", "a.id", "=", "coor_tmae_coordinacion_main.id_responsable")
            //->select("coor_tmae_coordinacion_main.*","a.name as responsable")
            ->select("coor_tmae_coordinacion_main.id", DB::raw("concat(coor_tmae_coordinacion_main.nombre,': ',a.name) as nombre"))
            ->where("coor_tmae_coordinacion_main.estado", "ACT")->pluck("nombre", "id")->all();
        $coordinacion->Valor = $this->escoja + $cmb;
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 5);
        /*====================================================*/
        // dd($objetos);
        $datos = array();
        if ($id == "") {
            $this->configuraciongeneral[2] = "crear";

            $anio = Input::get('anio');
            if ($anio == 2020) {
                $ante = CoordinacionDetaDireccionModel::where("id_direccion", Auth::user()->id_direccion)->first();
                if ($ante) {
                    $ante = array($ante->id_coor_cab);
                    $objetos[4]->ValorAnterior = $ante;
                }
                $objetos[5]->ValorAnterior = Auth::user()->id_direccion;
            }


            // show($objetos);
            $datos = [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            ];
        } else {
            $this->configuraciongeneral[2] = "editar";
            $id = intval(Crypt::decrypt($id));
            //return $this->construyetimeline($id);

            $tabla = $objetosmain[1]->where("con_convenios.id", $id)->first();
            // dd($id);
            // dd($objetosmain[1]->where("poa_tmae_cab.id", $id));
            $anio = $tabla->anio;
            $valanteriorobj = CoorComponenteObjetivosModel::find($tabla->id_objetivo_componente);
            if ($valanteriorobj) {
                //$valanteriorpro = clone $valanteriorobj;
                $valanteriorpro = $valanteriorobj;
                $valanteriorcom = CoorComponenteModel::find($valanteriorobj->id_componente);
                $objetos[0]->ValorAnterior = $valanteriorcom->id;
                $objetos[1]->Valor = array($valanteriorobj->id => $valanteriorobj->objetivo);
                $objetos[2]->Valor = array($valanteriorpro->id => $valanteriorpro->programa);
            }
            $objetos[7]->ValorAnterior = $tabla->estado_convenio;
            $objetos[8]->ValorAnterior = $tabla->id_entidad;
            $objetos[9]->ValorAnterior = $tabla->verificacion;
            // show($objetos);
            /*====================================*/
            /*====================================*/
            //COORDINACION
            //Valor Anterior
            $ante = CoordinacionDetaDireccionModel::where("id_direccion", $tabla->id_direccion)->first();
            if ($ante) {
                $ante = array($ante->id_coor_cab);
                $objetos[5]->ValorAnterior = $ante;
            }

            // $direccion = direccionesModel::where('id', $tabla->id_direccion)->first();

            // //Subdirecciones
            // $subdireccion = SubDireccionModel::join("tmae_direcciones as a", "a.id", "=", "tmae_direccion_subarea.area")
            //     ->select("a.direccion", "a.id")
            //     ->where("tmae_direccion_subarea.id_direccion", $tabla->id_direccion)
            //     ->orderby("tmae_direccion_subarea.id", "desc")
            //     ->pluck("direccion", "id")->all();

            //show($subdireccion);
            //Array de las Direcciones para Popup

            $verificacion = explodewords(ConfigSystem("verificacion"), "|");
            if ($id_tipo_pefil->tipo != 1  && $id_tipo_pefil->tipo != 4) {
                unset($objetos[12]);
                unset($objetos[13]);
            }

            // show($actividades);





            // dd($actividades);
            //////////////////////////////////////////////////CRHISTIAN///////////////////////////////////

            if ($this->configuraciongeneral[2] == "editar") {
                $this->configuraciongeneral[3] = "18"; //Tipo de Referencia de Archivo
                $this->configuraciongeneral[4] = "si";
                $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "=", "pdf"], ["tipo", "18"]])->get();
                $dicecionesImagenes = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "<>", "pdf"], ["tipo", "18"]])->get();
            } else {
                $dicecionesDocumentos = null;
                $dicecionesImagenes = null;
            }
            //Teomporal
            // dd($actividades);





            return view('vistas.create', [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
                // 'permisos' => $this->perfil(),
                'dicecionesImagenes' => $dicecionesImagenes,
                'dicecionesDocumentos' => $dicecionesDocumentos,
                // "permisos" => $this->perfil(),
                "permisos" =>  $this->perfil(),
                // "timeline" => $this->construyetimeline($id)
            ]);
        }

        return view('vistas.create', $datos);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if ($this->perfil()->crear == 'SI') {
            return $this->formularioscrear();
        } else {
            Session::flash('error', ' No tiene permiso para crear');
            return Redirect::to($this->configuraciongeneral[1]);
        };
    }

    public function guardar($id)
    {
        $input = Input::all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];
        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new ConConveniosModel;
            $msg = "😊 Registro Creado Exitosamente...! ";
            $msgauditoria = "Registro Satisfactorio " . $this->configuraciongeneral[0];
        } else {
            //$ruta .= "/$id/edit";
            $ruta .= "/" . Crypt::encrypt($id) . "/edit";
            $guardar = ConConveniosModel::find($id);
            $msg = "😊 Registro Actualizado Exitosamente...!";
            $msgauditoria = "Edición Satisfactoria " . $this->configuraciongeneral[0];
        }

        $input = Input::all();
        $arrapas = array();

        $validator = Validator::make($input, ConConveniosModel::rules($id));
        

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            DB::beginTransaction();
            try {
                $timeline = new ConConveniosDetaModel();
                foreach ($input as $key => $value) {

                    if ($key != "_method" && $key != "_token" && $key != "componente" && $key != "objetivo" && $key != "programa" && $key != "id_coordinacion" && $key != "menu" && $key != "ruta") {
                        $guardar->$key = $value;
                        $timeline->$key = $value;
                    }
                }



                
                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = \Request::getClientIp();
                $guardar->pc = \Request::getHost();
                //Componente
                $guardar->id_objetivo_componente = Input::get("objetivo");
                $guardar->save();
                $idcab = $guardar->id;
                /**/
                $timeline->id_convenio_cab = $idcab;
                $timeline->id_usuario = $guardar->id_usuario;
                $timeline->ip = $guardar->ip;
                $timeline->pc = $guardar->pc;
                $timeline->id_objetivo_componente = Input::get("objetivo");
                $timeline->save();
                Auditoria($msgauditoria . " - ID: " . $id);

                $idcab=$guardar->id;
                if ($guardar->verificacion == 'VERIFICADO CON OBSERVACIONES') {


                    $usuario = UsuariosModel::select("users.*")
                        ->join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
                        ->where(["id_direccion" => $guardar->id_direccion])
                        ->whereIn("p.tipo", [3, 10])
                        ->get();
                    // dd($usuario);

                    $guardar_verificacion = ConConveniosModel::find($idcab);
                    // $guardar_verificacion->avance = 60;
                    $guardar_verificacion->estado_convenio = 'EJECUCION_VENCIDO';
                    $guardar_verificacion->save();

                    foreach ($usuario as $key => $value_) {
                        # code...
                        $this->notificacion->notificacionesweb("Convenio <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . $idcab . '/edit', $value_->id, "2c438f");
                        $this->notificacion->EnviarEmail($value_->email, "Convenio ", "Convenio ha sido verificado con observaciones", $guardar->observacion_verificacion,  $this->configuraciongeneral[1] . '/' . $idcab . '/edit');
                    }
                }
                DB::commit();

                
            } //Fin Try
            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage() . " - " . $e->getLine();
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
            Session::flash('message', $msg);
            return Redirect::to($this->configuraciongeneral[1]);
        }
    }

    public function conveniosajax(Request $request)
    {
        
        $objetos = json_decode($this->objetos);
        $objetos[2]->Nombre = "direccion";
        $objetos[4]->Nombre = "nombre";
        $objetoscomponentes = json_decode($this->objetoscomponentes);
        $objetos = array_merge($objetoscomponentes, $objetos);
        /*====================================*/
        /*====================================*/
        //COORDINACION
        $coordinacion = '{"Tipo":"select","Descripcion":"Coordinación","Nombre":"coordinacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $coordinacion = json_decode($coordinacion);
        //show($coorinacion);
        $objetos = insertarinarray($objetos, $coordinacion, 5);
        // show($objetos);
        /*====================================*/
        $tabla = $this->verpermisos($objetos, null);
        $objetos = $tabla[0];
        
        // show($objetos);
        $tabla = $tabla[1];
        // show($tabla->get());
        //show($tabla->get());
        ///
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'id_direccion')->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
          
                $subarea = SubDireccionModel::where("area", $id_tipo_pefil->id_direccion)->first();
                if ($subarea)
                    $tabla->where('id_direccion', $subarea->id_direccion);
                else
                    $tabla->where('id_direccion', $id_tipo_pefil->id_direccion);
            
        }

        ////////
        if ($id_tipo_pefil->tipo == 10) {
            $direcciones = getDireccionesCoor(Auth::user()->id);
            $tabla = $tabla->whereIn("id_direccion", explode(",", $direcciones->id_direccion));
        }

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('con_convenios.id', 'LIKE', "%{$search}%")
                    ->orWhere('con_convenios.proyecto', 'LIKE', "%{$search}%")
                    ->orWhere('con_convenios.descripcion', 'LIKE', "%{$search}%")
                    ->orWhere('a.direccion', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {
                $permisos = $this->perfil();
                

                $acciones =""; //link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;' .
                $acciones =link_to_route(str_replace("/", ".",  $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id), "menu" => "no"), array('class' => 'fa fa-pencil-square-o divpopup', 'target' => '_blank'/*, 'onclick' => 'popup(this)'*/));
                //link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o'));

                if ($permisos->eliminar == 'SI') {
                    $acciones .= '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                }

                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    // dd($post);
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $nestedData['acciones'] = $acciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }
    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $tabla = ConConveniosModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        ConConveniosDetaModel::where('id_convenio_cab', $id)
            ->update(['estado' => 'INA']);
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
