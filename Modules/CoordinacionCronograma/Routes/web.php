<?php

use JasperPHP\JasperPHP as JasperPHP;
//Route::get("pruebalerta","");
Route::group(['prefix' => 'coordinacioncronograma', 'middleware' => ['https']],function () {
	/*JASPER*/

	Route::get('/', 'CoordinacionCronogramaController@index')->name('coordinacioncronograma.index');
	Route::resource('contratista', 'CoorContratistaController');
	Route::resource('listobras', 'CoorObrasController');
	Route::resource('cronogramacompromisos', 'CoorCronogramaCabController');
	Route::resource('tipoobra', 'CoorTipoObraController');

	// Route::resource('componente', 'CoorTipoProyectoController');
	Route::resource('componente', 'CoorTipoProyectoController');
	Route::resource('proyecto', 'CoorProyectoEstrategicoController');
	// Route::get('image', 'ImagenController@index');

	//actividad poa
	Route::resource('tipoactividadpoa', 'TipoActividadPoaController');
	Route::get('tipoactividadajax', 'TipoActividadPoaController@tipoactividadajax');

	///indicadores proyectos
	Route::resource('indicadoresproyecto', 'CoordinacionIndicadorProyectoController');



	////////indicadores 
	Route::resource('indicadores', 'IndicadoresFichasController');
	Route::get('indicadoresajax', 'IndicadoresFichasController@indicadoresajax');
	Route::post('agregar_item', 'IndicadoresFichasController@agregar_item');
	Route::get('consultar_sancion', 'IndicadoresFichasController@consultar_sancion');
	Route::get('consultar_mascotas', 'IndicadoresFichasController@consultar_mascotas');
	Route::get('consultarvalorAtencion', 'IndicadoresFichasController@consultarvalorAtencion');
	Route::get('getInicadoresDireccion/{id}', 'IndicadoresFichasController@getInicadoresDireccion');


	////////indicadores 
	Route::resource('unaser', 'MultasUnaserController');
	Route::get('unaserajax', 'MultasUnaserController@unaserajax');
	Route::get('consultar_placa/{placa}', 'MultasUnaserController@consultar_placa');


	////////indicadores 
	Route::resource('teletrabajo', 'TeleTeletrabajoSemanasController');
	Route::get('teletrabajoajax', 'TeleTeletrabajoSemanasController@teletrabajoajax');
	Route::post('deleteArchivoTeletrabajo', 'TeleTeletrabajoSemanasController@deleteArchivoTeletrabajo');
	Route::get('teletrabajoajaxgetarchivos', 'TeleTeletrabajoSemanasController@teletrabajoajaxgetarchivos');
	Route::get('teletrabajohistorial/{id}', 'TeleTeletrabajoSemanasController@teletrabajohistorial');


	//ajax
	Route::get('contratistaajax', 'CoorContratistaController@contratistaajax');
	Route::get('tipoobraajax', 'CoorTipoObraController@tipoobraajax');

	Route::get('obrasajax', 'CoorObrasController@obrasajax');
	Route::get('cronogramaajax', 'CoorCronogramaCabController@cronogramaajax');



	Route::resource('coordinacionlistaobras', 'CoordinacionListObrasController');
	Route::get("getValoresFiltroObras", "CoordinacionListObrasController@getValoresFiltroObras");
	Route::get("getComunicacionesFiltroObras", "CoordinacionListObrasController@getComunicacionesFiltroObras");

	Route::resource('coordinaciongraficosobras', 'CoordinacionCharsObrasController');
	Route::get("getValoresChartsObras", "CoordinacionCharsObrasController@getValoresChartsObras");

	Route::resource('coordinaciongraficos', 'CoordinacionChartsController');
	Route::get("getValoresCharts", "CoordinacionChartsController@getValoresCharts");


	Route::resource('coordinacionlista', 'CoordinacionListController');
	Route::get("getValoresFiltro", "CoordinacionListController@getValoresFiltro");
	Route::get("getTimeLine", "CoorObrasController@getTimeLine");
	Route::get("getComunicacionesFiltro", "CoordinacionListController@getComunicacionesFiltro");
	Route::get("traspasoobras", "CoorObrasController@traspasoobras");
	Route::get("traspasoactividad", "CoorCronogramaCabController@traspasoactividad");

	Route::resource('dashboard', 'DashboardController');
	Route::get("getChartLine", "DashboardController@getChartLine");

	Route::resource('dashboardproyectos', 'DashboardProyectosController');
	//Route::get("getChartLineProyectos","DashboardProyectosController@getChartLine");

	Route::get("actividadesvencidasindex", "CoorCronogramaCabController@actividadesvencidasindex");

	/*RECOMENDACIONES CONTRALORIA*/
	Route::resource('recomendaciones', 'RecomendacionContraloriaController');
	Route::get('recomendacionesajax', 'RecomendacionContraloriaController@recomendacionesajax');

	/**
	 * 
	 */
	Route::resource('reporteincidencia', 'ReporteincidenciaController');
	Route::get('ReporteincidenciaControllerajax', 'ReporteincidenciaController@ReporteincidenciaControllerajax');
	Route::get('getDelegadosByDireccion', 'ReporteincidenciaController@getDelegadosByDireccion')->name('delegados.ajax');
	Route::get('reporteincidencias', 'ReporteincidenciaController@reporteincidencias');
	Route::get('repors_incidencias', 'ReporteincidenciaController@repors_indicencias');
	Route::get('ajax_repors_indicencias', 'ReporteincidenciaController@ajax_repors_indicencias')->name('repors_indicencias.ajax');

	Route::get('getEntidad', 'ReporteincidenciaController@getEntidad');

	/*COMPROMISOS ALCALDÍA*/
	/*Route::resource('compromisosalcaldia', 'CoorCompromisosAlcaldiaController');
	Route::get('compromisosalcaldiaajax', 'CoorCompromisosAlcaldiaController@compromisosalcaldiaajax');
*/
	/*COMPROMISOS ALCALDÍA NUEVO*/
	Route::resource('compromisosalcaldiablue', 'CoorCompromisosAlcaldiaCabController');
	Route::get('compromisosalcaldiablueajax', 'CoorCompromisosAlcaldiaCabController@compromisosalcaldiablueajax');

	Route::get('pruebajsoncompromisos', 'CoorCompromisosAlcaldiaCabController@pruebajsoncompromisos');

	/*Componentes*/

	Route::get('componentesobjetivos', 'CoorComponentesObjetivosController@index');
	Route::get('componentesobjetivosajax', 'CoorComponentesObjetivosController@componentesobjetivosajax');

	/*INDICADORES DE GESTIÓN*/
	Route::resource('indicadoresgestion', 'GesCoorCabIndicadorGestionController');
	Route::resource('indicadoresgestionmain', 'GesIndicadoresController');
	Route::resource('indicadoresgestionsub', 'GesSubIndicadoresController');
	Route::get('subindicadoresgestionmainajax', 'GesSubIndicadoresController@subindicadoresgestionmainajax');
	Route::get('indicadoresgestionmainajax', 'GesIndicadoresController@indicadoresgestionmainajax');
	Route::get('indicadoresgestionajax', 'GesCoorCabIndicadorGestionController@indicadoresgestionajax');
	Route::get('getIndicadoresGestion', 'GesCoorCabIndicadorGestionController@getIndicadoresGestion');
	Route::get('getBarrios/{id_parroquia}', 'GesCoorCabIndicadorGestionController@getBarrios');
	Route::get('getCantones', 'GesCoorCabIndicadorGestionController@getCantones');





	/*Objetivos*/
	Route::resource('componentesobjetivosdeta', 'CoorComponentesObjetivosDetaController');
	Route::get('componentesobjetivosdetaajax', 'CoorComponentesObjetivosDetaController@componentesobjetivosdetaajax');
	/*Programas*/
	Route::resource('componentesprograma', 'CoorComponentesProgramaController');
	Route::get('componentesprogramaajax', 'CoorComponentesProgramaController@componentesprogramaajax');
	/*Ajax Objetivo*/
	Route::get("getcomponenteobjetivo", "CoorComponentesObjetivosDetaController@getcomponenteobjetivo");
	Route::get("getcomponenteprograma", "CoorComponentesObjetivosDetaController@getcomponenteprograma");
	/*Indicadores*/
	Route::resource('indicadoresmain', 'CoorIndicadorTipoController');
	Route::get('indicadoresmainajax', 'CoorIndicadorTipoController@indicadoresmainajax');
	Route::resource('indicadoresfiltros', 'CoorIndicadorTipoFiltroController');
	Route::get('indicadoresfiltrosajax', 'CoorIndicadorTipoFiltroController@indicadoresfiltrosajax');
	Route::resource('indicadoresdetalles', 'CoorIndicadorDetaController');
	Route::get('indicadoresdetallesajax', 'CoorIndicadorDetaController@indicadoresdetallesajax');
	//Gráficos Indicadores
	Route::get('estadisticaindicadoresget', 'CoorIndicadorTipoController@estadisticaindicadoresget');
	Route::get('resportes_estadisticos', 'DasboardPoaController@resportes_estadisticos');
	/**/
	Route::get("getcamposdatos", "CoorIndicadorDetaController@getcamposdatos");
	Route::get("combos/{id}/{tipo}", "CoorProyectoEstrategicoController@combos");
	/*POA*/
	Route::resource('poamain', 'CoorPoaCabController');
	Route::get('poamainajax', 'CoorPoaCabController@poamainajax');


	Route::post('add_act_poa', 'CoorPoaCabController@store_actividad');
	Route::post('add_act_tarea_poa', 'CoorPoaCabController@guardar_act_tarea');
	Route::post('eliminar_actividad/{id}', 'CoorPoaCabController@eliminar_actividad_poa');
	Route::get('get_actividad_poa/{id}', 'CoorPoaCabController@get_actividad_poa');
	Route::get('get_tareas_poa', 'CoorPoaCabController@get_tareas_poa');

	///////////////////////obras////////////////
	Route::post('add_act_obra', 'CoorObrasController@store_actividad_obras');
	Route::get('get_actividad_obra/{id}', 'CoorObrasController@get_actividad_obra');
	Route::get('show_actividad_obra/{id}', 'CoorObrasController@show_actividad_obra');
	Route::post('eliminar_actividad_obra/{id}', 'CoorObrasController@eliminar_actividad_obra');



	//////////////////proyecto///////////////////
	Route::post('add_act_proyecto', 'CoorProyectoEstrategicoController@store_actividad_proyecto');
	Route::post('eliminar_actividad_proyecto/{id}', 'CoorProyectoEstrategicoController@eliminar_actividad_proyecto');
	Route::get('get_actividad_proyecto/{id}', 'CoorProyectoEstrategicoController@get_actividad_proyecto');
	Route::get('show_actividad_proyecto/{id}', 'CoorProyectoEstrategicoController@show_actividad_proyecto');


	/////////////////////permisos circulacion
	Route::resource('reportecirculacion', 'ReporteCirculacionController');
	Route::get('reportecirculaciones', 'ReporteCirculacionController@reportecirculaciones');

	/*Coordinaciones*/
	Route::resource('coordinaciones', 'CoordinacionCabController');
	Route::get('coordinacionesajax', 'CoordinacionCabController@coordinacionesajax');
	Route::get("getdireccionescoordinacion", "CoordinacionCabController@getdireccionescoordinacion");

	Route::get('show_actividad/{id}', 'CoorPoaCabController@show_actividad');

	Route::resource('agendavirtual', 'AgendaVirtualController');
	Route::get('agendavirtualajax', 'AgendaVirtualController@agendavirtualajax');
	Route::post('atender', 'AgendaVirtualController@atender');
	Route::post('eliminar_archivo/{id}', 'AgendaVirtualController@eliminar_archivo');
	Route::get('getActividades', 'AgendaVirtualController@getActividades');
	Route::get('getCategoriaEvento', 'AgendaVirtualController@getCategoriaEvento');
	Route::get('verficar_existente/{fecha_inicio}/{fecha_fin}/{id}', 'AgendaVirtualController@verficar_existente');


	Route::resource('plancampania', 'PlanCampaniaController');
	Route::get('plancampaniaajax', 'PlanCampaniaController@plancampaniaajax');

	Route::get('mantapp', 'DashboardIncidenciasController@mantapp');
	Route::get('poa', 'DasboardPoaController@poa');
	Route::get('obras', 'DasboardPoaController@obras');
	Route::post('obrasdatos', 'DasboardPoaController@obrasdatos');
	Route::get('actividades', 'DasboardPoaController@actividades');
	Route::get('compromisos_territoriales', 'DasboardPoaController@compromisos_territoriales');
	Route::get('recomendaciones_contraloria', 'DasboardPoaController@recomendaciones_contraloria');
	Route::get('plan_propuesta', 'DasboardPoaController@plan_campana');
	Route::get('proyecto_estrategicos', 'DasboardPoaController@proyecto_estrategicos');
	Route::post('actividadesdatos', 'DasboardPoaController@actividadesdatos');
	Route::post('poadatos', 'DasboardPoaController@poadatos');
	Route::post('compromisos_territoriales_datos', 'DasboardPoaController@compromisos_territoriales_datos');
	Route::post('plan_campana_datos', 'DasboardPoaController@plan_campana_datos');
	Route::post('recomendaciones_contraloria_datos', 'DasboardPoaController@recomendaciones_contraloria_datos');
	Route::post('proyecto_estrategicos_datos', 'DasboardPoaController@proyecto_estrategicos_datos');
	Route::post('detalledasboard', 'DasboardPoaController@detalledasboard');




	//agenda turistiva
	Route::resource('agendaturistica', 'TurEventosController');
	Route::get('agendaturisticaajax', 'TurEventosController@agendaturisticaajax');
	Route::get('getActividadesAgendaTuristica', 'TurEventosController@getActividadesAgendaTuristica');
	//agenda turistiva
	Route::resource('publicaciones', 'MantappPublicacionesController');
	Route::get('publicacionesajax', 'MantappPublicacionesController@publicacionesajax');
	Route::post('deleteMultimediapublicaciones', 'MantappPublicacionesController@deleteMultimediapublicaciones');


	//Archivos Actividades
	Route::resource('archivosactividades', 'CoorArchivosActividadController');
	Route::get('archivosactividadesajax', 'CoorArchivosActividadController@archivosactividadesajax');
	//Convenios
	Route::resource('convenios', 'ConConveniosDetaController');
	Route::get('conveniosajax', 'ConConveniosDetaController@conveniosajax');
	//Certificaciones POA
	Route::resource('solicitapoa', 'PoaSolicitaAutorizacionController');
	Route::get('solicitapoaajax', 'PoaSolicitaAutorizacionController@solicitapoaajax');
	Route::get('pideautorizacionpoa/{id}', 'PoaSolicitaAutorizacionController@pideautorizacionpoa');
	Route::get('printautorizacionpoa/{id}', 'PoaSolicitaAutorizacionController@printautorizacionpoa');
	//Notificación Temporal
	//Route::get('tmpEnviarNotificacionBorrador', 'PoaSolicitaAutorizacionController@tmpEnviarNotificacionBorrador');

	//Plan Estratégico Archivos
	Route::resource('planestrategicoarchivos', 'PlanEstrategicoFileController');
	Route::get('planestrategicoarchivosajax', 'PlanEstrategicoFileController@planestrategicoarchivosajax');
	Route::get('planestrategicoarchivosajaxgetarchivos', 'PlanEstrategicoFileController@planestrategicoarchivosajaxgetarchivos');

	//Plan emergencia
	Route::resource('planemergencia', 'PlanesEmergenciasCoorController');
	Route::get('planemergenciaajax', 'PlanesEmergenciasCoorController@planemergenciaajax');
	Route::get('planemergenciaajaxgetarchivos', 'PlanesEmergenciasCoorController@planemergenciaajaxgetarchivos');
	
	
	//informe
	Route::resource('informes_gestion', 'InformesGestionController');
	Route::get('informes_gestionajax', 'InformesGestionController@informes_gestionajax');
	Route::get('informes_gestionajaxgetarchivos', 'InformesGestionController@informes_gestionajaxgetarchivos');
	//aydudas tecnicas
	Route::resource('ayudas_memoria', 'AyudaMemoriaController');
	Route::get('ayudas_memoriaajax', 'AyudaMemoriaController@ayudas_memoriaajax');
	Route::get('ayudas_memoriaajaxgetarchivos', 'AyudaMemoriaController@ayudas_memoriaajaxgetarchivos');
	//aydudas tecnicas
	Route::resource('emergencia_sanitaria', 'EmergenciaSanitariaController');
	Route::get('emergencia_sanitariaajax', 'EmergenciaSanitariaController@emergencia_sanitariaajax');
	Route::get('emergencia_sanitariaajaxgetarchivos', 'EmergenciaSanitariaController@emergencia_sanitariaajaxgetarchivos');
	//ordenanzas
	Route::resource('ordenanzas', 'OrdenanzasController');
	Route::get('ordenanzasajax', 'OrdenanzasController@ordenanzasajax');
	Route::get('ordenanzasajaxgetarchivos', 'OrdenanzasController@ordenanzasajaxgetarchivos');

	Route::resource('convenios', 'ConveniosFileController');
	Route::get('conveniosajax', 'ConveniosFileController@conveniosajax');
	Route::get('conveniosajaxgetarchivos', 'ConveniosFileController@conveniosajaxgetarchivos');

	// Mascotas
	Route::resource('mascotasreporte', 'MascotasReporteController');
});
