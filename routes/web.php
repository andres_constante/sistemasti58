<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
/*WebAPI*/
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Headers:*');

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

Route::get('/soap', 'SoapController@soap');
Route::post('/soap_subida', 'SoapController@soap');
Route::resource("subdirecciones", "SubDireccionController");
Route::get("subdireccionesajax", "SubDireccionController@subdireccionesajax");
/*REPORTES*/
//Route::post('reportescoordinacionactividades', "ReportesJasperController@reporteActividades");
// Route::get("pruebasemana", function () {
//     $fecha = new DateTime();
//     //$fecha->modify('first day of previous month');
//     $fecha->modify('this week');
//     $fechadesde = $fecha->format('Y-m-d');
//     //$fechadesde= "2018-11-05 00:00:00";
//     //$fecha->modify('last day of this month');
//     $fecha->modify('this week +4 days');
//     $fechahasta = $fecha->format('Y-m-d');
//     show(array($fechadesde, $fechahasta));
// });
/**/
Route::get("home", function () {
    return Redirect::to("/");
});





Route::get('certificados_public', 'DashboardCertificados@certificados_online');

Route::get('reportespoa', 'ReportController@reportespoa');
Route::get('reportesunaser', 'ReportController@reportesunaser');
Route::get('reportesgeneral', 'ReportController@reportesgeneral');
Route::get('reportesporactividad', 'ReportController@reportesporactividad');
Route::get('reportespoacertificaciones', 'ReportController@reportespoacertificaciones');
Route::get('coordinacioncronograma/reportedonaciones', 'ReportController@donaciones');
Route::get('coordinacioncronograma/descargarDocumentos', 'ReportController@descargarDocumentos');
Route::get('repositorio_imagenes', 'WebApiController@repositorio_imagenes');
Route::get('descargarZip/{d}', 'WebApiController@descargarZip');
Route::get('coordinacioncronograma/reporte_ingreso_donaciones', 'ReportController@reporte_ingreso_donaciones');
Route::get('reportescompromisos', 'ReportController@reportescompromisos');
Route::get('reportesrecomendaciones', 'ReportController@reportesrecomendaciones');
Route::get('reportescoordinacionobras', 'ReportController@reportescoordinacionobras');
Route::get('reportesagenda', 'ReportController@reportesagenda');
Route::get('reportesplanpropuesta', 'ReportController@reportesplancampania');
Route::get('reportespermisos', 'ReportController@reportespermisos');
Route::get('reportesindicadores', 'ReportController@reportesindicadores');
Route::get('getTipoPermisos', 'ReportController@getTipoPermisos');
Route::get('reportesplanpropuestaajax', 'ReportController@reportesplanpropuestaajax');

//
Route::get("ajaxreportehtmlfiltrosactividades", "ReportController@ajaxreportehtmlfiltrosactividades");
Route::get('reportescoordinacionactividades', 'ReportController@reportescoordinacionactividades');
Route::get('reportegraficosfiltrosactividades', 'ReportController@reportegraficosfiltrosactividades');
Route::get('reportegraficosfiltrosObras', 'WebApiAgendaController@reportegraficosfiltrosObras');

Route::get('coordinacioncronograma/reportesTramitesExternos', 'ReportController@reportesTramitesExternos');
Route::get('reportesTramitesExternos', 'ReportController@reportesTramitesExternos');
Route::get('reportecovid', 'ReportController@reportecovid');


/*===========================*/
Route::group(['middleware' => ['cors', 'https']], function () {
    Route::get("initwebapp", "WebApiController@initwebapp");
    Route::get("verificarTokenn/{token}", "WebApiController@verificarToken");
    Route::get("getobras", "WebApiController@getobras");
    Route::get("getobras/{id}", "WebApiController@getobrasid");
    Route::get("getobrastotal", "WebApiController@getobrastotal");
    Route::get("getobrasparroquias", "WebApiController@getobrasparroquias");
    Route::get("getobrasparroquias/{id}", "WebApiController@getobrasparroquiasid");
    Route::get("consultarPublicaciones", "WebApiController@consultarPublicaciones");
    ////
    Route::get("getactividades", "WebApiController@getactividades");
    Route::get("getactividades/{id}", "WebApiController@getactividadesid");
    Route::get("getactividadestotal", "WebApiController@getactividadestotal");
    Route::get("getactividadesdireccion", "WebApiController@getactividadesdireccion");
    Route::get("getactividadesdireccion/{id}", "WebApiController@getactividadesdireccionid");
    ///////
    Route::get("getcomunicacionactitotal", "WebApiController@getcomunicacionactitotal");
    Route::get("getcomunicacionactilista", "WebApiController@getcomunicacionactilista");
    Route::get("getcomunicacionactidireccion", "WebApiController@getcomunicacionactidireccion");
    Route::get("getcomunicacionactidireccion/{id}", "WebApiController@getcomunicacionactidireccionid");
    Route::get("getcomunicacionactilista/{id}", "WebApiController@getcomunicacionactiid");

    // proyectos webApi
    Route::get("getcomponentes", "WebApiController@getcomponentes");
    Route::get("getcomponentes", "WebApiController@getcomponentes");
    Route::get("getcomponentesbyid/{id}", "WebApiController@getcomponentesbyid");
    Route::get("getproyectosbyid/{id}", "WebApiController@getproyectosbyid");
    Route::get("getproyectototal", "WebApiController@getproyectototal");
    Route::get("getsumacomponentestotal", "WebApiController@getsumacomponentestotal");


    // tramites alcaldia
    Route::get("getpeticionestotalgrafico", "WebApiController@getpeticionestotalgrafico");
    Route::get("getpeticiones", "WebApiController@getpeticiones");
    Route::get("getpeticiones/{id}", "WebApiController@getpeticionesby");
    Route::get("getTramitestotal", "WebApiController@getTramitestotal");

    /*Route::middleware('auth:api')->get('apruebatramite', function(Request $request) {
        return "Hola";
    });*/
    Route::post("apruebatramite", "WebApiController@apruebatramite");

    //agenda
    Route::get("consultar_eventos/{fecha}", "WebApiAgendaController@consultar_eventos");
    Route::get("vistosagenda", "WebApiAgendaController@vistosagenda");
    Route::get("consultarVisto", "WebApiAgendaController@consultarVisto");
    Route::get("verificarDisponibilidad/{id_usuario}/{f_i}/{f_f}", "WebApiAgendaController@verificarDisponibilidad");
    Route::get("verificarDisponibilidad2/{id_usuario}/{f_i}/{f_f}", "WebApiAgendaController@verificarDisponibilidad2");
    Route::get("consultarEstadisticaAgendaCategoria", "WebApiAgendaController@consultarEstadisticaAgendaCategoria");
    Route::get("consultarEstadisticaAgendaCategoriaDeta", "WebApiAgendaController@consultarEstadisticaAgendaCategoriaDeta");
    Route::post("atender_actividad", "WebApiAgendaController@atender_actividad");
    Route::post("ingresarVisto", "WebApiAgendaController@ingresarVisto");
    Route::post("asistir_acompaniar", "WebApiAgendaController@asistir_acompaniar");
    Route::post("ingresar_calendar", "WebApiAgendaController@ingresar_calendar")->name('agenda-api.store');


    Route::post("valida_registro_asistencia", "WebApiAgendaController@valida_registro_asistencia");
    Route::post("silenciar", "WebApiAgendaController@silenciar");
    Route::post("confirmar_asistencia", "WebApiAgendaController@confirmar_asistencia");
    Route::get("obtener_evento/{id}/{token}", "WebApiAgendaController@obtener_evento");
    Route::get("obtener_categoria_agenda/{token}", "WebApiAgendaController@obtener_categoria_agenda");
    Route::get("listar_eventos/{inicio}/{fin}/{token}", "WebApiAgendaController@listar_eventos_2");
    Route::get("listar_eventos_2/{inicio}/{fin}/{token}", "WebApiAgendaController@listar_eventos_2");
    Route::get("pendientes/{token}", "WebApiAgendaController@obtener_eventos_pendientes");
    Route::get("getDelegadosAlcalde", "WebApiAgendaController@getDelegadosAlcalde");
    Route::get("getActividadAgenda/{token}/{id}", "WebApiAgendaController@getActividadAgenda");
    Route::get("validarAgenda/{c}/{i}/{f}/{t}/{id}/{id_usuario}", "WebApiAgendaController@validarAgenda");
    Route::get("timelineagenda/{id}/{token}", "WebApiAgendaController@timelineagenda");

    Route::get("generarReporteAgenda", "WebApiAgendaController@generarReporteAgenda");
    Route::get("consultarEstadisticaAgenda", "WebApiAgendaController@consultarEstadisticaAgenda");
    Route::get("consultarEstadisticaAgendaParroquias", "WebApiAgendaController@consultarEstadisticaAgendaParroquias");



    /*Indicadores*/
    Route::get("getindicadoresmain", "WebApiController@getindicadoresmain");
    Route::get("getindicadorescombo", "WebApiController@getindicadorescombo");

    /*Reporte de Rendimiento de Direcciones*/
    Route::get("getrendimientoactividadesapp", "WebApiController@getrendimientoactividadesapp");
    Route::get("getPoaActividadesapp/{id_direccion}", "WebApiController@getPoaActividadesapp");
    Route::get("getPoaActividadesDireccionapp", "WebApiController@getPoaActividadesDireccionapp");
    Route::get("getPoaActividadesResumenapp", "WebApiController@getPoaActividadesResumenapp");
    Route::get("getCompromisosAlcaldia/{tipo}/{id_direccion}", "WebApiController@getCompromisosAlcaldia");



    /*Indicdores 2020*/
    Route::get("IndicadoresTransitoCategoria", "WebApiController@IndicadoresTransitoCategoria");
    Route::get("IndicadoresTransitoCategoriaId", "WebApiController@IndicadoresTransitoCategoriaId");
    Route::get("IndicadoresSancionesCategoria", "WebApiController@IndicadoresSancionesCategoria");
    Route::get("IndicadoresSancionesCategoriaId", "WebApiController@IndicadoresSancionesCategoriaId");
    Route::get("IndicadoresEventosCulturaCategoria", "WebApiController@IndicadoresEventosCulturaCategoria");
    Route::get("IndicadoresEventosCulturaCategoriaId", "WebApiController@IndicadoresEventosCulturaCategoriaId");
    Route::get("IndicadoresDeporteCategoria", "WebApiController@IndicadoresDeporteCategoria");
    Route::get("IndicadoresDeporteCategoriaId", "WebApiController@IndicadoresDeporteCategoriaId");
    Route::get("IndicadoresArbolitoCategoria", "WebApiController@IndicadoresArbolitoCategoria");
    Route::get("IndicadoresArbolitoCategoriaId", "WebApiController@IndicadoresArbolitoCategoriaId");
    Route::get("IndicadoresSeguridadCiudadanaCategoria", "WebApiController@IndicadoresSeguridadCiudadanaCategoria");
    Route::get("IndicadoresSeguridadCiudadanaCategoriaId", "WebApiController@IndicadoresSeguridadCiudadanaCategoriaId");
    Route::get("IndicadoresTurismoCategoria", "WebApiController@IndicadoresTurismoCategoria");
    Route::get("IndicadoresTurismoIdCategoriaId", "WebApiController@IndicadoresTurismoIdCategoriaId");
    Route::get("IndicadoresAreaMantenimientoCategoria", "WebApiController@IndicadoresAreaMantenimientoCategoria");
    Route::get("IndicadoresAreaMantenimientoCategoriaId", "WebApiController@IndicadoresAreaMantenimientoCategoriaId");

    Route::get("IndicadoresTerritorialesCategoria", "WebApiController@IndicadoresTerritorialesCategoria");
    Route::get("IndicadoresTerritorialesCategoriaId", "WebApiController@IndicadoresTerritorialesCategoriaId");

    ///
    Route::get('eventosAgendaCultural', 'WebApiAgendaController@eventosAgendaCultural');
    Route::get('eventosAgendaCulturalWeb', 'WebApiAgendaController@eventosAgendaCulturalWeb');
    Route::post('ingresarReaccionEvento', 'WebApiAgendaController@ingresarReaccionEvento');
    Route::post('ingresarComentarioEvento', 'WebApiAgendaController@ingresarComentarioEvento');
    Route::post('agregar_categoria', 'WebApiAgendaController@agregar_categoria');

    ////multas unser
    Route::get('getMultasUnaser', 'WebApiController@getMultasUnaser');
    Route::get('consultarvalorMulta', 'WebApiController@consultarvalorMulta');

    //Fechas conmemorativas
    Route::resource("coordinacioncronograma/FechasConmemorativas", "FechasConmemorativasController");
    Route::get("coordinacioncronograma/FechasConmemorativas/edit/{id}", "FechasConmemorativasController@edit");
    Route::post("coordinacioncronograma/FechasConmemorativas/update", "FechasConmemorativasController@update")->name('FechasConmemorativas.update');
    Route::post("coordinacioncronograma/FechasConmemorativas/destroy", "FechasConmemorativasController@destroy")->name('FechasConmemorativas.destroy');


    ///donaciones
    Route::get("validarDonaciones", "WebApiController@validarDonaciones");
    Route::get("donacionesCanastas", "WebApiController@donacionesCanastas");
    Route::get("verApadrina", "WebApiController@verApadrina");
}); //CORS



Route::group(['middleware' => ['cors', 'webapi_auth']], function () {
// Route::group(['middleware' => ['webapi_auth']], function () {
    Route::get("generarDocumentoApp/{id}", "WebApiController@generarDocumentoApp");
    Route::get("getOficios", "WebApiController@getOficios");
    Route::post("generarCodigoApp", "WebApiController@generarCodigoApp");
    Route::post("frimarCodigoApp/{id}/{codigo}", "WebApiController@generarDocumentoApp");
    Route::post("enviarObs", "WebApiController@enviarObs");
}); //CORS
Route::get("getDate", "WebApiController@getDate");
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////

/**/
Route::get("getmenumodulostree", "MenuModulosController@getmenumodulostree");
Route::get("getmenumodulos", "MenuModulosController@getmenumodulos");
Route::resource("menumodulos", "MenuModulosController");
Route::resource("modulos", "ModulosController");

////////////////////////////////////
Route::get("moduleschoice", "ModulosController@moduleschoice");

Route::get('layout', function () {
    return view('home');
});
Route::get('/', ['middleware' => ['https', 'auth'], function () {
    // Only authenticated users may enter...
    $id_tipo_pefil = App\UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
    if (Auth::user()->id == 1)
        return view('home');
    if (Session::has("escoger") && $id_tipo_pefil->tipo != 1) {
        return Redirect::to("moduleschoice");
    } else {
        $tabla = App\MenuModulosModel::join("ad_modulos as m", "m.id", "=", "ad_menu_modulo.id_modulo")
            ->join("menu as me", "me.id", "=", "ad_menu_modulo.id_menu")
            ->join("ad_menu_perfil as mp", "mp.id_menu_modulo", "=", "ad_menu_modulo.id")
            ->join("ad_usuario_modulo as um", "um.id_modulo", "=", "m.id")
            ->select("me.menu", "ad_menu_modulo.adicional", "ad_menu_modulo.ruta", DB::raw("count(*) as total"), DB::raw("max(um.id_usuario) as usu"))
            ->groupby("me.menu", "ad_menu_modulo.adicional", "ad_menu_modulo.ruta")
            ->where("um.id_usuario", Auth::user()->id)
            ->where("ad_menu_modulo.nivel", "<>", 1)->get();
        $totalmod = $tabla->count();
        //show($tabla);
        if ($totalmod == 1) {
            foreach ($tabla as $key => $value) {
                # code...
                return Redirect::to($value->ruta);
                break;
            }
        } else {
            return Redirect::to("moduleschoice");
        }
    }
    return view('home');
}]);

Auth::routes();

// Password reset link request routes...
// Route::get('password/email', ['as' => 'password/email', 'uses' => 'Auth\PasswordController@getEmail']);
// Route::post('password/email', ['as' => 'password/postEmail', 'uses' => 'Auth\PasswordController@postEmail']);

// Password reset routes...
// Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
// Route::post('password/reset', ['as' => 'password/postReset', 'uses' =>  'Auth\PasswordController@postReset']);
/*
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);
*/

//Menu
Route::resource('menu', 'MenuController');
//Usuarios
Route::resource('usuarios', 'UsuariosController');
Route::get('getUsers', 'UsuariosController@getUsers');
//Editar Datos del Usuario
Route::resource('cambiardatos', 'CambiaDatosController');
//Tipo de Usuario
Route::resource('tipousuario', 'TipousuarioController');
//Perfil de Usuario
Route::resource('perfil', 'PerfilController');

//menu perfil
Route::resource('menuperfil', 'MenuPerfilControler');
//usuariomudulo
Route::resource('usuariomodulo', 'UsuarioModuloControler');
//Permisos de Usuario
//Route::resource('permisosusuario','PermisosUsuariosController');
//Route::resource('permisosasignar','PermisosAsignarController');
//Permisos Perfiles
//Route::resource('perfilpermisos','PerfilPermisosController');
Route::resource('perfilpermisosasignar', 'AsignarPermisosPerfilController');
//Auditoría
Route::resource('auditoria', 'AuditoriaController');
//Variables de Configuracion
Route::resource('configsystem', 'SysConfigController');
//menu notificaciones
Route::resource('notificaciones', 'NotificacionesController');


//Archivos
Route::resource("subirarchivos", "ArchivosController");
// Route::post('archivos-gruardar', 'ArchivosController@store');
// Route::post('archivos-borrar', 'ArchivosController@destroy');
// Route::get('archivos-mostrar', 'ArchivosController@index');
Route::get('eliminarArchivo/{id}', 'ArchivosController@eliminarArchivo');

//AJAX INDEX
Route::get('usuariosajax', 'UsuariosController@usuariosajax');
Route::get('auditoriaajax', 'AuditoriaController@auditoriaajax');
Route::get('modulosajax', 'ModulosController@modulosajax');
Route::get('perfilesajax', 'PerfilController@perfilesajax');

///DONACIONES
Route::resource("coordinacioncronograma/donaciones", "DonaDonacionesController");
// Route::resource("donaciones","DonaDonacionesController");
Route::get("coordinacioncronograma/donacionesajax", "DonaDonacionesController@donacionesajax");
Route::get("donacionesajax", "DonaDonacionesController@donacionesajax");
Route::get("consultardinaciones", "DonaDonacionesController@consultardinaciones");
Route::post("agregar_item", "DonaDonacionesController@agregar_item");
Route::post("entregar_donacion", "DonaDonacionesController@entregar_donacion");
///DONACIONES
Route::resource("coordinacioncronograma/ingeso_donaciones", "DonaIngresoDonacionesController");
Route::get("coordinacioncronograma/ingresodonacionesajax", "DonaIngresoDonacionesController@ingresodonacionesajax");
Route::get("coordinacioncronograma/getProductos", "DonaIngresoDonacionesController@getProductos");
Route::get("coordinacioncronograma/getArchivosD", "DonaIngresoDonacionesController@getArchivosD");
Route::post("agregar_producto", "DonaIngresoDonacionesController@agregar_producto");

Route::get("coordinacioncronograma/donacionesTramites", "DonaDonacionesController@donacionesTramites");
Route::get("coordinacioncronograma/donacionesTramitesajax", "DonaDonacionesController@donacionesTramitesajax");



//////arboles
Route::resource("coordinacioncronograma/donaciones_arboles", "DonacionesArbolesController");
////permiso de circulacion
Route::resource("coordinacioncronograma/permisocirculacion", "PermisoCirculacionController");
Route::resource("coordinacioncronograma/guiacomercial", "EstablecimientosController");

// Encuesta
// Route::resource("coordinacioncronograma/encuesta", "EncuestaAdminController");
Route::get("coordinacioncronograma/encuesta", "EncuestaAdminController@index");
Route::get("coordinacioncronograma/encuesta/{id}", "EncuestaAdminController@show");
Route::get("encuesta_ajax", "EncuestaAdminController@encuesta_ajax");

// Route::get('users/restaurarsesion', 'UsuariosController@restaurarSesion')->name('revertir');

Route::get('users/restaurarsesion', function () {
    if (Session::has('current-user')) {
        $currentUser = session()->get('current-user');
        auth()->loginUsingId($currentUser);
        session()->forget('current-user');
        return redirect()->route('usuarios.index');
    } else return back();
})->name('revertir');
Route::get('users/{cedula}', 'UsuariosController@tomarSesion')->name('tomar.session');

Route::get("getnotificatodos", "NotificacionesController@getnotificatodos");
Route::post("coordinacioncronograma/CitaSave", "CitasController@CitaSave");
Route::get("getnotificacuenta", "NotificacionesController@getnotificacuenta");
Route::get("desactivarNoti/{id}", "NotificacionesController@desactivarNoti");
Route::get("actividadesVencidas", "NotificacionesController@actividadesVencidas");

Route::get("vercorreoprueba", "NotificacionesController@vercorreoprueba");

Route::resource("datoseguroconsultar", "DatoSeguroController");
Route::get("consultardatosseguro", "DatoSeguroController@consultardatosseguro");
Route::get("consultardatosseguro_data", "DatoSeguroController@consultardatosseguro_data");
Route::get("consultardatosseguro_ruc", "DatoSeguroController@consultardatosseguro_ruc");
Route::get("getPredios", "WebApiController@getPredios");
Route::get("probarNotificaciones", "WebApiAgendaController@probarNotificaciones");

Route::get("coordinacioncronograma/citas/{tipo}", "CitasController@index");
Route::post("notificarciudadano", "CitasController@notificarCiudadano")->name('notificarciudadano');
Route::get("getDirecciones", "WebApiController@getDirecciones");
Route::get("barridoDinardap", "WebApiController@barridoDinardap");
Route::get("sincronizaBarrios", "WebApiController@sincronizaBarrios");
Route::get("consultarImagenPublicacion", "WebApiController@consultarImagenPublicacion");

Route::get('/soapFirma', ['as' => 'soapFirma', 'uses' => 'SoapController@soapFirma']);
Route::get('pruebaDomcumento', "FirmaElectronicaController@pruebaDomcumento");



Route::get('validarTramiteExterno', "WebApiTramitesController@validarTramiteExterno");
Route::get('verfunciones', "WebApiTramitesController@verfunciones");
Route::get('tramitesExternosSolitadosajax', "WebApiTramitesController@tramitesExternosSolitadosajax");

Route::resource("encuesta", "EncuestaController");
Route::get("getBarriosEncuesta/{id_parroquia}", "EncuestaController@getBarrios");
Route::get("validaEncuesta", "EncuestaController@validaEncuesta");
