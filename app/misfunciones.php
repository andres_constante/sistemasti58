<?php

use App\MenuModulosModel;
use App\SysConfigModel;
use App\UsuariosModel;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Modules\ComunicacionAlcaldia\Entities\AgendaVirtualModelDeta;
use Modules\Coordinacioncronograma\Entities\CoordinacionCabModel;

use GuzzleHttp\Client as GuzzleHttpClient;
//use stdClass;
function qrvcard($code, $tramite, $url, $datos = "")
{
    $qrch = "
  BEGIN:VCARD
  VERSION:3.0
  N:GAD;MANTA
  FN:DOCUMENTO DIGITAL CORRECTO: $code
  ORG:GAD MANTA
  TITLE:$tramite
  EMAIL;;INTERNET:ciudadanodigital@manta.gob.ec
  BDAY:Proceso validado exitosamente
  END:VCARD";
    $tramite = trim($tramite);
    $code = trim($code);
    $datos = trim(reemplazaespeciales($datos, "NO"));
    $fechahora = date("Y-m-d") . " " . date("H:i:s");
    $qrch = "GAD MANTA\n$tramite\nCODIGO: $code\n$datos\n$fechahora";
    return $qrch;
}
function reemplazaespeciales($string, $especiales = "SI")
{

    $string = trim($string);

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C'),
        $string
    );
    if ($especiales == "SI") {
        //Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
            array(
                "\\", "¨", "º", "-", "~",
                "#", "@", "|", "!", "\"",
                "·", "$", "%", "&", "/",
                "(", ")", "?", "'", "¡",
                "¿", "[", "^", "`", "]",
                "+", "}", "{", "¨", "´",
                ">", "< ", ";", ",", ":",
                " "
            ),
            '',
            $string
        );
    }
    /*        $string = str_replace(
                array("\\", "¨", "º", "-", "~",
                     "#", "@", "|", "!", "\"",
                     "·", "$", "%", "&", "/",
                     "(", ")", "?", "'", "¡",
                     "¿", "[", "^", "`", "]",
                     "+", "}", "{", "¨", "´",
                     ">", "< ", ";", ",", ":",
                     ".", " "),
                '',
                $string
            );*/


    return $string;
}
function insertarinarray($objetos, $dato, $pos)
{
    $objetosnuevos = array();
    foreach ($objetos as $key => $value) {
        if ($key != $pos) {
            $objetosnuevos[] = $value;
        } else {
            $objetosnuevos[] = $dato;
            $objetosnuevos[] = $value;
        }
    }

    return $objetosnuevos;
}

function GetToken($url, $method = "POST")
{
    $client = new GuzzleHttpClient();
    try {
        $respuesta = $client->request($method, $url, [
            'headers' => [
                'content-type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                "Username" => "portalciudadano#*",
                "Password" => "SWEB@321*"
            ],
        ]);
        $consultawebserviceobs = json_decode($respuesta->getBody(), true);
    } catch (\Exception $e) {
        return array("error", $e->getMessage() . " " . $e->getLine());
    }
    return array("ok", $consultawebserviceobs);
}

function insertarhistorial($objetos, $id_cabecera, $tipo, $id_usuario)
{
    // $model = null;
    if ($tipo == 1) {
        $objetos_ = $objetos->toArray();
        $objetos_ = array_merge($objetos_, ["id_cab_agenda" => $id_cabecera, "id_usuario_modi" => $id_usuario]);
        if (isset($objetos_['id'])) {
            unset($objetos_['id']);
        }

        DB::table('com_tmov_comunicacion_agenda_deta')->insert($objetos_);
    }
}


function menus($modulo = 2)
{
    // $modulo=
    $mod = App\ModulosModel::get();
    $rutas = [];
    $tablamen = MenuModulosModel::join("menu as a", "a.id", "=", "ad_menu_modulo.id_menu")
        ->select("ad_menu_modulo.*", "a.menu")
        ->where("ad_menu_modulo.nivel", 1)
        ->orderby("ad_menu_modulo.idmain");
    $id_tipo_pefil = App\UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.id", "ap.tipo")->where("users.id", Auth::user()->id)->first();
    if ($modulo != 0 && $id_tipo_pefil->tipo != 1) {
        $tablamen = $tablamen->where("ad_menu_modulo.id_modulo", $modulo);
    }
    if ($id_tipo_pefil->tipo != 1) {
        $tablamen = $tablamen
            ->join("ad_menu_perfil as b", "b.id_menu_modulo", "=", "ad_menu_modulo.id")
            ->where("b.id_perfil", $id_tipo_pefil->id);
    }
    $tablamen = $tablamen->get();

    foreach ($tablamen as $key => $value) {
        $segundo_data = MenuModulosModel::join("menu as a", "a.id", "=", "ad_menu_modulo.id_menu")
            ->select("ad_menu_modulo.*", "a.menu")
            ->where("ad_menu_modulo.visible", "SI")
            ->where("ad_menu_modulo.nivel", 2)
            ->where("ad_menu_modulo.idmain", $value->id)->get();
        $rutas_segundo = [];
        foreach ($segundo_data as $kk => $vv) {
            $rutas_data = [];
            // dd($vv);
            $tercer_data = MenuModulosModel::join("menu as a", "a.id", "=", "ad_menu_modulo.id_menu")
                ->select("ad_menu_modulo.*", "a.menu")
                ->where("ad_menu_modulo.visible", "SI")
                ->where("ad_menu_modulo.nivel", 3)
                ->where("ad_menu_modulo.idmain", $vv->id)
                ->get();
            foreach ($tercer_data as $kkk => $vvv) {
                $rutas_data[] = [
                    'menu' => $vvv->menu,
                    'icono' => $vvv->icono,
                    'adicional' => $vvv->adicional,
                    'ruta' => $vvv->ruta
                ];
            }
            $rutas_segundo[] = [
                'menu' => $vv->menu,
                'icono' => $vv->icono,
                'ruta' => $vv->ruta,
                'adicional' => $vv->adicional,
                'tercero' => $rutas_data
            ];
        }
        $rutas[] = [
            'menu' => $value->menu,
            'icono' => $value->icono,
            'ruta' => $value->ruta,
            'adicional' => $value->adicional,
            'segundo' => $rutas_segundo
        ];
    }


    // $tablamen = $tablamen->where("ad_menu_modulo.id_modulo", $mod->id);
    // if ($mod) {
    // }

    // $tablamen = $tablamen->orderby("ad_menu_modulo.orden")->get();
    // dd($rutas);
    return $rutas;
    // $datos = collect($tablamen);
    // $datos->groupBy('nivel');
    // dd($datos->groupBy('nivel'));
}

function permisoingresoactividades()
{
    //show(date("H"));   
    $ret = false;
    $diasfinalizacion = explode(";", ConfigSystem("quitareditar"));
    foreach ($diasfinalizacion as $key => $value) {
        # code...
        $datosFinalizacon = explode("|", $value);
        // show($datosFinalizacon);
        $fechabd = $datosFinalizacon[0];
        $horabd = $datosFinalizacon[1];

        $fechaactual = date("D");
        $horaactual = date("H");

        if ($fechaactual == $fechabd && intval($horabd) < intval($horaactual)) {
            $ret = true;
        }
        if (Auth::user()->id == 1182) {
            $ret = false;
        }
    }
    return $ret;
}
function permisoingresoplanificacion($dias)
{
    //show(date("H"));   
    $ret = false;
    $diasfinalizacion = explode(";", $dias);
    foreach ($diasfinalizacion as $key => $value) {
        # code...
        $datosFinalizacon = explode("|", $value);
        // show($datosFinalizacon);
        $fechabd = $datosFinalizacon[0];
        $horabd = $datosFinalizacon[1];

        $fechaactual = date("D");
        $horaactual = date("H");

        if ($fechaactual == $fechabd && intval($horabd) < intval($horaactual)) {
            $ret = true;
            // dd($ret);
        }
        // dd($horaactual);
        if (Auth::user()->id == 1182) {
            // $ret = false;
        }
    }
    return $ret;
}



function getDireccionesCoor($id)
{
    $tabla = CoordinacionCabModel::where("coor_tmae_coordinacion_main.estado", "ACT")
        ->join("users as a", "a.id", "coor_tmae_coordinacion_main.id_responsable")
        ->select(
            "coor_tmae_coordinacion_main.*",
            "a.name as responsable",
            DB::raw("(select GROUP_CONCAT(x.id SEPARATOR ', ') AS id_direccion from coor_tmov_coordinacion_direccion_deta AS y 
            inner join tmae_direcciones as x on x.id = y.id_direccion where y.id_coor_cab = coor_tmae_coordinacion_main.id) as id_direccion")
        )
        ->where("a.id", $id)
        ->get();
    $direcciones = array();
    foreach ($tabla as $key => $value) {
        $direcciones[] = $value->id_direccion;
    }
    $resul = new stdClass;
    $resul->id_direccion = implode(",", $direcciones);
    /*show($resul);
     show($tabla->toarray());
     */
    return $resul;
}
function colortimelineestado($estado)
{
    $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
    $estadoobrascolores = explode("|", ConfigSystem("estadoobrascolor"));
    $estadoarray = array();
    $i = 0;
    foreach ($estadoobras as $key => $value) {
        # code...
        $estadoarray[$value] = $estadoobrascolores[$i];
        $i++;
    }
    return (isset($estadoarray[$estado])) ? $estadoarray[$estado] : "#fff";
}
function colorobjestado($tabla, $campo, $find = 0)
{
    $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
    $estadoobrascolores = explode("|", ConfigSystem("estadoobrascolor"));
    $estadoarray = array();
    $i = 0;
    foreach ($estadoobras as $key => $value) {
        # code...
        $estadoarray[$value] = $estadoobrascolores[$i];
        $i++;
    }
    if ($find == 0)
        foreach ($tabla as $value) {
            $value->color = $estadoarray[$value->$campo];
        } else
        $tabla->color = $estadoarray[$tabla->$campo];
    return $tabla;
}
function colorobjestadoproyecto($tabla, $campo, $find = 0)
{
    $estadoproyecto = explodewords(ConfigSystem("estadoproyecto"), "|");
    $estadoproyectoscolores = explode("|", ConfigSystem("estadoproyectocolor"));
    $estadoarray = array();
    $i = 0;
    foreach ($estadoproyecto as $key => $value) {
        # code...
        $estadoarray[$value] = $estadoproyectoscolores[$i];
        $i++;
    }
    if ($find == 0)
        foreach ($tabla as $value) {
            $value->color = $estadoarray[$value->$campo];
        } else
        $tabla->color = $estadoarray[$tabla->$campo];
    return $tabla;
}
function explodewords($string, $car = " ")
{
    $array = explode($car, $string);
    $retorno = array();
    foreach ($array as $key => $value) {
        # code...
        $retorno[$value] = $value;
    }
    return $retorno;
}
function show($objeto, $i = 1)
{
    echo "<pre>";
    print_r($objeto);
    if ($i == 1)
        die();
}

function runObject()
{
    for ($i = 0; $i < 10; $i++) {
        # code...
        echo $i;
    }
}

function ConfigSystem($clave = "")
{
    $data = SysConfigModel::where("campo", $clave)->first();
    $res = '';
    if (!empty($data)) {
        $res = $data->valor;
    }
    return $res;
}
function Auditoria($accion)
{
    $historial = new \App\AuditoriaModel();
    $historial->idusuario = Auth::user()->id;
    $historial->urlmenu = Request::getRequestUri();
    $historial->accion = $accion;
    $historial->ip = Request::getClientIp();
    $historial->nompc = Request::getHost();

    $historial->save();
}
/**
 * Devuelve una cadena de un número rellenado a la izquierda con un caracter. Ej. 10 => 000010.
 *
 * @param string $valor = Cadena a rellenar
 * @param int $longitud = Longitud de caracteres a rellenar
 * @param string $car = Caracter de relleno
 * @return string
 * @static 
 */
function Autorizar($url)
{
    //return;
    $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "ap.id")->where("users.id", Auth::user()->id)->first();
    //show($id_tipo_pefil->toarray());
    $rucu = $url;
    Route::currentRouteName(); //Route::getCurrentRoute()->getPath();
    //show($url);
    $vermodulo = explode("/", $rucu);
    //show($vermodulo);
    $mod = App\ModulosModel::where("ruta", $vermodulo[0])->first();
    //show($mod->toarray());
    if (!$mod) {
        $urlar =  explode("/", $url);
        $url = $urlar[0];
    } else {
        $urlar =  explode("/", $url);
        $url = $urlar[0] . "/" . $urlar[1];
    }
    //show($url);
    if ($url == "cambiardatos")
        return;
    //show($url);
    //die($url);
    //show($mod);

    if ($id_tipo_pefil->tipo != 1) {
        // $tabla=DB::table("menu as a")
        //     ->join("ad_menuusuario as b","a.id","=","b.idmenu") 
        //     ->select("a.*")
        //     ->where(function($query) use ($url){
        //         $query->where("a.ruta","like",$url."%")->where("b.idusuario",Auth::user()->id);
        //     })
        //     ->first();
        $tabla = MenuModulosModel::join("ad_modulos as m", "m.id", "=", "ad_menu_modulo.id_modulo")
            ->join("ad_usuario_modulo as um", "um.id_modulo", "=", "m.id")
            ->join("ad_menu_perfil as a", "a.id_menu_modulo", "=", "ad_menu_modulo.id")
            ->select("ad_menu_modulo.*", "um.id_usuario")
            ->where(function ($query) use ($url, $id_tipo_pefil) {
                $query->where("ad_menu_modulo.ruta", "like", $url . "%")
                    ->where("um.id_usuario", Auth::user()->id)
                    ->where("a.id_perfil", $id_tipo_pefil->id);
            })
            ->first();
        //show($tabla);
        /*echo "<pre>";
            print_r($tabla);
            die();*/
        if (empty($tabla)) {
            Session::flash("msgacceso", "Se detectó un acceso no permitido el " .  fechas(2) . " a las " . date("H:i:s") . ".");

            Auditoria("Acceso no permitido 403");
            die(Response::view('errors/403', array(), 403));


            //App::abort(403, 'Unauthorized action.');
            die("
                <html>
                    <head>
                        <meta charset='utf-8'>
                        <meta http-equiv='Refresh' content='5;url=" . URL::to(URL::previous()) . "'>
                    </head>
                <body>
                    <p><h1>Acceso denegado</h1><h2>Será redireccionado automáticamente a la página prinipal. Caso contrario haga clic <a href='" . URL::to(URL::previous()) . "'>aquí</a> para regresar</h2></p>
                </body>
                </html>");
        }
    }
}
/**
 * Devuelve una cadena de un número rellenado a la izquierda con un caracter. Ej. 10 => 000010.
 *
 * @param string $valor = Cadena a rellenar
 * @param int $longitud = Longitud de caracteres a rellenar
 * @param string $car = Caracter de relleno
 * @return string
 * @static 
 */
function CerosIzquierda($valor, $longitud = 10, $car = "0")
{
    return str_pad($valor, $longitud, $car, STR_PAD_LEFT);
}
/**
 * Devuelve una cadena de un número rellenado a la derecha con un caracter. Ej. 10 => 000010.
 *
 * @param string $valor = Cadena a rellenar
 * @param int $longitud = Longitud de caracteres a rellenar
 * @param string $car = Caracter de relleno
 * @return string
 * @static 
 */

function CerosDerecha($valor, $longitud = 10, $car = "0")
{
    return str_pad($valor, $longitud, $car, STR_PAD_RIGHT);
}
/**
 * Devuelve la fecha actual del sistema en formato date o letras
 *
 * @param int $a Tipo de Dato a devolver. (0) Fecha Hora Actual. (1) Solo fecha actual. (2) Fecha actual o fecha enviada en letras. (3) Fecha y hora actual
 * @param string $fecha SI no se desea fecha actual puede asignar una fecha personalizada. Ej. 2016-05-13
 * @return string Fecha: Ej. Viernes, 13 de mayo del 2016
 * @static 
 */
function fechas($a = 0, $fecha = '')
{
    $mes = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'diciembre');
    $dia = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
    switch ($a) {
        case 0: // Solo fecha actual y hora
            $fecha = date("Y-m-d") . " " . date("H:i:s");
            $fechar = $fecha;
            break;
        case 1: // Solo fecha actual
            $fecha = date("Y-m-d");
            $fechar = $fecha;
            break;
        case 2: // Devuelve la fecha en letras cuando se le envie una fecha cualquiera
            if ($fecha == '')
                $fecha = date("Y-m-d") . " " . date("H:i:s");
            $mesn = date("n", strtotime($fecha));
            $dian = date("w", strtotime($fecha));
            $diad = date("d", strtotime($fecha));
            $anio = date("Y", strtotime($fecha));
            $fecha = $dia[$dian] . ", $diad de " . $mes[$mesn - 1] . " de $anio";
            // dd($fecha);
            $fechar = utf8_decode($fecha);
            break;
        case 111: // Devuelve la fecha en letras cuando se le envie una fecha cualquiera
            if ($fecha == '')
                $fecha = date("Y-m-d") . " " . date("H:i:s");
            $mesn = date("n", strtotime($fecha));
            $dian = date("w", strtotime($fecha));
            $diad = date("d", strtotime($fecha));
            $anio = date("Y", strtotime($fecha));
            $fecha = $dia[$dian] . ", $diad de " . $mes[$mesn - 1] . " de $anio";
            // dd($fecha);
            $fechar = $fecha;
            break;
        case 3: // Devuelve la fecha en letras y la hora
            if ($fecha == '')
                $fecha = date("Y-m-d") . " " . date("H:i:s");
            $mesn = date("n", strtotime($fecha));
            $dian = date("w", strtotime($fecha));
            $diad = date("d", strtotime($fecha));
            $anio = date("Y", strtotime($fecha));
            $hora = " a las " . date("H:i:s", strtotime($fecha));
            $fecha = $dia[$dian] . ", $diad de " . $mes[$mesn - 1] . " de $anio $hora";
            $fechar = utf8_decode($fecha);
            break;
        case 600: // Devuelve la fecha en letras pero no el dia ni la hora
            if ($fecha == '')
                $fecha = date("Y-m-d") . " " . date("H:i:s");
            $mesn = date("n", strtotime($fecha));
            $dian = date("w", strtotime($fecha));
            $diad = date("d", strtotime($fecha));
            $anio = date("Y", strtotime($fecha));
            //  $hora=date("H:i:s",strtotime($fecha));
            $fecha = "$diad de " . $mes[$mesn - 1] . " del $anio";
            $fechar = utf8_decode($fecha);
            break;
        case 800: // Devuelve la edad
            if ($fecha != null) {
                list($Y, $m, $d) = explode("-", $fecha);
                $fechar = (date("md") < $m . $d ? date("Y") - $Y - 1 : date("Y") - $Y) . " Años";
            } else {
                $fechar = null;
            }

            break;
        case 1100: // Devuelve la edad
            if ($fecha != null) {
                list($Y, $m, $d) = explode("-", $fecha);
                $fechar = (date("md") < $m . $d ? date("Y") - $Y - 1 : date("Y") - $Y);
            } else {
                $fechar = null;
            }

            break;
        case 900: //Devuelve solo el dia de la fecha  
            if ($fecha == '')
                $fecha = date("d-m-Y");
            else
                $fecha = date("w", strtotime($fecha));
            $fechar = $dia[$fecha];
            break;
        case 1000:
            if ($fecha == '')
                $fecha = date("d-m-Y");
            else
                $fecha = date("H:i:s", strtotime($fecha));
            $fechar = $fecha;
            break;
        case 1200:
            if ($fecha == '')
                $fecha = date("H:i");
            else
                $fecha = date("H:i", strtotime($fecha));
            $fechar = $fecha;
            break;
        case 300: // Devuelve la fecha en letras cuando se le envie una fecha cualquiera
            if ($fecha == '')
                $fecha = date("Y-m-d") . " " . date("H:i:s");
            $mesn = date("n", strtotime($fecha));
            $dian = date("w", strtotime($fecha));
            $diad = date("d", strtotime($fecha));
            $anio = date("Y", strtotime($fecha));
            $hora = date("H:i:s", strtotime($fecha));
            $fecha = $dia[$dian] . ", $diad de " . $mes[$mesn - 1] . " de $anio a las $hora";
            $fechar = utf8_decode($fecha);
            break;
    }
    return $fechar;
}
function formatBytes($bytes, $precision = 2)
{
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}
//
/**
 * Comprueba si un número de cédula ecuatoriana es correcta o no
 *
 * @param string $strCedula = Cédula
 * @return int (1) Correcto (0) Incorrecto         
 */
function validarCI($strCedula)
{
    $suma = 0;
    $strOriginal = $strCedula;
    $intProvincia = substr($strCedula, 0, 2);
    $intTercero = $strCedula[2];
    $intUltimo = $strCedula[9];
    if (!settype($strCedula, "float")) return FALSE;
    if ((int) $intProvincia < 1 || (int) $intProvincia > 25) return FALSE;
    //if ((int) $intTercero == 7 || (int) $intTercero == 4 )
    //return FALSE;
    for ($indice = 0; $indice < 9; $indice++) {
        //echo $strOriginal[$indice],'</br>';
        switch ($indice) {
            case 0:
            case 2:
            case 4:
            case 6:
            case 8:
                $arrProducto[$indice] = $strOriginal[$indice] * 2;
                if ($arrProducto[$indice] >= 10) $arrProducto[$indice] -= 9;
                //echo $arrProducto[$indice],'</br>';
                break;
            case 1:
            case 3:
            case 5:
            case 7:
                $arrProducto[$indice] = $strOriginal[$indice] * 1;
                if ($arrProducto[$indice] >= 10) $arrProducto[$indice] -= 9;
                //echo $arrProducto[$indice],'</br>';
                break;
        }
    }
    foreach ($arrProducto as $indice => $producto) $suma += $producto;
    $residuo = $suma % 10;
    $intVerificador = $residuo == 0 ? 0 : 10 - $residuo;
    return ($intVerificador == $intUltimo ? 1 : 0);
}
/**
 * Limita el contenido de las palabras en una cadena de caracteres
 *
 * @param string $cadena Cadena a limitar
 * @param int $longitud Longitud de palabras a limitar
 * @return string Cadena limitada         
 */
function limitarPalabras($cadena, $longitud)
{
    $palabras = explode(' ', $cadena);
    if (count($palabras) > $longitud)
        return implode(' ', array_slice($palabras, 0, $longitud));
    else
        return $cadena;
}

function base64ToFilePdf($base64File)
{
    $fileData = base64_decode($base64File);
    if (strpos($fileData, '%PDF') !== 0) {
        throw new \Exception('Missing the PDF file signature');
    }
    $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString() . '.pdf';
    file_put_contents($tmpFilePath, $fileData);
    $tmpFile = new File($tmpFilePath);
    $file = new UploadedFile(
        $tmpFile->getPathname(),
        $tmpFile->getFilename(),
        $tmpFile->getMimeType(),
        0,
        true
    );

    return $file;
}
