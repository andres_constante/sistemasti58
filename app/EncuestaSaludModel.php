<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EncuestaSaludModel extends Model
{
    protected $table = 'tma_encuesta_salud';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                            
                'cedula' => 'required',
                'edad' => 'required',
                'sexo' => 'required',
                'celular' => 'required',
                'direccion_id' => 'required|numeric',
                'nombre' => 'required',
                'cargo' => 'required'
            ], $merge);
        }    
}
