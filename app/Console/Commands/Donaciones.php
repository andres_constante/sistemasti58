<?php

namespace App\Console\Commands;

use App\DonaDonacionesModel;
use Illuminate\Console\Command;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\Log;

class Donaciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'donaciones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        // $datos = DonaDonacionesModel::where(['estado' => 'ACT', 'estado_donacion' => 'ENTREGADO', 'tipo_documento' => 'CEDULA'])->whereNull('tercera_edad')->skip(0)->take(400)->get();
        if (date('H') < 7) {

            $datos = DonaDonacionesModel::where(['estado' => 'ACT', 'estado_donacion' => 'ENTREGADO', 'tipo_documento' => 'CEDULA'])
                // ->where('fecha_entrega', '>', '2020-04-09 00:00:00')
                ->whereNull('edad')->skip(0)->take(400)->get();
            foreach ($datos as $key => $value) {
                # code...
                $url = "http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/" . $value->cedula_beneficiario . "/1015";
                try {
                    if ($value->tipo_documento == 'CEDULA') {
                        $client = new GuzzleHttpClient();
                        $respuesta = $client->request(
                            'GET',
                            $url,
                            ['timeout' => 2, 'http_errors' => false]
                        );
                        $consultawebserviceobs = json_decode($respuesta->getBody()->getContents(), true);
                        if (isset($consultawebserviceobs[0]['nombre'])) {
                            // $fechaNacimiento = date('Y-m-d', strtotime($consultawebserviceobs[0]['fechaNacimiento']));
                            $fecha = explode('/', $consultawebserviceobs[0]['fechaNacimiento']);
                            $f = '';
                            if (isset($fecha[0]) && isset($fecha[1]) && isset($fecha[2])) {
                                $f = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                            }
                            $edad = fechas(1100, $f);
                            // dd($consultawebserviceobs);
                            $tercera = '';
                            $dis = '';
                            if ($edad >= 65) {
                                $tercera = 'SI';
                            } else {
                                $tercera = 'NO';
                            }
                            if (strpos($consultawebserviceobs[0]['condicionCiudadano'], 'DISCA') !== false) {
                                $dis = 'SI';
                            } else {
                                $dis = 'NO';
                            }
                            DonaDonacionesModel::where('id', $value->id)->update(
                                [
                                    'tercera_edad' => $tercera,
                                    'discapacitado' => $dis,
                                    'fecha_nacimiento' => $f,
                                    'edad' => $edad
                                ]
                            );
                            // Log::info('-D');
                        } else {
                            if ($value->tercera_edad == null) {
                                DonaDonacionesModel::where('id', $value->id)->update(
                                    [
                                        'tercera_edad' => '-',
                                        'discapacitado' => '-',
                                        'edad' => 0
                                    ]
                                );
                            } else {
                                DonaDonacionesModel::where('id', $value->id)->update(
                                    [
                                        'edad' => 0
                                    ]
                                );
                            }
                        }
                    }
                } catch (\Exception $e) {
                    Log::info($e->getMessage() . ' ' . $e->getLine());
                    if ($value->tercera_edad == null) {
                        DonaDonacionesModel::where('id', $value->id)->update(
                            [
                                'tercera_edad' => '-',
                                'discapacitado' => '-',
                                'edad' => 0
                            ]
                        );
                    } else {
                        DonaDonacionesModel::where('id', $value->id)->update(
                            [
                                'edad' => 0
                            ]
                        );
                    }
                }
            }
        }
    }
}
