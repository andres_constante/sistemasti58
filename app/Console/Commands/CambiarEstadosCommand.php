<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Coordinacioncronograma\Entities\CabRecomendacionContraHistoModel;
use Modules\Coordinacioncronograma\Entities\CabRecomendacionContraModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaDetaModel;
use Modules\Coordinacioncronograma\Entities\MovimientoCronogramaPersonasModel;
//Compromisos
use Modules\Coordinacioncronograma\Entities\CabCompromisoalcaldiaModel;
use Modules\Coordinacioncronograma\Entities\CabCompromisoalcaldiaDetaModel;
use Modules\Coordinacioncronograma\Entities\CompromisosResponsablesModel;
//Obras
use Modules\CoordinacionCronograma\Entities\CoorObrasDetaModel;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use App\UsuariosModel;
use App\Http\Controllers\NotificacionesController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use Modules\CoordinacionCronograma\Entities\PlanCampaniaDetaModel;
use Modules\CoordinacionCronograma\Entities\PlanCampaniaModel;
use Modules\CoordinacionCronograma\Entities\CoorPoaActividadCabModel;
use Modules\CoordinacionCronograma\Entities\CoorPoaActividadDetaCabModel;
use DB;

class CambiarEstadosCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'estadoactividad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que permite cambiar los estados de las actividades';
    protected $notificacion;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NotificacionesController $notifica)
    {
        parent::__construct();
        $this->notificacion = $notifica;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        //ACTIVIDADES
        $query = CoorCronogramaCabModel::where(DB::raw("IFNULL(avance,0)"), "<", 100)
            ->where("estado", "ACT")
            ->where(function ($query) {
                $query->where("estado_actividad", "EJECUCION")
                    ->orWhere("estado_actividad", "POR_EJECUTAR");
            })
            ->where("fecha_fin", "<", date("Y-m-d"));
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        $actistr = "";
        foreach ($query as $key => $value) {
            # code...
            $actividades[] = $value->id;
            $guardar = CoorCronogramaCabModel::find($value->id);
            $guardar->estado_actividad = "EJECUCION_VENCIDO";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CoorCronogramaDetaModel;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->actividad = $guardar->actividad;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->estado = "INA";
            $timeline->id_direccion = $guardar->id_direccion;
            $timeline->avance = $guardar->avance;
            $timeline->id_crono_cab = $guardar->id;
            $personas = MovimientoCronogramaPersonasModel::where("id_crono_cab", $guardar->id)->get();
            $json = [];
            foreach ($personas as $key => $valueus) {
                # code...
                $json[] = $valueus->id_usuario;
            }
            $timeline->personas_json = json_encode($json);
            $timeline->prioridad = $guardar->prioridad;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION_VENCIDO un total de $total Actividades.";
        Log::info($msg . ". ACTIVIDADESTADO");
        $msg = "Proceso Automático: $total Actividad(es) cambiaron a estado \"EJECUCION_VENCIDO\"";
        $ruta = json_encode($actividades);
        //$ruta=Crypt::encrypt($ruta);
        if ($total > 0) {
            $emails = explode("|", ConfigSystem("correoscoordinacion"));
            for ($i = 0; $i < count($emails); $i++) {
                $user = UsuariosModel::where("email", $emails[$i])->first();
                if ($user) {
                    $token = Crypt::encrypt(str_random(10));
                    $this->notificacion->notificacionesweb($msg, "coordinacioncronograma/actividadesvencidasindex?acver=$token&actividades=$ruta", $user->id, "1B5E20");
                }
            }
        }



        //ACTIVIDADES
        $query = CoorCronogramaCabModel::where(DB::raw("IFNULL(avance,0)"), "<", 100)
            ->where("estado", "ACT")
            ->where(function ($query) {
                $query->where("estado_actividad", "EJECUCION_VENCIDO");
            })
            ->where("fecha_fin", ">=", date("Y-m-d"));
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        $actistr = "";
        foreach ($query as $key => $value) {
            # code...
            $actividades[] = $value->id;
            $guardar = CoorCronogramaCabModel::find($value->id);
            $guardar->estado_actividad = "EJECUCION";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CoorCronogramaDetaModel;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->actividad = $guardar->actividad;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->estado = "INA";
            $timeline->id_direccion = $guardar->id_direccion;
            $timeline->avance = $guardar->avance;
            $timeline->id_crono_cab = $guardar->id;
            $personas = MovimientoCronogramaPersonasModel::where("id_crono_cab", $guardar->id)->get();
            $json = [];
            foreach ($personas as $key => $valueus) {
                # code...
                $json[] = $valueus->id_usuario;
            }
            $timeline->personas_json = json_encode($json);
            $timeline->prioridad = $guardar->prioridad;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION un total de $total Actividades.";
        Log::info($msg . ". ACTIVIDADESTADO");

        /*===================================================================================================================*/
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        //ACTIVIDADES
        $query = CoorCronogramaCabModel::where("avance", "=", 100)
            ->where("estado", "ACT")
            ->where(function ($query) {
                $query->where("estado_actividad", "EJECUCION")
                    ->orWhere("estado_actividad", "EJECUCION_VENCIDO");
            });
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        $actistr = "";
        foreach ($query as $key => $value) {
            # code...
            $actividades[] = $value->id;
            $guardar = CoorCronogramaCabModel::find($value->id);
            $guardar->estado_actividad = "EJECUTADO";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CoorCronogramaDetaModel;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->actividad = $guardar->actividad;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->estado = "INA";
            $timeline->id_direccion = $guardar->id_direccion;
            $timeline->avance = $guardar->avance;
            $timeline->id_crono_cab = $guardar->id;
            $personas = MovimientoCronogramaPersonasModel::where("id_crono_cab", $guardar->id)->get();
            $json = [];
            foreach ($personas as $key => $valueus) {
                # code...
                $json[] = $valueus->id_usuario;
            }
            $timeline->personas_json = json_encode($json);
            $timeline->prioridad = $guardar->prioridad;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION_VENCIDO un total de $total Actividades.";
        Log::info($msg . ". ACTIVIDADESTADO");
        

        /*===================================================================================================================*/
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        //Compromisos Alcaldía
        $query = CabCompromisoalcaldiaModel::where(DB::raw("IFNULL(avance,0)"), "<", 100)
            ->where("estado", "ACT")
            ->where(function ($query) {
                $query->where("estado_actividad", "EJECUCION")
                    ->orWhere("estado_actividad", "POR_EJECUTAR");
            })
            ->where("fecha_fin", "<", date("Y-m-d"));
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CabCompromisoalcaldiaModel::find($value->id);
            $guardar->estado_actividad = "EJECUCION_VENCIDO";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CabCompromisoalcaldiaDetaModel;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->actividad = $guardar->actividad;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->estado = $guardar->estado;
            $timeline->id_direccion = $guardar->id_direccion;
            $timeline->avance = $guardar->avance;
            $timeline->prioridad = $guardar->prioridad;
            $timeline->id_parroquia = $guardar->id_parroquia;
            $timeline->ubicacion = $guardar->ubicacion;
            //CompromisosResponsablesModel
            $personas = CompromisosResponsablesModel::where("id_crono_cab", $guardar->id)->get();
            $json = [];
            foreach ($personas as $key => $valueus) {
                # code...
                $json[] = $valueus->id_usuario;
            }
            $timeline->persona_json = json_encode($json);
            //
            $timeline->id_crono_cab = $guardar->id;
            $timeline->id_objetivo_componente = $guardar->id_objetivo_componente;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION_VENCIDO un total de $total Compromisos Territoriales.";
        Log::info($msg . ". COMPROMISOSESTADO");


        //Compromisos Alcaldía
        $query = CabCompromisoalcaldiaModel::where(DB::raw("IFNULL(avance,0)"), "=", 100)
            ->where("estado", "ACT")
            ->where(function ($query_) {
                $query_->where("estado_actividad", "=", "EJECUCION");
                $query_->orWhere("estado_actividad", "=", "EJECUCION_VENCIDO");
                $query_->orWhere("estado_actividad", "=", "POR_EJECUTAR");
            });
        $total = $query->count();
        Log::info($total . ". -----------------------------------------");
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CabCompromisoalcaldiaModel::find($value->id);
            $guardar->estado_actividad = "EJECUTADO";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CabCompromisoalcaldiaDetaModel;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->actividad = $guardar->actividad;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->estado = $guardar->estado;
            $timeline->id_direccion = $guardar->id_direccion;
            $timeline->avance = $guardar->avance;
            $timeline->prioridad = $guardar->prioridad;
            $timeline->id_parroquia = $guardar->id_parroquia;
            $timeline->ubicacion = $guardar->ubicacion;
            //CompromisosResponsablesModel
            $personas = CompromisosResponsablesModel::where("id_crono_cab", $guardar->id)->get();
            $json = [];
            foreach ($personas as $key => $valueus) {
                # code...
                $json[] = $valueus->id_usuario;
            }
            $timeline->persona_json = json_encode($json);
            //
            $timeline->id_crono_cab = $guardar->id;
            $timeline->id_objetivo_componente = $guardar->id_objetivo_componente;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUTADO un total de $total Compromisos Territoriales.";
        Log::info($msg . ". COMPROMISOSESTADO");


        //Compromisos Alcaldía
        $query = CabCompromisoalcaldiaModel::where(DB::raw("IFNULL(avance,0)"), "<", 100)
            ->where("estado", "ACT")
            ->where(function ($query_) {
                $query_->Where("estado_actividad", "=", "EJECUCION_VENCIDO");
            })
            ->where("fecha_fin", ">=", date("Y-m-d"));
        $total = $query->count();
        Log::info($total . ". -----------------------------------------");
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CabCompromisoalcaldiaModel::find($value->id);
            $guardar->estado_actividad = "EJECUCION";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CabCompromisoalcaldiaDetaModel;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->actividad = $guardar->actividad;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->estado = $guardar->estado;
            $timeline->id_direccion = $guardar->id_direccion;
            $timeline->avance = $guardar->avance;
            $timeline->prioridad = $guardar->prioridad;
            $timeline->id_parroquia = $guardar->id_parroquia;
            $timeline->ubicacion = $guardar->ubicacion;
            //CompromisosResponsablesModel
            $personas = CompromisosResponsablesModel::where("id_crono_cab", $guardar->id)->get();
            $json = [];
            foreach ($personas as $key => $valueus) {
                # code...
                $json[] = $valueus->id_usuario;
            }
            $timeline->persona_json = json_encode($json);
            //
            $timeline->id_crono_cab = $guardar->id;
            $timeline->id_objetivo_componente = $guardar->id_objetivo_componente;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION un total de $total Compromisos Territoriales.";
        Log::info($msg . ". COMPROMISOSESTADO");
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        //Obras
        /*
     * use Modules\CoordinacionCronograma\Entities\CoorObrasDetaModel;
        use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
     * */
        $query = CoorObrasModel::where(DB::raw("IFNULL(avance,0)"), "<", 100)
            ->where("estado", "ACT")
            ->where(function ($query) {
                $query->where("estado_obra", "EJECUCION")
                    ->orWhere("estado_obra", "POR_EJECUTAR");
            })
            ->where("fecha_final", "<", date("Y-m-d"));
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CoorObrasModel::find($value->id);
            $guardar->estado_obra = "EJECUCION_VENCIDO";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CoorObrasDetaModel;
            $timeline->id_tipo_obra = $guardar->id_tipo_obra;
            $timeline->calle = $guardar->calle;
            $timeline->nombre_obra = $guardar->nombre_obra;
            $timeline->numero_proceso = $guardar->numero_proceso;
            $timeline->monto = $guardar->monto;
            $timeline->estado_obra = $guardar->estado_obra;
            $timeline->id_contratista = $guardar->id_contratista;
            $timeline->avance = $guardar->avance;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_final = $guardar->fecha_final;
            $timeline->estado = $guardar->estado;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->id_parroquia = $guardar->id_parroquia;
            $timeline->fiscalizador = $guardar->fiscalizador;
            $timeline->plazo = $guardar->plazo;
            $timeline->observacion = $guardar->observacion;
            $timeline->id_cab_obra = $guardar->id;
            $timeline->longitud = $guardar->longitud;
            $timeline->latitud = $guardar->latitud;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION_VENCIDO un total de $total Obras.";
        Log::info($msg . ". OBRASESTADO");


        $query = CoorObrasModel::where(DB::raw("IFNULL(avance,0)"), "=", 100)
            ->where("estado", "ACT")
            ->where(function ($query) {
                $query->where("estado_obra", "EJECUCION")
                    ->orWhere("estado_obra", "EJECUCION_VENCIDO");
            });
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CoorObrasModel::find($value->id);
            $guardar->estado_obra = "EJECUTADO";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CoorObrasDetaModel;
            $timeline->id_tipo_obra = $guardar->id_tipo_obra;
            $timeline->calle = $guardar->calle;
            $timeline->nombre_obra = $guardar->nombre_obra;
            $timeline->numero_proceso = $guardar->numero_proceso;
            $timeline->monto = $guardar->monto;
            $timeline->estado_obra = $guardar->estado_obra;
            $timeline->id_contratista = $guardar->id_contratista;
            $timeline->avance = $guardar->avance;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_final = $guardar->fecha_final;
            $timeline->estado = $guardar->estado;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->id_parroquia = $guardar->id_parroquia;
            $timeline->fiscalizador = $guardar->fiscalizador;
            $timeline->plazo = $guardar->plazo;
            $timeline->observacion = $guardar->observacion;
            $timeline->id_cab_obra = $guardar->id;
            $timeline->longitud = $guardar->longitud;
            $timeline->latitud = $guardar->latitud;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUTADO un total de $total Obras.";
        Log::info($msg . ". OBRASESTADO");
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        //Plan de Campaña
        $objetoscam = "";

        $query = PlanCampaniaModel::where(DB::raw("IFNULL(avance,0)"), "<", 100)
            ->where("estado", "ACT")
            ->where(function ($query) {
                $query->where("estado_plan", "EJECUCION")
                    ->orWhere("estado_plan", "POR_EJECUTAR")
                    ->orWhere(DB::raw("IFNULL(estado_plan,'')"), "");
            })
            ->where("fecha_fin", "<", date("Y-m-d"));
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = PlanCampaniaModel::find($value->id);
            $guardar->estado_plan = "EJECUCION_VENCIDO";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new PlanCampaniaDetaModel;
            $timeline->id_plan_cab = $guardar->id;
            $timeline->diagnostico = $guardar->diagnostico;
            $timeline->objetivo_general = $guardar->objetivo_general;
            $timeline->componente = $guardar->componente;
            $timeline->objetivo_especificos = $guardar->objetivo_especificos;
            $timeline->actividades = $guardar->actividades;
            $timeline->actividades_propuestas = $guardar->actividades_propuestas;
            $timeline->verificable = $guardar->verificable;
            $timeline->observaciones = $guardar->observaciones;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->id_direccion = $guardar->id_direccion;
            $timeline->json_anios = $guardar->json_anios;
            $timeline->suma = $guardar->suma;
            $timeline->estado = $guardar->estado;
            $timeline->created_at = $guardar->created_at;
            $timeline->updated_at = $guardar->updated_at;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->pc = $guardar->pc;
            $timeline->ip = $guardar->ip;
            $timeline->host = $guardar->host;
            $timeline->estado_plan = $guardar->estado_plan;
            $timeline->avance = $guardar->avance;

            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION_VENCIDO un total de $total Plan de Campaña.";
        Log::info($msg . ". PLANCAMPANA");
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        //Recomendaciones Contraloria
        $query = CabRecomendacionContraModel::where("estado", "ACT")
            ->where(function ($query) {
                $query->where("estado_proceso", "EJECUCION")
                    ->orWhere("estado_proceso", "POR_EJECUTAR")
                    ->orWhere(DB::raw("IFNULL(estado_proceso,'')"), "");
            })
            ->where("fecha_fin", "<", date("Y-m-d"));
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CabRecomendacionContraModel::find($value->id);
            $guardar->estado_proceso = "EJECUCION_VENCIDO";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CabRecomendacionContraHistoModel;

            $timeline->recomendacion = $guardar->recomendacion;
            $timeline->estado_proceso = $guardar->estado_proceso;
            $timeline->actividad = $guardar->actividad;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->estado = $guardar->estado;
            $timeline->created_at = $guardar->created_at;
            $timeline->updated_at = $guardar->updated_at;
            $timeline->tipo_documento = $guardar->tipo_documento;
            $timeline->numero_documento = $guardar->numero_documento;
            $timeline->periodo = $guardar->periodo;
            $timeline->fecha_entrega = $guardar->fecha_entrega;
            $timeline->fecha_aprobacion = $guardar->fecha_aprobacion;
            $timeline->id_direccion = $guardar->id_direccion;
            $timeline->nombre_examen = $guardar->nombre_examen;
            $timeline->id_cab_reco = $guardar->id;

            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION_VENCIDO un total de $total Recomendaciones Contraloría.";
        Log::info($msg . ". RECOMENDACIONCONTRALORIA");
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        //Actividades POA
        $query = CoorPoaActividadCabModel::where("estado", "ACT")
            ->where(DB::raw("IFNULL(avance,0)"), "<", 100)
            ->where(function ($query) {
                $query->where("estado_actividad", "EJECUCION")
                    ->orWhere("estado_actividad", "POR_EJECUTAR")
                    ->orWhere(DB::raw("IFNULL(estado_actividad,'')"), "");
            })
            ->where("fecha_fin", "<", date("Y-m-d H:m:s"));
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CoorPoaActividadCabModel::find($value->id);
            $guardar->estado_actividad = "EJECUCION_VENCIDO";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CoorPoaActividadDetaCabModel;
            $timeline->id_poa = $guardar->id_poa;
            $timeline->actividad = $guardar->actividad;
            $timeline->id_direccion_act = $guardar->id_direccion_act;
            $timeline->idusuario_act = $guardar->idusuario_act;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->created_at = $guardar->created_at;
            $timeline->updated_at = $guardar->updated_at;
            $timeline->estado = $guardar->estado;
            $timeline->id_poa_act_cab = $guardar->id;
            $timeline->avance = $guardar->avance;
            $timeline->monto_actividad = $guardar->monto_actividad;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION_VENCIDO un total de $total Actividades de POA.";
        Log::info($msg . ". ACTIVIDAESPOA");

        //Actividades POA
        $query = CoorPoaActividadCabModel::where("estado", "ACT")
            ->where(DB::raw("IFNULL(avance,0)"), "<", 100)
            ->where(function ($query) {
                $query->where("estado_actividad", "EJECUCION_VENCIDO");
            })
            ->where("fecha_fin", ">=", date("Y-m-d"));
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CoorPoaActividadCabModel::find($value->id);
            $guardar->estado_actividad = "EJECUCION";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CoorPoaActividadDetaCabModel;
            $timeline->id_poa = $guardar->id_poa;
            $timeline->actividad = $guardar->actividad;
            $timeline->id_direccion_act = $guardar->id_direccion_act;
            $timeline->idusuario_act = $guardar->idusuario_act;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->created_at = $guardar->created_at;
            $timeline->updated_at = $guardar->updated_at;
            $timeline->estado = $guardar->estado;
            $timeline->id_poa_act_cab = $guardar->id;
            $timeline->avance = $guardar->avance;
            $timeline->monto_actividad = $guardar->monto_actividad;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION un total de $total Actividades de POA.";
        Log::info($msg . ". ACTIVIDAESPOA");


        //Actividades POA con avance de 100

        $query = CoorPoaActividadCabModel::where("estado", "ACT")
            ->where(DB::raw("IFNULL(avance,0)"), "=", 100)
            ->where(function ($query) {
                $query->where("estado_actividad", "EJECUCION")
                    ->orWhere("estado_actividad", "POR_EJECUTAR");
            });
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CoorPoaActividadCabModel::find($value->id);

            if ($guardar->updated_at <= $guardar->fecha_fin) {
                $guardar->estado_actividad = "EJECUTADO";
            } else {
                $guardar->estado_actividad = "EJECUCION_VENCIDO";
            }

            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CoorPoaActividadDetaCabModel;
            $timeline->id_poa = $guardar->id_poa;
            $timeline->actividad = $guardar->actividad;
            $timeline->id_direccion_act = $guardar->id_direccion_act;
            $timeline->idusuario_act = $guardar->idusuario_act;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->created_at = $guardar->created_at;
            $timeline->updated_at = $guardar->updated_at;
            $timeline->estado = $guardar->estado;
            $timeline->id_poa_act_cab = $guardar->id;
            $timeline->avance = $guardar->avance;
            $timeline->monto_actividad = $guardar->monto_actividad;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUdatos  un total de $total Actividades de POA.";
        Log::info($msg . ". ACTIVIDAESPOA");
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        /*===================================================================================================================*/
        //Actividades POA

        $query = CoorPoaActividadCabModel::where("estado", "ACT")
            ->where(DB::raw("IFNULL(avance,0)"), "=", 100)
            ->where("estado_actividad", "EJECUCION_VENCIDO");


        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CoorPoaActividadCabModel::find($value->id);
            $guardar->estado_actividad = "EJECUTADO";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CoorPoaActividadDetaCabModel;
            $timeline->id_poa = $guardar->id_poa;
            $timeline->actividad = $guardar->actividad;
            $timeline->id_direccion_act = $guardar->id_direccion_act;
            $timeline->idusuario_act = $guardar->idusuario_act;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->created_at = $guardar->created_at;
            $timeline->updated_at = $guardar->updated_at;
            $timeline->estado = $guardar->estado;
            $timeline->id_poa_act_cab = $guardar->id;
            $timeline->avance = $guardar->avance;
            $timeline->monto_actividad = $guardar->monto_actividad;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUTADO un total de $total Actividades de POA.";
        Log::info($msg . ". ACTIVIDAESPOA");



        //Actividades POA a ejutarce
        $query = CoorPoaActividadCabModel::where("estado", "ACT")
            ->where(DB::raw("IFNULL(avance,0)"), "<", 100)
            ->where(DB::raw("IFNULL(estado_actividad,'')"), "")
            ->where("fecha_inicio", "<=", date("Y-m-d H:m:s"));
        $total = $query->count();
        $query = $query->get();
        $actividades = array();
        foreach ($query as $key => $value) {
            $actividades[] = $value->id;
            $guardar = CoorPoaActividadCabModel::find($value->id);
            $guardar->estado_actividad = "EJECUCION";
            $guardar->id_usuario = 1; //Administrador
            $guardar->save();
            //Historial
            $timeline = new CoorPoaActividadDetaCabModel;
            $timeline->id_poa = $guardar->id_poa;
            $timeline->actividad = $guardar->actividad;
            $timeline->id_direccion_act = $guardar->id_direccion_act;
            $timeline->idusuario_act = $guardar->idusuario_act;
            $timeline->fecha_inicio = $guardar->fecha_inicio;
            $timeline->fecha_fin = $guardar->fecha_fin;
            $timeline->observacion = $guardar->observacion;
            $timeline->estado_actividad = $guardar->estado_actividad;
            $timeline->id_usuario = $guardar->id_usuario;
            $timeline->ip = $guardar->ip;
            $timeline->pc = $guardar->pc;
            $timeline->created_at = $guardar->created_at;
            $timeline->updated_at = $guardar->updated_at;
            $timeline->estado = $guardar->estado;
            $timeline->id_poa_act_cab = $guardar->id;
            $timeline->avance = $guardar->avance;
            $timeline->monto_actividad = $guardar->monto_actividad;
            $timeline->save();
        }
        $msg = "Se cambió de estado a EJECUCION un total de $total Actividades de POA.";
        Log::info($msg . ". ACTIVIDAESPOA");
    }
}
