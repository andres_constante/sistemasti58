<?php

namespace App\Console\Commands;

use App\Http\Controllers\NotificacionesController;
use App\Http\Controllers\WebApiAgendaController;
use App\UsuariosModel;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Modules\ComunicacionAlcaldia\Entities\AgendaNotificacionesPersonalizadas;
use Modules\ComunicacionAlcaldia\Entities\AgendaVirtualModel;

class AgendaVirtualCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alertaragenda';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permite alertar al Sr. Alcalde sobre una actividad';
    protected $notificacion;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NotificacionesController $notifica)
    {
        parent::__construct();
        $this->notificacion = $notifica;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $query = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
            ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
            ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
            ->select(
                'com_tmov_comunicacion_agenda.nombre as title',
                'descripcion as desc',
                'fecha_fin as endTime',
                'fecha_inicio as startTime',
                DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",adjunto) as adjunto_link'),
                DB::raw("null as allDay"),
                "com_tmov_comunicacion_agenda.*",
                'tp.tipo as tipo_evento',
                'ct.id as categoria_id',
                'es.nombre as estado_aprobacion'
            )->where(["com_tmov_comunicacion_agenda.estado" => 'ACT', 'silenciar' => '0', 'confirmar_asistencia' => '1'])
            ->whereBetween("fecha_inicio", [date("Y-m-d") . ' 00:00:00', date("Y-m-d") . ' 23:59:59'])
            ->whereIn('id_estado', [3, 4, 8])
            ->get();


        $notificacion = new NotificacionesController;

        $web_api = new WebApiAgendaController;
        $cedula_alcalde =  ConfigSystem('cedula_alcalde');
        $telefonos_alacalde = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->where('cedula', $cedula_alcalde)->get();

        $players = array();
        foreach ($telefonos_alacalde as $key => $value) {
            array_push($players, $value->id_player);
        }

        $df = array();
        foreach ($query as $key => $value) {
            //convertimos la fecha 1 a objeto Carbon
            $carbon1 = new \Carbon\Carbon();
            //convertimos la fecha 2 a objeto Carbon
            $carbon2 = new \Carbon\Carbon($value->fecha_inicio);
            //de esta manera sacamos la diferencia en minutos
            //dd($carbon2);
            $minutesDiff = $carbon1->diffInMinutes($carbon2);

            // dd($minutesDiff);
            if (($minutesDiff >= 56 && $minutesDiff <= 60) || $minutesDiff <= -11) {
                // if ($value->masivo == 1 && $value->id_tipo == 1) {
                //     $web_api->notificarTodos('🔔  Asistir a evento', $value);
                // }
                $notificacionPersonalizada = AgendaNotificacionesPersonalizadas::where('id_agenda', $value->id)->get();
                foreach ($notificacionPersonalizada as $k => $v) {
                    $usuario = DB::table('tma_playerid')->where('users_id', $v->id_usuario)->get();
                    foreach ($usuario as $uc => $u) {
                        array_push($players, $u->id_player);
                    }
                }
                if ($value->coordinadores == 1) {
                    $usuario = DB::table('tma_playerid')->join('users as u', 'u.id', '=', 'tma_playerid.users_id')->where('id_perfil', 24)->get();
                    foreach ($usuario as $uc => $u) {
                        array_push($players, $u->id_player);
                    }
                }
                if ($value->concejales == 1) {

                    $usuario = DB::table('tma_playerid')->join('users as u', 'u.id', '=', 'tma_playerid.users_id')->where('id_perfil', 24)->get();
                    foreach ($usuario as $uc => $u) {
                        array_push($players, $u->id_player);
                    }
                }
                if ($value->misconcejales == 1) {

                    $misconcejales = explode("|", ConfigSystem("misconcejales"));

                    $usuario = DB::table('tma_playerid')->join('users as u', 'u.id', '=', 'tma_playerid.users_id')->whereIn('u.id', $misconcejales)->get();
                    foreach ($usuario as $uc => $u) {
                        array_push($players, $u->id_player);
                    }
                }
                if ($value->directores == 1) {
                    $usuario = DB::table('tma_playerid')->join('users as u', 'u.id', '=', 'tma_playerid.users_id')->whereIn('id_perfil', [3, 15, 5, 29, 22, 9])->get();
                    foreach ($usuario as $uc => $u) {
                        array_push($players, $u->id_player);
                    }
                }

                if ($value->delegacion_alcalde != null || $value->delegacion_alcalde != '') {

                    $usuario = DB::table('tma_playerid')->join('users as u', 'u.id', '=', 'tma_playerid.users_id')->whereIn('u.id', [$value->delegacion_alcalde])->get();
                    foreach ($usuario as $uc => $u) {
                        array_push($players, $u->id_player);
                    }
                }
                // $guardarres = AgendaNotificacionesPersonalizadas::where("id_agenda", $value->id)->get();
                // foreach ($guardarres as $key => $v) {
                //     # code...
                //     $usuario = DB::table('tma_playerid')->join('users as u', 'u.id', '=', 'tma_playerid.users_id')->where('u.id', $v->id_usuario)->get();
                //     foreach ($usuario as $u => $u) {
                //         array_push($players, $u->id_player);
                //     }
                // }



                $notificacion->notificacion($players, $value->id, '🔔  Asistir a evento', $value->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($value->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($value->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($value->fecha_fin)), 2, $value);
                $notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $value->id, '🔔  Asistir a evento',  $value->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($value->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($value->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($value->fecha_fin)), 2, $value);
            }
        }








        $query_ = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
            ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
            ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
            ->select(
                'com_tmov_comunicacion_agenda.nombre as title',
                'descripcion as desc',
                'fecha_fin as endTime',
                'fecha_inicio as startTime',
                DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",adjunto) as adjunto_link'),
                DB::raw("null as allDay"),
                "com_tmov_comunicacion_agenda.*",
                'tp.tipo as tipo_evento',
                'ct.id as categoria_id',
                'es.nombre as estado_aprobacion'
            )->where(["com_tmov_comunicacion_agenda.estado" => 'ACT', 'silenciar' => '0'])
            ->where("fecha_fin", "<", date("Y-m-d H:i:s"))
            ->where('id_estado', '<>', 5)
            ->where('id_estado', '<>', 12)
            ->where('id_estado', '<>', 6)
            ->get();


        foreach ($query_ as $key => $value) {

            $actividad = AgendaVirtualModel::find($value->id);
            if ($value->id_estado != 3 && $value->id_estado != 8 && $value->id_estado != 10) {
                $actividad->id_estado = 12;
            } else {
                $actividad->id_estado = 5;
            }
            $actividad->save();
            insertarhistorial($actividad, $actividad->id, 1, 1);
        }
        // $query_recordar = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
        //     ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
        //     ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
        //     ->select(
        //         'com_tmov_comunicacion_agenda.nombre as title',
        //         'descripcion as desc',
        //         'fecha_fin as endTime',
        //         'fecha_inicio as startTime',
        //         DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",adjunto) as adjunto_link'),
        //         DB::raw("null as allDay"),
        //         "com_tmov_comunicacion_agenda.*",
        //         'tp.tipo as tipo_evento',
        //         'ct.id as categoria_id',
        //         'es.nombre as estado_aprobacion'
        //     )->where(['silenciar' => '0'])
        //     ->where(["com_tmov_comunicacion_agenda.estado" => 'ACT', "recordar" => "SI"])->get();
        // foreach ($query_recordar as $key => $value) {
        //     $notificacion->notificacion($players, $value->id, '🔔 Recordartorio para atender actividad ', $value->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($value->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($value->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($value->fecha_fin)) , 2, $value);
        //     $notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $value->id, '🔔   Recordartorio para atender actividad',  $value->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($value->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($value->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($value->fecha_fin)), 2, $value);
        // }
        // $query_recordar = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
        //     ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
        //     ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
        //     ->select(
        //         'com_tmov_comunicacion_agenda.nombre as title',
        //         'descripcion as desc',
        //         'fecha_fin as endTime',
        //         'fecha_inicio as startTime',
        //         DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",adjunto) as adjunto_link'),
        //         DB::raw("null as allDay"),
        //         "com_tmov_comunicacion_agenda.*",
        //         'tp.tipo as tipo_evento',
        //         'ct.id as categoria_id',
        //         'es.nombre as estado_aprobacion'
        //     )->where(["com_tmov_comunicacion_agenda.estado" => 'ACT', 'silenciar' => '0'])
        //     ->whereIn('id_estado', [1, 2])
        //     ->get();
        // // dd($query_);
        // foreach ($query_ as $key => $value) {
        //     # code...
        //     // $notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6', '4b38db86-66ed-4057-ba4d-d593cbe3cae4'], $value->id, '🔔   Recordartorio para atender actividad',  $value->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($value->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($value->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($value->fecha_fin)), 2, $value);
        // }
    }
}
