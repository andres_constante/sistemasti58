<?php

namespace App\Console\Commands;

use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Modules\Coordinacioncronograma\Entities\CabCompromisoalcaldiaModel;
use Modules\Coordinacioncronograma\Entities\CabRecomendacionContraModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\CoordinacionCronograma\Entities\CoorPoaActividadCabModel;
use Modules\Coordinacioncronograma\Entities\CoorPoaCabModel;
use Modules\CoordinacionCronograma\Entities\PlanCampaniaModel;

class alertasActividades extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alertaractividades';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alertar actividades';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $notificacion = new NotificacionesController;
        try {
            //code...
            $url = URL::to('');
            $query = CoorCronogramaCabModel::where("avance", "<", 100)
                ->where("estado", "ACT")
                ->where(function ($query) {
                    $query->where("estado_actividad", "EJECUCION")
                        ->orWhere("estado_actividad", "EJECUCION_VENCIDO");
                })
                ->where("fecha_fin", "<", date("Y-m-d"));

            $query = $query->get();
            foreach ($query as $key => $value) {
                # code...
                $usuarios = UsuariosModel::select('users.id', 'users.email')->join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')->where(['p.tipo' => 3, 'users.id_direccion' => $value->id_direccion])->where('id_perfil', '<>', 13)->get();
                $this->enviar($value->fecha_inicio, $value->fecha_fin, "Disposicion general #" . $value->id, $usuarios, $url . '/coordinacioncronograma/cronogramacompromisos');
            }

            $query = CabCompromisoalcaldiaModel::where("avance", "<", 100)
                ->where("estado", "ACT")
                ->where(function ($query) {
                    $query->where("estado_actividad", "EJECUCION")
                        ->orWhere("estado_actividad", "EJECUCION_VENCIDO");
                })
                ->where("fecha_fin", "<", date("Y-m-d"));

            $query = $query->get();
            foreach ($query as $key => $value) {
                # code...
                $usuarios = UsuariosModel::select('users.id', 'users.email')->join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')->where(['p.tipo' => 3, 'users.id_direccion' => $value->id_direccion])->where('id_perfil', '<>', 13)->get();
                $this->enviar($value->fecha_inicio, $value->fecha_fin, "Compromiso Territorial #" . $value->id, $usuarios, $url . '/coordinacioncronograma/compromisosalcaldiablue');
            }
            $query = CoorObrasModel::where("avance", "<", 100)
                ->where("estado", "ACT")
                ->where(function ($query) {
                    $query->where("estado_obra", "EJECUCION")
                        ->orWhere("estado_obra", "EJECUCION_VENCIDO");
                })
                ->where("fecha_final", "<", date("Y-m-d"));

            $query = $query->get();
            foreach ($query as $key => $value) {
                # code...
                $usuarios = UsuariosModel::select('users.id', 'users.email')->join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')->where(['p.tipo' => 3, 'users.id_direccion' => $value->id_direccion])->where('id_perfil', '<>', 13)->get();
                $this->enviar($value->fecha_inicio, $value->fecha_final, "Obra #" . $value->id, $usuarios, $url . '/coordinacioncronograma/listobras');
            }

            $query = PlanCampaniaModel::where(DB::raw("IFNULL(avance,0)"), "<", 100)
                ->where("estado", "ACT")
                ->where(function ($query) {
                    $query->where("estado_plan", "EJECUCION")
                        ->orWhere("estado_plan", "EJECUCION_VENCIDO")
                        ->orWhere(DB::raw("IFNULL(estado_plan,'')"), "");
                })
                ->where("fecha_fin", "<", date("Y-m-d"));

            $query = $query->get();
            foreach ($query as $key => $value) {
                # code...
                $usuarios = UsuariosModel::select('users.id', 'users.email')->join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')->where(['p.tipo' => 3, 'users.id_direccion' => $value->id_direccion])->where('id_perfil', '<>', 13)->get();
                $this->enviar($value->fecha_inicio, $value->fecha_final, "Plan de propuestas #" . $value->id, $usuarios, $url . '/coordinacioncronograma/plancampania');
            }



            $query = CabRecomendacionContraModel::where("estado", "ACT")
                ->where(function ($query) {
                    $query->where("estado_proceso", "EJECUCION")
                        ->orWhere("estado_proceso", "EJECUCION_VENCIDO")
                        ->orWhere(DB::raw("IFNULL(estado_proceso,'')"), "");
                })
                ->where("fecha_fin", "<", date("Y-m-d"));

            $query = $query->get();
            foreach ($query as $key => $value) {
                # code...
                $usuarios = UsuariosModel::select('users.id', 'users.email')->join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')->where(['p.tipo' => 3, 'users.id_direccion' => $value->id_direccion])->where('id_perfil', '<>', 13)->get();
                $this->enviar($value->fecha_inicio, $value->fecha_final, "Recomendaciones Contraloría #" . $value->id, $usuarios, $url . '/coordinacioncronograma/recomendaciones');
            }


            $query = CoorPoaActividadCabModel::where("estado", "ACT")
                ->where(DB::raw("IFNULL(avance,0)"), "<", 100)
                ->where(function ($query) {
                    $query->where("estado_actividad", "EJECUCION")
                        ->orWhere("estado_actividad", "EJECUCION_VENCIDO");
                });
            $query = $query->get();
            foreach ($query as $key => $value) {
                # code...
                $poa = CoorPoaCabModel::where('id', $value->id_poa)->first();
                $usuarios = UsuariosModel::select('users.id', 'users.email')->join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')->where(['p.tipo' => 3, 'users.id_direccion' => $value->id_direccion])->where('id_perfil', '<>', 13)->get();
                $this->enviar($value->fecha_inicio, $value->fecha_final, "Actividad #" . $value->id . " del proyecto Poa #" . $value->id_poa, $usuarios, $url . '/coordinacioncronograma/poamain?anio=' . $poa->anio);
            }
        } catch (\Throwable $th) {

            $notificacion->EnviarEmail('cgpg94@gmail.com', 'SITEMA INTERNO DE CONTROL', '', 'Error :'.$th->getMessage().' en la linea '.$th->getLine(), $url);
        }
    }


    public function enviar($fecha_inicio, $fecha_fin, $mensaje, $usuarios, $url)
    {
        //convertimos la fecha 1 a objeto Carbon
        $carbon1 = new \Carbon\Carbon($fecha_inicio);
        //convertimos la fecha 2 a objeto Carbon
        $carbon2 = new \Carbon\Carbon($fecha_fin);
        //de esta manera sacamos la diferencia en minutos
        $minutesDiff = $carbon1->diffInDays($carbon2);
        $bandera = false;
        $hora = date('H');

        $notificacion = new NotificacionesController;

        if ($minutesDiff <= 3) {
            if ($hora == 9) {
                $bandera == true;
                $mensaje .= " vence en " . $minutesDiff . " dia(s), por favor atender.";
            }
        }
        if ($minutesDiff == -1) {
            if ($hora == 9) {

                $bandera == true;
                $mensaje .= " venció hace " . $minutesDiff . " dia(s), por favor atender.";
            }
        }

        if ($minutesDiff == 2) {
            if ($hora == 9 || $hora == 15) {

                $bandera == true;
                $mensaje .= " venció hace " . $minutesDiff . " dia(s), por favor atender.";
            }
        }
        if ($minutesDiff == 3) {
            if ($hora == 9 || $hora == 14 || $hora == 16) {

                $bandera == true;
                $mensaje .= " venció hace " . $minutesDiff . " dia(s), por favor atender.";
            }
        }
        if ($bandera) {
            foreach ($usuarios as $key => $v) {
                $notificacion->notificacionesweb($mensaje, $url, $v->id, "ed5565");
                $notificacion->EnviarEmail($v->email, 'SITEMA INTERNO DE CONTROL', 'Notificación de actividades', $mensaje, $url,'vistas.emails.email');
            }
        }
    }
}
