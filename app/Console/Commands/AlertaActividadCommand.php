<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use App\UsuariosModel;
use App\Http\Controllers\NotificacionesController;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
class AlertaActividadCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alertaactividad';//'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permite alertar al Funcionario las actividades Vencidas';
    protected $notificacion;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NotificacionesController $notifica)
    {
        parent::__construct();
        $this->notificacion=$notifica;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //        
        $query=CoorCronogramaCabModel::where("avance","<",100)
        ->where("estado","ACT")
        ->where("estado_actividad","EJECUCION")        
        ->where("fecha_fin","<",date("Y-m-d"));
        
        $vencidas=$query->get();
        $actividades=array();
        foreach ($vencidas as $key => $value) {
            # code...
            $actividades[]=$value->id;
        }
        $todalvencidas=$query->count();

        $emails= explode("|",ConfigSystem("correoscoordinacion"));
        if($todalvencidas>0)
        {
            $ruta= json_encode($actividades);
            //$ruta=Crypt::encrypt($ruta);
            for($i=0; $i<count($emails); $i++ ){
                $user=UsuariosModel::where("email",$emails[$i])->first();
                if($user){
                    $token=Crypt::encrypt(str_random(10));
                    $this->notificacion->notificacionesweb("Estimado(a) ".$user->name.", existen ".$todalvencidas." actividad(es) vencida(s), haga clic para visualizar.","coordinacioncronograma/actividadesvencidasindex?acver=$token&actividades=$ruta",$user->id,"ed5565");        
                }
            }
            Log::info("Actividades vencidas $todalvencidas. ACTIVIDADVENCIDA");
            //Actualizar estado de Vencidas
        }
    }
}
