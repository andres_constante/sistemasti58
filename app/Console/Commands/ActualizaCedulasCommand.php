<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\Log;

class ActualizaCedulasCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'actualizarCedulas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $cedulas = DB::table('cedulas')->where('estado_ws', 1)->skip(0)->take(400)->get();
        foreach ($cedulas as $key => $value) {
            # code...
            if (strlen($value->cedula) > 10) {
                $value->cedula = str_replace("0001", "", $value->cedula);
            }
            if (strlen($value->cedula) == 9) {
                $value->cedula = '0'.$value->cedula;
            }
            $url = "http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/" . $value->cedula . "/1015";
            try {
                $client = new GuzzleHttpClient();
                $respuesta = $client->request(
                    'GET',
                    $url,
                    ['timeout' => 2, 'http_errors' => false]
                );
                $consultawebserviceobs = json_decode($respuesta->getBody()->getContents(), true);
                if (isset($consultawebserviceobs[0]['nombre'])) {
                    // $fechaNacimiento = date('Y-m-d', strtotime($consultawebserviceobs[0]['fechaNacimiento']));
                    $fecha = explode('/', $consultawebserviceobs[0]['fechaNacimiento']);
                    $f = '';
                    if (isset($fecha[0]) && isset($fecha[1]) && isset($fecha[2])) {
                        $f = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                    }
                    $fecha_defuncion = '';
                    $estado_persona = '';
                    if ($consultawebserviceobs[0]['fechaDefuncion'] != null) {
                        $fecha_defuncion = $consultawebserviceobs[0]['fechaDefuncion'];
                        $fecha_defuncion = explode('/', $fecha_defuncion);
                        $f_d = '';
                        if (isset($fecha_defuncion[0]) && isset($fecha_defuncion[1]) && isset($fecha_defuncion[2])) {
                            $f_d = $fecha_defuncion[2] . '-' . $fecha_defuncion[1] . '-' . $fecha_defuncion[0];
                        }
                        $fecha_defuncion=$f_d;
                        $date = Carbon::parse($f_d);
                        $now = Carbon::parse($value->fecha);
                        $edad_fecha = $date->diffInDays($now);
                        if ($date <= $now) {
                            // dd($fecha_defuncion);
                            $estado_persona = 'MUERTO A LA FECHA';
                        } else {
                            $estado_persona = 'MUERTO DESPUES DE LA FECHA';
                        }
                    } else {
                        $estado_persona = 'VIVO';
                    }
                    // dd($estado_persona);


                    $date = Carbon::parse($f);
                    $now = Carbon::parse($value->fecha);
                    $edad_fecha = $date->diffInYears($now);


                    $edad = fechas(1100, $f);
                    $tercera = '';
                    $dis = '';
                    if ($edad >= 65) {
                        $tercera = 'SI';
                    } else {
                        $tercera = 'NO';
                    }
                    if ($edad_fecha >= 65) {
                        $tercera_fecha = 'SI';
                    } else {
                        $tercera_fecha = 'NO';
                    }
                    $detalle = '';
                    if (strpos($consultawebserviceobs[0]['condicionCiudadano'], 'DISCA') !== false) {
                        $dis = 'SI';
                        $detalle = $consultawebserviceobs[0]['condicionCiudadano'];
                    } else {
                        $dis = 'NO';
                    }
                    // dd($fecha_defuncion);
                    $dato = DB::table('cedulas')->where('id', $value->id)->update(
                        [
                            "fecha_nacimiento" => $f,
                            "edad" => $edad,
                            "tercera" => $tercera,
                            "edad_fecha" => $edad_fecha,
                            "tercera_fecha" => $tercera_fecha,
                            "discapacidad" => $dis,
                            "estado_ws" => 2,
                            "detalle_discapacidad" => $detalle,
                            "fecha_defuncion" => $fecha_defuncion,
                            "estado_persona" => $estado_persona
                        ]
                    );
                    // dd($dato);
                } else {
                    $dato = DB::table('cedulas')->where('id', $value->id)->update(
                        [
                            "estado_ws" => 3
                        ]
                    );
                }
            } catch (\Exception $e) {
                dd($e);
                Log::info('No se pudo'. $e->getMessage().' '.$e->getLine().' '.$value->id);
            }
        }
    }
}
