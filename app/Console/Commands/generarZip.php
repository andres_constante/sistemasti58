<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use ZipArchive;

class generarZip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generarDonacionesZip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $carbon1 = new \Carbon\Carbon('2019-11-18');
        // $carbon1 = new \Carbon\Carbon('2020-06-20');
        $carbon2 = new \Carbon\Carbon();
        $minutesDiff = $carbon1->diffInDays($carbon2) - 1;

        for ($i = 0; $i < $minutesDiff; $i++) {

            $fecha = strtotime('+' . $i . ' day', strtotime($carbon1));
            $fecha = date('Y-m-d', $fecha);
            if (!is_dir(base_path() . "/donaciones_img")) {
                mkdir(base_path() . "/donaciones_img", 777);
            }
            $consulta = DB::select("SELECT * FROM dona_donciones as d where d.estado_donacion='ENTREGADO' AND DATE_FORMAT(d.fecha_entrega, \"%Y-%m-%d\") BETWEEN '" . $fecha . "'  and '" . $fecha . "' and d.estado='ACT'");
            if (isset($consulta[0])) {
                // dd($consulta);
                $nombreArchivoZip = base_path() . "/donaciones_img/Fotos_" . $fecha . ".zip";
                if (!is_file($nombreArchivoZip)) {
                    $zip = new ZipArchive();
                    if (!$zip->open($nombreArchivoZip, ZipArchive::CREATE | ZipArchive::OVERWRITE)) {
                        return "Error abriendo ZIP en $nombreArchivoZip";
                    }
                    $bandera = false;
                    foreach ($consulta as $key => $value) {
                        // dd($consulta);
                        $keys = [
                            'adjunto',
                            'adjunto_entrega',
                            'frontal',
                            'trasera',
                            'entrega',
                            'ficha'
                        ];
                        foreach ($value as $kk => $vv) {
                            if (in_array($kk, $keys)) {
                                $rutaAbsoluta = public_path() . "/archivos_sistema/" . $vv;
                                // if ($vv != null) {
                                //     dd($vv);
                                // }
                                if (is_file($rutaAbsoluta)) {
                                    $nombre = basename($rutaAbsoluta);
                                    $zip->addFile($rutaAbsoluta, $nombre);
                                    $bandera = true;
                                }
                            }
                        }
                    }
                    $rutaAbsoluta = public_path() . "/robots.txt";
                    if (is_file($rutaAbsoluta)) {
                        $nombre = basename($rutaAbsoluta);
                        $zip->addFile($rutaAbsoluta, $nombre);
                    }

                    $resultado = $zip->close();
                    if (!$resultado) {
                        return 'ERROR';
                    }
                    if ($bandera == false) {
                        if (is_file($nombreArchivoZip)) {
                            unlink($nombreArchivoZip);
                        }
                    }
                }
            }
        }
    }
}
