<?php

namespace App\Console\Commands;

use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaAsiganrModel;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabModel;

class AlertarTramitesInternos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alertartramitesinternos';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Alertar sobre trámites sin atender.';
    protected $notificacion;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NotificacionesController $notifica)
    {
        parent::__construct();
        $this->notificacion = $notifica;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query = TramAlcaldiaCabModel::where(["estado" => 'ACT',"disposicion" => 'PENDIENTE'])
        ->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])
        ->whereNotIn('tipo_tramite', [8, 9])->get();
        $contador = 0;

        foreach ($query as $key => $value)
        {
            // $direcciones = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as da')
            // ->join('tmae_direcciones as td', 'td.id', 'da.id_direccion')
            // ->select()
            // ->where(['da.id_cab' => $value->id])->get();

            //convertimos la fecha 1 a objeto Carbon
            $carbon1 = new \Carbon\Carbon();
            //convertimos la fecha 2 a objeto Carbon
            $carbon2 = new \Carbon\Carbon($value->created_at);
            //de esta manera sacamos la diferencia en minutos
            $minutesDiff = $carbon1->diffInHours($carbon2);

            if ($minutesDiff >= 24)
            {
                $contador ++;
                $tipo_tramite = intval($value->tipo_tramite);
                $perfil = $tipo_tramite == 10 || $tipo_tramite == 11 
                || $tipo_tramite == 12 || $tipo_tramite == 13 
                || $tipo_tramite == 14 ? 8 : 10;

                $usuarios = UsuariosModel::where('id_perfil', $perfil)->get();
                $url = 'tramitesalcaldia/despachartramite?id='.$value->id.'&estado=1';

                foreach ($usuarios as $key => $v)
                {
                    $msj = "Estimado(a) " . $v->name . ", el trámite ".$value->numtramite." aún no ha sido atendido, se solicita la atención del mismo";
                    // $this->notificacion->notificacionesweb("Estimado(a) " . $v->name . ", el trámite ".$value->numtramite." aún no ha sido atendido, se solicita de atención del mismo", "tramitesalcaldia/tramitesdespacho", $v->id, "ed5565");
                    $this->notificacion->notificacionesweb($msj, $url, $v->id, "ed5565");
                    $this->notificacion->EnviarEmailTramites($v->email, 'Trámite sin atender', '', $msj, $url);
                }
            }
        }

        $tra = $contador > 1 ? ' trámites sin atender.' : ' trámite sin atender.'; 
        Log::info('Se notificaron sobre ' . $contador  . $tra);

        $tramitesCoordinadores = TramAlcaldiaCabModel::from('tram_peticiones_cab as cab')
        ->join('tram_coordinador_asignado as coor', 'cab.id', 'coor.id_cab')
        ->join('users as u', 'coor.id_coordinador', 'u.id')
        ->select('cab.id', 'cab.numtramite', 'coor.created_at', 'u.id as id_coordinador', 'u.email')->get();

        foreach($tramitesCoordinadores as $value)
        {
            $hoy = new \Carbon\Carbon();
            $fechasolicitudAprobacion = new \Carbon\Carbon($value->created_at);
            // $diferencia = $hoy->diffInHours($fechasolicitudAprobacion);
            $diferencia = $this->diasLaborables($hoy, $fechasolicitudAprobacion);
            
            if($diferencia  >= 1)
            {
                $ruta = 'tramitesalcaldia/finalizartramite?id='.$value->id;
                $mensaje = "Estimado(a), el trámite ".$value->numtramite." aún no ha sido atendido, se solicita la atención del mismo";
                $this->notificacion->notificacionesweb($mensaje, $ruta, $value->id_coordinador, "ed5565");
                $this->notificacion->EnviarEmailTramites($value->email, 'Trámite sin atender', '', $mensaje, $ruta);
            }
        }
    }

    function diasLaborables($from, $to)
    {
        $workingDays = [1, 2, 3, 4, 5];
        $holidayDays = ['*-12-25', '*-01-01'];
        $from = new DateTime($from); $to = new DateTime($to);
        
        $interval = new DateInterval('P1D');
        $periods = new DatePeriod($from, $interval, $to);
        $days = 0;

        foreach ($periods as $period)
        {
            if(!in_array($period->format('N'), $workingDays)) continue;
            if(in_array($period->format('Y-m-d'), $holidayDays)) continue;
            if(in_array($period->format('*-m-d'), $holidayDays)) continue;
            $days++;
        }

        return $days;
    }
}
