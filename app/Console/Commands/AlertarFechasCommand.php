<?php

namespace App\Console\Commands;

use App\FechasConmemorativasModel;
use App\Http\Controllers\NotificacionesController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AlertarFechasCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alertafecha';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $fecha_actual = date("Y-m-d");
        $fecha = date("Y-m-d", strtotime($fecha_actual . "- 15 days"));
        $datos = FechasConmemorativasModel::where('estado', 'ACT')
            ->where('fecha', '<=', $fecha)->get();
        $hora = date('H');
        $notificacion = new NotificacionesController;

        $usuarios = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->where('id_perfil', 9)->get();

        $players = array();
        $correos = array();

        foreach ($usuarios as $key => $value) {
            array_push($correos, $value->email);
            $telefonos = DB::table('tma_playerid')
                ->where('users_id', $value->users_id)->get();
            foreach ($telefonos as $kk => $vv) {
                array_push($players, $vv->id_player);
            }
        }
        foreach ($datos as $key => $value) {
            # code...
            // if ($hora == 10) {
                # code...
                $notificacion->notificacion($players, 0, '🔔  Fecha conmemorativa ', $value->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($value->fecha)), 2, $value);
                $notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], 0, '🔔  Fecha conmemorativa ',  $value->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($value->fecha)), 2, $value);
                $notificacion->EnviarEmailAgenda($correos, 'Fecha conmemorativa', '', '🔔  Fecha conmemorativa ', $value->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($value->fecha)), 'coordinacioncronograma/FechasConmemorativas', 'vistas.emails.email');
            // }
        }
    }
}
