<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        \App\Console\Commands\AlertaActividadCommand::class,
        \App\Console\Commands\CambiarEstadosCommand::class,
        \App\Console\Commands\AgendaVirtualCommand::class,
        \App\Console\Commands\AlertarTramitesInternos::class,
        \App\Console\Commands\generarZip::class,
        \App\Console\Commands\ActualizaCedulasCommand::class,
        \App\Console\Commands\AlertarFechasCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('alertaactividad')->twiceDaily(8);
        $schedule->command('estadoactividad')->twiceDaily(8,10,12,16);
        $schedule->command('alertaractividades')->twiceDaily(9,15,14,16);
        $schedule->command('alertaragenda')->everyFiveMinutes();
        // $schedule->command('alertartramitesinternos')->twiceDaily(7, 13);
        // $schedule->command('donaciones')->everyFiveMinutes();
        // $schedule->command('actualizarCedulas')->everyFiveMinutes();
        $schedule->command('alertafecha')->everyFiveMinutes();
        /*
        $schedule->command('enviarcorreotramites')->sundays()->at('16:00');//between("16:00","20:00");
        //$schedule->command('demonioplacetopay')->everyFiveMinutes();        
        //$schedule->command('enviarcorreotramites')->everyMinute();
        //$schedule->command('enviarcorreotramites')->everyFiveMinutes();
        */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
