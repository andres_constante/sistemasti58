<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuModulosModel extends Model
{
    //
    protected $table = 'ad_menu_modulo';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'id_modulo'=>'required',
                'id_menu'=>'required',
                'ruta'=>'required',
                'icono'=>'required',
                'nivel'=>'required',
                'idmain'=>'required',
                'orden'=>'required|numeric',
                'visible'=>'required'
            ], $merge);
        }    
}
