<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreguntasEncuestaModel extends Model
{
    //
    protected $table = 'salud_ocupacional_preguntas';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [
                'pregunta'=>'required',
            ], $merge);
        }
}
