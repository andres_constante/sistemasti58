<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class UsuariosModel extends Model
{
    use Notifiable;
    protected $table = 'users';
    protected $hidden = array('password', 'remember_token');

    const ANALISTA_DIRECTOR = [62, 67];

    const DIRECTORES = [
        3, 4, 5, 9, 15, 16, 21, 22, 25, 37, 42, 44, 45, 49, 52, 54, 70
    ];

    const SOLO_DIRECTORES = [
        3, 5, 9,  15, 22, 29, 32, 37, 42, 44, 49,
    ];

    const ID_COORDINADOR = [21, 70];
    const SECRETARIO_GENERAL = 54;
    const ID_SECRETARIA_GENERAL = [10, 68];

    const ANALISTAS = [
        23, 26, 46, 47, 48, 50, 51, 55, 56, 58, 61, 63, 66, 68, 69
    ];

    const DIRECCIONES_INA = [
        23, 29, 32, 33, 41, 25, 65, 78, 79, 80,
        81, 82, 83, 84, 85, 86, 87, 88, 89,
        90, 91, 92, 93, 94, 95, 96, 97, 98,
        99, 100, 101, 102, 103, 104, 105, 106, 107,
        108, 109, 110, 111, 112, 113, 114, 115, 116
    ];

    const REVISAR_TRAMITES = [50, 57];

    const COORDINACIONES = [18, 47, 63, 64];
     
    public static function rules ($id=0, $merge=[])
    {
        return array_merge([
            'name'=>'required',
            'cedula'=>'required',
            //'cedula'=>'required|min:10|unique:users'. ($id ? ",id,$id" : ''),
            'email'=>'required|email|unique:users'. ($id ? ",id,$id" : '')
        ], $merge);
    }

    public static function rulescon ($id=0, $merge=[])
    {
        return array_merge([
            'email'=>'required|email|unique:users'. ($id ? ",id,$id" : ''),
            'password'=>'required|alpha_num|min:4|confirmed',
            'password_confirmation'=>'required|alpha_num|min:4',
            'name'=>'required',
            'id_direccion'=>'required',
            'cargo'=>'required'
        ], $merge);
    }

    public function scopeDirectores($query)
    {
        return $query->join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
            ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
            ->select('users.id', 'users.name', 'users.email')
            ->whereIn('ap.id', [3, 5, 9, 15, 16, 22, 21, 25, 37, 49, 54]);
    }

    public function esDirector()
    {
        $director = Str::contains($this->cargo, 'DIRECTOR') || Str::contains($this->cargo, 'VICEALCALDE');
        $esDirector = collect(self::DIRECTORES)->contains(Auth::user()->id_perfil);
        
        return $director && $esDirector ? true : false;
    }

    public static function esCoordinador()
    {
        return collect(self::ID_COORDINADOR)->contains(Auth::user()->id_perfil);
    }
}
