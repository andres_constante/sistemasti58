<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonaDonacionesModel extends Model
{
    //
    protected $table = 'dona_donciones';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [               
                'nombre_beneficiario'=>'required',
                'cedula_beneficiario'=>'required',
                'id_donacion'=>'required',
                // 'telefono'=>'required',
                'direccion'=>'required',
                // 'observacion'=>'required',
                // 'donacion'=>'required',
            ], $merge);
        }    
    public static function rules_2 ($id=0, $merge=[]) {
            return array_merge(
            [               
                
            ], $merge);
        }    
}
