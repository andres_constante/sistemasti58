<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonaIngresoDonacionDetaModel extends Model
{
    //
    protected $table = 'dona_tmov_ingreso_donacion_deta';
}
