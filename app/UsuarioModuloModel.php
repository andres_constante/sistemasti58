<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioModuloModel extends Model
{
    //
    protected $table = 'ad_usuario_modulo';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'id_usuario'=>'required',
                'id_modulo'=>'required'
            ], $merge);
        }
}

