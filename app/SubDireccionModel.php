<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubDireccionModel extends Model
{
    
    //
    protected $table="tmae_direccion_subarea";
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'area'=>'required|unique:tmae_direccion_subarea'. ($id ? ",id,$id" : ''),
                'id_direccion'=>"required"
            ], $merge);
    }
}
