<?php

namespace App\Http\Resources;

use App\UsuariosModel;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class AgendaVirtualResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // dd($request);
        // return parent::toArray($request);
        // Carbon::setlocale('es');

        // Carbon::setLocale(env('LOCALE', 'es'));
        // dd();
        if ($this->delegacion_alcalde) {
            
            $usuarios =$this->delegacion_alcalde;
            $responsables = DB::select('select GROUP_CONCAT(name SEPARATOR ",") as delegacion_alcalde from users where id in (' . $usuarios . ') ');
            $responsables_ = $responsables[0]->delegacion_alcalde;
        } else {
            $responsables_ = null;
        }
        $user=UsuariosModel::
        join('ad_perfil as p','p.id','users.id_perfil')
        ->where('users.id',$this->id_usuario)->first();


        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'direccion' => $this->direccion,
            'detalle_direccion' => $this->detalle_direccion,
            'longitud' => $this->longitud,
            'latitud' => $this->latitud,
            'estado_aprobacion' => $this->estado_aprobacion,
            'tipo' => $this->tipo,
            'categoria' => $this->categoria,
            'id_estado' => $this->id_estado,
            'longitud' => $this->longitud,
            'observacion' => $this->observacion,
            'responsable' => $this->responsable,
            'n_beneficiarios' => $this->n_beneficiarios,
            'observacion_cancelacion' => $this->observacion_cancelacion,
            'fecha_evento' => Carbon::createFromFormat('Y-m-d H:i:s', $this->fecha_inicio)->format('d-m-Y'), //->format('l j F'),
            'hora_inicio' => Carbon::createFromFormat('Y-m-d H:i:s', $this->fecha_inicio)->format('H:i A'), //->toTimeString(),
            'hora_fin' => Carbon::createFromFormat('Y-m-d H:i:s', $this->fecha_fin)->format('H:i A'), //->toTimeString(),
            'usuario' => new UsuarioResource($this->usuario),
            'adjunto_link' => URL::to('') . '/archivos_sistema/' . $this->adjunto,
            'disposicion_alcalde' => $this->disposicion_alcalde,
            'delegacion_alcalde' => $responsables_,
            'telefono_solicitante' => $this->telefono_solicitante,
            'email_solicitante' => $this->email_solicitante,
            'hora_inicio_2' => Carbon::createFromFormat('Y-m-d H:i:s', $this->fecha_inicio)->format('Y-m-d H:i:s'), //->toTimeString(),
            'hora_fin_2' => Carbon::createFromFormat('Y-m-d H:i:s', $this->fecha_fin)->format('Y-m-d H:i:s'), //->toTimeString(),
            'fecha_inicio' => Carbon::createFromFormat('Y-m-d H:i:s', $this->fecha_inicio)->format('Y-m-d H:i:s'), //->toTimeString(),
            'fecha_fin' => Carbon::createFromFormat('Y-m-d H:i:s', $this->fecha_fin)->format('Y-m-d H:i:s'), //->toTimeString(),
            'tipo_evento' => $this->tipo_evento,
            'id_tipo' => $this->id_tipo,
            'usuario_registra' => $user->name,
            'perfil_registra' => $user->id_perfil
        ];
    }
}
