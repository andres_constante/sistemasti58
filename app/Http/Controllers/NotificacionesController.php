<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\NotificacionesModel;
use Session;
use Mail;
use DB;
use URL;

use Illuminate\Support\Facades\Auth;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use App\UsuariosModel;
use Illuminate\Support\Facades\Log;

class NotificacionesController extends Controller
{
    var $configuraciongeneral = array("Prueba De Notificaciones", "notificaciones", "index");
    var $objetos = '[
		{"Tipo":"text","Descripcion":"Título","Nombre":"titulo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"textarea","Descripcion":"Descripción","Nombre":"descripcion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
		]';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //show($this->objetos);

        $objetos = json_decode($this->objetos);
        return view('vistas.notificacion', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
        ]);
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|max:255',
            'descripcion' => 'required',
        ]);


        $guardar = new NotificacionesModel;
        $guardar->token = $request->_token;
        $guardar->titulo = $request->titulo;
        $guardar->descripcion = $request->descripcion;
        $guardar->save();


        $this->notificacion($guardar->token, $guardar->id, $guardar->titulo, $guardar->descripcion, 5);

        Session::put('success', 'Notifiación Enviada Con Exito!!');

        $objetos = json_decode($this->objetos);
        return view('vistas.notificacion', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
        ]);
    }


    public function notificacion($to, $id, $title, $body, $data_adicional, $evento = [])
    {
        $content = array(
            "en" => $body
        );

        $heading = array(
            "en" => $title
        );
        $hashes_array = array();
        array_push($hashes_array, array(
            "id" => "like-button",
            "text" => $body,
            "icon" => "http://i.imgur.com/N8SN8ZS.png",
            "url" => "http://sistemasic.manta.gob.ec"
        ));
        $fields = array(
            'app_id' => "92c8acfb-c9ad-406c-abfa-0e60da2baea2",
            'include_player_ids' => $to,
            'data' => array(
                "foo" => "bar",
                "id" => $id,
                "tipo" => $data_adicional,
                "body" => $body,
                "id_apoyo" => $data_adicional,
                "eventos" => $evento
            ),
            'contents' => $content,
            'web_buttons' => $hashes_array,
            'headings' => $heading
        );

        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic YzFhYzQ4MTctZWNmYi00ZGIyLTg0MmYtOTg3ZDY2ZTY3NGRi'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $correos = ConfigSystem("correos");
        if ($correos == "SI") {
            // dd('SI');

            $response = curl_exec($ch);
            curl_close($ch);

            return $response;
        } else {
            // dd('NO');
            return null;
        }
    }
    public function notificacionmantapp($to, $id, $title, $body, $data_adicional, $tipo)
    {


        $content = array(
            "en" => $body
        );

        $heading = array(
            "en" => $title
        );
        $hashes_array = array();
        array_push($hashes_array, array(
            "id" => "like-button",
            "text" => $body,
            "icon" => "http://i.imgur.com/N8SN8ZS.png",
            "url" => "https://portalciudadano.manta.gob.ec/portalciudadano/publicaciones"
        ));
        $fields = array(
            'app_id' => "ce747270-efac-4d89-8a4d-a8510b8cde8a",
            // 'include_player_ids' => $to,
            'included_segments' => array('All'),
            'data' => array(
                "foo" => "bar",
                "id" => $id,
                "body" => $body,
                "id_apoyo" => $data_adicional,
                "tipo" => $tipo
            ),
            'contents' => $content,
            'web_buttons' => $hashes_array,
            'headings' => $heading
        );

        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic MTFlOWM4YzAtNDk2MC00M2ExLTgyM2MtZTMxMDYyMzgyM2M2'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $enviarcorreo = ConfigSystem('correos');
        if ($enviarcorreo == 'SI') {
            $response = curl_exec($ch);
            curl_close($ch);
            // dd($response);
            // Log::info($response);
            return $response;
        } else {
            return null;
        }
    }

    public function EnviarEmail($email, $asunto, $tipo, $detalle, $ruta, $vista = "vistas.email", $bonton = "SI")
    {
        $objetos = '[
            {"Tipo":"tipo","Descripcion":"Notificación","Nombre":"tipo","Valor":"Null"},
            {"Tipo":"mensaje","Descripcion":"Detalle","Nombre":"detalle","Valor":"Null"},
            {"Tipo":"buttonlink","Descripcion":"Ver","Nombre":"botonlink","Valor":"Null"}
            ]';
        // dd($tabla);
        $objetos = json_decode($objetos);
        //show($tipo);
        $objetos[0]->Valor = $tipo;
        $objetos[1]->Valor = $detalle;
        $objetos[2]->Valor = config('app.url') . '/' . $ruta;
        if ($bonton == 'NO') {
            unset($objetos[2]);
        }
        $usuario = UsuariosModel::where('email', $email)->first();
        $name = '';
        if ($usuario) {
            $name = $usuario->name;
        }


        $correos = ConfigSystem("correos");
        if ($correos == "SI") {
            Log::info($email);
            Mail::send(
                $vista,
                [
                    'objetos' => $objetos,
                    'usuario' => $name
                ],
                function ($m) use ($email, $asunto) {
                    $m->from("sistemasic@manta.gob.ec", "SistemasIC");
                    $m->to($email)->subject($asunto);
                }
            );
        }
    }



    public function EnviarEmailAgenda($email, $asunto, $tipo, $detalle, $ruta, $vista = "vistas.email", $bonton = "SI")
    {
        $objetos = '[
            {"Tipo":"tipo","Descripcion":"Notificación","Nombre":"tipo","Valor":"Null"},
            {"Tipo":"mensaje","Descripcion":"Detalle","Nombre":"detalle","Valor":"Null"},
            {"Tipo":"buttonlink","Descripcion":"Ver","Nombre":"botonlink","Valor":"Null"}]';
        // dd($tabla);
        $objetos = json_decode($objetos);
        //show($tipo);
        $objetos[0]->Valor = $tipo;
        $objetos[1]->Valor = $detalle;
        $objetos[2]->Valor = config('app.url') . '/' . $ruta;
        if ($bonton == 'NO') {
            unset($objetos[2]);
        }

        $correos = ConfigSystem("correos");
        //show($correos);
        if ($correos == "SI") {
            Mail::send(
                $vista,
                ['objetos' => $objetos],
                function ($m) use ($email, $asunto) {
                    $m->from("sistemasic@manta.gob.ec", "SistemasIC");
                    $m->to($email)->subject($asunto);
                }
            );
        }
    }


    function getnotificacuenta()
    {
        $total = DB::table("tmo_notificaciones")
            ->where("users_id", "=", Auth::user()->id)
            ->where("estado", "=", "ACT")
            ->whereBetween("created_at", ['"' . date('Y-m-d') . ' 00:00:00"', date('Y-m-d ') . '23:59:59'])
            ->count();
        return $total;
    }


    public function EnviarEmailTram($email, $asunto, $tipo, $detalle, $vista = "vistas.email", $boton = 'SI')
    {
        $objetos = '[
            {"Tipo":"tipo","Descripcion":"Notificación","Nombre":"tipo","Valor":"Null"},
            {"Tipo":"mensaje","Descripcion":"Detalle","Nombre":"detalle","Valor":"Null"},
            {"Tipo":"buttonlink","Descripcion":"Ver","Nombre":"botonlink","Valor":"Null"}
        ]';

        $objetos = json_decode($objetos);

        $objetos[0]->Valor = $tipo;
        $objetos[1]->Valor = $detalle;

        if ($boton == 'SI') {
            $ruta = 'https://portalciudadano.manta.gob.ec/portalciudadano/serviciosdigitales/solicitudesalcaldia';
            $objetos[2]->Valor = $ruta;
        } else unset($objetos[2]);

        $correos = ConfigSystem("correos");
        //show($correos);
        if ($correos == "SI") {
            Mail::send(
                $vista,
                ['objetos' => $objetos],
                function ($m) use ($email, $asunto) {
                    $m->from("sistemasic@manta.gob.ec", "SistemasIC");
                    $m->to($email)->subject($asunto);
                }
            );
        }
    }

    public function EnviarEmailTramites($email, $asunto, $tipo, $detalle, $ruta, $vista = "vistas.email", $bonton = "SI", $timeline = array())
    {
        $objetos = '[
            {"Tipo":"timeline","Descripcion":"Linea de tiempo","Nombre":"timeline","Valor":"Null"}
            {"Tipo":"tipo","Descripcion":"Notificación","Nombre":"tipo","Valor":"Null"},
            {"Tipo":"mensaje","Descripcion":"Detalle","Nombre":"detalle","Valor":"Null"},
            {"Tipo":"buttonlink","Descripcion":"Ver","Nombre":"botonlink","Valor":"Null"}
        ]';

        $objetos = json_decode($objetos);

        $objetos[1]->Valor = $tipo;
        $objetos[2]->Valor = $detalle;
        $objetos[0]->Valor = $timeline;
        $objetos[3]->Valor = config('app.url') . '/' . $ruta;

        if ($bonton == 'NO') unset($objetos[2]);

        $correos = ConfigSystem("correos");

        if ($correos == "SI") {
            Mail::send(
                $vista,
                ['objetos' => $objetos],
                function ($m) use ($email, $asunto) {
                    $m->from("sistemasic@manta.gob.ec", "SistemasIC");
                    $m->to($email)->subject($asunto);
                }
            );
        }
    }


    public function EnviarEmailTramiteFin($email, $asunto, $tipo, $detalle, $pdf)
    {
        $objetos = '[
            {"Tipo":"tipo","Descripcion":"Tipo Certificado","Nombre":"tipo","Valor":"Null"},
            {"Tipo":"mensaje","Descripcion":"Detalle","Nombre":"detalle","Valor":"Null"}
        ]';

        $objetos = json_decode($objetos);
        $objetos[0]->Valor = $detalle;
        $objetos[1]->Valor = $tipo;

        $enviarcorreo = ConfigSystem('correos');
        if ($enviarcorreo == 'SI') {
            Mail::send(
                'vistas.email',
                ['objetos' => $objetos],
                function ($m) use ($email, $asunto, $pdf) {
                    $m->from("sistemasic@manta.gob.ec", "SistemasIC");
                    $m->to($email)->subject($asunto);
                    $m->attach($pdf, [
                        'as' => 'Trámite Finalizado.pdf',
                        'mime' => 'application/pdf',
                    ]);
                }
            );
        }
    }

    public function EnviarEmailTramiteFinArray($email, $asunto, $tipo, $detalle, $archivos)
    {
        $objetos = '[
            {"Tipo":"tipo","Descripcion":"Tipo Certificado","Nombre":"tipo","Valor":"Null"},
            {"Tipo":"mensaje","Descripcion":"Detalle","Nombre":"detalle","Valor":"Null"}
        ]';

        $objetos = json_decode($objetos);
        $objetos[0]->Valor = $detalle;
        $objetos[1]->Valor = $tipo;
        $path = public_path() . '/archivos_sistema/';

        $enviarcorreo = ConfigSystem('correos');
        if ($enviarcorreo == 'SI') {
            Mail::send('vistas.email', ['objetos' => $objetos], function ($m) use ($email, $asunto, $archivos, $path) {
                $m->from("sistemasic@manta.gob.ec", "SistemasIC");
                $m->to($email)->subject($asunto);
                foreach($archivos as $archivo)
                {
                    $ruta = $path . $archivo->ruta;
                    $m->attach($ruta, [
                        'as' => 'Trámite Finalizado.pdf',
                        'mime' => 'application/pdf',
                    ]);
                }
            });
        }
    }


    function getnotificatodos()
    {
        $notificaciones = DB::table("tmo_notificaciones")
            ->where("users_id", "=", Auth::user()->id)
            //->where("estado", "=", "ACT")
            ->whereBetween("created_at", ['"' . date('Y-m-d') . ' 00:00:00"', date('Y-m-d ') . '23:59:59'])
            ->orderBy("id", "desc")
            ->get();

        $html = '';
        foreach ($notificaciones as $notificacion) {
            $estilo = null;
            // if($notificacion->estado=="ACT"){
            $estilo = 'style="background: #' . $notificacion->color . '; color:white;"';
            //   $estilo='style="background: #2c438f; color:white;"';
            // }
            if ($notificacion->estado == "INA") {
                $estilo = '';
            }

            $html .= '<li class="divider"></li>
                            <li ' . $estilo . ' >
                                <div class="link-block" style="text-align: justify;">
                              
                                    <a href="' . URL::to($notificacion->ruta) . '" onclick="desactivarnoti(' . $notificacion->id . ')">
                                       <span> 🔔' . $notificacion->notificacion . ' 📨 </span>
                                        
                                    </a>
                                </div>
                            </li>';
        }

        return $html;
    }


    function desactivarNoti($id)
    {
        DB::table("tmo_notificaciones")
            ->where("id", "=", $id)
            ->update(["estado" => 'INA']);
    }


    function notificacionesweb($notificacion, $ruta, $user_id, $color)
    {
        DB::table("tmo_notificaciones")->insert(
            [
                'notificacion' => $notificacion, 'ruta' => $ruta,
                'users_id' => $user_id, 'color' => $color, 'created_at' => date('Y-m-d H:i:s')
            ]

        );
    }


    public function actividadesVencidas()
    {
        $query =
            CoorCronogramaCabModel::whereBetween("fecha_fin", ['2018-01-01 00:00:00', date('Y-m-d H:i:s')])
            ->where("avance", "<", 100)->where("estado", "ACT")->where("estado_actividad", "EJECUCION");

        $vencidas = $query->get();

        $todalvencidas = $query->count();

        $emails = explode("|", ConfigSystem("correoscoordinacion"));
        for ($i = 0; $i < count($emails); $i++) {
            $user = UsuariosModel::where("email", $emails[$i])->first();
            if ($user) {
                $this->notificacionesweb("Existen " . $todalvencidas . " vencida(s)", "ruta", $user->id, "ed5565");
            }
        }
        //return $vencidas;
    }


    public function vercorreoprueba()
    {
        $tipo = "Actividad Procesada";
        $detalle = "Se ha finalizado una actividad";
        $ruta = "ruta";
        $objetos = '[
            {"Tipo":"tipo","Descripcion":"Notificacion","Nombre":"tipo","Valor":"Null"},
            {"Tipo":"mensaje","Descripcion":"Detalle","Nombre":"detalle","Valor":"Null"},
            {"Tipo":"buttonlink","Descripcion":"Ver","Nombre":"botonlink","Valor":"Null"}            

        ]';
        // dd($tabla);
        $objetos = json_decode($objetos);
        //show($tipo);
        $objetos[0]->Valor = $tipo;
        $objetos[1]->Valor = $detalle;
        $objetos[2]->Valor = config('app.url') . '/' . $ruta;
        //show(array($email,$asunto,$tipo,$detalle,$ruta),0);
        //show($objetos);
        //show($correos);
        //return view("vistas.emails.email",['objetos' => $objetos]);
        Mail::send(
            'vistas.emails.email',
            ['objetos' => $objetos],
            function ($m) {
                $m->from("planificacion_controldegestion@manta.gob.ec", "Planificación");
                $m->to("andycmur@gmail.com")->subject("Prueba de Mensaje de Actividad");
            }
        );
    }
}
