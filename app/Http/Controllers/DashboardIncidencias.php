<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Auth;
use Carbon\Carbon;
use App\UsuariosModel;
use DB;


use Illuminate\Http\Request;

class DashboardIncidencias extends Controller{
    

	var $configuraciongeneral = array ("Coordinación Actividades - Búsqueda", "coordinacioncronograma/coordinacionlista", "index");
    var $escoja=array(0=>"TODOS");
	var $objetos = '[{"Tipo":"select","Descripcion":"Tipo de Filtro","Nombre":"tipofiltro","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';

	var $objetosTable = '[
		{"Tipo":"text","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Avance","Nombre":"avance","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Ultima Modificacion","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }

				]';




	public function __construct() {
				$this->middleware('auth');
	}

	public function denuncias()
	{

		$objetos = json_decode($this->objetos);
		//show($this->objetos);
		$objetos[0]->Nombre="tipofiltro";
		$tipoFiltro=array(1=>"DIRECCION",2=>"ESTADO",3=>"PRIORIDAD");

		$objetos[0]->Valor=$this->escoja + $tipoFiltro;
		$objetosTable = json_decode($this->objetosTable);
		$objetos=array_values($objetos);
		$delete='si';
		$tipo_fecha="Busqueda por Fecha de Última Modificación";
		$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
		if($id_tipo_pefil->tipo==2||$id_tipo_pefil->tipo==3){
			$delete=null;
		}

        return view('dashboard_denuncias',[
			"objetos"=>$objetos,
			"objetosTable"=>$objetosTable,
			"configuraciongeneral"=>$this->configuraciongeneral,
			"delete"=>$delete,
			"tipo_fecha"=>$tipo_fecha,
            "create"=>"si"
            ]);


	}



}
