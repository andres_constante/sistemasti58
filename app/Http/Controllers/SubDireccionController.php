<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use stdClass;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use App\SubDireccionModel;
use DB;
use Session;
class SubDireccionController extends Controller
{
    //
    var $configuraciongeneral = array ("Subdirecciones", "subdirecciones", "index",6=>"subdireccionesajax",7=>"subdirecciones");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"select-multiple","Descripcion":"SubDirección","Nombre":"area","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
        "tipo"=>"ruc: {
                            required: true
                        }"
    );
    public function __construct() {
        $this->middleware('auth');
    }
    public function perfil()
    {
        $object = new stdClass;
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
        //show($object);
        return $object;
    }
    public function index()
    {
        //$tabla=CoorTipoObraModel::where("estado","ACT")->orderby("id","desc")->get();//->paginate(500);
        $tabla=[];
        $objetos=json_decode($this->objetos);
        $objetos[0]->Nombre="direccion";
        return view('vistas.index',[
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "permisos" => $this->perfil(),
            "delete" => "si",
            "create" => "si"
        ]);
        //return view('coordinacioncronograma::index');
    }
    public function verpermisos($objetos = array(), $tipo = "index")
    {
        $tabla = SubDireccionModel::
        join("tmae_direcciones as a", "a.id", "=", "tmae_direccion_subarea.id_direccion")
            ->select("tmae_direccion_subarea.*", "a.direccion")
            //->where("coor_tmov_compromisos_cab.estado", "ACT")
            ->orderby("tmae_direccion_subarea.id","desc");
        //show($tabla->get());
        $tabla = direccionesModel::select("tmae_direcciones.id","tmae_direcciones.direccion",
            DB::raw("(select GROUP_CONCAT(y.direccion SEPARATOR ', ') from tmae_direcciones AS y inner join tmae_direccion_subarea as x on x.area = y.id where x.id_direccion = tmae_direcciones.id limit 1) as area")
            )
            ->whereRaw('tmae_direcciones.id IN (SELECT id_direccion FROM tmae_direccion_subarea)');
        //show($tabla->toarray());
        $seleccionar_direccion = direccionesModel::where("estado", "ACT");
        $seleccionar_direccion = $seleccionar_direccion->orderby("direccion", "asc")->pluck("direccion", "id")->all();
        //show($seleccionar_direccion);
        if (count($objetos)) {
            $objetos[0]->Valor = $seleccionar_direccion;
            $objetos[1]->Valor = $seleccionar_direccion;
        }
        if ($tipo == "index") {
            $objetos[0]->Nombre = "direccion";
        }
        //show($objetos);
        return array($objetos, $tabla);
    }
    function subdireccionesajax(Request $request)
    {
        $tabla = $this->verpermisos(json_decode($this->objetos));
        $objetos = $tabla[0];
        //show($objetos);
        $tabla = $tabla[1];
        $columns = array();
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        //show($columns);
        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('tmae_direccion_subarea.id', 'LIKE', "%{$search}%")
                    ->orWhere('tmae_direccion_subarea.area', 'LIKE', "%{$search}%")
                    ->orWhere('a.direccion', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();

        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            $permisos = $this->perfil();
            foreach ($posts as $post) {
                //=============================================================
                $show = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)'));
                //'&nbsp;&nbsp;' .
                $edit=link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o'));
                //. '&nbsp;&nbsp;
                $dele='<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                if ($permisos->ver == 'NO')
                    $show="";
                if ($permisos->editar == 'NO')
                    $edit="";
                if ($permisos->eliminar == 'NO')
                    $dele="";
                $aciones="$show&nbsp;&nbsp;$edit&nbsp;&nbsp;$dele";
                //=============================================================
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $valorc=$post->$campo;
                    $nestedData["$value->Nombre"] = "$valorc";
                }
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;

            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    public function formularioscrear($id)
    {
        $objetos = json_decode($this->objetos);
        $obaddjs=array();
        $objetosmain = $this->verpermisos($objetos, "crear");
        $objetos = $objetosmain[0];
        /*====================================================*/
        if($id!="")
        {
            $id=Crypt::decrypt($id);
            $tabla=SubDireccionModel::where("id_direccion",$id);
            $dirante=$tabla->pluck("area","area")->all();
            $tabla = $tabla->first();
            //==========================================================
            $this->configuraciongeneral[2] = "editar";
            $objetos[1]->ValorAnterior = $dirante;
            //show($objetos);
            $datos=array("objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla"=>$tabla,
                //"historialtable"=>$historialtable,
                //"obadd"=>$obaddjs,
                "botonguardaravance"=>"no"
            );
            //show($datos);
        }else{
            $this->configuraciongeneral[2] = "crear";
            //$objetos = $this->verpermisos($objetos, "crear")[0];
            /*Setear Chaleco Azul*/
            //show($objetos);
            $datos=array("objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            );
        }
        //show($datos);
        return view('vistas.create',$datos);
    }
    public function create()
    {
        return $this->formularioscrear("");
    }
    public function edit($id)
    {
        return $this->formularioscrear($id);
    }
    public function guardar($id)
    {
        //show($id);
        $input = Input::all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];
        DB::beginTransaction();
        try {
            if ($id == 0) {
                $ruta .= "/create";
                //$guardar = new subdireccionmodel;
                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro Compromiso Alcalde";
            } else {
                $ruta .= "/".Crypt::encrypt($id)."/edit";
                //$guardar = SubDireccionModel::find($id);
                $msg = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Compromiso Alcalde modificado";
            }


                if (Input::has("id_direccion")) {
                    $id_direccion=Input::get("id_direccion");
                    SubDireccionModel::where("id_direccion",$id_direccion)->delete();
                    $multiple = Input::get("area");
                    $json = [];
                    foreach ($multiple as $key => $value) {
                        # code...
                        $guardarres = new SubDireccionModel;
                        $guardarres->id_direccion = $id_direccion;
                        $guardarres->area = $value;
                        $guardarres->save();
                    }
                    Auditoria($msgauditoria . " - ID: " . $id);
                }
                DB::commit();
                Session::flash('message', $msg);
                return Redirect::to($this->configuraciongeneral[1]);
        } catch (\Exception $e) {
            // ROLLBACK
            DB::rollback();
            $mensaje = $e->getMessage();
            return redirect()->back()->withErrors([$mensaje])->withInput();
        }
    }
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }
}
