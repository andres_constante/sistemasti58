<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use GuzzleHttp\Client as GuzzleHttpClient;
use Auth;
use Carbon\Carbon;
use App\UsuariosModel;
use DateTimeZone;
use DB;
use Illuminate\Support\Facades\Log;

class CitasController extends Controller
{
    protected $notifica;

    public function __construct(NotificacionesController $notifica)
    {
        $this->middleware('auth');
        $this->notifica = $notifica;
    }

	public function index($tipo){
        $host = 'https://turnos.manta.gob.ec';
        $client = new GuzzleHttpClient();
        $respuesta = $client->request('GET',$host.'/CitasOnline/'.Auth::user()->cedula.'/'.$tipo, [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ]);
        $citas = json_decode($respuesta->getBody()->getContents());
        if($tipo == 1){ $title = 'Citas solicitadas';} elseif ($tipo ==2) {
           $title = 'Citas agendadas';
        }elseif ($tipo ==3) {
            $title = 'Citas';
        }
        return view('citas.index', compact('citas', 'citas'))->with('tipo', $tipo)->with('title', $title);
	}

	public function CitaSave(Request $request){
        try{
            $host = 'https://turnos.manta.gob.ec';
            $client = new GuzzleHttpClient(['verify' => false]);
            $respuesta = $client->request('GET', $host.'/SaveCitas?id_cita='.$request->id_cita.'&estado='.$request->estado.'&respuesta='.$request->respuesta.'&fecha='.$request->fecha, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ]
            ]);
            $citas = json_decode($respuesta->getBody()->getContents());
            if($citas->status == true){
                if($request->correo){
                    $correo = $this->notificarCiudadano($request);
                    Log::info('mensaje de correo '.$correo);
                }
                return response()->json(['status'=> $citas->status]);
            }else{
                return response()->json(['status'=> false, 'mensaje' =>'No se pudo notificar al ciudadano']);
            }

            // if($citas->status == true){
            //     return response()->json(['status'=> $citas->status]);
            // }
        }
		catch(\Exception $ex){
            return response()->json(['status'=> false, 'mensaje' =>'SaveCita '.$ex->getMessage(). ' linea->'. $ex->getLine() ]);
            Log::info($ex->getMessage());
            // dd($ex->getMessage());
        }
	}

    public function notificarCiudadano(Request $request)
    {
        try
        {
            Log::info('Notificando ciudadano');
            $fechaHora = explode(' ', $request->fecha);
            $hora = (new Carbon($fechaHora[1]))->format('g:i A');
            $fecha = Carbon::createFromFormat('Y-m-d H:i', $request->fecha, new DateTimeZone(config('app.timezone')))
                    ->isoFormat('dddd, D [de] MMMM [de] YYYY');

            $mensaje = '';
            $title = '';
            if($request->estado == 1){
                $title = 'Cita atendida';
                $mensaje = 'Estimado(a) ciudadano; su trámite ha sido atendido exitosamente. <br> <br>'.$request->respuesta;
            }
            if($request->estado == 2){
                $title = 'Cita agendada';
                $mensaje = 'Estimado(a) ciudadano; se le ha agendado una cita para el día: ' . $fecha . '; ' . $hora . '. <br> <br>'.$request->respuesta;
            }     
            if($request->estado == 3){
                $title = 'Cita rechazada';
                $mensaje = 'Estimado(a) ciudadano; su solicitud ha sido rechazada. <br> <br>'.$request->respuesta;
            }
           
        
            $observacion = $mensaje . '<br> <br>Por favor asistir cumpliendo con todas las medidas de bioseguridad requeridas.';
            Log::info(json_encode($request->all()));
            $this->notifica->EnviarEmailTram($request->correo, $title, '', $observacion, 'vistas.email', 'NO');
            Log::info('Se ha notificado al ciudadano sobre la cita agendada');
            return response()->json(['status' => true, 'mensaje' =>'Se ha notificado al ciudadano sobre la cita agendada']);

            // return back()->with('message', 'Se ha notificado al ciudadano sobre la cita agendada');
        }catch(\Exception $ex){
            // dd($ex->getMessage());
            Log::info($ex->getMessage());
            return response()->json(['status' => false, 'mensaje' => 'Correo '.$ex->getMessage().' linea->'. $ex->getLine()]);
            // return back()->withErrors(['No se pudo enviar el correo'])->withInput();
        }
    }
}
