<?php

namespace App\Http\Controllers;

use App\DonaDonacionesModel;
use App\DonaIngresoDonacionProductoModel;
use App\DonaIngresoProductoModel;
use App\DonaItemsModel;
use App\EncuestaSaludModel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\UsuariosModel;
use Config;
use DB;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
// use Auth;
use Carbon\Carbon;
use DateTime;
use Modules\CoordinacionCronograma\Http\Controllers\CoordinacionChartsController;
//use App\ReportesJasperController;
use Map;
use Modules\ComunicacionAlcaldia\Entities\AgendaVirtualModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\Coordinacioncronograma\Entities\CoorTipoObraModel;
use Modules\Coordinacioncronograma\Entities\CoorContratistaModel;

use Modules\Coordinacioncronograma\Entities\CabRecomendacionContraModel;
use Modules\Coordinacioncronograma\Entities\CabRecomendacionContraHistoModel;
use Modules\ComunicacionAlcaldia\Entities\AgendaEstadosModel;
use Modules\ComunicacionAlcaldia\Entities\TipoAgendaModel;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\CoordinacionCronograma\Entities\GesIndicadorModel;
use Modules\CoordinacionCronograma\Entities\MultasUnaserModel;
use Modules\Coordinacioncronograma\Entities\PoaAutorizacionActiModel;
use Modules\CoordinacionCronograma\Entities\TipoActividadPoaModel;
use ZipArchive;

class ReportController extends Controller
{
    /**
     * ReportController constructor. middleware for authenticate users
     */
    var $escoja = array(0 => "TODOS");
    var $formatos = array("pdf" => "PDF", "xls" => "XLS");
    var $reportes = array(
        null => "Escoja el reporte",
        "coordinacioncronograma/resportes_estadisticos" => "REPORTES ESTADÍSTICOS",
        // "coordinacioncronograma/reporteincidencia" => "REPORTE DE INCIDENCIAS TERRITORIALES",
        "reportescoordinacionactividades" => "REPORTE DE DISPOSICIONES GENERALES",
        "reportescompromisos" => "REPORTE COMPROMISOS TERRITORIALES",
        "reportespoa" => "REPORTE POA",
        "reportespoacertificaciones" => "REPORTE CERTIFICACIONES POA",
        "reportescoordinacionobras" => "REPORTE DE OBRAS",
        "reportesrecomendaciones" => "REPORTE RECOMENDACIONES DE CONTRALORIA",
        "reportesplanpropuesta" => "REPORTE PLAN DE PROPUESTA",
        "reportesindicadores" => "REPORTE INDICADORES DE GESTION",
        "coordinacioncronograma/coordinacionlista" => "Coordinación Actividades - Búsqueda",
        "coordinacioncronograma/coordinacionlistaobras" => "Coordinación Obras - Búsqueda"

    );
    var $reportes_objeto = '[
        {"Tipo": "select", "Descripcion": "Tipo de reporte", "Nombre": "reporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Ancho":12}    
    ]';
    protected $ReportJasper;
    protected $DatosActividad;

    public function __construct(ReportesJasperController $ReportJasper, CoordinacionChartsController $DatosActividad)
    {
        $this->middleware('auth', ['except' => ['reportescoordinacionactividades']]);
        $this->ReportJasper = $ReportJasper;
        $this->DatosActividad = $DatosActividad;
    }

    public function generarreporte()
    {
        $input = Input::all();
        //show($input);      
        $data = json_decode(Input::get("data"));
        // dd($data['ESTADO[]']);
        //$namedata=Crypt::decrypt(Input::get("namedata"));
        $paramadi = Crypt::decrypt(Input::get("paramadi"));
        $paramsok = array();
        $estado = array();
        $bandera_1 = true;
        $estado_actividad = array();
        $bandera_2 = true;
        $id_estado_actividad = array();
        $bandera_3 = true;
        $numebre_reporte = '';
        // $namedata=
        foreach ($data as $key => $value) {
            # code...
            //show($value);
            if ($value->name != "_token" && $value->name != "formato" && $value->name != "namereporte" && $value->name != "tituloreporte" && $value->name != "tiporeporte" && $value->name != "nada" && $value->name != "reporte") {


                if ($value->name == "ESTADO[]") {
                    // dd($value);
                    array_push($estado, "'" . $value->value . "'");
                } elseif ($value->name == "ESTADOACTIVIDAD[]") {
                    array_push($estado_actividad, "'" . $value->value . "'");
                } elseif ($value->name == "IDESTADO[]") {
                    array_push($id_estado_actividad, "'" . $value->value . "'");
                } else {

                    $paramsok[$value->name] = $value->value;
                }
            } elseif ($value->name == "formato")
                $formato = $value->value;
            elseif ($value->name == "namereporte") {
                $namedata = $value->value;
                // $numebre_reporte=
            } elseif ($value->name == "tituloreporte")
                $paramadi["TITULO"] = mb_strtoupper($value->value);
            elseif ($value->name == "tiporeporte")
                if ($value->value == "informe" || $value->value == "observa")
                    $namedata .= "_" . $value->value;
        }
        if (is_array($estado) && isset($estado[0])) {
            $estados = implode(',', $estado);
            $paramsok['ESTADO'] = $estados;
            if ($estados == "'0'") {
                $paramsok['ESTADO'] = 0;
            }
        }
        if (is_array($estado_actividad) && isset($estado_actividad[0])) {
            $estados = implode(',', $estado_actividad);
            $paramsok['ESTADOACTIVIDAD'] = $estados;
            if ($estados == "'0'") {
                $paramsok['ESTADOACTIVIDAD'] = 0;
            }
        }

        if (is_array($id_estado_actividad) && isset($id_estado_actividad[0])) {
            $estados = implode(',', $id_estado_actividad);
            $paramsok['IDESTADO'] = $estados;
            if ($estados == "'0'") {
                $paramsok['IDESTADO'] = 0;
            }
        }


        // Filtros

        // show($paramsok);
        //$name, $params, $format, $filename, $download, $download_is_attachment = true
        $filename = "Actividades-" . str_random(6);
        $filename = "Actividades-" . str_random(6);
        if(isset($namedata)){
            $filename = $namedata."-" . str_random(6);

        }
        //show(array($namedata,$paramsok+$paramadi,$formato,$filename,true,false));
        return $this->ReportJasper->generateReport($namedata, $paramsok + $paramadi, $formato, $filename, true, false);
    }

    public function reportescoordinacionactividades()
    {
        // dd(Input::all());
        if (Input::get("token"))
            return $this->generarreporte();
        //Objetos a Armar
        //FormHidden
        $objects = json_decode('[
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Prioridad", "Nombre": "PRIORIDAD", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},            
            {"Tipo": "select", "Descripcion": "Avance Mínimo", "Nombre": "AVANCEINI", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Avance Máximo", "Nombre": "AVANCEFIN", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Rendimiento Mínimo", "Nombre": "RENDMIENTOINI", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Rendimiento Máximo", "Nombre": "RENDMIENTOFIN", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Actualización Desde", "Nombre": "FECHAACTUDESDE", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Actualización Hasta", "Nombre": "FECHAACTUHASTA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select-multiple", "Descripcion": "Estado", "Nombre": "ESTADO", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo de actividad", "Nombre": "TIPOACTIVIDAD", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        /*COMBOS*/
        $direcciones = direccionesModel::join("coor_tmov_cronograma_cab as a", "a.id_direccion", "=", "tmae_direcciones.id")
            ->select("tmae_direcciones.direccion", "tmae_direcciones.id", DB::raw("count(*) as total"))
            ->groupBy("tmae_direcciones.direccion", "tmae_direcciones.id")
            ->orderby("tmae_direcciones.direccion")
            ->pluck("direccion", "id")
            ->all();

        $prioridad = explodewords(ConfigSystem("prioridad"), "|");

        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $fechaini = "2019-05-01";
        $fechafin = date('Y') . "-12-31";
        /**/
        // $fechahoy = new DateTime();
        // $fechahoy->modify('this week');
        $fechadesde = "2019-01-01";
        // $fechadesde = $fechahoy->format('Y-m-d');
        // $fechahoy->modify('this week +4 days');
        $fechahasta = date('Y') . "-12-31";;
        /**/
        $rendi = array();
        for ($i = 0; $i <= 100; $i += 5) {
            # code...
            $rendi[$i] = $i;
        }
        //TipoNameReporte
        $namerepor = array(
            //"rpt_rendimiento_actividad_noagrupado" => "Reporte Rendimiento de Actividades",
            "rpt_rendimiento_actividad" => "Reporte Rendimiento de Disposiciones Generales",
            //"rpt_rendimiento_direccion" => "Reporte Rendimiento por Direcciones",
            "rpt_rendimiento_actividad_sin_avance" => "Reporte Disposiciones Generales Sin Avance",
            //"rpt_direccion_sin_avance_detallado" => "Reporte Disposiciones Generales Sin Avance",
            "rpt_rendimiento_actividad_vencidas" => "Reporte Disposiciones Generales Vencidas",
            "rpt_rendimiento_actividad_cien" => "Reporte Rendimiento de Disposiciones Generales al 100%",
            "rpt_graficos" => 'Resumen de Disposiciones Generales - Gráficos'
        );
        $formatosjs = array(
            //"rpt_rendimiento_actividad_noagrupado" => '{"html":"Tabla HTML","pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_actividad" => '{"pdf":"PDF","xls":"XLS"}',
            //"rpt_rendimiento_direccion" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_actividad_sin_avance" => '{"pdf":"PDF","xls":"XLS"}',
            //"rpt_direccion_sin_avance_detallado" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_actividad_vencidas" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_actividad_cien" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_graficos" => '{"graficos":"Gráficos"}'
            //,"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            //"rpt_rendimiento_actividad_noagrupado" => '{"completo":"Completo","informe":"Informe"}',
            //"rpt_rendimiento_actividad" => '{"completo":"Completo","informe":"Informe","observa":"Informe con Observación"}',
            "rpt_rendimiento_actividad" => '{"completo":"Completo"}',
            //"rpt_rendimiento_direccion" => '{"completo":"Completo"}',
            "rpt_rendimiento_actividad_sin_avance" => '{"completo":"Completo"}',
            //"rpt_direccion_sin_avance_detallado" => '{"completo":"Completo","informe":"Informe"}',
            "rpt_rendimiento_actividad_vencidas" => '{"completo":"Completo"}',
            "rpt_rendimiento_actividad_cien" => '{"completo":"Completo"}',
            "rpt_graficos" => '{"informe":"Informe"}'
        );
        //show($objects);
        $objects[0]->Valor = $this->escoja + $direcciones;
        $objects[1]->Valor = $this->escoja + $prioridad;

        //$objects[3]->Valor=["-1"=>"Sin Redimiento"]+$rendi;
        $objects[2]->Valor = $rendi;
        $objects[2]->ValorAnterior = 0;
        $objects[3]->Valor = $rendi;
        $objects[3]->ValorAnterior = 100;

        $objects[4]->Valor = $rendi;
        $objects[4]->ValorAnterior = 0;
        $objects[5]->Valor = $rendi;
        $objects[5]->ValorAnterior = 100;

        $objects[6]->Valor = $fechaini;
        $objects[7]->Valor = $fechafin;

        $objects[8]->Valor = $fechadesde;
        $objects[9]->Valor = $fechahasta;
        $objects[10]->Valor = $this->escoja + $estadoobras;
        $objects[11]->Valor = $namerepor;
        //$objects[12]->Valor = ["html" => "Tabla HTML"] + $this->formatos;
        $objects[13]->Valor = $this->formatos;
        $objects[12]->Valor = $this->escoja + TipoActividadPoaModel::where('estado', 'ACT')->pluck('tipo_actividad', 'id')->all();
        //$objects[13]->Valor = ["completo" => "Completo", "informe" => "Informe","observa"=>"Informe con Observación"];
        $objects[14]->Valor = ["completo" => "Completo"];
        /**/
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        //show($params);
        $validate_js = [];
        $general_config = ['REPORTE DE DISPOSICIONES GENERALES', 'reportescoordinacionactividades', 'ajaxreportehtmlfiltrosactividades'];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $fechaactu = date("Y-m-d");
        $funcionjs = "
        <script type='text/javascript'> 
        function titurepor()
            {                
                
                var titu=$('#namereporte').find('option:selected').text()+':';
                var pri =  $('#PRIORIDAD').val();
                var esta = $('#ESTADO').val();
                var renmin = $('#RENDMIENTOINI').val();
                var renmax = $('#RENDMIENTOFIN').val();
                var direccion = $('#IDDIRECCION').val();
                var tipo_reporte = $('#namereporte').val();
                var avanceini =$('#AVANCEINI').val();
                var avancefin =$('#AVANCEFIN').val();
                /**/
                var fechactudesde =$('#FECHAACTUDESDE').val();
                var fechactuhasta =$('#FECHAACTUHASTA').val();
                var fechafintxt=$('#FECHAFINAL').val();
                var fe='" . date("m") . "/" . date("d") . "/" . date("Y") . "'
                var fechaactu=new Date(fe)
                
                var array_fecha = fechafintxt.split('-');
                var dia=parseInt(array_fecha[0]);
                var mes=parseInt(array_fecha[1]);
                var ano=parseInt(array_fecha[2]);
                //console.log(ano +' '+mes+' '+dia);
                var fechafin = new Date(dia+'/'+mes+'/'+ano)
                var txtvenci=' rendimiento '+renmin+ '-'+renmax+' % ';
                console.log('Fechas');
                console.log(fechafin);
                console.log(fechaactu);
                //if(fechafin<fechaactu)                
                if(fechafin<fechaactu && parseInt(renmax)<100)
                {
                    txtvenci=' vencidas ';
                    titu=titu.replace(':','');
                    pri='';
                    esta='';
                    direccion='';
                }else{
                /**/
                    if(pri!='0')
                        pri = ' prioridad '+pri+',';
                    else
                        pri='';
                    if(esta!='0')
                        esta = ' estado '+esta+',';
                    else
                        esta='';
                    if(direccion!='0')
                        direccion=' de '+ $('#IDDIRECCION').find('option:selected').text();
                    else
                        direccion='';
                }
                if (tipo_reporte == 'rpt_direccion_sin_avance_detallado' || tipo_reporte == 'rpt_direccion_sin_avance')
                    $('#tituloreporte').val(titu+pri+esta+' rendimiento 0% '+ direccion);
                else if(tipo_reporte == 'rpt_rendimiento_actividad_sin_avance' || tipo_reporte=='rpt_rendimiento_actividad_vencidas' || tipo_reporte=='rpt_rendimiento_actividad_cien')
                {
                    titu=titu.replace(':','');
                    if(tipo_reporte == 'rpt_rendimiento_actividad_sin_avance')
                        titu =titu+ ' semana del '+fechactudesde+' al '+ fechactuhasta;
                    $('#tituloreporte').val(titu);
                }
                else if(parseInt(avanceini)==100 && parseInt(avancefin)==100)
                {
                    titu=titu.replace(':','');
                    $('#tituloreporte').val(titu+' al 100% de avance');
                }
                else{
                    titu=titu.replace(':','');
                    var ti2= '('+pri.trim()+esta.trim()+txtvenci.trim()+')';
                    if(direccion.trim()=='')
                        titu=titu+' ';
                    else
                        direccion=direccion + ' ';
                    $('#tituloreporte').val(titu+direccion+ti2);
                }
            }        
          $(document).ready(function() {
                        
            $('#namereporte').change(function(event) {                
                return titurepor();
            });
            $('#namereporte').trigger('change');
            $('#PRIORIDAD').change(function(event) {
               return titurepor();
            });
            $('#ESTADO').change(function(event) {
                return titurepor();
            });
            $('#IDDIRECCION').change(function(event) {
                return titurepor();
            });
            $('#RENDMIENTOINI').change(function(event) {
                return titurepor();
            });
            $('#RENDMIENTOFIN').change(function(event) {
                return titurepor();
            });
            $('#IDDIRECCION').change(function(event) {
                return titurepor();
            });
          });
        </script>
        ";
        $funcalljs = "titurepor();";

        // $objetos_reportes=json_decode($this->reportes_objeto);
        // $objetos_reportes[0]->Valor=$this->reportes;
        // $objects=insertarinarray($objects,$objetos_reportes[0],0);
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor,
            "funcalljs" => $funcalljs
        ]);
        //Crypt::decrypt
    }

    public function ajaxreportehtmlfiltrosactividades()
    {
        return $this->DatosActividad->ajaxreportehtmlfiltrosactividades();
    }


    public function reportegraficosfiltrosactividades()
    {


        $datostodos = $this->DatosActividad->ajaxreportehtmlfiltrosactividades()->getData();
        //show($datostodos);

        $datos = collect($datostodos['tabla']);

        // $escoja = array(0 => "TODOS");
        // $objetos = '[{"Tipo":"select","Descripcion":"Tipo de Filtro","Nombre":"tipofiltro","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';

        // $objetosTable = '[


        //     {"Tipo":"text","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        //             {"Tipo":"text","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        //             {"Tipo":"text","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        //             {"Tipo":"text","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        //             {"Tipo":"text","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        //             {"Tipo":"text","Descripcion":"Avance","Nombre":"avance","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        //             {"Tipo":"text","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        //             {"Tipo":"text","Descripcion":"Ultima Modificacion","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }

        //             ]';

        // $configuraciongeneral = array("REPORTE ESTADÍSTICO DE LAS ACTIVIDADES", "coordinacioncronograma/actividades", "index", "actividadesdatos");

        // $objetos = json_decode($objetos);
        // //show($this->objetos);
        // $objetos[0]->Nombre = "tipofiltro";
        // $tipoFiltro = array(1 => "DIRECCION", 2 => "ESTADO", 3 => "PRIORIDAD");

        // $objetos[0]->Valor = $escoja + $tipoFiltro;
        // $objetosTable = json_decode($objetosTable);
        // $objetos = array_values($objetos);
        // $delete = 'si';
        // $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        // if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
        //     $delete = null;
        // }

        // $date = Carbon::now();
        // $endDate = $date->subDay(7);
        // $fin = $endDate->toDateString();
        // $date = Carbon::now();
        // $inicio = $date->toDateString();
        // $fin2 = date('Y-m-d', strtotime($inicio));
        // $inicio2 = date('Y-m-d', strtotime($fin));

        // $filtros = '[


        //             {"Tipo":"fecha","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        //             {"Tipo":"fecha","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        //             {"Tipo":"select","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        //             {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }


        //             ]';

        // $filtros = json_decode($filtros);
        // // dd($filtros);
        // $direciones = CoorCronogramaCabModel::select(DB::raw("dir.id,direccion"))
        //     ->join("tmae_direcciones as dir", "dir.id", "=", "coor_tmov_cronograma_cab.id_direccion")
        //     ->where([["coor_tmov_cronograma_cab.estado", "ACT"]])
        //     ->groupBy("coor_tmov_cronograma_cab.id_direccion")
        //     ->orderby("direccion", "asc")
        //     ->pluck('direccion', 'id')
        //     ->all();


        // // dd($direciones);


        // $filtros[0]->Valor = '2019-01-01';
        // $filtros[1]->Valor = date('Y-m-d');
        // $filtros[2]->Valor = $direciones;
        // $estados = explodewords(ConfigSystem("estadoobras"), "|");
        // $filtros[3]->Valor = $estados;


        // return view('vistas.dashboard.dasboardpoa_2', [
        //     "objetos" => $objetos,
        //     "estados" => $estados,
        //     "filtros" => $filtros,
        //     "objetosTable" => $objetosTable,
        //     "inicio_date" => $inicio2,
        //     "inicio_fin" => $fin2,
        //     "menu"=>'NO',

        //     "configuraciongeneral" => $configuraciongeneral,
        //     "create" => 'si',
        //     "delete" => $delete,
        // ]);


        //por direciones
        $direcciones = $datos->groupBy("direccion");
        $totalatrazadas = array();
        //para saber cuantas tienen atrazadas
        foreach ($direcciones as $key => $item) {
            $totalatrazadas[] = [$key, $item->count()];
        }


        //->count("id");
        //show($diretotal);
        $total = $datos->count();
        $porestados = $datos->groupBy('estado_actividad');
        $pordireciones = $datos->groupBy('alias');

        $graficopastel = json_encode($totalatrazadas);


        $totaldireciones = $pordireciones->count();
        $rendimientos = collect($datostodos['rendimiento']);


        //show($rendimientos);


        //show($pordireciones);
        $direccion = '';
        $EJECUCION = 0;
        $EJECUTADO = 0;
        $EJECUTADO_VENCIDO = 0;
        $POR_EJECUTAR = 0;
        $SUSPENDIDO = 0;
        $html = "<script>
        c3.generate({
            bindto:'#grafico',
            zoom: {
            rescale: true
            },
            data: {
                columns: [
                ";

        foreach ($pordireciones as $key => $valor) {

            $direccion = $valor[0]->direccion;

            $EJECUCION = $valor->where("estado_actividad", "EJECUCION")->count();
            $EJECUTADO = $valor->where("estado_actividad", "EJECUTADO")->count();
            $EJECUTADO_VENCIDO = $valor->where("estado_actividad", "EJECUCION_VENCIDO")->count();
            $POR_EJECUTAR = $valor->where("estado_actividad", "POR_EJECUTAR")->count();
            $SUSPENDIDO = $valor->where("estado_actividad", "SUSPENDIDO")->count();

            $html .= "['$direccion',$EJECUCION,$EJECUTADO,$EJECUTADO_VENCIDO,$POR_EJECUTAR,$SUSPENDIDO],";
        }
        $html .= "
                ],
                type : 'area-spline',
            },
            transition: {
                duration: 1000
            },
                legend: {
                        show: false
                    },
                    size: {
                       // height: 600
                    },                    
                axis: {
                        rotated: false,
		                x : {     
                        type: 'category',
                        categories:  ['EJECUCIÓN', 'EJECUTADO', 'EJECUCIÓN VENCIDO', 'POR EJECUTAR', 'SUSPENDIDO']
                },
				tick: {
                        x:{                  
                                    multiline:false,       
                                    culling: {
                                        max: 1
                                    },
                            },
                            label : {
                                text: 'Estados',
                                position: 'center-bottom',
                                
                            }
                        },
                        y: {
                            label : {
                                text: 'Cantidad',
                                position: 'outer-middle',
                            },
                          
                        }
                    },
                    count:2,
            });
            c3.generate({
                bindto:'#graficopor',
                
                data: {
                    columns: [
                       ";

        $html .= "['EJECUCIÓN',$EJECUCION,0,0,0,0],['EJECUTADO',0,$EJECUTADO,0,0,0],['EJECUCIÓN VENCIDO',0,0,$EJECUTADO_VENCIDO,0,0],[0,0,0,$POR_EJECUTAR,0],[0,0,0,0,$SUSPENDIDO]";

        $html .= "          
                    ],
                    type : 'donut',
                    onclick: function (d, i) { },
                    onmouseover: function (d, i) { console.log('onmouseover', d, i); },
                    onmouseout: function (d, i) { console.log('onmouseout', d, i); }
                },
                transition: {
                    duration: 3000
                },
                donut: {
                    title: 'ACTIVIDADES'
                },
                tooltip: {
                   // grouped: true,
                    format: {
                                    title: function (x, index) { return index; },
                                    value: function (value, ratio, id, index) { return value; }
                                }
                }
            });
    
            Highcharts.chart('rendimiento', 
                { chart: {
                    type: 'bar',
                    height: 700,
                //width:100,    
                },
                plotOptions: {
                        series: {
                            colorByPoint: true
                        }
                    },
            
                title: {
                    text: 'RENDIMIENTO POR DIRECCIÓN'
                },
                subtitle: {
                    text: 'GAD MANTA'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        //rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Rendimiento (%)'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Rendimiento: <b>{point.y:.1f} %</b>'
                },
                series: [{
                    name: 'Direcciones',
                    data:[";

        foreach ($rendimientos as $key => $rendi) {
            $html .= "['$rendi->alias',$rendi->Rendimiento],";
        }
        $html .= "],
                    dataLabels: {
                        enabled: true,
                        //rotation: -90,
                        //color: '#FFFFFF',
                        //align: 'right',
                        format: '{point.y:.2f} %', // one decimal
                        /*y: 3, // 10 pixels down from the top
                        
                        style: {
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }*/
                    }
                }],    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    layout: 'horizontal'
                },
                yAxis: {
                    labels: {
                        align: 'left',
                        x: 0,
                        y: -5
                    },
                    title: {
                        text: null
                    }
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: false
                }
            }
        }]
    }
                    
                });

                Highcharts.chart('pastel', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'ACTIVIDADES SIN RENDIMIENTO POR DIRECCIONES '
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
        style: {
          color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
        }
      }
    }
  },
  series: [{
    name: 'ACTIVIDADES',
    colorByPoint: true,
    data: " . $graficopastel . "
  }]
});
        </script>";

        $html .= "
<h2 style='text-align: center;'>RESUMEN DE ACTIVIDADES</h2>
        <div class='row'>
    <div class='col-lg-10'>
            <div class='float-e-margins'>
                <h4>NÚMERO TOTAL DE ACTIVIDADES <span class='badge' style='font-size: 1em;'>" . $total . "</span></h4>
                <div class='ibox-content'>
                    <div class='row'>
                    <table cellspacing='0' cellpadding='0' width='100%'>
                    <tr>
            ";
        foreach ($porestados as $key => $estados) {
            $total_estado = $estados->count();
            $porcentaje = round($total_estado / $total * 100, 2);
            $html .= "
<td>
            <div>
                <div class='ibox float-e-margins' s>
                    <div class='ibox-title'>
                    <span class='label pull-right' style='background-color:" . colortimelineestado($key) . "; color:#FFFFFF;'>     </span>
                        <h5>" . str_replace('_', ' ', $key) . "</h5>
                    </div>
                    <div class='ibox-content'>
                        <h1 class='no-margins'>" . $total_estado . "</h1>
                        <span class=' label pull-right stat-percent font-bold ' style='background-color:" . colortimelineestado($key) . "; color:#black'>" . $porcentaje . "% <i class='fa fa-bolt'></i></span>
                        <small>" . str_replace('_', ' ', $key) . "</small>
                    </div>
                </div>
            </div>
            </td>";
        }
        $html .= " 
 </tr>
 </table>          
                        </div>
                    </div>
                </div>
            </div>

        ";
        //show(Input::get('RENDMIENTOFIN'));
        if (Input::get('RENDMIENTOFIN') == 0) {
            $display = 'display : none;';
            $displaypastel = '';
        } else {
            $display = '';
            $displaypastel = 'display : none;';
        }
        if ($totaldireciones == 1 || $totaldireciones == 0) {
            $html .= "
            <div class='col-lg-6'>
                <div class='ibox float-e-margins'>
                    <div class='ibox-title'>
                        <h5> GRÁFICO DE ACTIVIDADES </h5>
                    </div>
                    <div class='ibox-content'>
                        <div>
                            <div id='grafico'></div>
                        </div>
                    </div>    
                </div>
            </div>                
            <div class='col-lg-6'>
                <div class='ibox float-e-margins'>
                    <div class='ibox-title'>
                        <h5> GRÁFICO DE ACTIVIDADES POR DIRECCIÓN </h5>
                    </div>
                    <div class='ibox-content'>
                        <div>
                            <div id='graficopor'></div>
                        </div>
                    </div>    
                </div>
            </div>
        ";
        } else {
            $html .= "
            <div class='row' style='" . $display . "'>
            <div class='col-lg-10'>
                <div class='float-e-margins'>                    
                    <div class='ibox-content'>
                        <div>
                            <div id='grafico'></div>
                        </div>
                    </div>    
                </div>
            </div>
            </div>
        ";
        }
        $html .= " 
            
        <div class='row' style='" . $display . "'>
        <div class='col-lg-10'>
        <div class='ibox float-e-margins'>
                <div class='ibox-content'>
                <div>
                    <div id='rendimiento'></div>
                </div>
            </div>    
        </div>
    </div></div>
    <div class='row' style='" . $displaypastel . "'>
        <div class='col-lg-10'>
        <div class='ibox float-e-margins'>
                <div class='ibox-content'>
                <div>
                    <div id='pastel'></div>
                </div>
            </div>    
        </div>
    </div></div>
    </div>";

        return $html;
    }


    /*============================================================*/
    public function reportescoordinacionobras()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        //Objetos a Armar
        //FormHidden
        $objects = json_decode('[
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Parroquia", "Nombre": "PARROQUIA", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Obra", "Nombre": "IDTIPOOBRA", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select-multiple", "Descripcion": "Estado", "Nombre": "ESTADO", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Contratista", "Nombre": "CONTRATISTA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Ancho":12},
            {"Tipo": "select", "Descripcion": "Rendimiento Mínimo", "Nombre": "RENDMIENTOINI", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Rendimiento Máximo", "Nombre": "RENDMIENTOFIN", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha hasta", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Actualización Desde", "Nombre": "FECHAACTUDESDE", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Actualización Hasta", "Nombre": "FECHAACTUHASTA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},            
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"}
        ]');
        /*COMBOS*/
        $parroquia = parroquiaModel::where("estado", "ACT")->pluck("parroquia", "id")->all();
        $tipoobra = CoorTipoObraModel::where("estado", "ACT")->pluck("tipo", "id")->all();
        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $contratista = CoorContratistaModel::where("estado", "ACT")->pluck("nombres", "id")->all();

        $fechaini = "2019-05-01";
        $fechafin = date('Y') . "-12-31";
        /**/
        $fechahoy = new DateTime();
        $fechahoy->modify('-1 week');
        $fechadesde = "2019-01-01";
        // $fechadesde = $fechahoy->format('Y-m-d');
        $fechahoy->modify('this week +11 days');
        $fechahasta = date('Y') . "-12-31"; //$fechahoy->format('Y-m-d');
        /**/
        $rendi = array();
        for ($i = 0; $i <= 100; $i += 5) {
            # code...
            $rendi[$i] = $i;
        }
        //TipoNameReporte
        $namerepor = array(
            "rpt_rendimiento_obras_detalle" => "Rendimiento Obras Detallado",
            "rpt_rendimiento_obras_graficos" => "Resumen de Obras - Gráficos",
        );
        $formatosjs = array(
            "rpt_rendimiento_obras_detalle" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_rendimiento_obras_graficos" => '{"graficos-obras":"Gráfico"}',

        );
        $tiporepor = array(
            "rpt_rendimiento_obras_detalle" => '{"completo":"Completo","informe":"Informe"}',
            "rpt_rendimiento_obras_graficos" => '{"completo":"Completo"}'
        );
        $direcciones = direccionesModel::where(["estado" => "ACT"])->wherein("id", [16, 26, 33])->pluck("direccion", "id")->all();
        $objects[0]->Valor = $this->escoja + $direcciones;
        $objects[1]->Valor = $this->escoja + $parroquia;
        $objects[2]->Valor = $this->escoja + $tipoobra;
        $objects[3]->Valor = $this->escoja + $estadoobras;
        $objects[4]->Valor = $this->escoja + $contratista;
        $objects[5]->Valor = $rendi;
        $objects[5]->ValorAnterior = 0;
        $objects[6]->Valor = $rendi;
        $objects[6]->ValorAnterior = 100;
        $objects[7]->Valor = $fechaini;
        $objects[8]->Valor = $fechafin;
        $objects[9]->Valor = $fechadesde;
        $objects[10]->Valor = $fechahasta;
        $objects[11]->Valor = $namerepor;
        $objects[12]->Valor = $this->formatos;
        $objects[13]->Valor = ["completo" => "Completo", "informe" => "Informe"];
        /**/
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        //show($params);
        $validate_js = [];
        $general_config = ['REPORTE DE OBRAS', 'reportescoordinacionobras', 'ajaxreportehtmlfiltrosactividades'];
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "
        <script type='text/javascript'>        
          $(document).ready(function() {
            function titurepor()
            {
                var titu=$('#namereporte').find('option:selected').text()+':';
                var parroquia = $('#PARROQUIA').val();
                var tipoobra =  $('#IDTIPOOBRA').val();
                var esta = $('#ESTADO').val();
                var contra =  $('#CONTRATISTA').val();
                var renmin = $('#RENDMIENTOINI').val();
                var renmax = $('#RENDMIENTOFIN').val();

                if(esta!='0')
                    esta = ' estado '+esta+',';
                else
                    esta='';
                if(parroquia!='0')
                    parroquia=' Parroquia '+ $('#PARROQUIA').find('option:selected').text()+',';
                else
                    parroquia='';
                if(tipoobra!='0')
                    tipoobra=' Tipo Obra: '+ $('#IDTIPOOBRA').find('option:selected').text()+',';
                else
                    tipoobra='';
                if(contra!='0')
                    tipoobra=' '+ $('#CONTRATISTA').find('option:selected').text()+'+';
                else
                    contra='';
                $('#tituloreporte').val(titu+parroquia+esta+' rendimiento '+renmin+ '-'+renmax+' %.'+tipoobra);
            }             
            $('#namereporte').change(function(event) {                
                return titurepor();
            });
            $('#namereporte').trigger('change');
            $('#PARROQUIA').change(function(event) {
               return titurepor();
            });
            $('#IDTIPOOBRA').change(function(event) {
                return titurepor();
            });
            $('#ESTADO').change(function(event) {
                return titurepor();
            });
            $('#CONTRATISTA').change(function(event) {
                return titurepor();
            });
            $('#RENDMIENTOINI').change(function(event) {
                return titurepor();
            });
            $('#RENDMIENTOFIN').change(function(event) {
                return titurepor();
            });            
          });
        </script>
        ";
        // $objetos_reportes=json_decode($this->reportes_objeto);
        // $objetos_reportes[0]->Valor=$this->reportes;
        // $objects=insertarinarray($objects,$objetos_reportes[0],0);

        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "tiporepor" => $tiporepor,
            "funcionjs" => $funcionjs
        ]);
        //Crypt::decrypt
    }

    public function reportegraficosfiltrosObras()
    {


        $datostodos = DB::select(queryobras());
        $datos = collect($datostodos);
        $total = $datos->count();
        $porestados = $datos->groupBy('estado_obra');
        $totalparroquias = $datos->groupBy('parroquia')->count();
        //show($datos);

        $parroquia = '';
        $EJECUCION = 0;
        $EJECUTADO = 0;
        $EJECUTADO_VENCIDO = 0;
        $POR_EJECUTAR = 0;
        $SUSPENDIDO = 0;

        $latitud = "-0.9643838581222217";
        $longitud = "-80.73529698377075";
        $autozoom = "13";
        $actionclick = '';
        $config['center'] = "$latitud,$longitud";
        $config['zoom'] = $autozoom; //'auto';
        $config['onclick'] = $actionclick;
        $config['map_height'] = "600px";
        Map::initialize($config);

        foreach ($datos as $d) {

            //show($datos);
            $latitud = $d->latitud;
            $longitud = $d->longitud;
            $autozoom = "15";
            $actionclick = '';
            if ($latitud == "") {
                $latitud = "-0.9484126160642922";
            }
            if ($longitud == "")
                $longitud = "-80.72165966033936";
            $marker = array();
            $marker['position'] = "$latitud,$longitud";
            $marker['draggable'] = false;
            $marker['infowindow_content'] = $d->nombre_obra;
            // $marker['icon'] = $d->avance."%";
            $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=1.4|0|' . str_replace('#', '', colortimelineestado($d->estado_obra)) . '|10|b|' . $d->avance . "%";
            $marker['onclick'] = $actionclick;
            Map::add_marker($marker);
        }
        //  show($datos);
        $map = Map::create_map();


        //show($map);

        $html = "<script>
        c3.generate({
            bindto:'#grafico',
            zoom: {
            rescale: true
            },
            data: {
                columns: [
                ";

        foreach ($porestados as $key => $valor) {

            $parroquia = $valor[0]->parroquia;

            $EJECUCION = $valor->where("estado_obra", "EJECUCION")->count();
            $EJECUTADO = $valor->where("estado_obra", "EJECUTADO")->count();
            $EJECUTADO_VENCIDO = $valor->where("estado_obra", "EJECUCION_VENCIDO")->count();
            $POR_EJECUTAR = $valor->where("estado_obra", "POR_EJECUTAR")->count();
            $SUSPENDIDO = $valor->where("estado_obra", "SUSPENDIDO")->count();

            $html .= "['$parroquia',$EJECUCION,$EJECUTADO,$EJECUTADO_VENCIDO,$POR_EJECUTAR,$SUSPENDIDO],";
        }
        $html .= "
                ],
                type : 'area-spline',
            },
            transition: {
                duration: 1000
            },
                legend: {
                        show: false
                    },
                    size: {
                       // height: 600
                    },                    
                axis: {
                        rotated: false,
		                x : {     
                        type: 'category',
                        categories:  ['EJECUCIÓN', 'EJECUTADO', 'EJECUCIÓN VENCIDO', 'POR EJECUTAR', 'SUSPENDIDO']
                },
				tick: {
                        x:{                  
                                    multiline:false,       
                                    culling: {
                                        max: 1
                                    },
                            },
                            label : {
                                text: 'Estados',
                                position: 'center-bottom',
                                
                            }
                        },
                        y: {
                            label : {
                                text: 'Cantidad',
                                position: 'outer-middle',
                            },
                          
                        }
                    },
                    count:2,
            });
            c3.generate({
                bindto:'#graficopor',
                
                data: {
                    columns: [
                       ";

        $html .= "['EJECUCIÓN',$EJECUCION,0,0,0,0],['EJECUTADO',0,$EJECUTADO,0,0,0],['EJECUCIÓN VENCIDO',0,0,$EJECUTADO_VENCIDO,0,0],[0,0,0,$POR_EJECUTAR,0],[0,0,0,0,$SUSPENDIDO]";

        $html .= "          
                    ],
                    type : 'donut',
                    onclick: function (d, i) { },
                    onmouseover: function (d, i) { console.log('onmouseover', d, i); },
                    onmouseout: function (d, i) { console.log('onmouseout', d, i); }
                },
                transition: {
                    duration: 3000
                },
                donut: {
                    title: 'OBRAS'
                },
                tooltip: {
                   // grouped: true,
                    format: {
                                    title: function (x, index) { return index; },
                                    value: function (value, ratio, id, index) { return value; }
                                }
                }
            });
            
        $(document).ready(function () {        
            //document.write();
        });
            
        </script>
        ";
        $mapita = $map['js'];
        $mapita = str_replace('https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCp0wkPAEubZZb6s09ajbOv3a93wHocW6k&', "", $mapita);
        $mapita = str_replace('<script type="text/javascript" src=""></script>', "", $mapita);

        $html .= "
<h2 style='text-align: center;'>RESUMEN DE OBRAS</h2>
        <div class='row'>
    <div class='col-lg-12'>
            <div class='ibox float-e-margins'>
                <div class='ibox-title'>
                    <span class='label label-primary pull-right'>HOY</span>
                    <h3>NÚMERO TOTAL DE OBRAS <span class='badge' style='font-size: 1em;'>" . $total . "</span></h3>
                </div>
                <div class='ibox-content'>
                    <div class='row'>
                    <table cellspacing='0' cellpadding='0' width='100%'>
                    <tr>
            ";
        foreach ($porestados as $key => $estados) {
            $total_estado = $estados->count();
            $porcentaje = round($total_estado / $total * 100, 2);
            $html .= "
<td>
            <div>
                <div class='ibox float-e-margins'>
                    <div class='ibox-title'>
                    <span class='label pull-right' style='background-color:" . colortimelineestado($key) . "; color:#FFFFFF;'>     </span>
                        <h5>" . str_replace('_', ' ', $key) . "</h5>
                    </div>
                    <div class='ibox-content'>
                        <h1 class='no-margins'>" . $total_estado . "</h1>
                        <span class=' label pull-right stat-percent font-bold ' style='background-color:" . colortimelineestado($key) . "; color:#black'>" . $porcentaje . "% <i class='fa fa-bolt'></i></span>
                        <small>" . str_replace('_', ' ', $key) . "</small>
                    </div>
                </div>
            </div>
           </td>";
        }
        $html .= "  </tr>
  </table>         
                        </div>
                    </div>
                </div>
            </div>

        ";

        if ($totalparroquias == 1 || $totalparroquias == 0) {
            $html .= "
            <div class='col-lg-6'>
                <div class='ibox float-e-margins'>
                    <div class='ibox-title'>
                        <h5> GRÁFICO DE OBRAS </h5>
                    </div>
                    <div class='ibox-content'>
                        <div>
                            <div id='grafico'></div>
                        </div>
                    </div>    
                </div>
            </div>                
            <div class='col-lg-6'>
                <div class='ibox float-e-margins'>
                    <div class='ibox-title'>
                        <h5> GRÁFICO DE ACTIVIDADES POR PARROQUIA </h5>
                    </div>
                    <div class='ibox-content'>
                        <div>
                            <div id='graficopor'></div>
                        </div>
                    </div>    
                </div>
            </div>
        ";
        } else {
            $html .= "
            <div class='col-lg-12' style='display: none !important;'>
                <div class='ibox float-e-margins'>
                    <div class='ibox-title'>
                        <h5> GRÁFICO DE OBRAS </h5>
                    </div>
                    <div class='ibox-content'>
                        <div>
                        <div id='grafico'></div>
                        </div>
                    </div>    
                </div>
            </div>
        ";
        }

        $html .= "
<!-- <div class='page-break'></div>--><!-- Indica que va nueva pagina -->
<div class='col-lg-12'>
        <div class='ibox float-e-margins'>
            <div class='ibox-title'>
                <h5> GRÁFICO DE OBRAS </h5>
            </div>
            <div class='ibox-content'>
                <div>
                <div>
                " . $map['html'] . $mapita . "<script>initialize_map();</script>                
                </div>
                </div>
            </div>    
        </div>
    </div> </div>";
        return $html;
        //echo $mapita;

        //echo $mapita.$map['html'];
        //return view("prueba",["html"=>$html,"map"=>$map,"mapita"=>$mapita]);


    }

    public function reportesrecomendaciones()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        $objects = json_decode('[
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null","Ancho":12},
            {"Tipo": "select-multiple", "Descripcion": "Estado", "Nombre": "ESTADO", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Documento", "Nombre": "TIPODOCUMENTO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Actualización desde", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Actualización hasta", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        /*COMBOS*/
        $direcciones = direccionesModel::join("coor_tmae_recomendacion_contraloria as a", "a.id_direccion", "=", "tmae_direcciones.id")
            ->select("tmae_direcciones.direccion", "tmae_direcciones.id", DB::raw("count(*) as total"))
            ->groupBy("tmae_direcciones.direccion", "tmae_direcciones.id")
            ->orderby("tmae_direcciones.direccion")
            ->pluck("direccion", "id")
            ->all();
        $estado = explodewords(ConfigSystem("estadoobras"), "|");
        $tipodocumento = CabRecomendacionContraModel::select("tipo_documento", DB::raw("count(*) as total"))
            ->groupBy("tipo_documento")
            ->orderby("tipo_documento")
            ->pluck("tipo_documento", "tipo_documento")
            ->all();
        $fechaini = "2019-05-01";
        $fechafin = date('Y') . "-12-31";
        //TipoNameReporte
        $namerepor = array(
            "rpt_recomendaciones_contraloria" => "Reporte Recomendaciones de Contraloria"
        );
        $formatosjs = array(
            "rpt_recomendaciones_contraloria" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_recomendaciones_contraloria" => '{"completo":"Completo"}'
        );
        $objects[0]->Valor = $this->escoja + $direcciones;
        $objects[1]->Valor = $this->escoja + $estado;
        $objects[2]->Valor = $this->escoja + $tipodocumento;
        $objects[3]->Valor = $namerepor;
        $objects[4]->Valor = $fechaini;
        $objects[5]->Valor = $fechafin;
        $objects[6]->Valor = $this->formatos;
        $objects[7]->Valor = ["completo" => "Completo"];
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        $validate_js = [];
        $general_config = ['REPORTE RECOMENDACIONES DE CONTRALORIA', 'reportesrecomendaciones', ''];
        $objects[8]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        $funcalljs = "";
        // $objetos_reportes=json_decode($this->reportes_objeto);
        // $objetos_reportes[0]->Valor=$this->reportes;
        // $objects=insertarinarray($objects,$objetos_reportes[0],0);
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor
        ]);
    }

    /*COMPROMISOS ALCALDIA*/
    public function reportescompromisos()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        $objects = json_decode('[
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null","Ancho":12},
            {"Tipo": "select-multiple", "Descripcion": "Estado", "Nombre": "ESTADO", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Parroquia", "Nombre": "IDPARROQUIA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Prioridad", "Nombre": "PRIORIDAD", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Actualización Desde", "Nombre": "FECHAACTUDESDE", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Actualización Hasta", "Nombre": "FECHAACTUHASTA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        //show($objects);
        /*COMBOS*/
        $direcciones = direccionesModel::join("coor_tmov_cronograma_cab as a", "a.id_direccion", "=", "tmae_direcciones.id")
            ->select("tmae_direcciones.direccion", "tmae_direcciones.id", DB::raw("count(*) as total"))
            ->where("a.prioridad", "COMPROMISOS_ALCALDIA")
            ->groupBy("tmae_direcciones.direccion", "tmae_direcciones.id")
            ->orderby("tmae_direcciones.direccion")
            ->pluck("direccion", "id")
            ->all();

        $estado = explodewords(ConfigSystem("estadoobras"), "|");
        $parroquia = parroquiaModel::where("estado", "ACT")->pluck("parroquia", "id")->all();
        $prioridad = explodewords(ConfigSystem("prioridad"), "|");
        unset($prioridad["COMPROMISOS_ALCALDIA"]);
        unset($prioridad["COMPROMISOS_PLANIFICACION"]);

        $fechaini = "2019-05-01";
        $fechafin = date('Y') . "-12-31";
        $fechahoy = new DateTime();
        // $fechahoy->modify('this week');
        $fechadesde = "2019-01-01";
        // $fechadesde = $fechahoy->format('Y-m-d');
        // $fechahoy->modify('this week +4 days');
        $fechahasta = date('Y') . "-12-31";
        // $fechahasta = $fechahoy->format('Y-m-d');
        //TipoNameReporte
        $namerepor = array(
            "rpt_rendimiento_compromisos" => "Reporte Compromisos de Alcaldía"
        );
        $formatosjs = array(
            "rpt_rendimiento_compromisos" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_rendimiento_compromisos" => '{"completo":"Completo"}'
        );
        $objects[0]->Valor = $this->escoja + $direcciones;
        $objects[1]->Valor = $this->escoja + $estado;
        $objects[2]->Valor = $this->escoja + $parroquia;
        $objects[3]->Valor = $this->escoja + $prioridad;
        $objects[4]->Valor = $fechaini;
        $objects[5]->Valor = $fechafin;
        $objects[6]->Valor = $fechadesde;
        $objects[7]->Valor = $fechahasta;
        //$objects[5]->Valor = $estado;
        $objects[8]->Valor = $namerepor;
        $objects[9]->Valor = $this->formatos;
        $objects[10]->Valor = ["completo" => "Completo"];
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        $validate_js = [];
        $general_config = ['REPORTE COMPROMISOS TERRITORIALES', 'reportescompromisos', ''];
        $objects[11]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        $funcalljs = "";
        // $objetos_reportes=json_decode($this->reportes_objeto);
        // $objetos_reportes[0]->Valor=$this->reportes;
        // $objects=insertarinarray($objects,$objetos_reportes[0],0);
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor
        ]);
    }


    ////////////////////////////////////AGENDA CHRISTIAN//////////////////////////////

    public function reportesagenda()
    {
        if (Input::get("token"))
            return $this->generarreporte();


        $objects = json_decode('[
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Estado", "Nombre": "ESTADO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo de actividad", "Nombre": "TIPO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "datetext", "Descripcion": "Creación Desde", "Nombre": "FECHAACTUDESDE", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Creación Hasta", "Nombre": "FECHAACTUHASTA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        /*COMBOS*/
        $direcciones = AgendaVirtualModel::join("users as u", "u.id", "=", "com_tmov_comunicacion_agenda.id_usuario")
            ->join("tmae_direcciones as tmae_direcciones", "tmae_direcciones.id", "=", "u.id_direccion")
            ->select("tmae_direcciones.direccion", "tmae_direcciones.id", DB::raw("count(*) as total"))
            ->groupBy("tmae_direcciones.direccion", "tmae_direcciones.id")
            ->orderby("tmae_direcciones.direccion")
            ->pluck("direccion", "id")
            ->all();
        $estado = AgendaEstadosModel::where('estado', 'ACT')->pluck('nombre', 'id')->all();

        $fechaini = "2019-05-01";
        $fechafin = date('Y') . "-12-31";
        //TipoNameReporte
        $namerepor = array(
            "rpt_reportesagenda" => "Reporte Agenda Territorial"
        );
        $formatosjs = array(
            "rpt_reportesagenda" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_reportesagenda" => '{"completo":"Completo"}'
        );
        $objects[0]->Valor = $this->escoja + $direcciones;
        $objects[1]->Valor = $this->escoja + $estado;
        $tipo = TipoAgendaModel::where(['estado' => 'ACT'])->pluck('tipo', 'id')->all();
        $objects[2]->Valor = $this->escoja + $tipo;
        $objects[3]->Valor = $namerepor;
        $objects[4]->Valor = $fechaini;
        $objects[5]->Valor = $fechafin;

        $objects[6]->Valor = $fechaini;
        $objects[7]->Valor = $fechafin;

        $objects[8]->Valor = $this->formatos;
        $objects[9]->Valor = ["completo" => "Completo"];
        // show($objects);
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        $validate_js = [];
        $general_config = ['REPORTE AGENDA TERRITORIAL', 'reportesagenda', ''];
        $objects[10]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        $funcalljs = "";
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor
        ]);
    }

    /*Reporte POA*/
    public function reportespoa()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        $objects = json_decode('[
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Estado Proyecto", "Nombre": "ESTADO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Avance Mínimo", "Nombre": "AVANCEINI", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Avance Máximo", "Nombre": "AVANCEFIN", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Actualización Desde", "Nombre": "FECHAACTUDESDE", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Actualización Hasta", "Nombre": "FECHAACTUHASTA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"}, 
            {"Tipo": "select", "Descripcion": "Año", "Nombre": "ANIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}, 
            {"Tipo": "select-multiple", "Descripcion": "Estado Actividad", "Nombre": "ESTADOACTIVIDAD", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},           
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Verificación", "Nombre": "VERIFICACION", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        //show($objects);
        /*COMBOS*/
        $direcciones = direccionesModel::join("poa_tmae_cab_actividad as a", "a.id_direccion_act", "=", "tmae_direcciones.id")
            ->select("tmae_direcciones.direccion", "tmae_direcciones.id", DB::raw("count(*) as total"))
            ->groupBy("tmae_direcciones.direccion", "tmae_direcciones.id")
            ->orderby("tmae_direcciones.direccion")
            ->pluck("direccion", "id")
            ->all();

        $estado = explodewords(ConfigSystem("estadopoa"), "|");
        $estadoactividad = explodewords(ConfigSystem("estadoobras"), "|");
        $prioridad = explodewords(ConfigSystem("prioridad"), "|");
        unset($prioridad["COMPROMISOS_ALCALDIA"]);
        unset($prioridad["COMPROMISOS_PLANIFICACION"]);

        $fechaini = "2019-05-01";
        $fechafin = date('Y') . "-12-31";
        $fechahoy = new DateTime();
        $fechahoy->modify('this week');
        $fechadesde = "2019-01-01"; //$fechahoy->format('Y-m-d');
        $fechahoy->modify('this week +4 days');
        $fechahasta = date('Y') . "-12-31"; //$fechahoy->format('Y-m-d');
        //TipoNameReporte
        $namerepor = array(
            "rpt_poaactividades" => "Reporte General POA"
        );
        $formatosjs = array(
            "rpt_poaactividades" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_poaactividades" => '{"completo":"Completo"}'
        );
        /**/
        $rendi = array();
        for ($i = 0; $i <= 100; $i += 5) {
            # code...
            $rendi[$i] = $i;
        }
        $objects[0]->Valor = $this->escoja + $direcciones;
        $objects[1]->Valor = $this->escoja + $estado;
        $objects[2]->Valor = $rendi;
        $objects[3]->Valor = $rendi;
        $objects[3]->ValorAnterior = 100;
        $objects[4]->Valor = $fechaini;
        $objects[5]->Valor = $fechafin;
        $objects[6]->Valor = $fechadesde;
        $objects[7]->Valor = $fechahasta;
        //$objects[5]->Valor = $estado;
        //$estadoactividad
        $objects[8]->Valor = [2019 => 2019, 2020 => 2020];
        $objects[9]->Valor = $this->escoja + $estadoactividad;
        $objects[10]->Valor = $namerepor;
        $objects[11]->Valor = $this->formatos;
        $verificacion = explodewords(ConfigSystem("verificacion"), "|");
        $objects[12]->Valor = $this->escoja + $verificacion;
        $objects[13]->Valor = ["completo" => "Completo"];
        $params = array();

        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        // show($objects);
        $validate_js = [];
        $general_config = ['REPORTE POA', 'reportescompromisos', ''];
        $objects[14]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        $funcalljs = "";
        // $objetos_reportes=json_decode($this->reportes_objeto);
        // $objetos_reportes[0]->Valor=$this->reportes;
        // $objects=insertarinarray($objects,$objetos_reportes[0],0);
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor
        ]);
    }

    /*Reporte POA*/
    public function reportespoacertificaciones()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        $objects = json_decode('[
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Estado Solicitud", "Nombre": "ESTADO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},      
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        //show($objects);
        /*COMBOS*/
        $direcciones = PoaAutorizacionActiModel::groupBy("direccion_adquiriente")
            ->orderby("direccion_adquiriente")
            ->pluck("direccion_adquiriente", "direccion_adquiriente")
            ->all();

        $estado = [
            'SOLICITADA' => 'SOLICITADA',
            'APROBADA' => 'APROBADA',
            'NEGADA' => 'NEGADA'
        ];


        $fechaini = "2019-05-01";
        $fechafin = date('Y') . "-12-31";
        $fechahoy = new DateTime();
        $fechahoy->modify('this week');
        $fechadesde = "2019-01-01"; //$fechahoy->format('Y-m-d');
        $fechahoy->modify('this week +4 days');
        $fechahasta = date('Y') . "-12-31"; //$fechahoy->format('Y-m-d');
        //TipoNameReporte
        $namerepor = array(
            "rpt_certificaciones" => "Reporte General Certificaciones POA"
        );
        $formatosjs = array(
            "rpt_certificaciones" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_certificaciones" => '{"completo":"Completo"}'
        );
        /**/
        $rendi = array();
        for ($i = 0; $i <= 100; $i += 5) {
            # code...
            $rendi[$i] = $i;
        }
        $objects[0]->Valor = $this->escoja + $direcciones;
        $objects[1]->Valor = $this->escoja + $estado;
        $objects[2]->Valor = $fechaini;
        $objects[3]->Valor = $fechafin;
        $objects[4]->Valor = $namerepor;
        $objects[5]->Valor = $this->formatos;
        $objects[6]->Valor = ["completo" => "Completo"];
        $params = array();

        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        // show($objects);
        $validate_js = [];
        $general_config = ['REPORTE CERTIFICACIONES POA', 'reportescompromisos', ''];
        $objects[7]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        $funcalljs = "";
        // $objetos_reportes=json_decode($this->reportes_objeto);
        // $objetos_reportes[0]->Valor=$this->reportes;
        // $objects=insertarinarray($objects,$objetos_reportes[0],0);
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor
        ]);
    }

    public function reportesplancampania()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        $objects = json_decode('[            
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null","Ancho":12},     
            {"Tipo": "select-multiple", "Descripcion": "Estado", "Nombre": "IDESTADO", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        //show($objects);
        /*COMBOS*/
        $direcciones = direccionesModel::join("plan_tmov_cab_plancampania as a", "a.id_direccion", "=", "tmae_direcciones.id")
            ->select("tmae_direcciones.direccion", "tmae_direcciones.id", DB::raw("count(*) as total"))
            ->groupBy("tmae_direcciones.direccion", "tmae_direcciones.id")
            ->orderby("tmae_direcciones.direccion")
            ->pluck("direccion", "id")
            ->all();
        $fechaini = "2019-05-01";
        $fechafin = "2025-12-31";
        //TipoNameReporte
        $namerepor = array(
            "rpt_plan_campania" => "Reporte General Plan De Propuesta"
        );
        $formatosjs = array(
            "rpt_plan_campania" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_plan_campania" => '{"completo":"Completo"}'
        );
        /**/
        $estadoobras = explodewords(ConfigSystem("estadoobras"), "|");
        $objects[0]->Valor = $this->escoja + $direcciones;
        // $objects[1]->Valor = $this->escoja + ["2019"=>"2019","2020"=>"2020","2021"=>"2021","2022"=>"2022","2023"=>"2023"];
        $objects[1]->Valor = $this->escoja + $estadoobras;
        $objects[2]->Valor = $fechaini;
        $objects[3]->Valor = $fechafin;
        $objects[4]->Valor = $namerepor;
        $objects[5]->Valor = $this->formatos;
        $objects[6]->Valor = ["completo" => "Completo"];
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        $validate_js = [];
        $general_config = ['REPORTE PLAN DE PROPUESTA', 'reportesplanpropuesta', ''];
        $objects[7]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        $funcalljs = "";
        // $objetos_reportes=json_decode($this->reportes_objeto);
        // $objetos_reportes[0]->Valor=$this->reportes;
        // $objects=insertarinarray($objects,$objetos_reportes[0],0);
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor
        ]);
    }


    public function reportesunaser()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        $objects = json_decode('[            
            {"Tipo": "text", "Descripcion": "Cédula", "Nombre": "CEDULA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},     
            {"Tipo": "text", "Descripcion": "Placa", "Nombre": "PLACA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},     
            {"Tipo": "select", "Descripcion": "Tipo de multa", "Nombre": "TIPOMULTA", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null", "Ancho":12},     
            {"Tipo": "select", "Descripcion": "Estado de multa", "Nombre": "ESTADO", "Clase": "chosen", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "datetext", "Descripcion": "Fecha Inicial", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Final", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        //show($objects);
        /*COMBOS*/
        $direcciones = MultasUnaserModel::select(DB::raw("concat(ti.id,' - ',ti.tipo) as tipo"), 'ti.id')
            ->join('una_tmo_multas_tipo as ti', 'ti.id', '=', 'una_tmo_multas.id_tipo_multa')
            ->where('una_tmo_multas.estado', 'ACT')
            ->groupBy('ti.id')->pluck('tipo', 'id')
            ->all();
        $estado_multa = MultasUnaserModel::where('una_tmo_multas.estado', 'ACT')
            ->pluck('estado_multa', 'estado_multa')->all();

        $fechaini = "2019-05-01";
        $fechafin = "2025-12-31";
        //TipoNameReporte
        $namerepor = array(
            "rpt_unaser" => "Reporte Unaser"
        );
        $formatosjs = array(
            "rpt_unaser" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_unaser" => '{"completo":"Completo"}'
        );
        $objects[2]->Valor = $this->escoja + $direcciones;
        $objects[3]->Valor = $this->escoja + $estado_multa;
        $objects[4]->Valor = $fechaini;
        $objects[5]->Valor = $fechafin;
        $objects[6]->Valor = $namerepor;
        $objects[7]->Valor = $this->formatos;
        $objects[8]->Valor = ["completo" => "Completo"];
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        $validate_js = [];
        $general_config = ['REPORTE DE MULTAS UNASER', 'reportesplanpropuesta', ''];
        $objects[9]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor
        ]);
    }

    public function reportesindicadores()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        $objects = json_decode('[            
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select", "Descripcion": "Indicador", "Nombre": "IDINDICADOR", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select", "Descripcion": "Parroquia", "Nombre": "IDPARROQUIA", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select", "Descripcion": "Barrrio", "Nombre": "IDBARRIO", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "FormHidden", "Descripcion": "Nada", "Nombre": "nada", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        //show($objects);
        /*COMBOS*/
        $direcciones = direccionesModel::join("ges_tmov_gestion_indicador as a", "a.id_direccion", "=", "tmae_direcciones.id")
            ->select("tmae_direcciones.direccion", "tmae_direcciones.id", DB::raw("count(*) as total"))
            ->groupBy("tmae_direcciones.direccion", "tmae_direcciones.id")
            ->orderby("tmae_direcciones.direccion")
            ->pluck("direccion", "id")
            ->all();
        $fechaini = "2019-05-01";
        $fechafin = "2025-12-31";
        //TipoNameReporte
        $namerepor = array(
            "rpt_indicadores" => "Reporte Indicadorres De Gestión",
            "rpt_indicadores_totalizado_parroquia" => "Reporte Indicadorres  De Gestión Por Parroquia",
            "rpt_indicadores_totalizado" => "Reporte Indicadorres  De Gestión Totalizados",
            "rpt_indicadores_sin_barrios" => "Reporte  Indicadorres De Gestión - Sin barrios"
        );
        $formatosjs = array(
            "rpt_indicadores" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_indicadores_totalizado_parroquia" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_indicadores_totalizado" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_indicadores_sin_barrios" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_indicadores" => '{"completo":"Completo"}',
            "rpt_indicadores_totalizado_parroquia" => '{"completo":"Completo"}',
            "rpt_indicadores_totalizado" => '{"completo":"Completo"}',
            "rpt_indicadores_sin_barrios" => '{"completo":"Completo"}'
        );
        /**/
        $objects[0]->Valor = $fechaini;
        $objects[1]->Valor = $fechafin;
        $objects[2]->Valor = $this->escoja + $direcciones;
        $objects[3]->Valor = $this->escoja;
        $objects[4]->Valor = $this->escoja + parroquiaModel::where(['estado' => 'ACT'])->pluck('parroquia', 'id')->all();
        $objects[5]->Valor = $this->escoja + $this->escoja + barrioModel::where(['estado' => 'ACT'])->pluck('barrio', 'id')->all();

        // unset($objects[3]);
        // unset($objects[4]);
        // unset($objects[5]);
        $objects[6]->Valor = $namerepor;
        $objects[7]->Valor = $this->formatos;
        $objects[8]->Valor = ["completo" => "Completo"];
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        $validate_js = [];
        $general_config = ['REPORTE INDICADORES DE GESTION', 'reportesindicadores', ''];
        $objects[9]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        $funcalljs = "";

        // $objetos_reportes=json_decode($this->reportes_objeto);
        // $objetos_reportes[0]->Valor=$this->reportes;
        // $objects=insertarinarray($objects,$objetos_reportes[0],0);

        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor,
            "indicadores" => "SI"
        ]);
    }

    public function getTipoPermisos()
    {
        $client = new GuzzleHttpClient();
        $resp_cliente = $client->request('GET', 'https://portalciudadano.manta.gob.ec/getTiposPermisos');
        $respuesta = json_decode($resp_cliente->getBody()->getContents());
        // dd($respuesta);
        // $array_resp = collect($respuesta)->pluck('responsable', 'id');
        // $array_resp->put(0 , 'TODOS');
        // $sort = $array_resp->sortKeys();
        // $sort->all();
        // $array_resp->all();
        return $respuesta;
    }


    public function generarreporte_2()
    {
        // dd();
        $input = Input::all();
        $data = json_decode(Input::get("data"));
        // show($data);      
        //$namedata=Crypt::decrypt(Input::get("namedata"));
        $paramadi = Crypt::decrypt(Input::get("paramadi"));
        // show(array($paramadi,$data));        
        $paramsok = array();
        foreach ($data as $key => $value) {
            # code...
            // if ($value->name == "namereporte"){
            //     dd($value);
            // }

            if ($value->name != "_token" && $value->name != "formato" && $value->name != "namereporte" && $value->name != "tituloreporte" && $value->name != "tiporeporte")
                $paramsok[$value->name] = $value->value;
            elseif ($value->name == "formato")
                $formato = $value->value;
            elseif ($value->name == "namereporte")
                $namedata = $value->value;
            elseif ($value->name == "tituloreporte")
                $paramadi["TITULO"] = mb_strtoupper($value->value);
            elseif ($value->name == "tiporeporte")
                if ($value->value == "informe" || $value->value == "observa")
                    $namedata .= "_" . $value->value;
        }
        // Filtros
        // show($paramsok);
        // dd();
        // show($paramadi);
        //$name, $params, $format, $filename, $download, $download_is_attachment = true
        $filename = "Permisos-" . str_random(6);
        //  show(array($namedata,$paramsok+$paramadi,$formato,$filename,true,false));
        //  show($namedata);
        $ruta = '';

        $param = $paramsok + $paramadi;
        // show($filename);

        foreach ($param as $key => $value) {
            # code...
            $ruta .= $key . '=' . $value . '&';
        }

        // dd($param);

        return $this->ReportJasper->generateReportByJson($namedata, $ruta, $formato, $filename, true, false, 1);
    }

    public function reportespermisos()
    {
        if (Input::get("token"))
            return $this->generarreporte_2();
        $objects = json_decode('[            
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo de permiso", "Nombre": "IDTIPOPERMISO", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select", "Descripcion": "Estado del permiso", "Nombre": "estado", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        //show($objects);
        /*COMBOS*/

        $fecha = date('Y-m-d');
        $nuevafecha = strtotime('-7 day', strtotime($fecha));
        $nuevafecha = date('Y-m-d', $nuevafecha);

        $fechaini = $nuevafecha;
        $fechafin = date('Y-m-d');
        //TipoNameReporte
        $namerepor = array(
            "rpt_permisos" => "Reporte  detallado de permisos",
            "rpt_permisos_" => "Reporte estadistico de permisos"
        );
        $formatosjs = array(
            "rpt_permisos" => '{"html":"Tabla","pdf":"PDF","xls":"XLS"}',
            "rpt_permisos_" => '{"html":"Tabla"}'
        );
        $tiporepor = array(
            "rpt_permisos" => '{"completo":"Completo"}',
            "rpt_permisos_" => '{"completo":"Completo"}'
        );
        /**/
        $objects[0]->Valor = $fechaini;
        $objects[1]->Valor = $fechafin;
        $objects[2]->Valor = [];
        $objects[3]->Valor = ['Aprobados' => 'Aprobados', 'Revision' => 'Revision', 'Negados' => 'Negados'];
        $objects[4]->Valor = $namerepor;
        $objects[5]->Valor = ['html' => 'Tabla'] + $this->formatos;
        // $objects[6]->Valor = ["completo" => "Completo"];
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        $validate_js = [];
        $general_config = ['REPORTE DE PERMISOS', 'reportespermisos', 'reportesplanpropuestaajax'];
        $objects[6]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        $funcalljs = "";
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor,
            "mapa" => 'si'
        ]);
    }

    public function reportesplanpropuestaajax()
    {
        $client = new GuzzleHttpClient();

        $data = Input::all();
        // dd($data);
        // $paramadi = Crypt::decrypt(Input::get("paramadi"));        
        $paramsok = array();
        foreach ($data as $key => $value) {
            //    dd($key);
            $paramsok[$key] = $value;
        }

        $ruta = '';
        $param = $paramsok;
        foreach ($param as $key => $value) {
            # code...
            $ruta .= $key . '=' . $value . '&';
        }
        $resp_cliente = $client->request('GET', 'https://portalciudadano.manta.gob.ec/consultaPermisos?' . $ruta);

        // dd('https://portalciudadano.manta.gob.ec/consultaPermisos?' . $ruta);

        $data_response = $resp_cliente->getBody()->getContents();
        $datos = json_decode($data_response);
        $html = '';
        if (isset($datos->datos)) {
            $html .= ' <div class="row">
        <div class="col-lg-12"><table class="table" id="tabla_permisos"><thead>
        <tr>
          <th scope="col" style="width: 1%;">#</th>
          <th scope="col" style="width: 5%;">Tipo de permiso</th>
          <th scope="col" style="width: 5%;">Clave</th>
          <th scope="col" style="width: 6%;">Solicitante/Ciudadano</th>
          <th scope="col" style="width: 12%;">Dirección</th>
          <th scope="col">Teléfono</th>
          <th scope="col">Fecha de solicitud</th>
          <th scope="col">En bandeja de</th>
          <th scope="col">Fecha asignación</th>
          <th scope="col"  style="width: 1%;">Estado</th>
          <th scope="col">Fecha aprobación</th>
          <th scope="col">Fecha vencimiento</th>
          <th scope="col">Mapa</th>
          <th scope="col">Accion</th>
        </tr>
      </thead>
      <tbody>';

            foreach ($datos->datos as $key => $value) {
                $html .= '<tr><td>' . ($key + 1) . '</td><td>' . $value->nombresolicitud . '</td><td>' . $value->clave . '</td><td>' . $value->name . '</td><td>' . $value->direccion . '</td><td>' . $value->telefono . '</td><td>' . $value->created_at . '</td><td>' . $value->delegado . '</td><td>' . $value->fecha_delegacion . '</td><td>' . $value->estado . '</td><td>' . $value->fecha_aprobacion . '</td><td>' . $value->fecha_vencimiento . '</td><td><button type="button"  class="btn btn-success" onClick="window.open(\'https://www.google.es/maps?q=' . preg_replace('~[\\\\/:*?"<>[]|]~', ' ', $value->mapa) . '\');"><span class="icon">Ver en mapa</span></button></td><td>' . $value->ruta . '</td></tr>';
            }

            $html .= '</tbody></table></div></div>';
        } else {

            $colect = collect($datos->datos_2);
            $mul = $colect->groupBy('nombresolicitud');
            foreach ($mul as $key => $estados) {
                // dd($estados);

                $html .= "<h3>" . $key . "</h3><table cellspacing='0' cellpadding='0' width='100%'><tr>";
                foreach ($estados as $k => $value) {
                    $color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                    $html .= "
                    <td>
              

                        <div class='ibox-title'>
                        <div class='ibox-content'>
                            <h1 class='no-margins' style='color:" . $color . ";'>" . $value->total . "</h1>
                            <span style='font-size: medium;'><i style='color:" . $color . ";' class='" . $value->icono . "' aria-hidden='true'></i> " . str_replace('_', ' ', $value->estado) . "</span>
                        </div>
               
                <td>
               ";
                }

                $html .= '</table>';
            }
        }
        return $html;
    }


    public function donaciones()
    {

        if (Input::get("token")) {
            // $filtros = json_decode(Input::get('data'));
            // // dd($filtros[11]->name );
            // if ($filtros[11]->value == "imagenes_donaciones") {
            //     return $this->descargarDocumentos($filtros);
            // } else {
            // }
            return $this->generarreporte();
        }

        $objects = json_decode('[
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "text", "Descripcion": "Cédula", "Nombre": "CEDULA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Parroquia", "Nombre": "IDPARROQUIA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Barrio", "Nombre": "IDBARRIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo de donación", "Nombre": "TIPO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Estado", "Nombre": "ESTADO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Asignado a", "Nombre": "ASIGNADO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo de fecha", "Nombre": "FECHATIPO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        //show($objects);
        /*COMBOS*/


        $fechaini = "2019-11-18";
        $fechafin = date('Y-m-d');
        $direcciones = direccionesModel::join("users as u", "u.id_direccion", "=", "tmae_direcciones.id")
            ->join("dona_donciones as a", "a.id_usuario", "=", "u.id")
            ->select("tmae_direcciones.direccion", "tmae_direcciones.id", DB::raw("count(*) as total"))
            ->groupBy("tmae_direcciones.direccion", "tmae_direcciones.id")
            ->orderby("tmae_direcciones.direccion")
            ->pluck("direccion", "id")
            ->all();


        $namerepor = array(
            "rpt_donaciones" => "Reporte Donaciones",
            "rpt_donaciones_territorio" => "Reporte Donaciones para el territorio",
            "rpt_donaciones_excel" => "Reporte Donaciones sin estilos",
            // "imagenes_donaciones" => "Imagenes de entrega de donaciones",
        );
        $formatosjs = array(
            "rpt_donaciones" => '{"pdf":"PDF"}',
            "rpt_donaciones_territorio" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_donaciones_excel" => '{"xls":"XLS"}',
            // "imagenes_donaciones" => '{"zip":"ZIP"}'
        );
        $tiporepor = array(
            "rpt_donaciones" => '{"completo":"Completo"}',
            "rpt_donaciones_territorio" => '{"completo":"Completo"}',
            "rpt_donaciones_excel" => '{"completo":"Completo"}',
            // "imagenes_donaciones" => '{"completo":"Completo"}'
        );

        $objects[0]->Valor = $this->escoja + $direcciones;
        $parroquia = parroquiaModel::where(['estado' => 'ACT'])->pluck('parroquia', 'id')->all();;
        $objects[2]->Valor = $this->escoja + $parroquia;
        $objects[3]->Valor = $this->escoja + $this->escoja + barrioModel::where(['estado' => 'ACT', 'id_parroquia' => 1])->pluck('barrio', 'id')->all();

        $objects[4]->Valor = $this->escoja + $this->escoja + DonaItemsModel::where(['estado' => 'ACT'])->pluck('nombre', 'id')->all();;
        $objects[5]->Valor = $this->escoja + [
            'PENDIENTE' => 'PENDIENTE',
            'ENTREGADO' => 'ENTREGADO'
        ];

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'users.id')->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo == 11 || $id_tipo_pefil->tipo == 1 || Auth::user()->id_perfil == 44 || Auth::user()->id_perfil == 42) {
            $array_ = [
                '1310803794',
                '1316060316',
                '1313505602',
                '1313466581',
                '1312573692',
                '1311810848',
                '1309665774',
                '1314346600',
                '1309954624',
                '1312044918',
                '1308730694'
            ];
            if (in_array(Auth::user()->cedula, $array_)) {
                $objects[6]->Valor = [Auth::user()->id => Auth::user()->name];
            } else {
                $objects[6]->Valor = $this->escoja + DonaDonacionesModel::from('dona_donciones as d')->join('users as u', 'u.id', 'd.id_responsable')->where('d.estado', 'ACT')->groupby('u.id')->pluck('name', 'u.id')->all();
            }
        } else {
        }

        $objects[7]->Valor = [
            1 => 'FECHA SOLICITUD',
            2 => 'FECHA ENTREGA'
        ];
        $objects[8]->Valor = $fechaini;
        $objects[9]->Valor = $fechafin;

        $objects[10]->Valor = $namerepor;
        $objects[11]->Valor = $this->formatos;
        $objects[12]->Valor = ["completo" => "Completo"];
        // show($objects);
        $params = array();

        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        // show($objects);
        $validate_js = [];
        $general_config = ['REPORTE DONACIONES', 'coordinacioncronograma/reportedonaciones', ''];
        // $this->cof
        $objects[14]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }

        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        // $objetos_reportes=json_decode($this->reportes_objeto);
        // $objetos_reportes[0]->Valor=$this->reportes;
        // $objects=insertarinarray($objects,$objetos_reportes[0],0);

        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor,
            "indicadores" => "SI"
        ]);
    }

    public function reporte_ingreso_donaciones()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        $objects = json_decode('[
            {"Tipo": "text", "Descripcion": "Cédula / RUC", "Nombre": "CEDULA_RUC", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo de producto", "Nombre": "PRODUCTO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Unidad", "Nombre": "UNIDAD", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Estado del producto", "Nombre": "ESTADO_PRODUCTO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        //show($objects);
        /*COMBOS*/


        $fechaini = "2019-11-18";
        $fechafin = date('Y-m-d');

        $namerepor = array(
            "rpt_donaciones_ingreso" => "Reporte de ingreso de donaciones"
        );
        $formatosjs = array(
            "rpt_donaciones_ingreso" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_donaciones_ingreso" => '{"completo":"Completo"}'
        );


        $objects[1]->Valor = $this->escoja + $this->escoja + DonaIngresoProductoModel::where(['estado' => 'ACT'])->pluck('producto', 'id')->all();
        $objects[2]->Valor = $this->escoja + DonaIngresoDonacionProductoModel::where('estado', 'ACT')->groupBy('unidad')->pluck('unidad', 'unidad')->all();
        $objects[3]->Valor = $this->escoja + DonaIngresoDonacionProductoModel::where('estado', 'ACT')->groupBy('estado_producto')->pluck('estado_producto', 'estado_producto')->all();

        $objects[4]->Valor = $fechaini;
        $objects[5]->Valor = $fechafin;
        $objects[6]->Valor = $namerepor;
        $objects[7]->Valor = $this->formatos;
        $objects[8]->Valor = ["completo" => "Completo"];
        // show($objects);
        $params = array();

        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        // show($objects);
        $validate_js = [];
        $general_config = ['REPORTE DE INGRESO DE DONACIONES', 'coordinacioncronograma/reporte_ingreso_donaciones', ''];
        // $this->cof
        $objects[9]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor,
            "indicadores" => "SI"
        ]);
    }

    public function descargarDocumentos($datos)
    {

        // set_time_limit(-1);
        ini_set('max_execution_time', -1);
        $zip = new ZipArchive();
        $nombreArchivoZip = public_path() . "/temp/Fotos.zip";

        if (!$zip->open($nombreArchivoZip, ZipArchive::CREATE | ZipArchive::OVERWRITE)) {
            return "Error abriendo ZIP en $nombreArchivoZip";
        }
        $cedula = "";
        $id_direccion = "";
        $id_parroquia = "";
        $id_barrio = "";
        $tipo = "";
        $estado = "";
        $asignado = "";
        $tipo_fecha = "";
        $fecha_inicio = "";
        $fecha_fin = "";
        foreach ($datos as $key => $value) {
            if ($value->name == 'CEDULA') {
                // dd($value);
                $cedula = $value->value;
            }
            if ($value->name == 'IDDIRECCION') {
                $id_direccion = $value->value;
            }
            if ($value->name == 'IDPARROQUIA') {
                $id_parroquia = $value->value;
            }
            if ($value->name == 'IDBARRIO') {
                $id_barrio = $value->value;
            }
            if ($value->name == 'TIPO') {
                $tipo = $value->value;
            }
            if ($value->name == 'ESTADO') {
                $estado = $value->value;
            }
            if ($value->name == 'ASIGNADO') {
                $asignado = $value->value;
            }
            if ($value->name == 'FECHATIPO') {
                $tipo_fecha = $value->value;
            }
            if ($value->name == 'FECHAINICIO') {
                $fecha_inicio = $value->value;
            }
            if ($value->name == 'FECHAFINAL') {
                $fecha_fin = $value->value;
            }
        }


        $consulta = DB::select(trim("SELECT * FROM
        dona_donciones as d,users as u, tmae_direcciones as dir,dona_items as i , barrio AS b,parroquia as p
       where  u.id=d.id_responsable and dir.id=u.id_direccion AND i.id=d.id_donacion and b.id=d.barrio_id and p.id=b.id_parroquia
           AND CAST(d.cedula_beneficiario AS CHAR) LIKE case when '" . $cedula . "'='' then '%' else  '" . $cedula . "' end
           AND CAST(dir.id AS CHAR) LIKE case when " . $id_direccion . "='0' then '%' else  " . $id_direccion . " end
           AND CAST(p.id AS CHAR) LIKE case when " . $id_parroquia . "='0' then '%' else  " . $id_parroquia . " end
           AND CAST(b.id AS CHAR) LIKE case when " . $id_barrio . "='0' then '%' else  " . $id_barrio . " end
               AND CAST(i.id AS CHAR) LIKE case when " . $tipo . "='0' then '%' else  " . $tipo . " end
                   AND CAST(d.estado_donacion AS CHAR) LIKE case when '" . $estado . "'='0' then '%' else  '" . $estado . "' end
                   and  	CAST(d.id_responsable AS CHAR) LIKE case when " . $asignado . "='0' then '%' else  " . $asignado . " end
                   AND case when  " . $tipo_fecha . "='1' THEN  DATE_FORMAT(d.fecha_ingreso, \"%Y-%m-%d\") BETWEEN '" . $fecha_inicio . "'  and '" . $fecha_fin . "' ELSE DATE_FORMAT(d.fecha_entrega, \"%Y-%m-%d\") BETWEEN '" . $fecha_inicio . "'  and '" . $fecha_fin . "' end
                   and d.estado='ACT'"));
        foreach ($consulta as $key => $value) {
            // dd($consulta);
            $keys = [
                'adjunto',
                'adjunto_entrega',
                'frontal',
                'trasera',
                'entrega',
                'ficha'
            ];
            foreach ($value as $kk => $vv) {
                if (in_array($kk, $keys)) {
                    $rutaAbsoluta = public_path() . "/archivos_sistema/" . $vv;
                    if (is_file($rutaAbsoluta)) {
                        $nombre = basename($rutaAbsoluta);
                        $zip->addFile($rutaAbsoluta, $nombre);
                    }
                }
            }
        }
        $rutaAbsoluta = public_path() . "/robots.txt";
        if (is_file($rutaAbsoluta)) {
            $nombre = basename($rutaAbsoluta);
            $zip->addFile($rutaAbsoluta, $nombre);
        }
        $resultado = $zip->close();
        if (!$resultado) {
            return null;
        }
        return ['estado' => true, 'ruta' => URL::to('') . "/temp/Fotos.zip"];
    }


    public function reportesgeneral()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        $objects = json_decode('[
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null","Ancho":12},
            {"Tipo": "select-multiple", "Descripcion": "Estado", "Nombre": "ESTADO", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "datetext", "Descripcion": "Fecha inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        /*COMBOS*/
        $direcciones = direccionesModel::select("tmae_direcciones.direccion", "tmae_direcciones.id")
            ->orderby("tmae_direcciones.direccion")
            ->pluck("direccion", "id")
            ->all();
        $estado = explodewords(ConfigSystem("estadoobras"), "|");

        $fechaini = "2019-05-01";
        $fechafin = date('Y') . "-12-31";
        //TipoNameReporte
        $namerepor = array(
            "rpt_general" => "Reporte general por dirección"
        );
        $formatosjs = array(
            "rpt_general" => '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_general" => '{"completo":"Completo"}'
        );
        $objects[0]->Valor = $this->escoja + $direcciones;
        $objects[1]->Valor = $this->escoja + $estado;
        $objects[2]->Valor = $namerepor;
        $objects[3]->Valor = $fechaini;
        $objects[4]->Valor = $fechafin;
        $objects[5]->Valor = $this->formatos;
        $objects[6]->Valor = ["completo" => "Completo"];
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        $validate_js = [];
        $general_config = ['REPORTE GENERAL POR DIRECCIÓN', 'reportesgeneral', ''];
        $objects[7]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        $funcalljs = "";
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor
        ]);
    }
    public function reportecovid()
    {
        if (Input::get("token")) {
            $data = json_decode(Input::get('data'));
            if ($data[8]->value == 'xls' && $data[5]->value == 'rpt_covid') {
                $data[5]->value = 'rpt_covid_sin_estilos';
                request()->merge(['data' => json_encode($data)]);
            }
            return $this->generarreporte();
        }
        $objects = json_decode('[
            {"Tipo": "text", "Descripcion": "Cédula", "Nombre": "CEDULA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null","Ancho":12},
            {"Tipo": "select", "Descripcion": "Orden del filtro", "Nombre": "ORDEN", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "text", "Descripcion": "Número de preguntas con si", "Nombre": "SI", "Clase": "solonumeros", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha fin", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},            
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Opciones Reporte", "Nombre": "tiporeporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null","Oculto":"SI"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');


        /*COMBOS*/
        $direcciones =  EncuestaSaludModel::where('tma_encuesta_salud.estado', 'ACT')->join('tmae_direcciones as d', 'd.id', 'tma_encuesta_salud.direccion_id')->select("d.direccion", "d.id")
            ->orderby("d.direccion")
            ->groupby("d.id")
            ->pluck("direccion", "id")
            ->all();

        $fechaini = "2019-05-01";
        $fechafin = date('Y') . "-12-31";
        //TipoNameReporte
        $namerepor = array(
            "rpt_covid" => "Reporte de general encuesta",
            "rpt_covid_detalle" => "Reporte de detallado"
        );
        $formatosjs = array(
            "rpt_covid" => '{"pdf":"PDF","xls":"XLS"}',
            "rpt_covid_detalle" =>  '{"pdf":"PDF","xls":"XLS"}'
        );
        $tiporepor = array(
            "rpt_covid" => '{"completo":"Completo"}',
            "rpt_covid_detalle" => '{"completo":"Completo"}'
        );
        $objects[2]->Valor = $this->escoja + $direcciones;
        $objects[3]->Valor = ['1' => 'MAYOR IGUAL QUÉ', '0' => 'MENOR IGUAL QUÉ'];
        $objects[4]->Valor = 4;
        $objects[4]->ValorAnterior = 4;
        $objects[1]->Valor = $namerepor;
        $objects[5]->Valor = $fechaini;
        $objects[6]->Valor = $fechafin;
        $objects[7]->Valor = $this->formatos;
        $objects[8]->Valor = ["completo" => "Completo"];
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        $validate_js = [];
        $general_config = ['ENCUESTA SALUD OCUPACIONAL', 'reportecovid', ''];
        $objects[9]->Valor = $general_config[0];
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";
        $funcalljs = "";
        // show($objects);
        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor
        ]);
    }


    public function reportesporactividad()
    {
        if (Input::get("token"))
            return $this->generarreporte();
        $objects = json_decode($this->reportes_objeto);
        $objects[0]->Valor = $this->reportes;


        $general_config = ['REPORTE GENERAL POR ACTIVIDAD', 'reportesgeneral', ''];
        if (!Auth::check()) {
            return redirect(url("login"));
        }

        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => [],
            'paramadi' => Crypt::encrypt([]),
            'formatosjs' => [],
            "funcionjs" => '',
            "tiporepor" => [],
            "boton" => 'no'
        ]);
    }


    public function reportesTramitesExternos()
    {
        if (Input::get("token")) {
            $data = json_decode(Input::get('data'));

            if ($data[10]->value == 'xls') {
                $data[12]->value = 'rpt_tramites_excel';
                request()->merge(['data' => json_encode($data)]);
            }
            // dd($data);
            if ($data[7]->value != 0) {
                $data[12]->value = 'rpt_reportestramitesexternos_direccion';
                request()->merge(['data' => json_encode($data)]);
            }


            return $this->generarreporte();
        }


        $objects = json_decode('[
            {"Tipo": "text", "Descripcion": "Número de trámite", "Nombre": "NUMTRAMITE", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "text", "Descripcion": "Identificación", "Nombre": "IDENTIFICACION", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "text", "Descripcion": "Nombre del remitente", "Nombre": "REMITENTE", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Disposición", "Nombre": "DISPOSICION", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Ingreso Desde", "Nombre": "CREACIONDESDE", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Ingreso Hasta", "Nombre": "CREACIONHASTA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Direccion", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tipo", "Nombre": "TIPO", "Clase": "select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Tiempo", "Nombre": "TIEMPO", "Clase": "select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Formato", "Nombre": "formato", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "FormHidden", "Descripcion": "Titulo", "Nombre": "tituloreporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "FormHidden", "Descripcion": "Tipo Reporte", "Nombre": "namereporte", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');


        $namerepor = [
            "rpt_reportestramitesexternos" => "Reporte de trámites externos"
        ];

        $formatosjs = [
            "rpt_reportestramitesexternos" => '{"pdf":"PDF","xls":"XLS"}',
        ];

        $tiporepor = [
            "rpt_reportestramitesexternos" => '{"completo":"Completo"}'
        ];

        $fechaini = "2020-06-10";
        $fechafin = date('Y') . "-12-31";

        $objects[3]->Valor = $this->escoja + ['0' => 'TODOS', 'EN PROCESO' => 'EN PROCESO', 'APROBADA' => 'APROBADA', 'FINALIZADO' => 'FINALIZADO'];
        $objects[4]->Valor = $fechaini;
        $objects[5]->Valor = $fechafin;

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'users.id', "users.id_perfil")->where("users.id", Auth::user()->id)->first();
        if ($id_tipo_pefil->tipo != 1 && $id_tipo_pefil->tipo != 4 && $id_tipo_pefil->id_perfil != 50 && $id_tipo_pefil->id_perfil != 22) {
            $direcciones = direccionesModel::where(['estado' => 'ACT', 'id' => Auth::user()->id_direccion])->pluck('direccion', 'id')->all();
            $objects[6]->Valor = $direcciones;
        } else {
            $direcciones = direccionesModel::where(['estado' => 'ACT'])->pluck('direccion', 'id')->all();
            $objects[6]->Valor = $this->escoja + $direcciones;
        }


        $objects[7]->Valor = $this->escoja + [
            'SECRETARIA GENERAL' => 'SECRETARÍA GENERAL',
            'TRAMITES MUNICIPALES' => 'PERMISOS MUNICIPALES'
        ];

        // show($objects);
        $objects[8]->Valor = $this->escoja + [
            'ATRASADO' => 'ATRASADO',
            'A TIEMPO' => 'A TIEMPO'
        ];
        $objects[9]->Valor = $this->formatos;
        $params = array();
        foreach ($objects as $key => $value) {
            # code...
            $params[] = $value->Nombre;
        }
        $validate_js = [];
        $general_config = ['REPORTE DE TRAMITES EXTERNOS', 'reportesTramitesExternos', ''];
        $objects[10]->Valor = $general_config[0];
        $objects[11]->Valor = 'rpt_reportestramitesexternos';
        if (!Auth::check()) {
            return redirect(url("login"));
        }
        $paramadi = array("TITULO" => $general_config[0], "USUARIO" => Auth::user()->name);
        $funcionjs = "";

        return view('reports.create', [
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' => Crypt::encrypt($paramadi),
            'formatosjs' => $formatosjs,
            "funcionjs" => $funcionjs,
            "tiporepor" => $tiporepor
        ]);
    }
}
