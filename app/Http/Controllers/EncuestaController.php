<?php

namespace App\Http\Controllers;

use App\EncuestaSaludModel;
use App\PreguntasEncuestaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\ComunicacionAlcaldia\Entities\CantonModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\CoordinacionCronograma\Entities\GesIndicadorGestionDetaBarrio;

class EncuestaController extends Controller
{
    var $escoja = array(null => "SELECCIONE UNA OPCIÓN");
    var $configuraciongeneral = array("ENCUESTA SALUD OCUPACIONAL COVID-19", "encuesta.store", "index");
    // var $configuraciongeneral = ['ENCUESTA SALUD OCUPACIONAL COVID-19', 'index', '', 'crear'];
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Cédula (*)","Nombre":"cedula","Clase":"cedula solonumeros","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Apellidos y nombres (*)","Nombre":"nombre","Clase":"Null","Valor":"Null","Ancho": "4","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Edad (*)","Nombre":"edad","Clase":"Null","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Celular (*)","Nombre":"celular","Clase":"solonumeros","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Sexo (*)","Nombre":"sexo","Clase":"chosen-select","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Dirección (*)","Nombre":"direccion_id","Clase":"chosen-select","Valor":"Null","Ancho": "3","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Cargo (*)","Nombre":"cargo","Clase":"Null","Valor":"Null","Ancho": "4","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Fecha de encuesta","Nombre":"fecha","Clase":"Null","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" }
    ]';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->configuraciongeneral[1] = '/encuesta/create';
        return view('vistas.encuesta.index', [
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    public function getBarrios($id_parroquia)
    {
        // dd($id_parroquia);
        if ($id_parroquia == 0)
            return $this->escoja + barrioModel::where('estado', 'ACT')->orderby("barrio", "asc")->get();

        $barrios = BarrioModel::where('id_parroquia', $id_parroquia)->orderby("barrio", "asc")->get();
        // asort($barrios);
        return $barrios;
    }

    public function validaEncuesta(Request $request)
    {
        $cedula = $request->cedula;
        $registro = EncuestaSaludModel::where('cedula', $cedula)->pluck('created_at')->all();
        $bandera = true;
        foreach ($registro as $key => $value) {
            # code...
            // $fecha = date("d/m/Y", strtotime($value));
            $carbon1 = new \Carbon\Carbon();
            //convertimos la fecha 2 a objeto Carbon
            $carbon2 = new \Carbon\Carbon($value);
            //de esta manera sacamos la diferencia en minutos
            //dd($carbon2);
            $diasDiff = $carbon1->diffInDays($carbon2);
            if ($diasDiff <= 15) {
                $bandera = false;
            }
            // dd($fecha);
        }
        // dd($bandera);
        return response()->json(['estado' => $bandera]);
    }

    // public function getCantones()
    // {
    //     return CantonModel::select("id", "nombre_canton as canton")->orderby("nombre_canton", "asc")->get();
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $preguntas = PreguntasEncuestaModel::where('estado', 'ACT')->get();
        // dd($preguntas);
        $direcciones = direccionesModel::where('estado', 'ACT')
            ->pluck("direccion", "id")
            ->all();
        // show($preguntas);
        $objetos = json_decode($this->objetos);
        $objetos[1]->Clase = "disabled";
        $objetos[2]->Clase = "disabled";
        $objetos[4]->Valor = $this->escoja + ["HOMBRE" => "HOMBRE", "MUJER" => "MUJER"];
        $objetos[5]->Valor = $this->escoja + $direcciones;
        // $objetos[7]->Valor = [];

        return view('vistas.encuesta.encuesta', [
            "preguntas" => json_decode($preguntas),
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->guardar($request);
    }

    public function guardar(Request $request)
    {
        $input = Input::all();
        // dd($input);

        $validator = Validator::make($input, EncuestaSaludModel::rules());

        if ($validator->fails()) {
            $datos = $validator->getMessageBag()->toArray();
            $mensaje = '<ul>';
            foreach ($datos as $key => $value) {
                # code...
                // $mensaje=$value[0];
                $mensaje .= '<li>' . $value[0] . '</li>';
            }
            $mensaje .= '</ul>';
            // dd($mensaje);
            Session::flash('error', $mensaje);
            return redirect()->back()->withInput();
            // return Redirect::to("/encuesta")
            //     ->withErrors($validator)
            //     ->withInput();
        } else {
            DB::beginTransaction();
            try {
                $guardar = new EncuestaSaludModel();


                $datos = [];
                $si = 0;
                $no = 0;
                foreach ($input as $key => $value) {
                    // dd($input);

                    if ($key != "_method" && $key != "_token") {
                        // dd(strpos($key, 'pregunta_'));
                        if (strpos($key, 'pregunta_') !== false) {
                            // $guardar->$key = $value;
                            $datos[str_replace('pregunta_', '', $key)] = $value;
                            if ($value == 1) {
                                $si++;
                            }
                            if ($value == 0) {
                                $no++;
                            }
                        } else {
                            $guardar->$key = $value;
                        }
                    }
                }
                $guardar->data = json_encode([$datos]);
                $guardar->si = $si;
                $guardar->no = $no;
                $guardar->save();

                DB::commit();
            } catch (\Throwable $th) {
                //throw $th;

                DB::rollback();
                $mensaje = $th->getMessage() . " - " . $th->getLine();
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }

            Session::flash('message', "Encuesta registrada correctamente");
            return Redirect::to("/encuesta")->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
