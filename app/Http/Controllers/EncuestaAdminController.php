<?php

namespace App\Http\Controllers;

use App\DonaDonacionesModel;
use App\EncuestaSaludModel;
use App\PreguntasEncuestaModel;
use App\UsuariosModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use stdClass;

class EncuestaAdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    var $configuraciongeneral = array("ENCUESTA SALUD OCUPACIONAL", "coordinacioncronograma/encuesta", "index", 6 => "encuesta_ajax", 7 => "encuesta_ajax");
    var $escoja = array(null => "Escoja opción...");
    var $estados = [
        'ACT' => 'ACTIVO',
        'INA' => 'INACTIVO',

    ];

    var $objetos = '[
        {"Tipo":"text","Descripcion":"Cédula","Nombre":"cedula","Clase":"cedula solonumeros","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Apellidos y nombres","Nombre":"nombre","Clase":"Null","Valor":"Null","Ancho": "6","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Edad","Nombre":"edad","Clase":"Null","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Celular","Nombre":"celular","Clase":"solonumeros","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Sexo","Nombre":"sexo","Clase":"chosen-select","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"direccion_id","Clase":"chosen-select","Valor":"Null","Ancho": "3","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Cargo","Nombre":"cargo","Clase":"Null","Valor":"Null","Ancho": "4","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Afirmativas","Nombre":"si","Clase":"Null","Valor":"Null","Ancho": "4","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Negativas","Nombre":"no","Clase":"Null","Valor":"Null","Ancho": "4","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Fecha de encuesta","Nombre":"fecha","Clase":"Null","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Detalle","Nombre":"preguntas","Clase":"Null","Valor":"Null","Ancho": "2","ValorAnterior" :"Null" }
    ]';

    var $filtros = '[
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"select","Descripcion":"Agrupado por cantidad de afirmaciones","Nombre":"numero","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"direccion_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"date","Descripcion":"Desde","Nombre":"desde","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"date","Descripcion":"Hasta","Nombre":"hasta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
    ]';

    


    public function verpermisos($objetos = array(), $tipo = "index")
    {
        $tabla = $tabla = DonaDonacionesModel::select('dona_donciones.*', 'u.name', 'd.nombre', 'p.parroquia', 'b.barrio', 'ur.name as responsable')
            ->join("users as u", "u.id", "=", "dona_donciones.id_usuario")
            ->join("users as ur", "ur.id", "=", "dona_donciones.id_responsable")
            ->join("dona_items as d", "d.id", "=", "dona_donciones.id_donacion")
            ->join("barrio as b", "b.id", "=", "dona_donciones.barrio_id")
            ->join("parroquia as p", "p.id", "=", "b.id_parroquia")
            ->where("dona_donciones.estado", "ACT");
        return array($objetos, $tabla);
    }

    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) {
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        }
        return $object;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $direcciones = direccionesModel::where('estado', 'ACT')
            ->pluck("direccion", "id")
            ->all();
            
        $tabla = [];
        $objetos = json_decode($this->objetos);
        $filtros = json_decode($this->filtros);
        $filtros[0]->Valor = $this->estados;

        $numeros = EncuestaSaludModel::select("si")->where('estado', 'ACT')->groupBy('si')->pluck('si', 'si')->all();

        $filtros[1]->Valor = $this->escoja + $numeros;
        $filtros[2]->Valor = $this->escoja + $direcciones;
        // dd($filtros[1]->Valor);
        

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $eliminar = 'no';
        if ($id_tipo_pefil->tipo == 11 || $id_tipo_pefil->tipo == 1) {
            if ($id_tipo_pefil->tipo == 1 || Auth::user()->cedula == '1314689827') {
                $eliminar = 'si';
            }
            $create = 'no';
        } else {
            $create = 'no';
        }

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            'filtros' => $filtros,
            "configuraciongeneral" => $this->configuraciongeneral,
            'create' => $create,
            'delete' => $eliminar
        ]);
        //return view('coordinacioncronograma::index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objetos = json_decode($this->objetos);
        $id = Crypt::decrypt($id);
        $tabla = EncuestaSaludModel::
        select('tma_encuesta_salud.id', 'cedula', 'edad', 'sexo', 'celular', 'tma_encuesta_salud.nombre', 
        'cargo', 'si', 'no', 'tma_encuesta_salud.created_at as fecha', 'data as preguntas', 'd.direccion as direccion_id')
        ->join('tmae_direcciones as d', 'd.id', 'tma_encuesta_salud.direccion_id')->where("tma_encuesta_salud.id",$id)
        ->first();
        
        $questions = json_decode($tabla->preguntas); 
        $datos = '<ul>';
        $class = '';
        foreach ($questions[0] as $key => $value) {
            # code...
                # code...
            $question = PreguntasEncuestaModel::where('id', $key)->first();
            $class = ($value == 1) ? 'badge-danger' : 'badge-info';
            $value = ($value == 1) ? 'Si' : 'No';
            $datos .= '<li>'.$question->pregunta . ': <span class="badge '.$class.'">'. $value .'</span> </li>';
        }
        $datos .= '</ul>';

        // show($datos);
        $tabla->preguntas = $datos;

        return view('vistas.show',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }

    public function encuesta_ajax(Request $request) {

        

        $columns = array(
            0 => 'tma_encuesta_salud.id',
            1 => 'cedula',
            2 => 'edad',
            3 => 'sexo',
            4 => 'celular',
            5 => 'direccion_id',
            6 => 'nombre',
            7 => 'cargo',
            8 => 'si',
            9 => 'no',
            10 => 'fecha',
            11 => 'preguntas',
        );

        $query = EncuestaSaludModel::
        select('tma_encuesta_salud.id', 'cedula', 'edad', 'sexo', 'celular', 'tma_encuesta_salud.nombre', 
        'cargo', 'si', 'no', 'tma_encuesta_salud.created_at', 'data', 'd.direccion')
        ->join('tmae_direcciones as d', 'd.id', 'tma_encuesta_salud.direccion_id');
        
        // dd($query);
        if ($request->estado != '0') {
            # code...
            $query = $query->where(['tma_encuesta_salud.estado' => $request->estado]);
        }
        
        if (isset($request->numero)) {
            # code...
            $query = $query->where('tma_encuesta_salud.si', $request->numero);
        }

        if (isset($request->direccion_id)) {
            # code...
            $query = $query->where('tma_encuesta_salud.direccion_id', $request->direccion_id);
        }

        
        if ((isset($request->desde) && !isset($request->hasta)) || (!isset($request->desde) && isset($request->hasta))) {
            # code...
            Session::flash('error', 'Debe seleccionar un rango válido');
            return;
        } 
        
        if (isset($request->desde)) {
            # code...
            $query = $query->whereBetween('tma_encuesta_salud.created_at', [$request->desde, $request->hasta]);
        }
        
        
        $totalData = $query->count();
        
        
        
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[8];
        $dir = $request->input('order.0.dir');
        // $dir = $request->input('order.0.dir');
        
        if (empty($request->input('search.value'))) {
            
            // dd($query);
            $posts =  $query
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            // dd($query);
            $search = $request->input('search.value');

            $posts =  $query->where(function ($query) use ($search) {
                $query->where('denu_tmo_denuncia.id', 'LIKE', "%{$search}%")
                    ->orWhere('name', 'LIKE', "%{$search}%")
                    ->orWhere(DB::raw("estado_alerta"), 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->groupBy('denu_tmo_denuncia.id')
                ->orderBy($order, $dir)
                ->get();


            $totalFiltered = $query->where(function ($query) use ($search) {
                $query->where('denu_tmo_denuncia.id', 'LIKE', "%{$search}%")
                    ->orWhere('name', 'LIKE', "%{$search}%")
                    ->orWhere(DB::raw("estado_alerta"), 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->groupBy('denu_tmo_denuncia.id')
                ->orderBy($order, $dir)
                ->count();
        }

        $data = array();
        if (!empty($posts)) {
            $aciones = '';
            foreach ($posts as $post) {
                // dd($this->configuraciongeneral);
                // coordinacioncronograma/detalle/{id}
                // $aciones = link_to_route(str_replace("/",".",'encuesta').'.show','', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o divpopup','target'=>'_blank', 'onclick'=>'popup(this)')).'&nbsp;&nbsp;';
                $aciones = "<a href='" . URL::to('/coordinacioncronograma/encuesta/' . Crypt::encrypt($post->id)) . "' class='btn btn-info dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-newspaper-o'></i></a>";

                $dt = Carbon::parse($post->created_at, 'UTC');
                $dt2 = Carbon::now('UTC');

                // if($post->estado_alerta!='ACTIVA'){
                //     $aciones='';
                // }

                $questions = json_decode($post->data); 
                $datos = '<ul>';
                $class = '';
                foreach ($questions[0] as $key => $value) {
                    # code...
                    if ($value == 1) {
                        # code...
                        $question = PreguntasEncuestaModel::where('id', $key)->first();
                        $class = ($value == 1) ? 'badge-danger' : 'badge-info';
                        $value = ($value == 1) ? 'Si' : 'No';
                        $datos .= '<li>'.$question->pregunta . ': <span class="badge '.$class.'">'. $value .'</span> </li>';
                    }
                }
                $datos .= '</ul>';

                $nestedData['id'] = $post->id;
                $nestedData['cedula'] = $post->cedula;
                $nestedData['edad'] = $post->edad;
                $nestedData['sexo'] = $post->sexo;
                $nestedData['celular'] = $post->celular;
                $nestedData['direccion_id'] = $post->direccion;
                $nestedData['nombre'] = $post->nombre;
                $nestedData['cargo'] = $post->cargo;
                $nestedData['si'] = $post->si;
                $nestedData['no'] = $post->no;
                $nestedData['fecha'] = date('Y-m-d H:m:s', strtotime($post->created_at));
                $nestedData['preguntas'] = $datos;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }
    
}
