<?php

namespace App\Http\Controllers;

use App\UsuariosModel;
use Illuminate\Http\Request;
use stdClass;

class EstablecimientosController extends Controller
{
    //
    var $configuraciongeneral = array("GUIA COMERCIAL", "coordinacioncronograma/guiacomercial", "index", 6 => "https://portalciudadano.manta.gob.ec/estableciminetosAdminAjax", 7 => "guiacomercial", 8 => "https://portalciudadano.manta.gob.ec");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"htmlplantilla","Descripcion":"Fecha de creación","Nombre":"created_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Cedula / RUC ","Nombre":"cedula","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"text","Descripcion":"Nombre / Razón social","Nombre":"name","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"select","Descripcion":"Categoria","Nombre":"id_categoria","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"text","Descripcion":"Nombre","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"text","Descripcion":"Imágenes","Nombre":"imagenes","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado_lugar","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
    ]';
    var $filtros = '[
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado_solicitud","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';

    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }
    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) {
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'NO';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'NO';
        }
        return $object;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla = [];
        $objetos = json_decode($this->objetos);
        $filtros = json_decode($this->filtros);
        $filtros[0]->Valor = [
            'PENDIENTE' => 'PENDIENTE',
            'APROBADO' => 'APROBADO',
            'NEGADO' => 'NEGADO'
        ];
        // dd($objetos);

        if (config('app.env') == 'local') {
            $this->configuraciongeneral[6] = 'http://localhost:8091/mantaentusmanos/public/estableciminetosAdminAjax';
            $this->configuraciongeneral[8] = 'http://localhost:8091/mantaentusmanos/public';
        }

        return view('vistas.donaciones_arboles.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "filtros" => $filtros,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }
}
