<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\FechasConmemorativasModel;

class FechasConmemorativasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = FechasConmemorativasModel::where(['estado' =>'ACT'])->get();
        $title = 'Fechas Conmemorativas';
        return view('FechasConmemorativas.index', compact('data', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $title = 'Nueva Fecha Conmemorativa';
        return view('FechasConmemorativas.create')->with('title', $title);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $guardar = new FechasConmemorativasModel();
        $guardar->nombre = $request->nombre;
        $guardar->fecha = $request->fecha;
        $guardar->descripcion = $request->descripcion;
        $guardar->estado = 'ACT';
        $guardar->save();
        DB::commit();
        return redirect('coordinacioncronograma/FechasConmemorativas');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

        $data = FechasConmemorativasModel::find($id);
        $title = 'Editar Fecha Conmemorativa';
        return view('FechasConmemorativas.edit', compact('data', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        DB::beginTransaction();
        $guardar = FechasConmemorativasModel::find($request->id);
        $guardar->nombre = $request->nombre;
        $guardar->fecha = $request->fecha;
        $guardar->descripcion = $request->descripcion;
        $guardar->save();
        DB::commit();
        return redirect('coordinacioncronograma/FechasConmemorativas');
    }

     public function update_fecha(Request $request)
    {
        DB::beginTransaction();
        $guardar = FechasConmemorativasModel::findOrFail($request->id);
        $guardar->nombre = $request->nombre;
        $guardar->fecha = $request->fecha;
        $guardar->descripcion = $request->descripcion;
        $guardar->save();
        DB::commit();
        return redirect('coordinacioncronograma/FechasConmemorativas');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::beginTransaction();
        $guardar = FechasConmemorativasModel::find($request->id);
        $guardar->estado = 'INA';
        $guardar->save();
        DB::commit();
        return redirect('coordinacioncronograma/FechasConmemorativas');
    }
}
