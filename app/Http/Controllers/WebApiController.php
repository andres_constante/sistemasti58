<?php

namespace App\Http\Controllers;

use App\DonaDonacionesModel;
use Illuminate\Http\Request;

use App\Http\Requests;
use Modules\CoordinacionCronograma\Entities\Indicadores2020Model;  //Cabecera
use Modules\CoordinacionCronograma\Entities\Indicadores2020TransitoModel;  //Indicador transito
use Modules\CoordinacionCronograma\Entities\Indicadores2020SancionesModel;  //Indicador Sanciones
use Modules\CoordinacionCronograma\Entities\Indicadores2020SancionesTiposModel;  //Indicador tipo sanciones
use Modules\CoordinacionCronograma\Entities\Indicadores2020TransitoTipoTramiteModel; //Transito - tipo tramite
use Modules\CoordinacionCronograma\Entities\Indicadores2020EventosModel;  //Indicador eventos
use Modules\CoordinacionCronograma\Entities\Indicadores2020ActividadesFisicasModel;  //Indicador actividad fisica
use Modules\CoordinacionCronograma\Entities\Indicadores2020ArbolesSembradosModel;  //Indicador arbolito
use Modules\CoordinacionCronograma\Entities\Indicadores2020SeguridadCiudadanaModel;  //Indicador seguridad ciudadana
use Modules\CoordinacionCronograma\Entities\Indicadores2020TurismoModel;  //Indicador Turismo
use Modules\CoordinacionCronograma\Entities\Indicadores2020MantenimientoAreasModel;  //Indicador Area mantenimiento
use Modules\CoordinacionCronograma\Entities\Indicadores2020MantenimientoAreasServicioModel;  //Indicador Area mantenimiento
use Modules\CoordinacionCronograma\Entities\Indicadores2020TurismoServiciosModel;  //Indicador Turismo
use Modules\CoordinacionCronograma\Entities\Indicadores2020SeguridadCiudadanaServiciosModel;  //Indicador seguridad ciudadana servicios
use Modules\CoordinacionCronograma\Entities\Indicadores2020ControlTerritorialModel;  //Indicador control territorial servicios
use Modules\CoordinacionCronograma\Entities\Indicadores2020ArbolesSembradosServiciosModel;  //Indicador servicios arbolito
use Modules\CoordinacionCronograma\Entities\Indicadores2020ActividadesFisicasTipoModel;  //Indicador tipo actividad fisica
use Modules\CoordinacionCronograma\Entities\Indicadores2020EventosTiposModel;  //Indicador tipo eventos
use Modules\CoordinacionCronograma\Entities\Indicadores2020TipoModel;  //Tip de indicador
use Modules\Comunicacionalcaldia\Entities\barrioModel; //bario 
use Modules\Comunicacionalcaldia\Entities\parroquiaModel; //parroquia



use Modules\Coordinacioncronograma\Entities\CoorIndicadorDetalleModel;
use Modules\Coordinacioncronograma\Entities\CoorIndicadorTipoFiltroModel;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\Coordinacioncronograma\Entities\CoorContratistaModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use App\User;
use App\UsuariosModel;
use Modules\Comunicacionalcaldia\Entities\cabComunicacionModel;
use Modules\Coordinacioncronograma\Entities\CoorTipoProyectoModel;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaCabModel;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaCabDetaModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\PerfilModel;
use Modules\Coordinacioncronograma\Entities\CoorProyectoEstrategicoModel;
use Illuminate\Support\Facades\Redirect;
use Modules\TramitesAlcaldia\Http\Controllers\TramAlcaldiaChartCabController;

use Mail;
use Illuminate\Support\Str;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaDelModel;
use App\UsuarioModuloModel;
use Modules\Coordinacioncronograma\Entities\CabCompromisoalcaldiaModel;
use Modules\CoordinacionCronograma\Entities\CoorPoaActividadCabModel;
use Modules\Coordinacioncronograma\Entities\CoorPoaCabModel;
use Modules\Coordinacioncronograma\Entities\PoaActividadModel;
use Modules\CoordinacionCronograma\Http\Controllers\CoordinacionChartsController;

use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Modules\CoordinacionCronograma\Entities\MultasUnaserModel;
use Modules\CoordinacionCronograma\Entities\MultasUnaserTipoModel;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\CoordinacionCronograma\Entities\MantappPublicacionesArchivosModel;
use Modules\CoordinacionCronograma\Entities\MantappPublicacionesModel;
use Modules\TramitesAlcaldia\Entities\AnexosModel;
use Modules\TramitesAlcaldia\Entities\TramInternoAutorizacionesModel;
use Modules\TramitesAlcaldia\Entities\TramInternoCodigosModel;
use Modules\TramitesAlcaldia\Entities\TramiteInternoModel;
use stdClass;
use Yajra\DataTables\DataTables;
use ZipArchive;

/*
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers:*');
//header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
*/

class WebApiController extends Controller
{
    //

    protected $ChartAlcaldia;
    protected $DatosActividad;
    public function __construct(TramAlcaldiaChartCabController $ChartAlcaldia, CoordinacionChartsController $DatosActividad)
    {
        $this->middleware('cors');
        $this->ChartAlcaldia = $ChartAlcaldia;
        $this->DatosActividad = $DatosActividad;
    }
    //$res=$this->TramiteDigital->liquidacioncertificadocatastro($idtramite,$idsecu);

    function initwebapp()
    {
        if (Auth::attempt(['cedula' => request('cedula'), 'password' => request('password')])) {


            $log = UsuariosModel::where("cedula", request('cedula'))->first();
            $perfil = PerfilModel::find($log->id_perfil);

            if ($perfil) {
                if ($perfil->tipo != 4 && $perfil->tipo != 1 && $perfil->tipo != 5 && $perfil->tipo != 8 && $perfil->tipo != 3 && $perfil->tipo != 7 && $perfil->tipo != 10 && $log->id != 1648)
                    return response()->json(['error' => 'Unauthorized'], 401);
            }


            $token = Str::random(60);
            $log->api_token = hash('sha256', $token);
            $log->save();

            $player_id = DB::table("tma_playerid")->where("id_player", request("id_player"))->first();

            if (!isset($player_id)) {
                // show($player_id);
                if (!is_null(request("id_player"))) {
                    DB::table("tma_playerid")
                        ->insert(
                            ['users_id' => $log->id, 'id_player' => request("id_player"), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
                        );
                }
            } else {

                if ($log->id != $player_id->users_id) {
                    DB::table("tma_playerid")
                        ->where('id', $player_id->id)
                        ->update(["users_id" => $log->id, 'updated_at' => date('Y-m-d H:i:s')]);
                }
            }


            $module = UsuarioModuloModel::join("ad_modulos as a", "a.id", "=", "ad_usuario_modulo.id_modulo")
                ->where("ad_usuario_modulo.id_usuario", $log->id)
                ->where("a.estado", "ACT")
                ->select("a.*")
                ->get();
            $rutas = array();
            foreach ($module as $key => $value) {
                # code...
                $rutas[] = $value->ruta;
            }
            //$rutas = substr ($rutas, 0, strlen($rutas) - 1);
            //show($module->toarray());
            $user = $log->toarray();
            $prfil = PerfilModel::where('id', $log->id_perdil)->first();

            // Cambia el perfil de secrataria a alcalde 
            $perfil_alcalde = explodewords(ConfigSystem("perfil_alcalde"), "|");
            foreach ($perfil_alcalde as $key => $value) {
                # code...
                if ($value == request('cedula')) {
                    # code...
                    $user["id_perfil"] = 12;
                    $perfil->tipo = 4;
                }
            }
            // dd($perfil->tipo);
            $user["modulos"] = $rutas;
            if ($perfil->tipo == 10) {
                $user["tipo"] = 3;
            } else {
                $user["tipo"] = (isset($perfil->tipo)) ? $perfil->tipo : null;
            }

            return $user;
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    function verificarToken($token)
    {
        $user = User::where([["estado", "ACT"], ["api_token", $token]])->first();
        // dd($user);

        $version = ConfigSystem("version");
        if (isset($user)) {
            if ($user->api_token == $token) {

                return response()->json(['ok' => true, 'data' => $token, 'version' => $version]);;
            }
        } else {
            return response()->json(['error' => 'Unauthorized', 'version' => $version], 200);
        }
    }



    function getobras()
    {
        $obras = CoorObrasModel::join("coor_tmae_contratista as a", "a.id", "=", "coor_tmov_obras_listado.id_contratista")
            ->join("coor_tmae_tipo_obra as b", "b.id", "=", "coor_tmov_obras_listado.id_tipo_obra")
            ->join("parroquia as c", "c.id", "=", "coor_tmov_obras_listado.id_parroquia")
            ->select("coor_tmov_obras_listado.*", "a.nombres as contratista", "b.tipo as tipoobra", "c.parroquia", DB::raw("'#00000' as color"))
            ->where(function ($query) {
                $query->where("coor_tmov_obras_listado.estado", "ACT");
                if (Input::has("estado"))
                    $query->where("coor_tmov_obras_listado.estado_obra", Input::get("estado"));
                if (Input::has("parroquia"))
                    $query->where("c.id", Input::get("parroquia"));
                if (Input::has("estado"))
                    $query->where("coor_tmov_obras_listado.estado_obra", Input::get("estado"));
            })
            ->where("coor_tmov_obras_listado.id_tipo_obra", "<>", 6)
            ->whereIn("coor_tmov_obras_listado.estado_obra", array("EJECUCION", "POR_EJECUTAR", "SUSPENDIDO"))
            ->orderby("c.parroquia") //->orderby("id","desc")
            ->get();
        $obras = colorobjestado($obras, "estado_obra");
        return $obras;
    }
    function getobrasid($id)
    {
        $obras = CoorObrasModel::join("coor_tmae_contratista as a", "a.id", "=", "coor_tmov_obras_listado.id_contratista")
            ->join("coor_tmae_tipo_obra as b", "b.id", "=", "coor_tmov_obras_listado.id_tipo_obra")
            ->join("parroquia as c", "c.id", "=", "coor_tmov_obras_listado.id_parroquia")
            ->select("coor_tmov_obras_listado.*", "a.nombres as contratista", "b.tipo as tipoobra", "c.parroquia", DB::raw("'#00000' as color"))
            ->where("coor_tmov_obras_listado.estado", "ACT")
            ->where("coor_tmov_obras_listado.id", $id)
            ->first();
        $obras = colorobjestado($obras, "estado_obra", 1);
        return $obras;
    }
    function getobrastotal()
    {
        $obras = CoorObrasModel::select("estado_obra", DB::raw("count(*) as total"), DB::raw("'#00000' as color"), DB::raw("sum(monto) as monto"))
            ->groupby("estado_obra")
            ->where("coor_tmov_obras_listado.estado", "ACT")
            ->whereIn("estado_obra", array("EJECUCION", "POR_EJECUTAR", "SUSPENDIDO"))
            ->where("coor_tmov_obras_listado.id_tipo_obra", "<>", 6)
            ->orderby("id", "desc")->get(); //->paginate(500);
        $obras = colorobjestado($obras, "estado_obra");
        return $obras;
    }
    function getobrasparroquias()
    {
        $obras = CoorObrasModel::join("coor_tmae_contratista as a", "a.id", "=", "coor_tmov_obras_listado.id_contratista")
            ->join("coor_tmae_tipo_obra as b", "b.id", "=", "coor_tmov_obras_listado.id_tipo_obra")
            ->join("parroquia as c", "c.id", "=", "coor_tmov_obras_listado.id_parroquia")
            ->select("c.id", "c.parroquia", DB::raw("count(*) as total"))
            ->where(function ($query) {
                $query->where("coor_tmov_obras_listado.estado", "ACT");
            })
            ->whereIn("coor_tmov_obras_listado.estado_obra", array("EJECUCION", "POR_EJECUTAR", "SUSPENDIDO"))
            ->where("coor_tmov_obras_listado.id_tipo_obra", "<>", 6)
            ->groupby("c.id", "c.parroquia")
            ->orderby(DB::raw("count(*)"), "desc")->get(); //->paginate(500);
        return $obras;
    }

    function getobrasparroquiasid($id)
    {
        $obras = CoorObrasModel::join("coor_tmae_contratista as a", "a.id", "=", "coor_tmov_obras_listado.id_contratista")
            ->join("coor_tmae_tipo_obra as b", "b.id", "=", "coor_tmov_obras_listado.id_tipo_obra")
            ->join("parroquia as c", "c.id", "=", "coor_tmov_obras_listado.id_parroquia")
            ->select("coor_tmov_obras_listado.*", "a.nombres as contratista", "b.tipo as tipoobra", "c.parroquia", DB::raw("'#00000' as color"))
            ->where(function ($query) use ($id) {
                $query->where("coor_tmov_obras_listado.estado", "ACT")
                    ->where("c.id", $id);
            })
            ->whereIn("coor_tmov_obras_listado.estado_obra", array("EJECUCION", "POR_EJECUTAR", "SUSPENDIDO"))
            ->where("coor_tmov_obras_listado.id_tipo_obra", "<>", 6)
            //->orderby("id","desc")->get();//->paginate(500);
            ->orderby("coor_tmov_obras_listado.estado_obra")
            ->get();
        $obras = colorobjestado($obras, "estado_obra");
        return $obras;
    }
    //ACTIVIDADES
    function getactividades()
    {
        $actividades = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*", "a.direccion", DB::raw("'#00000' as color"))
            ->where("coor_tmov_cronograma_cab.estado", "ACT")
            ->whereIn("coor_tmov_cronograma_cab.estado_actividad", ["EJECUCION", "POR_EJECUTAR"])
            ->orderby("coor_tmov_cronograma_cab.fecha_fin")->get();
        $actividades = colorobjestado($actividades, "estado_actividad");
        return $actividades;
    }
    function getactividadesid($id)
    {
        $actividades = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*", "a.direccion", DB::raw("'#00000' as color"))
            ->where("coor_tmov_cronograma_cab.estado", "ACT")
            ->where("coor_tmov_cronograma_cab.id", $id)
            ->first();
        $actividades = colorobjestado($actividades, "estado_actividad", 1);
        return $actividades;
    }
    function getactividadestotal()
    {
        $actividades = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.estado_actividad", DB::raw("count(*) as total"), DB::raw("'#00000' as color"))
            ->groupby("coor_tmov_cronograma_cab.estado_actividad")
            ->where("coor_tmov_cronograma_cab.estado", "ACT")
            ->whereIn("coor_tmov_cronograma_cab.estado_actividad", array("EJECUCION", "POR_EJECUTAR"))
            ->orderby("coor_tmov_cronograma_cab.fecha_inicio")->get();
        $actividades = colorobjestado($actividades, "estado_actividad");
        return $actividades;
    }
    function getactividadesdireccion()
    {
        $actividades = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
            ->select("a.id", DB::raw("replace(a.direccion,'DIRECCION DE ','') as direccion "), DB::raw("count(*) as total"))
            ->groupby("a.id", "a.direccion")
            //->where([["coor_tmov_cronograma_cab.estado","ACT"],["coor_tmov_cronograma_cab.estado_actividad","<>", "EJECUTADO"]])
            ->where("coor_tmov_cronograma_cab.estado", "ACT")
            ->whereIn("coor_tmov_cronograma_cab.estado_actividad", array("EJECUCION", "POR_EJECUTAR"))

            ->get();
        //show("aqui");
        return $actividades;
    }
    function getactividadesdireccionid($id)
    {
        $actividades = CoorCronogramaCabModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*", "a.direccion", DB::raw("'#00000' as color"))
            //->where([["coor_tmov_cronograma_cab.estado","ACT"],["coor_tmov_cronograma_cab.estado_actividad","<>", "EJECUTADO"]])
            ->where("coor_tmov_cronograma_cab.estado", "ACT")
            ->whereIn("coor_tmov_cronograma_cab.estado_actividad", array("EJECUCION", "POR_EJECUTAR"))
            ->where("a.id", $id)
            ->orderby("coor_tmov_cronograma_cab.fecha_fin")->get();
        $actividades = colorobjestado($actividades, "estado_actividad");
        return $actividades;
    }
    //ACTIVIDES DE LA CIUDAD
    function getcomunicacionactitotal()
    {
        $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
            ->select(
                "com_tmov_comunicacion_cab.estado_actividad",
                DB::raw("count(*) as total"),
                DB::raw("'#00000' as color")
            )
            ->where("com_tmov_comunicacion_cab.estado", "ACT")
            ->groupby("com_tmov_comunicacion_cab.estado_actividad")
            ->orderby("com_tmov_comunicacion_cab.fecha_fin")
            ->get();
        $tabla = colorobjestado($tabla, "estado_actividad");
        return $tabla;
    }
    function getcomunicacionactilista()
    {
        $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
            ->select(
                "com_tmov_comunicacion_cab.*",
                "ac.nombre",
                DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"),
                DB::raw("'#00000' as color")
            )
            ->where("com_tmov_comunicacion_cab.estado", "ACT")
            ->orderby("com_tmov_comunicacion_cab.fecha_fin")
            ->get();
        $tabla = colorobjestado($tabla, "estado_actividad");
        return $tabla;
    }
    function getcomunicacionactidireccion()
    {
        $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
            ->join("tmae_direcciones as di", "di.id", "=", "com_tmov_comunicacion_cab.id_direccion")
            ->select("di.id", "di.direccion", DB::raw("count(*) as total"))
            ->where("com_tmov_comunicacion_cab.estado", "ACT")
            ->groupby("di.direccion")
            ->orderby("com_tmov_comunicacion_cab.fecha_fin")
            ->get();
        return $tabla;
    }
    function getcomunicacionactidireccionid($id)
    {
        $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
            ->join("tmae_direcciones as di", "di.id", "=", "com_tmov_comunicacion_cab.id_direccion")
            ->select("com_tmov_comunicacion_cab.*", "ac.nombre as actividad", "di.direccion", DB::raw("'#00000' as color"))
            ->where("com_tmov_comunicacion_cab.estado", "ACT")
            ->where("di.id", $id)
            ->orderby("com_tmov_comunicacion_cab.fecha_fin")
            ->get();
        $tabla = colorobjestado($tabla, "estado_actividad");
        return $tabla;
    }
    function getcomunicacionactiid($id)
    {
        $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
            ->select(
                "com_tmov_comunicacion_cab.*",
                "ac.nombre",
                DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"),
                DB::raw("'#00000' as color")
            )
            ->where("com_tmov_comunicacion_cab.estado", "ACT")
            ->where("com_tmov_comunicacion_cab.id", $id)
            ->orderby("com_tmov_comunicacion_cab.fecha_fin")
            ->first();
        $tabla = colorobjestado($tabla, "estado_actividad", 1);
        return $tabla;
    }

    /*Obtener el total por tipos*/
    function getcomponentes()
    {
        $tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as ac", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "ac.id")
            ->select("coor_tmov_proyecto_estrategico.id_tipo_proyecto", "ac.tipo", DB::raw("count(*) as total"))
            ->where("coor_tmov_proyecto_estrategico.estado", "ACT")
            ->groupby("ac.tipo")
            ->orderby("coor_tmov_proyecto_estrategico.id", "desc")->get();
        return $tabla;
    }

    /** Obtener todos los componentes de id */
    function getcomponentesbyid($id)
    {
        $tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as ac", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "ac.id")
            ->select("coor_tmov_proyecto_estrategico.*", "ac.tipo")
            ->where("coor_tmov_proyecto_estrategico.estado", "ACT")
            ->where("coor_tmov_proyecto_estrategico.id_tipo_proyecto", $id)
            ->orderby("coor_tmov_proyecto_estrategico.id", "desc")->get();

        return $tabla;
    }

    function getsumacomponentestotal()
    {
        $obras = CoorProyectoEstrategicoModel::select("coor_tmov_proyecto_estrategico.id_tipo_proyecto", DB::raw("count(*) as total"), DB::raw("sum(monto) as monto"))
            ->groupby("id_tipo_proyecto")
            ->where("coor_tmov_proyecto_estrategico.estado", "ACT")
            // ->whereIn("estado_obra",array("EJECUCION","POR_EJECUTAR"))
            // ->where("coor_tmov_obras_listado.id_tipo_obra","<>",6)
            ->orderby("id", "desc")->get(); //->paginate(500);
        // $obras=colorobjestado($obras,"estado_obra");
        return $obras;
    }




    /*Para obtener los detalles*/
    function getproyectosbyid($id)
    {
        $tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as ac", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "ac.id")
            ->select("coor_tmov_proyecto_estrategico.*", "ac.tipo")
            ->where("coor_tmov_proyecto_estrategico.estado", "ACT")
            ->where("coor_tmov_proyecto_estrategico.id", $id)
            ->orderby("coor_tmov_proyecto_estrategico.id", "desc")->get();

        return $tabla;
    }

    /** Total de Proyectos */
    function getproyectototal()
    {
        $tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as ac", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "ac.id")
            ->select(
                "coor_tmov_proyecto_estrategico.estado_proyecto",
                "ac.tipo",
                DB::raw("count(*) as total"),
                DB::raw("'#00000' as color")
            )
            ->where("coor_tmov_proyecto_estrategico.estado", "ACT")
            ->groupby("coor_tmov_proyecto_estrategico.estado_proyecto")
            ->orderby("coor_tmov_proyecto_estrategico.id", "desc")->get();
        $tabla = colorobjestadoproyecto($tabla, "estado_proyecto");
        return $tabla;
    }


    /** Listado de peticiones */
    function getpeticionestotalgrafico()
    {
        return $this->ChartAlcaldia->getValoresCharts("PENDIENTE");
    }

    function getpeticiones()
    {
        if (Input::has("director")) {
            $tabla = UsuariosModel::join("tmae_direcciones as a", "a.id", "=", "users.id_direccion")
                ->select("users.id", DB::raw("replace(a.direccion,'DIRECCION DE ','') as direccion "), "users.name", "users.cargo")
                ->where("users.id_perfil", 3)
                ->where("users.cargo", "like", "%DIRECTOR%")
                ->where("users.name", "not like", "%prueba%")
                ->where("users.name", "not like", "%VELASQUEZ REYES FRANCISCO GABRIEL%")
                ->where("users.name", "not like", "%Ericka Macias%")
                ->orWhere('users.id', 45)->orWhere('users.id', 88)->orWhere('users.id', 121)
                ->orWhere('users.id', 175)->orWhere('users.id', 1019)
                ->orWhere('users.id', 1469)->orWhere('users.id', 1680)->orWhere('users.id', 1668)

                ->orWhere('users.id', 1682)->orWhere('users.id', 1683)->orWhere('users.id', 1684)
                ->orWhere('users.id', 1687)->orWhere('users.id', 1688)->orWhere('users.id', 1689)

                ->orWhere('users.id', 1633)->orWhere('users.id', 1648)
                ->orWhere('users.id', 1630)->orWhere('users.id', 1649)->orWhere('users.id', 1676)
                ->orWhere('users.id', 1651)->orWhere('users.id', 1634)
                ->orWhere('users.id', 1641)->orWhere('users.id', 1646)->orWhere('users.id', 1182)
                ->orderby("cargo", "asc");
            $id = intval(Input::get("director"));
            if ($id != 0)
                $tabla = $tabla->select("users.id", "a.direccion", "users.name", "users.cargo", "users.email", "users.cedula", "users.telefono")->where("users.id", $id)->first();
            else
                $tabla = $tabla->get();
        } else {
            $tabla = TramAlcaldiaCabModel::where("estado", "ACT")
                ->select("*", DB::raw(" DATE_FORMAT(created_at,'%Y-%m-%d') as created_at_n"), DB::raw(" DATE_FORMAT(updated_at,'%Y-%m-%d') as updated_at_n"), DB::raw("case when archivo <>'' then concat('" . URL::to("/") . "/archivos_sistema/',archivo) else '' end as archivo"))
                ->where(function ($query) {
                    if (Input::has("prioridad"))
                        $query->where("prioridad", Input::get("prioridad"));
                })
                ->where("disposicion", "PENDIENTE")
                ->orderby("updated_at")
                ->orderby("prioridad")
                ->orderby("disposicion");
            if (Input::has("idtramite"))
                $tabla = $tabla->where("id", Input::get("idtramite"))->first();
            else
                $tabla = $tabla->get();
        }
        return $tabla;
    }



    /**
     * obtener el total de tramites
     */
    function getTramitestotal()
    {
        $tabla = TramAlcaldiaCabModel::select(
            DB::raw("count(*) as valor")
        )
            ->where("tram_peticiones_cab.estado", "ACT")
            ->where("tram_peticiones_cab.disposicion", "=", "PENDIENTE")
            ->groupby("tram_peticiones_cab.disposicion")
            ->orderby("tram_peticiones_cab.id", "desc")->get();

        $tablaTotal = TramAlcaldiaCabModel::select(
            DB::raw("count(*) as valor")
        )
            ->where("tram_peticiones_cab.estado", "ACT")
            ->where("tram_peticiones_cab.disposicion", "not like", "%PENDIENTE%")
            ->where("tram_peticiones_cab.estado_proceso", "=", 0)
            ->orderby("tram_peticiones_cab.id", "desc")->get();

        $tablaEstado = TramAlcaldiaCabModel::select(
            DB::raw("count(*) as valor")
        )
            ->where("tram_peticiones_cab.estado", "ACT")
            ->where("tram_peticiones_cab.estado_proceso", "=", 1)
            ->orderby("tram_peticiones_cab.id", "desc")->get();

        $datos[] = ["valor" => $tabla[0]->valor];
        $datos[] = ["valor" => $tablaTotal[0]->valor];
        $datos[] = ["valor" => $tablaEstado[0]->valor];
        return $datos;
    }


    public function EnviarEmail($id, $email, $nombres, $asunto = "Trámite Despachado")
    {


        $tabla = TramAlcaldiaCabModel::where([["estado", "ACT"], ["id", $id]])->first();
        $tabladel = TramAlcaldiaDelModel::join("users as us", "us.id", "=", "tram_peticion_del.id_usuario")
            ->where([["tram_peticion_del.estado", "ACT"], ["id_tram_cab", $id]])->first();



        Mail::send(
            'vistas.reporteshtml.reportemail',
            ["tabla" => $tabla, "tabladel" => $tabladel],
            function ($m) use ($email, $nombres, $asunto) {
                $m->from("alcaldia@manta.gob.ec", "GAD Municipal Manta");
                $m->to($email, $nombres)->subject($asunto);
            }
        );
        return "";
    }
    public function apruebatramite(Request $request)
    {
        if ($request->_token) {
            $token = UsuariosModel::where("api_token", $request->_token)->first();
            if (!$token)
                return response()->json(['error' => 'Unauthorized'], 401);
            $id = intval($request->id);
            $observacion = trim($request->observacion);
            $disposicion = trim($request->disposicion);
            $ip = trim($request->ip);
            $pc = trim($request->pc);
            $delegados = $request->delegados;
            $tabla = TramAlcaldiaCabModel::find($id);
            if (!$tabla)
                return response()->json(['error' => 'No existe el trámite.'], 401);
            DB::beginTransaction();
            try {
                $tabla->observacion = $observacion;
                $tabla->disposicion = $disposicion;
                $tabla->id_usuario = $token->id;
                $tabla->fecha_respuesta = date("Y-m-d");
                $tabla->ip = $ip;
                $tabla->pc = $pc;
                $tabla->save();
                //Delegados
                if ($delegados != "") {
                    $dele = json_decode($delegados, true);
                    foreach ($dele as $key => $value) {
                        # code...
                        $savedele = TramAlcaldiaDelModel::where("id_tram_cab", $id)->where("id_usuario", $value["id"])->first();
                        if (!$savedele)
                            $savedele = new TramAlcaldiaDelModel;
                        $savedele->id_tram_cab = $id;
                        $savedele->id_usuario = $value["id"];
                        $savedele->save();
                    }
                }
                //Historial
                $histoact = TramAlcaldiaCabDetaModel::where('id_tram_cab', $id)->update(['estado' => 'INA']);
                $tabla = TramAlcaldiaCabModel::select(DB::raw("numtramite,remitente,asunto,peticion,prioridad,recomendacion,disposicion,observacion,id_usuario,ip,pc,id,archivo,fecha_ingreso,fecha_respuesta,'$delegados' as delegados,'" . date('Y-m-d H:i:s') . "' as created_at,'" . date('Y-m-d H:i:s') . "' as updated_at"))
                    ->where("id", $id);
                $bindings = $tabla->getBindings();
                $insertQuery = "INSERT into tram_peticiones_cab_deta(numtramite,remitente,asunto,peticion,prioridad,recomendacion,disposicion,observacion,id_usuario,ip,pc,id_tram_cab,archivo,fecha_ingreso,fecha_respuesta,delegados_json,created_at,updated_at)"
                    . $tabla->toSql();
                DB::insert($insertQuery, $bindings);

                //return $insertQuery;
                //return response()->json($tabla->get());
                //return response()->json(json_decode($delegados));
                DB::commit();


                $this->EnviarEmail($id, 'cgpg94@gmail.com', "Gonzalo");

                //DB::rollback();
                return response()->json(["success", "Grabado Exitosamente"]);
            } //Try Transaction
            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage();
                return response()->json(['error' => $mensaje], 401);
            }
        } else
            return response()->json(['error' => 'Unauthorized'], 401);
    }
    /*Indicadores APP*/
    public function getindicadoresmain()
    {
        $tabla = CoorIndicadorDetalleModel::where("ind_tmov_indicadores_deta.estado", "ACT")
            ->join("ind_tmae_indicador_tipo as a", "a.id", "=", "ind_tmov_indicadores_deta.id_indicador_mae");
        //$group=clone $tabla;
        $group = $tabla->groupBy("a.detalle", "a.icono_app")
            ->select("a.id", "a.detalle", "a.icono_app", "a.tipografico", "a.medida", "a.color", "a.fuente", DB::raw("count(*) as total"))
            ->orderby("a.orden")
            ->get();
        //return $group;
        $resul = array();
        $sumto = 0;
        $counpro = 0;
        foreach ($group as $g => $valg) {

            $idtipo = $valg->id;
            $detalle = $valg->detalle;
            $tipografico = $valg->tipografico;
            $medida = $valg->medida;
            $color = $valg->color;
            $icono_app = $valg->icono_app;
            $fuente = $valg->fuente;
            $parroquias = CoorIndicadorDetalleModel::where("ind_tmov_indicadores_deta.estado", "ACT")
                ->join("ind_tmae_indicador_tipo as a", "a.id", "=", "ind_tmov_indicadores_deta.id_indicador_mae")
                ->join("parroquia as c", "c.id", "=", "ind_tmov_indicadores_deta.id_parroquia")
                ->select("ind_tmov_indicadores_deta.*", "a.detalle", "c.parroquia")
                ->where("ind_tmov_indicadores_deta.id_indicador_mae", $idtipo)
                ->orderby("a.id")
                ->orderby("c.zona", "desc")
                ->get();
            $arpa = array();
            $acum = 0;
            $tipocam = "";
            foreach ($parroquias as $keyp => $valuep) {
                # code...
                $json = json_decode($valuep->json, true);
                $jsondata = array();
                $to = 0;
                $coval = 0;
                foreach ($json as $key => $value) {
                    # code...
                    $campos = CoorIndicadorTipoFiltroModel::where("id_indicador_mae", $idtipo)
                        ->where("id_campotxt", $key)
                        ->first();
                    $jsondata[] = array("rubro" => $campos->filtro, "valor" => $value);
                    //if($campos->tipo=="VAL") {
                    $to += floatval($value);
                    if (floatval($value) != 0)
                        $coval++;
                    //}
                    $tipocam = $campos->tipo;
                    $counpro++;
                }
                //$co=count($jsondata);
                $acum += $to;
                $toti = $to;
                if ($tipocam == "POR")
                    $toti = round($toti / $coval, 2);
                if ($coval == 1)
                    $toti = "";
                $par = array("parroquia" => $valuep->parroquia, "total" => $toti, "indicador" => $jsondata);
                $arpa[] = $par;
            }
            $sumto += $acum;
            if ($tipocam == "POR")
                $sumto = round($sumto / $counpro, 2);
            array_push($resul, array(
                "id" => $idtipo,
                "detalle" => $detalle,
                "icono_app" => asset($icono_app),
                "tipografico" => $tipografico,
                "color" => $color,
                "fuente" => $fuente,
                "medida" => $medida,
                "sumatotal" => $sumto,
                "detalletotales" => $arpa
            ));
            $sumto = 0;
            $counpro = 0;
            /*$resul[]=array(
                "id"=>$idtipo,
                "detalle"=>$detalle,
                "totales"=>$arpa
            );*/
        }
        //dd($resul);
        return $resul;
    }
    //Filtro para Combo
    public function getindicadorescombo()
    {
        $table = $this->getindicadoresmain();
        $res = array();
        foreach ($table as $key => $value) {
            # code...
            unset($value["detalletotales"]);
            $res[] = $value;
        }
        return $res;
    }
    /**/
    /*Rendimiento*/
    public function getrendimientoactividadesapp()
    {
        $datostodos = $this->DatosActividad->ajaxreportehtmlfiltrosactividades("si")->getData();
        $rendimientos = collect($datostodos['rendimiento']);
        //show($rendimientos);
        return $rendimientos;
    }
    public function getCompromisosAlcaldia($tipo, $id_direccion)
    {
        switch ($tipo) {
            case 1:
                $composentesql = "SELECT aa.nombre AS componente from coor_tmae_cronograma_componente_cab aa 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bb on aa.id=bb.id_componente
            where bb.id=coor_tmov_compromisos_cab.id_objetivo_componente";
                $objetivosql = "SELECT bx.objetivo from coor_tmae_cronograma_componente_cab ax 
            INNER JOIN coor_tmae_cronograma_componente_objetivo bx on ax.id=bx.id_componente
            WHERE bx.id=coor_tmov_compromisos_cab.id_objetivo_componente";
                $programasql = str_replace("bx.objetivo", "bx.programa", $objetivosql);
                /*============================*/
                $coordinacion = "SELECT nombre from coor_tmae_coordinacion_main AS a INNER JOIN coor_tmov_coordinacion_direccion_deta AS b
	                    WHERE a.id=b.id_coor_cab AND b.id_direccion=coor_tmov_compromisos_cab.id_direccion
            ";
                $tabla = CabCompromisoalcaldiaModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_compromisos_cab.id_direccion")
                    ->join("parroquia as p", "p.id", "=", "coor_tmov_compromisos_cab.id_parroquia")
                    ->join("users as u", "u.id", "=", "coor_tmov_compromisos_cab.id_usuario")
                    ->select(
                        "coor_tmov_compromisos_cab.*",
                        "a.direccion",
                        "p.parroquia",
                        "u.name as usuario",
                        DB::raw("(select GROUP_CONCAT(x.name SEPARATOR ', ') from coor_tmov_compromisos_resposables AS y inner join users as x on x.id = y.id_usuario where y.id_crono_cab = coor_tmov_compromisos_cab.id limit 1) as idusuario"),
                        DB::raw("ifnull(($composentesql),'') as componente"),
                        DB::raw("ifnull(($objetivosql),'') as objetivo"),
                        DB::raw("ifnull(($programasql),'') as programa"),
                        DB::raw("ifnull(($coordinacion),'') as coordinacion")
                    )
                    ->where("coor_tmov_compromisos_cab.estado", "ACT")
                    ->orderby("coor_tmov_compromisos_cab.id", "desc")->get();

                return $tabla;
                break;
            case 2:
                $tabla = CabCompromisoalcaldiaModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_compromisos_cab.id_direccion")
                    ->join("parroquia as p", "p.id", "=", "coor_tmov_compromisos_cab.id_parroquia")
                    ->join("users as u", "u.id", "=", "coor_tmov_compromisos_cab.id_usuario")
                    ->select('a.id as id', DB::raw('count(coor_tmov_compromisos_cab.id) as total'), DB::raw('IFNULL(AVG(coor_tmov_compromisos_cab.avance),0) as avance'), 'a.direccion')
                    ->where("coor_tmov_compromisos_cab.estado", "ACT")
                    ->orderby("avance", "desc")
                    ->groupby("a.direccion")
                    ->get();
                return $tabla;
                break;
            case 3:
                $composentesql = "SELECT aa.nombre AS componente from coor_tmae_cronograma_componente_cab aa 
                INNER JOIN coor_tmae_cronograma_componente_objetivo bb on aa.id=bb.id_componente
                where bb.id=coor_tmov_compromisos_cab.id_objetivo_componente";
                $objetivosql = "SELECT bx.objetivo from coor_tmae_cronograma_componente_cab ax 
                INNER JOIN coor_tmae_cronograma_componente_objetivo bx on ax.id=bx.id_componente
                WHERE bx.id=coor_tmov_compromisos_cab.id_objetivo_componente";
                $programasql = str_replace("bx.objetivo", "bx.programa", $objetivosql);
                /*============================*/
                $coordinacion = "SELECT nombre from coor_tmae_coordinacion_main AS a INNER JOIN coor_tmov_coordinacion_direccion_deta AS b
                            WHERE a.id=b.id_coor_cab AND b.id_direccion=coor_tmov_compromisos_cab.id_direccion
                ";
                $tabla = CabCompromisoalcaldiaModel::join("tmae_direcciones as a", "a.id", "=", "coor_tmov_compromisos_cab.id_direccion")
                    ->join("parroquia as p", "p.id", "=", "coor_tmov_compromisos_cab.id_parroquia")
                    ->join("users as u", "u.id", "=", "coor_tmov_compromisos_cab.id_usuario")
                    ->select(
                        "coor_tmov_compromisos_cab.*",
                        "a.direccion",
                        "p.parroquia",
                        "u.name as usuario",
                        DB::raw("(select GROUP_CONCAT(x.name SEPARATOR ', ') from coor_tmov_compromisos_resposables AS y inner join users as x on x.id = y.id_usuario where y.id_crono_cab = coor_tmov_compromisos_cab.id limit 1) as idusuario"),
                        DB::raw("ifnull(($composentesql),'') as componente"),
                        DB::raw("ifnull(($objetivosql),'') as objetivo"),
                        DB::raw("ifnull(($programasql),'') as programa"),
                        DB::raw("ifnull(($coordinacion),'') as coordinacion")
                    )
                    ->where(["coor_tmov_compromisos_cab.estado" => "ACT", "coor_tmov_compromisos_cab.id_direccion" => $id_direccion])
                    ->orderby("coor_tmov_compromisos_cab.id", "desc")->get();
                return $tabla;
                break;
        }
    }
    public function getPoaActividadesapp($id_direccion)
    {
        // $id_direccion=Input::get('id_direccion');

        $actividades = CoorPoaCabModel::join("tmae_direcciones as a", "a.id", "poa_tmae_cab.id_direccion")
            ->select('poa_tmae_cab.*', 'a.direccion', 'a.alias')
            ->where(['poa_tmae_cab.estado' => 'ACT', 'id_direccion' => $id_direccion])

            ->orderBy('id_direccion')->get();
        $actividades_collect = collect($actividades);
        $multiplied = $actividades_collect->map(function ($item, $key) {

            $subActividades = CoorPoaActividadCabModel::join("tmae_direcciones as a", "a.id", "poa_tmae_cab_actividad.id_direccion_act")
                ->select('poa_tmae_cab_actividad.id as id_actividad_poa', 'id_direccion_act', 'a.direccion', 'a.alias', 'actividad', 'fecha_inicio', 'fecha_fin', 'observacion', 'estado_actividad', 'avance')
                ->where('id_poa', $item->id)
                ->where(['poa_tmae_cab_actividad.estado' => 'ACT'])
                ->get();
            $avance = CoorPoaActividadCabModel::join("tmae_direcciones as a", "a.id", "poa_tmae_cab_actividad.id_direccion_act")
                ->select(DB::raw('avg(IFNULL(avance,0)) as promedio'))
                ->where('id_poa', $item->id)
                ->where(['poa_tmae_cab_actividad.estado' => 'ACT'])
                ->groupBy('id_poa')
                ->first();

            // dd($avance);
            return collect(
                [
                    'id' => $item->id,
                    'proyecto' => $item->proyecto,
                    'id_direccion' => $item->id_direccion,
                    'direccion' => $item->direccion,
                    'alias' => $item->alias,
                    'monto_asignado' => $item->monto_asignado,
                    'total_asignado' => $item->total_asignado,
                    'estado_poa' => $item->estado_poa,
                    'promedio_avance' => $avance['promedio'],
                    'actividades' => $subActividades

                ]
            );
        });
        return $multiplied;
    }
    public function getPoaActividadesResumenapp()
    {

        $direciones = CoorPoaCabModel::join("tmae_direcciones as a", "a.id", "poa_tmae_cab.id_direccion")
            ->select('a.id', 'a.direccion', 'a.alias')
            ->where(['poa_tmae_cab.estado' => 'ACT'])
            ->groupBy('id_direccion')
            ->orderBy('id_direccion')
            ->get();
        $direcciones_collect = collect($direciones);
        $multiplied = $direcciones_collect->map(function ($item, $key) {

            $actividades = CoorPoaCabModel::join("tmae_direcciones as a", "a.id", "poa_tmae_cab.id_direccion")
                ->select('poa_tmae_cab.*', 'a.direccion', 'a.alias')
                ->where(['poa_tmae_cab.estado' => 'ACT', "poa_tmae_cab.id_direccion" => $item->id])
                ->orderBy('id_direccion')
                ->get();

            $actividades_collect = collect($actividades);
            $multiplied_act = $actividades_collect->map(function ($item_act, $key_act) {
                $avance = CoorPoaActividadCabModel::join("tmae_direcciones as a", "a.id", "poa_tmae_cab_actividad.id_direccion_act")
                    ->select(DB::raw('avg(IFNULL(avance,0)) as promedio'))
                    ->where('id_poa', $item_act->id)
                    ->where(['poa_tmae_cab_actividad.estado' => 'ACT'])
                    ->groupBy('id_poa')
                    ->first();
                return collect(['avance' => (isset($avance['promedio'])) ? floatval($avance['promedio']) : 0.0000]);
            });
            $promedios = collect($multiplied_act);
            return collect(
                [
                    'id' => $item->id,
                    'direccion' => $item->direccion,
                    'alias' => $item->alias,
                    'avance' => round($promedios->avg('avance'), 2)
                ]
            );
        });
        return $multiplied;
    }
    public function getPoaActividadesDireccionapp()
    {
        $actividades = CoorPoaCabModel::join("tmae_direcciones as a", "a.id", "poa_tmae_cab.id_direccion")
            ->select('poa_tmae_cab.id_direccion', 'a.direccion', 'a.alias', DB::raw('count(poa_tmae_cab.id_direccion) as total'))
            ->where(['poa_tmae_cab.estado' => 'ACT'])
            ->orderBy('id_direccion')
            ->groupBy('id_direccion')
            ->get();
        $subActividades = CoorPoaActividadCabModel::join("tmae_direcciones as a", "a.id", "poa_tmae_cab_actividad.id_direccion_act")
            ->select('id_direccion_act', 'a.direccion', DB::raw('count(id_direccion_act) as total'))
            ->where(['poa_tmae_cab_actividad.estado' => 'ACT'])
            ->groupBy('id_direccion_act')
            ->get();


        return ['poa_direccion' => $actividades, 'sub_actividades_poa_direccion' => $subActividades];
    }



    public function getPredios()
    {
        $documento = Input::get("documento");
        $url = "http://webserver.manta.gob.ec:81/api/Operaciones/consultaPropiedades/$documento";
        $client = new GuzzleHttpClient();
        try {
            $respuesta = $client->request('GET', $url);
            $consultawebserviceobs = json_decode($respuesta->getBody()->getContents(), true);
            return $consultawebserviceobs;
        } catch (\Exception $e) {
            //return array("error",$e->getMessage());
            return redirect()->back()->withErrors(["message" => $e->getMessage()]);
        }
    }
    public function getMultasUnaser()
    {
        $documento = Input::get("placa");
        $id = Input::get("id");

        try {


            if ($id) {
                $multas = MultasUnaserModel::select('tt.tipo', 'una_tmo_multas.*')
                    ->join('una_tmo_multas_tipo as tt', 'tt.id', '=', 'una_tmo_multas.id_tipo_multa')
                    ->where(['una_tmo_multas.estado' => 'ACT', 'una_tmo_multas.id' => $id])->first();
                return $multas;
            } elseif ($documento) {
                $multas = MultasUnaserModel::select('tt.tipo', 'una_tmo_multas.*')
                    ->join('una_tmo_multas_tipo as tt', 'tt.id', '=', 'una_tmo_multas.id_tipo_multa')
                    ->where(['una_tmo_multas.estado' => 'ACT', 'placa' => $documento])->get();
                return ['estado' => true, 'data' => $multas];
            }
        } catch (\Exception $e) {
            //return array("error",$e->getMessage());
            return ['estado' => false, "message" => $e->getMessage()];
        }
    }


    public function consultarvalorMulta()
    {
        $tipo = Input::get("tipo");

        try {
            $multas = MultasUnaserTipoModel::where(['estado' => 'ACT', 'id' => $tipo])->first();
            // dd(floatval($multas->porcentaje));
            $valor = ((400 / 100) * floatval($multas->porcentaje));
            return ['estado' => true, 'valor' => number_format($valor, 2)];
        } catch (\Exception $e) {
            //return array("error",$e->getMessage());
            return ['estado' => false, "message" => $e->getMessage()];
        }
    }



    //Seccion de indicadores (Marce)

    //Alcalcia - Tramites
    public function IndicadoresTransitoCategoria()
    {
        $parroquias = parroquiaModel::from('parroquia as prr')->select('prr.id as idparroquia', 'prr.parroquia as parroquia')->get();
        $tp = Indicadores2020TransitoTipoTramiteModel::from('ind2020_tmov_transito_tipo_tramite as tp')->select('tp.id', 'tp.tipo')->where(['tp.estado' => 'ACT'])->get();
        $sql_collect = collect($parroquias);
        $parroquias = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020TransitoModel::from('ind2020_tmov_transito as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'brr.id_parroquia' => $item->idparroquia]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.valor_tramite) as valor"))->get();
            $n_cantidad = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = $deta->select('tra.id as id')->where(['cab.discapacidad' => 'SI'])->count();
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_cantidad[0]->valor == '' || $n_cantidad[0]->valor == null) {
                $n_cantidad[0]->valor = 0;
            }
            return collect(
                [
                    'parroquia' => $item->parroquia,
                    'id_parroquia' => $item->idparroquia,
                    'total' => $total,
                    'cantidad' => intval($n_cantidad[0]->valor),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => $total_disc,
                    'beneficiarios' => null
                ]
            );
        });
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020TransitoModel::from('ind2020_tmov_transito as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('ind2020_tmov_transito_tipo_tramite as tp', 'tp.id', '=', 'tra.id_tipo_tramite')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'tp.id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.valor_tramite) as valor"))->get();
            $n_cantidad = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = $deta->select('tra.id as id')->where(['cab.discapacidad' => 'SI'])->count();
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_cantidad[0]->valor == '' || $n_cantidad[0]->valor == null) {
                $n_cantidad[0]->valor = 0;
            }
            return collect(
                [
                    'indicador' => $item->tipo,
                    'id_indicador' => $item->id,
                    'total' => $total,
                    'cantidad' => intval($n_cantidad[0]->valor),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => $total_disc,
                    'beneficiarios' => null,

                ]
            );
        });
        $data = ['parroquias' => $parroquias, 'tipo' => $tp];
        return response()->json($data);
    }

    public function IndicadoresTransitoCategoriaId()
    {
        $barrio = barrioModel::from('barrio as brr')->select('brr.id', 'brr.barrio')
            ->where(['brr.estado' => 'ACT', 'brr.id_parroquia' => Input::get("id_parroquia")])->get();
        $tp = Indicadores2020TransitoTipoTramiteModel::from('ind2020_tmov_transito_tipo_tramite as tp')->select('tp.id', 'tp.tipo')->where(['tp.estado' => 'ACT'])->get();
        $sql_collect = collect($barrio);
        $barrio = $sql_collect->map(function ($item) {
            $deta = Indicadores2020TransitoModel::from('ind2020_tmov_transito as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'cab.barrio_id' => $item->id]);

            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.valor_tramite) as valor"))->get();
            $n_cantidad = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = $deta->select('tra.id as id')->where(['cab.discapacidad' => 'SI'])->count();
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_cantidad[0]->valor == '' || $n_cantidad[0]->valor == null) {
                $n_cantidad[0]->valor = 0;
            }
            return collect(
                [
                    'barrio' => $item->barrio,
                    'id_barrio' => $item->id,
                    'total' => $total,
                    'cantidad' => intval($n_cantidad[0]->valor),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => $total_disc,
                    'beneficiarios' => null
                ]
            );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();
        $parroki = Input::get("id_parroquia");
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) use ($parroki) {
            $deta = Indicadores2020TransitoModel::from('ind2020_tmov_transito as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->join('ind2020_tmov_transito_tipo_tramite as tp', 'tp.id', '=', 'tra.id_tipo_tramite')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'tp.id' => $item->id, 'prr.id' => $parroki]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.valor_tramite) as valor"))->get();
            $n_cantidad = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = $deta->select('tra.id as id')->where(['cab.discapacidad' => 'SI'])->count();
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_cantidad[0]->valor == '' || $n_cantidad[0]->valor == null) {
                $n_cantidad[0]->valor = 0;
            }
            return
                collect(
                    [
                        'indicador' => $item->tipo,
                        'id_indicador' => $item->id,
                        'total' => $total,
                        'cantidad' => intval($n_cantidad[0]->valor),
                        'aporte' => floatval($sum[0]->valor),
                        'discapacidad' => $total_disc,
                        'beneficiarios' => null,

                    ]
                );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();

        $data = ['barrio' => $barrio, 'tipo' => $tp];
        return response()->json($data);
    }

    //Comisaria Publica
    public function IndicadoresSancionesCategoria()
    {
        $parroquias = parroquiaModel::from('parroquia as prr')->select('prr.id as idparroquia', 'prr.parroquia as parroquia')->get();
        $tp = Indicadores2020SancionesTiposModel::from('ind2020_tmae_tipo_sancion as ts')->select('ts.id', 'ts.tipo')->where(['ts.estado' => 'ACT'])->get();
        $sql_collect = collect($parroquias);
        $parroquias = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020SancionesModel::from('ind2020_tmov_sanciones as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'brr.id_parroquia' => $item->idparroquia]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.valor) as valor"))->get();
            $n_cantidad = $total;
            $total_disc = $deta->select('tra.id as id')->where(['cab.discapacidad' => 'SI'])->count();
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            return collect(
                [
                    'parroquia' => $item->parroquia,
                    'id_parroquia' => $item->idparroquia,
                    'total' => $total,
                    'cantidad' => intval($n_cantidad),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => $total_disc,
                    'beneficiarios' => null
                ]
            );
        });
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020SancionesModel::from('ind2020_tmov_sanciones as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('ind2020_tmae_tipo_sancion as ts', 'ts.id', '=', 'tra.id_sancion')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'ts.id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.valor) as valor"))->get();
            $n_cantidad = $total;
            $total_disc = $deta->select('tra.id as id')->where(['cab.discapacidad' => 'SI'])->count();
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_cantidad),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => $total_disc,
                'beneficiarios' => null,

            ]);
        });
        $data = ['parroquias' => $parroquias, 'tipo' => $tp];
        return response()->json($data);
    }


    public function IndicadoresSancionesCategoriaId()
    {
        $barrio = barrioModel::from('barrio as brr')->select('brr.id', 'brr.barrio')
            ->where(['brr.estado' => 'ACT', 'brr.id_parroquia' => Input::get("id_parroquia")])->get();
        $tp = Indicadores2020SancionesTiposModel::from('ind2020_tmae_tipo_sancion as ts')->select('ts.id', 'ts.tipo')->where(['ts.estado' => 'ACT'])->get();
        $sql_collect = collect($barrio);
        $barrio = $sql_collect->map(function ($item) {
            $deta = Indicadores2020SancionesModel::from('ind2020_tmov_sanciones as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'cab.barrio_id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.valor) as valor"))->get();
            $n_cantidad = $total;
            $total_disc = $deta->select('tra.id as id')->where(['cab.discapacidad' => 'SI'])->count();
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            return collect(
                [
                    'barrio' => $item->barrio,
                    'id_barrio' => $item->id,
                    'total' => $total,
                    'cantidad' => $n_cantidad,
                    'discapacidad' => $total_disc,
                    'aporte' => floatval($sum[0]->valor),
                    'beneficiarios' => null,
                ]
            );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();
        $parroki = Input::get("id_parroquia");
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) use ($parroki) {
            $deta = Indicadores2020SancionesModel::from('ind2020_tmov_sanciones as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->join('ind2020_tmae_tipo_sancion as ts', 'ts.id', '=', 'tra.id_sancion')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'ts.id' => $item->id, 'prr.id' => $parroki]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.valor) as valor"))->get();
            $n_cantidad = $total;
            $total_disc = $deta->select('tra.id as id')->where(['cab.discapacidad' => 'SI'])->count();
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            return collect(
                [
                    'indicador' => $item->tipo,
                    'id_indicador' => $item->id,
                    'total' => $total,
                    'cantidad' => intval($n_cantidad),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => $total_disc,
                    'beneficiarios' => null,
                ]
            );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();

        $data = ['barrio' => $barrio, 'tipo' => $tp];
        return response()->json($data);
    }


    //Eventos - cultura
    public function IndicadoresEventosCulturaCategoria()
    {
        $parroquias = parroquiaModel::from('parroquia as prr')->select('prr.id as idparroquia', 'prr.parroquia as parroquia')->get();
        $tp = Indicadores2020EventosTiposModel::from('ind2020_tmov_eventos_tipo as te')->select('te.id', 'te.tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($parroquias);
        $parroquias = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020EventosModel::from('ind2020_tmov_eventos as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'brr.id_parroquia' => $item->idparroquia]);
            $total = $deta->select('tra.id as id')->count();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'parroquia' => $item->parroquia,
                'id_parroquia' => $item->idparroquia,
                'total' => $total,
                'cantidad' => intval($n_cantidad),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020EventosModel::from('ind2020_tmov_eventos as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('ind2020_tmov_eventos_tipo as te', 'te.id', '=', 'tra.id_tipo_evento')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $total_disc = null;
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_cantidad),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $data = ['parroquias' => $parroquias, 'tipo' => $tp];
        return response()->json($data);
    }
    public function IndicadoresEventosCulturaCategoriaId()
    {
        $barrio = barrioModel::from('barrio as brr')->select('brr.id', 'brr.barrio')
            ->where(['brr.estado' => 'ACT', 'brr.id_parroquia' => Input::get("id_parroquia")])->get();
        $tp = Indicadores2020EventosTiposModel::from('ind2020_tmov_eventos_tipo as te')->select('te.id', 'te.tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($barrio);
        $barrio = $sql_collect->map(function ($item) {
            $deta = Indicadores2020EventosModel::from('ind2020_tmov_eventos as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'cab.barrio_id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect(
                [
                    'barrio' => $item->barrio,
                    'id_barrio' => $item->id,
                    'total' => $total,
                    'cantidad' => $n_cantidad,
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => null,
                    'beneficiarios' => floatval($n_beneficiario[0]->valor),
                ]
            );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();
        $parroki = Input::get("id_parroquia");
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) use ($parroki) {
            $deta = Indicadores2020EventosModel::from('ind2020_tmov_eventos as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->join('ind2020_tmov_eventos_tipo as te', 'te.id', '=', 'tra.id_tipo_evento')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id, 'prr.id' => $parroki]);
            $total = $deta->select('tra.id as id')->count();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_cantidad),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();

        $data = ['barrio' => $barrio, 'tipo' => $tp];
        return response()->json($data);
    }


    //Deporte
    public function IndicadoresDeporteCategoria()
    {
        $parroquias = parroquiaModel::from('parroquia as prr')->select('prr.id as idparroquia', 'prr.parroquia as parroquia')->get();
        $tp = Indicadores2020ActividadesFisicasTipoModel::from('ind2020_tmov_actividades_fisicas_tipo as te')->select('te.id', 'te.tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($parroquias);
        $parroquias = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020ActividadesFisicasModel::from('ind2020_tmov_actividades_fisicas as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'brr.id_parroquia' => $item->idparroquia]);
            $total = $deta->select('tra.id as id')->count();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'parroquia' => $item->parroquia,
                'id_parroquia' => $item->idparroquia,
                'total' => $total,
                'cantidad' => intval($n_cantidad),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020ActividadesFisicasModel::from('ind2020_tmov_actividades_fisicas as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('ind2020_tmov_actividades_fisicas_tipo as te', 'te.id', '=', 'tra.id_tipo_actividad')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_cantidad),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $data = ['parroquias' => $parroquias, 'tipo' => $tp];
        return response()->json($data);
    }

    public function IndicadoresDeporteCategoriaId()
    {
        $barrio = barrioModel::from('barrio as brr')->select('brr.id', 'brr.barrio')
            ->where(['brr.estado' => 'ACT', 'brr.id_parroquia' => Input::get("id_parroquia")])->get();
        $tp = Indicadores2020ActividadesFisicasTipoModel::from('ind2020_tmov_actividades_fisicas_tipo as te')->select('te.id', 'te.tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($barrio);
        $barrio = $sql_collect->map(function ($item) {
            $deta = Indicadores2020ActividadesFisicasModel::from('ind2020_tmov_actividades_fisicas as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'cab.barrio_id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect(
                [
                    'barrio' => $item->barrio,
                    'id_barrio' => $item->id,
                    'total' => $total,
                    'cantidad' => intval($n_cantidad),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => null,
                    'beneficiarios' => $n_beneficiario[0]->valor,
                ]
            );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();
        $parroki = Input::get("id_parroquia");
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) use ($parroki) {
            $deta = Indicadores2020ActividadesFisicasModel::from('ind2020_tmov_actividades_fisicas as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->join('ind2020_tmov_actividades_fisicas_tipo as te', 'te.id', '=', 'tra.id_tipo_actividad')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id, 'prr.id' => $parroki]);
            $total = $deta->select('tra.id as id')->count();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_cantidad),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();

        $data = ['barrio' => $barrio, 'tipo' => $tp];
        return response()->json($data);
    }

    //Arbolito
    public function IndicadoresArbolitoCategoria()
    {
        $parroquias = parroquiaModel::from('parroquia as prr')->select('prr.id as idparroquia', 'prr.parroquia as parroquia')->get();
        $tp = Indicadores2020ArbolesSembradosServiciosModel::from('ind2020_tmae_arboles_lugares as te')->select('te.id', 'te.lugar as tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($parroquias);
        $parroquias = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020ArbolesSembradosModel::from('ind2020_tmov_arboles as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'brr.id_parroquia' => $item->idparroquia]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_arboles = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_arboles[0]->valor == '' || $n_arboles[0]->valor == null) {
                $n_arboles[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'parroquia' => $item->parroquia,
                'id_parroquia' => $item->idparroquia,
                'total' => $total,
                'cantidad' => intval($n_arboles[0]->valor),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020ArbolesSembradosModel::from('ind2020_tmov_arboles as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('ind2020_tmae_arboles_lugares as te', 'te.id', '=', 'tra.id_lugar')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_arboles = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_arboles[0]->valor == '' || $n_arboles[0]->valor == null) {
                $n_arboles[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_arboles[0]->valor),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $data = ['parroquias' => $parroquias, 'tipo' => $tp];
        return response()->json($data);
    }


    public function IndicadoresArbolitoCategoriaId()
    {
        $barrio = barrioModel::from('barrio as brr')->select('brr.id', 'brr.barrio')
            ->where(['brr.estado' => 'ACT', 'brr.id_parroquia' => Input::get("id_parroquia")])->get();
        $tp = Indicadores2020ArbolesSembradosServiciosModel::from('ind2020_tmae_arboles_lugares as te')->select('te.id', 'te.lugar as tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($barrio);
        $barrio = $sql_collect->map(function ($item) {
            $deta = Indicadores2020ArbolesSembradosModel::from('ind2020_tmov_arboles as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'cab.barrio_id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_arboles = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_arboles[0]->valor == '' || $n_arboles[0]->valor == null) {
                $n_arboles[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect(
                [
                    'barrio' => $item->barrio,
                    'id_barrio' => $item->id,
                    'total' => $total,
                    'cantidad' => intval($n_arboles[0]->valor),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => null,
                    'beneficiarios' => floatval($n_beneficiario[0]->valor),
                ]
            );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();
        $parroki = Input::get("id_parroquia");
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) use ($parroki) {
            $deta = Indicadores2020ArbolesSembradosModel::from('ind2020_tmov_arboles as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->join('ind2020_tmae_arboles_lugares as te', 'te.id', '=', 'tra.id_lugar')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id, 'prr.id' => $parroki]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_arboles = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_arboles[0]->valor == '' || $n_arboles[0]->valor == null) {
                $n_arboles[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_arboles[0]->valor),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();

        $data = ['barrio' => $barrio, 'tipo' => $tp];
        return response()->json($data);
    }


    //SEGURIDAD CIUDADANA
    public function IndicadoresSeguridadCiudadanaCategoria()
    {
        $parroquias = parroquiaModel::from('parroquia as prr')->select('prr.id as idparroquia', 'prr.parroquia as parroquia')->get();
        $tp = Indicadores2020SeguridadCiudadanaServiciosModel::from('ind2020_tmae_seguridad_ciudadana as te')->select('te.id', 'te.tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($parroquias);
        $parroquias = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020SeguridadCiudadanaModel::from('ind2020_tmov_seguridad_ciudadana as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'brr.id_parroquia' => $item->idparroquia]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_cantidad = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_cantidad[0]->valor == '' || $n_cantidad[0]->valor == null) {
                $n_cantidad[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'parroquia' => $item->parroquia,
                'id_parroquia' => $item->idparroquia,
                'total' => $total,
                'cantidad' => intval($n_cantidad[0]->valor),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020SeguridadCiudadanaModel::from('ind2020_tmov_seguridad_ciudadana as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('ind2020_tmae_seguridad_ciudadana as te', 'te.id', '=', 'tra.id_tipo_servicio')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_cantidad = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_cantidad[0]->valor == '' || $n_cantidad[0]->valor == null) {
                $n_cantidad[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_cantidad[0]->valor),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $data = ['parroquias' => $parroquias, 'tipo' => $tp];
        return response()->json($data);
    }


    public function IndicadoresSeguridadCiudadanaCategoriaId()
    {
        $barrio = barrioModel::from('barrio as brr')->select('brr.id', 'brr.barrio')
            ->where(['brr.estado' => 'ACT', 'brr.id_parroquia' => Input::get("id_parroquia")])->get();
        $tp = Indicadores2020SeguridadCiudadanaServiciosModel::from('ind2020_tmae_seguridad_ciudadana as te')->select('te.id', 'te.tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($barrio);
        $barrio = $sql_collect->map(function ($item) {
            $deta = Indicadores2020SeguridadCiudadanaModel::from('ind2020_tmov_seguridad_ciudadana as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'cab.barrio_id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_cantidad = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_cantidad[0]->valor == '' || $n_cantidad[0]->valor == null) {
                $n_cantidad[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect(
                [
                    'barrio' => $item->barrio,
                    'id_barrio' => $item->id,
                    'total' => $total,
                    'cantidad' => intval($n_cantidad[0]->valor),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => null,
                    'beneficiarios' => floatval($n_beneficiario[0]->valor),
                ]
            );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();
        $parroki = Input::get("id_parroquia");
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) use ($parroki) {
            $deta = Indicadores2020SeguridadCiudadanaModel::from('ind2020_tmov_seguridad_ciudadana as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->join('ind2020_tmae_seguridad_ciudadana as te', 'te.id', '=', 'tra.id_tipo_servicio')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id, 'prr.id' => $parroki]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_cantidad = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_cantidad[0]->valor == '' || $n_cantidad[0]->valor == null) {
                $n_cantidad[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_cantidad[0]->valor),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();

        $data = ['barrio' => $barrio, 'tipo' => $tp];
        return response()->json($data);
    }


    //TURISMO
    public function IndicadoresTurismoCategoria()
    {
        $parroquias = parroquiaModel::from('parroquia as prr')->select('prr.id as idparroquia', 'prr.parroquia as parroquia')->get();
        $tp = Indicadores2020TurismoServiciosModel::from('ind2020_tmae_turismo_servicios as te')->select('te.id', 'te.tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($parroquias);
        $parroquias = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020TurismoModel::from('ind2020_tmov_turismo as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'brr.id_parroquia' => $item->idparroquia]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $total_disc = null;
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'parroquia' => $item->parroquia,
                'id_parroquia' => $item->idparroquia,
                'total' => $total,
                'cantidad' => intval($n_cantidad),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020TurismoModel::from('ind2020_tmov_turismo as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('ind2020_tmae_turismo_servicios as te', 'te.id', '=', 'tra.id_tipo_servicio')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $total_disc = null;
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_cantidad),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $data = ['parroquias' => $parroquias, 'tipo' => $tp];
        return response()->json($data);
    }


    public function IndicadoresTurismoIdCategoriaId()
    {
        $barrio = barrioModel::from('barrio as brr')->select('brr.id', 'brr.barrio')
            ->where(['brr.estado' => 'ACT', 'brr.id_parroquia' => Input::get("id_parroquia")])->get();
        $tp = Indicadores2020TurismoServiciosModel::from('ind2020_tmae_turismo_servicios as te')->select('te.id', 'te.tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($barrio);
        $barrio = $sql_collect->map(function ($item) {
            $deta = Indicadores2020TurismoModel::from('ind2020_tmov_turismo as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'cab.barrio_id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_cantidad = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect(
                [
                    'barrio' => $item->barrio,
                    'id_barrio' => $item->id,
                    'total' => $total,
                    'cantidad' => intval($n_cantidad),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => null,
                    'beneficiarios' => floatval($n_beneficiario[0]->valor),
                ]
            );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();
        $parroki = Input::get("id_parroquia");
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) use ($parroki) {
            $deta = Indicadores2020TurismoModel::from('ind2020_tmov_turismo as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->join('ind2020_tmae_turismo_servicios as te', 'te.id', '=', 'tra.id_tipo_servicio')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id, 'prr.id' => $parroki]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_cantidad = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            $n_cantidad = $total;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_cantidad),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();

        $data = ['barrio' => $barrio, 'tipo' => $tp];
        return response()->json($data);
    }

    //Areas en Mantenimiento
    public function IndicadoresAreaMantenimientoCategoria()
    {
        $parroquias = parroquiaModel::from('parroquia as prr')->select('prr.id as idparroquia', 'prr.parroquia as parroquia')->get();
        $tp = Indicadores2020MantenimientoAreasServicioModel::from('ind2020_tmae_arboles_lugares as te')->select('te.id', 'te.lugar as tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($parroquias);
        $parroquias = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020MantenimientoAreasModel::from('ind2020_tmov_mantenimientos_areas as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'brr.id_parroquia' => $item->idparroquia]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_arboles = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_arboles[0]->valor == '' || $n_arboles[0]->valor == null) {
                $n_arboles[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'parroquia' => $item->parroquia,
                'id_parroquia' => $item->idparroquia,
                'total' => $total,
                'cantidad' => intval($n_arboles[0]->valor),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020MantenimientoAreasModel::from('ind2020_tmov_mantenimientos_areas as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('ind2020_tmae_arboles_lugares as te', 'te.id', '=', 'tra.id_lugar')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_arboles = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_arboles[0]->valor == '' || $n_arboles[0]->valor == null) {
                $n_arboles[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_arboles[0]->valor),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        });
        $data = ['parroquias' => $parroquias, 'tipo' => $tp];
        return response()->json($data);
    }


    public function IndicadoresAreaMantenimientoCategoriaId()
    {
        $barrio = barrioModel::from('barrio as brr')->select('brr.id', 'brr.barrio')
            ->where(['brr.estado' => 'ACT', 'brr.id_parroquia' => Input::get("id_parroquia")])->get();
        $tp = Indicadores2020MantenimientoAreasServicioModel::from('ind2020_tmae_arboles_lugares as te')->select('te.id', 'te.lugar as tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($barrio);
        $barrio = $sql_collect->map(function ($item) {
            $deta = Indicadores2020MantenimientoAreasModel::from('ind2020_tmov_mantenimientos_areas as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'cab.barrio_id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_arboles = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_arboles[0]->valor == '' || $n_arboles[0]->valor == null) {
                $n_arboles[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect(
                [
                    'barrio' => $item->barrio,
                    'id_barrio' => $item->id,
                    'total' => $total,
                    'cantidad' => intval($n_arboles[0]->valor),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => null,
                    'beneficiarios' => floatval($n_beneficiario[0]->valor),
                ]
            );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();
        $parroki = Input::get("id_parroquia");
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) use ($parroki) {
            $deta = Indicadores2020MantenimientoAreasModel::from('ind2020_tmov_mantenimientos_areas as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->join('ind2020_tmae_arboles_lugares as te', 'te.id', '=', 'tra.id_lugar')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id, 'prr.id' => $parroki]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_arboles = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_arboles[0]->valor == '' || $n_arboles[0]->valor == null) {
                $n_arboles[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_arboles[0]->valor),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();

        $data = ['barrio' => $barrio, 'tipo' => $tp];
        return response()->json($data);
    }




    //Areas en Mantenimiento
    public function IndicadoresTerritorialesCategoria()
    {
        $parroquias = parroquiaModel::from('parroquia as prr')->select('prr.id as idparroquia', 'prr.parroquia as parroquia')->get();
        $tp = Indicadores2020ControlTerritorialModel::from('ind2020_tmov_control_territorial as te')->select('te.inspeccion')->where(['te.estado' => 'ACT'])->groupBy('te.inspeccion')->get();

        $sql_collect = collect($parroquias);
        $parroquias = $sql_collect->map(function ($item, $key) {
            $deta = Indicadores2020ControlTerritorialModel::from('ind2020_tmov_control_territorial as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'brr.id_parroquia' => $item->idparroquia]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $total;
            $n_cantidad = $total;
            $total_disc = $deta->select('tra.id as id')->where(['cab.discapacidad' => 'SI'])->count();
            if ($sum == '' || $sum == null) {
                $sum = 0;
            }
            if ($n_cantidad == '' || $n_cantidad == null) {
                $n_cantidad = 0;
            }
            return collect(
                [
                    'parroquia' => $item->parroquia,
                    'id_parroquia' => $item->idparroquia,
                    'total' => $total,
                    'cantidad' => intval($n_cantidad),
                    'aporte' => null,
                    'discapacidad' => $total_disc,
                    'beneficiarios' => null
                ]
            );
        });
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) {
            // return $item->inspeccion;
            $deta = Indicadores2020ControlTerritorialModel::from('ind2020_tmov_control_territorial as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT'])->where('tra.inspeccion', 'like', "%$item->inspeccion%");


            $total = $deta->select('tra.id as id')->count();
            $sum = $total;
            $n_cantidad = $total;
            $total_disc = $deta->select('tra.id as id')->where(['cab.discapacidad' => 'SI'])->count();

            if ($sum == '' || $sum == null) {
                $sum = 0;
            }
            if ($n_cantidad == '' || $n_cantidad == null) {
                $n_cantidad = 0;
            }
            return collect(
                [
                    'indicador' => $item->tipo,
                    'id_indicador' => $item->id,
                    'total' => $total,
                    'cantidad' => intval($n_cantidad),
                    'aporte' => null,
                    'discapacidad' => $total_disc,
                    'beneficiarios' => null,

                ]
            );
        });
        $data = ['parroquias' => $parroquias, 'tipo' => $tp];
        return response()->json($data);
    }


    public function IndicadoresTerritorialesCategoriaId()
    {
        $barrio = barrioModel::from('barrio as brr')->select('brr.id', 'brr.barrio')
            ->where(['brr.estado' => 'ACT', 'brr.id_parroquia' => Input::get("id_parroquia")])->get();
        $tp = Indicadores2020MantenimientoAreasServicioModel::from('ind2020_tmae_arboles_lugares as te')->select('te.id', 'te.lugar as tipo')->where(['te.estado' => 'ACT'])->get();
        $sql_collect = collect($barrio);
        $barrio = $sql_collect->map(function ($item) {
            $deta = Indicadores2020MantenimientoAreasModel::from('ind2020_tmov_mantenimientos_areas as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'cab.barrio_id' => $item->id]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_arboles = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_arboles[0]->valor == '' || $n_arboles[0]->valor == null) {
                $n_arboles[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect(
                [
                    'barrio' => $item->barrio,
                    'id_barrio' => $item->id,
                    'total' => $total,
                    'cantidad' => intval($n_arboles[0]->valor),
                    'aporte' => floatval($sum[0]->valor),
                    'discapacidad' => null,
                    'beneficiarios' => floatval($n_beneficiario[0]->valor),
                ]
            );
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();
        $parroki = Input::get("id_parroquia");
        $sql_collect = collect($tp);
        $tp = $sql_collect->map(function ($item, $key) use ($parroki) {
            $deta = Indicadores2020MantenimientoAreasModel::from('ind2020_tmov_mantenimientos_areas as tra')
                ->join('ind2020_tmov_cab as cab', 'cab.id', '=', 'tra.id_indicador_cab')
                ->join('barrio as brr', 'brr.id', '=', 'cab.barrio_id')
                ->join('parroquia as prr', 'prr.id', '=', 'brr.id_parroquia')
                ->join('ind2020_tmae_arboles_lugares as te', 'te.id', '=', 'tra.id_lugar')
                ->whereBetween('cab.created_at', [Input::get("date_ini") . ' 00:00:00', Input::get("date_fin") . ' 23:59:59'])
                ->where(['cab.estado' => 'ACT', 'te.id' => $item->id, 'prr.id' => $parroki]);
            $total = $deta->select('tra.id as id')->count();
            $sum = $deta->select(DB::raw("sum(tra.inversion) as valor"))->get();
            $n_beneficiario = $deta->select(DB::raw("sum(tra.n_beneficiarios) as valor"))->get();
            $n_arboles = $deta->select(DB::raw("sum(tra.cantidad) as valor"))->get();
            $total_disc = null;
            if ($sum[0]->valor == '' || $sum[0]->valor == null) {
                $sum[0]->valor = 0;
            }
            if ($n_arboles[0]->valor == '' || $n_arboles[0]->valor == null) {
                $n_arboles[0]->valor = 0;
            }
            if ($n_beneficiario[0]->valor == '' || $n_beneficiario[0]->valor == null) {
                $n_beneficiario[0]->valor = 0;
            }
            return collect([
                'indicador' => $item->tipo,
                'id_indicador' => $item->id,
                'total' => $total,
                'cantidad' => intval($n_arboles[0]->valor),
                'aporte' => floatval($sum[0]->valor),
                'discapacidad' => null,
                'beneficiarios' => floatval($n_beneficiario[0]->valor)
            ]);
        })->filter(function ($valores, $key) {
            return $valores['cantidad'] != 0;
        })->values()->all();

        $data = ['barrio' => $barrio, 'tipo' => $tp];
        return response()->json($data);
    }





    public function consultarPublicaciones()
    {
        try {
            //code...
            $eventos = MantappPublicacionesModel::select('man_tmov_publicaciones.*')->where('man_tmov_publicaciones.estado', 'ACT')->orderBy('created_at', 'desc');
            $eventos = $eventos->get();
            $collection2 = collect($eventos);
            $total = $collection2->count();
            $multimedia = $collection2->map(function ($mul) {
                //SELECTGROUP_CONCAT(CONCAT('https://sistemasic.manta.gob.ec/publicaciones_manta_app/',man_tmov_publicaciones.id,'/',man_tmov_publicaciones_archivos.nombre),') AS ruta

                $imagenes = MantappPublicacionesArchivosModel::select(DB::raw("(CONCAT('https://sistemasic.manta.gob.ec/publicaciones_manta_app/',man_tmov_publicaciones_archivos.id_publicacion,'/',man_tmov_publicaciones_archivos.nombre)) AS ruta"))->where(['id_publicacion' => $mul->id, 'estado' => 'ACT', 'tipo' => 'IMG'])->get();
                $videos = MantappPublicacionesArchivosModel::select(DB::raw("(CONCAT('https://sistemasic.manta.gob.ec/publicaciones_manta_app/',man_tmov_publicaciones_archivos.id_publicacion,'/',man_tmov_publicaciones_archivos.nombre)) AS ruta"))->where(['id_publicacion' => $mul->id, 'estado' => 'ACT', 'tipo' => 'VIDEO'])->get();
                return [
                    'id' => $mul->id,
                    'titulo' => $mul->titulo,
                    'descripcion' => $mul->descripcion,
                    'create_at' => date('Y-m-d', strtotime($mul->created_at)),
                    'fecha' => fechas(2, $mul->created_at),
                    'link_facebook' => $mul->link_facebook,
                    'link_instagram' => $mul->link_instagram,
                    'link_twitter' => $mul->link_twitter,
                    'imagenes' => $imagenes,
                    'videos' => $videos

                ];
            });
            $collection = collect($multimedia);

            $page = Paginator::resolveCurrentPage('page');
            // dd($page);
            $perPage = 50;

            $paginate = new LengthAwarePaginator(
                $collection->forPage($page, $perPage)->all(),
                $collection->count(),
                $perPage,
                $page,
                ['path' =>  Paginator::resolveCurrentPath()]
            );
            return $paginate;
            // return $this->paginate($multimedia, $total, 5);
        } catch (\Throwable $th) {
            //throw $th;
            return $th;
        }
    }

    protected static function paginator($items, $total, $perPage, $currentPage, $options)
    {
        dd($items);
        return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
            'items',
            'total',
            'perPage',
            'currentPage',
            'options'
        ));
    }

    public static function paginate(Collection $results, $total, $pageSize)
    {
        $page = Paginator::resolveCurrentPage('page');

        // dd($results->forPage($page, $pageSize);

        return self::paginator($results->forPage($page, $pageSize), $total, $pageSize, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);
    }


    /////
    ///////////// DONACIONES
    public function validarDonaciones()
    {
        $datos = DonaDonacionesModel::where('tipo_subida', 1)->get();
        foreach ($datos as $key => $value) {
            # code...
            $url = "http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/" . $value->cedula_beneficiario . "/1015";
            try {
                if ($value->tipo_documento == 'CEDULA') {
                    $client = new GuzzleHttpClient();
                    $respuesta = $client->request('GET', $url, ['timeout' => 2, 'http_errors' => false]);
                    $consultawebserviceobs = json_decode($respuesta->getBody()->getContents(), true);
                    if (isset($consultawebserviceobs[0]['nombre'])) {
                        DonaDonacionesModel::where('id', $value->id)->update(
                            [
                                'tipo_subida' => 3,
                                'nombre_beneficiario' => $consultawebserviceobs[0]['nombre']
                            ]
                        );
                    } else {
                        DonaDonacionesModel::where('id', $value->id)->update(
                            [
                                'tipo_subida' => 3
                            ]
                        );
                    }
                }
            } catch (\Exception $e) {
            }
        }
        return 'Ok';
        # code...
    }

    public function repositorio_imagenes()
    {
        // Se comprueba que realmente sea la ruta de un directorio
        $ruta = base_path() . '/donaciones_img';
        if (is_dir($ruta)) {
            // Abre un gestor de directorios para la ruta indicada
            $gestor = opendir($ruta);
            echo "<ul>";
            // sort($gestor);
            // Recorre todos los elementos del directorio
            while (($archivo = readdir($gestor)) !== false) {
                // Se muestran todos los archivos y carpetas excepto "." y ".."
                if ($archivo != "." && $archivo != "..") {
                    if (filesize($ruta . '/' . $archivo) > 0) {
                        // echo "<li>" . $archivo . " <a href='/descargarZip/" . $archivo . "'> Descargar archivo </a>   (" . formatBytes(filesize($ruta . '/' . $archivo)) . ")</li>";
                    }
                }
            }
            closedir($gestor);
            echo "</ul>";
        } else {
            echo "No es una ruta de directorio valida<br/>";
        }
    }
    public function descargarZip($archivo)
    {
        // Se comprueba que realmente sea la ruta de un directorio
        $ruta = base_path() . '/donaciones_img/' . $archivo;
        $response = response(file_get_contents($ruta), 200);
        $response->header('Content-Type', mime_content_type($ruta));
        $response->header('Content-disposition', 'inline; filename=' . $archivo);
        $response->send();
        return $response->send();
    }


    public function barridoDinardap()
    {
        $total = Input::get('n');
        if ($total == null) {
            $total = 10;
        }
        // $datos = DonaDonacionesModel::where(['discapacitado' => 'NO'])->take($total)->get();
        $datos = DonaDonacionesModel::where(['estado' => 'ACT'])->whereNull('tercera_edad')->take($total)->get();
        foreach ($datos as $key => $value) {
            # code...
            $url = "http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/" . $value->cedula_beneficiario . "/1015";
            try {
                if ($value->tipo_documento == 'CEDULA') {
                    $client = new GuzzleHttpClient();
                    $respuesta = $client->request('GET', $url);
                    $consultawebserviceobs = json_decode($respuesta->getBody()->getContents(), true);
                    if (isset($consultawebserviceobs[0]['nombre'])) {
                        // $fechaNacimiento = date('Y-m-d', strtotime($consultawebserviceobs[0]['fechaNacimiento']));
                        $fecha = explode('/', $consultawebserviceobs[0]['fechaNacimiento']);
                        $f = '';
                        if (isset($fecha[0]) && isset($fecha[1]) && isset($fecha[2])) {
                            $f = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                        }
                        $edad = fechas(1100, $f);
                        // dd($consultawebserviceobs);
                        $tercera = '';
                        $dis = '';
                        if ($edad >= 65) {
                            $tercera = 'SI';
                        } else {
                            $tercera = 'NO';
                        }
                        if (strpos($consultawebserviceobs[0]['condicionCiudadano'], 'DISCA') !== false) {
                            $dis = 'SI';
                        } else {
                            $dis = 'NO';
                        }
                        DonaDonacionesModel::where('id', $value->id)->update(
                            [
                                'tercera_edad' => $tercera,
                                'discapacitado' => $dis
                            ]
                        );
                    }
                }
            } catch (\Exception $e) {
            }
        }
        return 'Ok';
        # code...
    }

    public function donacionesCanastas()
    {
        $donaciones = DonaDonacionesModel::where([
            'id_donacion' => 5,
            'estado_donacion' => 'ENTREGADO',
            'estado' => 'ACT',
        ])
            ->where('fecha_entrega', '>=', '2020-05-14 00:00:00')
            ->where('id_usuario', '<>', 1494)
            ->count();
        return $donaciones;
    }
    public function verApadrina()
    {
        $cedula = Input::get('cedula_beneficiario');
        $donaciones = DonaDonacionesModel::where([
            'id_donacion' => 9,
            'estado_donacion' => 'ENTREGADO',
            'cedula_beneficiario' => $cedula,
            'estado' => 'ACT',
        ])
            ->orderBy('fecha_entrega', 'DESC')
            ->first();
        if ($donaciones) {
            return $donaciones->semana + 1;
        } else {
            return 1;
        }
    }


    public function consultarImagenPublicacion()
    {
        $archivos = MantappPublicacionesArchivosModel::select(DB::raw('concat(\'' . URL::to('') . '/publicaciones_manta_app/\',id_publicacion,\'/\',nombre) as ruta'))->where('estado', 'ACT')
            ->where('id_publicacion', DB::raw("(select max(`id_publicacion`) from man_tmov_publicaciones_archivos)"))
            ->orderBy('created_at', 'ASC')
            ->first();
        return $archivos;
        # code...
    }


    public function getDirecciones()
    {
        $tabla = direccionesModel::where('estado', 'ACT')->wherenotin('id', [62, 63, 64])->orderBy('direccion', 'ASC')->pluck('direccion', 'id')->all();
        sort($tabla);
        return $tabla;
    }

    public function sincronizaBarrios()
    {
        $barrios_portal = [
            "ELOY ALFARO" => [802, 2],
            "LOS ESTEROS" => [799, 3],
            "MANTA" => [803, 1],
            "SAN LORENZO" => [804, 6],
            "SAN MATEO" => [800, 7],
            "SANTA MARIANITA" => [805, 5],
            "TARQUI" => [801, 4]
        ];
        $resultado = '';
        foreach ($barrios_portal as $key => $value) {
            $url = '';
            $url = 'https://portalciudadano.manta.gob.ec/barrios_by_parroquia/' . $value[0];
            $client = new GuzzleHttpClient();
            $respuesta = $client->request('GET', $url);
            $consultawebserviceobs = json_decode($respuesta->getBody()->getContents(), true);
            if ($consultawebserviceobs['ok']) {
                $barios = barrioModel::where('id_parroquia', $value[1])->pluck('barrio')->toArray();
                $barrios_portal = [];
                foreach ($consultawebserviceobs['data'] as $kkk => $vvv) {
                    $barrios_portal[] = strtoupper($vvv['barrio']);
                }
                foreach ($barios as $kk => $vv) {
                    if (!in_array($vv, $barrios_portal)) {
                        $resultado .= 'INSERT INTO `tmo_barrio`(`id`, `id_parroquia`, `barrio`, `estado`, `updated_at`, `created_at`) VALUES (null,' . $value[0] . ',\'' . $vv . '\',\'ACT\',\'' . date('Y-m-d H:i:s') . '\',\'' . date('Y-m-d H:i:s') . '\');';
                        // $resultado .= '<b> ' . $key . ':</b> ' . $vv . '<br> ';
                    }
                }
            }
        }
        return $resultado;
    }





    public function getOficios()
    {
        # code...

        try {
            //code...
            $estado = 'INICIAL';
            $tipo_envio = 'BORRADOR';
            $tipo_en = Input::get('tipo_envio');
            if ($tipo_en) {
                $tipo_envio = $tipo_en;
            }
            // dd($tipo_envio);


            $id_alcalde = 1643;
            if (Auth::user()->id != 1643) {
            }
            if (config('app.env') == 'local') {
            }
            $id_alcalde = Auth::user()->id;

            $tramites = TramiteInternoModel::with('remitente', 'asignados.usuario')
                ->where(['tipo_tramite' => 'OFICIO']);
            if ($tipo_envio) {
                $tramites = $tramites->where(['tipo_envio' => $tipo_envio]);
            }
            // dd($tramites->get());
            $id = Input::get('id');
            if ($id != null && $id != "") {
                $id = Crypt::decrypt($id);
                $tramites = $tramites->where(['id' => $id]);
            }
            // dd($tramites->get());
            if ($tipo_en == 'RECIBIDO') {
                $tramites = $tramites->whereHas('asignados', function ($q) use ($estado) {
                    $q
                        ->where(['id_usuario_asignado' => Auth::user()->id])
                        ->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO']);;
                });
                // dd($tramites->get());
            } else if ($tipo_en == 'COPIA') {
                $tramites = $tramites->whereHas('asignados', function ($q) use ($estado) {
                    $q->where(['id_usuario_asignado' => Auth::user()->id])
                        ->whereIn('estado', ['COPIA']);;
                });
            } else {
                $tramites = $tramites->where(['id_remitente' => $id_alcalde]);
            }

            $data = DataTables::of($tramites)->addColumn('asignados', function ($tramite) {
                if ($tramite->tipo_tramite == 'MEMO') {
                    return $tramite->asignados->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO'])->map(function ($asignado) {
                        return ucwords(strtolower($asignado->usuario->name)) . ' - ' . ucwords(strtolower($asignado->usuario->cargo));
                    })->implode(', ');
                } else {
                    $para = json_decode($tramite->delegados_para);
                    $para = collect($para);

                    $servidores_p = $tramite->asignados->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO'])->map(function ($asignado) {
                        return ucwords(strtolower($asignado->usuario->name)) . ' - ' . ucwords(strtolower($asignado->usuario->cargo));
                    })->implode(', ');

                    return (($servidores_p) ?  $servidores_p . ', ' : '') . $para->map(function ($asignado) {
                        return ucwords(strtolower($asignado->titulo)) . ' ' . ucwords(strtolower($asignado->nombre)) . ' - ' . ucwords(strtolower($asignado->cargo));
                    })->implode(', ');
                }
            })->addColumn('copias', function ($tramite) {
                if ($tramite->tipo_tramite == 'MEMO') {
                    return $tramite->asignados->where('estado', '=', 'COPIA')->map(function ($asignado) {
                        return ucwords(strtolower($asignado->usuario->name)) . ' - ' . ucwords(strtolower($asignado->usuario->cargo));
                    })->implode(', ');
                } else {
                    $copia = json_decode($tramite->delegados_copia);
                    $copia = collect($copia);

                    $servidores_p = $tramite->asignados->where('estado', '=', 'COPIA')->map(function ($asignado) {
                        return ucwords(strtolower($asignado->usuario->name)) . ' - ' . ucwords(strtolower($asignado->usuario->cargo));
                    })->implode(', ');
                    return (($servidores_p) ?  $servidores_p . ', ' : '') . $copia->map(function ($asignado) {
                        return ucwords(strtolower($asignado->titulo)) . ' ' . ucwords(strtolower($asignado->nombre)) . ' - ' . ucwords(strtolower($asignado->cargo));
                    })->implode(', ');
                }
            })->addColumn('documento', function ($tramite) {
                if ($tramite->documento) {
                    $memo = asset('archivos_firmados/' . $tramite->documento);
                } else {
                    $memo = null;
                }
                return $memo;
            })->addColumn('id', function ($tramite) {

                return Crypt::encrypt($tramite->id);
            })->addColumn('anexos', function ($tramite) {
                $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                    ->join('users as u', 'ax.usuario_id', 'u.id')
                    ->select('ax.*', 'u.name')->where(['id_referencia' => $tramite->id, 'tipo' => 11])->get();
                $archivos = null;

                foreach ($anexos as $anexo) {
                    $archivos[] = asset('archivos_sistema/' . $anexo->ruta);
                }

                return $archivos;
            })->rawColumns(['peticion'])->make(true);
            // dd();
            return ['estado' => true, "data" => $data->getData()->data];
        } catch (\Throwable $th) {
            //throw $th;
            return ['estado' => false, "data" => []];
        }
    }

    public function generarDocumentoApp($id, $codigo_x = null)
    {
        try {
            $id = Crypt::decrypt($id);
            $tramite = TramiteInternoModel::with('remitente', 'asignados.usuario')->where(['id' => $id])->first();
            $id = $tramite->remitente->id;
            $nombre_documento = 'Tramite_interno' . $tramite->numtramite . '_' . $id . '.pdf';
            $datos = new stdClass;
            $datos->nombre_documento = $nombre_documento;
            $userpara = $tramite->asignados->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO'])->map(function ($asignado) {
                return $asignado->id_usuario_asignado;
            });

            $para = UsuariosModel::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')->select('name', 'cargo', 'direccion')->whereIn('users.id', $userpara->toArray())->get();
            $usercopia = $tramite->asignados->where('estado', '=', 'COPIA')->map(function ($asignado) {
                // dd($asignado);
                return $asignado->id_usuario_asignado;
            });

            $copia = UsuariosModel::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')->select('name', 'cargo', 'direccion')->whereIn('users.id', $usercopia->toArray())->get();
            // dd($copia);
            $datos->asunto = $tramite->asunto;

            $anexos_json = json_decode(Input::get('anexos_json'));
            if (is_array($anexos_json) && isset($anexos_json[0])) {
                $datos->anexos = $anexos_json;
            } else {
                $datos->anexos = null;
            }

            $datos->tipo_firma = '';
            if ($tramite->tipo_tramite == 'OFICIO') {
                $datos->fecha_firma = date('Y-m-d H:i:s');
                $datos->codigo = $codigo_x;
            } else {
                $datos->codigo = '';
                $datos->fecha_firma = '';
            }




            $tipo = $tramite->tipo_tramite;
            // dd($tipo);
            $datos->tipo = 'MEMORANDO';
            if ($tipo == 'OFICIO') {
                $datos->tipo = 'OFICIO';
                $delegados_para = json_decode($tramite->delegados_para);

                if (is_array($delegados_para) && isset($delegados_para[0])) {
                    // $para = [];
                    foreach ($delegados_para as $key => $value) {
                        $para_datos = new stdClass;
                        $para_datos->titulo = $value->titulo;
                        $para_datos->name = $value->nombre;
                        $para_datos->cargo = $value->cargo;
                        $para_datos->institucion = $value->institucion;
                        $para[] = $para_datos;
                    }
                } else {
                    // $para = [];
                }

                // dd($para);

                $delegados_copia = json_decode($tramite->x);

                if (is_array($delegados_copia) && isset($delegados_copia[0])) {
                    // $copia = [];
                    foreach ($delegados_copia as $key => $value) {
                        $copia_datos = new stdClass;
                        $copia_datos->name = $value->titulo . '<br>' . $value->nombre;
                        $copia_datos->cargo = $value->cargo;
                        $copia_datos->institucion = $value->institucion;
                        $copia[] = $copia_datos;
                    }
                } else {
                    // $copia = [];
                }
                //  dd($copia);
            }

            if ($tramite->fecha) {
                $datos->fecha = $tramite->fecha;
            }

            if ($tipo == 'INFORME') {
                $datos->tipo = 'INFORME';
                $datos->numtramite_externo = $tramite->numtramite_externo;
                $datos->nombre_informe = $tramite->nombre_informe;
            }


            $datos->para = $para;
            $datos->copia = $copia;
            $datos->peticion = $tramite->peticion;
            // $tipo = 3;
            $datos->borrador =  3;
            $codigo = TramInternoCodigosModel::where(['estado' => 'ACT', 'numtramite' =>  $tramite->numtramite, 'codigo' => $codigo_x])->first();
            if ($codigo) {
                $datos->borrador = 2;
                $datos->tipo_firma = 2;
            }
            // $datos->prioridad = Input::get('prioridad');
            // dd($datos);
            $datos->numtramite = $tramite->numtramite;
            $user = UsuariosModel::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')
                ->where('users.id', Auth::user()->id)->first();
            $datos->user = $user;
            // dd($datos);
            $pdf = \App::make('dompdf.wrapper');
            $context = stream_context_create([
                'ssl' => [
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                    'allow_self_signed' => TRUE
                ]
            ]);
            // $pdf->setOptions(['isPhpEnabled' => true]);
            $configuraciongeneral = ['Gestión de trámites internos', 'tramitesinternos.store', 'tramitesinternos.form', 'crear', '', 6 => ''];
            $pdf->getDompdf()->setHttpContext($context);
            $pdf->loadView('vistas.firmaelectronica.memo', [
                "datos" => $datos,
                "configuraciongeneral" => $configuraciongeneral
            ])->setPaper('a4', 'portrait'); //])->setPaper('a4','landscape'); 
            // return $pdf->stream($nombre_documento);
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            
            if ($tipo == 'INFORME') {
                $canvas->page_text(430, 90, "Pag. {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            } else {
                $canvas->page_text(10, 800, "Pag. {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            }


            $pdf->stream($nombre_documento);

            // return $pdf;
            $documento = $pdf->output();
            if ($codigo_x != null) {
                $codigo = TramInternoCodigosModel::where(['estado' => 'ACT', 'numtramite' =>  $tramite->numtramite, 'codigo' => $codigo_x])->first();
                // $codigo = TramInternoCodigosModel::where(['estado' => 'ACT'])->first();
                // dd($codigo);
                if ($codigo) {
                    $codigo = TramInternoCodigosModel::find($codigo->id);
                    $codigo->fecha_firma = date('Y-m-d H:i:s');
                    $codigo->save();
                    $carpeta_2  = public_path() . '/archivos_firmados';
                    $rutafinal_2 = $carpeta_2 . '/' . $nombre_documento;
                    if (is_file($rutafinal_2)) {
                        unlink($rutafinal_2);
                    }
                    file_put_contents($rutafinal_2, $documento);
                    Log::info(sha1_file($rutafinal_2));
                    Log::info(md5_file($rutafinal_2));
                    return ['estado' => true, 'documento' => Url::to('') . '/archivos_firmados/' . $nombre_documento];
                } else {
                    return ['estado' => false, 'documento' => null,  'msg' => 'Código inválido'];
                }
            }

            $carpeta  = public_path() . '/archivos_temporales';
            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777, true);
            }
            $rutafinal = $carpeta . '/' . $nombre_documento;
            file_put_contents($rutafinal, $documento);
            return ['estado' => true, 'documento' => Url::to('') . '/archivos_temporales/' . $nombre_documento];
        } catch (\Throwable $th) {
            // return $th;
            Log::info($th->getMessage());
            return ['estado' => false, 'documento' => null,  'msg' => 'No se pudo realizar la operación'];
        }
    }

    public function generarCodigoApp()
    {

        try {

            $datos = TramInternoAutorizacionesModel::where(['cedula' => Auth::user()->cedula, 'estado_autorizacion' => 'APROBADO'])->first();
            if (!$datos) {
                if (Auth::user()->cedula != '1310182470' && Auth::user()->cedula != '1314854918' && Auth::user()->cedula != '1312469651') {
                    return ['estado' => false, 'msg' => 'No tiene autorización, por favor acercarse a R.R.H.H para que sea habilitado.'];
                }
            }

            TramInternoCodigosModel::where('numtramite', Input::get('numtramite'))->update(['estado' => 'INA']);
            $codigo = new TramInternoCodigosModel;
            $codigo->codigo = Auth::user()->id . rand(1000, 5000);
            $codigo->numtramite = Input::get('numtramite');
            $codigo->id_usuario = Auth::user()->id;
            $codigo->save();
            $notificacion = new NotificacionesController;
            $notificacion->EnviarEmail(Auth::user()->email, 'CÓDIGO DE AUTORIZACIÓN', $codigo->codigo, 'CÓDIGO DE AUTORIZACIÓN', '',  "vistas.emails.email", "NO");
            return ['estado' => true, 'msg' => 'Código enviado al correo ' . Auth::user()->email];
        } catch (\Throwable $th) {
            // return ['estado' => false, 'msg' => 'No se pudo enviar el código de validación'];
            return ['estado' => false, 'msg' => $th->getMessage()];
        }
    }



    public function enviarObs()
    {
        # code...

        try {
            //code...
            $id = Input::get('id');
            $id = Crypt::decrypt($id);
            $observacion = Input::get('observacion');
            $tramite = TramiteInternoModel::find($id);
            $notificacion = new NotificacionesController;
            if ($tramite) {
                $tramite->observacion = $tramite->observacion . ' - ' . date('Y-m-d H:i:m') . ': ' . $observacion;
                $tramite->save();
                $usuario = User::join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
                    ->select('users.*', 'p.tipo')
                    ->where('p.tipo', 8)->get();
                foreach ($usuario as $key => $value) {
                    if ($value) {
                        $notificacion->EnviarEmailAgenda($value->email, 'Observaciones de ' . $tramite->tipo_tramite . ' recibidas', $tramite->tipo_tramite . ' N° ' . $tramite->numtramite . ': ' . $observacion, '', "tramitesalcaldia/tramitesinternos/borradores");
                        $notificacion->notificacionesweb('Observaciones de ' . $tramite->tipo_tramite . ' N° ' . $tramite->numtramite . ' recibidas', "tramitesalcaldia/tramitesinternos/borradores", $value->id, "ed5565");
                    }
                }
                return ['estado' => true, 'msg' => 'Operación realizada correctamente'];
            } else {
                return ['estado' => false, 'msg' => 'No se pudo realizar la operación'];
            }
        } catch (\Throwable $th) {
            //throw $th;
            return ['estado' => false, 'msg' => $th->getMessage()];
            // return ['estado' => false, 'msg' => 'No se pudo realizar la operación'];
        }
    }


    public function getDate()
    {
        return date('Y-m-d H:i');
    }
}
