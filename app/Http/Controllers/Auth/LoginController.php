<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = 'moduleschoice';
    protected $loginPath = 'login';
    protected $redirectAfterLogout = 'login';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if(Auth::attempt($credentials))
        {
            return redirect()->intended('dashboard');
        }
    }

    protected function authenticated(Request $request, $user)
    {
        $user->update([
            'last_login_at' => date("Y-m-d")." ".date("H:i:s"),
            'last_login_ip' => $request->getClientIp()
        ]);
        if($user->nuevo==1)
            return redirect()->intended("cambiardatos/".$user->id."/edit");
        return redirect()->intended($this->redirectPath());
    }

    public function username()
    {
        return 'cedula';
    }
}
