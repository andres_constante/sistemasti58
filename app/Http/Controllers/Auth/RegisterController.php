<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session;
use Illuminate\Http\Request;
use Auth;
use App\UsuarioModuloModel;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = 'moduleschoice';

    public function __construct()
    {
        $this->middleware('guest');//, ['except' => 'logout']);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'cedula' => ['required', 'string', 'min:10', 'unique:users'],
        ]);
    }

    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        // if ($validator->fails())
        // {
        //     $this->throwValidationException(
        //         $request, $validator
        //     );
        // }

        $this->create($request->all());
        Auth::logout();
        Session::flash('message','Cuenta creada exitosamente. Ingrese por favor...');
        return redirect('login');

        // event(new Registered($user = $this->create($request->all())));
        // $this->guard()->login($user);
        // return $this->registered($request, $user) ?: redirect($this->redirectPath());
    }

    protected function create(array $data)
    {

        $tabla=User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'id_direccion' => 0,
            'id_perfil'=>2,
            'fecha_nacimiento' => now(),
            'cedula' => $data['cedula'],
            'password' => Hash::make($data['password']),
        ]);

        if($tabla)
        {
            $guardar= new UsuarioModuloModel;
            $guardar->id_modulo=$data["id_modulo"];
            $guardar->id_usuario=$tabla->id;
            $guardar->save();
        }
        return $tabla;
    }
}
