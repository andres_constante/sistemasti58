<?php

namespace App\Http\Controllers;

use PHPJasper\PHPJasper;
use Config;
use Illuminate\Support\Facades\Input;
use GuzzleHttp\Client as GuzzleHttpClient;

/**
 * Class Report
 * This class is a helper to generate reports using Jasper Reports from PHP.
 * @package app\controllers
 */
class Report
{
    /**
     * Generate a new UID.
     * @return string
     */
    private function newUID()
    {
        $s = mb_strtoupper(md5(uniqid(rand(),true)));
        $uid_text =
            substr($s, 0, 8) . '-' .
            substr($s, 8, 4) . '-' .
            substr($s, 12, 4) . '-' .
            substr($s, 16, 4) . '-' .
            substr($s, 20);
        return $uid_text;
    }

    /**
     * Generate report.
     * @param string $name Report name
     * @param array $params Report parameters
     * @param string $format Report format
     * @param string $filename Report filename
     * @param bool $download Download option
     * @param bool $download_is_attachment Download as attachment
     * @return array|null
     * @throws
     */
    public function & generate($name, $params, $format, $filename, $download, $download_is_attachment = true)
    {
        // show(array($name, $params, $format, $filename, $download, $download_is_attachment));
        $result = null;
        $input = base_path() . '/reports/' . $name . '.jasper';
        $output = public_path() . '/temp/' . $this->newUID();
        $db = Config::get('database')["connections"]["mysql"];

        if (config('app.env') == 'local') {
            $options = [
                'format' => [$format],
                'locale' => Config::get('app.locale'),
                'params' => $params,
                'db_connection' => [
                    'driver' => $db["driver"],
                    'username' => $db["username"],
                    // 'password' => $db["password"],
                    'host' => $db["host"],
                    'database' => $db["database"],
                    'port' => $db["port"]
                ]
            ];
        } else {
            $options = [
                'format' => [$format],
                'locale' => Config::get('app.locale'),
                'params' => $params,
                'db_connection' => [
                    'driver' => $db["driver"],
                    'username' => $db["username"],
                    'password' => $db["password"],
                    'host' => $db["host"],
                    'database' => $db["database"],
                    'port' => $db["port"]
                ]
            ];
        }
        // show(array($input,
        //         $output,
        //         $options));
        
        try {
            $jasper = new PHPJasper;

            $jasper->process(
                $input,
                $output,
                $options
            );
        //     error_log('DEBUG JASPER OUTPUT => ' . $jasper->output());
        //    dd($jasper); 
            $jasper->execute();
            $output .= '.' . $format;

            if ($download) {
                $response = response(file_get_contents($output), 200);
                $response->header('Content-Type', mime_content_type($output));
                $response->header('Content-disposition', ($download_is_attachment ? 'attachment' : 'inline') . '; filename=' . $filename . '.' . $format);
                $response->send();
                unlink($output);

            } else {
                // show($output);
                $result = [
                    'type' => mime_content_type($output),
                    'name' => $filename . '.' . $format,
                    'data' => base64_encode(file_get_contents($output)),
                ];
                
                unlink($output);
            }
            
            return $result;
        }
        catch (\Exception $e) {
            
            dd($jasper);
        }
    }

    /**
     * Generate report by jsonDataProvider.
     * @param string $name Report name
     * @param array $params Report parameters
     * @param string $format Report format
     * @param string $filename Report filename
     * @param bool $download Download option
     * @param bool $download_is_attachment Download as attachment
     * @return array|null
     * @throws
     */
    public function & generateByJsonDataProvider($name, $params, $format, $filename, $download, $download_is_attachment = true,$tipo=0)
    {
        // show(array($name, $params, $format, $filename, $download, $download_is_attachment));
        $result = null;
        $input = base_path() . '/reports/' . $name . '.jasper';
        $output = public_path() . '/temp/' . $this->newUID();
        //$data_file = base_path() . '/reports/WebApiResource/consultaReportes.json';
        //$data_url = 'https://portalciudadano.manta.gob.ec/consultaReportes.json';
        
        $client = new GuzzleHttpClient();
        $url = config('app.env') == 'local' ? 'http://mantaentusmanos.test' : 'https://portalciudadano.manta.gob.ec';
        if($tipo==1){
            $resp_cliente = $client->request('GET', $url.'/consultaPermisos?'.$params.'namereporte='.$name);
        }elseif($tipo==10){
            // dd($params.'namereporte='.$name);
            $resp_cliente = $client->request('GET', $url.'/reportePermisosCirculacion?'.$params.'namereporte='.$name);
           
        }else{
            $resp_cliente = $client->request('GET', $url.'/consultaReportes?'.$params);
            // dd($url.'/consultaReportes?'.$params);
        }
        // \dd($tipo);
        $data_response = $resp_cliente->getBody()->getContents();
        $data_file = public_path() . '/temp/' . $this->newUID() . '.json';
        $data = fopen($data_file, 'w');
        fwrite($data, $data_response);
        fclose($data);
    
        //$db = Config::get('database')["connections"]["mysql"];

        $options = [
            'format' => [$format],
            'locale' => Config::get('app.locale'),
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
            ]
        ];
        
        // show(array($input,
        //         $output,
        //         $options));
        
        try {
            $jasper = new PHPJasper;

            $jasper->process(
                $input,
                $output,
                $options
            );
            // dd('DEBUG JASPER OUTPUT => ' . $jasper->output());
            
            $jasper->execute();
            $output .= '.' . $format;

            if ($download) {
                ini_set('memory_limit', '-1');
                $response = response(file_get_contents($output), 200);
                $response->header('Content-Type', mime_content_type($output));
                $response->header('Content-disposition', ($download_is_attachment ? 'attachment' : 'inline') . '; filename=' . $filename . '.' . $format);
                $response->send();
                unlink($output);
                unlink($data_file);
            } else {
                // show($output);
                $result = [
                    'type' => mime_content_type($output),
                    'name' => $filename . '.' . $format,
                    'data' => base64_encode(file_get_contents($output)),
                ];
                
                unlink($output);
                unlink($data_file);
            }
            
            return $result;
        }
        catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * Download report.
     * @param string $name Report name
     * @param array $params Report parameters
     * @param string $format Report format
     * @param string $filename Report filename
     * @param bool $download_is_attachment  Download as attachment
     * @throws \yii\web\HttpException
     */
    public function download($name, $params, $format, $filename, $download_is_attachment = true)
    {
        $this->generate($name, $params, $format, $filename, true, $download_is_attachment);
    }

    /**
     * Download as data.
     * @param string $name Report name
     * @param array $params Report parameters
     * @param string $format Report format
     * @param string $filename Report filename
     * @return array|null Report data
     * @throws \yii\web\HttpException
     */
    public function & asData($name, $params, $format, $filename)
    {
        return $this->generate($name, $params, $format, $filename, false);
    }
}

class ReportesJasperController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * @var array Report data.
     */
    private $reportData;

    /**
     * Render report in a view.
     * @return string
     */
    public function report()
    {
        if ($this->reportData) {
            $report_data = &$this->reportData;
            return '<script>saveAs(new Blob([base64toBlob("' . $report_data['data'] . '", "' . $report_data['type'] . '")], {type: "' . $report_data['type'] . '"}), "' . $report_data['name'] . '");</script>';
        }
    }

    /**
     * Generate report.
     * @param string $name Report name
     * @param array $params Report parameters
     * @param string $format Report format
     * @param string $filename Report filename
     * @param bool $download Download option
     * @param bool $download_is_attachment Download as attachment
     * @throws \yii\web\HttpException
     */
    public function generateReport($name, $params, $format, $filename, $download, $download_is_attachment = true)
    {
        $report = new Report();
        // show($report);
        $this->reportData = &$report->generate(
            $name,
            $params,
            $format,
            $filename,
            $download,
            $download_is_attachment
        );
    }

    /**
     * Generate report by json data provider.
     * @param string $name Report name
     * @param array $params Report parameters
     * @param string $format Report format
     * @param string $filename Report filename
     * @param bool $download Download option
     * @param bool $download_is_attachment Download as attachment
     * @throws \yii\web\HttpException
     */
    public function generateReportByJson($name, $params, $format, $filename, $download, $download_is_attachment = true,$tipo=0)
    {
        $report = new Report();
        // show($name.''.$params);
        $this->reportData = &$report->generateByJsonDataProvider(
            $name,
            $params,
            $format,
            $filename,
            $download,
            $download_is_attachment,$tipo
        );
    }

    public function reporteActividades()
    {
        $id = intval(Input::get("id"));
        if (($id = intval($id)) > 0) {
            $this->generateReport(
                'ReporteActividades',
                [
                    'ID_DIRECCION' => $id
                ],
                'pdf',
                'Reporte de Disposiciones generales',
                true,
                false
            );
        }
    }
}
