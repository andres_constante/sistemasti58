<?php

namespace App\Http\Controllers;

use App\DonaDonacionesDetaModel;
use App\DonaDonacionesModel;
use App\DonaItemsModel;
use App\User;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\Coordinacioncronograma\Entities\CoordinacionDetaDireccionModel;
use stdClass;
use Map;
use Intervention\Image\ImageManagerStatic as Image;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaAsiganrModel;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabModel;
use Yajra\DataTables\Facades\DataTables;

class DonaDonacionesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    var $configuraciongeneral = array("ENTREGA DE DONACIONES", "coordinacioncronograma/donaciones", "index", 6 => "donacionesajax", 7 => "donaciones");
    var $escoja = array(null => "Escoja opción...");
    var $estados = [
        'PENDIENTE' => 'PENDIENTE',
        'ENTREGADO' => 'ENTREGADO',
        'NO AMERITA' => 'NO AMERITA'

    ];

    var $cedulas = [
        '1309666517',
        '1309904413',
        '1311778995',
        '1314689827',
        '0921083168',
        '1715132443',
        '1317095014',
        '1351659030',
        '1309029708'
    ];
    var $cedula_eliminar = [
        '1314689827',
        '1310803794',
        '1317095014'
    ];
    var $pupilos_jorge = [
        '1310803794',
        '1316060316',
        '1313505602',
        '1313466581',
        '1312573692',
        '1311810848',
        '1309665774',
        '1314346600',
        '1309954624',
        '1312044918',
        '1308730694',
        '0960383057',
        '1310090095',
        '1312429887',
        '1311542011',
        '1300997721',
        '1308987971',
        '1310260102',
        '1310340250',
        '1310105141',
        '1304344805'
    ];
    var $cedula_editar = [
        '1351659030',
        '1317095014'
    ];
    var $objetos = '[
        {"Tipo":"datetimetext","Descripcion":"Fecha solicitud","Nombre":"fecha_ingreso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tipo documento (*)","Nombre":"tipo_documento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Cédula (*)","Nombre":"cedula_beneficiario","Clase":"cedula mayuscula","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tercera edad","Nombre":"tercera_edad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Discapacitad","Nombre":"discapacitado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha nacimiento","Nombre":"fecha_nacimiento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Edad","Nombre":"edad","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Nombre del beneficiario (*)","Nombre":"nombre_beneficiario","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tipo donación (*)","Nombre":"id_donacion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Cantidad (*)","Nombre":"cantidad","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Semana","Nombre":"semana","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Observaciones","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetimetext","Descripcion":"Fecha de entrega","Nombre":"fecha_entrega","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Parroquia (*)","Nombre":"parroquia_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Barrio (*)","Nombre":"barrio_id","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Dirección (*)","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Teléfono","Nombre":"telefono","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Correo","Nombre":"correo","Clase":"correo","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Asignado a","Nombre":"id_responsable","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Adjunto de solicitud del ciudadano","Nombre":"adjunto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Adjunto respuesta","Nombre":"adjunto_entrega","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Cédula frontal","Nombre":"frontal","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Cédula posterior","Nombre":"trasera","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Foto de entrega","Nombre":"entrega","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Ficha","Nombre":"ficha","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Observacion final","Nombre":"observacion_final","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetimetext","Descripcion":"Fecha de entrega","Nombre":"fecha_entrega","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"mapa","Descripcion":"Mapa","Nombre":"mapa","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado_donacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Longitud","Nombre":"longitud","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Latitud","Nombre":"latitud","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "nombre_beneficiario" => "nombre_beneficiario: {
                            required: true
                        }",
        "cedula_beneficiario" => "cedula_beneficiario: {
                            required: true
                        }",
        "donacion" => "donacion: {
                            required: true
                        }",
        "direccion" => "direccion: {
                            required: true
                        }",
        // "telefono" => "telefono: {
        //                     required: true
        //                 }",
        // "observacion" => "observacion: {
        //                     required: true
        //                 }",

    );

    var $filtros = '[
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Cedula beneficiario","Nombre":"cedula_beneficiario","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';


    public function verpermisos($objetos = array(), $tipo = "index")
    {
        // show(Input::all());
        // if(){

        // }
        $tabla = $tabla = DonaDonacionesModel::select('dona_donciones.*', 'u.name', 'd.nombre', 'p.parroquia', 'b.barrio', 'ur.name as responsable')
            ->join("users as u", "u.id", "=", "dona_donciones.id_usuario")
            ->join("users as ur", "ur.id", "=", "dona_donciones.id_responsable")
            ->join("dona_items as d", "d.id", "=", "dona_donciones.id_donacion")
            ->join("barrio as b", "b.id", "=", "dona_donciones.barrio_id")
            ->join("parroquia as p", "p.id", "=", "b.id_parroquia")
            ->where("dona_donciones.estado", "ACT");
        return array($objetos, $tabla);
    }
    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) {
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        }
        return $object;
    }
    public function donacionesajax(Request $request)
    {
        $objetos = json_decode($this->objetos);
        $tabla = $this->verpermisos($objetos);
        $objetos = $tabla[0];

        //show($objetos);
        $tabla  = DonaDonacionesModel::select('dona_donciones.*', 'u.name', 'd.nombre', 'p.parroquia', 'b.barrio', 'ur.name as responsable')
            ->join("users as u", "u.id", "=", "dona_donciones.id_usuario")
            ->join("users as ur", "ur.id", "=", "dona_donciones.id_responsable")
            ->join("dona_items as d", "d.id", "=", "dona_donciones.id_donacion")
            ->join("barrio as b", "b.id", "=", "dona_donciones.barrio_id")
            ->join("parroquia as p", "p.id", "=", "b.id_parroquia")
            ->where(["dona_donciones.estado" => "ACT", "estado_donacion" => Input::get('estado')]);
        //show($tabla->get());
        $cedula = Input::get('cedula_beneficiario');
        if ($cedula != null && $cedula != '') {
            $tabla = $tabla->where(["dona_donciones.cedula_beneficiario" => $cedula]);
        }
        ///
        $columns = array();
        $columns[] = 'id';
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'id_direccion')->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 12  || $id_tipo_pefil->tipo == 3 || in_array(Auth::user()->cedula, $this->pupilos_jorge)) {

            // if(Auth::user()->cedula != "1310803794"){
            $tabla->where('id_responsable', Auth::user()->id);

            // }
        }

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        // dd($start);


        if (empty($request->input('search.value'))) {


            if ($limit == '-1') {
                // dd($limit);
                $posts = $tabla
                    // ->offset($start)
                    // ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $posts = $tabla->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            }
        } else {
            $search = $request->input('search.value');

            if ($limit == '-1') {
                $posts = $tabla->where(function ($query) use ($search) {
                    $query->where('dona_donciones.nombre_beneficiario', 'LIKE', "%{$search}%")
                        ->orWhere('dona_donciones.cedula_beneficiario', 'LIKE', "%{$search}%");
                })
                    // ->offset($start)
                    // ->limit($limit)
                    ->orderBy($order, $dir);
                $totalFiltered = $posts->count();
                $posts = $posts->get();
            } else {
                $posts = $tabla->where(function ($query) use ($search) {
                    $query->where('dona_donciones.nombre_beneficiario', 'LIKE', "%{$search}%")
                        ->orWhere('dona_donciones.cedula_beneficiario', 'LIKE', "%{$search}%");
                })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir);
                $totalFiltered = $posts->count();
                $posts = $posts->get();
            }
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {
                $permisos = $this->perfil();

                $acciones = '';
                if (Auth::user()->id_perfil == 1 || Auth::user()->cedula == '1314689827') {
                    $acciones .= '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash  btn btn-danger"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                    <input name="_token" type="hidden" value="' . csrf_token() . '">
                    <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                }
                // dd($post);
                $acciones .= link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', ' EDITAR', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o btn btn-info'));
                if ($post->adjunto != null) {
                    if (strpos($post->adjunto, 'pdf') === false) {
                        $adjunto = "<a onclick='mostrarImagen(\"" . URL::to('/archivos_sistema/' . $post->adjunto) . "\")' class='btn btn-info dropdown-toggle '><i class='fa fa-picture-o'></i></a>";
                    } else {
                        $adjunto = ($post->adjunto != null) ? "<a href='" . URL::to('/archivos_sistema/' . $post->adjunto) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>" : "";
                    }
                } else {
                    $adjunto = '';
                }
                if ($post->adjunto_entrega != null) {
                    // dd(strpos($post->adjunto_entrega,'pdf'));
                    // dd($post->adjunto_entrega);
                    if (strpos($post->adjunto_entrega, 'pdf') === false) {
                        $adjunto_entrega = "<a onclick='mostrarImagen(\"" . URL::to('/archivos_sistema/' . $post->adjunto_entrega) . "\")' class='btn btn-info dropdown-toggle '><i class='fa fa-picture-o'></i></a>";
                    } else {
                        $adjunto_entrega = ($post->adjunto_entrega != null) ? "<a href='" . URL::to('/archivos_sistema/' . $post->adjunto_entrega) . "'  class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>" : "";
                    }
                } else {
                    $adjunto_entrega = '';
                }
                if ($post->frontal != null) {
                    if (strpos($post->frontal, 'pdf') === false) {
                        $frontal = "<a onclick='mostrarImagen(\"" . URL::to('/archivos_sistema/' . $post->frontal) . "\")' class='btn btn-info dropdown-toggle '><i class='fa fa-picture-o'></i></a>";
                    } else {
                        $frontal = ($post->frontal != null) ? "<a href='" . URL::to('/archivos_sistema/' . $post->frontal) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>" : "";
                    }
                } else {
                    $frontal = '';
                }
                if ($post->trasera != null) {
                    if (strpos($post->trasera, 'pdf') === false) {
                        $trasera = "<a onclick='mostrarImagen(\"" . URL::to('/archivos_sistema/' . $post->trasera) . "\")' class='btn btn-info dropdown-toggle '><i class='fa fa-picture-o'></i></a>";
                    } else {
                        $trasera = ($post->trasera != null) ? "<a href='" . URL::to('/archivos_sistema/' . $post->trasera) . "'  class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>" : "";
                    }
                } else {
                    $trasera = '';
                }
                if ($post->entrega != null) {
                    if (strpos($post->entrega, 'pdf') === false) {
                        $entrega = "<a onclick='mostrarImagen(\"" . URL::to('/archivos_sistema/' . $post->entrega) . "\")' class='btn btn-info dropdown-toggle '><i class='fa fa-picture-o'></i></a>";
                    } else {
                        $entrega = ($post->entrega != null) ? "<a href='" . URL::to('/archivos_sistema/' . $post->entrega) . "'  class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>" : "";
                    }
                } else {
                    $entrega = '';
                }
                if ($post->ficha != null) {
                    if (strpos($post->ficha, 'pdf') === false) {
                        $ficha = "<a onclick='mostrarImagen(\"" . URL::to('/archivos_sistema/' . $post->ficha) . "\")' class='btn btn-info dropdown-toggle '><i class='fa fa-picture-o'></i></a>";
                    } else {
                        $ficha = ($post->ficha != null) ? "<a href='" . URL::to('/archivos_sistema/' . $post->ficha) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>" : "";
                    }
                } else {
                    $ficha = '';
                }

                if ($post->estado_donacion == "ENTREGADO") {
                    if (Auth::user()->id_perfil != 1 && !in_array(Auth::user()->cedula, $this->cedula_eliminar) && !in_array(Auth::user()->cedula, $this->cedula_editar)) {
                        $acciones = '';
                    }
                }
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $nestedData['id_donacion'] = $post->nombre;
                $nestedData['barrio_id'] = $post->barrio;
                $nestedData['parroquia_id'] = $post->parroquia;
                $nestedData['adjunto'] = $adjunto;
                $nestedData['adjunto_entrega'] = $adjunto_entrega;
                $nestedData['frontal'] = $frontal;
                $nestedData['trasera'] = $trasera;
                $nestedData['entrega'] = $entrega;
                $nestedData['ficha'] = $ficha;
                $nestedData['ficha'] = $ficha;
                // $nestedData['usuario_registra'] =;
                $dona = DonaDonacionesDetaModel::join('users as u', 'u.id', 'id_usuario')
                    ->select('dona_donciones_deta.*', 'name')
                    ->where('id_donacione_cab', $post->id)->orderBy('dona_donciones_deta.created_at', 'DESC')->first();
                if ($dona) {
                    $nestedData['acciones'] =  $acciones . ' <br> <b>Usuario registra: </b>' . $post->name . ' - ' . $post->created_at . (($post->created_at != $dona->created_at) ? ' <br><b>Modificado por: </b>' . $dona->name . ' - ' . $dona->created_at : '');
                } else {
                    $nestedData['acciones'] =  $acciones . ' <br> <b>Usuario registra: </b>' . $post->name . ' - ' . $post->created_at;
                }
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = json_decode($this->objetos);
        $filtros = json_decode($this->filtros);
        $filtros[0]->Valor = $this->estados;
        unset($objetos[3]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[6]);
        unset($objetos[10]);
        unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[18]);
        unset($objetos[19]);
        // unset($objetos[17]);
        // unset($objetos[18]);
        // unset($objetos[19]);
        // unset($objetos[20]);
        unset($objetos[27]);
        unset($objetos[29]);
        unset($objetos[30]);
        unset($objetos[31]);

        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $eliminar = 'no';
        if ($id_tipo_pefil->tipo == 11 || $id_tipo_pefil->tipo == 1) {
            if ($id_tipo_pefil->tipo == 1 || Auth::user()->cedula == '1314689827') {
                $eliminar = 'si';
            }
            $create = 'si';
        } else {
            // if (in_array(Auth::user()->cedula, $this->cedulas)) {
            $create = 'si';
            // } else {
            //     $create = 'no';
            // }
        }

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            'filtros' => $filtros,
            "configuraciongeneral" => $this->configuraciongeneral,
            'create' => $create,
            'delete' => $eliminar
        ]);
        //return view('coordinacioncronograma::index');
    }

    public function formularioscrear($id)
    {
        /*QUITAR CAMPOS*/
        $objetos = json_decode($this->objetos);
        $objetos = $this->verpermisos($objetos, "crear");
        $items = DonaItemsModel::where('estado', 'ACT')->orderby('orden', 'ASC')->pluck('nombre', 'id')->all();
        $datos = array();
        $objetos[0][1]->Valor = ['CEDULA' => "CEDULA", 'PASAPORTE' => "PASAPORTE"];
        $objetos[0][3]->Valor = ['NO' => "NO", 'SI' => "SI"];
        $objetos[0][4]->Valor = ['NO' => "NO", 'SI' => "SI"];
        $objetos[0][8]->Valor = $items;
        $objetos[0][8]->Adicional = '<button id="agregar_item" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Agregar Donación </button>';

        // $objetos[0][6]->Valor =date('Y-m-d');
        $objetos[0][13]->Valor =  $this->escoja + parroquiaModel::where('estado', 'ACT')->wherenotin('id', [9])->pluck('parroquia', 'id')->all();
        $objetos[0][27]->Adicional = '<button  onClick="localizar()" type="button"><i class="fa fa-location-arrow" aria-hidden="true"></i> Posicionarme en el mapa </button>';
        $objetos[0][14]->Valor = [];
        $objetos[0][10]->Valor = ['1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9];

        // show($objetos[0]);

        // show($objetos[0]);
        unset($objetos[0][12]);
        // unset($objetos[0][11]);
        // unset($objetos[0][13]);
        // unset($objetos[0][14]);
        $objetos[0][18]->Valor =
            UsuariosModel::where('id', Auth::user()->id)->pluck('name', 'id')->all()
            + UsuariosModel::where('estado', 'ACT')->whereIn('id_perfil', [40])->pluck('name', 'id')->all()
            + UsuariosModel::where('cedula', '0921083168')->pluck('name', 'id')->all()
            + UsuariosModel::where('cedula', '1307916476')->pluck('name', 'id')->all();
        $objetos[0][28]->Valor = $this->estados;

        $latitud = '-0.9484126160642922';
        $longitud = '-80.72165966033936';
        $autozoom = "18";
        $actiondrag = '$("#latitud").val(event.latLng.lat()); $("#longitud").val(event.latLng.lng());';
        $config['center'] = "$latitud,$longitud";
        $config['zoom'] = $autozoom; //'auto';
        $config['onclick'] = $actiondrag;
        Map::initialize($config);
        //Marker
        $marker = array();
        $marker['position'] = "$latitud,$longitud";
        $marker['draggable'] = true;
        $marker['ondragend'] = $actiondrag;
        Map::add_marker($marker);
        $data['map'] = Map::create_map();



        if ($id == "") {
            $this->configuraciongeneral[2] = "crear";
            $objetos[0][0]->Valor = date('Y-m-d H:i');
            $objetos[0][26]->Valor = date('Y-m-d H:i');
            // show($objetos[0]);



            // if (!in_array(Auth::user()->cedula, $this->cedulas) || Auth::user()->cedula == '1310803794') {
            if (in_array(Auth::user()->cedula, $this->pupilos_jorge)) {
                unset($objetos[0][20]);
                unset($objetos[0][21]);
                unset($objetos[0][22]);
                unset($objetos[0][23]);
                unset($objetos[0][24]);
                unset($objetos[0][25]);
                unset($objetos[0][26]);
                unset($objetos[0][27]);
                unset($objetos[0][29]);
                unset($objetos[0][30]);
                // if (Auth::user()->cedula == '1310803794') {
                $objetos[0][28]->Valor = ['ENTREGADO' => 'ENTREGADO'];
                // }
            }
            $datos = [
                "objetos" => $objetos[0],
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "mapa" => $data,
                "autoLocalizar" => "SI"

            ];
            // show($objetos[0]);
        } else {
            $this->configuraciongeneral[2] = "editar";
            $id = intval(Crypt::decrypt($id));
            $tabla = $objetos[1]->where("dona_donciones.id", $id)->first();

            $parroquia_id = barrioModel::join('parroquia as p', 'p.id', 'id_parroquia')->where(['barrio.estado' => 'ACT', 'barrio.id' => $tabla->barrio_id])->first();
            $objetos[0][13]->ValorAnterior = $parroquia_id->id_parroquia;


            if ($tabla->fecha_entrega == null || $tabla->fecha_entrega == '') {
                $tabla->fecha_entrega = date('Y-m-d H:i');
            }

            if ($tabla->fecha_ingreso == null || $tabla->fecha_ingreso == '') {
                $tabla->fecha_ingreso = date('Y-m-d H:i', strtotime($tabla->created_at));
            }

            $objetos[0][14]->Valor = barrioModel::where(['estado' => 'ACT', 'id_parroquia' => $parroquia_id->id_parroquia])->pluck('barrio', 'id')->all();
            $objetos[0][14]->ValorAnterior = $tabla->barrio_id;
            $objetos[0][18]->Valor = [$tabla->id_responsable => $tabla->responsable]
                + UsuariosModel::where('estado', 'ACT')->whereIn('id_perfil', [40])->pluck('name', 'id')->all()
                + UsuariosModel::where('cedula', '0921083168')->pluck('name', 'id')->all()
                + UsuariosModel::where('cedula', '1307916476')->pluck('name', 'id')->all();

            $objetos[0][18]->ValorAnterior = $tabla->id_responsable;

            $perfiles = [1, 11];
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
            if (!in_array($id_tipo_pefil->tipo, $perfiles)) {

                // dd('');
                $objetos[0][19]->Tipo = "file-solo";
                $items = DonaItemsModel::where('id', $tabla->id_donacion)->orderby('orden', 'ASC')->first();
                $objetos[0][1]->Valor = $tabla->tipo_documento;
                $objetos[0][1]->Tipo = 'html2';

                $objetos[0][8]->Valor = [$items->id => $items->nombre];
                $objetos[0][8]->Adicional = '';
                $objetos[0][8]->Clase = 'Null';

                // show($objetos[0]);
                $objetos[0][13]->Valor = $parroquia_id->parroquia;
                $objetos[0][13]->Tipo = 'html2';
                $objetos[0][14]->Valor = $parroquia_id->barrio;
                $objetos[0][14]->Tipo = 'html2';

                $objetos[0][18]->Valor = $tabla->responsable;
                $objetos[0][18]->Tipo = 'html2';
                $objetos[0][18]->Clase = 'Null';

                foreach ($objetos[0] as $key => $vv) {
                    if ($vv->Tipo == 'text' && $vv->Nombre != 'telefono') {
                        $valor = $vv->Nombre;
                        $objetos[0][$key]->Tipo = 'html2';
                        $objetos[0][$key]->Valor = $tabla->$valor;
                    }
                    if ($vv->Nombre == 'observacion') {
                        $valor = $vv->Nombre;
                        $objetos[0][$key]->Tipo = 'html2';
                        $objetos[0][$key]->Valor = $tabla->$valor;
                    }
                    if ($vv->Nombre == 'fecha_ingreso') {
                        $valor = $vv->Nombre;
                        $objetos[0][$key]->Tipo = 'html2';
                        $objetos[0][$key]->Valor = $tabla->$valor;
                    }
                }
            }


            if (in_array(Auth::user()->cedula, $this->pupilos_jorge) && $tabla->estado_donacion == 'ENTREGADO') {
                $objetos[0][19]->Tipo = "file-solo";
                $items = DonaItemsModel::where('id', $tabla->id_donacion)->orderby('orden', 'ASC')->first();
                $objetos[0][1]->Valor = $tabla->tipo_documento;
                $objetos[0][1]->Tipo = 'html2';

                $objetos[0][8]->Valor = [$items->id => $items->nombre];
                $objetos[0][8]->Adicional = '';
                $objetos[0][8]->Clase = 'Null';

                // show($objetos[0]);
                $objetos[0][13]->Valor = $parroquia_id->parroquia;
                $objetos[0][13]->Tipo = 'html2';
                $objetos[0][14]->Valor = $parroquia_id->barrio;
                $objetos[0][14]->Tipo = 'html2';

                $objetos[0][18]->Valor = $tabla->responsable;
                $objetos[0][18]->Tipo = 'html2';
                $objetos[0][18]->Clase = 'Null';
                $array_nombres = ['observacion', 'fecha_ingreso', 'fecha_entrega'];
                foreach ($objetos[0] as $key => $vv) {
                    if ($vv->Tipo == 'text' && $vv->Nombre != 'telefono') {
                        $valor = $vv->Nombre;
                        $objetos[0][$key]->Tipo = 'html2';
                        $objetos[0][$key]->Valor = $tabla->$valor;
                    }

                    if (in_array($vv->Nombre, $array_nombres)) {
                        $valor = $vv->Nombre;
                        $objetos[0][$key]->Tipo = 'html2';
                        $objetos[0][$key]->Valor = $tabla->$valor;
                    }
                }
                unset($objetos[0][28]);
            }

            // show($objetos[0]);
            // 
            // $objetos[0][1]->Tipo = 'html2';


            $datos = [
                "tabla" => $tabla,
                "objetos" => $objetos[0],
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "mapa" => $data,
                "autoLocalizar" => "SI"
            ];
        }
        return view('vistas.create', $datos);
    }

    public function create()
    {
        $perfiles = [1, 11];
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if (!in_array($id_tipo_pefil->tipo, $perfiles)) {
            // if (!in_array(Auth::user()->cedula, $this->cedulas)) {
            //     Session::flash('error', 'Ud no tiene permisos para crear');
            //     return Redirect::to($this->configuraciongeneral[1]);
            // }
        }
        return $this->formularioscrear("");
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $id = intval(Crypt::decrypt($id));
        $objetos = $this->verpermisos(json_decode($this->objetos));
        $tabla = $objetos[1]->where("coor_tmae_coordinacion_main.id", $id)->first();

        $this->configuraciongeneral[3] = "5"; //Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = "si";
        $objetos = $objetos[0];
        $objetos[1]->Nombre = "responsable";
        //$dicecionesImagenes = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "<>", "pdf"], ["tipo", "5"]])->get();
        //$dicecionesDocumentos = ArchivosModel::where([["id_referencia", $tabla->id], ["tipo_archivo", "=", "pdf"], ["tipo", "5"]])->get();
        return view('vistas.show', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
        //return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
        //return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $tabla = DonaDonacionesModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }


    public function guardar($id)
    {
        $input = Input::all();
        // show($input);
        $ruta = $this->configuraciongeneral[1];

        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new DonaDonacionesModel();
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro de Datos";
        } else {
            $ruta .= "/" . Crypt::encrypt($id) . "/edit";
            $guardar = DonaDonacionesModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Actualización de Datos";
        }

        $input = Input::all();
        // show($input);
        $arrapas = array();
        if ($id == 0) {
            $validator = Validator::make($input, DonaDonacionesModel::rules($id));
        } else {
            $validator = Validator::make($input, DonaDonacionesModel::rules_2($id));
        }

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            DB::beginTransaction();
            try {
                $timeline = new DonaDonacionesDetaModel();
                foreach ($input as $key => $value) {

                    if ($key != "_method" && $key != "_token" && $key != "parroquia_id") {
                        $guardar->$key = $value;
                        $timeline->$key = $value;
                    }
                }
                if ($id == 0) {
                    $guardar->id_usuario = Auth::user()->id;
                }
                if ($guardar->estado_donacion == 'PENDIENTE') {
                    $guardar->fecha_entrega = null;
                    $timeline->fecha_entrega =  null;
                }
                if (in_array(Auth::user()->cedula, $this->pupilos_jorge) && $guardar->estado_donacion == 'ENTREGADO') {
                    if ($guardar->fecha_entrega == null) {
                        $guardar->fecha_entrega =  $guardar->fecha_ingreso;
                        $timeline->fecha_entrega =  $guardar->fecha_ingreso;
                    }
                }
                if (in_array(Auth::user()->cedula, $this->pupilos_jorge)) {
                    if (date('m', strtotime($guardar->fecha_ingreso)) >= 5) {

                        if (date('m', strtotime($guardar->fecha_ingreso)) >= 6) {
                            return Redirect::to("$ruta")
                                ->withErrors(['Sr. ' . Auth::user()->name . ', no tiene permiso para ingresar donaciones en este mes.'])
                                ->withInput();
                        }
                        if (date('m', strtotime($guardar->fecha_ingreso)) == 5) {
                            if (date('d', strtotime($guardar->fecha_ingreso)) > 8) {
                                return Redirect::to("$ruta")
                                    ->withErrors(['Sr. ' . Auth::user()->name . ', no tiene permiso para ingresar donaciones en este mes.'])
                                    ->withInput();
                            }
                        }
                    }
                }

                if ($guardar->estado_donacion == 'ENTREGADO' && $guardar->fecha_entrega == null) {
                    // dd('');
                    $guardar->fecha_entrega = date('Y-m-d H:i');
                    $timeline->fecha_entrega = date('Y-m-d H:i');
                }

                if ($guardar->id_donacion != 9) {
                    $guardar->semana = 0;
                    $timeline->semana = 0;
                }
                $guardar->save();

                $id = $guardar->id;
                $idcab = $guardar->id;

                $timeline->id_usuario = Auth::user()->id;
                $timeline->id_donacion     = $guardar->id_donacion;
                $timeline->id_responsable     = $guardar->id_responsable;
                $timeline->id_donacione_cab     = $idcab;


                $timeline->save();
                /*ARCHIVO*/
                if (Input::hasFile("adjunto")) {
                    $dir = public_path() . '/archivos_sistema/';
                    $docs = Input::file("adjunto");
                    if ($docs) {
                        $fileName = "donacion-$idcab-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                        $ext = $docs->getClientOriginalExtension();
                        $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                        if (!in_array($ext, $perfiles)) {
                            DB::rollback();
                            $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                            return redirect()->back()->withErrors([$mensaje])->withInput();
                        }

                        $file = DonaDonacionesModel::find($idcab);
                        $oldfile = $dir . $file->adjunto;
                        File::delete($oldfile);
                        $file->adjunto = $fileName;
                        $file->save();
                        $docs->move($dir, $fileName);
                        // if ($ext == 'pdf' || $ext == 'PDF') {
                        // } else {
                        //     $image_resize = Image::make($docs->getRealPath());
                        //     $image_resize->resize(1200, 1800);
                        //     $image_resize->save($dir . $fileName);
                        // }
                    }
                }
                /*ARCHIVO*/
                if (Input::hasFile("adjunto_entrega")) {
                    $dir = public_path() . '/archivos_sistema/';
                    $docs = Input::file("adjunto_entrega");
                    if ($docs) {
                        $fileName = "donacion_entrega-$id-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                        $ext = $docs->getClientOriginalExtension();
                        $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                        if (!in_array($ext, $perfiles)) {
                            DB::rollback();
                            $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                            return redirect()->back()->withErrors([$mensaje])->withInput();
                        }

                        $file = DonaDonacionesModel::find($idcab);
                        $oldfile = $dir . $file->adjunto_entrega;
                        File::delete($oldfile);
                        $file->adjunto_entrega = $fileName;
                        $file->save();
                        $docs->move($dir, $fileName);
                        // if ($ext == 'pdf' || $ext == 'PDF') {
                        // } else {
                        //     $image_resize = Image::make($docs->getRealPath());
                        //     $image_resize->resize(1200, 1800);
                        //     $image_resize->save($dir . $fileName);
                        // }
                    }
                }
                if (Input::hasFile("frontal")) {
                    $dir = public_path() . '/archivos_sistema/';
                    $docs = Input::file("frontal");
                    if ($docs) {
                        $fileName = "donacion_frontal-$id-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                        $ext = $docs->getClientOriginalExtension();
                        $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                        if (!in_array($ext, $perfiles)) {
                            DB::rollback();
                            $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                            return redirect()->back()->withErrors([$mensaje])->withInput();
                        }




                        $file = DonaDonacionesModel::find($idcab);
                        $oldfile = $dir . $file->frontal;
                        File::delete($oldfile);
                        $file->frontal = $fileName;
                        $file->save();
                        $docs->move($dir, $fileName);
                        // if ($ext == 'pdf' || $ext == 'PDF') {
                        // } else {
                        //     $image_resize = Image::make($docs->getRealPath());
                        //     $image_resize->resize(1200, 1800);
                        //     $image_resize->save($dir . $fileName);
                        // }
                    }
                }
                if (Input::hasFile("trasera")) {
                    $dir = public_path() . '/archivos_sistema/';
                    $docs = Input::file("trasera");
                    if ($docs) {
                        $fileName = "donacion_trasera-$id-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                        $ext = $docs->getClientOriginalExtension();
                        $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                        if (!in_array($ext, $perfiles)) {
                            DB::rollback();
                            $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                            return redirect()->back()->withErrors([$mensaje])->withInput();
                        }

                        $file = DonaDonacionesModel::find($idcab);
                        $oldfile = $dir . $file->trasera;
                        File::delete($oldfile);
                        $file->trasera = $fileName;
                        $file->save();
                        $docs->move($dir, $fileName);
                        // if ($ext == 'pdf' || $ext == 'PDF') {
                        // } else {
                        //     $image_resize = Image::make($docs->getRealPath());
                        //     $image_resize->resize(1200, 1800);
                        //     $image_resize->save($dir . $fileName);
                        // }
                    }
                }
                if (Input::hasFile("entrega")) {
                    $dir = public_path() . '/archivos_sistema/';
                    $docs = Input::file("entrega");
                    if ($docs) {
                        $fileName = "donacion_entrega-$id-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                        $ext = $docs->getClientOriginalExtension();
                        $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                        if (!in_array($ext, $perfiles)) {
                            DB::rollback();
                            $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                            return redirect()->back()->withErrors([$mensaje])->withInput();
                        }

                        $file = DonaDonacionesModel::find($idcab);
                        $oldfile = $dir . $file->entrega;
                        File::delete($oldfile);
                        $file->entrega = $fileName;
                        $file->save();
                        $docs->move($dir, $fileName);
                        // if ($ext == 'pdf' || $ext == 'PDF') {
                        // } else {
                        //     $image_resize = Image::make($docs->getRealPath());
                        //     $image_resize->resize(1200, 1800);
                        //     $image_resize->save($dir . $fileName);
                        // }
                    }
                }
                if (Input::hasFile("ficha")) {
                    $dir = public_path() . '/archivos_sistema/';
                    $docs = Input::file("ficha");
                    if ($docs) {
                        $fileName = "donacion_ficha-$id-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                        $ext = $docs->getClientOriginalExtension();
                        $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                        if (!in_array($ext, $perfiles)) {
                            DB::rollback();
                            $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                            return redirect()->back()->withErrors([$mensaje])->withInput();
                        }

                        $file = DonaDonacionesModel::find($idcab);
                        $oldfile = $dir . $file->ficha;
                        File::delete($oldfile);
                        $file->ficha = $fileName;
                        // dd($fileName);
                        $file->save();
                        $docs->move($dir, $fileName);
                        // if ($ext == 'pdf' || $ext == 'PDF') {
                        // } else {
                        //     $image_resize = Image::make($docs->getRealPath());
                        //     $image_resize->resize(1200, 1800);
                        //     $image_resize->save($dir . $fileName);
                        // }
                    }
                }
                Auditoria($msgauditoria . " - ID: " . $id . "- donacion");
                DB::commit();
            } //Fin Try


            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage() . " - " . $e->getLine();
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }
    public function agregar_item()
    {

        try {
            //code...
            $nombre = strtoupper(Input::get('nombre'));
            if ($nombre == null || $nombre == '') {
                $datos = DonaItemsModel::orderby('nombre', 'ASC')->get();
                return response()->json(["estado" => "error", "msg" => "Datos erroneos", "datos" => $datos], 200);
            }
            $item = DonaItemsModel::where('nombre', $nombre)->first();

            if ($item) {
                $datos = DonaItemsModel::get();
                return response()->json(["estado" => "error", "msg" => $nombre . ", ya se encuentra registrado", "datos" => $datos], 200);
            }
            $item = new DonaItemsModel();
            $item->nombre = $nombre;
            $item->save();
            $datos = DonaItemsModel::orderby('nombre', 'ASC')->get();

            return response()->json(["estado" => "ok", "msg" => "Agregado Exitosamente", "datos" => $datos, "seleccionado" => $item->id], 200);
        } catch (\Throwable $th) {
            $datos = DonaItemsModel::orderby('nombre', 'ASC')->get();
            //throw $th;
            return response()->json(["estado" => "error", "msg" => "No se pudo agregar", "datos" => $datos], 200);
        }
    }
    public function entregar_donacion()
    {

        try {
            //code...
            $obs = Input::get('obs');
            $fecha_entrega = Input::get('fecha_entrega');
            $id = Input::get('id');
            // $adjunto = Input::get('adjunto');
            $tipo = Input::get('tipo');
            $id = Crypt::decrypt($id);
            $donacion = DonaDonacionesModel::find($id);
            // dd(Input::all());
            if ($tipo == 1) {
                // $donacion->fecha_entrega = date('Y-m-d H:m:s');
                $donacion->observacion_final = $obs;
                $donacion->fecha_entrega = $fecha_entrega;
                $donacion->estado_donacion = 'ENTREGADO';
                if (Input::hasFile("adjunto")) {
                    // dd()
                    // show(Input::all());
                    $dir = public_path() . '/archivos_sistema/';
                    $docs = Input::file("adjunto");
                    if ($docs) {
                        $fileName = "donacion_entrega-$id-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                        $ext = $docs->getClientOriginalExtension();
                        $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                        if (!in_array($ext, $perfiles)) {
                            DB::rollback();
                            $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                            return redirect()->back()->withErrors([$mensaje])->withInput();
                        }


                        $oldfile = $dir . $donacion->adjunto_entrega;
                        File::delete($oldfile);
                        $donacion->adjunto_entrega = $fileName;
                        $donacion->save();
                        $docs->move($dir, $fileName);
                    }
                }
            } else {
                $donacion->estado_donacion = 'ARCHIVADO';
                $donacion->observacion_final = $obs;
            }
            $donacion->save();

            return response()->json(["estado" => "ok", "msg" => "Agregado Exitosamente"], 200);
        } catch (\Throwable $th) {
            return $th;
            return response()->json(["estado" => "error", "msg" => "No se pudo agregar"], 200);
        }
    }
    public function consultardinaciones()
    {

        try {
            //code...
            $cedula = Input::get('documento');

            $donaciones = DonaDonacionesModel::select('dona_donciones.*', 'u.name', 'd.nombre', 'dd.direccion')
                ->join("users as u", "u.id", "=", "dona_donciones.id_usuario")
                ->leftjoin("tmae_direcciones as dd", "dd.id", "=", "u.id_direccion")
                ->join("dona_items as d", "d.id", "=", "dona_donciones.id_donacion")
                ->where("dona_donciones.estado", "ACT")
                ->where('cedula_beneficiario', $cedula)
                // ->where('estado_donacion','ENTREGADO')
                // $post->estado_donacion=="ENTREGADO"
                ->get();
            $html = null;
            if (isset($donaciones[0])) {
                $html = '
                <div style=" font-size: larger;"><b>HISTORIAL DE DONACIONES</b> <br> ' . $donaciones[0]->nombre_beneficiario . '</div><br>
                <table class="table" >
                <thead>
                <tr>
                <th>Cant.</th>
                <th>Tipo de donación</th>
                <th>Estado</th>
                <th>Solicitado</th>
                <th>Entregado</th>
                </tr>
                </thead>
                <tbody>';
                foreach ($donaciones as $key => $value) {
                    $html .= '<tr>
                    <td>' . $value->cantidad . ' </td>
                    <td>' . $value->nombre . ' </td>
                    <td>' . $value->estado_donacion . ' </td>
                    <td>' . $value->fecha_ingreso . ' </td>
                    <td>' . $value->fecha_entrega . ' </td>
                    </tr>';
                }


                $html .= '</tbody>
                
                </table>';
            }



            return response()->json(["estado" => "ok", "msg" => "Agregado Exitosamente", "tabla" => $html], 200);
        } catch (\Throwable $th) {
            return $th;
            return response()->json(["estado" => "error", "msg" => "No se pudo agregar", "tabla" => $html], 200);
        }
    }


    public function donacionesTramites()
    {
        $this->configuraciongeneral[4] = 'Trámites por donaciones';
        $this->configuraciongeneral[0] = 'Trámites por donaciones';
        $this->configuraciongeneral[6] = 'coordinacioncronograma/donacionesTramitesajax';
        return view('vistas.busqueda', [
            'configuraciongeneral' => $this->configuraciongeneral
        ]);
    }

    public function donacionesTramitesAjax(Request $request)
    {
        $tramites = TramAlcaldiaCabModel::with('area', 'tipoTramite')->where(['estado' => 'ACT','donaciones'=>1]);
        // dd($tramites->take(10)->get());
        return DataTables::of($tramites)->editColumn('tipo_tramite', function ($tramite) {
            return $tramite->tipo_tramite == null ? '' : $tramite->tipoTramite->valor;
        })->addColumn('atender', function ($tramite) {
            $atender = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
                ->select('dir.direccion')
                ->where(['asig.id_cab' => $tramite->id])
                ->where('asig.estado', '<>', 'CON COPIA')->get();

            return $atender->map(function ($asignado) {
                return $asignado->direccion;
            })->implode(' - ');
        })->addColumn('copia', function ($tramite) {
            $copia = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
                ->select('dir.direccion')
                ->where(['asig.id_cab' => $tramite->id, 'asig.estado' => 'CON COPIA'])->get();

            return $copia->map(function ($asignado) {
                return $asignado->direccion;
            })->implode(' - ');
        })->addColumn('origen', function ($tramite) {
            return $tramite->tipo == 'VENTANILLA' ? 'VENTANILLA' : ($tramite->tipo == 'TRAMITE MUNICIPAL' ? 'PORTAL CIUDADANO' : 'PORTAL CIUDADANO');
        })->addColumn('igresado_por', function ($tramite) {
            if ($tramite->creado_por != null) {
                $usuario = User::find($tramite->creado_por);
                return $usuario->name;
            } else return 'CIUDADANO';
        })->addColumn('action', function ($tramite) {
            return '<a href="' . route('tramites.show', ['id' => $tramite->id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
            <i class="fa fa-newspaper-o"></i></a>';
        })->filter(function ($query) use ($request) {
            if ($request->has('numtramite') && $request->numtramite != '') {
                $query->where('numtramite', 'LIKE', "%$request->numtramite%");
            }
            if ($request->has('cedula') && $request->cedula != '') {
                $query->where(['cedula_remitente' => $request->cedula]);
            }
            if ($request->has('disposicion') && $request->disposicion != '') {
                $query->where(['disposicion' => $request->disposicion]);
            }
        })->make(true);
    }
}
