<?php

namespace App\Http\Controllers;

use App\Http\Resources\AgendaVirtualResource;
use App\User;
use App\UsuariosModel;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
// use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Modules\Comunicacionalcaldia\Entities\actividadesModel;
use Modules\ComunicacionAlcaldia\Entities\AgendaNotificacionesPersonalizadas;
use Modules\ComunicacionAlcaldia\Entities\AgendaVirtualModel;
use Modules\ComunicacionAlcaldia\Entities\AgendaVirtualModelDeta;
use Modules\ComunicacionAlcaldia\Entities\AgendaVistaModel;
use Modules\ComunicacionAlcaldia\Entities\TipoAgendaModel;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabModel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Modules\ComunicacionAlcaldia\Entities\CategoriaAgendaModel;
use Modules\CoordinacionCronograma\Entities\AgendaArchivosModel;
use Modules\CoordinacionCronograma\Entities\TurEventosCategoriasModel;
use Modules\CoordinacionCronograma\Entities\TurEventosComentariosModel;
use Modules\CoordinacionCronograma\Entities\TurEventosModel;
use Modules\CoordinacionCronograma\Entities\TurEventosReaccionesModel;

//use App\Http\Controllers\ReportesJasperController;

class WebApiAgendaController extends Controller
{
    //
    //protected $ReportJasper;
    public function __construct()
    {
        $this->middleware('cors');
        //$this->ReportJasper = $ReportJasper;
    }

    const LUNES = 1;
    const MARTES = 2;
    const MIERCOLES = 3;
    const JUEVES = 4;
    const VIERNES = 5;
    const SABADO = 6;
    const DOMINGO = 7;
    const MIN_EVENTO = 1;
    const MAX_EVENTO = 23;
    const DURACION_ENTRE_EVENTOS = 15;
    const DURACION_MINIMA_EVENTOS = 15;










    function confirmar_asistencia(Request $request)
    {
        $token = $request->api_token;
        $user = User::where(['api_token' => $token])->first();
        if ($user) {
            try {
                DB::beginTransaction();
                $id = Input::get('id_agenda');
                $confirmar = Input::get('confirmar');
                $agenda = AgendaVirtualModel::find($id);
                $agenda->confirmar_asistencia = $confirmar;
                $agenda->save();



                DB::commit();

                if ($agenda->confirmar_asistencia == 1) {
                    $msg2 = "El Sr.Alcalde confirmó que asistirá a la actividad ";
                } else {
                    $msg2 = "El Sr.Alcalde no asistirá a la actividad ";
                }
                if ($agenda->coordinadores == 1 || $agenda->coordinadores == true) {
                    $this->notificarTodos('🔔  ' . $msg2, $agenda, 10);
                }

                if ($agenda->concejales == 1 || $agenda->concejales == true) {
                    $this->notificarTodos('🔔  ' . $msg2, $agenda, 25);
                }

                if ($agenda->directores == 1 || $agenda->directores == true) {
                    $this->notificarTodos('🔔  ' . $msg2, $agenda, 3);
                    $this->notificarTodos('🔔  ' . $msg2, $agenda, 5);
                    $this->notificarTodos('🔔  ' . $msg2, $agenda, 15);
                    $this->notificarTodos('🔔  ' . $msg2, $agenda, 22);
                    $this->notificarTodos('🔔  ' . $msg2, $agenda, 29);
                    $this->notificarTodos('🔔  ' . $msg2, $agenda, 9);
                }

                if ($agenda->misconcejales == 1 || $agenda->misconcejales == true) {

                    $this->notificarGrupito('🔔  ' . $msg2, $agenda);
                }
                $this->notificarDelgados('🔔  ' . $msg2, $agenda);
                $this->notificarPersonalizada('🔔  ' . $msg2, $agenda);


                return response()->json(["success", "Grabado Exitosamente"], 200);
            } catch (\Throwable $th) {
                return response()->json(["error", "Se ha producido un error"], 200);
            }
        }
    }

    function valida_registro_asistencia(Request $request)
    {
        $token = $request->api_token;
        $user = User::where(['api_token' => $token])->first();
        if ($user) {
            try {
                DB::beginTransaction();
                $id = Input::get('id_agenda');
                $asistio = Input::get('asistio');
                $agenda = AgendaVirtualModel::find($id);
                $agenda->asistio = $asistio;
                $agenda->save();
                DB::commit();
                return response()->json(["success", "Grabado Exitosamente"], 200);
            } catch (\Throwable $th) {
                return response()->json(["error", "Se ha producido un error"], 200);
            }
        }
    }

    function silenciar(Request $request)
    {
        $token = $request->api_token;
        $user = User::where(['api_token' => $token])->first();
        if ($user) {
            try {
                DB::beginTransaction();
                $id = Input::get('id_agenda');
                $silenciar = Input::get('silenciar');
                $agenda = AgendaVirtualModel::find($id);
                $agenda->silenciar = $silenciar;
                $agenda->save();
                DB::commit();
                return response()->json(["success", "Grabado Exitosamente"], 200);
            } catch (\Throwable $th) {
                return response()->json(["error", "Se ha producido un error"], 200);
            }
        }
    }

    function obtener_evento($id, $token)
    {
        $usuario = User::where(['api_token' => $token])->first();

        if ($usuario) {
            DB::beginTransaction();
            try {
                // return $evento = AgendaVirtualModel::find($id);
                // dd(AgendaVirtualModel::find($id));

                return AgendaVirtualResource::collection(AgendaVirtualModel::find($id));
                // if ($evento) return response()->json($evento);
                // else return response()->json(['error' => 'No existe el evento.'], 200);
                DB::commit();
            } catch (\Exception $ex) {
                return response()->json(['error' => $ex->getMessage()], 200);
                DB::rollback();
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }

    function obtener_categoria_agenda($token)
    {
        $usuario = User::where(['api_token' => $token])->first();
        if ($usuario) {
            DB::beginTransaction();
            try {

                // select `com_tma_tipo_agenda`.*, `ct`.`nombre` from `com_tma_tipo_agenda` 
                // inner join `com_tma_categoria_agenda` as `ct` on `ct`.`id` = `com_tma_tipo_agenda`.`id_categoria` 
                // where `com_tma_tipo_agenda`.`estado` = 'ACT'
                return TipoAgendaModel::join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'com_tma_tipo_agenda.id_categoria')
                    ->Select('com_tma_tipo_agenda.id', 'com_tma_tipo_agenda.tipo', 'ct.nombre as categoria', 'com_tma_tipo_agenda.id_categoria')
                    ->where('com_tma_tipo_agenda.estado', '=', 'ACT')
                    ->orderBy('com_tma_tipo_agenda.tipo', 'desc')
                    ->get();
                // if ($evento) return response()->json($evento);
                // else return response()->json(['error' => 'No existe el evento.'], 200);
                DB::commit();
            } catch (\Exception $ex) {
                return response()->json(['error' => $ex->getMessage()], 200);
                DB::rollback();
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }

    function obtener_eventos_pendientes($token)
    {
        $usuario = User::where(['api_token' => $token])->first();
        $id_estado = Input::get('estado');
        $id_estado = explode(',', $id_estado);
        if ($usuario) {
            DB::beginTransaction();
            try {

                if ($usuario->id_perfil == 12) {
                    return ['data' => AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                        ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                        ->select(
                            'com_tmov_comunicacion_agenda.id',
                            'com_tmov_comunicacion_agenda.nombre',
                            'com_tmov_comunicacion_agenda.descripcion',
                            'com_tmov_comunicacion_agenda.detalle_direccion',
                            'com_tmov_comunicacion_agenda.fecha_inicio',
                            'com_tmov_comunicacion_agenda.fecha_fin',
                            'com_tmov_comunicacion_agenda.confirmar_asistencia',
                            'e.nombre as estado_aprobacion',
                            't.tipo as tipo_evento'
                        )
                        ->whereIn('id_estado', [3, 4, 13, 8])
                        ->whereNotIn('confirmar_asistencia', [1])
                        ->orderBy('fecha_inicio', 'asc')->orderBy('fecha_fin', 'asc')->get()];
                    // ->orderBy('fecha_inicio', 'asc')->orderBy('fecha_fin', 'asc')->paginate(10);
                } else {
                    return  ['data' => AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                        ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                        ->select(
                            'com_tmov_comunicacion_agenda.id',
                            'com_tmov_comunicacion_agenda.nombre',
                            'com_tmov_comunicacion_agenda.descripcion',
                            'com_tmov_comunicacion_agenda.detalle_direccion',
                            'com_tmov_comunicacion_agenda.fecha_inicio',
                            'com_tmov_comunicacion_agenda.fecha_fin',
                            'com_tmov_comunicacion_agenda.confirmar_asistencia',
                            'e.nombre as estado_aprobacion',
                            't.tipo as tipo_evento'
                        )
                        ->whereIn('id_estado', [50])
                        ->whereNotIn('confirmar_asistencia', [1])
                        // ->orderBy('fecha_inicio', 'asc')->orderBy('fecha_fin', 'asc')->paginate(10);
                        ->orderBy('fecha_inicio', 'asc')->orderBy('fecha_fin', 'asc')->get()];
                }


                DB::commit();
            } catch (\Exception $ex) {
                return response()->json(['error' => $ex->getMessage()], 200);
                DB::rollback();
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }


    function listar_eventos_2($inicio, $fin, $token)
    {
        $user = User::where(['api_token' => $token])->first();


        // dd($user->id_perfil);
        // dd(date('Y-m', strtotime($fecha)));
        if ($user) {
            DB::beginTransaction();
            try {

                $multiplied_delegados = null;
                $eventos = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                    ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                    ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                    ->leftjoin('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                    ->leftjoin('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')

                    ->select(
                        'com_tmov_comunicacion_agenda.nombre as title',
                        'descripcion as desc',
                        'fecha_fin as endTime',
                        'fecha_inicio as startTime',
                        DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",adjunto) as adjunto_link'),
                        DB::raw("null as allDay"),
                        "com_tmov_comunicacion_agenda.*",
                        'tp.tipo as tipo_evento',
                        'ct.id as categoria_id',
                        'es.nombre as estado_aprobacion',
                        'pr.parroquia',
                        'b.barrio',
                        'b.id_parroquia'
                    )
                    ->where('com_tmov_comunicacion_agenda.estado','ACT')
                ;


                $eventos_delegados = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                    ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                    ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                    ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                    ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                    ->select(
                        'com_tmov_comunicacion_agenda.nombre as title',
                        'descripcion as desc',
                        'fecha_fin as endTime',
                        'fecha_inicio as startTime',
                        DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",adjunto) as adjunto_link'),
                        DB::raw("null as allDay"),
                        "com_tmov_comunicacion_agenda.*",
                        'tp.tipo as tipo_evento',
                        'ct.id as categoria_id',
                        'es.nombre as estado_aprobacion',
                        'pr.parroquia',
                        'b.barrio',
                        'b.id_parroquia'
                    )->where('com_tmov_comunicacion_agenda.estado','ACT')->whereIn('id_estado', [8])->orderBy('fecha_inicio', 'asc')->orderBy('fecha_fin', 'asc')->get();

                $actividades_collect = collect($eventos_delegados);
                // dd($actividades_collect);
                $multiplied_delegados = $actividades_collect->map(function ($item, $key) use ($user) {
                    if ($item->delegacion_alcalde != null) {
                        $responsables = DB::select('select GROUP_CONCAT(name SEPARATOR ", ") as delegacion_alcalde from users where id in (' . $item->delegacion_alcalde . ')');
                        $responsables_ = $responsables[0]->delegacion_alcalde;
                    } else {
                        $responsables_ = null;
                    }


                    $responsables_para_mostrar = [];
                    if ($item->delegacion_alcalde != null) {
                        $responsables_para_mostrar = DB::select('select id from users where id in (' . $item->delegacion_alcalde . ')');
                    }
                    $bandera = false;
                    foreach ($responsables_para_mostrar as $k) {
                        if ($user->id == $k->id) {
                            // dd($user->id);
                            $bandera = true;
                        }
                    }

                    // dd($bandera);
                    if ($bandera) {
                        // dd();
                        $user_ = UsuariosModel::where('id', $item->id_usuario)->first();
                        if ($user_) {
                            $user = $user_;
                        }
                        return collect(
                            [
                                'title' => $item->nombre,
                                'desc' => $item->desc,
                                'endTime' => $item->endTime,
                                'startTime' => $item->startTime,
                                // 'adjunto_link' => $item->adjunto_link,
                                'allDay' => $item->allDay,
                                // 'adjunto_link' => $item->adjunto_link,
                                'tipo_evento' => $item->tipo_evento,
                                'categoria_id' => $item->categoria_id,
                                'id' => $item->id,
                                // 'nombre' => $item->nombre,
                                // 'descripcion' => $item->descripcion,
                                // 'direccion' => $item->direccion,
                                // 'detalle_direccion' => $item->detalle_direccion,
                                // 'longitud' => $item->longitud,
                                // 'latitud' => $item->latitud,
                                'fecha_inicio' => $item->fecha_inicio,
                                'fecha_fin' => $item->fecha_fin,
                                'id_usuario' => $item->id_usuario,
                                // 'usuario_registra' => $user->name,
                                // 'perfil_registra' => $user->id_perfil,
                                'estado_aprobacion' => $item->estado_aprobacion,
                                // 'observacion' => $item->observacion,
                                'estado' => $item->estado,
                                // 'responsable' => $item->responsable,
                                'silenciar' => $item->silenciar,
                                // 'n_beneficiarios' => $item->n_beneficiarios,
                                // 'observacion_cancelacion' => $item->observacion_cancelacion,
                                // 'created_at' => $item->created_at,
                                // 'updated_at' => $item->updated_at,
                                'tipo_agenda' => $item->tipo_agenda,
                                'tipo' => $item->tipo,
                                'categoria' => $item->categoria,
                                // 'telefono_solicitante' => $item->telefono_solicitante,
                                // 'email_solicitante' => $item->email_solicitante,
                                'recordar' => $item->recordar,
                                // 'delegacion_alcalde' => $responsables_,
                                // 'disposicion_alcalde' => $item->disposicion_alcalde,
                                'id_estado' => $item->id_estado,
                                'id_tipo' => $item->id_tipo,
                                // 'adjunto' => $item->adjunto,
                                'tipo_evento' => $item->tipo_evento,
                                // 'telefono_solicitante' => $item->telefono_solicitante,
                                // 'email_solicitante' => $item->email_solicitante,
                                // 'parroquia' => $item->parroquia,
                                // 'id_parroquia' => $item->id_parroquia,
                                'confirmar' => (boolval($item->confirmar_asistencia) ? true : false),
                                'asistio' => (boolval($item->asistio) ? true : false),
                                // 'barrio'=>$item->barrio,
                                // 'barrio_id'=>$item->barrio_id
                            ]
                        );
                    }
                });
                // $eventos = [];
                // Cambia el perfil de secrataria a alcalde 
                $perfil_alcalde = explodewords(ConfigSystem("perfil_alcalde"), "|");
                $bandera_alcalde = false;
                foreach ($perfil_alcalde as $key => $value) {
                    # code...
                    if ($value == $user->cedula) {
                        $bandera_alcalde = true;
                    }
                }


                if ($user->id_perfil == 8 || $user->id_perfil == 16 || $user->id_perfil == 12  || $bandera_alcalde == true) {
                    // dd($user);
                    $eventos = $eventos->whereIn('id_estado', [3, 4, 8, 13, 5])->orderBy('fecha_inicio', 'asc')->orderBy('fecha_fin', 'asc')->get();
                    $actividades_collect = collect($eventos);
                    $multiplied = $actividades_collect->map(function ($item, $key) {
                        if ($item->delegacion_alcalde != null) {
                            $responsables = DB::select('select GROUP_CONCAT(name SEPARATOR ", ") as delegacion_alcalde from users where id in (' . $item->delegacion_alcalde . ')');
                            $responsables_ = $responsables[0]->delegacion_alcalde;
                        } else {
                            $responsables_ = null;
                        }

                        $user = UsuariosModel::join('ad_perfil as p', 'p.id', 'users.id_perfil')
                            ->where('users.id', $item->id_usuario)->first();

                        return collect(
                            [
                                'title' => $item->nombre,
                                'desc' => $item->desc,
                                'endTime' => $item->endTime,
                                'startTime' => $item->startTime,
                                // 'adjunto_link' => $item->adjunto_link,
                                'allDay' => $item->allDay,
                                // 'adjunto_link' => $item->adjunto_link,
                                'tipo_evento' => $item->tipo_evento,
                                'categoria_id' => $item->categoria_id,
                                'id' => $item->id,
                                // 'nombre' => $item->nombre,
                                // 'descripcion' => $item->descripcion,
                                // 'direccion' => $item->direccion,
                                // 'detalle_direccion' => $item->detalle_direccion,
                                // 'longitud' => $item->longitud,
                                // 'latitud' => $item->latitud,
                                'fecha_inicio' => $item->fecha_inicio,
                                'fecha_fin' => $item->fecha_fin,
                                'id_usuario' => $item->id_usuario,
                                // 'usuario_registra' => $user->name,
                                // 'perfil_registra' => $user->id_perfil,
                                'estado_aprobacion' => $item->estado_aprobacion,
                                // 'observacion' => $item->observacion,
                                'estado' => $item->estado,
                                // 'responsable' => $item->responsable,
                                'silenciar' => $item->silenciar,
                                // 'n_beneficiarios' => $item->n_beneficiarios,
                                // 'observacion_cancelacion' => $item->observacion_cancelacion,
                                // 'created_at' => $item->created_at,
                                // 'updated_at' => $item->updated_at,
                                'tipo_agenda' => $item->tipo_agenda,
                                'tipo' => $item->tipo,
                                'categoria' => $item->categoria,
                                // 'telefono_solicitante' => $item->telefono_solicitante,
                                // 'email_solicitante' => $item->email_solicitante,
                                'recordar' => $item->recordar,
                                // 'delegacion_alcalde' => $responsables_,
                                // 'disposicion_alcalde' => $item->disposicion_alcalde,
                                'id_estado' => $item->id_estado,
                                'id_tipo' => $item->id_tipo,
                                // 'adjunto' => $item->adjunto,
                                'tipo_evento' => $item->tipo_evento,
                                // 'telefono_solicitante' => $item->telefono_solicitante,
                                // 'email_solicitante' => $item->email_solicitante,
                                // 'parroquia' => $item->parroquia,
                                // 'id_parroquia' => $item->id_parroquia,
                                'confirmar' => (boolval($item->confirmar_asistencia) ? true : false),
                                'asistio' => (boolval($item->asistio) ? true : false),
                                // 'barrio'=>$item->barrio,
                                // 'barrio_id'=>$item->barrio_id
                                'metodo_regsitro' => $item->metodo_regsitro
                            ]
                        );
                    });
                    $eventos = $multiplied;
                    // dd($eventos);
                } else {


                    if ($user->id_perfil == 10) {
                        $eventos = $eventos->where('id_estado', '<>', 6)->where('coordinadores', 1)->where('tipo_agenda', 0)->orderBy('fecha_inicio', 'asc')->orderBy('fecha_fin', 'asc')->get();
                    } elseif ($user->id_perfil == 25) {
                        // $misconcejales=[];
                        $misconcejales = explode("|", ConfigSystem("misconcejales"));
                        if (is_array($misconcejales) && in_array($user->id, $misconcejales)) {
                            $eventos = $eventos->where('id_estado', '<>', 6)->where('tipo_agenda', 0)
                                ->where(function ($query) {
                                    $query
                                        ->orwhere('concejales', 1)
                                        ->orwhere('misconcejales', 1);
                                })
                                ->orderBy('fecha_inicio', 'asc')
                                ->orderBy('fecha_fin', 'asc')
                                ->get();
                            // Log::info()
                        } else {
                            $eventos = $eventos->where('id_estado', '<>', 6)->where('concejales', 1)->where('tipo_agenda', 0)->orderBy('fecha_inicio', 'asc')->orderBy('fecha_fin', 'asc')->get();
                        }
                    } elseif ($user->id_perfil == 3 || $user->id_perfil == 15 || $user->id_perfil == 22 || $user->id_perfil == 9) {
                        $eventos = $eventos->where('id_estado', '<>', 6)->where('directores', 1)->where('tipo_agenda', 0)->orderBy('fecha_inicio', 'asc')->orderBy('fecha_fin', 'asc')->get();
                        // dd($eventos);
                    }
                    if (isset($eventos[0]->id)) {
                        $actividades_collect = collect($eventos);
                        // dd($eventos);
                        // return response()->json($eventos); 
                        $multiplied = $actividades_collect->map(function ($item, $key) {
                            if (isset($item->delegacion_alcalde) && $item->delegacion_alcalde != null) {
                                $responsables = DB::select('select GROUP_CONCAT(name SEPARATOR ", ") as delegacion_alcalde from users where id in (' . $item->delegacion_alcalde . ')');
                                $responsables_ = $responsables[0]->delegacion_alcalde;
                            } else {
                                $responsables_ = null;
                            }

                            // $user = UsuariosModel::join('ad_perfil as p', 'p.id', 'users.id_perfil')
                            //     ->where('users.id', $item->id_usuario)->first();

                            return collect(
                                [
                                    'title' => $item->nombre,
                                    'desc' => $item->desc,
                                    'endTime' => $item->endTime,
                                    'startTime' => $item->startTime,
                                    // 'adjunto_link' => $item->adjunto_link,
                                    'allDay' => $item->allDay,
                                    // 'adjunto_link' => $item->adjunto_link,
                                    'tipo_evento' => $item->tipo_evento,
                                    'categoria_id' => $item->categoria_id,
                                    'id' => $item->id,
                                    // 'nombre' => $item->nombre,
                                    // 'descripcion' => $item->descripcion,
                                    // 'direccion' => $item->direccion,
                                    // 'detalle_direccion' => $item->detalle_direccion,
                                    // 'longitud' => $item->longitud,
                                    // 'latitud' => $item->latitud,
                                    'fecha_inicio' => $item->fecha_inicio,
                                    'fecha_fin' => $item->fecha_fin,
                                    'id_usuario' => $item->id_usuario,
                                    // 'usuario_registra' => $user->name,
                                    // 'perfil_registra' => $user->id_perfil,
                                    'estado_aprobacion' => $item->estado_aprobacion,
                                    // 'observacion' => $item->observacion,
                                    'estado' => $item->estado,
                                    // 'responsable' => $item->responsable,
                                    'silenciar' => $item->silenciar,
                                    // 'n_beneficiarios' => $item->n_beneficiarios,
                                    // 'observacion_cancelacion' => $item->observacion_cancelacion,
                                    // 'created_at' => $item->created_at,
                                    // 'updated_at' => $item->updated_at,
                                    'tipo_agenda' => $item->tipo_agenda,
                                    'tipo' => $item->tipo,
                                    'categoria' => $item->categoria,
                                    // 'telefono_solicitante' => $item->telefono_solicitante,
                                    // 'email_solicitante' => $item->email_solicitante,
                                    'recordar' => $item->recordar,
                                    // 'delegacion_alcalde' => $responsables_,
                                    // 'disposicion_alcalde' => $item->disposicion_alcalde,
                                    'id_estado' => $item->id_estado,
                                    'id_tipo' => $item->id_tipo,
                                    // 'adjunto' => $item->adjunto,
                                    'tipo_evento' => $item->tipo_evento,
                                    // 'telefono_solicitante' => $item->telefono_solicitante,
                                    // 'email_solicitante' => $item->email_solicitante,
                                    // 'parroquia' => $item->parroquia,
                                    // 'id_parroquia' => $item->id_parroquia,
                                    'confirmar' => (boolval($item->confirmar_asistencia) ? true : false),
                                    'asistio' => (boolval($item->asistio) ? true : false),
                                    // 'barrio'=>$item->barrio,
                                    // 'barrio_id'=>$item->barrio_id
                                    'metodo_regsitro' => $item->metodo_regsitro
                                ]
                            );
                        });
                        $eventos = $multiplied;
                    } else {

                        $eventos = collect();
                    }
                }

                $eventos_ = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                    ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                    ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                    ->join('com_tmo_notifiacion_personalizada as p', 'p.id_agenda', '=', 'com_tmov_comunicacion_agenda.id')
                    ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                    ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                    ->select(
                        'com_tmov_comunicacion_agenda.nombre as title',
                        'descripcion as desc',
                        'fecha_fin as endTime',
                        'fecha_inicio as startTime',
                        DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",adjunto) as adjunto_link'),
                        DB::raw("null as allDay"),
                        "com_tmov_comunicacion_agenda.*",
                        'tp.tipo as tipo_evento',
                        'ct.id as categoria_id',
                        'es.nombre as estado_aprobacion',
                        'pr.parroquia',
                        'b.barrio',
                        'b.id_parroquia'
                    )->where('p.id_usuario', $user->id)
                    ->where('id_estado', '<>', 6)->get();

                $actividades_collect = collect($eventos_);

                $multiplied = $actividades_collect->map(function ($item, $key) {
                    if ($item->delegacion_alcalde != null) {
                        $responsables = DB::select('select GROUP_CONCAT(name SEPARATOR ", ") as delegacion_alcalde from users where id in (' . $item->delegacion_alcalde . ')');
                        $responsables_ = $responsables[0]->delegacion_alcalde;
                    } else {
                        $responsables_ = null;
                    }

                    $user = UsuariosModel::join('ad_perfil as p', 'p.id', 'users.id_perfil')
                        ->where('users.id', $item->id_usuario)->first();

                    return collect(
                        [
                            'title' => $item->nombre,
                            'desc' => $item->desc,
                            'endTime' => $item->endTime,
                            'startTime' => $item->startTime,
                            // 'adjunto_link' => $item->adjunto_link,
                            'allDay' => $item->allDay,
                            // 'adjunto_link' => $item->adjunto_link,
                            'tipo_evento' => $item->tipo_evento,
                            'categoria_id' => $item->categoria_id,
                            'id' => $item->id,
                            // 'nombre' => $item->nombre,
                            // 'descripcion' => $item->descripcion,
                            // 'direccion' => $item->direccion,
                            // 'detalle_direccion' => $item->detalle_direccion,
                            // 'longitud' => $item->longitud,
                            // 'latitud' => $item->latitud,
                            'fecha_inicio' => $item->fecha_inicio,
                            'fecha_fin' => $item->fecha_fin,
                            'id_usuario' => $item->id_usuario,
                            // 'usuario_registra' => $user->name,
                            // 'perfil_registra' => $user->id_perfil,
                            'estado_aprobacion' => $item->estado_aprobacion,
                            // 'observacion' => $item->observacion,
                            'estado' => $item->estado,
                            // 'responsable' => $item->responsable,
                            'silenciar' => $item->silenciar,
                            // 'n_beneficiarios' => $item->n_beneficiarios,
                            // 'observacion_cancelacion' => $item->observacion_cancelacion,
                            // 'created_at' => $item->created_at,
                            // 'updated_at' => $item->updated_at,
                            'tipo_agenda' => $item->tipo_agenda,
                            'tipo' => $item->tipo,
                            'categoria' => $item->categoria,
                            // 'telefono_solicitante' => $item->telefono_solicitante,
                            // 'email_solicitante' => $item->email_solicitante,
                            'recordar' => $item->recordar,
                            // 'delegacion_alcalde' => $responsables_,
                            // 'disposicion_alcalde' => $item->disposicion_alcalde,
                            'id_estado' => $item->id_estado,
                            'id_tipo' => $item->id_tipo,
                            // 'adjunto' => $item->adjunto,
                            'tipo_evento' => $item->tipo_evento,
                            // 'telefono_solicitante' => $item->telefono_solicitante,
                            // 'email_solicitante' => $item->email_solicitante,
                            // 'parroquia' => $item->parroquia,
                            // 'id_parroquia' => $item->id_parroquia,
                            'confirmar' => (boolval($item->confirmar_asistencia) ? true : false),
                            'asistio' => (boolval($item->asistio) ? true : false),
                            // 'barrio'=>$item->barrio,
                            // 'barrio_id'=>$item->barrio_id
                            'metodo_regsitro' => $item->metodo_regsitro
                        ]
                    );
                });
                $eventos_notificacion = $multiplied;


                if ($user->id_perfil != 8 && $user->id_perfil != 16 && $user->id_perfil != 12  && $bandera_alcalde != true) {
                    if (isset($multiplied_delegados[0]->id)) {
                        $multiplied_delegados = $multiplied_delegados->filter(function ($user, $key) {
                            return $user != null;
                        });
                        $eventos = $eventos->merge($multiplied_delegados)->all();
                    }
                    if (isset($eventos_notificacion[0]->id)) {
                        $eventos = collect($eventos);

                        $eventos = $eventos->merge($eventos_notificacion)->all();
                    }

                    $eve = collect($eventos)->unique();
                } else {
                    $eve = collect($eventos)->unique();
                }

                DB::commit();
                return response()->json($eve);
            } catch (\Exception $ex) {
                DB::rollback();
                return response()->json(['error' => $ex->getMessage() . ' ' . $ex->getLine()], 200);
            }
        } else return response()->json(['error' => 'Unauthorized'], 200);
    }



    function ingresar_calendar(Request $request)
    {
        if (isset($request->id_usuario_tramite)) {
            $user = User::where(['id' => $request->id_usuario_tramite])->first();
        } else {
            $token = $request->api_token;
            $user = User::where(['api_token' => $token])->first();
        }

        if ($user) {
            try {
                DB::beginTransaction();
                $input = Input::all();
                $notificacion = new NotificacionesController;

                $mensaje = '';
                $id = Input::get('id');
                $id_usuario_sesion = $user->id;
                $validator = Validator::make($input, AgendaVirtualModel::rules(0));
                // $verificacion = $this->verficar_existente(Input::get('fecha_inicio'), Input::get('fecha_fin'), $id, 3, 0, $user->id);

                // if ($verificacion['estado'] == 'false' && Input::get('accion') == 0) {
                //     DB::rollback();
                //     return response()->json(['error' => 'Existe una o varias actividades en este rango de fecha y hora. ', 'html' => $verificacion['actividades']], 200);
                // }

                $fecha = date('w', strtotime(Input::get('fecha_inicio')));
                $fin_fecha = date('w', strtotime(Input::get('fecha_fin')));

                if ($fecha != $fin_fecha) {
                    return response()->json(['error' => "La duración de las actividades no pueden durar mas de un día."], 200);
                }

                if (Input::hasFile("adjunto")) {
                    $docs = Input::file("adjunto");
                    if ($docs) {
                        $ext = $docs->getClientOriginalExtension();
                        $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                        if (!in_array($ext, $perfiles)) {
                            DB::rollback();
                            return response()->json(['error' => "No se admite el tipo de archivo " . strtoupper($ext)], 200);
                        }
                    }
                }

                $carbon1 = new \Carbon\Carbon(Input::get('fecha_inicio'));
                $carbon2 = new \Carbon\Carbon(Input::get('fecha_fin'));
                $minutesDiff = $carbon1->diffInMinutes($carbon2, true);
                // dd($minutesDiff);
                // dd($value);

                if ($minutesDiff <= 0) {
                    return response()->json(['error' => 'La hora de inicio debe ser mayor a la hora fin'], 200);
                }


                if (Input::get('id') == null || Input::get('id') == '') {
                    $guardar = new AgendaVirtualModel();
                    $msg2 = "Actividad registrada";
                    $guardar->id_usuario = $id_usuario_sesion;
                    $guardar->metodo_regsitro = 'APP';
                } else {

                    $guardar = AgendaVirtualModel::find(Input::get('id'));
                    $msg2 = "Actividad modificada";
                    // $guardar->metodo_regsitro = 'APP';

                }

                if ($validator->fails()) {
                    DB::rollback();
                    return response()->json(['error' => $validator->errors()], 200);
                } else {
                    foreach ($input as $key => $value) {
                        if (
                            $key != "_method" && $key != "_token" && $key != 'api_token' && $key != "title" && $key != "desc"
                            && $key != "endTime" && $key != "startTime" && $key != "allDay" && $key != "adjunto"
                            && $key != "fecha_evento" && $key != "hora_fin" && $key != "hora_fin_2"
                            && $key != "hora_inicio" && $key != "hora_inicio_2" && $key != "usuario"
                            && $key != "adjunto_link" && $key != "tipo_evento"  && $key != 'id'
                            && $key != 'en_espera' && $key != 'created_at' && $key != 'updated_at'
                            && $key != 'categoria_id'
                            && $key != 'usuario_registra'
                            && $key != 'perfil_registra'
                            && $key != 'id_usuario_tramite'
                            && $key != 'id_usuario_masivo'
                            && $key != 'obs_delegacion'
                            && $key != 'accion'
                            && $key != 'delegacion_alcalde_id'
                            && $key != 'delegacion_alcalde'
                            && $key != 'notificacion_personlizada'
                            && $key != 'notificacion_personlizada_usuarios'
                            && $key != 'id_parroquia'
                            && $key != 'barrio'
                            && $key != 'id_usuario_notificacion'
                            && $key != 'canton'
                        ) {

                            if ($id != null &&  $key == "id_usuario") {
                            }
                            if (($key == "coordinadores" && $value == true) || ($key == "concejales"  && $value == true)  || ($key == "directores" && $value ==  true) || ($key == "misconcejales" && $value ==  true)) {
                                $guardar->$key = true;
                            } elseif ($key == "parroquia") {
                                $guardar->barrio_id = $value;
                            } elseif ($key == "confirmar") {
                                $guardar->confirmar_asistencia = $value;
                            } else {
                                $guardar->$key = $value;
                            }
                        }
                    }
                    $guardar->save();
                    $accion = Input::get('accion');
                    $multiple = Input::get("id_usuario_masivo");
                    $obs = Input::get("obs_delegacion");
                    if ($accion == 1  || $accion == 2) {

                        $guardar->id_estado = 8;
                        $guardar->delegacion_alcalde = (is_array($multiple)) ? implode(',', $multiple) : $multiple;
                        $guardar->disposicion_alcalde = $obs;
                        $guardar->recordar = "NO";
                    } else {
                    }

                    if ($guardar->barrio_id == 276) {
                        $guardar->canton_id = 8;
                    }
                    $guardar->save();

                    $idcab = $guardar->id;

                    insertarhistorial($guardar, $idcab, 1, $id_usuario_sesion);

                    if (isset($input['adjunto'])) {

                        $dir = public_path() . '/archivos_sistema/';
                        $archivos = $input['adjunto'];
                        foreach ($archivos as $key => $value) {

                            $docs = trim($value);

                            $data = null;
                            $ext = '.png';
                            if (strpos($docs, "data:image/*;charset=utf-8;base64,") !== false) {
                                $data = base64_decode(str_replace('data:image/*;charset=utf-8;base64,', '', $docs));
                            } elseif (strpos($docs, "data:image/jpeg;base64,") !== false) {
                                $data = base64_decode(str_replace('data:image/jpeg;base64,', '', $docs));
                            } elseif (strpos($docs, "data:application/*;charset=utf-8;base64,") !== false) {
                                $data = base64_decode(str_replace('data:application/*;charset=utf-8;base64,', '', $docs));
                                $ext = '.pdf';
                            } elseif (strpos($docs, "data:application/pdf;base64,") !== false) {
                                $data = base64_decode(str_replace('data:application/pdf;base64,', '', $docs));
                                $ext = '.pdf';
                            }


                            if ($data) {
                                $fileName = "agenda-$idcab-$key" . date("YmdHis") . $ext;
                                file_put_contents($dir . '' . $fileName, $data);
                                $file = new AgendaArchivosModel();
                                $file->ruta = $fileName;
                                $file->id_agenda = $idcab;
                                $file->save();
                            }
                        }
                    }


                    if (Input::has("id_usuario_masivo") || Input::has("id_usuario_notificacion")) {

                        $multiple = Input::get("id_usuario_masivo");
                        if (is_array($multiple) && isset($multiple[0])) {

                            $players = array();
                            $dele_ = '';
                            if (is_array($multiple)) {
                                foreach ($multiple as $key => $value) {
                                    $usuario = UsuariosModel::where('id', $value)->first();
                                    if ($usuario) {
                                        // dd("vale verga");
                                        $verificacion_delegado = $this->verificarDisponibilidad2($usuario->id,  $guardar->fecha_inicio,  $guardar->fecha_fin, $guardar->id);

                                        Log::info($verificacion_delegado);
                                        if ($verificacion_delegado['respuesta'] == true) {
                                            DB::rollback();
                                            return response()->json(['error' => $verificacion_delegado['mensaje_app']], 200);
                                        }
                                        $telefonos_personas = DB::table('tma_playerid')
                                            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                                            ->where('u.id', $usuario->id)->get();
                                        foreach ($telefonos_personas as $key => $v) {
                                            array_push($players, $v->id_player);
                                        }
                                        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $guardar->id, '🔔  Ha sido delegado a una actividad de la agenda virtual',  $guardar->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($guardar->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($guardar->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($guardar->fecha_fin)), 2, $guardar);
                                        $notificacion->EnviarEmailAgenda($usuario->email, ' 🔔  Ha sido delegado a una actividad de la agenda virtual', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $guardar->nombre . ' <br> <b>Fecha:</b> ' . date('d/m/Y', strtotime($guardar->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($guardar->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($guardar->fecha_fin)) . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $guardar->descripcion . '<b> Observación: </b>' . ((isset($guardar->observacion)) ? '' . $guardar->observacion : 'Ninguna') .  '</div>', "coordinacioncronograma/agendavirtual");
                                        $notificacion->notificacion($players, $guardar->id, '🔔  Ha sido delegado a una actividad de la agenda virtual',  $guardar->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($guardar->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($guardar->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($guardar->fecha_fin)), 2, $this->getActividad($guardar->id));
                                    }
                                    $dele_ .= $usuario->name . '<br>';
                                }
                            }
                        } else {
                            $guardar->id_estado = 3;
                            $guardar->delegacion_alcalde = null;
                            $guardar->disposicion_alcalde = '';
                            $guardar->recordar = "NO";
                        }

                        $multiple = Input::get("id_usuario_notificacion");
                        if (is_array($multiple) && isset($multiple[0])) {

                            AgendaNotificacionesPersonalizadas::where("id_agenda", $idcab)
                                ->delete();
                            if (is_array($multiple)) {
                                // Log::info(json_encode($multiple));
                                $players = [];

                                foreach ($multiple as $key => $value) {
                                    $guardarres = AgendaNotificacionesPersonalizadas::where("id_usuario", $value)
                                        ->where("id_agenda", $idcab)
                                        ->first();
                                    if (!$guardarres)
                                        $guardarres = new AgendaNotificacionesPersonalizadas();
                                    $guardarres->id_usuario = $value;
                                    $guardarres->id_agenda = $idcab;
                                    $guardarres->save();
                                    $verificacion_delegado = $this->verificarDisponibilidad2($value,  $guardar->fecha_inicio,  $guardar->fecha_fin, $guardar->id);

                                    Log::info($verificacion_delegado);
                                    if ($verificacion_delegado['respuesta'] == true) {
                                        DB::rollback();
                                        return response()->json(['error' => $verificacion_delegado['mensaje_app']], 200);
                                    }

                                    $usuario = DB::table('tma_playerid')->where('users_id', $guardarres->id_usuario)->get();
                                    // dd($usuario);
                                    foreach ($usuario as $u => $u_) {
                                        array_push($players, $u_->id_player);
                                    }
                                }
                                $web_api = new WebApiAgendaController;
                                $notificacion->notificacion($players, $guardar->id, '🔔  ' . $msg2, $guardar->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($guardar->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($guardar->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($guardar->fecha_fin)), 2, $web_api->getActividad($guardar->id));
                            }
                        }
                    } else {

                        AgendaNotificacionesPersonalizadas::where("id_agenda", $idcab)
                            ->delete();
                    }
                }
                DB::commit();
                $this->notificarAlcalde('🔔  ' . $msg2, $guardar);

                if ($guardar->coordinadores == 1 || $guardar->coordinadores == true) {
                    //    dd(10);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 10);
                }
                if ($guardar->concejales == 1 || $guardar->concejales == true) {

                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 25);
                }
                if ($guardar->directores == 1 || $guardar->directores == true) {
                    // dd($guardar);

                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 3);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 5);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 15);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 22);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 29);
                    $this->notificarTodos('🔔  ' . $msg2, $guardar, 9);
                }






                return response()->json(["success", "Grabado Exitosamente", $guardar->id], 200);
            } //Try Transaction
            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                // $mensaje = $e->getMessage();
                // return $e;
                return response()->json(['error' => $mensaje . $e], 200);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }

    public function asistir_acompaniar()
    {
        $token = Input::get('api_token');
        $user = User::where(['api_token' => $token])->first();
        $id_agenda = Input::get('id_agenda');
        $tipo = Input::get('tipo');
        $msg2 = "";
        if ($user) {




            $agenda = AgendaVirtualModel::find($id_agenda);
            if ($tipo == 1) {
                // $verificacion = $this->verficar_existente($agenda->fecha_inicio, $agenda->fecha_fin, $id_agenda, 3, 0, $user->id);

                // if ($verificacion['estado'] == 'false') {
                //     return response()->json(['error' => 'Existe una o varias actividades en este rango de fecha y hora. ', 'html' => $verificacion['actividades']], 200);
                // }
                $msg2 = "Queda sin efecto la delegación del Sr.Alcalde porque el asistirá";
            } else {
                $msg2 = "El Sr.Alcalde se integrará a la actividad ";
            }


            if ($agenda->coordinadores == 1 || $agenda->coordinadores == true) {
                //    dd(10);
                $this->notificarTodos('🔔  ' . $msg2, $agenda, 10);
            }
            if ($agenda->concejales == 1 || $agenda->concejales == true) {

                $this->notificarTodos('🔔  ' . $msg2, $agenda, 25);
            }

            if ($agenda->directores == 1 || $agenda->directores == true) {
                // dd($agenda);

                $this->notificarTodos('🔔  ' . $msg2, $agenda, 3);
                $this->notificarTodos('🔔  ' . $msg2, $agenda, 5);
                $this->notificarTodos('🔔  ' . $msg2, $agenda, 15);
                $this->notificarTodos('🔔  ' . $msg2, $agenda, 22);
                $this->notificarTodos('🔔  ' . $msg2, $agenda, 29);
                $this->notificarTodos('🔔  ' . $msg2, $agenda, 9);
            }

            $this->notificarDelgados('🔔  ' . $msg2, $agenda);


            if ($tipo == 1) {
                $agenda->id_estado = 3;
                // $agenda->id_estado=3;
                $agenda->delegacion_alcalde = null;
                $agenda->disposicion_alcalde = '';
            } else {
                $agenda->id_estado = 13;
            }
            $agenda->save();








            return response()->json(["success", "Grabado Exitosamente", $agenda->id], 200);
        } else {

            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }

    function consultar_eventos($fecha)
    {
        $token = Input::get('api_token');
        $user = User::where(['api_token' => $token])->first();
        if ($user) {
            // return ModelVehicleResource::collection(ModelVehicle::all());
            return AgendaVirtualResource::collection(
                AgendaVirtualModel::whereBetween('fecha_inicio', [$fecha . ' 00:00:00', $fecha . ' 23:59:59'])->get()
            );

            // $actividades = AgendaVirtualModel::join('users as u', 'u.id', '=', 'com_tmov_comunicacion_agenda.id_usuario')
            //     ->select("com_tmov_comunicacion_agenda.*", 'u.name as responsable')
            //     ->whereBetween('fecha_inicio', [$fecha . ' 00:00:00', $fecha . ' 23:59:59'])->get();

            // return response()->json($actividades);
        } else return response()->json(['error' => 'Unauthorized'], 200);
    }


    function atender_actividad()
    {
        $token = Input::get('api_token');
        $user = User::join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
            ->select('users.*', 'p.tipo')
            ->where(['api_token' => $token])->first();

        $cedula_alcalde =  ConfigSystem('cedula_alcalde');
        if ($user) {
            $id_usuario_sesion = $user->id;
            DB::beginTransaction();
            try {

                $id = Input::get('id');
                $id_usuario = Input::get('id_usuario');
                $observacion = Input::get('obs');
                $observacion_cancelacion = Input::get('disposicion_alcalde');
                $delegacion_alcalde = Input::get('delegacion_alcalde');
                $tipo = Input::get('tipo');
                $inicio = Input::get('fecha_inicio');
                $fin = Input::get('fecha_fin');

                //$responsable = Input::get('responsable');
                $actividad = AgendaVirtualModel::find($id);
                $notificacion = new NotificacionesController;

                if ($tipo == 1) {

                    $verificacion = $this->verficar_existente($actividad->fecha_inicio, $actividad->fecha_fin, $id, 2, $user->id);
                    if ($verificacion['estado'] == 'true') {
                        // $actividad->estado_aprobacion = 'PRE APROBADA';
                        $actividad->id_estado = 2;
                        $actividad->observacion = $observacion;
                        $actividad->save();
                        insertarhistorial($actividad, $actividad->id, 1, $id_usuario_sesion);

                        DB::commit();

                        $cedula_alcalde =  ConfigSystem('cedula_alcalde');
                        $telefonos_alacalde = DB::table('tma_playerid')
                            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                            ->where('cedula', $cedula_alcalde)->get();

                        $usuario = User::where('id', $cedula_alcalde)->first();
                        if ($usuario) {
                            $notificacion->EnviarEmailAgenda($usuario->email, 'Actividad Pre-Aprobada', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha:</b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin))  . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '<br><b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') . '</div>', "coordinacioncronograma/agendavirtual");
                            //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', 'Actividad Pre-Aprobada', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha:</b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin)) . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '<br><b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') . '</div>', "coordinacioncronograma/agendavirtual");
                        }

                        $players = array();
                        foreach ($telefonos_alacalde as $key => $value) {
                            array_push($players, $value->id_player);
                        }

                        $notificacion->notificacion($players, $actividad->id, '🔔  Actividad Pre-Aprobada',  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));
                        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, '🔔  Actividad Pre-Aprobada',  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));
                    } else {

                        return response()->json(['error' =>  $verificacion['msg']], 200);
                    }
                } elseif ($tipo == 3) {

                    // $verificacion = $this->verficar_existente($actividad->fecha_inicio, $actividad->fecha_fin, $id, 2, 0, $user->id);
                    // if ($verificacion['estado'] == 'false') {
                    // }

                    if ($observacion === '' || $observacion == null) {
                        return response()->json(['error' => 'Campo requerido'], 200);
                    }

                    $actividad->id_estado = 6;
                    $actividad->observacion = $observacion;
                    $actividad->save();
                    insertarhistorial($actividad, $actividad->id, 1, $id_usuario_sesion);
                    DB::commit();

                    $telefonos_alacalde = DB::table('tma_playerid')
                        ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                        ->where('cedula', $cedula_alcalde)->get();


                    $players = array();
                    foreach ($telefonos_alacalde as $key => $value) {
                        array_push($players, $value->id_player);
                    }
                    $notificacion->notificacion($players, $actividad->id, '🔔 Actividad supendida ❌',  $actividad->nombre, 2, $this->getActividad($actividad->id));
                    //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, '🔔 Actividad supendida ❌',  $actividad->nombre, 2, $this->getActividad($actividad->id));

                    $usuario = User::where('cedula', $cedula_alcalde)->first();
                    if ($usuario) {
                        $notificacion->EnviarEmailAgenda($usuario->email, 'Actividad supendida', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '</div>', '<div style="padding: 1% 0 0 6%"><b> Motivo: </b> ' . $actividad->observacion_cancelacion . '</div>', "coordinacioncronograma/agendavirtual");
                        //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', 'Actividad supendida', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '</div>', '<div style="padding: 1% 0 0 6%"><b> Motivo: </b> ' . $actividad->observacion_cancelacion . '</div>',  "coordinacioncronograma/agendavirtual");
                    }




                    $msg2 = 'Actividad suspendida';
                    if ($actividad->coordinadores == 1 || $actividad->coordinadores == true) {
                        //    dd(10);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 10);
                    }
                    if ($actividad->concejales == 1 || $actividad->concejales == true) {

                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 25);
                    }
                    if ($actividad->misconcejales == 1 || $actividad->misconcejales == true) {

                        $this->notificarGrupito('🔔  ' . $msg2, $actividad);
                    }
                    if ($actividad->directores == 1 || $actividad->directores == true) {
                        // dd($actividad);

                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 3);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 5);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 15);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 22);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 29);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 9);
                    }

                    $this->notificarPersonalizada('🔔  ' . $msg2, $actividad);
                    $this->notificarDelgados('🔔  ' . $msg2, $actividad);
                } elseif ($tipo == 2) {

                    if ($observacion == '' || $observacion == null) {
                        return response()->json(['error' => 'Campo requerido'], 200);
                    }
                    $actividad->id_estado = 7;
                    $actividad->observacion = $observacion;
                    $actividad->save();
                    insertarhistorial($actividad, $actividad->id, 1, $id_usuario_sesion);
                    DB::commit();


                    $notificacion->notificacion($players, $actividad->id, '🔔 Actividad rechazada ❌',  $actividad->nombre, 2, $this->getActividad($actividad->id));
                    $notificacion->notificacion([['58daed51-3f54-47a0-8df6-d325eb5da7c6']], $actividad->id, '🔔 Actividad rechazada ❌',  $actividad->nombre, 2, $this->getActividad($actividad->id));
                } elseif ($tipo == 4) {

                    if ($observacion == '' || $observacion == null) {
                        return response()->json(['error' => 'Campo requerido'], 200);
                    }

                    $carbon1 = new \Carbon\Carbon($inicio);
                    $carbon2 = new \Carbon\Carbon($fin);
                    $minutesDiff = $carbon1->diffInMinutes($carbon2, true);
                    // dd($minutesDiff);
                    // dd($value);

                    if ($minutesDiff <= 0) {
                        return response()->json(['error' => 'La hora de inicio debe ser mayor a la hora fin'], 200);
                    }
                    // $verificacion = $this->verficar_existente($inicio, $fin, $id, 3, 0, $user->id);
                    // if ($verificacion['estado'] == 'false') {
                    //     // return response()->json(['error' => 'Existe una actividad en este rango de fechas'], 200);
                    //     return response()->json(['error' => 'Existe una o varias actividades en este rango de fecha y hora. ', 'html' => $verificacion['actividades']], 200);
                    // }

                    // $fecha = date('w', strtotime($inicio));
                    // $categoria = $actividad->id_tipo;
                    // if ($fecha == 4  && $categoria != 7) {
                    //     return response()->json(['error' => 'Los días jueves no se permite agendar'], 200);
                    // }
                    if ($actividad->id_estado == 8) {

                        $actividad->id_estado = 8;
                    } else {
                        $actividad->id_estado = 4;
                    }

                    $actividad->observacion = $observacion;
                    $actividad->fecha_inicio = $inicio;
                    $actividad->fecha_fin = $fin;
                    $actividad->save();
                    insertarhistorial($actividad, $actividad->id, 1, $id_usuario_sesion);


                    $cedula_alcalde =  ConfigSystem('cedula_alcalde');
                    $telefonos_alacalde = DB::table('tma_playerid')
                        ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                        ->where('cedula', $cedula_alcalde)->get();

                    $players = array();
                    foreach ($telefonos_alacalde as $key => $value) {
                        array_push($players, $value->id_player);
                    }


                    $bandera = false;
                    if ($actividad->delegacion_alcalde != null) {
                        $delegacion_alcalde_ = explode(',', $actividad->delegacion_alcalde);
                        $mensajes = '';
                        if (!is_array($delegacion_alcalde_)) {
                            $delegacion_alcalde_ = [$actividad->delegacion_alcalde];
                        }
                        foreach ($delegacion_alcalde_ as $key => $value) {
                            # code...
                            $verificacion_delegado = $this->verificarDisponibilidad2($value, $inicio, $fin, $actividad->id);

                            if ($verificacion_delegado['respuesta'] == true) {
                                $bandera = true;
                                $mensajes .= $verificacion_delegado['mensaje_app'] . '<br>';
                            }
                            Log::info('--------------1486');
                            Log::info($verificacion_delegado);
                        }
                    }

                    $notificaciones = AgendaNotificacionesPersonalizadas::where("id_agenda",  $actividad->id)->get();
                    foreach ($notificaciones as $key => $value) {
                        # code...
                        $verificacion_delegado = $this->verificarDisponibilidad2($value->id_usuario, $inicio, $fin, $actividad->id);

                        if ($verificacion_delegado['respuesta'] == true) {
                            $bandera = true;
                            $mensajes .= $verificacion_delegado['mensaje_app'] . '<br>';
                        }
                        Log::info('--------------1500');
                        Log::info($verificacion_delegado);
                    }

                    if ($bandera) {
                        return response()->json(['error' => $mensajes], 200);
                    }

                    DB::commit();







                    $notificacion->notificacion($players, $actividad->id, '🔔  Actividad reprogramada',  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));
                    //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, '🔔  Actividad reprogramada',  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));


                    $msg2 = 'Actividad reprogramada';
                    if ($actividad->coordinadores == 1 || $actividad->coordinadores == true) {
                        //    dd(10);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 10);
                    }
                    if ($actividad->concejales == 1 || $actividad->concejales == true) {

                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 25);
                    }
                    if ($actividad->misconcejales == 1 || $actividad->misconcejales == true) {

                        $this->notificarGrupito('🔔  ' . $msg2, $actividad);
                    }
                    if ($actividad->directores == 1 || $actividad->directores == true) {
                        // dd($actividad);

                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 3);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 5);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 15);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 22);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 29);
                        $this->notificarTodos('🔔  ' . $msg2, $actividad, 9);
                    }
                    $this->notificarPersonalizada('🔔  ' . $msg2, $actividad);

                    $this->notificarDelgados('🔔  ' . $msg2, $actividad);
                } elseif ($tipo == 5) {

                    $verificacion = $this->verficar_existente($actividad->fecha_inicio, $actividad->fecha_fin, $id, 2, 0, $user->id);
                    if ($verificacion['estado'] == 'true') {
                        $actividad->id_estado = 3;
                        $actividad->recordar = "NO";
                    } else {
                        return response()->json(['error' =>  $verificacion['msg'], 'html' => $verificacion['actividades']], 200);
                    }

                    $actividad->save();
                    insertarhistorial($actividad, $actividad->id, 1, $id_usuario_sesion);
                    DB::commit();
                } elseif ($tipo == 6) { /////////pedir informacion
                    // $actividad->id_estado = 9;
                    $actividad->observacion_cancelacion = $observacion_cancelacion;
                    // $actividad->recordar = "NO";
                    $actividad->save();


                    insertarhistorial($actividad, $actividad->id, 1, $id_usuario_sesion);
                    DB::commit();
                    $this->notificarAlcaldia($observacion_cancelacion, $actividad);
                } elseif ($tipo == 7) { /////////delegar a....

                    $actividad->id_estado = 8;
                    $actividad->delegacion_alcalde = (is_array($delegacion_alcalde)) ? implode(',', $delegacion_alcalde) : $delegacion_alcalde;
                    $actividad->disposicion_alcalde = $observacion;
                    $actividad->recordar = "NO";
                    // dd("vale verga");
                    $actividad->save();
                    insertarhistorial($actividad, $actividad->id, 1, $id_usuario_sesion);

                    // AgendaNotificacionesPersonalizadas::where("id_agenda", $actividad->id)
                    //     ->delete();




                    $delegacion_alcalde = (is_array($delegacion_alcalde)) ? implode(',', $delegacion_alcalde) : $delegacion_alcalde;
                    if (!is_array($delegacion_alcalde)) {
                        if ($delegacion_alcalde != null) {
                            $delegacion_alcalde = [$delegacion_alcalde];
                        }
                    }
                    if (is_array($delegacion_alcalde)) {
                        $dele_ = '';
                        foreach ($delegacion_alcalde as $key => $value) {
                            $players = array();
                            $usuario = UsuariosModel::where('id', $value)->first();
                            if ($usuario) {
                                $verificacion_delegado = $this->verificarDisponibilidad2($value, $actividad->fecha_inicio, $actividad->fecha_fin, $actividad->id);

                                if ($verificacion_delegado['respuesta'] == true) {
                                    DB::rollback();
                                    return response()->json(['error' => $verificacion_delegado['mensaje_app']], 200);
                                }

                                $telefonos_personas = DB::table('tma_playerid')
                                    ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                                    ->where('u.id', $usuario->id)->get();
                                foreach ($telefonos_personas as $key => $v) {
                                    array_push($players, $v->id_player);
                                }
                                //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, '🔔  Ha sido delegado a una actividad de la agenda virtual',  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $actividad);
                                $notificacion->EnviarEmailAgenda($usuario->email, ' 🔔  Ha sido delegado a una actividad de la agenda virtual', '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha:</b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin)) . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') .  '</div>', "coordinacioncronograma/agendavirtual");
                                $notificacion->notificacion($players, $actividad->id, '🔔  Ha sido delegado a una actividad de la agenda virtual',  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));
                            }
                            $dele_ .= $usuario->name . '<br>';
                        }
                        // $notificacion->EnviarEmailAgenda($actividad->email_solicitante, 'Estimado, ' . $actividad->responsable . ' su actividad ha sido delegada', '<div style="padding: 5% 0 0 6%">   <b>Delegados: </b>' . $dele_ . '  <b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha: </b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin))  . '<br><b> Responsable: </b> ' . $actividad->responsable . '</div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '</div><div style="padding: 1% 0 0 6%"><b> Observación: </b> ' . $actividad->observacion . '</div>', "coordinacioncronograma/agendavirtual", "vistas.email", "NO");
                        DB::commit();
                        $msg2 = 'Actividad fue delegada';
                        if ($actividad->coordinadores == 1 || $actividad->coordinadores == true) {
                            //    dd(10);
                            $this->notificarTodos('🔔  ' . $msg2, $actividad, 10);
                        }

                        if ($actividad->concejales == 1 || $actividad->concejales == true) {

                            $this->notificarTodos('🔔  ' . $msg2, $actividad, 25);
                        }
                        if ($actividad->misconcejales == 1 || $actividad->misconcejales == true) {

                            $this->notificarGrupito('🔔  ' . $msg2, $actividad);
                        }

                        if ($actividad->directores == 1 || $actividad->directores == true) {
                            // dd($actividad);

                            $this->notificarTodos('🔔  ' . $msg2, $actividad, 3);
                            $this->notificarTodos('🔔  ' . $msg2, $actividad, 5);
                            $this->notificarTodos('🔔  ' . $msg2, $actividad, 15);
                            $this->notificarTodos('🔔  ' . $msg2, $actividad, 22);
                            $this->notificarTodos('🔔  ' . $msg2, $actividad, 29);
                            $this->notificarTodos('🔔  ' . $msg2, $actividad, 9);
                        }


                        $this->notificarPersonalizada('🔔  ' . $msg2, $actividad);
                    }
                } elseif ($tipo == 8) { /////////avisarle mas luego
                    $actividad->recordar = "SI";
                    // dd("vale verga");
                    $actividad->save();
                    insertarhistorial($actividad, $actividad->id, 1, $id_usuario_sesion);
                    DB::commit();
                } elseif ($tipo == 11) { /////////avisarle mas luego

                    $actividad->id_estado = 11;
                    // dd("vale verga");
                    $actividad->disposicion_alcalde = $observacion;
                    $actividad->save();
                    insertarhistorial($actividad, $actividad->id, 1, $id_usuario_sesion);
                    DB::commit();
                    $this->notificarAlcaldia('🔔  Actividad en lista de espera', $actividad);
                    $this->notificarRegistra('🔔  Actividad en lista de espera', $actividad);
                }


                return response()->json(["success", "Grabado Exitosamente"], 200);
            } catch (\Throwable $th) {
                DB::rollback();
                // return $th->getMessage();
                // return response()->json(['error' => 'Ocurrió un error intentelo nuevamente, por favor......'], 200);
                return response()->json(['error' => $th->getMessage()], 200);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }

    function verficar_existente($fecha_inicio, $fecha_fin, $id, $tipo, $tipo_agenda = 0, $perfil = 0)
    {

        $fecha_inicio = date('Y-m-d H:i:s', strtotime($fecha_inicio));
        $fecha_fin = date('Y-m-d H:i:s', strtotime($fecha_fin));

        $actividades = null;
        $eventos = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
            ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
            ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo as tipo_evento');



        // dd($perfil);
        if ($tipo == 1) {
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "users.id_perfil")->where("users.id", $perfil)->first();

            $actividades =  $eventos->where([['id_usuario', $id], ['id_estado', '<>', 7]])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })->get();

            // dd();
        } elseif ($tipo == 2) {

            // return ['estado' => 'true', 'msg' => $perfil];
            $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "users.id_perfil")->where("users.id", $perfil)->first();

            $actividades =  $eventos->whereIn('id_estado', [3, 4, 8])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })->get();
        } else {

            $actividades =  $eventos->
                // join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                // ->select('com_tmov_comunicacion_agenda.*','tp.tipo')
                // ->
                where([['com_tmov_comunicacion_agenda.id', '<>', $id]])
                ->whereIn('id_estado', [3, 4])
                // where([['com_tmov_comunicacion_agenda.id', '<>', $id], ['fecha_inicio', '<=', $fecha_inicio], ['fecha_fin', '>', $fecha_inicio], ['id_estado', 3]])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })->get();
        }
        // return $actividades;
        if (isset($actividades[0])) {
            $multiplied = AgendaVirtualResource::collection($actividades);
            return ['estado' => 'false', 'msg' => 'Existe una actividad en este rango de fecha', 'actividades' => $multiplied];
        } else {
            return ['estado' => 'true', 'msg' => 'ok...'];
        }
    }


    function verficar_existente_dos($fecha_inicio, $id, $tipo, $categoria)
    {
        if ($tipo == 1) {
            $actividades = AgendaVirtualModel::where([['id', '<>', $id], ['fecha_inicio', '<=', $fecha_inicio], ['fecha_fin', '>', $fecha_inicio]])
                ->whereIn('id_estado', [3, 4, 8])
                ->first();

            if ($actividades) {
                return ['estado' => 'false', 'msg' => 'Existe una actividad en este rango de fecha'];
            } else {
                return ['estado' => 'true', 'msg' => 'ok...'];
            }
        } elseif ($tipo == 2) {
            $fecha_inicio_ = date('Y-m-d', strtotime($fecha_inicio));
            $fecha_inicio = $fecha_inicio_ . ' 00:00:00';
            $fecha_fin = $fecha_inicio_ . ' 23:59:59';

            $barrios = AgendaVirtualModel::where([['id', '<>', $id], ['fecha_inicio', '>=', $fecha_inicio], ['fecha_inicio', '<=', $fecha_fin]])
                ->whereIn('id_estado', [2, 3, 4, 8])
                ->where('id_tipo', 4)
                ->count();

            // dd($barrios);
            $ciudadano = AgendaVirtualModel::where([['id', '<>', $id], ['fecha_inicio', '>=', $fecha_inicio], ['fecha_inicio', '<=', $fecha_fin]])
                ->whereIn('id_estado', [2, 3, 4, 8])
                ->where('id_tipo', 5)
                ->count();

            $gremios = AgendaVirtualModel::where([['id', '<>', $id], ['fecha_inicio', '>=', $fecha_inicio], ['fecha_inicio', '<=', $fecha_fin]])
                ->whereIn('id_estado', [2, 3, 4, 8])
                ->where('id_tipo', 6)
                ->count();

            // dd($barrios.$ciudadano.$gremios);

            if ($ciudadano == 4) {
                return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            }

            if ($barrios + $gremios == 4) {
                return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            }

            if ($ciudadano == 3 && ($barrios + $gremios) == 1) {
                return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            }

            if ($ciudadano == 2 && ($barrios + $gremios) == 2) {
                return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            }

            if ($ciudadano == 1 && ($barrios + $gremios) == 3) {
                return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            }

            // if ($ciudadano == 2 || $ciudadano == 1 || $ciudadano == 0) {
            //     // dd($barrios+$gremios);
            //     if (($barrios + $gremios) >= 2) {
            //         return ['estado' => 'false', 'msg' => 'Cupo lleno'];
            //     }
            //     // dd($barrios.$ciudadano.$gremios);
            // }

            return ['estado' => 'true', 'msg' => 'ok...'];
        }
    }


    function getDelegadosAlcalde()
    {
        $delegados = UsuariosModel::join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
            ->join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')
            ->select('users.id', DB::raw('CONCAT(d.alias," - ",users.name) as name'))
            ->whereIn('tipo', [3, 4, 10])
            ->orWhere('p.id', 25)
            ->orWhereIn('users.id', [1648, 1809])
            ->orderBy('d.alias', 'ASC')
            ->get();
        return $delegados;
    }



    public function notificarAlcalde($titulo, $actividad)
    {
        $cedula_alcalde =  ConfigSystem('cedula_alcalde');
        $notificacion = new NotificacionesController;
        $telefonos_alacalde = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->where('cedula', $cedula_alcalde)->get();

        $players = array();
        foreach ($telefonos_alacalde as $key => $value) {
            array_push($players, $value->id_player);
        }

        // $telefonos_personas = DB::table('tma_playerid')
        //     ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
        //     ->where('id_perfil', '<>', '2')->get();

        // foreach ($telefonos_personas as $key => $v) {
        //     array_push($players, $v->id_player);
        //     $notificacion->EnviarEmailAgenda($v->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha y hora de inicio:</b> ' . $actividad->fecha_inicio . ' <br> <b>Fecha y hora final:</b> ' . $actividad->fecha_fin . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '</div>', "coordinacioncronograma/agendavirtual");
        // }

        // $user_registra = UsuariosModel::where("id", $actividad->id_usuario)->first();
        // if ($user_registra) {
        //     $notificacion->EnviarEmailAgenda($user_registra->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha y hora de inicio:</b> ' . $actividad->fecha_inicio . ' <br> <b>Fecha y hora final:</b> ' . $actividad->fecha_fin . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '</div>', "coordinacioncronograma/agendavirtual");
        // }


        $notificacion->notificacion($players, $actividad->id, $titulo,  $actividad->nombre . '  🕐  Fecha:  ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));

        $usuario = User::where('cedula', $cedula_alcalde)->first();
        if ($usuario) {
            $notificacion->EnviarEmailAgenda($usuario->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha y hora de inicio:</b> ' . $actividad->fecha_inicio . ' <br> <b>Fecha y hora final:</b> ' . $actividad->fecha_fin . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') .  '</div>', "coordinacioncronograma/agendavirtual");
            //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha y hora de inicio:</b> ' . $actividad->fecha_inicio . ' <br> <b>Fecha y hora final:</b> ' . $actividad->fecha_fin . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') .  '</div>',  "coordinacioncronograma/agendavirtual");
        }
    }

    public function notificarRegistra($titulo, $actividad)
    {
        $notificacion = new NotificacionesController;
        $telefonos = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->where('u.id', $actividad->id_usuario)->get();

        $usuario = User::where('id', $actividad->id_usuario)->first();
        if ($usuario) {
            $notificacion->EnviarEmailAgenda($usuario->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");
            //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', " vistas.email");
        }
        $players = array();
        foreach ($telefonos as $key => $value) {
            array_push($players, $value->id_player);
        }
        $notificacion->notificacion($players, $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));
        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));
    }
    public function notificarTodos($titulo, $actividad, $perfil)
    {
        // dd($perfil);
        $web_api = new WebApiAgendaController;
        $notificacion = new NotificacionesController;
        $telefonos_personas = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->join('ad_perfil as p', 'p.id', '=', 'u.id_perfil')
            ->where('u.id_perfil', $perfil)
            ->get();

        $usuarios = UsuariosModel::where('id_perfil', $perfil)->get();
        foreach ($usuarios as $key => $value_) {
            $notificacion->EnviarEmailAgenda($value_->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");
        }

        $players = array();
        foreach ($telefonos_personas as $key => $value) {
            array_push($players, $value->id_player);
            // $notificacion->EnviarEmailAgenda($value->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");

        }
        $notificacion->notificacion($players, $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
    }
    public function notificarGrupito($titulo, $actividad)
    {
        // dd($perfil);
        $web_api = new WebApiAgendaController;
        $notificacion = new NotificacionesController;
        $misconcejales = explode("|", ConfigSystem("misconcejales"));

        $telefonos_personas = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->join('ad_perfil as p', 'p.id', '=', 'u.id_perfil')
            ->whereIn('u.id', $misconcejales)
            ->get();

        $usuarios = UsuariosModel::whereIn('id', $misconcejales)->get();
        foreach ($usuarios as $key => $value_) {
            $notificacion->EnviarEmailAgenda($value_->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");
        }

        $players = array();
        foreach ($telefonos_personas as $key => $value) {
            array_push($players, $value->id_player);
            // $notificacion->EnviarEmailAgenda($value->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");

        }
        $notificacion->notificacion($players, $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
    }



    public function notificarPersonalizada($titulo, $actividad)
    {
        // dd($perfil);
        $web_api = new WebApiAgendaController;
        $notificacion = new NotificacionesController;
        $telefonos_personas =  AgendaNotificacionesPersonalizadas::join('tma_playerid as p', 'p.users_id', '=', 'com_tmo_notifiacion_personalizada.id_usuario')
            // ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            // ->join('ad_perfil as p', 'p.id', '=', 'u.id_perfil')
            ->where("com_tmo_notifiacion_personalizada.id_agenda", $actividad->id)
            ->get();

        $usuarios =  AgendaNotificacionesPersonalizadas::
            // join('tma_playerid as p','p.users_id','=','com_tmo_notifiacion_personalizada.id_usuario')
            join('users as u', 'u.id', '=', 'com_tmo_notifiacion_personalizada.id_usuario')
            // ->join('ad_perfil as p', 'p.id', '=', 'u.id_perfil')
            ->where("com_tmo_notifiacion_personalizada.id_agenda", $actividad->id)
            ->get();
        foreach ($usuarios as $key => $value_) {
            $notificacion->EnviarEmailAgenda($value_->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), '', 'coordinacioncronograma/agendavirtual', "vistas.email");
        }

        $players = array();
        foreach ($telefonos_personas as $key => $value) {
            array_push($players, $value->id_player);
            // $notificacion->EnviarEmailAgenda($value->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna'), 'coordinacioncronograma/agendavirtual', "vistas.email");

        }
        $notificacion->notificacion($players, $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
        //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $web_api->getActividad($actividad->id));
    }
    public function notificarDelgados($titulo, $actividad)
    {
        $notificacion = new NotificacionesController;

        $delegados = $actividad->delegacion_alcalde;
        $array_delegados = explode(',', $delegados);
        if (!is_array($array_delegados) && $actividad->delegacion_alcalde != null) {
            $array_delegados = [$delegados];
        }

        if (is_array($array_delegados)) {
            foreach ($array_delegados as $key => $value) {
                $usuario = UsuariosModel::where('id', $value)->first();
                if ($usuario) {
                    $players = array();
                    $telefonos_personas = DB::table('tma_playerid')
                        ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                        ->where('u.id', $usuario->id)->get();
                    foreach ($telefonos_personas as $key => $v) {
                        array_push($players, $v->id_player);
                    }
                    //$notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, '🔔 ' . $titulo,  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $actividad);
                    $notificacion->EnviarEmailAgenda($usuario->email, ' 🔔 ' . $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre . ' <br> <b>Fecha:</b> ' . date('d/m/Y', strtotime($actividad->fecha_inicio)) . ' <br> <b> Desde: </b> ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' <b> Hasta: </b> ' . date('H:i', strtotime($actividad->fecha_fin)) . '<br></div>', '<div style="padding: 1% 0 0 6%"><b> Descripción: </b> ' . $actividad->descripcion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') .  '</div>', "coordinacioncronograma/agendavirtual");
                    $notificacion->notificacion($players, $actividad->id, '🔔 ' . $titulo,  $actividad->nombre . '  🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));
                }
            }
        }
    }






    public function notificarAlcaldia($titulo, $actividad)
    {
        $notificacion = new NotificacionesController;
        $telefonos = DB::table('tma_playerid')
            ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
            ->join('ad_perfil as p', 'p.id', '=', 'u.id_perfil')
            ->where('p.tipo', 8)->get();

        $usuario = User::join('ad_perfil as p', 'p.id', '=', 'users.id_perfil')
            ->select('users.*', 'p.tipo')
            ->where('p.tipo', 8)->get();

        foreach ($usuario as $key => $value) {
            # code...
            if ($value) {
                $notificacion->EnviarEmailAgenda($value->email, $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre,  '<div style="padding: 1% 0 0 6%"><b> Dirección:</b> ' . $actividad->direccion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') . '</div>', "coordinacioncronograma/agendavirtual");
                //$notificacion->EnviarEmailAgenda('cgpg94@gmail.com', $titulo, '<div style="padding: 5% 0 0 6%"><b>Actividad: </b>' . $actividad->nombre,  '<div style="padding: 1% 0 0 6%"><b> Dirección:</b> ' . $actividad->direccion . '<b> Observación: </b>' . ((isset($actividad->observacion)) ? '' . $actividad->observacion : 'Ninguna') . '</div>', " coordinacioncronograma/agendavirtual");
                $notificacion->notificacionesweb("Estimado(a) " . $value->name . ", existe una nueva actividad por revisar.", "coordinacioncronograma/agendavirtual", $value->id, "ed5565");
            }
        }

        $players = array();
        foreach ($telefonos as $key => $value) {
            array_push($players, $value->id_player);
        }
        $notificacion->notificacion($players, $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));
        $notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], $actividad->id, $titulo, $actividad->nombre . ' 🕐  Fecha: ' . date('d/m/Y', strtotime($actividad->fecha_inicio))  . '  desde: ' . date('H:i', strtotime($actividad->fecha_inicio)) . ' hasta ' . date('H:i', strtotime($actividad->fecha_fin)), 2, $this->getActividad($actividad->id));
    }


    public function getActividad($id)
    {
        $eventos = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
            ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
            ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
            ->select(
                'com_tmov_comunicacion_agenda.nombre as title',
                'descripcion as desc',
                'fecha_fin as endTime',
                'fecha_inicio as startTime',
                DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",adjunto) as adjunto_link'),
                DB::raw("null as allDay"),
                "com_tmov_comunicacion_agenda.*",
                'tp.tipo as tipo_evento',
                'ct.id as categoria_id',
                'es.nombre as estado_aprobacion'
            )
            ->where("com_tmov_comunicacion_agenda.id", $id)
            ->get();
        $actividades_collect = collect($eventos);
        $multiplied = $actividades_collect->map(function ($item, $key) {
            if ($item->delegacion_alcalde != null) {
                $responsables = DB::select('select GROUP_CONCAT(name SEPARATOR ", ") as delegacion_alcalde from users where id in (' . $item->delegacion_alcalde . ')');
                $responsables_ = $responsables[0]->delegacion_alcalde;
            } else {
                $responsables_ = null;
            }

            $user = UsuariosModel::join('ad_perfil as p', 'p.id', 'users.id_perfil')
                ->where('users.id', $item->id_usuario)->first();
            // dd($item);
            return collect(
                [
                    'title' => $item->nombre,
                    'desc' => $item->desc,
                    'endTime' => $item->endTime,
                    'startTime' => $item->startTime,
                    'adjunto_link' => $item->adjunto_link,
                    'allDay' => $item->allDay,
                    'adjunto_link' => $item->adjunto_link,
                    'tipo_evento' => $item->tipo_evento,
                    'categoria_id' => $item->categoria_id,
                    'id' => $item->id,
                    'nombre' => $item->nombre,
                    'descripcion' => $item->descripcion,
                    'direccion' => $item->direccion,
                    'longitud' => $item->longitud,
                    'latitud' => $item->latitud,
                    'fecha_inicio' => $item->fecha_inicio,
                    'fecha_fin' => $item->fecha_fin,
                    'id_usuario' => $item->id_usuario,
                    'estado_aprobacion' => $item->estado_aprobacion,
                    'observacion' => $item->observacion,
                    'estado' => $item->estado,
                    'responsable' => $item->responsable,
                    'silenciar' => $item->silenciar,
                    'n_beneficiarios' => $item->n_beneficiarios,
                    'observacion_cancelacion' => $item->observacion_cancelacion,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at,
                    'tipo_agenda' => $item->tipo_agenda,
                    'tipo' => $item->tipo,
                    'categoria' => $item->categoria,
                    'email_solicitante' => $item->email_solicitante,
                    'recordar' => $item->recordar,
                    'delegacion_alcalde' => $responsables_,
                    'disposicion_alcalde' => $item->disposicion_alcalde,
                    'id_estado' => $item->id_estado,
                    'id_tipo' => $item->id_tipo,
                    'adjunto' => $item->adjunto,
                    'categoria_id' => $item->categoria_id,
                    'telefono_solicitante' => $item->telefono_solicitante,
                    'email_solicitante' => $item->email_solicitante,
                    'usuario_registra' => $user->name,
                    'perfil_registra' => $user->id_perfil
                ]
            );
        });

        return $multiplied[0];
    }












    public function getActividadAgenda($token, $id)
    {

        $usuario = User::where(['api_token' => $token])->first();
        if ($usuario) {
            $eventos = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                ->leftjoin('tmo_canton as ca', 'ca.id', '=', 'com_tmov_comunicacion_agenda.canton_id')

                ->select(
                    'com_tmov_comunicacion_agenda.nombre as title',
                    'descripcion as desc',
                    'fecha_fin as endTime',
                    'fecha_inicio as startTime',
                    DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",adjunto) as adjunto_link'),
                    DB::raw("null as allDay"),
                    "com_tmov_comunicacion_agenda.*",
                    'tp.tipo as tipo_evento',
                    'ct.id as categoria_id',
                    'es.nombre as estado_aprobacion',
                    'pr.parroquia',
                    'b.barrio',
                    'b.id_parroquia',
                    'ca.nombre_canton as canton',
                    'com_tmov_comunicacion_agenda.canton_id'
                )
                ->where("com_tmov_comunicacion_agenda.id", $id)
                ->get();
            $actividades_collect = collect($eventos);
            $multiplied = $actividades_collect->map(function ($item, $key) {
                $delegados = [];
                // $notificados=[];
                $accion = null;
                $notificacion_personlizada = AgendaNotificacionesPersonalizadas::where('id_agenda', $item->id)->pluck('id_usuario')->all();
                if ($item->delegacion_alcalde != null) {
                    $responsables = DB::select('select GROUP_CONCAT(name SEPARATOR ", ") as delegacion_alcalde from users where id in (' . $item->delegacion_alcalde . ')');
                    $responsables_ = $responsables[0]->delegacion_alcalde;
                    $delegados = explode(',', $item->delegacion_alcalde);
                    if (!isset($delegados[0]) && $item->delegacion_alcalde != null) {
                        $delegados = [$item->delegacion_alcalde];
                        $accion = 1;
                    }
                } else {
                    $responsables_ = null;
                }
                $notificacion_personlizada_usuarios = DB::select('select GROUP_CONCAT(name SEPARATOR ", ") as notificacion_personlizada_usuarios from com_tmo_notifiacion_personalizada as n inner join users as u on u.id=n.id_usuario  where id_agenda=' . $item->id . '');
                if (isset($notificacion_personlizada_usuarios[0]->notificacion_personlizada_usuarios)) {
                    $notificacion_personlizada_usuarios = $notificacion_personlizada_usuarios[0]->notificacion_personlizada_usuarios;
                    $accion = 0;
                } else {
                    $notificacion_personlizada_usuarios = null;
                }

                if (isset($notificacion_personlizada_usuarios[0]) && isset($delegados[0])) {

                    $accion = 2;
                }

                $archivos = AgendaArchivosModel::select(DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",ruta) as ruta'))->where('id_agenda', $item->id)->pluck('ruta')->all();
                $user = UsuariosModel::join('ad_perfil as p', 'p.id', 'users.id_perfil')
                    ->where('users.id', $item->id_usuario)->first();
                return collect(
                    [
                        'title' => $item->nombre,
                        'desc' => $item->desc,
                        'endTime' => $item->endTime,
                        'startTime' => $item->startTime,
                        'adjunto_link' => $archivos,
                        'allDay' => $item->allDay,
                        'tipo_evento' => $item->tipo_evento,
                        'categoria_id' => $item->categoria_id,
                        'id' => $item->id,
                        'nombre' => $item->nombre,
                        'descripcion' => $item->descripcion,
                        'direccion' => $item->direccion,
                        'longitud' => $item->longitud,
                        'latitud' => $item->latitud,
                        'fecha_inicio' => $item->fecha_inicio,
                        'fecha_fin' => $item->fecha_fin,
                        'id_usuario' => $item->id_usuario,
                        'estado_aprobacion' => $item->estado_aprobacion,
                        'observacion' => $item->observacion,
                        'estado' => $item->estado,
                        'responsable' => $item->responsable,
                        'silenciar' => $item->silenciar,
                        'n_beneficiarios' => $item->n_beneficiarios,
                        'observacion_cancelacion' => $item->observacion_cancelacion,
                        'created_at' => $item->created_at,
                        'updated_at' => $item->updated_at,
                        'tipo_agenda' => $item->tipo_agenda,
                        'tipo' => $item->tipo,
                        'categoria' => $item->categoria,
                        'email_solicitante' => $item->email_solicitante,
                        'recordar' => $item->recordar,
                        'delegacion_alcalde' => $responsables_,
                        'delegacion_alcalde_id' => $delegados,
                        'disposicion_alcalde' => $item->disposicion_alcalde,
                        'id_estado' => $item->id_estado,
                        'id_tipo' => $item->id_tipo,
                        'adjunto' => $item->adjunto,
                        'categoria_id' => $item->categoria_id,
                        'telefono_solicitante' => $item->telefono_solicitante,
                        'email_solicitante' => $item->email_solicitante,
                        'usuario_registra' => $user->name,
                        'perfil_registra' => $user->id_perfil,
                        'detalle_direccion' => $item->detalle_direccion,
                        'id_parroquia' => $item->id_parroquia,
                        'parroquia' => $item->parroquia,
                        'canton_id' => ($item->canton_id == 0 ? 146 : $item->canton_id),
                        'canton' => ($item->canton == NULL ? 'MANTA' : $item->canton),
                        'notificacion_personlizada' => $notificacion_personlizada,
                        'notificacion_personlizada_usuarios' => $notificacion_personlizada_usuarios,
                        'obs_delegacion' => $item->obs_delegacion,
                        'coordinadores' => (boolval($item->coordinadores) ? true : false),
                        'directores' => (boolval($item->directores) ? true : false),
                        'concejales' => (boolval($item->concejales) ? true : false),
                        'misconcejales' => (boolval($item->misconcejales) ? true : false),
                        'confirmar' => (boolval($item->confirmar_asistencia) ? true : false),
                        'asistio' => (boolval($item->asistio) ? true : false),
                        'barrio' => $item->barrio,
                        'barrio_id' => $item->barrio_id,
                        'metodo_regsitro' => $item->metodo_regsitro,
                        'accion' => $accion

                    ]
                );
            });
            return (isset($multiplied[0])) ? $multiplied[0] : null;
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }


    public function validarAgenda($categoria, $fecha_inicio, $fecha_fin, $tipo_agenda = 0, $id = 0, $id_usuario)
    {

        $fecha = date('w', strtotime($fecha_inicio));
        $hora = date('H', strtotime($fecha_inicio));
        $hora_fin = date('H', strtotime($fecha_fin));
        $minutos = date('i', strtotime($fecha_inicio));
        $minutos_fin = date('i', strtotime($fecha_fin));
        $hora_fin = date('H', strtotime($fecha_fin));
        $errores = '';
        $tipo = 0;
        if ($categoria == 8) {
            if ($fecha != self::MIERCOLES) {
                $errores .= '\n Solo se puede agendar los días miercoles desde las 01:00 hasta las 12:00.';
            }

            if ($hora <  self::MIN_EVENTO  || $hora > 12) {
                if ($fecha != 6) {
                    if ($hora == 12 && $minutos > 0) {
                        $errores .= '\n Solo se pueden agendar a hasta las 12:00.';
                    } else {
                        $errores .= '\n  Solo se pueden agendar a partir de las 01:00 hasta las 12:00.';
                    }
                }
            }
            if ($hora_fin >  12) {
                $errores .= '\n  La hora de fin no puede ser mayor a 12:00.';
            }
            if ($hora_fin >  12 && $minutos_fin > 0) {
                $errores .= '\n  Solo se pueden agendar a partir de las 01:00 hasta las 12:00.';
            }
        }

        // dd($id_usuario);
        $verificacion = $this->verficar_existente(Input::get('fecha_inicio'), Input::get('fecha_fin'), $id_usuario, 1, $tipo_agenda, $id_usuario);
        if ($verificacion['estado'] == 'false' && Input::get('en_espera') == null) {
            $errores .= '\n Ud. ya registró un evento en este rango de fecha y hora. ';
        }


        $verificacion = $this->verficar_existente(Input::get('fecha_inicio'), Input::get('fecha_fin'), $id, 2, 0, $id_usuario);
        if ($verificacion['estado'] == 'false' && Input::get('en_espera') == null) {
            $errores .= '\n  Existe una actividad en este rango de fecha y hora.';
        }


        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "users.id_perfil")->where("users.id", $id_usuario)->first();
        if ($id_tipo_pefil->id_perfil != 12 && $id_tipo_pefil->id_perfil != 16) {
            if ($categoria == 3 && Input::get('en_espera') == null) {
                if ($fecha != self::LUNES) {
                    $errores .= '\n  Solo se puede agendar reuniones con el Alcalde los días lunes.';
                }
            }

            // if ($categoria == 3 && $hora < 15) {
            //     return response()->json(['error' => 'Las reuniones con el Alcalde son apartir de las 15:00 en adelante.', 'tipo' => 0], 200);
            // }


            // if (($fecha == self::JUEVES  && $categoria != 7) && Input::get('en_espera') == null) {
            //     // si es director
            //     if ($id_tipo_pefil->id_perfil != 12 && $id_tipo_pefil->id_perfil != 16) {
            //         $errores .= '\n  Los días jueves no se pueden agendar actividades.';
            //     }
            // }

            // if ($categoria == 3 && $fecha == self::JUEVES) {
            //     $errores .= '\n  Los días jueves no se pueden agendar actividades.';
            // }
            if ($hora_fin > self::MAX_EVENTO) {
                $errores .= '\n  Solo se pueden agendar hasta las 23:00.';
            }

            if ($categoria == 4 || $categoria == 5 || $categoria == 6) {
                if ($categoria == 4 && $fecha != self::MARTES && $fecha != self::VIERNES && $fecha != self::SABADO) {
                    $errores .= '\n  Solo se puede agendar únicamente los Martes, Viernes y Sabados.';
                }
                if ($categoria != 4 && $fecha != self::MARTES  && $fecha !=  self::VIERNES) {
                    $errores .= '\n Solo se puede agendar únicamente los Martes y Viernes.';
                }
                if ($hora <  self::MIN_EVENTO  || $hora > 12) {
                    if ($fecha != 6) {
                        // tipo: 0 => validación de horas / 1 => puede ir a lista de espera
                        if ($hora == 12 && $minutos > 0) {
                            $errores .= '\n  Solo se pueden agendar a partir de las 01:00 hasta las 12:00.';
                        } else {
                            $errores .= '\n  Solo se pueden agendar a partir de las 01:00 hasta las 12:00.';
                        }
                    }
                }

                $verificacion_ = $this->verficar_existente_dos(Input::get('fecha_inicio'), Input::get('id'), 2, $categoria);
                // if ($verificacion['estado'] == 'false' && Input::get('en_espera') == null) {
                if ($verificacion_['estado'] == 'false') {
                    $errores .= '\n  Ya no se puede registrar este tipo de agenda para el día de hoy.';
                }
            }
            if ($fecha == self::MIERCOLES  && $categoria != 1 && $categoria != 7 && $hora > 12) {
                $errores .= '\n  Los días miercoles a partir de medio día solo se pueden agendar eventos y reuniones con instituciones externas.';
            }
            if ($errores != '') {
                return response()->json(['estado' => false, 'error' =>  $errores, 'html' => (isset($verificacion['actividades'])) ? $verificacion['actividades'] : ''], 200);
            } else {
                return response()->json(['estado' => true,  'msg' =>  'ok....', 'tipo' => 0], 200);
            }
        }
    }



    public function timelineagenda($agenda, $token)
    {

        $usuario = User::where(['api_token' => $token])->first();
        if ($usuario) {
            $timeline_agenda = AgendaVirtualModelDeta::leftjoin('users as u', 'u.id', '=', 'com_tmov_comunicacion_agenda_deta.id_usuario_modi')
                ->join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda_deta.id_estado')
                ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda_deta.id_tipo')
                ->select('com_tmov_comunicacion_agenda_deta.*', 'u.name', 'e.nombre as estado_agenda')
                ->where('id_cab_agenda', $agenda)->get();

            if (isset($timeline_agenda[0])) {
                return response()->json(['estado' =>  true, 'datos' => $timeline_agenda], 200);
            } else {
                return response()->json(['estado' =>  false, 'datos' => []], 200);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }


    public function vistosagenda()
    {

        $token = Input::get('token');
        $id_agenda = Input::get('id_agenda');
        $usuario = User::where(['api_token' => $token])->first();
        if ($usuario) {
            $verificar = AgendaVistaModel::select('u.name', 'com_tma_visto_agenda.created_at')
                ->join('users as u', 'u.id', '=', 'com_tma_visto_agenda.id_usuario')
                ->where("id_agenda", $id_agenda)->get();

            if (isset($verificar[0])) {
                return response()->json(['estado' =>  true, 'datos' => $verificar], 200);
            } else {
                return response()->json(['estado' =>  false, 'datos' => []], 200);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }
    public function ingresarVisto()
    {
        $token = Input::get('token');
        $id_agenda = Input::get('id_agenda');
        $usuario = User::where(['api_token' => $token])->first();
        if ($usuario) {
            if($usuario->id==1818){
                return response()->json(['estado' =>  true, 'msg' => "Ok"], 200);
            }
            $verificar = AgendaVistaModel::where(['id_usuario' => $usuario->id, "id_agenda" => $id_agenda])->first();
            if (!$verificar) {
                $verificar = new AgendaVistaModel;
                $verificar->id_usuario = $usuario->id;
                $verificar->id_agenda = $id_agenda;
                $verificar->save();


                $cedula_alcalde =  ConfigSystem('cedula_alcalde');
                $notificacion = new NotificacionesController;
                $telefonos_alacalde = DB::table('tma_playerid')
                    ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                    ->where('cedula', $cedula_alcalde)->get();
                $players = array();
                foreach ($telefonos_alacalde as $key => $value) {
                    array_push($players, $value->id_player);
                }
                $actividad = AgendaVirtualModel::where('id', $id_agenda)->first();
                if ($actividad) {
                    $notificacion->notificacion($players,  $id_agenda, '🔔 Actividad vista por ' . $usuario->name,  $actividad->nombre, 2, $this->getActividad($id_agenda));
                }
                return response()->json(['estado' =>  true, 'msg' => "Ok"], 200);
            } else {
                return response()->json(['estado' =>  true, 'msg' => "Actividad vista"], 200);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }


    public function consultarVisto()
    {
        $token = Input::get('token');
        $id_agenda = Input::get('id_agenda');
        $usuario = User::where(['api_token' => $token])->first();
        if ($usuario) {
            $eventos = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->select(
                    'com_tmov_comunicacion_agenda.nombre as title',
                    'descripcion as desc',
                    'fecha_fin as endTime',
                    'fecha_inicio as startTime',
                    DB::raw('CONCAT("' . URL::to('') . '/archivos_sistema","/",adjunto) as adjunto_link'),
                    DB::raw("null as allDay"),
                    "com_tmov_comunicacion_agenda.*",
                    'tp.tipo as tipo_evento',
                    'ct.id as categoria_id',
                    'es.nombre as estado_aprobacion'
                )
                ->where("com_tmov_comunicacion_agenda.id", $id_agenda)
                ->get();
            $actividades_collect = collect($eventos);
            $multiplied = $actividades_collect->map(function ($item, $key) use ($id_agenda) {
                $responsables = [];
                if ($item->masiva == 0) {
                    if ($item->delegacion_alcalde != null) {
                        $responsables_ = explode(',', $item->delegacion_alcalde);
                        foreach ($responsables_ as $key => $value) {
                            array_push($responsables, intVal($value));
                        }
                    }
                    $select = AgendaNotificacionesPersonalizadas::where('id_agenda', $id_agenda)->get();
                    foreach ($select as $key => $value) {
                        if (!in_array($value->id_usuario, $responsables)) {
                            array_push($responsables, $value->id_usuario);
                        }
                    }

                    $select = AgendaVistaModel::where('id_agenda', $id_agenda)->get();
                    foreach ($select as $key => $value) {
                        if (!in_array($value->id_usuario, $responsables)) {
                            array_push($responsables, $value->id_usuario);
                        }
                    }
                } else {

                    $usuarios = UsuariosModel::whereIN('id_perfil', [3, 4])->get();
                    foreach ($usuarios as $key => $value) {
                        array_push($responsables, $value->id);
                    }
                }

                $usuarios_final = [];
                foreach ($responsables as $key => $value) {
                    $usuario = UsuariosModel::find($value);
                    $verificar = AgendaVistaModel::where(['id_usuario' => $value, "id_agenda" => $id_agenda])->first();
                    if ($verificar) {
                        $usuarios_final[] = ['name' => $usuario->name, 'visto' => true, 'fecha' => date('Y-m-d H:i', strtotime($verificar->created_at))];
                    } else {
                        $usuarios_final[] = ['name' => $usuario->name, 'visto' => false, 'fecha' => null];
                    }
                }
                return $usuarios_final;
            });

            return response()->json(['estado' =>  true, 'data' => $multiplied[0]], 200);
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }


    public function subirArchivo()
    {
        $token = Input::get('api_token');
        $id_usuario = Input::get('id_usuario');
        $id_agenda = Input::get('id_agenda');

        $usuario = User::where(['api_token' => $token, 'id' => $id_usuario])->first();
        if ($usuario) {
            if (Input::hasFile("adjunto")) {
                $dir = public_path() . '/archivos_sistema/';
                $docs = Input::file("adjunto");
                if ($docs) {
                    $fileName = "agenda-$id_agenda-" . date("YmdHis") . "." . $docs->getClientOriginalExtension();
                    $ext = $docs->getClientOriginalExtension();
                    $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                    if (!in_array($ext, $perfiles)) {
                        return response()->json(['error' => 'Archivo no valido'], 200);
                    }

                    $file = AgendaVirtualModel::find($id_agenda);
                    $oldfile = $dir . $file->archivo;
                    File::delete($oldfile);
                    $file->adjunto = $fileName;
                    $file->save();
                    $docs->move($dir, $fileName);
                    return response()->json(['estado' =>  true, 'data' => 'Archivo agregado correctamente'], 200);
                }
            } else {
                return response()->json(['error' => 'Archivo no valido'], 200);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }

    public function generarReporteAgenda()
    {
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');

        $fecha_inicio = date('Y-m-d 00:00:00', strtotime($fecha_inicio));
        $fecha_fin = date('Y-m-d 23:59:59', strtotime($fecha_fin));

        $token = Input::get('api_token');
        $id_usuario = Input::get('id_usuario');
        $usuario = User::where(['api_token' => $token, 'id' => $id_usuario])->first();
        if ($usuario) {
            $json = '[
                {"name":"_token","value":"Spd0QAKYsX9VWEEiw5lmMMFs2gF1uXGv1FGZ1AMs"},
                {"name":"IDDIRECCION","value":"0"},
                {"name":"ESTADO","value":"0"},
                {"name":"TIPO","value":"0"},
                {"name":"namereporte","value":"rpt_reportesagenda"},
                {"name":"FECHAINICIO","value":"' . $fecha_inicio . '"},
                {"name":"FECHAFINAL","value":"' . $fecha_fin . '"},
                {"name":"FECHAACTUDESDE","value":"' . $fecha_inicio . '"},
                {"name":"FECHAACTUHASTA","value":"' . $fecha_fin . '"},
                {"name":"formato","value":"pdf"},
                {"name":"tiporeporte","value":"completo"},
                {"name":"tituloreporte","value":"REPORTE AGENDA TERRITORIAL"}
            ]';
            $data = json_decode($json);
            // dd($data);
            //$namedata=Crypt::decrypt(Input::get("namedata"));
            $paramadi = 1;
            $paramsok = array();
            foreach ($data as $key => $value) {
                # code...
                //show($value);
                if ($value->name != "_token" && $value->name != "formato" && $value->name != "namereporte" && $value->name != "tituloreporte" && $value->name != "tiporeporte" && $value->name != "nada")
                    $paramsok[$value->name] = $value->value;
                elseif ($value->name == "formato")
                    $formato = $value->value;
                elseif ($value->name == "namereporte")
                    $namedata = $value->value;
                elseif ($value->name == "tituloreporte")
                    $paramsok["TITULO"] = mb_strtoupper($value->value);
                elseif ($value->name == "tiporeporte")
                    if ($value->value == "informe" || $value->value == "observa")
                        $namedata .= "_" . $value->value;
            }
            // Filtros

            // show($paramadi);
            //$name, $params, $format, $filename, $download, $download_is_attachment = true
            $filename = "Actividades-" . str_random(6);
            //show(array($namedata,$paramsok+$paramadi,$formato,$filename,true,false));
            $r = new ReportesJasperController();

            return $r->generateReport($namedata, $paramsok + ['USUARIO' => $paramadi], $formato, $filename, true, false);
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }



    public function consultarEstadisticaAgenda()
    {
        $token = Input::get('token');
        $usuario = User::where(['api_token' => $token])->first();
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');

        $fecha_inicio = date('Y-m-d 00:00:00', strtotime($fecha_inicio));
        $fecha_fin = date('Y-m-d 23:59:59', strtotime($fecha_fin));

        if ($usuario) {
            $total = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                // ->where('tipo_agenda', 0)
                ->where(['ct.estado' => 'ACT'])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })
                ->count();
            $eventos = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                ->select(
                    'com_tmov_comunicacion_agenda.barrio_id',
                    'pr.parroquia',
                    'b.id_parroquia',
                    'ct.porcentaje as porcentaje_categoria',
                    DB::raw('count(*) as total'),
                    DB::raw('round((count(com_tmov_comunicacion_agenda.id)/' . $total . ')*100,2) as porcentaje')
                )
                // ->where('tipo_agenda', 0)
                ->where(['ct.estado' => 'ACT'])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })
                ->groupby('b.id_parroquia')
                ->orderby('total', 'desc')
                ->get();




            return response()->json(['estado' =>  true, 'data' =>  $eventos], 200);
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }
    public function consultarEstadisticaAgendaCategoria()
    {
        $token = Input::get('token');
        $usuario = User::where(['api_token' => $token])->first();
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');

        $fecha_inicio = date('Y-m-d 00:00:00', strtotime($fecha_inicio));
        $fecha_fin = date('Y-m-d 23:59:59', strtotime($fecha_fin));

        if ($usuario) {
            $total = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                ->where(['ct.estado' => 'ACT'])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })
                ->count();
            $eventos = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                ->select(
                    'ct.id',
                    'ct.nombre',
                    'ct.porcentaje as porcentaje_categoria',
                    DB::raw('count(*) as total'),
                    DB::raw('round((count(com_tmov_comunicacion_agenda.id)/' . $total . ')*100,2) as porcentaje')

                )
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })
                ->where(['ct.estado' => 'ACT'])
                ->groupby('ct.nombre')
                ->orderby('total', 'desc')
                ->get();

            $collectdetalle = collect($eventos);
            $collectdetalle = $collectdetalle->map(function ($item, $key) use ($fecha_inicio, $fecha_fin) {

                $total = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                    ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                    ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                    ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                    ->where(['ct.estado' => 'ACT', 'ct.id' => $item->id, 'tp.estado' => 'ACT'])
                    ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                        $query->where('fecha_inicio', '<=', $fecha_inicio)
                            ->where('fecha_fin', '>=', $fecha_fin)

                            ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                            ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                    })
                    ->count();

                $detalle = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                    ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                    ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                    ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                    ->select(
                        'tp.id',
                        'tp.tipo',
                        'tp.porcentaje_tipo as porcentaje_categoria',
                        DB::raw('count(*) as total'),
                        DB::raw('round((count(com_tmov_comunicacion_agenda.id)/' . $total . ')*100,2) as porcentaje')

                    )
                    ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                        $query->where('fecha_inicio', '<=', $fecha_inicio)
                            ->where('fecha_fin', '>=', $fecha_fin)

                            ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                            ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                    })
                    ->where(['ct.estado' => 'ACT', 'tp.estado' => 'ACT', 'ct.id' => $item->id])
                    ->groupby('tp.tipo')
                    ->orderby('total', 'desc')
                    ->get();

                $categorias = TipoAgendaModel::where(['estado' => 'ACT', 'id_categoria' => $item->id])->get();
                foreach ($categorias as $key => $value) {
                    $eventos_ = collect($detalle);
                    $eventos = $eventos_->where('id', $value->id)->first();
                    if (!isset($eventos)) {

                        $detalle[] = ['id' => $value->id, 'tipo' => $value->tipo, 'porcentaje_categoria' => $value->porcentaje_tipo, 'total' => 0, 'porcentaje' => 0];
                    }
                }

                return ['id' => $item->id, 'nombre' => $item->nombre, 'porcentaje_categoria' => $item->porcentaje_categoria, 'total' => $item->total, 'porcentaje' => $item->porcentaje, 'detalle' => $detalle];
            });
            // return $collectdetalle;







            $categorias = CategoriaAgendaModel::where('estado', 'ACT')->get();
            $array_categorias = [];
            foreach ($categorias as $key => $value) {
                $eventos_ = collect($eventos);
                $eventos_ = $eventos_->where('id', $value->id)->first();
                if (!isset($eventos_)) {
                    // dd($eventos_);

                    $categorias = TipoAgendaModel::where(['estado' => 'ACT', 'id_categoria' => $value->id])->get();
                    $detalle = [];
                    foreach ($categorias as $key => $v) {
                        $detalle[] = ['id' => $v->id, 'tipo' => $v->tipo, 'porcentaje_categoria' => $v->porcentaje_tipo, 'total' => 0, 'porcentaje' => 0];
                    }
                    $array_categorias[] = ['id' => $value->id, 'nombre' => $value->nombre, 'porcentaje_categoria' => $value->porcentaje, 'total' => 0, 'porcentaje' => 0, 'detalle' =>  $detalle];
                }
            }

            $array_categorias = collect($array_categorias);
            $eventos = collect($collectdetalle);
            $eventos = $eventos->merge($array_categorias);

            return response()->json(['estado' =>  true, 'data' =>  $eventos], 200);
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }
    public function consultarEstadisticaAgendaParroquias()
    {
        $token = Input::get('token');
        $id_parroquia = Input::get('id_parroquia');
        $usuario = User::where(['api_token' => $token])->first();
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');

        $fecha_inicio = date('Y-m-d 00:00:00', strtotime($fecha_inicio));
        $fecha_fin = date('Y-m-d 23:59:59', strtotime($fecha_fin));

        if ($usuario) {

            $eventos = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                ->select(
                    'com_tmov_comunicacion_agenda.nombre as title',
                    'descripcion as desc',
                    "com_tmov_comunicacion_agenda.fecha_inicio",
                    "com_tmov_comunicacion_agenda.fecha_fin",
                    "com_tmov_comunicacion_agenda.nombre",
                    "com_tmov_comunicacion_agenda.tipo_agenda",
                    // "com_tmov_comunicacion_agenda.*",
                    'tp.tipo as tipo_evento',
                    'ct.id as categoria_id',
                    'es.nombre as estado_aprobacion',
                    'pr.parroquia',
                    'b.id_parroquia'
                )
                ->where(['pr.id' => $id_parroquia, 'ct.estado' => 'ACT'])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })
                ->orderBy('com_tmov_comunicacion_agenda.fecha_inicio', 'desc')
                ->paginate(15);

            return $eventos;
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }
    public function consultarEstadisticaAgendaCategoriaDeta()
    {
        $token = Input::get('token');
        $id_categoria = Input::get('id_categoria');
        $usuario = User::where(['api_token' => $token])->first();
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');

        $fecha_inicio = date('Y-m-d 00:00:00', strtotime($fecha_inicio));
        $fecha_fin = date('Y-m-d 23:59:59', strtotime($fecha_fin));

        if ($usuario) {

            $eventos = AgendaVirtualModel::join('com_tma_tipo_agenda as tp', 'tp.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
                ->join('com_tma_categoria_agenda as ct', 'ct.id', '=', 'tp.id_categoria')
                ->join('com_tma_estados_agenda as es', 'es.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
                ->join('barrio as b', 'b.id', '=', 'com_tmov_comunicacion_agenda.barrio_id')
                ->join('parroquia as pr', 'pr.id', '=', 'b.id_parroquia')
                ->select(
                    'com_tmov_comunicacion_agenda.nombre as title',
                    'descripcion as desc',
                    "com_tmov_comunicacion_agenda.fecha_inicio",
                    "com_tmov_comunicacion_agenda.fecha_fin",
                    "com_tmov_comunicacion_agenda.nombre",
                    "com_tmov_comunicacion_agenda.tipo_agenda",
                    // "com_tmov_comunicacion_agenda.*",
                    'tp.tipo as tipo_evento',
                    'ct.id as categoria_id',
                    'es.nombre as estado_aprobacion',
                    'pr.parroquia',
                    'b.id_parroquia'
                )
                ->where(['ct.id' => $id_categoria, 'ct.estado' => 'ACT'])
                ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)

                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                })
                ->orderBy('com_tmov_comunicacion_agenda.fecha_inicio', 'desc')
                ->paginate(15);

            return $eventos;
        } else {
            return response()->json(['error' => 'Unauthorized'], 200);
        }
    }



    public function verificarDisponibilidad($id_usuario, $fecha_inicio, $fecha_fin)
    {
        $tipo = Input::get('tipo');
        if ($tipo != null) {
            $tipo = Crypt::decrypt($tipo);
            $agenda = AgendaVirtualModel::where('id', $tipo)->first();
            if ($agenda) {
                $fecha_inicio = $agenda->fecha_inicio;
                $fecha_fin = $agenda->fecha_fin;
            }
        }
        // $fecha_inicio=Input::get('fecha_inicio');
        // $fecha_fin=Input::get('fecha_fin');
        // dd(Input::all());

        $eventos = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
            ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
            ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo as tipo_evento')
            ->where([['id_estado', '<>', 7], ['id_estado', '<>', 6]])
            ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                $query->where('fecha_inicio', '<=', $fecha_inicio)
                    ->where('fecha_fin', '>=', $fecha_fin)

                    ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                    ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
            })->get();
        // dd( $eventos);

        $actividades_collect = collect($eventos);
        $multiplied_delegados = $actividades_collect->map(function ($item, $key) use ($id_usuario) {
            $responsables_para_mostrar = [];
            if ($item->delegacion_alcalde != null) {
                $responsables_para_mostrar = DB::select('select id,name from users where id in (' . $item->delegacion_alcalde . ')');
            }
            $bandera = false;
            $html = '';
            foreach ($responsables_para_mostrar as $k) {
                if ($id_usuario == $k->id) {
                    // show($responsables_para_mostrar);
                    $bandera = true;
                    $html .= $item->id . '  ' . $k->name . "<br>";
                }
            }
            // $bandera = false;

            // show($bandera);
            return ["valor" => $bandera, "personas" => $html];
        });


        $eventos_ = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
            ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
            ->join('com_tmo_notifiacion_personalizada as  np', 'np.id_agenda', '=', 'com_tmov_comunicacion_agenda.id')
            ->join('users as  u', 'u.id', '=', 'np.id_usuario')
            ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo as tipo_evento', 'u.name')
            ->where([['id_estado', '<>', 7], ['id_estado', '<>', 6], ['np.id_usuario', '=', $id_usuario]])
            ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                $query->where('fecha_inicio', '<=', $fecha_inicio)
                    ->where('fecha_fin', '>=', $fecha_fin)

                    ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                    ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
            })

            ->get();

        $actividades_collect_ = collect($eventos_);
        $multiplied_delegados_ = $actividades_collect_->map(function ($item, $key) {
            $bandera = true;
            $html = "";
            return ["valor" => $bandera, "personas" => "<b>" . $item->id . '  ' . $item->nombre . ' Inicio: ' . $item->fecha_inicio . ' Fin: ' . $item->fecha_fin . ": </b> " . $html];
        });

        // dd($multiplied_delegados_);
        $multiplied_delegados = collect($multiplied_delegados)->merge($multiplied_delegados_);

        $datos = collect($multiplied_delegados)->where('valor', true);

        $datos_ = collect($multiplied_delegados)->where('valor', true)->first();
        if ($datos_) {
            $personas = "";
            foreach ($datos as $key => $value) {
                $personas .= $value['personas'];
                # code...

            }
            return response()->json(['respuesta' => true, 'mensaje' => 'No se puede agregar a esta actividad porque ya se encuentra delegado en otra', "mensaje_app" => 'La(s) persona(s) encuentra(n) asignada <br> ' . $personas], 200);
        } else {
            // return 0 ;
            return response()->json(['respuesta' => false, 'mensaje' => 'ok', "mensaje_app" => 'Ok'], 200);
        }
    }
    public function verificarDisponibilidad2($id_usuario, $fecha_inicio, $fecha_fin, $id = 0)
    {
        // $tipo = Input::get('tipo');
        // if ($tipo > 0 && $tipo != null) {
        //     $agenda = AgendaVirtualModel::where('id', $tipo)->first();
        //     if ($agenda) {
        //         $fecha_inicio = $agenda->fecha_inicio;
        //         $fecha_fin = $agenda->fecha_fin;
        //     }
        // }
        // // $fecha_inicio=Input::get('fecha_inicio');
        // // $fecha_fin=Input::get('fecha_fin');
        // // dd(Input::all());

        $fecha_inicio = date('Y-m-d H:i:s', strtotime($fecha_inicio));
        $fecha_fin = date('Y-m-d H:i:s', strtotime($fecha_fin));
        // $fecha_inicio = date('Y-m-d 00:00:00', strtotime($fecha_inicio));
        // $fecha_fin = date('Y-m-d 23:59:59', strtotime($fecha_fin));
        // // $fecha_fin=$agenda->fecha_fin;
        Log::info($fecha_inicio . '   ' . $fecha_fin);
        $eventos = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
            ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
            ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo as tipo_evento')
            ->where([['id_estado', '<>', 7], ['id_estado', '<>', 6], ['com_tmov_comunicacion_agenda.id', '<>', $id]])

            ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                $query->where('fecha_inicio', '<=', $fecha_inicio)
                    ->where('fecha_fin', '>=', $fecha_fin)

                    ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                    ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
            })

            ->get();
        Log::info($eventos);

        $actividades_collect = collect($eventos);
        $multiplied_delegados = $actividades_collect->map(function ($item, $key) use ($id_usuario) {
            $responsables_para_mostrar = [];
            if ($item->delegacion_alcalde != null) {
                $responsables_para_mostrar = DB::select('select id,name from users where id in (' . $item->delegacion_alcalde . ')');
                // dd($responsables_para_mostrar);
            }
            $bandera = false;
            $html = '';
            foreach ($responsables_para_mostrar as $k) {
                if ($id_usuario == $k->id) {
                    // show($responsables_para_mostrar);
                    $bandera = true;
                    $html .= '  ' . $k->name . "<br>";
                }
            }
            // $bandera = false;

            // show($html);
            return ["valor" => $bandera, "personas" => "<b> Delegado - " . $item->nombre . ": </b> " . $html . " <b><br>Inicio: </b> " . date('H:i', strtotime($item->fecha_inicio)) . " <b>Fin: </b> " . date('H:i', strtotime($item->fecha_fin))];
        });



        $eventos_ = AgendaVirtualModel::join('com_tma_estados_agenda as e', 'e.id', '=', 'com_tmov_comunicacion_agenda.id_estado')
            ->join('com_tma_tipo_agenda as t', 't.id', '=', 'com_tmov_comunicacion_agenda.id_tipo')
            ->join('com_tmo_notifiacion_personalizada as  np', 'np.id_agenda', '=', 'com_tmov_comunicacion_agenda.id')
            ->join('users as  u', 'u.id', '=', 'np.id_usuario')
            ->select('com_tmov_comunicacion_agenda.*', 'e.nombre as estado_aprobacion', 't.tipo as tipo_evento', 'u.name')
            ->where([['id_estado', '<>', 7], ['id_estado', '<>', 6], ['com_tmov_comunicacion_agenda.id', '<>', $id], ['np.id_usuario', '=', $id_usuario]])
            ->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                $query->where('fecha_inicio', '<=', $fecha_inicio)
                    ->where('fecha_fin', '>=', $fecha_fin)

                    ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                    ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
            })

            ->get();

        $actividades_collect_ = collect($eventos_);
        $multiplied_delegados_ = $actividades_collect_->map(function ($item, $key) {
            $bandera = true;
            return ["valor" => $bandera, "personas" => " <b>Notificado - " . $item->nombre . ": </b> " . $item->name . " <b><br>Inicio:</b> " . date('H:i', strtotime($item->fecha_inicio)) . " <b>Fin:</b> " . date('H:i', strtotime($item->fecha_fin))];
        });

        // dd($multiplied_delegados_);
        $multiplied_delegados = collect($multiplied_delegados)->merge($multiplied_delegados_);

        $datos = collect($multiplied_delegados)->where('valor', true);
        $datos_ = collect($multiplied_delegados)->where('valor', true)->first();
        if ($datos_) {
            // dd($datos);
            $personas = '   <table style="border: 1px solid black; font-size: 0.9em; width: 100%;">
            <tr>
              <th style="border: 1px solid black; font-size: 0.9em; color:#ffffff; background-color: #0e4194">Detalle</th>
            </tr>
         ';
            foreach ($datos as $key => $value) {
                $personas .= '<tr><td style="border: 1px solid black; font-size: 0.9em;">' . $value['personas'] . '</td></tr>';
                # code...

            }
            $personas .= ' </table>';
            return ['respuesta' => true, 'mensaje' => 'No se puede agregar a esta actividad porque ya se encuentra delegado en otra', "mensaje_app" => 'La(s) persona(s) encuentra(n) asignada en la actividad.<br><br> ' . $personas];
        } else {
            // return 0 ;
            return ['respuesta' => false, 'mensaje' => 'ok', "mensaje_app" => 'Ok'];
        }
    }




    public function eventosAgendaCulturalWeb()
    {
        $categoria = TurEventosCategoriasModel::all();
     
        $id_usuario = floatval(Input::get('id_usuario'));

        $collection2 = collect($categoria);
        $categoria = $collection2->map(function ($mul) {
            $eventos = TurEventosModel::select(
            'tur_agenda.id',
            'tur_agenda.id_categoria',
            'c.categoria',
            'tur_agenda.evento',
            'tur_agenda.descripcion',
            'tur_agenda.fecha_inicio',
            'tur_agenda.fecha_fin',
            'tur_agenda.direccion',
            'tur_agenda.longitud',
            'tur_agenda.latitud'
            )->join('tur_agenda_categoria as c', 'c.id', 'tur_agenda.id_categoria')
            ->where('id_categoria', '=', $mul->id);
            
            $fecha_inicio = Input::get('fecha_inicio');
            $fecha_fin = Input::get('fecha_fin');
            $id_evento = Input::get('id_evento');
            if ($fecha_inicio != null &&  $fecha_fin != null) {
                $eventos = $eventos->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where('fecha_inicio', '<=', $fecha_inicio)
                        ->where('fecha_fin', '>=', $fecha_fin)
                        ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                        ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
                });
            }
            if ($id_evento != null) {
                $eventos = $eventos->where('tur_agenda.id', $id_evento);
            }

            $eventos = $eventos->where('tur_agenda.estado', 'ACT')->orderBy('fecha_inicio', 'asc')->get();
            $collection2 = collect($eventos);
            $total = $collection2->count();
            $multimedia = $collection2->map(function ($mul)   {
                $carbon1 = new \Carbon\Carbon($mul->fecha_inicio);
                $carbon2 = new \Carbon\Carbon($mul->fecha_fin);
                $minutesDiff = $carbon1->diffForHumans($carbon2, true);
                $eventos = DB::select("SELECT CONCAT('https://sistemasic.manta.gob.ec/archivos_sistema/',ruta) AS ruta, tipo_archivo FROM tmov_archivos  WHERE id_referencia=" . $mul->id . " and tipo=20");
                
               
                return [
                    'id' => $mul->id,
                    'id_categoria' => $mul->id_categoria,
                    'categoria' => $mul->categoria,
                    'evento' => $mul->evento,
                    'descripcion' => $mul->descripcion,
                    'fecha_inicio' => $mul->fecha_inicio,
                    'fecha' =>  date('Y-m-d', strtotime($mul->fecha_inicio)),
                    'fecha_letras' => fechas(111, $mul->fecha_inicio),
                    'fecha_fin' => $mul->fecha_fin,
                    'inicio' => date('H:i a', strtotime($mul->fecha_inicio)),
                    'fin' => date('H:i a', strtotime($mul->fecha_fin)),
                    'diferencia' => $minutesDiff,
                    'direccion' => $mul->direccion,
                    'longitud' => $mul->longitud,
                    'latitud' => $mul->latitud,
                    'multimedia' => $eventos
                ];
            });

            return ['id_categoria' =>  $mul->id, 'categoria' =>  $mul->categoria, 'data'=>$multimedia];
            
        });



        foreach ($categoria as $key => $value) {
            if(count($value['data']) == 0){
                unset($categoria[$key]);
            }
        }
        return response()->json($categoria);
    }


    public function eventosAgendaCultural()
    {
        $id_usuario = floatval(Input::get('id_usuario'));
        $eventos = TurEventosModel::select(
            'tur_agenda.id',
            'tur_agenda.id_categoria',
            'c.categoria',
            'tur_agenda.evento',
            'tur_agenda.descripcion',
            'tur_agenda.fecha_inicio',
            'tur_agenda.fecha_fin',
            'tur_agenda.direccion',
            'tur_agenda.longitud',
            'tur_agenda.latitud'
        )->join('tur_agenda_categoria as c', 'c.id', 'tur_agenda.id_categoria');
        $fecha_inicio = Input::get('fecha_inicio');
        $fecha_fin = Input::get('fecha_fin');
        $id_evento = Input::get('id_evento');
        if ($fecha_inicio != null &&  $fecha_fin != null) {
            $eventos = $eventos->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                $query->where('fecha_inicio', '<=', $fecha_inicio)
                    ->where('fecha_fin', '>=', $fecha_fin)
                    ->orwhereBetween('fecha_inicio', array($fecha_inicio, $fecha_fin))
                    ->orwhereBetween('fecha_fin', array($fecha_inicio, $fecha_fin));
            });
        }
        if ($id_evento != null) {
            $eventos = $eventos->where('tur_agenda.id', $id_evento);
        }
        $eventos = $eventos->where('tur_agenda.estado', 'ACT')->orderBy('fecha_inicio', 'asc')->get();
        $collection2 = collect($eventos);
        $total = $collection2->count();
        $multimedia = $collection2->map(function ($mul)  use ($id_usuario) {
            $carbon1 = new \Carbon\Carbon($mul->fecha_inicio);
            $carbon2 = new \Carbon\Carbon($mul->fecha_fin);
            $minutesDiff = $carbon1->diffForHumans($carbon2, true);
            $eventos = DB::select("SELECT CONCAT('https://sistemasic.manta.gob.ec/archivos_sistema/',ruta) AS ruta, tipo_archivo FROM tmov_archivos  WHERE id_referencia=" . $mul->id . " and tipo=20");
            $reacciones = "";
            $array_reacciones = array();
            if (empty($id_usuario)) {
                $reacciones = collect([['id' => null, 'id_reacion' => null]]);
            } else {
                $reacciones = TurEventosReaccionesModel::select("id", "id_agenda", "id_reacion", "id_tipo_usuario", "id_usuario")->where(['id_agenda' => $mul->id, 'id_usuario' => $id_usuario, 'estado' => 'ACT'])->get();
            }
            if (!isset($reacciones[0])) {
                array_push($array_reacciones, ['id' => null, 'id_agenda' =>  $mul->id, 'id_reacion' => 1, 'id_usuario' => $id_usuario, 'id_tipo_usuario' => null]);
                array_push($array_reacciones, ['id' => null, 'id_agenda' =>  $mul->id, 'id_reacion' => 2, 'id_usuario' => $id_usuario, 'id_tipo_usuario' => null]);
            } else {
                $like = null;
                $interes = null;
                foreach ($reacciones as $key) {
                    if ($key['id_reacion'] == null) {
                    } elseif ($key['id_reacion'] == 1) {
                        $like = $key['id_reacion'];
                    } elseif ($key['id_reacion'] == 2) {
                        $interes = $key['id_reacion'];
                    } else {
                    }
                }
                if ($like == null) {
                    array_push($array_reacciones, ['id' => null, 'id_agenda' =>  $mul->id, 'id_reacion' => 1, 'id_usuario' => $id_usuario, 'id_tipo_usuario' => null]);
                } else {
                    foreach ($reacciones as $key) {
                        if ($key->id_reacion == 1) {
                            array_push($array_reacciones, $key);
                        }
                    }
                }
                if ($interes == null) {
                    array_push($array_reacciones, ['id' => null, 'id_agenda' =>  $mul->id, 'id_reacion' => 2, 'id_usuario' => $id_usuario, 'id_tipo_usuario' => null]);
                } else {
                    foreach ($reacciones as $key) {
                        if ($key->id_reacion == 2) {
                            array_push($array_reacciones, $key);
                        }
                    }
                }
            }
            $comentarios = TurEventosComentariosModel::where(['id_agenda' => $mul->id, 'estado' => 'ACT'])->get();
            return [
                'id' => $mul->id,
                'id_categoria' => $mul->id_categoria,
                'categoria' => $mul->categoria,
                'evento' => $mul->evento,
                'descripcion' => $mul->descripcion,
                'fecha_inicio' => $mul->fecha_inicio,
                'fecha' =>  date('Y-m-d', strtotime($mul->fecha_inicio)),
                'fecha_letras' => fechas(111, $mul->fecha_inicio),
                'fecha_fin' => $mul->fecha_fin,
                'inicio' => date('H:i a', strtotime($mul->fecha_inicio)),
                'fin' => date('H:i a', strtotime($mul->fecha_fin)),
                'diferencia' => $minutesDiff,
                'direccion' => $mul->direccion,
                'longitud' => $mul->longitud,
                'latitud' => $mul->latitud,
                'multimedia' => $eventos,
                'reacciones' => $array_reacciones,
                'comentarios' => $comentarios

            ];
        });
        return $this->paginate($multimedia, $total, 15);
    }



    

    protected static function paginator($items, $total, $perPage, $currentPage, $options)
    {
        return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
            'items',
            'total',
            'perPage',
            'currentPage',
            'options'
        ));
    }

    public static function paginate(Collection $results, $total, $pageSize)
    {
        $page = Paginator::resolveCurrentPage('page');

        return self::paginator($results->forPage($page, $pageSize), $total, $pageSize, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);
    }


    public function ingresarReaccionEvento()
    {
        try {
            //code...
            DB::beginTransaction();
            $id_reaccion = Input::get('id');
            $id_agenda = Input::get('id_agenda');
            $id_reacion = Input::get('id_reacion');
            $id_usuario = Input::get('id_usuario');
            $id_tipo_usuario = Input::get('id_tipo_usuario');
            $nombre_usuario = Input::get('nombre_usuario');

            if ($id_reaccion != null) {
                $c = TurEventosReaccionesModel::find($id_reaccion);

                $c->ip = \Request::getClientIp();
                $c->pc = \Request::getHost();
                $c->estado = 'INA';

                $c->save();
                DB::commit();
                return response()->json(['success', null], 200);
            } else {
                $c = new TurEventosReaccionesModel();
                $c->id_agenda = $id_agenda;
                $c->id_reacion = $id_reacion;
                $c->id_usuario = $id_usuario;
                $c->id_tipo_usuario = $id_tipo_usuario;
                $c->nombre_usuario = $nombre_usuario;
                $c->ip = \Request::getClientIp();
                $c->pc = \Request::getHost();
                $c->save();
                DB::commit();
                return response()->json(['success', $c->id, $c->id_reacion], 200);
            }
        } catch (\Throwable $th) {
            //throw $th;
            // return  $th->;
            DB::rollback();
            // return response()->json(['error'], 200,  $th->getMessage(), $th->getLine());
            return response()->json(['error' => $th->getMessage() . ' ' . $th->getLine()], 200);
        }
    }


    public function ingresarComentarioEvento()
    {

        try {
            //code...
            $id_comentario = Input::get('id');
            $id_agenda = Input::get('id_agenda');
            $comentario = Input::get('comentario');
            $id_usuario = Input::get('id_usuario');
            $id_tipo_usuario = Input::get('id_tipo_usuario');
            $nombre_usuario = Input::get('nombre_usuario');

            if ($id_reaccion != null) {
                $c = TurEventosComentariosModel::find($id_comentario);
            } else {
                $c = new TurEventosComentariosModel();
            }
            $c->id_agenda = $id_agenda;
            $c->comentario = $comentario;
            $c->id_usuario = $id_usuario;
            $c->id_tipo_usuario = $id_tipo_usuario;
            $c->nombre_usuario = $nombre_usuario;
            // $c->ip = $id_tipo_usuario;
            $c->ip = \Request::getClientIp();
            $c->pc = \Request::getHost();
            $c->save();

            return response()->json(['success' => 'Comentario agregado correctamente', $c->id], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['error' => $th->getMessage() . ' ' . $th->getLine()], 200);
        }
    }


    public function agregar_categoria()
    {

        try {
            //code...
            $categoria = Input::get('categoria');
            $item = TurEventosCategoriasModel::where('categoria', $categoria)->first();
            if ($item) {
                $datos = TurEventosCategoriasModel::get();
                return response()->json(["estado" => "error", "msg" => $categoria . ", ya se encuentra registrado", "datos" => $datos], 200);
            }
            $item = new TurEventosCategoriasModel();
            $item->categoria = $categoria;
            $item->save();
            $datos = TurEventosCategoriasModel::orderby('categoria', 'ASC')->get();

            return response()->json(["estado" => "ok", "msg" => "Agregado Exitosamente", "datos" => $datos, "seleccionado" => $item->id], 200);
        } catch (\Throwable $th) {
            $datos = TurEventosCategoriasModel::orderby('categoria', 'ASC')->get();
            //throw $th;
            return response()->json(["estado" => "error", "msg" => "No se pudo agregar", "datos" => $datos], 200);
        }
    }

    public function probarNotificaciones()
    {
        $notificacion = new NotificacionesController;
        $notificacion->notificacion(['58daed51-3f54-47a0-8df6-d325eb5da7c6'], 0, '🔔  Prueba de notificaciones', 'Prueba', 2, []);
    }
}
//http://sistemasti.manta.gob.ec/reportesagenda?token=Spd0QAKYsX9VWEEiw5lmMMFs2gF1uXGv1FGZ1AMs&data=[{%22name%22:%22_token%22,%22value%22:%22Spd0QAKYsX9VWEEiw5lmMMFs2gF1uXGv1FGZ1AMs%22},{%22name%22:%22IDDIRECCION%22,%22value%22:%220%22},{%22name%22:%22ESTADO%22,%22value%22:%220%22},{%22name%22:%22TIPO%22,%22value%22:%220%22},{%22name%22:%22namereporte%22,%22value%22:%22rpt_reportesagenda%22},{%22name%22:%22FECHAINICIO%22,%22value%22:%222020-01-01%22},{%22name%22:%22FECHAFINAL%22,%22value%22:%222020-12-31%22},{%22name%22:%22FECHAACTUDESDE%22,%22value%22:%222020-01-01%22},{%22name%22:%22FECHAACTUHASTA%22,%22value%22:%222020-12-31%22},{%22name%22:%22formato%22,%22value%22:%22pdf%22},{%22name%22:%22tiporeporte%22,%22value%22:%22completo%22},{%22name%22:%22tituloreporte%22,%22value%22:%22/%22}]&paramadi=eyJpdiI6IkFLVVN6cTZ1UHJLNHRhNGNlQmJRemc9PSIsInZhbHVlIjoiZkUzT2ZRMGRzOTdCYmdnK2I0cVNEQ1VubzVOU1I3RWtRektyQnRuZ1wvMVwvNUk5NnFBbUhxQndrc3BoYm84VjhwMWhSNEt1OGJPU1RPVnFXVHdBaCtNb2NpK0Q4K20yS1hsQkNsaEttNmZ1UFBnVXFrVm1ORGJNcW5SbmhvMmw2eUxUZXhwM2krZGYrdXk4bXQzMUZEMHc9PSIsIm1hYyI6IjZjZGJjMDZmOTMwYTJkNmY3MmZmMWQyM2IyOGRmMTYwOTFiMTRmZmRiMzk2NmUxYTExMWVkOGE3NzI0M2VmZjcifQ==
