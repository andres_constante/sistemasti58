<?php

namespace App\Http\Controllers;

use App\ArchivosModel;
use App\User;
use App\UsuariosModel;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\TramitesAlcaldia\Entities\AnexosModel;
use Modules\TramitesAlcaldia\Entities\AreasModel;
use Modules\TramitesAlcaldia\Entities\TipoTramiteModel;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaAsiganrModel;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabDetaModel;
use Modules\TramitesAlcaldia\Entities\TramiteInternoModel;
use Modules\TramitesAlcaldia\Http\Controllers\TramAlcaldiaProcesosController;
use Yajra\DataTables\Facades\DataTables;

class WebApiTramitesController extends Controller
{
    protected $notificar;

    public function __construct(NotificacionesController $notificar)
    {
        $this->notificar = $notificar;
    }


    public function crearSolicitud(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $numtramite = $this->numTramite('PM');

            $parroquia = parroquiaModel::where(['parroquia' => $request->parroquia])->first();
            $barrio = barrioModel::where(['barrio' => $request->barrio])->first();
            $id_externo = isset($request->id_usuario) ? $request->id_usuario : null;

            if ($request->has('direccion_municipal') && $request->direccion_municipal != null) {
                $direccionAtender = AreasModel::where(['area' => $request->direccion_municipal])->first();
                $tipoTramite = TipoTramiteModel::where([
                    'id_area' => $direccionAtender->id,
                    'valor' => $request->tipo_tramite
                ])->first();

                $solicitud = TramAlcaldiaCabModel::create([
                    'numtramite' => $numtramite,
                    'remitente' => $request->remitente,
                    'asunto' => $request->asunto,
                    'peticion' => $request->peticion,
                    'fecha_ingreso' => date('Y-m-d'),
                    'cedula_remitente' => $request->cedula_remitente,
                    'correo_electronico' => $request->correo_electronico,
                    'telefono' => $request->telefono,
                    'telefono_2' => $request->telefono_2,
                    'direccion' => $request->direccion,
                    'id_parroquia' => $parroquia->id,
                    'id_barrio' => $barrio->id,
                    'tipo' => $request->tipo,
                    'id_area' => $direccionAtender->id,
                    'tipo_tramite' => $tipoTramite->id,
                    'direccion_atender' => $direccionAtender->id_direccion,
                    'id_usuario' => 1,
                    'id_usuario_externo' => $id_externo,
                ]);

                TramAlcaldiaAsiganrModel::create([
                    'id_cab' => $solicitud->id,
                    'id_direccion' => $direccionAtender->id_direccion,
                    'estado' => 'PENDIENTE'
                ]);

                if ($direccionAtender->id_direccion == 6) {
                    $director = UsuariosModel::where(['id_perfil' => 48, 'id_direccion' => $direccionAtender->id_direccion])->first();
                } else {
                    $director = UsuariosModel::directores()->where('users.id_direccion', $direccionAtender->id_direccion)->first();
                }

                $ruta = 'tramitesalcaldia/finalizartramite?id=' . $solicitud->id;
                $mensaje = 'Se le ha asignado el trámite N° ' . $solicitud->numtramite;
                if (isset($director)) {
                    $this->notificar->notificacionesweb($mensaje . '', $ruta, $director->id, '2c438f');
                    $this->notificar->EnviarEmail($director->email, 'Trámite Asignado', '', $mensaje, $ruta);
                }
            }
            else
            {    
                $numtramite = $this->numTramite('TE');
                $solicitud = TramAlcaldiaCabModel::create([
                    'numtramite' => $numtramite,
                    'remitente' => $request->remitente,
                    'asunto' => $request->asunto,
                    'peticion' => $request->peticion,
                    'fecha_ingreso' => date('Y-m-d'),
                    'cedula_remitente' => $request->cedula_remitente,
                    'correo_electronico' => $request->correo_electronico,
                    'telefono' => $request->telefono,
                    'telefono_2' => $request->telefono_2,
                    'direccion' => $request->direccion,
                    'id_parroquia' => $parroquia->id,
                    'id_barrio' => $barrio->id,
                    'tipo' => $request->tipo,
                    'id_usuario' => 1,
                    'disposicion' => 'REVISION',
                    'id_usuario_externo' => $id_externo,
                ]);

                // $ventanilla = User::where(['id_perfil' => 20, 'id_direccion' => 25])->get();
                // $ruta = 'tramitesalcaldia/tramites' . $solicitud->id . '/edit';
                // $mensaje = 'Se ha ingresado un nuevo tramite N° ' . $solicitud->numtramite;
                // foreach($ventanilla as $v)
                // {
                //     $this->notificar->notificacionesweb($mensaje . '', $ruta, $v->id, '2c438f');
                // }
            }

            $dir = public_path() . '/archivos_sistema/';

            if ($request->documento_pdf) {
                $base64File = $request->documento_pdf;
                $file = base64ToFilePdf($base64File);
                $docs = $file;

                if ($docs) {
                    $fileName = "tramite-$solicitud->id-" . date("YmdHis") . "." . $docs->getClientOriginalExtension();
                    $file = TramAlcaldiaCabModel::find($solicitud->id);
                    $file->archivo = $fileName;
                    $file->save();
                    $docs->move($dir, $fileName);

                    $upload = new ArchivosModel;
                    $upload->tipo = 10;
                    $upload->id_usuario = 1;
                    $upload->id_referencia = $solicitud->id;
                    $upload->nombre = basename($docs->getClientOriginalName());
                    $upload->tipo_archivo = $docs->getClientOriginalExtension();
                    $upload->ruta = $fileName;
                    $upload->save();
                }
            }

            if ($request->anexos_pdf != 'null' && $request->anexos_pdf != null) {
                $archivos = json_decode($request->anexos_pdf);
                foreach ($archivos as $key => $value) {
                    $base64File = $value->archivo;
                    $archivo = base64ToFilePdf($base64File);

                    $name = 'Archivo_Ref_' . $solicitud->id . '_tipo_10_' . sha1(date('YmdHis') . Str::random(10));
                    $resize_name = $name . Str::random(2) . '.' . $archivo->getClientOriginalExtension();

                    $archivo->move($dir, $resize_name);

                    $upload = new AnexosModel;
                    $upload->usuario_id = 1;
                    $upload->id_referencia = $solicitud->id;
                    $upload->ruta = $resize_name;
                    $upload->nombre = basename($archivo->getClientOriginalName());
                    $upload->hojas = $value->hojas;
                    $upload->descripcion = $value->descripcion;
                    $upload->tipo = 10;
                    $upload->save();
                }
            }

            $this->guardarTimeline($solicitud->id, $solicitud, 'Solicitud ingresada satisfactoriamente');

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Solicitud guardada correctamente',
                'numtramite' => $solicitud->numtramite,
                'id' => $solicitud->id
            ]);
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo guardar la solicitud',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function numTramite($tipo)
    {
        $hoy = date('dmYHi');
        $numtramite = $tipo . $hoy;
        $existe = TramAlcaldiaCabModel::where(['numtramite' => $numtramite])->first();
        
        if($existe)
        {
            $future = strtotime('now + 1 minute');
            $hoy = date('dmYHi', $future);
            $numtramite = $tipo . $hoy;
        }

        return $numtramite;
    }

    public function guardarTimeline($id, $data, $observacion = null, $estado = 0, $disposicion = null): int
    {
        $timeline = new TramAlcaldiaCabDetaModel;
        $timeline->numtramite = $data->numtramite;
        // $timeline->recomendacion = 'Solicitud ingresada satisfactoriamente';
        $timeline->observacion = $observacion;
        $timeline->remitente = $data->remitente;
        $timeline->asunto = $data->asunto;
        $timeline->peticion = $data->peticion;
        $timeline->prioridad = $data->prioridad;
        $timeline->direccion_atender = $data->direccion_atender;
        $timeline->fecha_fin = $data->fecha_fin;
        $timeline->estado_proceso = $estado;
        $timeline->disposicion = $disposicion == null ? $data->disposicion : $disposicion;
        // $timeline->observacion = $data->observacion;
        $timeline->id_usuario = 1;
        $timeline->ip = request()->ip();
        $timeline->pc = request()->getHttpHost();
        $timeline->id_tram_cab = $id;
        $timeline->archivo = $data->archivo;
        $timeline->fecha_ingreso = $data->fecha_ingreso;
        if ($observacion == 'Trámite en proceso de gestión') {
            $timeline->fecha_respuesta = now();
        }
        $timeline->save();

        return $timeline->id;
    }


    // tramitesExternosSolitadosajax

    public function tramitesExternosSolitadosajax(Request $request)
    {

        $columns = array(
            0 => 'numtramite',
            2 => 'tipo_tramite',
            2 => 'cedula_remitente',
            2 => 'asunto',
            3 => 'peticion',
            1 => 'origen',
            5 => 'disposicion',
            6 => 'fecha_ingreso',
            7 => 'fecha_respuesta'
        );
        $id_usuario_ajax = Input::get('id_usuario_ajax');
        $query = TramAlcaldiaCabModel::with('area', 'tipoTramite')->where('tipo', 'TRAMITES MUNICIPALES')->orderBy('created_at', 'DESC');

        if (empty($request->input('search.value'))) {
            if ($id_usuario_ajax != null) {
                // $query = $query->where('id_usuario_externo', $id_usuario_ajax);
            }
        }

        $totalData = $query->get()->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();
        } else {
            $search = $request->input('search.value');
            $posts = $query->where(function ($query) use ($search) {

                $query->where('numtramite', 'LIKE', "%{$search}%")
                    ->orWhere('remitente', 'LIKE', "%{$search}%")
                    ->orWhere('cedula_remitente', 'LIKE', "%{$search}%");
            })->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $totalFiltered = $query->where(function ($query) use ($search) {
                $query->where('numtramite', 'LIKE', "%{$search}%")
                    ->orWhere('remitente', 'LIKE', "%{$search}%")
                    ->orWhere('cedula_remitente', 'LIKE', "%{$search}%");
            })->get()->count();
        }

        $data = array();
        if (!empty($posts)) {
            $usuarios = [
                28107 => "CABRERA CEDEÑO ADRIANA MARIELA",
                28105 => "TRIVIÑO ZAMBRANO RAUL EDUARDO",
                28104 => "ECHEVERRIA NAVARRETE JONATHAN DANIEL",
                28101 => "SOLORZANO GARCIA ANA GABRIELA",
                28109 => "BARBERAN CEDEÑO VANESSA DEL CARMEN"
            ];
            foreach ($posts as $k => $post) {
                $acciones = '';

                // dd($post);
                // case when (cab.tipo = 'VENTANILLA' || cab.id_usuario_externo IS NOT NULL) THEN 'VENTANILLA' ELSE 'PORTAL' END AS origen

                if ($post->id_usuario_externo == null && $post->creado_por == null) {
                    // dd('ss');
                }
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['origen'] = (($post->tipo = 'VENTANILLA' ||  $post->id_usuario_externo != null) ? (($post->id_usuario_externo == null && $post->creado_por == null) ? 'PORTAL' : 'VENTANILLA') : 'PORTAL') . '<br>' . ((isset($usuarios[$post->id_usuario_externo])) ? '<b>' . $usuarios[$post->id_usuario_externo] . '</b>' : '-');
                $nestedData['remitente'] = $post->remitente;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['cedula_remitente'] = $post->cedula_remitente;
                $nestedData['peticion'] = $post->peticion;
                $nestedData['disposicion'] = $post->disposicion;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['fecha_respuesta'] = $post->fecha_respuesta;
                $nestedData['tipo_tramite'] = (isset($post->tipoTramite->valor)) ? $post->tipoTramite->valor : '';
                $nestedData['acciones'] = '----';


                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }




    public function validarTramiteExterno()
    {
        # code...
        try {
            $bandera=true;
            $tramite = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'numtramite' => Input::get('numtramite')])->first();
            if($tramite){
               $bandera=false; 
            }
            $tramite = TramiteInternoModel::where(['numtramite' => Input::get('numtramite')])->first();
            
            if($tramite){
               $bandera=false; 
            }

            if ($bandera) {
                return response()->json(['estado' => false]);
            }
            return response()->json(['estado' => true]);
        } catch (\Throwable $th) {
            return response()->json(['estado' => false]);
            //throw $th;
        }
    }


    public function verfunciones()
    {

        $disabled = explode(',', ini_get('disable_functions'));
        dd($disabled);
        # code...
    }
}
