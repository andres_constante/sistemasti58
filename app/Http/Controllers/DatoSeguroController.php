<?php

namespace App\Http\Controllers;

use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\Auth;
use stdClass;

class DatoSeguroController extends Controller
{
    //
    var $configuraciongeneral = array("Dato Seguro", "datoseguroconsultar", "index", 6 => '');


    var $objetos = '[ {"Tipo":"text","Descripcion":"Cédula / R.U.C","Nombre":"documento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
                    {"Tipo":"select","Descripcion":"Tipo de Búsqueda","Nombre":"tipo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
                  ]';
    var $escoja = array(null => "Escoja opción...");
    //var $index
    //https://jqueryvalidation.org/validate/
    var $validarjs = array();
    public function __construct()
    {
        // $this->middleware('auth','excepct'=>['consultardatosseguro_data']);
        $this->middleware('auth', ['except' => ['consultardatosseguro_data']]);
    }
    public function index()
    {
        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);
        $opcion = array(
            1015 => "Cédula",
            1021 => "R.U.C"
        );
        $objetos[1]->Valor = $opcion;
        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs
        ]);
    }
    public function create()
    {
        return $this->index();
    }
    public function procesar($ajax = "no")
    {
        if (!Input::has("documento"))
            return $this->index();
        $input = Input::all();
        //show($input);
        $ruta = $this->configuraciongeneral[1];
        $require = array(
            "documento" => "required|numeric"
        );
        $validator = Validator::make($input, $require);

        if ($validator->fails()) {
            //die($ruta);
            if ($ajax == "si")
                return array("estado" => "error", "mensaje" => "No se pudo procesar");
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            $documento = Input::get("documento");
            $tipo = Input::get("tipo");
            $url = "http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/$documento/$tipo";
            $client = new GuzzleHttpClient();
            try {
                $respuesta = $client->request('GET', $url);
                $consultawebserviceobs = json_decode($respuesta->getBody()->getContents(), true);
            } catch (\Exception $e) {
                //return array("error",$e->getMessage());
                return redirect()->back()->withErrors(["message" => $e->getMessage()]);
            }
            //show($consultawebserviceobs);
            $consultawebserviceobs = $consultawebserviceobs[0];
            $objetos = array();
            $ajaxtxt = "<p><table class='table table-bordered table-hover'>";
            foreach ($consultawebserviceobs as $key => $value) {
                //show($key, 0);
                $stc = new stdClass();
                $stc->Tipo = "html2";
                $stc->Descripcion = $key;
                $stc->Nombre = $key;
                $stc->Clase = "Null";
                $stc->Valor = $value;
                $stc->ValorAnterior = "";
                $objetos[] = $stc;
                $ajaxtxt .= "<tr><td  width='40px'><strong>$key</strong>:</td><td>$value</td></tr>";
            }
            $ajaxtxt .= "</table></p>";
            //$objetos=json_decode($this->objetos);
            //show($objetos);
            $this->configuraciongeneral[2] = "crear";
            if ($ajax == "si") {
                return $ajaxtxt;
            }
            return view('vistas.create', [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            ]);
        }
    }
    public function store()
    {
        return $this->procesar();
    }
    public function consultardatosseguro()
    {
        return $this->procesar("si");
    }
    public function consultardatosseguro_data()
    {
        $documento = Input::get("documento");
        $tipo = Input::get("tipo");
        $url = "http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/$documento/$tipo";
        $client = new GuzzleHttpClient();
        try {
            $respuesta = $client->request('GET', $url);
            $consultawebserviceobs = json_decode($respuesta->getBody()->getContents(), true);
            if (isset($consultawebserviceobs[0]['nombre'])) {
                // $fechaNacimiento = date('Y-m-d', strtotime($consultawebserviceobs[0]['fechaNacimiento']));
                $fecha = explode('/', $consultawebserviceobs[0]['fechaNacimiento']);
                $f = '';
                if (isset($fecha[0]) && isset($fecha[1]) && isset($fecha[2])) {
                    $f = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                }
                $edad = fechas(1100, $f);
                // dd($consultawebserviceobs);
                $tercera = '';
                $dis = '';
                $consultawebserviceobs[0]['fecha_nacimiento'] = $f;
                $consultawebserviceobs[0]['edad'] = $edad;
                if ($edad >= 65) {
                    $tercera = 'SI';
                } else {
                    $tercera = 'NO';
                }
                // strpos($consultawebserviceobs[0]['condicionCiudadano'], 'DISCA')
                if (strpos($consultawebserviceobs[0]['condicionCiudadano'], 'DISCA') !== false) {
                    $dis = 'SI';
                } else {
                    $dis = 'NO';
                }
                // dd($consultawebserviceobs[0]['condicionCiudadano']);
                // dd(stripos($consultawebserviceobs[0]['condicionCiudadano'], 'D'));
                $users_sistema = UsuariosModel::where('cedula', $documento)->first();
                if ($users_sistema) {
                    $consultawebserviceobs[0]['email'] = $users_sistema->email;
                    $consultawebserviceobs[0]['cargo'] = $users_sistema->cargo;
                    $consultawebserviceobs[0]['id_direccion'] = $users_sistema->id_direccion;
                }
                $consultawebserviceobs[0]['tercera'] = $tercera;
                $consultawebserviceobs[0]['dicapacitado'] = $dis;
            }

            return $consultawebserviceobs;
        } catch (\Exception $e) {
            return array("error", $e->getMessage());
            return redirect()->back()->withErrors(["message" => $e->getMessage()]);
        }
    }
    public function consultardatosseguro_ruc()
    {
        $documento = Input::get("documento");
        $tipo = Input::get("tipo");
        $url = "http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/$documento/$tipo";
        $client = new GuzzleHttpClient();
        try {
            $respuesta = $client->request('GET', $url);
            $consultawebserviceobs = json_decode($respuesta->getBody()->getContents(), true);
            return $consultawebserviceobs;
        } catch (\Exception $e) {
            //return array("error",$e->getMessage());
            return redirect()->back()->withErrors(["message" => $e->getMessage()]);
        }
    }
}
