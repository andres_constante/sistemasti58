<?php

namespace App\Http\Controllers;

use App\DonaDonacionesModel;
use App\DonaIngresoDonacionArchivoModel;
use App\DonaIngresoDonacionDetaModel;
use App\DonaIngresoDonacionModel;
use App\DonaIngresoDonacionProductoModel;
use App\DonaIngresoProductoModel;
use App\UsuariosModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use stdClass;

class DonaIngresoDonacionesController extends Controller
{
    var $configuraciongeneral = array("INGRESO DE DONACIONES", "coordinacioncronograma/ingeso_donaciones", "index", 6 => "coordinacioncronograma/ingresodonacionesajax", 7 => "ingeso_donaciones");
    var $escoja = array(null => "Escoja opción...");
    var $estados = [
        'PENDIENTE' => 'PENDIENTE',
        'ENTREGADO' => 'ENTREGADO',
        'ARCHIVADO' => 'ARCHIVADO'

    ];
    var $estados_producto = "['BUENO','DAÑADO','CADUCADO']";
    var $unidades = "['CAJA','GALÓN','LIBRA','QUINTAL','UNIDAD']";
    var $objetos = '[
        {"Tipo":"datetimetext","Descripcion":"Fecha de ingreso (*)","Nombre":"fecha_ingreso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Cédula / RUC (*)","Nombre":"cedula_razon","Clase":" mayuscula cedularuc","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Nombres / Razón social (*)","Nombre":"nombre_razon","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Producto","Nombre":"producto","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"htmlplantilla","Descripcion":"Detalle productos","Nombre":"donaciones","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"htmlplantilla","Descripcion":"Documentación","Nombre":"documentacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Observaciones","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
    //https://jqueryvalidation.org/validate/
    var $validarjs = array(
        "nombre_beneficiario" => "nombre_beneficiario: {
                            required: true
                        }",
        "cedula_beneficiario" => "cedula_beneficiario: {
                            required: true
                        }",
        "donacion" => "donacion: {
                            required: true
                        }",
        "direccion" => "direccion: {
                            required: true
                        }",
        // "telefono" => "telefono: {
        //                     required: true
        //                 }",
        // "observacion" => "observacion: {
        //                     required: true
        //                 }",

    );

    var $filtros = '[
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Cedula beneficiario","Nombre":"cedula_beneficiario","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function verpermisos($objetos = array(), $tipo = "index")
    {
        // show(Input::all());
        // if(){

        // }
        $tabla = $tabla = DonaIngresoDonacionModel::from('dona_tmov_ingreso_donacion as d')
            ->select('d.*', 'u.name')
            ->join("users as u", "u.id", "=", "d.id_usuario")
            ->where("d.estado", "ACT");
        return array($objetos, $tabla);
    }
    public function perfil()
    {
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4) {
            // dd($id_tipo_pefil->tipo);
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'NO';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
        }
        return $object;
    }
    public function index()
    {
        //
        $tabla = [];
        $objetos = json_decode($this->objetos);
        unset($objetos[3]);
        unset($objetos[4]);
        unset($objetos[5]);

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "create" => "si"
        ]);
    }

    public function ingresodonacionesajax(Request $request)
    {
        $objetos = json_decode($this->objetos);
        $tabla = $this->verpermisos($objetos);
        $objetos = $tabla[0];
        $tabla  = $tabla[1];
        $columns = array();
        $columns[] = 'id';
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        $id_tipo_pefil = UsuariosModel::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'id_direccion')->where("users.id", Auth::user()->id)->first();
        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('dona_tmov_ingreso_donacion.cedula_razon', 'LIKE', "%{$search}%")
                    ->orWhere('dona_tmov_ingreso_donacion.nombre_razon', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {
                $permisos = $this->perfil();

                $acciones = '';
                if (Auth::user()->id_perfil == 1) {
                    $acciones .= '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash  btn btn-danger"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                    <input name="_token" type="hidden" value="' . csrf_token() . '">
                    <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                }
                // dd($post);
                $acciones .= link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', ' EDITAR', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o btn btn-info'));

                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                $nestedData['id_donacion'] = $post->nombre;
                $nestedData['barrio_id'] = $post->barrio;
                $nestedData['parroquia_id'] = $post->parroquia;
                // $nestedData['usuario_registra'] =;
                $dona = DonaIngresoDonacionDetaModel::join('users as u', 'u.id', 'id_usuario')
                    ->select('dona_tmov_ingreso_donacion_deta.*', 'name')
                    ->where('id_ingreso_cab', $post->id)->orderBy('dona_tmov_ingreso_donacion_deta.created_at', 'DESC')->first();
                $nestedData['acciones'] =  $acciones . ' <br> <b>Usuario registra: </b>' . $post->name . ' <br><b>Modificado por: </b>' . $dona->name . ' - ' . $dona->created_at;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    public function formularioscrear($id)
    {
        /*QUITAR CAMPOS*/
        $objetos = json_decode($this->objetos);

        $objetos = $this->verpermisos($objetos, "crear");

        $objetos[0][0]->Valor = date('Y-m-d H:m');
        // $objetos[0][0]->Tipo = 'text';
        $objetos[0][1]->Adicional = ' <label  for="anonimo"><input type="checkbox" id="anonimo" name="anonimo">  ANÓNIMO </label>';

        $objetos[0][3]->Valor = $this->escoja + DonaIngresoProductoModel::where('estado', 'ACT')->pluck('producto', 'id')->all();
        $objetos[0][3]->Adicional = '<button  onClick="agregar_producto()" class="btn btn-success" type="button"><i class="fa fa-location-arrow" aria-hidden="true"></i> Nuevo </button>';
        $objetos[0][4]->Valor = '<script>
        var n_filas_producto=0;
        $(document).ready(function () {
            // $("#fecha_ingreso").attr("readonly",true);
            $("#producto").change(function (e) {
                if(this.value!=""){
                    var unidades=' . $this->unidades . ';
                    var txtSelect="";
                    $.each(unidades, function(key, data) {
                        txtSelect+=\'<option value="\'+data+\'">\'+data+\'</option>\';
                        
                    });
                    var estados=' . $this->estados_producto . ';
                    var estadoSelect="";
                    $.each(estados, function(key, d) {
                        estadoSelect+=\'<option value="\'+d+\'" selected >\'+d+\'</option>\';
                    });

                    var valor=this.value;
                    var texto=$("#producto option:selected").text();
                    $("#tabla_productos tbody").prepend(\'<tr id="fila_contacto_\'+n_filas_producto+\'" ><td>\'+texto+\'<input hidden  type="number" name="producto[]" value="\'+valor+\'"></td> <td><select class="form-control" name="unidad[]">\'+txtSelect+\'</select></td><td><input type="number" name="cantidad[]" class="form-control solonumeros"></td> <td><select name="estado[]" class="form-control">\'+estadoSelect+\'</select></td><td><textarea name="observacion_donacion[]" class="form-control"></textarea></td> <td><i onclick="borrar_producto(\'+n_filas_producto+\')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
                    n_filas_producto++;
                }   
            });
            var ruta_productos= "' . URL::to('coordinacioncronograma/getProductos') . '?id=' . $id . '";
            $.get(ruta_productos, function(data) {
                $.each(data, function(key, data) {
                    var unidades=' . $this->unidades . ';
                    var estado=' . $this->estados_producto . ';
                    var txtSelect="";
                    var estadoSelect="";
                    $.each(unidades, function(key, d) {
                        if(data.unidad==d){
                            txtSelect+=\'<option value="\'+d+\'" selected >\'+d+\'</option>\';
                        }
                    });
                    $.each(estado, function(key, d) {
                        if(data.estado_producto==d){
                            estadoSelect+=\'<option value="\'+d+\'" selected >\'+d+\'</option>\';
                        }else{
                            estadoSelect+=\'<option value="\'+d+\'" >\'+d+\'</option>\';
                        }
                    });
                    var obs=data.observacion;
                    if(obs==null){
                        obs=" ";
                    }    
                    $("#tabla_productos tbody").prepend(\'<tr id="fila_contacto_\'+n_filas_producto+\'" ><td>\'+data.producto+\'<input hidden  type="number" name="producto[]" value="\'+data.id_producto+\'"></td> <td><select class="form-control" name="unidad[]">\'+txtSelect+\'</select></td><td  style="width: 120px;"><input type="number" value="\'+data.cantidad+\'" name="cantidad[]" class="form-control solonumeros"></td> <td><select name="estado[]" class="form-control">\'+estadoSelect+\'</select></td><td><textarea name="observacion_donacion[]"  class="form-control">\'+obs+\'</textarea></td> <td><i onclick="borrar_producto(\'+n_filas_producto+\')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
                    n_filas_producto++;
                });
            });
           
        });


        function agregar_producto(){
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: "btn btn-success",
                    cancelButton: "btn btn-danger",
                    //  container: "sweet_containerImportant",
                    popup:"sweet_popupImportant"
                    
                },
                buttonsStyling: false
            });
      
            swalWithBootstrapButtons.fire({
                    title: "Ingrese el nombre del procucto",
                    input: "text",
                    inputAttributes: {
                    autocapitalize: "off"
                    },
                    showCancelButton: true,
                    confirmButtonText: "Agregar!",
                    cancelButtonText: "No, cancelar",
                    showLoaderOnConfirm: true,
                    //customClass:"swal-height-show-agenda_2",
                    preConfirm: (obs) => {
                        $.ajax({
                            type: "POST",
                            data: {
                                "_token": "' . csrf_token() . '",
                                "producto": obs,
                            },
                            url: "' . URL::to('') . '/agregar_producto",
                            success: function (data) {
                                try {
      
                                    if (data["estado"] == "ok") {
                                        Swal.fire({
                                            position: "center",
                                            type: "success",
                                            title: data["msg"],
                                            showConfirmButton: false,
                                            timer: 1500
                                        });
                                               $("#producto").empty();
                                              $.each(data["datos"], function(key, element) {
                                                $("#producto").append("<option value=\"" + element.id + "\">" + element.producto+"</option>");
                                              });
                                              $("#producto").trigger("chosen:updated");
                                              $("#producto").val(data["seleccionado"]);
                                              $("#producto").trigger("chosen:updated");
                                              $("#producto").trigger("change");

                                            
      
                                    } else {
                                        Swal.fire({
                                            type: "error",
                                            title: data["msg"]
                                        });
                                        $("#producto").empty();
                                              $.each(data["datos"], function(key, element) {
                                                $("#producto").append("<option value=\'" + element.id + "\'>" + element.producto+"</option>");
                                              });
                                              $("#producto").trigger("chosen:updated");
                                    }
      
                                } catch (error) {
      
                                }
      
                            },
                            error: function (data) {
                                //swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                            }
                        });
                    }
              });


        }

        function borrar_producto(id){

            $("#fila_contacto_"+id).remove();
        }
    
    
        
        
        </script>
        <br>
        <br>
        <div class="table-responsive">
        <br>
            <table class="table table-striped" id="tabla_productos">
            <thead>
                <tr>
                <th scope="col">Producto</th>
                <th scope="col">Unidad</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Estado</th>
                <th scope="col">Observación</th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
        <br>
        
        
        ';


        $objetos[0][5]->Valor = '<script>
        var n_filas_arr=0;
        $(document).ready(function () {
            $("#add_documentacion").click(function (e) {
                $("#tabla_documento tbody").prepend(\'<tr id="fila_ar\'+n_filas_arr+\'" ><td><input name="detalle_archivo[]" type="text"></td><td><input type="file" name="archivo_donacion[]" id="archivo_\'+n_filas_arr+\'" class="valores"></td><td></td> <td><input hidden name="archivo_nuevos[]" value="0"><i onclick="borrar_archivo(\'+n_filas_arr+\',0)" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
                n_filas_arr++;
            });
            
            var ruta_archivos= "' . URL::to('coordinacioncronograma/getArchivosD') . '?id=' . $id . '";
            $.get(ruta_archivos, function(data) {
                $.each(data, function(key, data) {
                    
                    var a="";
                    if(data.tipo=="pdf"){
                        a = \'<a href="' . URL::to('') . '/archivos_sistema/\'+data.nombre+\'"  class="btn btn-success dropdown-toggle divpopup" target="_blank" onclick="popup(this)"><i class="fa fa-file-pdf-o"></i></a>\';
                    } else {
                        var d="\'"+data.nombre+"\'";
                        a = \'<a onclick="mostrarImagen_2(\'+d+\')" class= "btn btn-info dropdown-toggle"><i class="fa fa-picture-o"></i></a>\';
                    } 
                    $("#tabla_documento tbody").prepend(\'<tr id="fila_ar\'+n_filas_arr+\'" ><td>\'+data.descripcion+\'</td><td>\'+a+\'</td><td>\'+data.tipo+\'</td> <td><input hidden name="archivo_nuevos[]" value="\'+data.id+\'"><i onclick="borrar_archivo(\'+n_filas_arr+\',\'+data.id+\')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>\');
                    n_filas_arr++;
                });
            });
        });


        function borrar_archivo(id,tipo){
            $("#fila_ar"+id).remove();    
        }
    
        
        
        </script>
       
        <br>
        <br>
        <div style="">
        <button type="button" id="add_documentacion" class="btn btn-info"><i class="fa fa-plus-circle" style="font-size: 1.5em;" aria-hidden="true"></i> Agregar</button>
           <br>Solo se adminten docuementos en formato <b>pdf</b> y imágenes en formato <b>png, jpg, jpeg</b>
        </div>
        <div class="table-responsive">
        <br>
            <table class="table table-striped" id="tabla_documento">
            <thead>
                <tr>
                <th scope="col">Nombre / detalle </th>
                <th scope="col">Archivo</th>
                <th scope="col">Tipo</th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
        <br>
        
        
        ';



        if ($id == "") {
            $this->configuraciongeneral[2] = "crear";
            $datos = [
                "objetos" => $objetos[0],
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs

            ];
        } else {
            $this->configuraciongeneral[2] = "editar";
            $id = intval(Crypt::decrypt($id));
            $tabla = $objetos[1]->where("d.id", $id)->first();

            $objetos[0][1]->Tipo = 'textdisabled';


            $datos = [
                "tabla" => $tabla,
                "objetos" => $objetos[0],
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "autoLocalizar" => "SI"
            ];
        }
        return view('vistas.create', $datos);
    }

    public function create()
    {
        return $this->formularioscrear("");
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

        //return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
        //return view('coordinacioncronograma::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $tabla = DonaDonacionesModel::find($id);
        //->update(array('estado' => 'INACTIVO'));
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
    public function getProductos()
    {
        // 
        $id = Input::get('id');
        try {
            $id = Crypt::decrypt($id);
        } catch (\Exception $e) {
        }
        $tabla = DonaIngresoDonacionProductoModel::join('dona_tma_ingreso_producto as p', 'p.id', 'id_producto')
            ->where(['id_ingreso' => $id, 'dona_tmov_ingreso_donacion_producto.estado' => 'ACT'])->get();
        return $tabla;
    }
    public function getArchivosD()
    {
        // 
        $id = Input::get('id');
        try {
            $id = Crypt::decrypt($id);
        } catch (\Exception $e) {
        }
        $tabla = DonaIngresoDonacionArchivoModel::where(['id_ingreso' => $id, 'estado' => 'ACT'])->get();
        return $tabla;
    }


    public function guardar($id)
    {
        $input = Input::all();
        // show($input);

        $ruta = $this->configuraciongeneral[1];

        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new DonaIngresoDonacionModel();
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro de Datos";
        } else {
            $ruta .= "/" . Crypt::encrypt($id) . "/edit";
            $guardar = DonaIngresoDonacionModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Actualización de Datos";
        }

        $input = Input::all();
        // show($input);
        $arrapas = array();
        $validator = Validator::make($input, DonaIngresoDonacionModel::rules($id));

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            DB::beginTransaction();
            try {
                $timeline = new DonaIngresoDonacionDetaModel();
                foreach ($input as $key => $value) {

                    if (
                        $key != "_method" &&
                        $key != "_token" &&
                        $key != "producto" &&
                        $key != "unidad" &&
                        $key != "cantidad" &&
                        $key != "estado" &&
                        $key != "observacion_donacion" &&
                        $key != "archivo_donacion" &&
                        $key != "archivo_nuevos" &&
                        $key != "anonimo" &&
                        $key != "detalle_archivo"
                    ) {
                        $guardar->$key = $value;
                        $timeline->$key = $value;
                    }
                }
                if ($id == 0) {
                    $guardar->id_usuario = Auth::user()->id;
                }
                $guardar->save();

                $id = $guardar->id;
                $idcab = $guardar->id;
                $timeline->id_usuario = Auth::user()->id;
                $timeline->id_ingreso_cab = $idcab;
                $timeline->save();

                ///////// productos
                if (Input::has("producto")) {
                    DonaIngresoDonacionProductoModel::where('id_ingreso', $idcab)->update([
                        'estado' => 'INA'
                    ]);
                    $array_producto = Input::get("producto");
                    $array_unidad = Input::get("unidad");
                    $array_cantidad = Input::get("cantidad");
                    $array_estado = Input::get("estado");
                    $array_observacion = Input::get("observacion_donacion");
                    if (is_array($array_producto)) {
                        foreach ($array_producto as $key => $value) {
                            $item = new DonaIngresoDonacionProductoModel();
                            $item->id_producto =  $value;
                            $item->id_ingreso = $idcab;
                            $item->cantidad = $array_cantidad[$key];
                            $item->unidad = $array_unidad[$key];
                            $item->observacion = $array_observacion[$key];
                            $item->estado_producto =  $array_estado[$key];
                            $item->estado = 'ACT';
                            $item->save();
                        }
                    }
                }

                ////archivos
                $archivo_nuevos = Input::get('archivo_nuevos');
                // dd($archivo_nuevos);
                if (is_array($archivo_nuevos)) {
                    DonaIngresoDonacionArchivoModel::where('id_ingreso', $idcab)
                        ->wherenotIN('id', $archivo_nuevos)->update([
                            'estado' => 'INA'
                        ]);
                }


                if (Input::has("archivo_donacion")) {


                    $array_archivo = Input::file("archivo_donacion");
                    // dd($array_archivo);
                    $array_detalle = Input::get("detalle_archivo");
                    if (is_array($array_archivo)) {
                        foreach ($array_archivo as $key => $value) {
                            $item = new DonaIngresoDonacionArchivoModel();
                            $item->id_ingreso =  $idcab;
                            $item->descripcion = $array_detalle[$key];
                            $dir = public_path() . '/archivos_sistema/';
                            $docs = $value;
                            if ($docs) {
                                $fileName = $key . "_donacion_ingreso-$idcab-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                                $ext = $docs->getClientOriginalExtension();
                                $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                                if (!in_array($ext, $perfiles)) {
                                    DB::rollback();
                                    $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                                    return redirect()->back()->withErrors([$mensaje])->withInput();
                                }
                                $item->nombre = $fileName;
                                $item->tipo =  $ext;
                                $docs->move($dir, $fileName);
                            }
                            $item->save();
                        }
                    }
                }



                Auditoria($msgauditoria . " - ID: " . $id . "- donacion");
                DB::commit();
            } //Fin Try


            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage() . " - " . $e->getLine();
                dd($mensaje);
                dd(Input::all());
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }


    public function agregar_producto()
    {
        try {
            //code...
            $producto = Input::get('producto');
            $item = DonaIngresoProductoModel::where('producto', $producto)->first();

            if ($producto == null || $producto == '') {
                $datos = DonaIngresoProductoModel::orderby('producto', 'ASC')->get();
                return response()->json(["estado" => "error", "msg" => "Datos erroneos", "datos" => $datos], 200);
            } else {

                $producto = Input::get('producto');
                $producto = strtoupper($producto);
            }
            if ($item) {
                $datos = DonaIngresoProductoModel::get();
                return response()->json(["estado" => "error", "msg" => $producto . ", ya se encuentra registrado", "datos" => $datos], 200);
            }
            $item = new DonaIngresoProductoModel();
            $item->producto = $producto;
            $item->save();
            $datos = DonaIngresoProductoModel::orderby('producto', 'ASC')->get();

            return response()->json(["estado" => "ok", "msg" => "Agregado Exitosamente", "datos" => $datos, "seleccionado" => $item->id], 200);
        } catch (\Throwable $th) {
            $datos = DonaIngresoProductoModel::orderby('producto', 'ASC')->get();
            //throw $th;
            return response()->json(["estado" => "error", "msg" => "No se pudo agregar", "datos" => $datos], 200);
        }
    }
}
