<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request)
        ->header('Access-Control-Allow-Origin' , '*')
        ->header('Access-Control-Allow-Headers' , 'Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, If-Modified-Since, Cache-Control, Pragma,Application' )
        // ‘X-Requested-With, Content-Type, X-Token-Auth, Authorization’);
        ->header('Access-Control-Allow-Methods',  'GET, POST, PUT, DELETE, OPTIONS')
        // ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        ->header('Access-Control-Expose-Headers','*')
        //->header('Access-Control-Expose-Headers','Content-Disposition')
        ->header('Access-Control-Allow-Headers','*')
        ->header('Access-Control-Expose-Headers','Content-Disposition')
        ->header('maxAge','0');
        
        // $origin = $request->getHttpHost() == 'localhost' ? 'http://localhost' : 'http://sistemasti.manta.gob.ec';        
        // return $next($request)->header('Access-Control-Allow-Origin','*')
        // ->header('Access-Control-Allow-Headers','*')// ‘X-Requested-With, Content-Type, X-Token-Auth, Authorization’);
        // ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        // ->header('Access-Control-Expose-Headers','Content-Disposition')
        // ->header('maxAge','0');
    }
}
