<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        "apruebatramite*",
        "atender_actividad*",
        "ingresar_calendar*",
        "silenciar*",
        "valida_registro_asistencia*",
        "confirmar_asistencia*",
        "ingresarVisto*",
        "asistir_acompaniar*",
        "reporteincidencias*",
        "ingresarReaccionEvento*",
        "ingresarComentarioEvento*",
        "logout",
        "login",
        "subirArchivo*",
        "soap_subida*",
        "frimarCodigoApp*",
        "generarCodigoApp*",
        "enviarObs*"
    ];
}
