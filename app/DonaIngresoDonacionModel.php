<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonaIngresoDonacionModel extends Model
{
    //
    protected $table = 'dona_tmov_ingreso_donacion';
    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'fecha_ingreso' => 'required',
                'cedula_razon' => 'required',
                'nombre_razon' => 'required'
            ],
            $merge
        );
    }
}
