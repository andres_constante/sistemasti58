<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use App\Notifications\CustomResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\ComunicacionAlcaldia\Entities\AgendaVirtualModel;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'fecha_nacimiento','id_direccion', 'id_perfil', 'cedula', 'password','last_login_at','last_login_ip', 'id_perfil',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'modulo','api_token' ,
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPasswordNotification($token));
    }

    public function agendas()
    {
        $this->hasMany(AgendaVirtualModel::class);
    }

    public function esDirector()
    {
        $director = Str::contains($this->cargo, 'DIRECTOR');
        return ($this->id_perfil == 3 || $this->id_perfil == 5 || $this->id_perfil == 9 || $this->id_perfil == 22)
        && $director ? true : false;
    }
}
