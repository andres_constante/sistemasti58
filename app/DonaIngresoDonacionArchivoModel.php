<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonaIngresoDonacionArchivoModel extends Model
{
    //
    protected $table = 'dona_tmov_ingreso_donacion_archivos';
}
