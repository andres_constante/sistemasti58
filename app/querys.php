<?php

use Illuminate\Support\Facades\Input;
function queryactividades()
{
        $iddireccion=Input::get("IDDIRECCION");
		$prioridad=Input::get("PRIORIDAD");
		$estado=Input::get("ESTADO");
		$rendimientoini=intval(Input::get("RENDMIENTOINI"));
		$rendimientofin=intval(Input::get("RENDMIENTOFIN"));
		$fechainicio=Input::get("FECHAINICIO");
		$fechafinal=Input::get("FECHAFINAL");
		$fechaactudesde=Input::get("FECHAACTUDESDE");
		$fechaactuhasta=Input::get("FECHAACTUHASTA");
		$avanceini=intval(Input::get("AVANCEINI"));
		$avancefin=intval(Input::get("AVANCEFIN"));
		$TIPOACTIVIDAD=intval(Input::get("TIPOACTIVIDAD"));
		/**/
		$formato=Input::get("formato");
		$formato=Input::get("namereporte");
		$sql="
		SELECT 
b.id, -- max(a.id) as id,
c.direccion,
c.alias,
b.estado_actividad,
	b.actividad,
	b.fecha_inicio,
	b.fecha_fin,
	MAX(d.name) AS usuario,
	-- DATE_FORMAT(a.updated_at,'%Y-%m-%d') as updated_at,
	(SELECT avance FROM coor_tmov_cronograma_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1) AS avance,
	MAX(b.prioridad) AS prioridad,
	MAX(b.estado) AS estado,
	c.id AS id_direccion,
	-- Dias Plan
	DATEDIFF(b.fecha_fin, b.fecha_inicio) +1 AS dias_plan,
	-- Dias Trans
	CASE WHEN DATEDIFF(CURDATE(), b.fecha_inicio) +1 < 0 THEN 0 ELSE 
		case when b.fecha_fin<CURDATE() then DATEDIFF(b.fecha_fin, b.fecha_inicio) + 1
			else
				DATEDIFF(CURDATE(), b.fecha_inicio) + 1 END END AS dias_trans,
	-- Meta Planificada
	CASE WHEN DATEDIFF(b.fecha_fin, b.fecha_inicio)  = 0 OR b.fecha_fin < CURDATE() THEN 100.00
		ELSE	
		CASE WHEN DATEDIFF(CURDATE(), b.fecha_inicio) +1 < 0 THEN 0.00
			ELSE ROUND((((DATEDIFF(CURDATE(), b.fecha_inicio) + 1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))*100),2) + 1 END END AS Meta_Planificada,
  -- Rendmiento
  -- ==================================
CASE WHEN (DATEDIFF(CURDATE(), b.fecha_inicio)/DATEDIFF(b.fecha_fin, b.fecha_inicio)) = 0 THEN 0
  ELSE
  CASE WHEN (DATEDIFF(CURDATE(), b.fecha_inicio) +1) < 0 THEN 0 
	ELSE
	case when
((((SELECT avance FROM coor_tmov_cronograma_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1)/100)/
((DATEDIFF(CURDATE(), b.fecha_inicio)+1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))) * 100)>100 then 100.00
else
	case when b.fecha_fin < CURDATE() then -- Pregunta de ser Vencido
		ROUND(((((SELECT avance FROM coor_tmov_cronograma_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1)/100)/
		((DATEDIFF(b.fecha_fin, b.fecha_inicio)+1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))) * 100),2)
	else
		ROUND(((((SELECT avance FROM coor_tmov_cronograma_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1)/100)/
		((DATEDIFF(CURDATE(), b.fecha_inicio)+1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))) * 100),2) END  END END END AS Rendimiento,
	-- ===============================================================
	(SELECT updated_at FROM coor_tmov_cronograma_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1) AS fecha_update,
	MAX(a.id) AS id_deta
		 FROM coor_tmov_cronograma_deta AS a, coor_tmov_cronograma_cab AS b, tmae_direcciones AS c,users AS d
		 WHERE a.id_crono_cab=b.id AND b.id_direccion=c.id AND d.id=a.id_usuario
			AND b.estado='ACT'
			AND CAST(c.id AS CHAR) LIKE case when '$iddireccion'='0' then '%' else  '$iddireccion' end
				AND b.prioridad LIKE case when '$prioridad'='0' then '%' else  '$prioridad' end AND
					 b.estado_actividad LIKE case when '$estado'='0' then '%' else  '$estado' end
				AND DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '$fechaactudesde' AND '$fechaactuhasta'
				AND
				b.id_tipo_actividad LIKE case when '$TIPOACTIVIDAD'='0' then '%' else  '$TIPOACTIVIDAD' end
	  
				AND b.fecha_inicio >= '$fechainicio' 
				AND b.fecha_fin <='$fechafinal'
		GROUP BY 
			b.id, -- max(a.id) as id,
			c.direccion,
			c.alias,
			b.estado_actividad,
			b.actividad,
			b.fecha_inicio,
			b.fecha_fin,	
			-- DATE_FORMAT(a.updated_at,'%Y-%m-%d'),
			c.id
		";
		if($rendimientoini>=0)
			$sql.="HAVING Rendimiento BETWEEN  $rendimientoini AND $rendimientofin and 
			avance BETWEEN  $avanceini AND $avancefin";
		$sql.="
			ORDER BY c.direccion,Rendimiento desc,b.id
		";
		return $sql;
}
function queryrendimiento($fechadesde,$fechahasta,$semanaactual="no")
{
    $iddireccion=Input::get("IDDIRECCION");
		$prioridad=Input::get("PRIORIDAD");
		$estado=Input::get("ESTADO");
		$rendimientoini=intval(Input::get("RENDMIENTOINI"));
		$rendimientofin=intval(Input::get("RENDMIENTOFIN"));
		$fechainicio=Input::get("FECHAINICIO");
		$fechafinal=Input::get("FECHAFINAL");
		$fechaactudesde=Input::get("FECHAACTUDESDE");
		$fechaactuhasta=Input::get("FECHAACTUHASTA");
		$avanceini=intval(Input::get("AVANCEINI"));
		$avancefin=intval(Input::get("AVANCEFIN"));
		/**/
		$formato=Input::get("formato");
		$formato=Input::get("namereporte");
    if($semanaactual=="si")
    {
        $fechahoy = new DateTime();
        $fechahoy->modify('this week');
        $fechaactudesde = $fechahoy->format('Y-m-d');
        $fechahoy->modify('this week +4 days');
        $fechaactuhasta = $fechahoy->format('Y-m-d');
        //$fechaactudesde='2019-08-19';
        //$fechaactuhasta='2019-08-23';
        //Seteo a Semana Actual
        $iddireccion="0";
        $prioridad="0";
        $estado="0";
        $rendimientoini=0;
        $rendimientofin=100;
        $fechainicio=date("Y")."-01-01";
        $fechafinal=date("Y")."-12-31";
        $avanceini=0;
        $avancefin=100;
    }
    $sql="SELECT direccion,
MAX(alias) AS alias,
AVG(avance) AS avance,
ROUND(AVG(dias_plan),2) AS dias_plan,
ROUND(AVG(dias_trans),2) AS dias_trans,
ROUND(AVG(Meta_Planificada),2) AS Meta_Planificada,
CASE WHEN AVG(Rendimiento)>100 THEN 100 ELSE ROUND(AVG(Rendimiento),2) END AS Rendimiento
 FROM (
SELECT 
b.id AS id_cab, -- max(a.id) as id,
c.direccion,
c.alias,
b.estado_actividad,
	b.actividad,
	b.fecha_inicio,
	b.fecha_fin,
	MAX(d.name) AS usuario,
	-- DATE_FORMAT(a.updated_at,'%Y-%m-%d') as updated_at,
	(SELECT avance FROM coor_tmov_cronograma_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1) AS avance,
	MAX(b.prioridad) AS prioridad,
	MAX(b.estado) AS estado,
	c.id AS id_direccion,
	-- Dias Plan
	DATEDIFF(b.fecha_fin, b.fecha_inicio) +1 AS dias_plan,
	-- Dias Trans
	CASE WHEN DATEDIFF(CURDATE(), b.fecha_inicio) +1 < 0 THEN 0 ELSE 
		case when b.fecha_fin<CURDATE() then DATEDIFF(b.fecha_fin, b.fecha_inicio) + 1
			else
				DATEDIFF(CURDATE(), b.fecha_inicio) + 1 END END AS dias_trans,
	-- Meta Planificada
	CASE WHEN DATEDIFF(b.fecha_fin, b.fecha_inicio)  = 0 OR b.fecha_fin < CURDATE() THEN 100.00
		ELSE	
		CASE WHEN DATEDIFF(CURDATE(), b.fecha_inicio) +1 < 0 THEN 0.00
			ELSE ROUND((((DATEDIFF(CURDATE(), b.fecha_inicio) + 1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))*100),2) + 1 END END AS Meta_Planificada,
  -- Rendmiento
  -- ==================================
CASE WHEN (DATEDIFF(CURDATE(), b.fecha_inicio)/DATEDIFF(b.fecha_fin, b.fecha_inicio)) = 0 THEN 0
  ELSE
  CASE WHEN (DATEDIFF(CURDATE(), b.fecha_inicio) +1) < 0 THEN 0 
	ELSE
	case when
((((SELECT avance FROM coor_tmov_cronograma_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1)/100)/
((DATEDIFF(CURDATE(), b.fecha_inicio)+1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))) * 100)>100 then 100.00
else
	case when b.fecha_fin < CURDATE() then -- Pregunta de ser Vencido
		ROUND(((((SELECT avance FROM coor_tmov_cronograma_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1)/100)/
		((DATEDIFF(b.fecha_fin, b.fecha_inicio)+1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))) * 100),2)
	else
		ROUND(((((SELECT avance FROM coor_tmov_cronograma_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1)/100)/
		((DATEDIFF(CURDATE(), b.fecha_inicio)+1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))) * 100),2) END  END END END AS Rendimiento,
	-- ===============================================================
	(SELECT updated_at FROM coor_tmov_cronograma_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1) AS fecha_update,
	MAX(a.id) AS id_deta
 FROM coor_tmov_cronograma_deta AS a, coor_tmov_cronograma_cab AS b, tmae_direcciones AS c,users AS d
 WHERE a.id_crono_cab=b.id AND b.id_direccion=c.id AND d.id=a.id_usuario
	AND b.estado='ACT'		
	AND CAST(c.id AS CHAR) LIKE case when '$iddireccion'='0' then '%' else  '$iddireccion' end
				AND b.prioridad LIKE case when '$prioridad'='0' then '%' else  '$prioridad' end AND
					 b.estado_actividad LIKE case when '$estado'='0' then '%' else  '$estado' end
				AND DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '$fechaactudesde' AND '$fechaactuhasta'
				AND b.fecha_inicio >= '$fechainicio' 
				AND b.fecha_fin <='$fechafinal'	
	 -- and b.id=1
GROUP BY 
	b.id, -- max(a.id) as id,
	c.direccion,
	c.alias,
	b.estado_actividad,
	b.actividad,
	b.fecha_inicio,
	b.fecha_fin,	
	-- DATE_FORMAT(a.updated_at,'%Y-%m-%d'),
	c.id
 HAVING Rendimiento BETWEEN  $rendimientoini AND $rendimientofin and 
	avance BETWEEN  $avanceini AND $avancefin
ORDER BY c.direccion,Rendimiento desc,b.id
) AS Tabla
 GROUP BY direccion
 ORDER BY Rendimiento desc,direccion";
    //show($sql);
    return $sql;
}
function queryobras()
{
	//show(Input::all());
	$IDTIPOOBRA=Input::get("IDTIPOOBRA");
    $ESTADO=Input::get("ESTADO");
    $CONTRATISTA=Input::get("CONTRATISTA");
    $PARROQUIA=Input::get("PARROQUIA");
    $FECHAACTUDESDE=Input::get("FECHAACTUDESDE");
    $FECHAACTUHASTA=Input::get("FECHAACTUHASTA");
    $FECHAINICIO=Input::get("FECHAINICIO");
    $FECHAFINAL=Input::get("FECHAFINAL");
    $FECHAFINAL=Input::get("FECHAFINAL");
    $RENDMIENTOINI=intval(Input::get("RENDMIENTOINI"));
    $RENDMIENTOFIN=intval(Input::get("RENDMIENTOFIN"));
    $sql="
    SELECT 
b.id AS id_cab,
c.tipo,
b.calle,
b.nombre_obra,
b.numero_proceso,
b.monto,
b.estado_obra,
MAX(d.nombres) AS contratista,
b.fecha_inicio,
b.fecha_final,
b.longitud,
b.latitud,
 (SELECT avance FROM coor_tmov_obras_listado_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1) AS avance,
MAX(e.parroquia) AS parroquia,
MAX(b.fiscalizador) AS fiscalizador,
IFNULL(MAX(b.plazo),0) AS plazo,
IFNULL(DATEDIFF(b.fecha_final, b.fecha_inicio) +1,0) AS dias_plan,

CASE WHEN IFNULL(DATEDIFF(CURDATE(), b.fecha_inicio) +1,-1) < 0 THEN 0 ELSE DATEDIFF(CURDATE(), b.fecha_inicio) + 1 END AS dias_trans,

-- Meta Planificada
	CASE WHEN DATEDIFF(b.fecha_final, b.fecha_inicio)  = 0 THEN 100
		ELSE	
		CASE WHEN IFNULL(DATEDIFF(CURDATE(), b.fecha_inicio) +1,-1) < 0 THEN 0 ELSE ROUND((((DATEDIFF(CURDATE(), b.fecha_inicio) + 1)/(DATEDIFF(b.fecha_final, b.fecha_inicio)+1))*100),2) END END AS Meta_Planificada,
  -- Rendmiento
CASE WHEN IFNULL((DATEDIFF(CURDATE(), b.fecha_inicio)/DATEDIFF(b.fecha_final, b.fecha_inicio)),0) = 0 THEN 0
  ELSE
  CASE WHEN (DATEDIFF(CURDATE(), b.fecha_inicio) +1) < 0 THEN 0 
	ELSE 
	CASE WHEN
((((SELECT avance FROM coor_tmov_obras_listado_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1)/100)/
((DATEDIFF(CURDATE(), b.fecha_inicio)+1)/(DATEDIFF(b.fecha_final, b.fecha_inicio)+1))) * 100)>100 THEN 100
ELSE
ROUND(
((((SELECT avance FROM coor_tmov_obras_listado_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1)/100)/
((DATEDIFF(CURDATE(), b.fecha_inicio)+1)/(DATEDIFF(b.fecha_final, b.fecha_inicio)+1))) * 100),2) END  END END AS Rendimiento,
-- ------
(SELECT updated_at FROM coor_tmov_obras_listado_deta WHERE id= MAX(a.id) ORDER BY  updated_at DESC LIMIT 1) AS fecha_update,
	MAX(a.id) AS id_deta

FROM coor_tmov_obras_listado_deta a, coor_tmov_obras_listado b, coor_tmae_tipo_obra c, coor_tmae_contratista d , parroquia e
	WHERE b.id=a.id_cab_obra AND c.id=b.id_tipo_obra AND d.id=b.id_contratista  AND e.id=b.id_parroquia	
	AND b.estado='ACT'
	AND CAST(c.id AS CHAR) LIKE case when '$IDTIPOOBRA'='0' then '%' else  '$IDTIPOOBRA' end 
	AND b.estado_obra LIKE case when '$ESTADO'='0' then '%' else  '$ESTADO' end	
	AND d.id LIKE case when '$CONTRATISTA'='0' then '%' else  '$CONTRATISTA' end
	AND e.id LIKE case when '$PARROQUIA'='0' then '%' else  '$PARROQUIA' end
	AND DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '$FECHAACTUDESDE' AND '$FECHAACTUHASTA'
	AND b.fecha_inicio >= '$FECHAINICIO' 
	AND b.fecha_final <='$FECHAFINAL'
GROUP BY 
b.id ,
c.tipo,
b.calle,
b.nombre_obra,
b.numero_proceso,
b.monto,
b.estado_obra,
b.fecha_inicio,
b.fecha_final
HAVING Rendimiento BETWEEN $RENDMIENTOINI AND $RENDMIENTOFIN
ORDER BY c.tipo;
    ";
    return $sql;
}
?>