<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonaIngresoDonacionProductoModel extends Model
{
    //	dona_tmov_ingreso_donacion_producto
    protected $table = 'dona_tmov_ingreso_donacion_producto';
}
