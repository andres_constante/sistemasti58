<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivosModel extends Model
{
    //
    protected $table = 'tmov_archivos';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'tipo'=>'required',
                'id_referencia'=>'required',
                'ruta'=>'required',
                'estado'=>'required',
                'tipo_archivo'=>'required',
                'nom'=>'required',


            ], $merge);
    }
}
