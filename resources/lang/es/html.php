<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do
    |
    */
    'main' => [
        'sistema' => 'Sistema Integral de Planificación para el Desarrollo',
        'sistemasub' => 'Gobierno Autónomo Descentralizado Municipal del Cantón Manta',
        'copyright' => 'Copyright TI GAD Manta &copy; 2019-2023'
    ],
];
