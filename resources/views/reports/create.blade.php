<?php
if(!isset($nobloqueo) && Auth::user()->id_perfil!=4)
    // Autorizar(Request::path());
?>
@extends('layout')
@section ('titulo') {{ $general_config[0] }} @stop
@section ('scripts')
@include("vistas.includes.mainjs")
@if(isset($funcionjs))
  {!! $funcionjs !!}
@endif


@isset($indicadores)
  @include("vistas.includes.jsindicadoresgestion")
@endisset

<style>
#map_canvas{
  width: 100%; height: 800px; position: relative; overflow: hidden;
}

</style>


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyDy1ViaIr_ziZYThMeiNuvrZK7pUb7X_SI&"></script>
<script type="text/javascript">
    function imprimirdiv(obj){
        var printWindow = window.open("", "Imprimir" );
        $("link, style").each(function() {
            $(printWindow.document.head).append($(this).clone())
        });
        //var estilo='<style type="text/css" media="print"> *{  margin: 0px;  padding: 0px;}</style>';
        //$(printWindow.document.head).append(estilo);
        var toInsert = $(obj).clone();//.html();
//        toInsert= toInsert.find('#divpagoonline').remove().end();
        var tituloreporte= $("#tituloreporte").val();
        $("#tdtitulo").text(tituloreporte);
        var divheader= $("#divheader").html();
        toInsert= toInsert.html(divheader+toInsert.html());
        $(printWindow.document.body).append(toInsert);
        setTimeout(function(){
            printWindow.print();
            //printWindow.close();
        },1000);
        //printWindow.print();
        //printWindow.close();
    }
  var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
function pdfiframe(URL)
{
    //alert(URL);
   @if(isset($funcalljs))
            {!! $funcalljs !!}
   @endif
    $("#btnImprime").hide();
    var msg='<h1>Por favor espere...</h1><div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>';
  $("#restable").html(msg);
  var form = $("#form1");  
  var data = JSON.stringify( $(form).serializeArray() ); 
  var paramadi= $("#paramadi").val();
  var formato= $("#formato").val();  
  //console.log(URL);
  //console.log(params);
  //console.log(data);
  URL=URL+"?token={{csrf_token()}}&data="+data+"&paramadi="+paramadi;
  //console.log(URL);
  //window.location=URL;
    //alert(URL);
   if(formato=="xls")
   {
    //$("#restable").html("");
    toastr["success"]("Espere un momento por favor, se está generando el archivo.");
    //$('.ibox-content').block({ message: msg, centerY: 0,centerX: 0 }); 
    window.open(URL,"_self");
    setTimeout(function(){
      //$('.ibox-content').unblock();
      $("#restable").html("");      
    },5000);
    
    return false;
    }else if(formato=="zip")
   {
    //$("#restable").html("");
    toastr["success"]("Espere un momento por favor, se está generando el archivo.");
    //$('.ibox-content').block({ message: msg, centerY: 0,centerX: 0 }); 
    // window.open(URL,"_self");
    $.ajax({
      type: "GET",
      url: URL,
      success: function (response) {
        $("#restable").html("");      
        if (response.estado) {
          window.open(response.ruta,"_blank");
          $("#restable").html("<a href="+response.ruta+">Archivos</a>");      
        }
        $("#restable").html("<a href="+response.ruta+">Archivos</a>");      
      },error: function (response){
        
        $("#restable").html("<a href='/temp/Fotos_1.zip'>Archivos</a>");    
      }
    });
   
    
    return false;
   }else if(formato=="html")
   {            
      $("#btnImprime").show();
       $.ajax({
                    type: "GET",
                    url: "{{ $general_config[2] }}",
                    data: $(form).serialize(),
                    statusCode: {
                        404: function() {
                        toastr["error"]("Se produjo un error al consultar los datos. 1 :(");                        
                        },                            
                        401: function() {
                        toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                        }                        
                    },
                    error: function(objeto, opciones, quepaso){
                        toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                    },
                    success: function (data) {       
                      //console.log(data);
                      $("#restable").html(data);
                        @isset($mapa)
                          try {
                            if($("#estado").val()=='Aprobados'){
                                buscarmantaap();
                            }
                                      
                      } catch (error) {
                        
                      }
                      @endisset    
                  //     $('html, body').animate({
                  //     scrollTop: $("#restable").offset().top
                  // }, 2000);
                }
            });
      return false;
   }else if(formato=="graficos")
   {
       $("#btnImprime").show();
      $.ajax({
                    type: "GET",
                    url: "reportegraficosfiltrosactividades",
                    data: $(form).serialize(),
                    statusCode: {
                        404: function() {
                        toastr["error"]("Se produjo un error al consultar los datos. 1 :(");                        
                        },                            
                        401: function() {
                        toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                        }                        
                    },
                    error: function(objeto, opciones, quepaso){
                        toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                    },
                    success: function (data) {       
                      //console.log(data);
                      $("#restable").html(data);
                      
                }
            });
      return false;
   }else if(formato=="graficos-obras")
   {       $("#btnImprime").show();
      $.ajax({
                    type: "GET",
                    url: "reportegraficosfiltrosObras",
                    data: $(form).serialize(),
                    statusCode: {
                        404: function() {
                        toastr["error"]("Se produjo un error al consultar los datos. 1 :(");                        
                        },                            
                        401: function() {
                        toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                        }                        
                    },
                    error: function(objeto, opciones, quepaso){
                        toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                    },
                    success: function (data) {       
                      //console.log(data);
                      $("#restable").html(data);
                     /// $('body').trigger('contentUpdated');
                }
            });
      return false;
   }else{
       //alert("si");

    if(isMobile.any()) {
        //alert("Hola");
        //window.location=URL;
        //window.open("http://docs.google.com/gview?url="+URL,"_self");
        ///window.open(URL,"_self");
        //console.log(URL);
        window.location=URL;
        return;
        var formu= $("#form2");
        console.log(formu);
        formu.prop('action', URL);
        //formu.submit();
        $("#restable").html("");
        return;
    }
}
  $.colorbox({href:URL, iframe:true,fixed:true, innerWidth:screen.width -(screen.width * 0.10), innerHeight:screen.height -(screen.height * 0.30),
    onLoad:function(){
      $('#cboxContent').block({ message: msg, centerY: 0,centerX: 0 }); 
    },
    onComplete:function(){      
      $("#cboxLoadedContent .cboxIframe").load(function(){        
        $('#cboxContent').unblock(); 
        $("#restable").html("");        
      });
    }    
  });
}


function getEstadosTramites(id) {
  $('#estado').empty();
  var ruta_indicadores= '{{URL::to('getTipoPermisos')}}';
    $.get(ruta_indicadores, function(data)  {
        datos=data;  
      $.each(datos, function(key, data) {
        if(data.id_tipo_tramite==id){
            $.each(data.estados, function(k, d) {
              $('#estado').append("<option value=\'" + k + "\'>" + d+ "</option>");
            });
               
        }
                
      });  
    });
}

function getDelegadosByDireccion(id) {
    try {
      var id=$("#idtipo_entidad").val();
      if (id == 0) return;
      $.ajax({
        url: '{!! route('delegados.ajax') !!}?entidad='+id,
      }).done(function(resutado) {
        console.log(resutado);
        if (resutado['estado']) {
          $('#delegado').empty();
          var select = document.getElementById('delegado'); 
          var option = document.createElement("option"); //Creamos la opcion
          option.value = 0; //Metemos el texto en la opción
          option.innerHTML ='TODOS'; //Metemos el texto en la opción
          select.appendChild(option); 
          
          resutado['data'].forEach(element => {
                var option = document.createElement("option"); //Creamos la opcion
                option.value = element.usuario; //Metemos el texto en la opción
                option.innerHTML = element.name; //Metemos el texto en la opción
                select.appendChild(option); //Metemos la opción en el select
              });
        }
            

      });
  } catch (error) {
    console.log(error);
  }
}

$(document).ready(function() {

  $("#delegado").prop('disabled', 'disabled');

  $("#reporte").change(function (e) { 
    
    window.location.assign("{{URL::to('')}}/"+this.value)
    
  });

  try{
    var ruta_indicadores= '{{URL::to('getTipoPermisos')}}';
    $.get(ruta_indicadores, function(data)  {
        datos=data;
        console.log(data);
        $('#IDTIPOPERMISO').empty();
      $('#IDTIPOPERMISO').append("<option value='0'>TODOS</option>");
      $.each(datos, function(key, data) {
                $('#IDTIPOPERMISO').append("<option value=\'" + data.id_tipo_tramite + "\'>" + data.tramite+ "</option>");
                $('#IDTIPOPERMISO').trigger("chosen:updated");
      });  
    });

    $('#IDTIPOPERMISO').change(function (e) { 
      // if(this.value==0){
      //   // alert(9); 
      //   $('#estado').empty();
      //   $('#estado').append("<option value='Aprobados'>Aprobados</option>");
      // }else{
      //   getEstadosTramites(this.value);
      // }
      
    });





$('#tabla_permisos').dataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]])
                    //"order": ([[ 2, 'asc' ], [ 1, 'asc' ]])
                    //../js/plugins/dataTables/swf/copy_csv_xls_pdf.swf
                  
            });  
  }catch{

  }
  


try {
    var id=$("#idtipo_entidad").val();
    $.ajax({
      url: "https://portalciudadano.manta.gob.ec/conusultatipoincidenciabyidentidad?id="+id,
    }).done(function(resutado) {
      $('#id_tipo_denuncia').empty();
          
      var select = document.getElementById('id_tipo_denuncia'); 
      var option = document.createElement("option"); //Creamos la opcion
      option.value = 0; //Metemos el texto en la opción
      option.innerHTML ='TODOS'; //Metemos el texto en la opción
      select.appendChild(option); 
      
      resutado.forEach(element => {
            var option = document.createElement("option"); //Creamos la opcion
            option.value = element.id; //Metemos el texto en la opción
            option.innerHTML = element.nombre_denuncia; //Metemos el texto en la opción
            select.appendChild(option); //Metemos la opción en el select
          });

    });
} catch (error) {
  
}

  $( "#idtipo_entidad" ).change(function() {
    var id=$(this).val();

    if ($("#namereporte").val() == 'rpt_reporte_incidencias_rendimiento') {
      getDelegadosByDireccion(id);
    }


    $.ajax({
      url: "https://portalciudadano.manta.gob.ec/conusultatipoincidenciabyidentidad?id="+id,
    }).done(function(resutado) {
      $('#id_tipo_denuncia').empty();
          
      var select = document.getElementById('id_tipo_denuncia'); 
      var option = document.createElement("option"); //Creamos la opcion
      option.value = 0; //Metemos el texto en la opción
      option.innerHTML ='TODOS'; //Metemos el texto en la opción
      select.appendChild(option); 
      
      resutado.forEach(element => {
            var option = document.createElement("option"); //Creamos la opcion
            option.value = element.id; //Metemos el texto en la opción
            option.innerHTML = element.nombre_denuncia; //Metemos el texto en la opción
            select.appendChild(option); //Metemos la opción en el select
          });

    });

  });
    varjson=@php print json_encode($formatosjs,JSON_FORCE_OBJECT) @endphp;
    varjsontipo=@php print json_encode($tiporepor,JSON_FORCE_OBJECT) @endphp;
    //console.log(varjson.rpt_rendimiento_actividad_noagrupado);
    //console.log([varjson][0]["rpt_direccion_sin_avance"]);
    $("#namereporte").change(function(){
        selval=$(this).val();
        // alert(selval);

        if (selval == 'rpt_reporte_incidencias_rendimiento') {
          $("#parroquia").val('');
          $("#parroquia").prop('disabled', 'disabled');
          $("#delegado").removeAttr("disabled"); 

          if ($("#idtipo_entidad").val() != "") {
            getDelegadosByDireccion($("#idtipo_entidad").val());
          }


        } else {
          $("#parroquia").removeAttr("disabled"); 
          $("#delegado").val('');
          $("#delegado").prop('disabled', 'disabled');
        }

        $("#formato").empty();
        /*$("#id_menu_hijo").append("<option value>Escoja...</option>");*/
        data=JSON.parse([varjson][0][selval]);
        //console.log(data);
        $.each(data, function(key, element) {
            $("#formato").append("<option value='" + key + "'>" + element + "</option>");
        });

        
        //
        $("#tiporeporte").empty();
        /*$("#id_menu_hijo").append("<option value>Escoja...</option>");*/
        data=JSON.parse([varjsontipo][0][selval]);
        //console.log(data);
        $.each(data, function(key, element) {
            $("#tiporeporte").append("<option value='" + key + "'>" + element + "</option>");
        });
    });

    //

});



</script>
<!--include("vistas.includes.jsfunciones") -->

@stop
@section ('contenido')
<div id="divheader" style="display:none;">
    <center>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
<img style="width: 200px; height: auto;" src="{{ asset("img/mantafirmes.png") }}" />
            </td>
            <td style="text-align: center;">
                <h2>GOBIERNO AUTÓNOMO DESCENTRALIZADO MUNICIPAL DEL CANTÓN MANTA</h2>
                @if(isset($mapa))
                <h3>CONTROL TERRITORIAL</h3>
                @else
                <h3>COORDINACIÓN GENERAL DE PLANIFICACIÓN PARA EL DESARROLLO</h3>
                @endif
            </td>
            <td></td>
        </tr>
        <tr><td colspan="3"><img align="center" alt="Image" border="0" style="height: 5px;" src="{{ asset("img/lineafirmes.png")}}"  title="Image" width="100%"/></td></tr>
        <!--
        <tr>
            <td colspan="3"><div id="tdtitulo"></div></td>
        </tr>
        -->
    </table>
        </center>
</div>
<h1> 
  {!! $general_config[0] !!}  
</h1>
<div class="ibox-content">
    {!!  Form::open(['url' => $general_config[1], 'role' => 'form', 'id' => 'form1', 'class' => 'form-horizontal  col-lg-8  ', 'style'=>';', 'files' => true])  !!}
    <?php 
      $ik=1;
    ?>
         @foreach($objects as $i => $campos)                
                  <?php $mod=$ik % 2; ?>
                  
                  <div class="{{((isset($campos->Ancho))?'col-lg-'.$campos->Ancho.'':'col-lg-6') }} "  style="{{((isset($campos->Oculto))?'display:none;':'') }}">
                    @if($campos->Tipo!="FormHidden")
                    {!! Form::label($campos->Nombre, $campos->Descripcion.':', array("class"=>"control-label")) !!}                  
                    @endif
                      @if($campos->Tipo=="date")
                          <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                        {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datefecha','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                      @elseif($campos->Tipo=="datetext")
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          
                          {!! Form::text($campos->Nombre, $campos->Valor , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha',
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            'readonly'=>'readonly'
                            )) !!}
                      </div>
                      @elseif($campos->Tipo=="select")
                         {!! Form::select($campos->Nombre,$campos->Valor, ((Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null"))) ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                      @elseif($campos->Tipo=="select2")
                         {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                      @elseif($campos->Tipo=="select-multiple")
                         {!! Form::select($campos->Nombre."[]",$campos->Valor, (Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                      @elseif($campos->Tipo=="file")                         
                          {!! Form::file($campos->Nombre, array('class' => 'form-control')) !!}
                        @if(isset($tabla))
                        <br>
                          <?php
                            $cadena="if(\$tabla->".$campos->Nombre."!='') echo '<a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$tabla->".$campos->Nombre.").' class=\"btn btn-success divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Ver Archivo</a>';";
                            eval($cadena);
                          ?>  
                        @endif
                      @elseif($campos->Tipo=="textdisabled")
                         {!!   Form::text($campos,Input::old($campos), array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i],'readonly')) !!}
                      @elseif($campos->Tipo=="textarea")
                          {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                      @elseif($campos->Tipo=="textmayus")
                          {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion,'onkeyup'=>'aMays(event, this)')) !!}
                          
                      @elseif($campos->Tipo=="password")
                         {!!   Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre)) !!}
                         @elseif($campos->Tipo=="textdisabled2")
                         {!!   Form::text($campos->Nombre,Input::old($campos->Valor), array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}
                      @elseif($campos->Tipo=="selectdisabled")
                         {!! Form::select($campos->Nombre,$campos->Valor, (Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase, 'readonly')) !!}
                      @elseif($campos->Tipo=="select-multiple-disabled")
                         {!! Form::select($campos->Nombre."[]",$campos->Valor, (Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre,'readonly'))!!}          
                      @elseif($campos->Tipo=="textarea-disabled")
                          {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}
                      @elseif($campos->Tipo=="FormHidden")
                        {!! Form::hidden($campos->Nombre,$campos->Valor ,array("id"=>$campos->Nombre)) !!}                         
                      @else
                          {!! Form::text($campos->Nombre, $campos->ValorAnterior!="Null" ? $campos->ValorAnterior : Input::old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                      @endif  
                    </div> <!-- COL -->                
                 
      <?php 
        $ik++;
      ?>
        @endforeach
        
        <div class="form-group" style="padding: 1%;">          
          <div class="col-lg-12" {{(isset($boton))?'hidden':''}}> 
              
          <button type="button" class="bnt btn-success form-control" name="btnPDF" onclick="return pdfiframe('{{ URL::to($general_config[1]) }}')" style="float:left; color:#ffff; background-color: #1485c9; width: 200px;"><i class="fa fa-check-square-o"></i>&nbsp&nbsp&nbsp&nbspProcesar Filtros</button>
          <input type="hidden" id="paramadi" value="{{ $paramadi }}">
          
          </div>
      
        </div>
        {!!  Form::close()  !!}
    
  
      

  </div>
  @isset($mapa)
          {!!  Form::open(['url' => "", 'role' => 'form', 'id' => 'form2', 'class' => 'form-horizontal jumbotron col-lg-4','name'=>'form2','method'=>'get'])  !!}
          <h3>Buscar relación con MANTAPP</h3>
          <input id="id_incidencia"  class="form-control" maxlength="15">
          <button type="button" class="bnt btn-success form-control" onclick="buscarmantaap()" id="btn_buscar" style="float:right; color:#ffff; background-color: #1485c9;"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>    
          <div style="text-align: center;">
          <br>
          <br>
          <br>
            <img src="{{asset('img/mantapp.png')}}" alt="" style="width: 45%"> 

          </div>
          {!!  Form::close()  !!} 
        @endisset
   

  
  @isset($mapa)
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy1ViaIr_ziZYThMeiNuvrZK7pUb7X_SI&libraries=places"></script>
  <script>
    var map = null;
    var myMarker;
    var myLatlng;

  function initMap(lat, lng,t)
    {
      myLatlng = new google.maps.LatLng(lat, lng);
      
      let myOptions = {
        zoom: 14,
        zoomControl: true,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      if(t==1){
      //   myMarker = new google.maps.Marker({
      //   position: myLatlng,
      //   draggable: false,  
      //  });
      }else{
          myMarker = new google.maps.Marker({
          position: myLatlng,
          draggable: false,
          icon: {
                  url: "{{asset('img/mantapp_xs.png')}}",
                  
                }
        });
        var cityCircle = new google.maps.Circle({ 
        // strokeColor: '#0000FF', 
        strokeOpacity: 0.8, 
        strokeWeight: 2, 
        fillColor: '#00dc4980', 
        fillOpacity: 0.35, 
        map: map, 
        center: myLatlng, 
        radius: 50 });
        myMarker.setMap(map);
      }
      

     


      

    }
    function buscarmantaap(){
       var id_denuncia=$("#id_incidencia").val();
       if(id_denuncia==null || id_denuncia==''){
        id_denuncia=0;
       }
       var construccion=0;
       var demolicion=0;
       var cerramiento=0;
       var uso_suelo=0;
       var regulacion_urbana=0;
        $('#tabla_permisos_2').empty();
       $.ajax({
         type: "get",
         url: "https://portalciudadano.manta.gob.ec/public/getCordenadasPermisos/"+id_denuncia+"?construccion="+$('#construccion').is(':checked')+"&demolicion="+$('#demolicion').is(':checked')+"&cerramiento="+$('#cerramiento').is(':checked')+"&uso_suelo="+$('#uso_suelo').is(':checked')+"&regulacion_urbana="+$('#regulacion_urbana').is(':checked')+"&FECHAINICIO="+$('#FECHAINICIO').val()+"&FECHAFINAL="+$('#FECHAFINAL').val(),
         contentType: false,
                    // enctype: 'multipart/form-data',
        data: null,
        processData: false,
        cache: false,
         success: function (response) {
           console.log(response);
           if(response.error){
              toastr["error"](response.error);

           }else{
            
              if(response.denuncia){
                
                $('#denuncia').html('<b>Tipo de incidencia: </b>'+response.denuncia.nombre_denuncia);
                initMap(response.denuncia.latitud, response.denuncia.longitud);
              }else{
                initMap(-0.9484151471928869, -80.72165359236527,2);
             }
             
          var i;
          
          
          for (i = 0; i < response.permisos.length; i++) {  
            let url = "http://maps.google.com/mapfiles/ms/icons/";
            if(response.permisos[i].permiso.id_tipo==4){
              regulacion_urbana=regulacion_urbana+1;
                url += "blue-dot.png";
            }else if(response.permisos[i].permiso.id_tipo==1){
              console.log(response.permisos[i].permiso);
              construccion=construccion+1;
              url += "green-dot.png";
            }else if(response.permisos[i].permiso.id_tipo==6){
              demolicion=demolicion+1;
              url += "orange-dot.png";
            }else if(response.permisos[i].permiso.id_tipo==7){
              uso_suelo=uso_suelo+1;
              url += "red-dot.png";
            }else if(response.permisos[i].permiso.id_tipo==9){
              cerramiento=cerramiento+1;
              url += "yellow-dot.png";
            }else{
              url += "purple-dot.png";
            }


            myMarker = new google.maps.Marker({
              position: new google.maps.LatLng(response.permisos[i]['coordenadas'][0], response.permisos[i]['coordenadas'][1]),
              map: map,
              icon: {
                url: url
              }
            });

            // $('#tabla_permisos_body').html('');
            
            if(response.permisos[i].distancia<=0.05){
              console.log(response.permisos[i].distancia);
              // alert(response.permisos[i].distancia);
              $('#tabla_permisos_').append('<tr><td>'+response.permisos[i].permiso.nombresolicitud+'</td><td>'+response.permisos[i].permiso.name+'</td><td>'+response.permisos[i].permiso.direccion+'</td><td>'+response.permisos[i].permiso.telefono+'</td><td>'+response.permisos[i].distancia+' km</td><td>'+response.permisos[i].permiso.fecha_aprobacion+'</td><td>'+response.permisos[i].permiso.fecha_vencimiento+'</td></tr>');
              
            }
            // $('#map_canvas').focus()
            // $('html, body').animate({
            //           scrollTop: $("#map_canvas").offset().top
            //       }, 2000);
            // $("#id_incidencia").val('');
            // $("#btn_buscar").attr('disabled',true);
            // console.log();
            $("#t_construccion").html(construccion);
            $("#t_demolicion").html(demolicion);
            $("#t_cerramiento").html(cerramiento);
            $("#t_uso_suelo").html(uso_suelo);
            $("#t_regulacion_urbana").html(regulacion_urbana);
          }
           
         }
        }
       });
    }
    $(document).ready(function () {
      initMap(-0.9484151471928869, -80.72165359236527,1);
      // $('#construccion').is(':checked');
      $("#construccion").attr('checked', true);
      $("#demolicion").attr('checked', true);
      $("#cerramiento").attr('checked', true);
      $("#uso_suelo").attr('checked', true);
      $("#regulacion_urbana").attr('checked', true);
      
      $("#btn_buscar").attr('disabled',true);
      $("#id_incidencia").keyup(function (e) { 
        var dato =$(this).val();
        console.log(dato.length );
        if(dato.length==15){
          $("#btn_buscar").attr('disabled',false);
        }else{
          $("#btn_buscar").attr('disabled',true);
        }
        
      });

    });
  </script>
  <div class="ibox-content">
    <div><h3>FINALIZADOS</h3></div>
  <div id="denuncia"></div>
  <div>
    <table class="table">
      <tr style="font-size: larger;">
        <td>Permiso de construcción <input class="form-check-input" onclick="buscarmantaap()" type="checkbox" value="" id="construccion"> <img src="http://maps.google.com/mapfiles/ms/icons/green-dot.png" alt=""><br><span class="badge" style="background-color: #00e64d; font-size: large;" id="t_construccion"></span></td>
        <td>Permiso de demolición <input class="form-check-input"  onclick="buscarmantaap()" type="checkbox" value="" id="demolicion"> <img src="http://maps.google.com/mapfiles/ms/icons/orange-dot.png" alt=""><br><span class="badge"  style="background-color: #ff9900; font-size: large;"  id="t_demolicion"></span> </td>
        <td>Permiso de cerramiento  <input class="form-check-input" onclick="buscarmantaap()" type="checkbox" value="" id="cerramiento"> <img src="http://maps.google.com/mapfiles/ms/icons/yellow-dot.png" alt=""><br><span class="badge" style="background-color: #ffff6e; font-size: large;"  id="t_cerramiento"></span></td>
        <td>Uso de suelo <input class="form-check-input" type="checkbox" onclick="buscarmantaap()" value="" id="uso_suelo"><img src="http://maps.google.com/mapfiles/ms/icons/red-dot.png" alt=""><br><span class="badge" style="background-color: #fd7567; font-size: large;"  id="t_uso_suelo"></span></td>
        <td>Regulación urbana <input class="form-check-input" type="checkbox" onclick="buscarmantaap()" value="" id="regulacion_urbana"> <img src="http://maps.google.com/mapfiles/ms/icons/blue-dot.png" alt=""><br><span class="badge" style="background-color: #6991fd; font-size: large;" id="t_regulacion_urbana"></span></td>
      </tr>
    </table>
    
    

  </div>
  <div style="width: 100%; height: 500px;" id="map_canvas"></div>
  </div>
  
      
  @endisset
  <div class="ibox-content">
      <input id="btnImprime" type="button" class="btn btn-danger" value="Imprimir" onclick="return imprimirdiv('#restable')" style="display:none;">
      <div id="restable">
      @isset($mapa)  
      <table class="table" id="tabla_permisos_">
        <thead>
        <tr><th>Tipo de permiso</th><th>Nombre</th><th>Dirección</th><th>Telefono</th><th>Distancia</th><th>Fecha de emisión</th><th>Fecha de expiración</th></tr>
        </thead>
        <tbody  id="tabla_permisos_2"></tbody>
        </table>
      @endisset
      </div>
      
  </div>



@stop

