<?php
if(!isset($nobloqueo) && Auth::user()->id_perfil!=4) ?>
@extends('layout')



@section ('contenido')
<h1 style="padding: 1%">{{ $general_config[0] }}</h1>

<div id="divheader">
  <center style="padding: 1%">
     {!! Form::open(['method' => 'POST', 'id' => 'search-form', 'role'=>'form', 'class'=>'form-row', 'style' => 'margin: 0 0 0 0;']) !!}
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label class="control-label text-left" for="estado">Dirección:</label>
                    <select class="select2 form-control" name="entidades" id="entidades">
                    </select>
                </div>
            </div>
         

            <div class="col-sm-5">
                <div class="form-group">
                    <label class="control-label" for="fecha"><b>Fecha:</b></label>
                    <div id="demo-dp-range">
                        <div class="input-daterange input-group" id="datepicker">
                            <span class="input-group-addon btn-primary">Desde</span>
                            <input type="text" class="form-control " name="inicio" id="inicio" autocomplete="off"/>
                            <span class="input-group-addon btn-primary">Hasta</span>
                            <input type="text" class="form-control" name="fin" id="fin" autocomplete="off"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                  <label class="control-label" ><b>&nbsp;</b></label>
                  <div class="form-actions text-left">
                      <a class="btn btn-success"  onclick="onchanguecede()">Buscar</a>
                  </div>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>

          <hr>
          <div class="row">
            <div class="col-sm-12">
              <div class="table-responsive" id="tablemaster">
                <table class="table table-striped" cellspacing="0" width="100%" id="data-table-turnos">
                    <thead>
                        <tr>
                          <th>Denuncia</th>
                          <th>Total</th>
                        </tr>
                    </thead>
                    <tbody id="ss">
                      
                    </tbody>
                </table>
              </div>
            </div>
            <div class="col-sm-12">
              <div id="chart_pie"></div>
            </div>
            <div class="col-sm-12">
              <div id="chart_column"></div>
            </div>
          </div>
          
          

       

          <hr class="new-section-xs">


   <div id="container"></div>

  </center>
</div>
@stop

@section ('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>


<script>


    // $(function() {

    $( document ).ready(function() {
        $('.input-group').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd',
            language: 'es'
        });

      $.ajax({
         type: "POST",
          url:  'https://portalciudadano.manta.gob.ec/exportar_entidades',
          success : function(data) {
            $('#entidades').append('<option value="" selected>Todos</option>');
            $.each(data, function( index, value ) {
              $('#entidades').append('<option value="'+value.id+'">'+value.nombre_entidad+'</option>');
            });
          }       
      });

      reloadTable(null);
    });


      
        // var table = $('#data-table-turnos').DataTable({
        //     dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
        //     "<'row'<'col-xs-12't>>"+
        //     "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        //     processing: true,
        //     serverSide: true,
        //     searching: false,
        //     responsive: true,
        //     order: [[ 1, 'ASC' ]],
        //     ajax: {
        //         url: '{!! route('repors_indicencias.ajax') !!}',
        //         success : function(data) {
                    
        //             console.log('gola');
                    
        //         },       
        //         data: function (d) {
        //             d.estado = $('select[name=estado]').val();
        //             d.cede = $('select[name=cede]').val();
        //             d.inicio = $('input[name=inicio]').val();
        //             d.fin = $('input[name=fin]').val();
        //             d.placa = $('input[name=placa]').val();
        //         }
        //     },
        //     language: { url: '../language/es.json' },
        //     lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
        //     dom: 'Blfrtip',
        //     buttons: [
        //         {
        //             extend: 'pdfHtml5',
        //             title: 'Turnos atendidos',
        //             text: '<i class="fa fa-file-pdf"></i> PDF'
        //         },
        //         {
        //             extend: 'excelHtml5',
        //             title: 'Turnos atendidos',
        //             footer: true
        //         },
        //     ],
        //     columns: [
        //         { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
        //         { data: 'denuncia', name: 'denuncia' },
        //         { data: 'total', name: 'total' }
        //     ]
        // });

        // $('#search-form').on('submit', function(e) {
        //     // table.draw();
        //     reloadTable();
        //     e.preventDefault();
        // });

        // $('#limpiar').on('click', function(e) {
        //     $('#estado').val('-1').trigger('change');
        //     $('#cede').val('-1').trigger('change');
        //     $('#search-form')[0].reset()
        //     table.draw();
        //     e.preventDefault();
        // });
    // });



    function onchanguecede(){
      reloadTable($('#entidades').val());
    }
      var table;
     function reloadTable(id_denuncia){
      $.ajax({
          url:  '{!! route('repors_indicencias.ajax') !!}',
          data: {
            'id_direccion': id_denuncia, 
            'fecha_ini': $('#inicio').val(), 
            'fecha_fin': $('#fin').val()},
          success : function(data) {
            console.log(data);
              
              table = $('#example').DataTable();
              table.draw();

              table = $('#data-table-turnos').dataTable( {
                 destroy: true,
                  data : data,
                  columns: [

                  { data: 'denuncia', name: 'denuncia' },
                  { data: 'total', name: 'total' }
              
                  ],
                  language: { url: '../language/es.json' },
                  lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
                  dom: 'Blfrtip',
                  buttons: [
                      {
                          extend: 'pdfHtml5',
                          title: 'Turnos atendidos',
                          text: '<i class="fa fa-file-pdf"></i> PDF'
                      },
                      {
                          extend: 'excelHtml5',
                          title: 'Turnos atendidos',
                          footer: true
                      },
                  ]
              });
              var ArrayData = [];
              var ArrayTitle = [];
              var total=0;
              $.each(data, function( index, value ) {
                total += value.total;
                ArrayData.push({ name:value.denuncia, y:value.total, porcentaje: null});
                ArrayTitle.push(value.denuncia);
              });
              $.each(ArrayData, function( index, value ) {
                ArrayData[index].porcentaje = ((ArrayData[index].y / total) * 100).toFixed(2);              
              });

              show_pie('chart_pie', 'RESUMEN DE INCIDENCIAS', ArrayData, 'pie');

              // ColumnChart('chart_column', 'Incidencias por direccion', ArrayTitle, ArrayData, 'column');
          }       
      });
    }

     function ColumnChart(Name, Title, ArrayTitle, ArrayData, Tipe_chart){
        Highcharts.chart(Name, {
            chart: {
                type: Tipe_chart
            },
            title: {
                text: null
            },
            subtitle: {
                text: '<b style="color: #000; font-weight: bold">'+Title+'<b>'
            },
            xAxis: {
                categories: ArrayTitle,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: null
                }
            },
            colors: ['#A3CB38', '#0984e3'],
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f}  ({point.porcentaje}%)</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                pie: {
                    shadow: false,
                    center: ['50%', '50%']
                },
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        format:  '<span>{series.name}</span>:<br><b >{point.y}</b><br><b style="font-size:1.2em">({point.porcentaje}%)</b>',
                        enabled: true,
                        crop: false,
                        overflow: 'none'
                    }
                }
            },
            series: ArrayData
        });
    }



    function show_pie(Name, Title, ArrayPie, Tipe_chart) {
        Highcharts.chart(Name, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: Tipe_chart
            },
            title: {
                text:null
                
            },
            subtitle: {
                text:'<b style="color: #000; font-weight: bold">'+Title+'<br>'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b> <b>({point.porcentaje}%)</b>'
            },
            colors: ['#0d233a', '#1aadce', '#8bbc21', '#910000', '#1aadce',
                '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: <br><b>{point.y}</b> <br><b style="font-size:1.2em">({point.porcentaje}%)</b>'
                    },
                    // showInLegend: true
                }
            },
            legend: {
                enabled: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Cantidad',
                data: ArrayPie
            }]
        });
    }



      function ChartPie(id_chart, title_chart, data_chart){
        Highcharts.chart(id_chart, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: title_chart
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            colors: ['#0d233a', '#f28f43', '#8bbc21', '#910000', '#c42525',
                '#492970', '#1aadce', '#ffea00', '#77a1e5', '#a6c96a'],
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: <b>{point.y}</b>',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Porcentaje',
                data: data_chart
            }]
        });
      }



</script>

@stop