<?php
  $images=explode("|",ConfigSystem("backgroundlogin"));
  $c=count($images)-1;  
  $imgbk=$images[rand(0,$c)];
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>@yield('titulo')</title>

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('font-awesome/css/font-awesome.css') }}
    {{ HTML::style('css/animate.css') }}
    {{ HTML::style('css/style.css') }}

   <!-- Data Tables -->
    {{ HTML::style('css/plugins/dataTables/dataTables.bootstrap.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.responsive.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.tableTools.min.css') }}

<!-- Mensaje -->

{{ HTML::style('css/plugins/toastr/toastr.min.css') }}

    @yield('estilos')
    <style type="text/css">
    .bgbody {
        background: url({{ asset($imgbk) }}) no-repeat 0px 0px !important;
        background-size: cover !important;  
        /*filter:opacity(30%);       */
    }
    .white-bg {
    background-color: #f7f7f7b3 !important;
    
}
</style>
<!-- Mainly scripts -->
{{ HTML::script('js/jquery-2.1.1.js') }}
{{ HTML::script('js/bootstrap.min.js') }}
{{ HTML::script('js/plugins/metisMenu/jquery.metisMenu.js') }}
{{ HTML::script('js/plugins/slimscroll/jquery.slimscroll.min.js') }}


<!-- Custom and plugin javascript -->
{{ HTML::script('js/inspinia.js') }}
{{ HTML::script('js/plugins/pace/pace.min.js') }}

 <!-- Data Tables -->
 {{ HTML::script('js/plugins/dataTables/jquery.dataTables.js') }}
    {{ HTML::script("js/plugins/dataTables/dataTables.bootstrap.js") }}
    {{ HTML::script("js/plugins/dataTables/dataTables.responsive.js") }}
    {{ HTML::script("js/plugins/dataTables/dataTables.tableTools.min.js") }}

 <!-- Mensaje -->
 {{ HTML::script('js/plugins/toastr/toastr.min.js') }}

      <!-- CSS Notificacion -->
    {{ HTML::style('ventanas-modales/ventanas-modales.css') }}
    <!-- CSS Notificacion -->
    {{ HTML::script('ventanas-modales/ventanas-modales.js') }}
    <!-- Graficos estadissticos -->
{{ HTML::style('css/plugins/morris/morris-0.4.3.min.css') }}
{{-- {{ HTML::script('js/plugins/morris/raphael-2.1.0.min.js') }} --}}
{{ HTML::script('js/plugins/morris/raphael.js') }}
{{ HTML::script('js/plugins/morris/morris.js') }}
<!-- Chosen -->
    {{ HTML::style('chosen/docsupport/prism.css') }}
    {{ HTML::style('chosen/chosen.css') }}

    {{ HTML::script('chosen/chosen.jquery.js') }}
    {{ HTML::script('chosen/docsupport/prism.js') }}
<!-- ColorBox -->
    {{ HTML::style('colorbox/colorbox.css') }}

    {{ HTML::script('colorbox/jquery.colorbox.js') }}
    

{{ HTML::style('css/jquery-gallery.css') }}
{{ HTML::script('js/jquery-gallery.js') }}
 <!--sweetalert-->
 {{ HTML::style('css/plugins/sweetalert/sweetalert.css')}}
      {{ HTML::script('js/plugins/sweetalert/ciudadano/sweetalert.min.js') }}
      {!! Html::script('js/randomColor.js') !!}
  
<style>
/* .fade{
    opacity: 1 !important;
} */
@media only screen and (max-width: 600px) {
    .responsiveagcm{
        display: none;
    }
    .titulo{
        font-size: 25px !important;
        font-weight: bold;
        text-align: center;
        text-shadow: 2px 2px 2px #828288;
    }
    .titulo2{
        color: #00000;
        font-size: 18px !important;
        font-weight: bold;
        text-align: center;
        text-shadow: 2px 2px 2px #00000;
    }
    .imgresponsiveandres{
        /*width: 100% !important;*/
        height: 200px !important;
    }
    .menuvisible{
        display: none;
    }
    #centraricons {
        display: block !important;
    }

}
#centraricons{
    display: flex;justify-content: center;
}
.centerdiv{
    position: absolute;
    height: 50px;
    width: 50px;
    top:calc(50% - 50px/2); /* height divided by 2*/
    left:calc(50% - 50px/2); /* width divided by 2*/
}
</style>
@yield('scripts')
<!--Loader-->
    {{ HTML::script('js/jquery.queryloader2.min.js') }}
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            QueryLoader2(document.querySelector("body"), {
                barColor: "#00000",
                backgroundColor: "#ffffff75",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        $(document).ready(function() {
            $("a").click(
                function () {
                    var bad = this.href.lastIndexOf('#') >= 0 || this.href.indexOf('javascript') >= 0;
                    if(!bad) {
                        if (isMobile.any()) {
                            QueryLoader2(document.querySelector("body"), {
                                barColor: "#ffffff",
                                backgroundColor: "#ffffff75",
                                //percentage: true,
                                barHeight: 1,
                                minimumTime: 200,
                                fadeOutTime: 1000
                            });
                            $("#qLoverlay").html("<div class='centerdiv'><div class=\"sk-spinner sk-spinner-double-bounce\">\n" +
                                "                                    <div class=\"sk-double-bounce1\"></div>\n" +
                                "                                    <div class=\"sk-double-bounce2\"></div>\n" +
                                "                                </div></div>");
                        }
                    }
                }
            );
        });
 function cargaatender()
 {

	    $.ajax({
			type: "GET",
			url: '{{ URL::to("getnotificatodos") }}',
			data: '',
			error: function(objeto, quepaso, otroobj){
				$("#txttodos").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
			},
			success: function(datos){
				$("#txttodos").html(datos);
		  },
  			statusCode: {
			    404: function() {
				$("#txttodos").html("<div class='alert alert-danger'>No existe URL</div>");
		    }
			}
    });
 }
 function NotificacionTotal()
 {
	    $.ajax({
			type: "GET",
			url: '{{ URL::to("getnotificacuenta") }}',
			data: '',
			error: function(objeto, quepaso, otroobj){
				$("#txtcontar").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
			},
			success: function(datos){
				$("#txtcontar").html(datos);
		  },
  			statusCode: {
			    404: function() {
				$("#txtcontar").html("<div class='alert alert-danger'>No existe URL</div>");
		    }
			}
    });
 	 //setTimeout(NotificacionTotal, 5000);
 }
 //setTimeout(NotificacionTotal, 1000);


 </script>
</head>
<body class="top-navigation">
<div id="wrapper">
        <div id="page-wrapper" class="gray-bg bgbody">
        <div class="row border-bottom white-bg responsiveagcm">
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <i class="fa fa-reorder"></i>
                </button>
                <a href="#" class="navbar-brand">{{ trans('html.main.sistema') }}</a>
            </div>
            <div class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-top-links navbar-right">
                    @if(isset(Auth::user()->id))
                    <li>
                            @if(Auth::user()->id==1)
                            <a href="#">
                                <i class="fa fa-user"></i> {{ Auth::user()->name }}
                            </a>
                            @else
                            <a href="{{ URL::to('cambiardatos/'.Auth::user()->id.'/edit') }}">
                                <i class="fa fa-user"></i> {{ Auth::user()->name }}
                            </a>
                            @endif
                    </li>
                    @if (Session::has('current-user'))
                        <li>
                            <a href="{{ route('revertir') }}">
                                <i class="fa fa-user"></i>
                                Revertir Sesión
                            </a>
                        </li>
                    @endif
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i>
                            Salir
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    @else
                     <li>
                        <a href="#">
                                <i class="fa fa-user"></i> Invitado
                            </a>
                     </li>
                     <li>
                         <a href="{{ URL::to('auth/login') }}">
                            <i class="fa fa-sign-out"></i> Iniciar Sesión
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
        </nav>
        </div>
    <!-- Cuerpo -->
    <div class="row wrapper border-bottom white-bg page-heading">
                    @yield('contenido')
            </div>

        <div class="footer">
            <div class="pull-right">
                <!--10GB of <strong>250GB</strong> Free. -->
                {{ isset(Auth::user()->name) ? Auth::user()->name : '' }}
            </div>
            <div>
                {!! trans('html.main.copyright') !!}
            </div>
        </div>

        </div>
        </div>

</body>

</html>
