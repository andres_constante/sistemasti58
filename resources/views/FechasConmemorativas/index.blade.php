<?php
if(!isset($nobloqueo))
   Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{$title}} @stop

@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }


    thead tr {
        color:#000;
        /* background-color:#fff; */
    }

    .DTTT_Print #page-wrapper {
        margin: 0;
        background: #fff;
    }

    button.DTTT_button,
    div.DTTT_button,
    a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    button.DTTT_button:hover,
    div.DTTT_button:hover,
    a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <h1 style="margin-bottom: 2%">{{$title}}</h1> <hr><a href="/coordinacioncronograma/FechasConmemorativas/create"><button class="btn btn-primary">Nuevo</button></a>

         <table id="example" class="display compact" style="width:100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Fecha</th>
                    <th>Descripción</th>
                    <th>Opciones</th>
                </tr>
            </thead>

            <tbody>@foreach ($data as $d)
                <tr>
                    <td>{{$d->nombre}}</td>
                    <td>{{$d->fecha}}</td>
                    <td>{{$d->descripcion}}</td>
                    <td><a href="/coordinacioncronograma/FechasConmemorativas/edit/2"><i class="fa fa-edit"></i> Editar</a> <a data-target="#modal-delete-{{$d->id}}" data-toggle="modal"> <i class="fa fa-trash"></i> Eliminar</a></td>
                    @include('FechasConmemorativas.delete')
                </tr>@endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Nombre</th>
                    <th>Fecha</th>
                    <th>Descripción</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>


@stop

@section ('scripts')

<script>

    $(document).ready(function () {

         var table = $('#example').DataTable({
            dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
            "<'row'<'col-xs-12't>>"+
            "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
            processing: true,
            serverSide: false,
            searching: false,
            responsive: true,
            order: [[ 3, 'ASC' ]],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
            dom: 'Blfrtip',
              language: {
                    "emptyTable": "No hay datos disponibles en la tabla",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtered from _MAX_ total entries)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontraron registros coincidentes",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Atrás"
                    },
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
            buttons: [
                {
                    extend: 'pdfHtml5',
                    title: 'Turnos atendidos',
                    text: '<i class="fa fa-file-pdf"></i> PDF'
                },
                {
                    extend: 'excelHtml5',
                    title: 'Turnos atendidos',
                    footer: true
                },
            ]
        });



    });
</script>
@stop