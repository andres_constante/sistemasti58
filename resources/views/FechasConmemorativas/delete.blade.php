
<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$d->id}}">
    {!! Form::open(['route' => 'FechasConmemorativas.destroy', 'method' => 'POST', 'id' => "form", 'class' => 'form-horizontal','files' => true]) !!}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <input type="hidden" name="id" value="{{$d->id}}">
                <button type="button" class="close" data-dismiss="modal" 
                aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Eliminar fecha conmemorativa</h4>
            </div>
            <div class="modal-body">
                <p>Confirme si desea eliminar la fecha conmemorativa</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>  Confirmar</button>
            </div>
        </div>
    </div>
    {{Form::Close()}}
</div>


