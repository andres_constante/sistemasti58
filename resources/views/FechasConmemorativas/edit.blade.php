<?php
if(!isset($nobloqueo))
   Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{$title}} @stop
@section ('scripts')
<script>
   
$(document).ready(function () {
    $('.input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false, //True para mostrar Numero de semanas
        autoclose: true,
        format: 'yyyy-mm-dd',
        language: 'es'
    });
});
</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }


    thead tr {
        color:#000;
        /* background-color:#fff; */
    }

    .DTTT_Print #page-wrapper {
        margin: 0;
        background: #fff;
    }

    button.DTTT_button,
    div.DTTT_button,
    a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    button.DTTT_button:hover,
    div.DTTT_button:hover,
    a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')


<div class="ibox float-e-margins">
    <div class="ibox-title">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <h2 style="margin-bottom: 2%; color: #000">{{$title}}</h2>
        {!! Form::open(['route' => 'FechasConmemorativas.update', 'method' => 'POST', 'id' => "form", 'class' => 'form-horizontal','files' => true]) !!}
            <div class="row " style="margin: 20px">
                <div class="col-xs-6">
                    <div class="form-group" style="padding: 0%">
                        <p style="padding: 0%; font-weight: bold;">Nombre:</p>
                        <input   id="nombre" type="text" name="nombre" class="form-control" value="{{$data->nombre}}">
                        <input   id="id" type="hidden" name="id" class="form-control" value="{{$data->id}}">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group" style="padding: 0%">
                        <p style="padding: 0%; font-weight: bold;">Fecha del conmemorativa:</p>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input onchange="searchDataGeneral()" name="fecha"  id="fecha" type="text" class="form-control" value="{{$data->fecha}}">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group" style="padding: 0%">
                        <p style="padding: 0%; font-weight: bold;">Descripción:</p>
                        <textarea type="text" class="form-control" name="descripcion" id="descripcion" rows="3" cols="" >{{$data->descripcion}}</textarea>
                    </div>
                </div>
                <div class="col-xs-12 text-center"><br>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div> 
         {!! Form::close() !!}
    </div>
</div>


@stop