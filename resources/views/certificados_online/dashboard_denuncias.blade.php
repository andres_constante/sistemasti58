<?php
if(!isset($nobloqueo) && Auth::user()->id_perfil!=4)
    Autorizar(Request::path());
?>

<!-- @extends ('layout') -->
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
@include('certificados_online.jsDenuncias')


</script>
@stop
@section('estilos')
 <style>
      .chart {
          width: 100%; 
          min-height: 450px;
        }


        body.DTTT_Print {
            background: #fff;

        }
        .nopadding{
            padding: 0%;
        }
        .nomargin{
            margin: 0%;
        }
        .tet_center{
            text-align: center
        }

        .DTTT_Print #page-wrapper {
            margin: 0;
            background: #fff;
        }
        .titlecert{
            padding: 0%; font-weight: bold; color:#000;
            text-transform: uppercase;
        }
        .titlecant{

            font-weight: bold; font-size: 2.5em;
        }

        .card_azul{
            padding: 0.2%; 
            background-color: #0097e6; 
            color: #fff;
            border-radius: 10px !important;
        }
        .card_rosa{
            padding: 0.2%; 
            background-color: #0a3d62; 
            color: #fff;
            border-radius: 10px !important;
        }
        .card_verde{
            padding: 0.2%; 
            background-color: #44bd32; 
            color: #fff;
            border-radius: 10px !important;
        }
        button.DTTT_button, div.DTTT_button, a.DTTT_button {
            border: 1px solid #e7eaec;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }
        .fondoblanco{
            background-color: #fff;
        }
        .title_card{
            font-weight: bold; 
            font-size: 1.2em;
            text-transform: uppercase;
             text-shadow: 2px 2px #333 !important;
        }

        .card_blanco{
            padding: 0.2%; 
            background-color: #eb4d4b; 
            color: #fff;
            border-radius: 10px !important;
        }

        .card_sub_title{
            padding-left: 4%; 
            padding-top: 0%; 
            padding-bottom: 0%;
            font-size: 0.9em !important;
             text-shadow: 0.5px 0.5px #333 !important;
        
        }

        .card_value{

            padding-left: 4%;
            font-weight: bold;
            font-size: 2em !important;
            text-shadow: 2px 2px #333 !important;
        }

        button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
            border: 1px solid #d2d2d2;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
            
        }

        .dataTables_filter label {
            margin-right: 5px;

        }
</style>

@stop
@section ('contenido')

{{ HTML::script('js/exporting.js')}}
    
<div style="height:1000px;width:100%;overflow:auto; padding: 0%; margin: 0%">
        
    <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0%; height: 100%">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 100%">
                
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style=" padding-left: 0%; height: 100%">
                    <div class="form-group">
                    <p class="titlecert">Desde:</p>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                           <!--  <input onchange="searchData()"  id="date_added" type="text" class="form-control" value="2019-01-01"> -->
                            <input onchange="search_certificados()"  id="date_added" type="text" class="form-control" value="{{$inicio_fin}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="height: 100%">
                    <div class="form-group" >
                        <p class="titlecert">Hasta:</p>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input onchange="search_certificados()" id="date_added_fin" type="text" class="form-control" value="{{$inicio_fin}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tet_center nopadding nomargin titlecert" style="padding-bottom: 2%">
                Documentos digitales entregados
            </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0%" id="detalles_tramites"></div> 
        </div>


        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0%; height: 100%"  id="container" ></div>
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0%; height: 100%">
            

        </div>
    </div>

</div>

    

  



@stop
