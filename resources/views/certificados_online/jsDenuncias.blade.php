<script type="text/javascript">
    var host = 'https://portalciudadano.manta.gob.ec/';
    // var host = 'https://portalciudadano.manta.gob.ec/';
    $(document).ready(function () {
        search_certificados();


          $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es'
            });

      
    });


    function convertDateFormat(string) {
        var info = string.split('/').reverse().join('-');
        return info;
    }


    function graficar(name, data_serie){

        Highcharts.chart('container', {
        title: {
            text: 'TOTAL DE TRAMITES Y CERTIFICADOS DIGITALES'
        },
        colors: ['#2196f3', '#8bc34a', '#e65100', '#1a237e', '#1a237e',
                    '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
        xAxis: {
            categories: name
        },
        labels: {
            items: [{
                html: 'Total de certificados entregados',
                style: {
                    left: '50px',
                    top: '18px',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'black'
                }
            }]
        },
        series: data_serie
    });


    }

    function search_certificados(){
        var dataInicio =  $('#date_added').val();
        var dataFin = $('#date_added_fin').val();
        convertDateFormat(dataInicio);
        $.ajax({
            type: 'post',
            url: host+"api/export_certificados",
            data: {
                'FechaHome': dataInicio,
                'FechaEnd': dataFin,
            },
            success: function (data) {
                $('#detalles_tramites').html('');
                data['totales'].forEach(d => {
                    var data_html = '<div class="col-lg-2 col-md-6 col-sm-12 col-xs-12"><div class="small-box '+d.color+'"><div class="inner" ><p class="card_sub_title nopadding nomargin"><br>Total de certificados solicitados</p><p class="card_value nopadding nomargin" >'+d.sol+'<br><p class="card_sub_title nopadding nomargin">Total de certificados entregados</p><p class="card_value nopadding nomargin">'+d.ent+'</p></div></div></div>';
                    $('#detalles_tramites').append(data_html);
                });

                data['data'].forEach(d => {
                    var data_html = '<div class="col-lg-2 col-md-6 col-sm-12 col-xs-12"><div class="small-box '+d.color+'"><div class="inner" ><p class="title_card">'+d.name+'</p><p class="card_value nopadding nomargin" >'+d.data.sol+'</p><p class="card_sub_title nopadding nomargin">'+d.data.val+'<br>'+d.data.ven+'</p></div></div></div>';
                    $('#detalles_tramites').append(data_html);
                });

               graficar(data['name'], data['grafica']);
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
  </script>  


      
    
        


