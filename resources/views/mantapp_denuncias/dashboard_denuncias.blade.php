<?php
if(!isset($nobloqueo) && Auth::user()->id_perfil!=4)
    Autorizar(Request::path());
?>

<!-- @extends ('layout') -->
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
@include('mantapp_denuncias.jsDenuncias')


</script>
@stop
@section('estilos')
 <style>
        .chart {
          width: 100%; 
          min-height: 450px;
        }
        body.DTTT_Print {
            background: #fff;
        }
        .DTTT_Print #page-wrapper {
            margin: 0;
            background: #fff;
        }
        button.DTTT_button, div.DTTT_button, a.DTTT_button {
            border: 1px solid #e7eaec;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }
        button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
            border: 1px solid #d2d2d2;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }

        .dataTables_filter label {
            margin-right: 5px;

        }

         td{
        color: #000 !important;
        font-size: 0.88em;
    }

    th{
        font-weight: bold !important;
        color: #072e6b !important;
        text-align: center !important;
        text-transform: uppercase !important;
        font-size: 0.88em;

    }

    .scroll {
     width:100%;
     height:1000px;
     overflow:auto;
    }

    .dataTables_info {
        font-size: 0.8em;
    }

    .pagination {
        font-size: 12
    }

    ul li {
        padding-left: 0%;
    }
    </style>

<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')

{{ HTML::script('js/exporting.js')}}
    
<div style="height:1000px;width:100%;overflow:auto; padding: 0%; margin: 0%">
        <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div>
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0%; height: 100%">

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group" style="padding: 0%">
                        <p style="padding: 0%; font-weight: bold;">Desde:</p>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input onchange="searchDataGeneral()"  id="date_added" type="text" class="form-control" value="{{$inicio_fin}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group" style="padding: 0%">
                        <p style="padding: 0%; font-weight: bold;">Hasta:</p>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input onchange="searchDataGeneral()" id="date_added_fin" type="text" class="form-control" value="{{$inicio_fin}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group" style="padding: 0%">
                        <p style="padding: 0%; font-weight: bold;">Entidades:</p>
                        <div class="input-group">
                            <select onchange="searchDataGeneral()" class="js-example-basic-single" name="entidades" id="Selectentidades">
                                <option value="0">TODOS</option>
                            @foreach ($entidades as $d)
                                <option value="{{$d->id}}">{{$d->nombre_entidad}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0%; height: 100%">
                <div id="data_busqueda" >
                    <a target="_blank" href="documentos_view_incidencias/2000-01-01/2024-01-01/0"><i class="fa fa-download" style="padding-left: 3%; color:#0d47a1 "> Descargar Informe</i></a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="box box-success" style="height: 100%">
                        <div class="box-header with-border" style="height: 100%; padding: 0%; padding-bottom: 4%">
                            <div class="col-lg-12" style="padding-top: 0%">
                                <p style=" padding-bottom: 0%; padding-top: 3%; font-weight: bold; color: #000; font-size: 1.2em;">TOTAL</p>
                                <h1 style="font-weight: bold; padding-top: 0%; font-size: 2.5em; text-align: center; padding: 1%" id="total_denuncias">0</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <div class="small-box" style="padding: 0.3%; background-color: #FFF700; color: #fff">
                        <div class="inner" >
                            <p style="font-weight: bold; font-size: 1.2em">REVISIÓN</p>
                            <h3 id="total_revision" style="padding-left: 2%;">0</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <div class="small-box" style="padding: 0.3%; background-color: #1a237e; color: #fff">
                        <div class="inner">
                            <p style="font-weight: bold; font-size: 1.2em">EN PROCESO</p>
                            <h3 id="total_proceso" style="padding-left: 2%">0</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <div class="small-box" style="padding: 0.3%; background-color: #0091ea; color: #fff">
                        <div class="inner">
                            <p style="font-weight: bold; font-size: 1.2em">POR EJECUTAR</p>
                            <h3 id="total_reprogramado" style="padding-left: 2%">0</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                    <div class="small-box" style="padding: 0.3%; background-color: #1b5e20; color: #fff">
                        <div class="inner">
                            <p style="font-weight: bold; font-size: 1.2em; color: #fff">FINALIZADO</p>
                            <h3 id="total_finalizado" style="padding-left: 2%">0</h3>
                        </div>
                    </div>
                </div>                
                
            </div>
        </div>
   

    <div style="margin-left: 10px; margin-right: 10px; padding: 0%"  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-success" style="height: 100%">
                <div class="box-header with-border" style="height: 100%;">
                    <div class="col-lg-12" style="padding-top: 0%">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div id="linechart_material_barras" class="chart"><div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" hidden>
                            <div id="linechart_material_two" ><div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary" style="height: 100%">
                <div class="box-header with-border" style="height: 100%;">
                    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 0%">
                            <div id="linechart_material_barras_ejecutar" class="chart"><div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary" style="height: 100%">
                <div class="box-header with-border" style="height: 100%;">
                    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 0%">
                            <div id="linechart_material_barras_entidades" class="chart"><div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-info" style="height: 100%">
                <div class="box-header with-border" style="height: 100%;">
                    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 0%">
                            <div id="linechart_material_resulestas_dia" class="chart"><div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-info" style="height: 100%">
                <div class="box-header with-border" style="height: 100%;">
                    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 0%">
                            <div id="linechart_material_resulestas_semana" class="chart"><div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0%" > 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right ui-sortable-handle">
                  <li class="active"><a href="#lineas-chart4" data-toggle="tab" aria-expanded="true" style="color: #000; font-size: 1em">Incidencias con mas reportes</a></li>
                </ul>
                <div class="tab-content no-padding">
                    <div class="chart tab-pane active" id="lineas-chart4" style="position: relative; height: relative">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                </div>
                                <div class="box-body" >
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linechart_material_div23" style="padding: 0%">
                                        <div id="linechart_top_15_55" ><div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right ui-sortable-handle">
            
                  <li class="active"><a href="#lineas-chart_four_dr" data-toggle="tab" aria-expanded="true" style="color: #000; font-size: 0.8em">USUARIOS</a></li>
                  <li class=""><a href="#curvas-chart_five_fr" data-toggle="tab" aria-expanded="false" style="color: #000; font-size: 0.8em">FUNCIONARIOS</a></li>
                </ul>
                <div class="tab-content no-padding">
                 <div style="text-align: center;"> <h3>AGOSTO 2019 - ACTUALIDAD</h3> </div>
                    <div class="chart tab-pane active" id="lineas-chart_four_dr" style="position: relative; height: relative">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                <b>Total de Incidencias</b>
                                </div>
                                <div class="box-body" >
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linechart_material_div23" style="padding: 0%">
                                        <div id="linear_round" >hola<div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                <b>Incidencias con menor tiempo de respuesta</b>
                                </div>
                                <div class="box-body" >
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linechart_material_div23" style="padding: 0%">
                                        <div id="linechart_top_10_resueltas" ><div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="chart tab-pane " id="curvas-chart_five_fr" style="position: relative; height: relative" >
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                <b>Total de Incidencias</b>
                                </div>
                                <div class="box-body">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linechart_material_div2" style="padding: 0%">
                                        <div id="linear_round2" ><div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                <b>Incidencias con menor tiempo de respuesta</b>
                                </div>
                                <div class="box-body">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linechart_material_div2" style="padding: 0%">
                                        <div id="linechart_top_10_resueltas_dos" ><div style="text-align: center; padding: 0%"><img src="img/loading35.gif"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        
    </div>

    <div style="margin-left: 10px; margin-right: 10px; padding: 0%" hidden class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-info" style="height: 100%">
                <div class="box-header with-border" style="height: 100%;">
                    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 0%">
                        <table id="example" class="display compact" style="width:100%">
                            <thead>
                                <tr>
                                    <th>COÓDIGO</th>
                                    <th>ENTIDAD</th>
                                    <th>NOMBRE DE DENUNCIA</th>
                                    <th>USUARIO</th>
                                    <th>CÉDULA</th>
                                    <th>TELÉFONO</th>
                                    <th>ESTADO</th>
                                    <th>CREACIÓN</th>
                                    <th>SOLUCIÓN </th>
                                    <th>SOLUCIÓN REAL</th>
                                    <th>REPROGRAMACIÓN</th>
                                </tr>
                            </thead>

                            <tbody>@foreach ($denuncia as $d)
                                <tr>
                                    <td>{{$d->codigo}}</td>
                                    <td>{{$d->entidad}}</td>
                                    <td>{{$d->nombre_entidad}}</td>

                                    <td>{{$d->name}}</td>
                                    <td>{{$d->cedula}}</td>
                                    <td>{{$d->telefono}}</td>

                                    <td>{{$d->estado}}</td>
                                    <td>{{$d->creacion}}</td>
                                    <td>{{$d->solucion}}</td>
                                    <td>{{$d->fecha_real}}</td>
                                    <td>{{$d->reprogramacion}}</td>
                                   
                                </tr>@endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>COÓDIGO</th>
                                    <th>ENTIDAD</th>
                                    <th>NOMBRE DE DENUNCIA</th>
                                    <th>USUARIO</th>
                                    <th>CÉDULA</th>
                                    <th>TELÉFONO</th>
                                    <th>ESTADO</th>
                                    <th>CREACIÓN</th>
                                    <th>SOLUCIÓN </th>
                                    <th>SOLUCIÓN REAL</th>
                                    <th>REPROGRAMACIÓN</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

    

  



@stop
