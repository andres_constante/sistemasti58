<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title> Reporte de incidencias</title>
</head>
<head>
<style type="text/css">
  .tb{
    border-collapse: collapse;
    border:0.1px solid black;
    font-size: 0.8em; 
    padding-left: 1%;
        padding:0.12%;
    border-color: #000;
    
  }
   body::after {
             content: "";
             background: url('{{ asset('img/escudo44.jpg') }}');
             opacity: 0.2;
             top: 0;
             left: 0;
             bottom: 0;
             right: 0;
             background-repeat: no-repeat;
             position: absolute;
             background-position: center;
             z-index: -1;   
    }  

    body {
      font-variant: unicase;
       font-family:'Helvetica','Verdana','Monaco',sans-serif;
    background-attachment: fixed;
    background-color: #FFFFFF;
  }

        * {
            margin: 0;
            padding: 0;
        }
        html,body {
            height:100%;
        }
        #wrapper {
          padding-right: 1%;
          padding-left: 1%;
          /*height: 600px;*/
            /*min-height:100%;*/
        }
        footer {
            position: relative;
            margin-top: -50px;
            padding:5px 0px;
            clear: both;
            text-align: center;
            color: #fff;
        }


    .sin_bordes {
        width: 100%;
    }

    .bordes {
        border: 1px solid #ddd;
    }

    #tb_segj {
        border-collapse: collapse;
    }

    #barra {
        position: relative;
        left: 0%;
    }

    #barra p {
        position: relative;
        left: -7%;
        font-size: 12px;
    }


    </style>
</head>

<body>
    <div id="wrapper">
        <section>
            <div class='define' style="padding: 1%; padding-top: 3%">
                <table style=" width: 100%; padding-top: 0%;">
                    <th style="width: 20%; text-align: left;"  >
                        <table style="border-collapse: collapse; width: 100%; text-align: left;">
                            <tr><th style="padding-left: 10%; color: red; font-size: 16; width: 50px"><img src="{{URL::to('img/mantafirmes.png')}}"></th></tr>
                        </table>
                    </th>
                    <th style="width: 80%; padding-left: 5%"  valign="top">
                        <table style="text-align: left; width: 100% ">
                            <tr style="width: 100%; padding: 6%">
                                <th  style=" font-size: 1em; text-align: center;"><b>GOBIERNO AUTÓNOMO DESCENTRALIZADO MUNICIPAL DEL CANTÓN MANTA</b></th>
                            </tr>
                        </table>
                    </th>
                </table>
                <table style=" width: 100%; padding-top: 0%">
                <th style="width: 100%; text-align: center; display: flex; justify-content: center; align-items: center;"><img src="{{ asset('img/lineafirmes2.png') }}"></th></table>
            </div>

            <div>
                <p style="text-transform: uppercase; font-weight: bold; color: #000; text-align: center; font-size: 1em; padding: 3%: padding-top: 4%">Reporte de Incidencias</p>
            </div>
            <div style="padding-left: 10%; padding-right: 10%; padding-top: 0.5%; padding-bottom: 1%">
                <p style="font-size: 0.8em">Reporte de pagos realizados desde el portal web https://portalciudadano.manta.gob.ec;  desde: {{$date_inicio}}, hasta: {{$date_fin}}</p>
            </div>

            <div style="padding-bottom: 0%; padding-top: 0%; padding-left: 10%; padding-right: 10%; ">
                <div style="padding-top: 2%;">
                    <table style="width: 100%" class="tb">
                        <thead>
                            <tr style="text-align: center;"><th colspan="2" style="text-transform: uppercase; padding: 0.5%; background-color: #00b0ff">Resumen de las incidencias registradas por estado</th></tr>
                            <tr style="text-align: center; background-color: #bdbdbd; ">
                                <th class="tb" style="text-transform: uppercase; ">ESTADO</th>
                                <th class="tb" style="text-transform: uppercase; ">TOTAL</th>
                            </tr>   
                        </thead>
                        <tbody >
                        @foreach ($data as $d)
                            <tr style="text-align: center;">
                                <td class="tb" style="text-align: left; ">{{$d->estado_denuncia}}</td>
                                <td class="tb" style="text-align: right; padding-right: 2%;">
                                    <?php 
                                        echo(number_format((float)$d->total, 0, '.', ','));
                                    ?>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr style="background-color: #b3e5fc">
                                <td class="tb" style="text-align: center; font-weight: bold;font-size: 1em">TOTAL</td>
                                <td class="tb" style="text-align: center; color: #000; font-weight: bold; font-size: 1em">
                                    <?php 
                                        $var = 0;
                                            foreach ($data as  $d) {
                                                $var += floatval($d->total);
                                            }
                                        echo($var);
                                    ?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
              </div>

                <div style="padding: 1%; padding-top: 1%; padding-left: 10%; padding-right: 10%; ">
                    <table style="width: 100%" class="tb">
                        <thead>
                            <tr style="text-align: center;"><th colspan="5" style="text-transform: uppercase; padding: 0.5%; background-color: #00b0ff">Resumen de las incidencias por entidad</th></tr>
                            <tr style="text-align: center; background-color: #bdbdbd; ">
                                <th class="tb" style="text-transform: uppercase; ">ENTIDAD</th>
                                <th class="tb" style="text-transform: uppercase; ">ASIGNADAS</th>
                                <th class="tb" style="text-transform: uppercase; ">RESUELTAS</th>
                                <th class="tb" style="text-transform: uppercase; ">PROCESO/REVISION</th>
                                <th class="tb" style="text-transform: uppercase; ">REPROGRAMADAS</th>
                            </tr>   
                        </thead>
                        <tbody >
                        @foreach ($data3 as $d)
                            <tr style="text-align: center;">
                                <td class="tb" style="text-align: left; ">{{$d->nombre_entidad}}</td>
                                <td class="tb" style="text-align: right; padding-right: 2%;">
                                    <?php 
                                        echo(number_format((float)$d->asignadas, 0, '.', ','));
                                    ?>
                                </td>
                                <td class="tb" style="text-align: right; padding-right: 2%;">
                                    <?php 
                                        echo(number_format((float)$d->resuletas, 0, '.', ','));
                                    ?>
                                </td>
                                <td class="tb" style="text-align: right; padding-right: 2%;">
                                    <?php 
                                        echo(number_format((float)$d->proceso, 0, '.', ','));
                                    ?>
                                </td>
                                <td class="tb" style="text-align: right; padding-right: 2%;">
                                    <?php 
                                        echo(number_format((float)$d->reprogramadas, 0, '.', ','));
                                    ?>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr style="background-color: #b3e5fc">
                                <td class="tb" style="text-align: center; font-weight: bold;font-size: 1em">TOTAL</td>
                                <td class="tb" style="text-align: center; color: #000; font-weight: bold; font-size: 1em">
                                    <?php 
                                        $var = 0;
                                            foreach ($data3 as  $d) {
                                                $var += floatval($d->asignadas);
                                            }
                                        echo($var);
                                    ?>
                                </td>
                                <td class="tb" style="text-align: center; color: #000; font-weight: bold; font-size: 1em">
                                    <?php 
                                        $var = 0;
                                            foreach ($data3 as  $d) {
                                                $var += floatval($d->resuletas);
                                            }
                                        echo($var);
                                    ?>
                                </td>
                                <td class="tb" style="text-align: center; color: #000; font-weight: bold; font-size: 1em">
                                    <?php 
                                        $var = 0;
                                            foreach ($data3 as  $d) {
                                                $var += floatval($d->proceso);
                                            }
                                        echo($var);
                                    ?>
                                </td>
                                <td class="tb" style="text-align: center; color: #000; font-weight: bold; font-size: 1em">
                                    <?php 
                                        $var = 0;
                                            foreach ($data3 as  $d) {
                                                $var += floatval($d->reprogramadas);
                                            }
                                        echo($var);
                                    ?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div style="padding: 1%; padding-top: 1%; padding-left: 10%; padding-right: 10%; ">
                    <table style="width: 100%" class="tb">
                        <thead>
                            <tr style="text-align: center;"><th colspan="2" style="text-transform: uppercase; padding: 0.5%; background-color: #00b0ff">RESUMEN DE INCIDENCIAS POR TIPO DE DENUNCIA</th></tr>
                            <tr style="text-align: center; background-color: #bdbdbd; ">
                                <th class="tb" style="text-transform: uppercase; ">Tipo de incidencias</th>
                                <th class="tb" style="text-transform: uppercase; ">Numero de Incidencias</th>
                            </tr>   
                        </thead>
                        <tbody >
                          
                        @foreach ($TipoDenuncia as $d)
                            <tr style="text-align: center;">
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d->nombre_denuncia}}</td>
                                <td class="tb" style="text-align: right; margin-right: 1% ">{{$d->valor}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div style="padding: 1%; padding-top: 1%; padding-left: 10%; padding-right: 10%; ">
                    <table style="width: 100%" class="tb">
                        <thead>
                            <tr style="text-align: center;"><th colspan="2" style="text-transform: uppercase; padding: 0.5%; background-color: #00b0ff">Resumen de usuarios con mas reportes</th></tr>
                            <tr style="text-align: center; background-color: #bdbdbd; ">
                                <th class="tb" style="text-transform: uppercase; ">NOMBRE</th>
                                <th class="tb" style="text-transform: uppercase; ">NUMERO DE REPORTES</th>
                            </tr>   
                        </thead>
                        <tbody >
                          
                        @foreach ($UsuariosTop as $d)
                            <tr style="text-align: center;">
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d->name}}</td>
                                <td class="tb" style="text-align: right; margin-right: 1% ">{{$d->canti}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


                <div style="padding: 1%; padding-top: 2%; padding-left: 10%; padding-right: 10%; ">
                    <table style="width: 100%" class="tb">
                        <thead>
                            <tr style="text-align: center;"><th colspan="2" style="text-transform: uppercase; padding: 0.5%; background-color: #00b0ff">Resumen de funcionarios con mas reportes</th></tr>
                            <tr style="text-align: center; background-color: #bdbdbd; ">
                                <th class="tb" style="text-transform: uppercase; ">NOMBRE</th>
                                <th class="tb" style="text-transform: uppercase; ">NUMERO DE REPORTES</th>
                            </tr>   
                        </thead>
                        <tbody >
                          
                        @foreach ($FuncionarioTop as $d)
                            <tr style="text-align: center;">
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d->name}}</td>
                                <td class="tb" style="text-align: right; margin-right: 1% ">{{$d->canti}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


                <div style="padding: 1%; padding-top: 1%; padding-left: 10%; padding-right: 10%; ">
                    <table style="width: 100%" class="tb">
                        <thead>
                            <tr style="text-align: center;"><th colspan="4" style="text-transform: uppercase; padding: 0.5%; background-color: #00b0ff">Resumen de funcionarios con menor tiempo de respuesta</th></tr>
                            <tr style="text-align: center; background-color: #bdbdbd; ">
                                <th class="tb" style="text-transform: uppercase; ">NOMBRE</th>
                                <th class="tb" style="text-transform: uppercase; ">CÓDIGO</th>
                                <th class="tb" style="text-transform: uppercase; ">TIPO DE INCIDENCIAS</th>
                                <th class="tb" style="text-transform: uppercase; ">TIEMPO DE RESPUESTA</th>
                            </tr>   
                        </thead>
                        <tbody >
                          
                        @foreach ($queryFuncionario as $d)
                            <tr style="text-align: center;">
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d['Usuario']}}</td>
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d['codigo ']}}</td>
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d['Nombre_denuncia']}}</td>
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d['Tiempo_solucion']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                 <div style="padding: 1%; padding-top: 1%; padding-left: 10%; padding-right: 10%; ">
                    <table style="width: 100%" class="tb">
                        <thead>
                            <tr style="text-align: center;"><th colspan="4" style="text-transform: uppercase; padding: 0.5%; background-color: #00b0ff">Resumen de ciudadanos con menor tiempo de respuesta</th></tr>
                            <tr style="text-align: center; background-color: #bdbdbd; ">
                                <th class="tb" style="text-transform: uppercase; ">NOMBRE</th>
                                <th class="tb" style="text-transform: uppercase; ">CÓDIGO</th>
                                <th class="tb" style="text-transform: uppercase; ">TIPO DE INCIDENCIAS</th>
                                <th class="tb" style="text-transform: uppercase; ">TIEMPO DE RESPUESTA</th>
                            </tr>   
                        </thead>
                        <tbody >
                        @foreach ($queryCiudadano as $d)
                            <tr style="text-align: center;">
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d['Usuario']}}</td>
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d['codigo']}}</td>
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d['Nombre_denuncia']}}</td>
                                <td class="tb" style="text-align: left; margin-left: 1% ">{{$d['Tiempo_solucion']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>