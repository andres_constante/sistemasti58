<script type="text/javascript">
        var dataInicio;
        var dataFin;
        var Enti;
        var DataPOST;

        var arrayDataPie = [];
        var arrayDataPie2 = [];

        function RealoadDate(){
            dataInicio =  convertDateFormat($('#date_added').val());
            dataFin = convertDateFormat($('#date_added_fin').val());
            Enti = $('#Selectentidades').val();
            DataPOST = {'FechaInicio': dataInicio, 'FechaFin': dataFin,
                    'Identidad': Enti,
                }
            $('#data_busqueda').html('<a target="_blank" href="documentos_view_incidencias/'+dataInicio+'/'+dataFin+'/'+Enti+'"><i class="fa fa-download" style="padding-left: 3%; color:#0d47a1 "> Descargar Informe</i></a>');

            search_incidencias_estado();
            data_denuncias_entidad();
            Lineal_grafig_top_10();
            Lineal_grafig_Linea_de_Time_denucias_Dia();
            Lineal_grafig_detalle_finalizado();
            Lineal_estado_entidades();
        }


        function convertDateFormat(string) {
            var info = string.split('/').reverse().join('-');
            return info;
        }

        function search_incidencias_estado(){
            event.preventDefault();
            $.ajax({
                type: 'post',
                url: "data_estado_denuncias",
                data: {
                    'FechaInicio': dataInicio,
                    'FechaFin': dataFin,
                    'Identidad': Enti,
                },
                success: function (data) {
                   console.log(data);
                   var Drevision = 0;
                   var Dproceso = 0;
                   var Dfinalizado = 0;
                   var Dreprogramado = 0;
                   var Dtotal = 0;
                    var leng = (Object.keys(data).length);
                    for (paso = 0; paso < leng; paso++) {
                        if (data[paso].estado_denuncia === 'EN PROCESO') {
                            Dproceso = data[paso].total;
                            Dtotal = parseInt(Dtotal) + parseInt(Dproceso);
                       }else{
                            if (data[paso].estado_denuncia === 'FINALIZADO') {
                                Dfinalizado = data[paso].total;
                                Dtotal = parseInt(Dtotal) + parseInt(Dfinalizado);
                           }else{
                                if (data[paso].estado_denuncia === 'REPROGRAMADO') {
                                    Dreprogramado = data[paso].total;
                                    Dtotal = parseInt(Dtotal) + parseInt(Dreprogramado);
                               }else{
                                    if (data[paso].estado_denuncia === 'REVISIÓN' || data[0].estado_denuncia === 'REVISION') {
                                        Drevision = data[paso].total;
                                        Dtotal = parseInt(Dtotal) + parseInt(Drevision);
                                    }
                               }
                           }
                       }
                    }

                    $('#total_revision').html(Intl.NumberFormat("en-IN").format(Drevision));
                    $('#total_proceso').html(Intl.NumberFormat("en-IN").format(Dproceso));
                    $('#total_finalizado').html(Intl.NumberFormat("en-IN").format(Dfinalizado));
                    $('#total_reprogramado').html(Intl.NumberFormat("en-IN").format(Dreprogramado));
                    $('#total_denuncias').html(Intl.NumberFormat("en-IN").format(Dtotal));

                    arrayDataPie = [];
                    arrayDataPie.push({ name:'EN PROCESO', y:Dproceso});
                    arrayDataPie.push({ name:'FINALIZADO', y:Dfinalizado});
                    arrayDataPie.push({ name:'REPROGRAMADO', y:Dreprogramado});
                    arrayDataPie.push({ name:'REVISIÓN', y:Drevision});

                    arrayDataPie2 = [];
                    var arrayDataPro = [];
                    arrayDataPro.push('EN PROCESO');
                    arrayDataPro.push(Dproceso);
                        arrayDataPie2.push(arrayDataPro);
                        arrayDataPro = [];
                    arrayDataPro.push('FINALIZADO');
                    arrayDataPro.push(Dfinalizado);
                        arrayDataPie2.push(arrayDataPro);
                        arrayDataPro = [];
                    arrayDataPro.push('REPROGRAMADO');
                    arrayDataPro.push(Dreprogramado);
                        arrayDataPie2.push(arrayDataPro);
                        arrayDataPro = [];
                    arrayDataPro.push('REVISIÓN');
                    arrayDataPro.push(Drevision);
                        arrayDataPie2.push(arrayDataPro);
                        arrayDataPro = [];

                    Show_pie_chart();
                    Show_bar_char();
                   
                },
                error: function (data) {
                    console.log(data);
                }
            });

        }



        var categoriData = [];
        var infoDataSolvencia = [];
        var infoDataFinancieroGratis = [];
        var infoDataFinanciero = [];
        var infoDataNoBienes = [];
        var infoDataCatastro = [];
        var infoDataFinal = [];
        var infoDataSobreposicion = [];
        var dataTTY = '';
        function Lineal_grafig_Linea_de_Tiempo2(){
            event.preventDefault();
            $.ajax({
                type: 'post',
                url: "data_certificados_linea_tiempo",
                data: {
                    'FechaInicio': dataInicio,
                    'FechaFin': dataFin,
                    'Identidad': Enti,
                },
                success: function (data) {
                    categoriData = [];
                    infoDataSolvencia = [];
                    infoDataFinancieroGratis = [];
                    infoDataFinanciero = [];
                    infoDataNoBienes = [];
                    infoDataCatastro = [];
                    infoDataFinal = [];
                    infoDataSobreposicion = [];
                    dataTTY = '';

                    var leng = (Object.keys(data).length);
                    for (paso = 0; paso < leng; paso++) {
                        categoriData.push(data[paso].fecha)
                        infoDataSolvencia.push(data[paso].solvencia);
                        infoDataFinancieroGratis.push(data[paso].financiero_gratis);
                        infoDataFinanciero.push(data[paso].financiero);
                        infoDataNoBienes.push(data[paso].no_bienes);
                        infoDataCatastro.push(data[paso].catastro);
                        infoDataSobreposicion.push(data[paso].sobreposicion);
                    }
                    infoDataFinal.push({name:'SOLVENCIA', data:infoDataSolvencia});
                    infoDataFinal.push({ name:'FINANCIERO', data:infoDataFinanciero});
                    infoDataFinal.push({name:'CATASTRAL', data:infoDataCatastro});
                    infoDataFinal.push({name:'NO POSEER BIENES', data:infoDataNoBienes});
                    infoDataFinal.push({name:'SOBREPOSICIÓN', data:infoDataSobreposicion});
                    Lineal_grafig_barras_tiempo();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }


        var EntidadesAsignadas = [];
        var EntidadesResueltas = [];
        var EntidadesReprogamadas = [];
        var EntidadesProceso = [];
        var EntidadesName = [];
        var EntidadesFinalArray = [];
        function Lineal_estado_entidades(){
            console.log("Data data data");
            // event.preventDefault();
            $.ajax({
                type: 'post',
                url: "data_search_estados_entidades",
                data: {
                    'FechaInicio': dataInicio,
                    'FechaFin': dataFin,
                    'Identidad': Enti,
                },
                success: function (data) {
                    EntidadesAsignadas = [];
                    EntidadesResueltas = [];
                    EntidadesReprogamadas = [];
                    EntidadesFinalArray = [];
                    EntidadesProceso = [];
                    EntidadesName = [];
                    console.log("Data succes");

                    console.log(data);
                    var leng = (Object.keys(data).length);
                    for (paso = 0; paso < leng; paso++) {
                        EntidadesAsignadas.push(data[paso].asignadas);
                        EntidadesResueltas.push(data[paso].resuletas);
                        EntidadesReprogamadas.push(data[paso].reprogramadas);
                        EntidadesProceso.push(data[paso].proceso);
                        EntidadesName.push(data[paso].nombre_entidad);
                    }

                    EntidadesFinalArray.push({name:'ASIGNADO', data:EntidadesAsignadas});
                    EntidadesFinalArray.push({name:'RESUELTO', data:EntidadesResueltas});
                    EntidadesFinalArray.push({name:'PROCESO / REVISIÓN', data:EntidadesProceso});
                    EntidadesFinalArray.push({name:'REPROGRAMADO', data:EntidadesReprogamadas});

                    Show_bar_entidades();
                },
                error: function (data) {
                     console.log("Data error");
                    console.log(data);
                }
            });
        }



        var ArrayFinalCategorisDenuncia = [];
        var ArrayFinalTimeDenuncias = [];
        function Lineal_grafig_Linea_de_Time_denucias_Dia(){
            event.preventDefault();
            $.ajax({
                type: 'post',
                url: "data_denuncias_history",
                data: {
                    'FechaInicio': dataInicio,
                    'FechaFin': dataFin,
                    'Identidad': Enti,
                },
                success: function (data) {
                    ArrayFinalCategorisDenuncia = [];
                    ArrayFinalTimeDenuncias = [];
                    var leng = (Object.keys(data).length);
                    for (paso = 0; paso < leng; paso++) {
                        ArrayFinalCategorisDenuncia.push(data[paso].fecha);
                        ArrayFinalTimeDenuncias.push(data[paso].valor);
                    }
                    Lineal_grafic_time_resletas_por_dia();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }

        function Lineal_grafig_detalle_finalizado(){            
            event.preventDefault();
            $.ajax({
                type: 'post',
                url: "incidencias_respuesta_time",
                success: function (data) {
                    $('#linechart_top_10_resueltas').html('');
                    var leng = 15;
                    var colors = Array('label-success', 'label-primary', 'label-warning');
                    for (paso = 0; paso < leng; paso++) {
                        var number = paso +1;
                        var aleatorio = Math.floor(Math.random()*(colors.length));
                        var seleccionData = colors[aleatorio];
                        var dataHt = '<p style="font-size:0.9em; color:#000"><b style="color:#000; margin-right:3%; font-size:0.9em">'+number+'</b>  '+data[paso].Usuario+'  ('+data[paso].Nombre_denuncia+') <span class="label '+seleccionData+' pull-right"><b style="font-size:1.3em">'+data[paso].Tiempo_solucion+'</b></span></p>';

                        $('#linechart_top_10_resueltas').append(dataHt);
                    }
                    Lineal_grafig_detalle_finalizado_dos();
                    Lineal_grafig_detalle_lary();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }



        function Lineal_grafig_detalle_lary(){            
            event.preventDefault();
            $.ajax({
                type: 'post',
                url: "data_denuncias_tipo",
                success: function (data) {
                    $('#linechart_top_15_55').html('');
                    var leng = 15;
                    var colors = Array('label-success', 'label-primary', 'label-warning');
                    for (paso = 0; paso < leng; paso++) {
                        var number = paso +1;
                        var aleatorio = Math.floor(Math.random()*(colors.length));
                        var seleccionData = colors[aleatorio];
                        var dataHt = '<p style="font-size:0.9em; color:#000"><b style="color:#000; margin-right:3%; font-size:0.9em">'+number+'</b>  '+data[paso].nombre_denuncia+' <span class="label '+seleccionData+' pull-right"><b style="font-size:1.3em">'+data[paso].valor+'</b></span></p>';

                        $('#linechart_top_15_55').append(dataHt);
                    }
                    // Lineal_grafig_detalle_finalizado_dos();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }



        function Lineal_grafig_detalle_finalizado_dos(){            
            event.preventDefault();
            $.ajax({
                type: 'post',
                url: "incidencias_respuesta_time_funcionarios",
                success: function (data) {
                    $('#linechart_top_10_resueltas_dos').html('');
                    var leng = 15;
                    var colors = Array('label-success', 'label-primary', 'label-warning');
                    for (paso = 0; paso < leng; paso++) {
                        var number = paso +1;
                        var aleatorio = Math.floor(Math.random()*(colors.length));
                        var seleccionData = colors[aleatorio];
                        var dataHt = '<p style="font-size:0.9em; color:#000"><b style="color:#000; margin-right:3%; font-size:0.9em">'+number+'</b>  '+data[paso].Usuario+'  ('+data[paso].Nombre_denuncia+') <span class="label '+seleccionData+' pull-right"><b style="font-size:1.3em">'+data[paso].Tiempo_solucion+'</b></span></p>';

                        $('#linechart_top_10_resueltas_dos').append(dataHt);
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }






        function Lineal_grafig_top_10(){
            event.preventDefault();
            $.ajax({
                type: 'post',
                url: "data_usuarios_denuncias_history",
                data: {
                    'FechaInicio': dataInicio,
                    'FechaFin': dataFin,
                    'Identidad': Enti,
                },
                success: function (data) {
                    ArrayFinalCategorisDenuncia = [];
                    var leng = (Object.keys(data).length); 
                    var colors = Array('label-success', 'label-primary', 'label-danger', 'label-warning');
                    $('#linechart_top_10').html('');
                    for (paso = 0; paso < leng; paso++) {
                        var aleatorio = Math.floor(Math.random()*(colors.length));
                        var seleccionData = colors[aleatorio];
                        var number = paso +1;
                        var infoDash = '<p style="font-size:0.9em; color:#000"><b style="color:#000; margin-right:2%; font-size:0.9em">'+number+'</b>  '+data[paso].name+' <span class="label '+seleccionData+' pull-right"><b style="font-size:1.3em">'+data[paso].canti+'</b></span></p>'
                        $('#linechart_top_10').append(infoDash);
                    }
                    // Lineal_grafic_time_resletas_por_dia();
                     Lineal_grafig_top_102();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }


        function Lineal_grafig_top_102(){
            event.preventDefault();
            $.ajax({
                type: 'post',
                url: "data_usuarios_denuncias_history_funcionarios",
                data: {
                    'FechaInicio': dataInicio,
                    'FechaFin': dataFin,
                    'Identidad': Enti,
                },
                success: function (data) {
                    ArrayFinalCategorisDenuncia = [];
                    var leng = (Object.keys(data).length); 
                    var colors = Array('label-success', 'label-primary', 'label-danger', 'label-warning');
                    $('#linechart_top_10_dos').html('');
                    for (paso = 0; paso < leng; paso++) {
                        var aleatorio = Math.floor(Math.random()*(colors.length));
                        var seleccionData = colors[aleatorio];
                        var number = paso +1;
                        var infoDash = '<p style="font-size:0.9em; color:#000"><b style="color:#000; margin-right:2%; font-size:0.9em">'+number+'</b>  '+data[paso].name+' <span class="label '+seleccionData+' pull-right"><b style="font-size:1.3em">'+data[paso].canti+'</b></span></p>'
                        $('#linechart_top_10_dos').append(infoDash);
                    }
                    
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }


        var arrayDataPieInfo = [];
        var arrayDataPieProFull = [];
        function data_denuncias_entidad(){
            var base = [];
            var final = [];
            event.preventDefault();
            $.ajax({
                type: 'post',
                url: "data_denuncias_entidad",
                data: {
                    'FechaInicio': dataInicio,
                    'FechaFin': dataFin,
                    'Identidad': Enti,
                },
                success: function (data) {
                    arrayDataPieInfo = [];
                    arrayDataPieProFull = [];
                    var leng = (Object.keys(data).length);
                    for (paso = 0; paso < leng; paso++) {
                        arrayDataPieInfo = [];
                        arrayDataPieInfo.push(data[paso].nombre_entidad);
                        arrayDataPieInfo.push(data[paso].total);
                        arrayDataPieProFull.push(arrayDataPieInfo);
                    }
                    Show_pie_chart3();
                    Show_pie_chart4();
                },
                error: function (data) {
                    console.log(data);
                }
            });
           

        }

        function Show_bar_entidades(){
            Highcharts.chart('linechart_material_barras_entidades', {
            chart: {
                type: 'column'
            },
            title: {
                 text: 'REPORTE DE INCIDENCIAS POR ENTIDAD'
            },
            subtitle: {
                text: 'https://portalciudadano.manta.gob.ec'
            },
            xAxis: {
                categories: EntidadesName,
                crosshair: true
            },

            yAxis: {
                min: 0,
                title: {
                    text: '<b>cantidad de incidencias</b>',
                },
                labels: {
                    overflow: 'leth'
                }
            },

            colors: ['#4caf50', '#1aadce', '#8bbc21', '#910000', '#1aadce',
                    '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
            tooltip: {
                headerFormat: '<span style="font-size:10px"><b>{point.key}</b></span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:1">{series.name}: </td>' +
                    '<td style="padding:1"><b>  {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            
            credits: {
                enabled: false
            },
            series: EntidadesFinalArray
        });


        //     Highcharts.chart('linechart_material_barras_entidades', {
        //     chart: {
        //         type: 'bar'
        //     },
        //     title: {
        //         text: 'REPORTE DE INCIDENCIAS POR ENTIDAD'
        //     },
        //     subtitle: {
        //         text: 'https://portalciudadano.manta.gob.ec'
        //     },
        //     xAxis: {
        //         categories: EntidadesName,
        //         title: {
        //             text: null
        //         }
        //     },
        //     yAxis: {
        //         min: 0,
        //         title: {
        //             text: '<b>Dirección de Desarrollo, Innovación e Infraestructura Tecnológica</b>',
        //             align: 'high'
        //         },
        //         labels: {
        //             overflow: 'leth'
        //         }
        //     },
        //     tooltip: {
        //         valueSuffix: ' INCIDENCIAS'
        //     },
        //     colors: ['#4caf50', '#1aadce', '#8bbc21', '#910000', '#1aadce',
        //             '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
        //     plotOptions: {
        //         bar: {
        //             dataLabels: {
        //                 enabled: true
        //             }
        //         }
        //     },
        //     legend: {
        //         layout: 'vertical',
        //         align: 'right',
        //         verticalAlign: 'top',
        //         x: -40,
        //         y: 80,
        //         floating: true,
        //         borderWidth: 1,
        //         backgroundColor:
        //             Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        //         shadow: true
        //     },
        //     credits: {
        //         enabled: false
        //     },
        //     series: EntidadesFinalArray
        // });


        }
        
        function Show_bar_char(){
            Highcharts.chart('linechart_material_barras', {
                chart: {
                    type: 'bar',
                    marginLeft: 100
                },
                title: {
                    text: 'ESTADO DE LAS INCIDENCIAS'
                },
                subtitle: {
                    text: 'https://portalciudadano.manta.gob.ec'
                },
                xAxis: {
                    type: 'category',
                    title: {
                        text: null
                    },
                    min: 0,
                    
                    scrollbar: {
                        enabled: true
                    },
                    tickLength: 0
                },
                colors: ['#4caf50', '#0d233a', '#8bbc21', '#910000', '#1aadce',
                    '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
                yAxis: {
                    min: 0,
                    title: {
                        text: '<b>Dirección de Desarrollo, Innovación e Infraestructura Tecnológica</b>',
                        align: 'left'
                    }
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                series: 
                [{
                    name: 'REPORTE DE INCIDENCIAS',
                    data: arrayDataPie2
                }]
            });
        }


        function Show_pie_chart(){
            // Highcharts.chart('linechart_material_two', {
            //     chart: {
            //         plotBackgroundColor: null,
            //         plotBorderWidth: null,
            //         plotShadow: false,
            //         type: 'pie'
            //     },
            //     title: {
            //         text: 'ESTADO DE LAS INCIDENCIAS'
            //     },
            //     subtitle: {
            //         text: 'https://portalciudadano.manta.gob.ec'
            //     },
            //     tooltip: {
            //         pointFormat: '{series.name}: <b>{point.y}</b>'
            //     },
            //     colors: ['#0d233a', '#1aadce', '#8bbc21', '#910000', '#1aadce',
            //         '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
            //     plotOptions: {
            //         pie: {
            //             allowPointSelect: true,
            //             cursor: 'pointer',
            //             dataLabels: {
            //                 enabled: true,
            //                 format: '<b>{point.name}</b>: <b>{point.y}</b>',
            //                 connectorColor: 'silver'
            //             }
            //         }
            //     },
            //     legend: {
            //         enabled: false
            //     },
            //     credits: {
            //         enabled: false
            //     },

            //     series: [{
            //         name: 'Cantidad',
            //         data: arrayDataPie
            //     }]
            // });
        }

        function Show_pie_chart3(){
            Highcharts.chart('linechart_material_four', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Incidencias Creadas por departamento'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                colors: ['#0d233a', '#f28f43', '#8bbc21', '#910000', '#c42525',
                    '#492970', '#1aadce', '#ffea00', '#77a1e5', '#a6c96a'],
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: <b>{point.y}</b>',
                            connectorColor: 'silver'
                        }
                    }
                },
                series: [{
                    name: 'Porcentaje',
                    data: arrayDataPieProFull
                }]
            });
        }

        function Show_pie_chart4(){
            Highcharts.chart('linechart_material_five', {
                chart: {
                    type: 'bar',
                    marginLeft: 100
                },
                title: {
                    text: 'Incidencias Creadas por departamento'
                },
                subtitle: {
                    text: 'https://portalciudadano.manta.gob.ec'
                },
                xAxis: {
                    type: 'category',
                    title: {
                        text: null
                    },
                    min: 0,
                    
                    scrollbar: {
                        enabled: true
                    },
                    tickLength: 0
                },
                colors: ['#910000', '#0d233a', '#8bbc21', '#910000', '#1aadce',
                    '#492970', '#f28f43', '#77a1e5', '#1aadce', '#a6c96a'],
                yAxis: {
                    min: 0,
                    title: {
                        text: 'INCIDENCIAS',
                        align: 'high'
                    }
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                series:  [{
                    name: 'Asignadas',
                    data: arrayDataPieProFull
                }]
            });
        }

        function Lineal_grafic_time_resletas_por_dia(){
            Highcharts.chart('linechart_material_resulestas_dia', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'INCIDENCIAS RESUESTAS POR DIA'
            },
            subtitle: {
                text: 'https://portalciudadano.manta.gob.ec'
            },
            xAxis: {
                categories: ArrayFinalCategorisDenuncia
            },
            yAxis: {
                title: {
                    text: 'Incidencias resueltas'
                }
            },
            colors: ['#1aadce', '#0d233a', '#8bbc21', '#910000', '#1aadce',
                    '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },

            credits: {
                    enabled: false
                },
            series: [{
                name: 'Incidencias',
                data: ArrayFinalTimeDenuncias
            }]
        });
        }



        function Lineal_grafig_barras_tiempo(){
            Highcharts.chart('linechart_material_curvas', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Certificados Digitales'
                },
                subtitle: {
                    text: 'portalciudadano.manta.gob.com'
                },
                xAxis: {
                    categories: categoriData
                },
                yAxis: {
                    title: {
                        text: 'Certificados Generados'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: infoDataFinal
            });

            LinealGrafigTwo();
        }


        function LinealGrafigTwo(){
            Highcharts.chart('linechart_material_lineal', {
            chart: {
                type: 'areaspline'
            },
            title: {
                text: 'Certificados Digitales'
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 150,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
            },
            xAxis: {
                categories: categoriData,
                plotBands: [{ // visualize the weekend
                    from: 4.5,
                    to: 6.5,
                    color: 'rgba(68, 170, 213, .2)'
                }]
            },
            yAxis: {
                title: {
                    text: 'Certificados Generados'
                }
            },
            tooltip: {
                shared: true,
                valueSuffix: ''
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.5
                }
            },
            series:infoDataFinal
            });

            // setTimeout(searchData, 5000);
        }

        function searchDataGeneral(){
            RealoadDate();
        }


        $(document).ready(function () {
             $('.js-example-basic-single').select2();
             $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es'
            });
            searchDataGeneral();



            $('#example tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input style="width:100%" type="text" placeholder="'+title+'" />' );
            });

            var table = $('#example').DataTable({
                responsive: true,
                "pagingType": "full_numbers",
                lengthMenu: [[20, 30, 50, 100, -1], [20, 30, 50, 100, "All"]],
                order: ([4, 'desc']),
                dom: 'flrBtip',
                buttons: ['print', 'copy', 'excel', 'pdf'],
                buttons: [
                     'csv', 
                     'excel',
                      'pdf', 
                      'print'
                ],
                language: {
                    "emptyTable": "No hay datos disponibles en la tabla",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtered from _MAX_ total entries)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontraron registros coincidentes",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Atrás"
                    },
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
            });

            table.columns().every( function () {
                var that = this;
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            });

        });

  </script>  


      
    
        


