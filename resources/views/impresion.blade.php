<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>@yield('titulo','INSPINIA | Main view')</title>
    @yield('css')
    <script>
    function imprimir()
    {
        window.print();
    }
    </script>
</head>
<body>
              @yield('contenido')
</body>
</html>