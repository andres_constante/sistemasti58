<script type="text/javascript">
$(document).ready(function() {    
$('.mapacantonajax').click(function()
{
  var valor_filtro= $(this).attr("codigo");
  console.log(valor_filtro);
  $('#tbbuzonmain').DataTable({
                destroy: true,
                responsive : true,
                ajax: {
                url: '{{ URL::to("coordinacioncronograma/getComunicacionesFiltroObras") }}'+'?tipo_filtro=PARROQUIA&valor_filtro='+valor_filtro+'&desde=2019-01-01&hasta=2500-01-01',
                dataSrc:"",
                },
                columns: [
                        { data: 'id'},
                        { data: 'parroquia'},
                        { data: 'tipoobra'},
                        { data: 'nombre_obra'},
                        { data: 'calle'},
                        { data: 'numero_proceso'},
                        { data: 'monto'},
                        { data: 'estado_obra'},
                        { data: 'contratista'},
                        { data: 'avance'},
                        { data: 'fecha_inicio'},
                        { data: 'fecha_final'},
                        { data: 'updated_at'},

                    {data: "id" , render : function ( data, type, row, meta ) {
              return type === 'display'  ?
              '<a  id="mostrar" href="listobras/'+data+'"  class="divpopup" target="_blank"> <i class="fa fa-check-square"  aria-hidden="true"> Ver </i></a>&nbsp;&nbsp; @if(isset($delete))<a href="javascript::" onclick="eliminar('+data+',2)"><i class="fa fa-trash"></i> </a>@endif':
                data;
            }}],

                    language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',

                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]])

            });
});
});
</script>

<div class="col-lg-12">
                                        
  <div class="ibox float-e-margins">
    <div class="ibox-title">
      <h5>{{ $value->Descripcion }}</h5>
    </div>
<div class="ibox-content">
<!-- --------------------------------- --> 
<!-- --------------------------------- --> 
<!-- --------------------------------- --> 
<!--INICIO DEL MAPA-->
<div id="mapilla" style="width:90%;margin-left:auto;margin-top:3em;margin-right:auto;text-align: center;" class="table-responsive">
<div class="description"></div>
                
                    <svg id="Layer_1" version="1.0" xmlns="http://www.w3.org/2000/svg" class="mapasize" viewBox="0 0 380.000000 293.000000" preserveAspectRatio="xMidYMid meet">
                        <g transform="translate(0.000000,293.000000) scale(0.110000,-0.110000)" fill="#000000" stroke="none">
                        <path id="manta01" codigo="1" alt="MANTA" class="mapacantonmanta mapacantonajax" d="M2650 2621 c-5 -11 -10 -27 -10 -35 0 -19 -36 -38 -157 -82 -85 -31
-104 -34 -203 -34 -124 0 -117 5 -122 -90 -3 -42 -6 -46 -44 -64 -27 -13 -47
-31 -55 -50 l-14 -31 -146 4 c-163 4 -186 0 -199 -35 -13 -33 -53 -32 -104 3
-78 52 -78 52 -56 83 36 51 20 63 -47 35 -19 -8 -51 -15 -69 -15 -32 0 -36 -4
-66 -65 -18 -35 -29 -68 -25 -72 4 -4 36 -9 70 -11 l62 -4 29 -99 c18 -62 35
-103 46 -109 9 -5 110 -12 223 -16 l207 -7 -25 -43 c-30 -50 -26 -99 9 -108
11 -3 65 -6 119 -6 l98 0 6 -31 c7 -36 33 -49 104 -49 27 0 49 -3 49 -7 0 -5
15 -25 33 -47 l32 -38 42 22 c23 12 47 28 54 36 15 18 60 18 85 -1 29 -22 45
-18 86 21 l37 37 -23 18 c-13 10 -26 32 -30 49 -7 39 -20 32 -52 -31 -16 -33
-30 -49 -41 -47 -21 4 -41 36 -28 46 11 8 45 77 45 92 0 6 -15 31 -34 58 -43
59 -53 94 -32 111 20 17 21 50 2 57 -8 3 -29 -5 -46 -17 l-31 -23 -24 26 -24
27 25 45 c14 25 30 46 35 46 5 0 9 16 9 35 0 30 4 35 28 41 34 7 38 9 87 39
22 13 56 33 77 44 42 24 80 68 76 90 -2 8 -18 19 -36 23 -25 7 -37 18 -48 44
-14 34 -14 37 16 83 16 26 30 53 30 59 0 19 -18 14 -30 -7z"/>
<path id="losesteros" codigo="3" alt="LOS ESTEROS" class="mapacantonesteros mapacantonajax" d="M3343 2601 c-13 -11 -31 -17 -41 -14 -12 3 -43 -13 -84 -45 -89 -67
-157 -97 -263 -118 -50 -9 -96 -18 -102 -20 -7 -2 -13 -17 -13 -33 0 -31 14
-41 56 -41 35 0 40 -25 11 -63 l-25 -33 24 -44 c13 -25 23 -58 24 -75 1 -32
58 -147 76 -152 10 -3 45 30 150 139 23 24 54 62 68 86 21 34 31 42 46 37 18
-6 20 -1 20 50 0 53 3 60 44 106 58 64 84 167 56 220 -13 23 -19 23 -47 0z"/>
<path id="sanmateo" codigo="7" alt="SAN MATEO" class="mapacantonmateo mapacantonajax" d="M2055 2409 c-99 -72 -150 -95 -248 -116 -75 -15 -92 -16 -139 -4 -29
8 -59 18 -66 24 -9 7 -17 5 -27 -9 -23 -31 -17 -47 29 -76 48 -31 76 -36 76
-13 0 28 41 36 198 39 l157 3 9 25 c5 16 26 34 52 47 39 18 44 25 44 53 0 18
3 43 6 56 10 34 -12 27 -91 -29z"/>
<path id="tarqui" codigo="4" alt="TARQUI" class="mapacanton mapacantonajax" d="M2685 2340 c-80 -60 -170 -110 -199 -110 -11 0 -16 -9 -16 -26 0 -14
-11 -39 -25 -56 -26 -30 -33 -72 -15 -83 6 -3 22 3 36 14 29 23 55 16 75 -20
9 -17 7 -25 -11 -44 -27 -28 -22 -48 26 -110 39 -52 42 -80 13 -129 -11 -20
-19 -41 -17 -48 2 -6 19 14 36 45 23 41 38 57 53 57 12 0 36 15 55 34 29 29
33 38 27 72 -3 21 -12 49 -19 61 -8 12 -14 27 -14 32 0 16 141 121 163 121 11
0 29 -8 40 -17 19 -17 19 -17 12 7 -4 14 -14 37 -21 52 -19 36 -18 51 6 81 26
32 25 37 -4 37 -42 0 -55 11 -66 51 -15 55 -39 51 -135 -21z"/>
<path id="eloyalfaro" codigo="2" alt="ELOY ALFARO" class="mapacantonalfaro mapacantonajax" d="M2780 2088 c-36 -27 -64 -55 -63 -62 9 -51 32 -104 51 -119 12 -10
30 -33 39 -50 11 -23 18 -28 21 -17 3 8 14 24 26 36 12 11 26 28 31 38 6 10
27 20 51 23 l42 5 -15 35 c-25 61 -97 164 -112 162 -3 0 -35 -23 -71 -51z"/>
<path id="santamarianita" codigo="5" alt="SANTA MARIANITA" class="mapacantonmariana mapacantonajax"  d="M1267 2083 c-49 -69 -194 -221 -217 -228 -19 -6 -37 -33 -133 -197
-54 -91 -57 -100 -57 -162 0 -73 -15 -154 -36 -194 -19 -36 -18 -78 2 -86 9
-3 32 3 50 15 l34 21 35 -31 c41 -36 55 -39 62 -12 3 11 26 29 52 41 25 12 56
31 68 42 29 27 152 88 178 88 17 0 25 9 34 40 8 29 23 47 50 64 50 32 85 75
106 131 25 64 16 292 -14 379 -12 33 -21 69 -21 82 0 49 -8 54 -85 54 l-73 0
-35 -47z"/>
<path id="sanlorenzo" codigo="6" alt="SAN LORENZO" class="mapacantonlorenzo mapacantonajax" d="M1548 1913 c-14 -4 -17 -24 -20 -152 l-3 -148 -37 -56 c-21 -30 -58
-70 -83 -87 -25 -17 -45 -39 -45 -48 0 -29 -29 -62 -55 -62 -13 0 -41 -11 -61
-25 -20 -14 -47 -25 -59 -25 -13 0 -30 -11 -39 -25 -9 -14 -41 -37 -70 -50
-33 -15 -56 -32 -59 -45 -7 -28 -39 -25 -76 6 -30 25 -32 26 -65 10 -42 -20
-51 -20 -70 0 -22 21 -20 62 5 115 16 32 23 70 27 137 5 89 4 92 -17 92 -15 0
-31 -15 -52 -49 -38 -61 -98 -104 -167 -121 -62 -14 -78 -33 -87 -105 -7 -54
-33 -135 -43 -135 -4 -1 -17 -9 -30 -20 l-23 -19 65 -133 c41 -86 71 -165 85
-223 11 -50 33 -119 47 -155 15 -36 49 -126 74 -200 l48 -135 98 -3 98 -3 45
45 46 44 110 6 c61 4 132 6 158 6 45 -1 52 3 103 52 51 50 55 57 65 120 9 59
8 70 -11 107 l-20 41 241 0 241 0 -4 138 c-3 75 -7 144 -9 152 -3 11 4 14 26
12 18 -2 58 10 95 26 36 16 73 32 83 36 17 5 18 17 15 120 l-3 115 38 39 c29
29 54 43 112 60 48 14 75 27 75 36 0 8 17 32 38 53 l37 38 -25 50 c-14 28 -35
67 -48 88 -22 34 -27 37 -72 37 -70 0 -99 14 -106 50 l-6 30 -93 0 c-51 0
-107 3 -124 6 -30 6 -31 8 -31 58 0 29 6 61 15 73 13 19 13 21 -8 27 -25 6
-343 6 -369 -1z"/>
                        </g>
                    </svg>
                </div>                
                <!--FIN DEL MAPA-->
<!-- --------------------------------- --> 
<!-- --------------------------------- --> 
<!-- --------------------------------- --> 
<!-- --------------------------------- --> 

                                                
<!-- TABLA HTML-->
<table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
                    <thead>
                        <tr>
                           <th>ID</th>
                           @foreach($objetosTable as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                           <th>ACCION</th>
                    </tr>
                    </thead>

                    <tfoot>
                        <tr>
                          <th>ID</th>
                           @foreach($objetosTable as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                           <th>ACCION</th>

                    </tr>
                    </tfoot>
</table>
<!-- -->                                                

                                                            
                                                </div>
                                                </div>
                                                
                                            
                                        </div>