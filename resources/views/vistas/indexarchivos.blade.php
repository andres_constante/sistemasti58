<?php
if(!isset($nobloqueo))
//    Autorizar(Request::path());
    ?>
<!--extends('layout')-->
@extends(Input::has("menu") ? 'layout_basic_no_head':'layout' )
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
    <script type="text/javascript">
        setTimeout(function(){
            parent.$.fn.colorbox.close();
        },2000);
        $(document).ready(function() {
            setTimeout(cargararchivos(),1000);
           function cargararchivos() {
                    $.ajax({
                        type: "GET",
                        url: "{{URL::to('').'/'.$configuraciongeneral[20]}}",
                        data: null,
                        success: function (resultado) {
                            $("#divviewfiles").html(resultado);
                        }
                    });  //Fin Ajax
           }//Fin Function
        });//Document Ready
        function popup(obj)
        {
            //console.log(obj);
            //console.log($(obj).parent().parent());
            var row= $(obj).parent().parent();
            row.css("background-color","#cdf7c8");
            $(obj).colorbox({fixed:true,iframe:true, innerWidth:screen.width -(screen.width * 0.20), innerHeight:screen.height -(screen.height * 0.25)});
        }


        @if(isset($delete))
            @if($delete=='si')
                function eliminar($id)
                {
                        var r= confirm("Seguro de eliminar este registro?");
                        if(r==true)                        
                            $('#frmElimina'+$id).submit();
                        else
                            return false;
                }
            @endif
        @endif


    </script>

@stop
@section ('contenido')
    <h1 style="background-color: #FFFFFF; padding: 1% 0 1% 2%;"> {{ $configuraciongeneral[0] }}</h1>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            @if (Session::has('message'))
                <script>
                    $(document).ready(function() {
                        toastr.options = {
                            "positionClass": "toast-bottom-right"
                        };
                        toastr["success"]("{{ Session::get('message') }}");
                        //$.notify("{{ Session::get('message') }}","success");
                    });
                </script>
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            @if (Session::has('error'))
                <script>
                    $(document).ready(function() {
                        toastr["error"]("{{ Session::get('error') }}");
  });
                </script>
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="ibox-tools">
                <a class="collapse-link" href="javascript:">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="close-link" href="javascript:">
                    <i class="fa fa-times"></i>
                </a>
            </div>
                    <div class="">
                        @if($configuraciongeneral[1]=='usuarios')

                            <label for="cadena"> Nombre</label>
                            <input id="cadena" class="cadena" type="text">
                            <a onclick="mostrar()" class="btn btn-primary" href="javascript:">Mostrar</a>
                        @endif
                        @if($create=='si')
                            <a href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary "><i class="fa fa-align-justify"></i> Todos</a>
                            @if(isset($permisos))
                                @if($permisos->crear=="SI")
                                    <a href="{{ URL::to($configuraciongeneral[1]."/create") }}" class="btn btn-default ">Nuevo</a>
                                @endif
                            @else
                                <a href="{{ URL::to($configuraciongeneral[1]."/create") }}" class="btn btn-default ">Nuevo</a>
                            @endif
                        @endif

                        @if (isset($actividades_agenda))
                            <a onclick="mostrar()" class="btn btn-default " href="javascript:"><i class="fa fa-eye" aria-hidden="true"></i> Agenda</a>
                        @endif
                    </div>

        </div>
        <div class="ibox-content imgback">
        <div class=" col-md-12 col-lg-12" style="background-color: #ffffff;">
        @isset($entidades)
        @foreach($entidades  as $k=> $v)
        
            <div class="col-md-6  zoom col-lg-6" style="text-align: center; padding: 1%;">
                <img class="circulo " style="box-shadow: 0px 6px 5px #0092d6;" src="{{URL::to('')}}/img/entidades/{{$v->icono}}" width="20%" 
                        onclick="javascript:location.href='{{URL::to("").'/'.$configuraciongeneral[1]}}?entidad={{$v->id}}'" height="30%">
        
                <div style="padding-top: 10px;"><p style="font-size: mediun; ">{{$v->nombre}}</p></div>
            </div>
        @endforeach
        @endisset
        @isset($direciones)
        @foreach($direciones  as $k=> $v)
        
            <div class="col-md-2  zoom col-lg-2" style="text-align: center; padding: 1%;">
                <div style="padding: 2px; box-shadow: 0px 6px 5px #0092d6;" onclick="javascript:location.href='{{URL::to("").'/'.$configuraciongeneral[1]}}?entidad={{$v->id}}'"><p style="font-size: mediun; " >{{$v->alias}}
                    </a>
                </p></div>
            </div>
        @endforeach
        @endisset
              
        </div>
        <div id="divindex" class="ibox-content imgback">
                <div class="row" id="divviewfiles">
                <!-- <div class="col-lg-12 animated" >
                   
                </div> -->
            </div>
        </div>
    </div>

@stop




