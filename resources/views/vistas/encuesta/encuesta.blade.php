<?php
  $images=explode("|",ConfigSystem("backgroundlogin"));
  $c=count($images)-1;  
  $imgbk=$images[rand(0,$c)];
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset("favicon.ico") }} " rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <title>Crear</title>
    {!! HTML::style('css/bootstrap.min.css') !!}
    {!! HTML::style('css/card.css') !!}
    {!! HTML::style('font-awesome/css/font-awesome.css') !!}


    <!-- Data Tables -->
    {{ HTML::style('css/plugins/dataTables/dataTables.bootstrap.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.responsive.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.tableTools.min.css') }}

    <!-- Mensaje -->

    {{ HTML::style('css/plugins/toastr/toastr.min.css') }}
    {{ HTML::style('css/animate.css') }}
    {{ HTML::style('css/style.css?v='.rand(1,1000)) }}

    <!--Wizard-->
    {{ HTML::style('css/plugins/steps/jquery.steps.css')}}
    {{ HTML::script('css/plugins/iCheck/custom.css')}}

    @yield('estilos')
    <!-- Mainly scripts -->
    {{ HTML::script('js/jquery-2.1.1.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/plugins/metisMenu/jquery.metisMenu.js') }}
    {{ HTML::script('js/plugins/slimscroll/jquery.slimscroll.min.js') }}


    <!-- Custom and plugin javascript -->
    {{ HTML::script('js/inspinia.js') }}
    {{ HTML::script('js/plugins/pace/pace.min.js') }}
    {{ HTML::script('js/plugins/steps/jquery.steps.min.js') }}
    {{ HTML::script('js/plugins/validate/jquery.validate.min.js') }}

    <!-- Data Tables -->
    {{ HTML::script('js/plugins/dataTables/jquery.dataTables.js') }}
    {{ HTML::script("js/plugins/dataTables/dataTables.bootstrap.js") }}
    {{ HTML::script("js/plugins/dataTables/dataTables.responsive.js") }}
    {{ HTML::script("js/plugins/dataTables/dataTables.tableTools.min.js") }}
    {{ HTML::style('css/plugins/sweetalert/sweetalert.css')}}
    {{ HTML::script('js/plugins/sweetalert/ciudadano/sweetalert.min.js') }}

    <!-- Mensaje -->
    {{ HTML::script('js/plugins/toastr/toastr.min.js') }}

    <!-- CSS Notificacion -->
    {{ HTML::style('ventanas-modales/ventanas-modales.css') }}
    <!-- CSS Notificacion -->
    {{ HTML::script('ventanas-modales/ventanas-modales.js') }}
    <!-- Graficos estadissticos -->
    {{-- {{ HTML::script('js/plugins/morris/raphael-2.1.0.min.js') }} --}}
    {{ HTML::script('js/plugins/morris/raphael.js') }}
    {{ HTML::script('js/plugins/morris/morris.js') }}
    {{ HTML::style('css/plugins/morris/morris-0.4.3.min.css') }}



    <!-- Chosen -->
    {{ HTML::style('chosen/docsupport/prism.css') }}
    {{ HTML::style('chosen/chosen.css') }}

    {{ HTML::script('chosen/chosen.jquery.js') }}
    {{ HTML::script('chosen/docsupport/prism.js') }}
    <!-- ColorBox -->
    {{ HTML::style('colorbox/colorbox.css') }}

    {{ HTML::script('colorbox/jquery.colorbox.js') }}
    <!-- Jquery Validate -->
    {{ HTML::script('js/jquery.validate.min.js') }}

    <!-- DatePicker -->
    {{ HTML::style('bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ HTML::script('bootstrap-datepicker/bootstrap-datepicker.js') }}
    {{ HTML::script('bootstrap-datepicker/locales/bootstrap-datepicker.es.js') }}
    <!-- Selective -->
    {!! HTML::style('selective/selectize.bootstrap3.css'); !!}
    {!! Html::script('selective/selectize.js') !!}
    <!-- Tree -->
    {!! HTML::style('css/plugins/jsTree/style.css'); !!}
    {!! Html::script('js/plugins/jsTree/jstree.min.js') !!}

    <!-- dropzone -->
    {!! HTML::style('css/plugins/dropzone/dropzone.css'); !!}
    {!! Html::script('js/plugins/dropzone/dropzone.js') !!}

    {!! HTML::style('css/plugins/c3/c3.min.css'); !!}
    {!! Html::script('js/plugins/d3/d3.min.js') !!}
    {!! Html::script('js/plugins/c3/c3.min.js') !!}


    {!! Html::script('js/plugins/sparkline/jquery.sparkline.min.js') !!}

    {!! HTML::style('css/plugins/blueimp/css/blueimp-gallery.min.css'); !!}
    {!! Html::script('js/plugins/blueimp/jquery.blueimp-gallery.min.js') !!}
    <!-- {!! Html::script('js/plugins/highcharts/highcharts.js') !!} -->
    
   {{-- ///editor  --}}
    {!! HTML::style('css/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.css'); !!}
    {!! Html::script('js/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.js') !!}

    {{ HTML::style('css/jquery-gallery.css') }}
    {{ HTML::script('js/jquery-gallery.js') }}
    {!! Html::script('js/randomColor.js') !!}


<style type="text/css">
    /* .bgbody {
        background: url({{ asset($imgbk) }}) repeat 0px 0px;
        background-size: cover;
    } */
.middle-box h1 {
    font-size: 100px !important;
}
#myVideo {
  position: fixed;
  right: 0;
  bottom: 0;
  min-width: 100%; 
  min-height: 100%;
  filter:opacity(30%); 
}

tr:hover {
  background-color: #009cdf;
  color: #F5F5F5;
}
/* .container { margin: 150px auto; max-width: 600px; } */
@media (min-aspect-ratio: 16/9) {
    #myVideo {
        width:100%;
        height: auto;
    }
}
@media (max-aspect-ratio: 16/9) {
    #myVideo { 
        width:auto;
        height: 100%;
    }
}

.fade{
    opacity: 1 !important;
}

.disabled {
    pointer-events: none;
    color: #AAA;
    background: #F5F5F5;
  }
</style> 
{{-- @include("vistas.includes.mainjs"); --}}
<script type="text/javascript">
$(function() {
    $('.gallery-slideshow').slideshow({
        // default: 2000
        interval: 3000,
        // default: 500
        width: 850,
        // default: 350
        height: 620
    });
});



$(document).ready(function(){
    $('.solonumeros').on('input', function () { 
        this.value = this.value.replace(/[^0-9.]/g,'');
    });

    $("#parroquia_id").change(function (e) { 
    var parroquia=$(this).val();
    $.ajax({
            type: "GET",
            url: "{{ URL::to('') }}/getBarriosEncuesta/"+parroquia+'?tipo=2',
            success: function (response) {
                    $("#barrio_id").empty();
                    // $("#barrio_id").append("<option value='0'>TODOS</option>");
                    $.each(response, function(key, element) {
                    $("#barrio_id").append("<option value=\'" + element.id + "\'>" + element.barrio+"</option>");
                    });
                    // $('#barrio_id').val(165).change();
                    $("#barrio_id").trigger("chosen:updated");  
                }
            });
});
    
    $("#fecha").attr("disabled", true);
    $("#fecha").val(today(1));
    $(".chosen-select").chosen(
    {
        no_results_text: "No existe coincidencia con lo que busca...",
        placeholder_text_single: "Seleccione...",
        placeholder_text_multiple: "Seleccione...",
        width: "100%",
        // height:"1000"
    });
    $("#celular").attr('maxlength',10);
    $('.cedula').keyup(async function (event) { 

$(".cedula").attr('maxlength',10);
if(this.value.length==10){
    validacion = await validaUsuario(this.value);

    if (!validacion) {
        Swal.fire({
            type: 'error',
            title: 'Alerta',
            text: "Estimado usuario ud ya ha realizado el test"
        }).then((result) => {
            location.reload();
        });

        return;
    }

      $.ajax({
      type: "GET",
      url: "{{URL::to('')}}/consultardatosseguro_data?documento="+this.value+"&tipo=1015",
      data:null,
      // dataType: "dataType",
      success: function (response) {
          
          if(response[0]){
            $("#cargo").val(response[0].cargo);
            $("#fecha_nacimiento").val(response[0].fecha_nacimiento);
            $("#edad").val(response[0].edad);
            $("#nombre").val(response[0].nombre);
            if(response[0].tercera){
              console.log(response[0].tercera);
              
              $("#tercera_edad").val(response[0].tercera);
            }
            if(response[0].dicapacitado){
              $("#discapacitado").val(response[0].dicapacitado);
            }
            $("#cedula_razon").val(response[0].nombre);
            try {
                var nombre_servicio=response[0].nombre.split(' ');
              $("#apellido_custodio").val(nombre_servicio[0]+" "+nombre_servicio[1]);
              $("#nombre_custodio").val(nombre_servicio[2]+" "+nombre_servicio[3]);
            } catch (error) {
              
            }
           
          }else{
            // $("#nombre_beneficiario").removeAttr('readonly');
          }
        
      }
    });
}else{
  var tipo=$("#tipo_documento").val();
  if(tipo=='PASAPORTE'){

  }else{
        this.value = this.value.replace(/[^0-9.]/g,'');
  }
}


});



});

function validaUsuario(cedula) {
    console.log(cedula);
    return new Promise( resolve => {
        $.ajax({
            type: 'GET',
            url: `../validaEncuesta?cedula=${cedula}`,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        }).done(function(msg) {
            resolve(msg['estado']);
        }).fail(function (data) {
            console.log(data, 'data');
            resolve(false);
        });
    });
}

function today(tipo) {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    if(dd<10)
    {
        dd='0'+dd;
    }

    if(mm<10)
    {
        mm='0'+mm;
    }

    today = ( tipo == 1 ) ? dd+'/'+mm+'/'+yyyy : yyyy+'-'+mm+'-'+dd;
    return today;
}


</script>    
</head>

<body class="gray-bglogin bgbody">
<video autoplay muted loop id="myVideo">
  <source src="{{ asset("img/videobg.mp4") }}" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

@if (Session::has('message'))
<script>
  $(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
    $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)}); 
        }); 
});
</script>
<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if (Session::has('warning'))
<script>
  $(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["warning"]("{{ Session::get('warning') }}");
    //$.notify("{{ Session::get('message') }}","success");
    // $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)}); 
    //     }); 
});
</script>
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="alert alert-warning text-center">⚠ Advertencia ⚠ <br>{{ Session::get('warning') }}</div>
  </div>
</div>
@endif



{{--    <div class="form-row" >--}}
{{--        <div class="form-group col-md-12 col-lg-12" style="    margin-top: 2%;">--}}
            <div class="middle-box text-center encuesta animated fadeInDown">
                <div class="row">
                    <div class="col-lg-2">
                        <h1> 
                            {{ HTML::image('img/logo.png','LOGO',array('style'=>'width: 180px;')) }} 
                        </h1>
                    </div>
                    <div class="col-lg-8">
                        <h1 style="font-size: 3em !important"> 
                            Gobierno Autónomo Descentralizado Municipal del Cantón Manta 
                        </h1>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
                <div class="row titulo-encuesta">
                    <b>{{ $configuraciongeneral[0] }}</b>
                </div>
                <div>
                    <div class="panel-body">
                        @if (Session::has('error'))
                            <script>
                                $(document).ready(function() {  
                                    toastr["error"]('@php echo Session::get('error') @endphp', 'Error' , {
                                        enableHtml: true,
                                        closeButton: true,
                                        timeOut: 10000
                                    });
                                });
                            </script>
                            <div class="alert alert-danger" style="text-align: left;">@php echo Session::get('error') @endphp </div>
                        @endif
                        <div class="row">
                            {!! Form::open(['route' => $configuraciongeneral[1], 'method' => 'POST', 'id' => "form"]) !!}
                            @foreach($objetos as $i => $campos)
                            <div id="div_{{$campos->Nombre}}" class="form-group col-md-12 {{((isset($campos->Ancho))?'col-lg-'.$campos->Ancho:'col-lg-6')}}">
                                @if($campos->Tipo != "htmlplantilla")
                                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-12 control-label label_$campos->Nombre", "style" => "text-align: left")) !!}
                                @endif

                                @if($campos->Tipo=="date")
                                    {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datefecha','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                                @elseif($campos->Tipo=="select")
                                    {!! Form::select($campos->Nombre,$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre) : ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : old($campos->Nombre), array('class' => "form-control ".$campos->Clase,((isset($campos->Requerido))?$campos->Requerido:''))) !!}
                                @elseif($campos->Tipo=="select-multiple")
                                    {!! Form::select($campos->Nombre."[]",$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre) : ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                    
                                @elseif($campos->Tipo=="htmlplantilla")
                                    <div class="form-group col-md-12 col-lg-12" id="obj-{{$campos->Nombre}}">
                                        <div class="col-lg-12 " id="{{$campos->Nombre}}">
                                            <?php echo $campos->Valor; ?>
                    
                                        </div>
                                    </div>
                                @elseif($campos->Tipo=="datetimetext")
                                    <div id="date_{{$campos->Nombre}}" class="input-group datetime">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>

                                    {!! Form::text($campos->Nombre, (($campos->Valor != 'Null')?$campos->Valor:null) , array(
                                    'data-placement' =>'top',
                                    'data-toogle'=>'tooltip',
                                    'class' => 'form-control datefecha '.$campos->Clase,
                                    'placeholder'=>str_replace("(*)","",$campos->Descripcion),
                                    $campos->Nombre,
                                    )) !!}
                                    </div>

                                @elseif($campos->Tipo=="textdisabled")
                                {!!   Form::text($campos,Input::old($campos), array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i],'readonly')) !!}
                                    
                                @else
                                    {!! Form::text($campos->Nombre, $campos->ValorAnterior!="Null" ? $campos->ValorAnterior : old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.str_replace('(*)','',$campos->Descripcion), ((isset($campos->Requerido))?$campos->Requerido:'') )) !!}
                                @endif
                            </div>
                            @endforeach
                            <div class="table-responsive-lg">
                                @if (isset($preguntas))
                                <table class="table">
                                    <thead style="background: #2c438f; color: white; font-size: 1em !important">
                                        <tr>
                                          <th scope="col-lg-1">#</th>
                                          <th scope="col-lg-8">PREGUNTAS</th>
                                          <th scope="col-lg-3">RESPUESTAS</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          @foreach($preguntas as $i => $campos)
                                          <tr>
                                            <th scope="row">{{$i + 1}}</th>
                                            <td>
                                                {!! Form::label($campos->pregunta,$campos->pregunta.'',array("class"=>"col-lg-12 control-label label_$campos->pregunta", "style" => "text-align: left")) !!}
                                            </td>
                                            <td>
                                                {!! Form::radio("pregunta_$campos->id", '1' , false) !!} Si
                                                {!! Form::radio("pregunta_$campos->id", '0' , true) !!} No
                                            </td>
                                          </tr>
                                          @endforeach
                                      </tbody>
                                </table>
                                @endif       
                
                            </div>
                            {{-- {!! Form::textarea('observacion', array('class' => 'form-control ','placeholder'=>'Ingrese una observación', 'rows' => 3)) !!} --}}
                            {!! Form::textarea('observacion', null, ['id' => 'observacion', 'rows' => 4, 'class' => 'form-group col-md-12', 'style' => 'resize:none', 'placeholder'=>'Ingrese una observación']) !!}
                            <button type="submit" class="btn btn-success" id="submit">Guardar</button>
                            {!! Form::close() !!}
                        </div>
                    
                    </div>
                    <h3 class="titulo">{{ trans('html.main.sistema') }}</h3>
                  
                    <p class="tab">@yield('cabecera')</p>
                                   @yield('errores')
                    <!--form class="m-t" role="form" action="index.html"-->
                        @yield('formulario')
                        <br>
                    <p class="m-tLogin"> <small>{!! trans('html.main.copyright') !!}</small> </p>
                </div>
            </div>
{{--        </div>--}}
{{--        <div class="form-group col-md-8 col-lg-8" style="    margin-top: 2%;"> --}}

{{--            <div class="animated fadeInDown">--}}
{{--                <?php $imagenes= explode('|',ConfigSystem('imagenes-slide')) ?>--}}

{{--                <ul class="gallery-slideshow">--}}
{{--                    @foreach ($imagenes as $key => $item)--}}
{{--                       <li><img src="{{asset('img_gallery/'.$item)}}" width="100%" height="auto"/></li>--}}
{{--                    @endforeach--}}
{{--                 --}}
{{--                </ul>--}}
{{--              </div>--}}

{{--        </div>--}}

{{--    </div>--}}
{{ HTML::script('js/jquery.snow.min.1.0.js') }}
@if(date("m")==12)
    {{ HTML::script('js/cursor-con-nieve.js') }}
@endif
<script>
    $(document).ready( function(){
        var d = new Date();// Capturo Fecha Actual
        var n = d.getMonth();//Capturo Mes de la Fecha Actual
        //0: Enero, 1: Febrero, 2:Marzo, etc.
        //console.log(n);
        if(n==11)//Diciembre
            $.fn.snow();
    });
</script>
</body>

</html>
