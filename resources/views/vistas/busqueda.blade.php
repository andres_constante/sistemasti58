@extends('layout')

@section ('titulo') {{ $configuraciongeneral[0] }} @stop

@section('contenido')
<div class="row wrapper border-bottom">
    <div class="col-lg-10">
        <h1 class="font-bold text-primary">{{ $configuraciongeneral[4] }}</h1>
    </div>
</div>
<div class="animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        {!! Form::open(['method' => 'POST', 'id' => 'search-form', 'role'=>'form', 'class'=>'form-row', 'style' => 'margin: 0 0 0 0;']) !!}
                        <div class="form-group">
                            <div class="col-lg-3">
                                <select type="text" class="form-control" id="disposicion" name="disposicion">
                                    <option value="">TODOS</option>
                                    <option value="APROBADA">APROBADA</option>
                                    <option value="EN PROCESO">EN PROCESO</option>
                                    <option value="FINALIZADO">FINALIZADO</option>
                                    <option value="REVISION">REVISION</option>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" id="numtramite" name="numtramite"
                                    value="{{ old('numtramite') }}" placeholder="Número de trámite" autocomplete="off">
                            </div>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" id="cedula" name="cedula"
                                    value="{{ old('cedula') }}" placeholder="Número de cédula / RUC" autocomplete="off">
                            </div>
                            <div class="col-lg-2">
                                <button type="submit" class="btn btn-success">Buscar</button>
                                <button type="button" class="btn btn-info" id="limpiar">Limpiar</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <hr>
                    <div class="row">
                        <table class="table table-striped table-bordered table-hover display" id="tabla-tramites" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Número de trámite</th>
                                    <th>Origen</th>
                                    <th>Ingresado por</th>
                                    <th>Fecha de ingreso</th>
                                    <th>Tipo de trámite</th>
                                    <th>Remitente</th>
                                    <th>Dirección a atender</th>
                                    <th>Con copia</th>
                                    <th>Disposición</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            var table = $('#tabla-tramites').DataTable({
                // dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                // "<'row'<'col-xs-12't>>"+
                // "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                processing: true,
                serverSide: true,
                responsive: true,
                searching: false,
                order: [[ 0, 'DESC' ]],
                ajax: {
                    url: '/{{$configuraciongeneral[6]}}',
                    data: function(d) {
                        d.disposicion = $('#disposicion').val();
                        d.numtramite = $('input[name=numtramite]').val();
                        d.cedula = $('input[name=cedula]').val();
                    }
                },
                language: { url: '../../language/es.json' },
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
                "dom": 'Bfrtlip',
                buttons: [
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>' //text: '<i class="fa fa-print"></i> Imprimir'
                    },
                    {
                        extend: 'copy',
                        text: '<i class="fa fa-copy"></i>' //text: '<i class="fa fa-copy"></i> Copiar'
                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i>' //text: '<i class="fa fa-file-excel-o"></i> Excel'
                    },
                    {
                        extend: 'pdf',
                        text: '<i class="fa fa-file-pdf-o"></i>', //text: '<i class="fa fa-file-pdf-o"></i> PDF',
                        orientation: 'landscape',
                            title: '{{$configuraciongeneral[0]}}'
                    },
                    {
                        extend: 'colvis',
                        text: '<i class="fa fa-eye-slash"></i>' //text: '<i class="fa fa-eye-slash"></i> Ocultar Columnas'
                    }
                ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'numtramite', name: 'numtramite' },
                    { data: 'origen', name: 'origen' },
                    { data: 'igresado_por', name: 'igresado_por' },
                    { data: 'fecha_ingreso', name: 'fecha_ingreso' },
                    { data: 'tipo_tramite', name: 'tipo_tramite' },
                    { data: 'remitente', name: 'remitente' },
                    { data: 'atender', name: 'atender' },
                    { data: 'copia', name: 'copia' },
                    { data: 'disposicion', name: 'disposicion' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });

            $('#search-form').on('submit', function(e) {
                table.draw();
                e.preventDefault();
            });

            $('#limpiar').on('click', function(e) {
                $('#cedula').val('');
                $('#numtramite').val('');
                table.draw();
                e.preventDefault();
            });
        });
    </script>
@endsection