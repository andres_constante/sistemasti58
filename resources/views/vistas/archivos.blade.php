{{-- @if ((isset($archivos) && isset($archivos_total) && $tabla->tipo == 'VENTANILLA') || Auth::user()->id_perfil == 47) --}}
@if ((isset($archivos) && isset($archivos_total)) || Auth::user()->id_perfil == 47)
    <div class="form-group " id="div_imagenes_html">
        <label for="imagenes_html" id="label_imagenes_html" class="col-lg-3 control-label class_imagenes_html">Archivos:</label>
        <div class="col-lg-8" id="div-imagenes_html">
            <div class="form-group col-md-12 col-lg-12" id="obj-imagenes_html">
                <div class="col-lg-12 " id="imagenes_html"> 
                    <div style="text-align: end;">
                        <button type="button" class="btn btn-info" id="add_archivo">
                            <i class="fa fa-plus-circle" style="font-size: 1.5em;"> </i> Agregar archivo
                        </button>
                    </div>
                    <table class="table table-striped" style="text-align: center;" id="tabla_archivos">
                        <thead>
                            <tr>
                                <th>Nombre de Archivo</th>
                                <th>Fecha de Registro</th>
                                <th scope="col" style="text-align: center;">Archivo</th>
                                <th scope="col" style="text-align: center;">Usuario</th>
                                <th scope="col" style="text-align: center;">Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($archivos as $key => $archivo)
                                <tr id="fila_ar{{$key + 1}}">
                                    <td>{{ $archivo->nombre }}</td>
                                    <td>{{ $archivo->created_at }}</td>
                                    <td>
                                        <a href="{{ asset('archivos_sistema/'.$archivo->ruta) }}" type="button"
                                            class="btn btn-success dropdown-toggle divpopup" target="_blank">
                                            <i class="fa fa-file-pdf-o" style="font-size: 2em;"></i>
                                        </a>
                                    </td>
                                    <td>
                                        {{ $archivo->name == 'ADMINISTRADOR' ? 'CIUDADANO' : $archivo->name }}
                                    </td>
                                    <td>
                                        @if (Auth::user()->id == $archivo->id_usuario)
                                            <i onclick=" borrar_bd({{$archivo->id}}, {{ $key + 1 }})" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var n_filas_arr = {{ $archivos_total }};
        $("#add_archivo").click(function (e) {
            let html = `
            <tr id="fila_ar${n_filas_arr}">
                <td></td>
                <td></td>
                <td>
                    <form action="{{ route('subirarchivo.tramite') }}" method="post" enctype="multipart/form-data" id="form-file">
                        <input type="file" accept="application/pdf,application/msword,.docx" onChange="subirArchivo(this, {{ $referencia }}, ${n_filas_arr})" required class="form-control" name="archivo" id="archivo">
                    </form>
                </td>
                <td>{{ Auth::user()->name }}</td>
                <td>
                    <i onclick="borrar_archivo(${n_filas_arr})" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i>
                </td>
            </tr>`;
            $("#tabla_archivos tbody").prepend(html);
            n_filas_arr++;
        });
    
        function borrar_archivo(id) {
            $("#fila_ar"+id).remove();
        }
    
        function borrar_bd(id, fila) {
            $.ajax({
                type: 'POST',
                url: `${URLactual}//tramitesalcaldia/eliminararchivotramite`,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: { id }
            }).done((data) => {
                if(data.ok)
                {
                    $(`#fila_ar${fila}`).remove();
                    toastr["success"](data.message);
                }
                else toastr["error"](data.message);
            })
        }
    
        function subirArchivo(input, referencia, fila) {
            let totalByte = input.files[0].size;
            let totalSize = totalByte / Math.pow(1024, 2);
            let tamanio = 20;
            if (totalSize.toFixed(2) > tamanio)
            {
                toastr["error"](`El peso del archivo debe ser menor o igual que ${tamanio}MB.`);
                input.value = "";
            }
            // if(input.files[0].size > 47185920)
            // {
            //     toastr["error"]("El peso del archivo debe ser menor o igual que 45MB.");
            //     input.value = "";
            // }
            else
            {
                let data = new FormData($('#form-file')[0]);
                data.append('referencia', referencia);
    
                $.ajax({
                    type: 'POST',
                    url: `${URLactual}/tramitesalcaldia/subirarchivotramite`,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: data,
                    processData: false,
                    contentType: false
                }).done((data) => {
                    if(data.ok) {
                        $(`#fila_ar${fila}`).remove();
                        let html = `
                        <tr id="fila_ar${n_filas_arr}">
                            <td>${data.archivo.nombre}</td>
                            <td>${data.archivo.created_at}</td>
                            <td>
                                <a href="{{ asset('archivos_sistema/') }}/${data.archivo.ruta}" type="button"
                                    class="btn btn-success dropdown-toggle divpopup" target="_blank">
                                    <i class="fa fa-file-pdf-o" style="font-size: 2em;"></i>
                                </a>
                            </td>
                            <td>{{ Auth::user()->name }}</td>
                            <td>
                                <i onclick="borrar_bd(${data.archivo.id}, ${n_filas_arr})" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i>
                            </td>
                        </tr>`;
                        $("#tabla_archivos tbody").append(html);
                        n_filas_arr++;
                        toastr["success"](data.message);
                    }
                    else toastr["error"](data.message);
                })
            }
        }
    </script>
@endif

@if(isset($alcance) && count($alcance) > 0)
    <div class="form-group " id="div_alcance">
        <label for="alcance" id="label_alcance" class="col-lg-3 control-label class_alcance">Alcance:</label>
        <div class="col-lg-8" id="div-alcance">
            <div class="form-group col-md-12 col-lg-12" id="obj-alcance">
                <div class="col-lg-12 " id="alcance"> 
                    <table class="table table-striped" style="text-align: center;">
                        <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Fecha de Registro</th>
                                <th scope="col" style="text-align: center;">Archivo</th>
                                <th scope="col" style="text-align: center;">Usuario</th>
                                @if (Auth::user()->id_perfil == 20)
                                    <th scope="col" style="text-align: center;">Acción</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($alcance as $key => $item)
                                <tr id="fila_alcance{{$key + 1}}">
                                    <td>{{ $item->descripcion }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>
                                        <a href="{{ asset('archivos_sistema/'.$item->ruta) }}" type="button"
                                            class="btn btn-success dropdown-toggle divpopup" target="_blank">
                                            <i class="fa fa-file-pdf-o" style="font-size: 2em;"></i>
                                        </a>
                                    </td>
                                    <td>{{ $item->name }}</td>
                                    @if(Auth::user()->id_perfil == 20)
                                        <td>
                                            <a onclick="borrar_archivo_alcance({{$item->id}}, {{$key+1}})">
                                                <i class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        function borrar_archivo_alcance(id, fila) {
            $.ajax({
                type: 'POST',
                url: `${URLactual}/tramitesalcaldia/eliminararchivoalcance`,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: { id }
            }).done((data) => {
                if(data.ok)
                {
                    $(`#fila_alcance${fila}`).remove();
                    toastr["success"](data.message);
                }
                else toastr["error"](data.message);
            })
        }
    </script>
@endif

@if (Auth::user()->id_perfil == 20)
    <div class="form-group " id="div_alcance">
        <label for="alcance" id="label_alcance" class="col-lg-3 control-label class_alcance">Alcance:</label>
        <div class="col-lg-8" id="div-alcance">
            <div class="form-group col-md-12 col-lg-12" id="obj-alcance">
                <div class="col-lg-12 " id="alcance"> 
                    <div style="text-align: end;">
                        <button type="button" class="btn btn-info" id="add_alcance">
                            <i class="fa fa-plus-circle" style="font-size: 1.5em;"> </i> Agregar archivo de alcance
                        </button>
                    </div>
                    <table class="table table-striped" style="text-align: center;" id="tabla_alcance">
                        <thead>
                            <tr>
                                <th>Descripción</th>
                                {{-- <th>Fecha de Registro</th> --}}
                                <th scope="col" style="text-align: center;">Archivo</th>
                                <th scope="col" style="text-align: center;">Usuario</th>
                                <th scope="col" style="text-align: center;">Acción</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#add_alcance").click(function (e) {
                var nFilas = $("#tabla_alcance tr").length;
                
                if(nFilas == 1)
                {
                    let html = `
                    <tr id="fila_ar">
                        <td>
                            <input type="text" required class="form-control" name="descripcion" id="descripcion" autocomplete="off">
                        </td>
                        <td>
                            <form action="{{ route('subiralcance.tramite') }}" method="post" enctype="multipart/form-data" id="form-alcance">
                                <input type="file" accept="application/pdf" required class="form-control" name="archivo" id="archivo_alcance">
                            </form>
                        </td>
                        <td>{{ Auth::user()->name }}</td>
                        <td>
                            <button type="button" class="btn btn-info" id="add_alcance" onclick="subirArchivoAlcance({{ $referencia }})">
                                <i class="fa fa-plus-circle" style="font-size: 1.5em;"> </i> Subir
                            </button>
                        </td>
                    </tr>`;
                    
                    $("#tabla_alcance tbody").prepend(html);
                }
                else borrar_alcance()
            });
        })

        function borrar_alcance() {
            $("#fila_ar").remove();
        }

        function subirArchivoAlcance(referencia) {
            let input = $('#archivo_alcance')

            // if(input[0].files[0].size > 47185920)
            if(input[0].files[0].size > 20971520)
            {
                toastr["error"]("El peso del archivo debe ser menor o igual que 20MB.");
                input.value = "";
            }
            else
            {
                let descripcion = $('#descripcion').val()
                let data = new FormData($('#form-alcance')[0]);
                data.append('referencia', referencia);
                data.append('descripcion', descripcion);
    
                $.ajax({
                    type: 'POST',
                    url: `${URLactual}/tramitesalcaldia/subiralcancetramite`,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: data,
                    processData: false,
                    contentType: false
                }).done((data) => {
                    if(data.ok) {
                        // $(`#fila_ar`).remove();
                        // let html = `
                        // <tr>
                        //     <td>${data.archivo.descripcion}</td>
                        //     <td>
                        //         <a href="{{ asset('archivos_sistema/') }}/${data.archivo.ruta}" type="button"
                        //             class="btn btn-success dropdown-toggle divpopup" target="_blank">
                        //             <i class="fa fa-file-pdf-o" style="font-size: 2em;"></i>
                        //         </a>
                        //     </td>
                        //     <td>{{ Auth::user()->name }}</td>
                        // </tr>`;
                        // $("#tabla_alcance tbody").append(html);
                        // n_filas_arr++;
                        toastr["success"](data.message);
                        location.reload();
                    }
                    else toastr["error"](data.message);
                })
            }
        }
    </script>
@endif

@if (isset($anexos) && count($anexos) > 0)
    <hr>
    <div class="form-group " id="div_anexos">
        <label for="anexos" id="label_anexos" class="col-lg-3 control-label class_anexos">Anexos:</label>
        <div class="col-lg-8" id="div-anexos">
            <div class="form-group col-md-12 col-lg-12" id="obj-anexos">
                <div class="col-lg-12 " id="anexos"> 
                    <table class="table table-striped" style="text-align: center;" id="tabla_anexos">
                        <thead>
                            <tr>
                                <th>Nombre de Achivo</th>
                                <th>Fecha de Registro</th>
                                <th>N° de hojas</th>
                                <th>Descripción</th>
                                <th scope="col" style="text-align: center;">Archivo</th>
                                <th scope="col" style="text-align: center;">Usuario</th>
                                {{-- @if (Auth::user()->id == $archivo->id_usuario)
                                    <th scope="col" style="text-align: center;">Acción</th>
                                @endif --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($anexos as $key => $archivo)
                                <tr id="fila_ar{{$key + 1}}">
                                    <td>{{ $archivo->nombre }}</td>
                                    <td>{{ $archivo->created_at }}</td>
                                    <td>{{ $archivo->hojas }}</td>
                                    <td>{{ $archivo->descripcion }}</td>
                                    <td>
                                        <a href="{{ asset('archivos_sistema/'.$archivo->ruta) }}" type="button"
                                            class="btn btn-success dropdown-toggle divpopup" target="_blank">
                                            <i class="fa fa-file-pdf-o" style="font-size: 2em;"></i>
                                        </a>
                                    </td>
                                    <td>
                                        {{ $archivo->name == 'ADMINISTRADOR' ? 'CIUDADANO' : $archivo->name }}
                                    </td>
                                    {{-- <td>
                                        @if (Auth::user()->id == $archivo->id_usuario)
                                            <i onclick=" borrar_bd({{$archivo->id}}, {{ $key + 1 }})" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i>
                                        @endif
                                    </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endif