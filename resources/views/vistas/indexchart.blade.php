<?php
if(!isset($nobloqueo))
   Autorizar(Request::path());
?>

@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')

 <script>
        $(document).ready(function() {
            

            var chartrendimineto;


            $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es'
            });
            $('#desde').datepicker("setDate", new Date('01-01-2015'));
            $('#hasta').datepicker("setDate", new Date('01-01-2030'));
            $(".chosen-select").chosen(
  {
    no_results_text: "No existe coincidencia con lo que busca...",
    placeholder_text_single: "Seleccione...",
    placeholder_text_multiple: "Seleccione..."
});

        $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)}); 
        
        if("{{$configuraciongeneral[1]}}"=="coordinacioncronograma/coordinaciongraficos"){
            var chart = c3.generate({
            bindto:'#chart1',
            data: {
                columns: [
                    
                ],
                type : 'donut',
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "OBRAS"
            }
        });
        var chart2 = c3.generate({
            bindto:'#chart2',
            data: {
                columns: [
                    
                ],
                type : 'donut',
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES"
            }
        });
        var chart3 = c3.generate({
            bindto:'#chart3',
            
            data: {
                columns: [
                    
                ],
                type : 'bar',
                labels: true,
                
                
                onclick: function (d, i) { console.log("onclixck", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            legend: {
        // show: false
    },
    bar: {
        // width: {
        //     ratio:  // this makes bar width 50% of length between 
    
        // },
         width: 2
        // or
        //width: 100 // this makes bar width 100px
    },
            axis: {
//rotated: true,
                // padding:{
                //     left: 1,
                //     right: 1
                // },
                x: {
                    // type: 'indexed',
                    // height: 20
    //                 tick: {
        max: 40,
        type: 'category',
        
    //     tick: {
    //   count: 
    // }
show: false,
       
    //   count: 3
    // }
                }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES POR DIRECCIONES"
            },
            size:{ 
              // height: 500,
                //width:12000
            
            },
            tooltip: {
               // grouped: true,
                format: {
                                title: function (x, index) { return 'Rendimiento'; },
                                value: function (value, ratio, id, index) { return "Tiene un "+value+"% de Rendimiento"; }
                            }

            }
        });

        var chart7 = c3.generate({
            bindto:'#lineChart',
            zoom: {
            rescale: true
            },
            data: {
                columns: [
                    
                ],
                
                type : 'area-spline',
                //  labels: true

            },
            transition: {
                duration: 1000
            },
                legend: {
                        show: false
                    },
                    size: {
                       // height: 600
                    },
                    
                axis: {
                        rotated: false,
		                x : {
                           
                        type: 'category',
                        categories:  ['EJECUCION', 'EJECUTADO', 'EJECUTADO VENCIDO', 'POR EJECUTAR', 'SUSPENDIDO']
                },
               
				tick: {
                        x:{
                                    
                                    multiline:false,
                                  
                                    culling: {
                                        max: 1
                                    },
                            },
                            label : {
                                text: 'Estados',
                                position: 'center-bottom',
                                
                            }                 },
                        y: {
                            label : {
                                text: 'Cantidad',
                                position: 'outer-middle',
                            },
                          
                        }
                    },
                    count:2,
                    

                    
                    
                    
            });

    

//   });
        $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getChartLine") }}',
                    data: {'tipo':1},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       console.log( datos)
                        chart7.load({columns: datos, unload: true});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                });


            $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getValoresCharts") }}',
                    data: {'tipo':'3'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       
                        //  // Create an array of old keys
                        // var nkeys = datos.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        // var remove = used3.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        // used3 = nkeys;

                        // // Load the new data
                        // chart3.load({columns: datos, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 

   
        var chart4 = c3.generate({
            bindto:'#chart4',
            
            data: {
                columns: [
                    
                ],
                type : 'donut',
                
                onclick: function (d, i) { 
                    var desde= $("#desde").val();
                    var hasta= $("#hasta").val();
                    var direccion= $("#direccion").val();
                    $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getComunicacionesFiltro") }}'+'?tipo_filtro=ESTADODIRECCION&valor_filtro='+d["id"]+'&desde='+desde+'&hasta='+hasta+'&direccion='+direccion,
                    data: {'tipo':'1'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                    //    console.log(data);

                 
                               var myTable= "<table id='mitabla' class='table table-striped' ><tr><td style='width: 100px; '>Identificador</td>";
                                myTable+= "<td style='width: 100px; color:#000000;  text-align: right;'>Estado<spam</td>";
                                myTable+="<td style='width: 1000px; color:#000000;  text-align: right;'>Actividad</td>";
                                myTable+="<td style='width: 300px; color:#000000; text-align: right;'>Fecha de Inicio</td>";
                                myTable+="<td style='width: 300px; color:#000000;text-align: right;'>Fecha Fin</td>";
                                myTable+="<td style='width: 200px; color:#000000; text-align: right;'>Dirección</td>";
                                myTable+="<td style='width: 50px; color:#000000; text-align: right;'>Avance</td>";
                                myTable+="<td style='width: 50px; color:#000000; text-align: right;'>Prioridad</td></tr>";    

                                for (let i = 0; i < datos.length; i++) {
                                    myTable+="<tr><td style='width: 100px;text-align: right;'>" + datos[i].id + "</td>";        
                                    myTable+="<td style='width: 100px;text-align: right;'>" + datos[i].estado_actividad + "</td>";    
                                    myTable+="<td style='width: 1000px;text-align: justify;'>" + datos[i].actividad + "</td>";    
                                    myTable+="<td style='width: 300px;text-align: right;'>" + datos[i].fecha_inicio + "</td>";    
                                    myTable+="<td style='width: 300px;text-align: right;'>" + datos[i].fecha_fin + "</td>";    
                                    myTable+="<td style='width: 200px;text-align: right;'>" + datos[i].direccion + "</td>";    
                                    myTable+="<td style='width: 50px;text-align: right;'>" + datos[i].avance + "</td>";    
                                    myTable+="<td style='width: 50px;text-align: right;'>" + datos[i].prioridad + "</td>";    
                                    myTable+="</tr>";
                                }
                                myTable+="</table>";
                                $("#tablaactividades").html(myTable);
                                $("#myModal").modal();
                                
  
                          
                            
        
                },
                    statusCode: {
                        404: function() {
                       alert("No se pudo...");
                    }	
                    }	  
                }); 
                    console.log(d["id"]); 
            
            
            },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES"
            },
            tooltip: {
               // grouped: true,
                format: {
                                title: function (x, index) { return index; },
                                value: function (value, ratio, id, index) { return value; }
                            }

            }
        });



       

        setTimeout(function () {
                //chart 1
                var used1 = []; 
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getValoresCharts") }}',
                    data: {'tipo':'1'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['estado_obra'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        var remove = used1.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        used1 = nkeys;

                        // Load the new data
                        chart.load({columns: x, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 

                //chart2
                var used2 = []; 
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getValoresCharts") }}',
                    data: {'tipo':'2'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['estado_actividad'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        var remove = used2.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        used2 = nkeys;

                        // Load the new data
                        chart2.load({columns: x, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 

                //CHART3
                var used3 = []; 
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getValoresCharts") }}',
                    data: {'tipo':'3'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       
                        //  // Create an array of old keys
                        // var nkeys = datos.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        // var remove = used3.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        // used3 = nkeys;

                        // // Load the new data
                        // chart3.load({columns: datos, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 
            
        }, 10);


        $("#direccion").change(function(){               
                var used = []; 
                var direccion=0;

                direccion= $("#direccion").val();
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getValoresCharts") }}',
                    data: {'tipo':'4','direccion':direccion},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['estado_actividad'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        // var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        
                        // used = nkeys;

                       
                            chart4.load({columns: x, unload: true});
                         
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 
            });

            $("#direccion").change(function(){ 
                    direccion= $("#direccion").val();
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("coordinacioncronograma/getChartLine") }}',
                        data: {'id':direccion,'tipo':2},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           
                            chart7.load({columns: datos, unload: true});
                            
                    },
                        statusCode: {
                            404: function() {
                        alert("No existe URL");
                        }	
                        }	  
                    }); 
                });

            function requestData() {
    $.ajax({
        url: '{{ URL::to("coordinacioncronograma/getValoresCharts") }}',
        data: {'tipo':'3'},

        success: function(point) {
            var series = chartrendimineto.series[0],
            shift = series.data.length > 25; // shift if the series is 
                              // longer than 20
            // chartrendimineto.series[0].remove();
            // add the 
            console.log(point.length);console.log(series.data.length);
            // chartrendimineto.series[0].addPoint(point[3], true, shift);
            for(c = 0; c < point.length; c++){
                chartrendimineto.series[0].addPoint(point[c], true, shift);
            }
            //alert("aqui");
            // call it again after one second
            setTimeout(requestData, 30000);    

            height = chartrendimineto.height
            width = $("#chart5").width() 
            chartrendimineto.setSize(width, height, doAnimation = true);
        },
        cache: false
    });
}

// document.addEventListener('DOMContentLoaded', function() {
   var  chartrendimineto=  Highcharts.chart('chart5', 
     { chart: {
        type: 'column',
        height: 700,
       //width:100,
        events: {
    	load: requestData
    }
        
    },
    plotOptions: {
            series: {
                colorByPoint: true
            }
        },
 
    title: {
        text: 'RENDIMIENTO POR DIRECCION (SEMANA ACTUAL)'
    },
    subtitle: {
        text: 'GAD MANTA'
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rendimiento (%)'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Rendimiento: <b>{point.y:.1f} %</b>'
    },
    series: [{
        name: 'Direcciones',
        data:[],
        dataLabels: {
            enabled: true,
            rotation: -90,
            //color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f} %', // one decimal
            y: 3, // 10 pixels down from the top
            
            style: {
                fontSize: '12px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
        
     });

        }else if("{{$configuraciongeneral[1]}}"=="comunicacionalcaldia/comunicaciongraficos"){
            
        var chart2 = c3.generate({
            bindto:'#chart2',
            data: {
                columns: [
                    
                ],
                type : 'donut',
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES"
            }
        });
        var chart3 = c3.generate({
            bindto:'#chart3',
            
            data: {
                columns: [
                    
                ],
                type : 'bar',
                
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES POR DIRECCIONES"
            }
        });


        var chart4 = c3.generate({
            bindto:'#chart4',
            
            data: {
                columns: [
                    
                ],
                type : 'donut',
                
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES"
            }
        });






        setTimeout(function () {
                //chart2
                var used2 = []; 
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("comunicacionalcaldia/getValoresCharts") }}',
                    data: {'tipo':'2'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['estado_actividad'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        var remove = used2.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        used2 = nkeys;

                        // Load the new data
                        chart2.load({columns: x, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 

                //CHART3
                var used3 = []; 
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("comunicacionalcaldia/getValoresCharts") }}',
                    data: {'tipo':'3'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['direccion'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        var remove = used3.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        used3 = nkeys;

                        // Load the new data
                        chart3.load({columns: x, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 
            
        }, 10);





       

        $("#direccion").change(function(){               
                var used = []; 
                var direccion=0;

                direccion= $("#direccion").val();
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("comunicacionalcaldia/getValoresCharts") }}',
                    data: {'tipo':'4','direccion':direccion},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['estado_actividad'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                       
                        used = nkeys;
                        chart4.load({columns: x, unload: true});
                        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 
            });
        }else if("{{$configuraciongeneral[1]}}"=="coordinacioncronograma/coordinaciongraficosobras"){
            
            var chart2 = c3.generate({
                bindto:'#chart2',
                data: {
                    columns: [
                        
                    ],
                    type : 'donut',
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                transition: {
                duration: 3000
            },
                donut: {
                    title: "OBRAS"
                }
            });
            var chart3 = c3.generate({
                bindto:'#chart3',
                
                data: {
                    columns: [
                        
                    ],
                    type : 'bar',
                    
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                transition: {
                duration: 3000
            },
                donut: {
                    title: "OBRAS POR PARROQUIAS"
                }
            });
            var chart4 = c3.generate({
                bindto:'#chart4',
                
                data: {
                    columns: [
                        
                    ],
                    type : 'donut',
                    
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                transition: {
                duration: 3000
            },
                donut: {
                    title: "OBRAS POR PARROQUIA"
                }
            });      
    
            var chart7 = c3.generate({
            bindto:'#lineChart',
            zoom: {
            rescale: true
            },
            data: {
                columns: [
                    
                ],
                
                type : 'area-spline',
                //  labels: true

            },
            transition: {
                duration: 1000
            },
                legend: {
                        show: false
                    },
                    size: {
                       // height: 600
                    },
                    
                axis: {
                        rotated: false,
		                x : {
                           
                        type: 'category',
                        categories:  ['EJECUCION', 'POR_EJECUTAR']
                },
               
				tick: {
                        x:{
                                    
                                    multiline:false,
                                  
                                    culling: {
                                        max: 1
                                    },
                            },
                            label : {
                                text: 'Estados',
                                position: 'center-bottom',
                                
                            }                 },
                        y: {
                            label : {
                                text: 'Cantidad',
                                position: 'outer-middle',
                            },
                          
                        }
                    },
                    count:2,
                    

                    
                    
                    
            });
    
    
            setTimeout(function () {
                    //chart2
                    var used2 = []; 
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("coordinacioncronograma/getValoresChartsObras") }}',
                        data: {'tipo':'2'},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           var x=[];
                           for (i = 0; i < datos.length; i++) {
                            x.push([datos[i]['estado_obra'],datos[i]['valor']]);
                            }
                             // Create an array of old keys
                            var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                            var remove = used2.filter(function(v){ return nkeys.indexOf(v) < 0;});
                            used2 = nkeys;
    
                            // Load the new data
                            chart2.load({columns: x, unload: remove});
    
            
                    },
                        statusCode: {
                            404: function() {
                           alert("<div class='alert alert-danger'>No existe URL</div>");
                        }	
                        }	  
                    }); 
    
                    //CHART3
                    var used3 = []; 
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("coordinacioncronograma/getValoresChartsObras") }}',
                        data: {'tipo':'6'},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           
                            chart7.load({columns: datos, unload: true});
    
            
                    },
                        statusCode: {
                            404: function() {
                           alert("<div class='alert alert-danger'>No existe URL</div>");
                        }	
                        }	  
                    }); 
                
            }, 10);
    
    
            $("#direccion").change(function(){               
                    var used = []; 
                    var parroquia=0;
    
                    parroquia= $("#direccion").val();
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("coordinacioncronograma/getValoresChartsObras") }}',
                        data: {'tipo':'4','direccion':parroquia},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           var x=[];
                           for (i = 0; i < datos.length; i++) {
                            x.push([datos[i]['estado_obra'],datos[i]['valor']]);
                            }
                             // Create an array of old keys
                            var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                           
                            used = nkeys;
                            //console.log(x);
                            chart4.load({columns: x, unload: true});
                            
                    },
                        statusCode: {
                            404: function() {
                           alert("<div class='alert alert-danger'>No existe URL</div>");
                        }	
                        }	  
                    }); 
                });

                $("#direccion").change(function(){ 
                    direccion= $("#direccion").val();
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("coordinacioncronograma/getValoresChartsObras") }}',
                        data: {'direccion':direccion,'tipo':5},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           
                         

                            chart7.load({columns: datos, unload: true});
                            
                    },
                        statusCode: {
                            404: function() {
                        alert("No existe URL");
                        }	
                        }	  
                    }); 
                });

            }else if("{{$configuraciongeneral[1]}}"=="tramitesalcaldia/tramitesgraficos"){
                var chart2 = c3.generate({
                bindto:'#chart2',
                data: {
                    columns: [
                        
                    ],
                    type : 'donut',
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                transition: {
                duration: 3000
            },
                donut: {
                    title: "POR DISPOSICIÓN"
                }
            });
            var chart4 = c3.generate({
                bindto:'#chart4',
                
                data: {
                    columns: [
                        
                    ],
                    type : 'donut',
                    
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                transition: {
                duration: 3000
            },
                donut: {
                    title: "POR PRIORIDAD"
                }
            });

            // var chart3 = c3.generate({
            //     bindto:'#chart3',
                
            //     data: {
            //         columns: [
                        
            //         ],
            //         type : 'area-spline',
                    
            //         onclick: function (d, i) { console.log("onclick", d, i); },
            //         onmouseover: function (d, i) { console.log("onmouseover", d, i); },
            //         onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            //     },
            //     transition: {
            //     duration: 3000
            // },
            //     donut: {
            //         title: "POR SOLICITUD"
            //     }
            // });
           
            
            setTimeout(function () {
                    //chart2
                    var used2 = []; 
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("tramitesalcaldia/getValoresCharts") }}',
                        data: {'tipo':'2'},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           var x=[];
                           for (i = 0; i < datos.length; i++) {
                            x.push([datos[i]['disposicion'],datos[i]['valor']]);
                            }
                             // Create an array of old keys
                            var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                            var remove = used2.filter(function(v){ return nkeys.indexOf(v) < 0;});
                            used2 = nkeys;
    
                            // Load the new data
                            chart2.load({columns: x, unload: remove});
    
            
                    },
                        statusCode: {
                            404: function() {
                           alert("<div class='alert alert-danger'>No existe URL</div>");
                        }	
                        }	  
                    }); 
    
                    //CHART3
                    var used3 = []; 
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("tramitesalcaldia/getValoresCharts") }}',
                        data: {'tipo':'3'},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           var x=[];
                           for (i = 0; i < datos.length; i++) {
                            x.push([datos[i]['prioridad'],datos[i]['valor']]);
                            }
                             // Create an array of old keys
                            var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                            var remove = used3.filter(function(v){ return nkeys.indexOf(v) < 0;});
                            used3 = nkeys;
    
                            // Load the new data
                            chart4.load({columns: x, unload: remove});
    
            
                    },
                        statusCode: {
                            404: function() {
                           alert("<div class='alert alert-danger'>No existe URL</div>");
                        }	
                        }	  
                    }); 
                //     $.ajax({
                //     type: "GET",
                //     url: '{{ URL::to("tramitesalcaldia/getValoresCharts") }}',
                //     data: {'tipo':'4'},
                //     error: function(objeto, quepaso, otroobj){
                //         alert(quepaso);
                //     },				
                //     success: function(datos){
                //        var x=[];
                //        for (i = 0; i < datos.length; i++) {
                //         x.push([datos[i]['created_at'],datos[i]['valor']]);
                //         }
                //          // Create an array of old keys
                //         var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                       
                //         used = nkeys;
                //         chart3.load({columns: x, unload: true});
                        
                // },
                //     statusCode: {
                //         404: function() {
                //        alert("<div class='alert alert-danger'>No existe URL</div>");
                //     }	
                //     }	  
                // }); 
                
            }, 10);
        
            }
                
    });  
    



//MAPA

$(document).ready( function () {
    $description = $(".description");
    function mapacolors(obj,clase)
    {
        obj.attr("class", clase);
            $description.addClass('active');
            $description.html(obj.attr('alt'));            
    }
    $('.mapacanton').hover(function(){        
            mapacolors($(this),$(this).attr('class')+" heyo");
        },function() {
            $description.removeClass('active');
    });
    $('.mapacantonmanta').hover(function(){        
            mapacolors($(this),$(this).attr('class')+" heyo");
        },function() {
            $description.removeClass('active');
    });
    $('.mapacantonesteros').hover(function(){        
            mapacolors($(this),$(this).attr('class')+" heyo");
        },function() {
            $description.removeClass('active');
    });
    $('.mapacantonlorenzo').hover(function(){        
            mapacolors($(this),$(this).attr('class')+" heyo");
        },function() {
            $description.removeClass('active');
    });
    $('.mapacantonmariana').hover(function(){        
            mapacolors($(this),$(this).attr('class')+" heyo");
        },function() {
            $description.removeClass('active');
    });
    $('.mapacantonmateo').hover(function(){        
            mapacolors($(this),$(this).attr('class')+" heyo");
        },function() {
            $description.removeClass('active');
    });
    $('.mapacantonalfaro').hover(function(){        
            mapacolors($(this),$(this).attr('class')+" heyo");
        },function() {
            $description.removeClass('active');
    });
    
});
/*
 $(document).on('mousemove', function(e){
            $description.css({
                left:  e.pageX,
                top:   e.pageY - 200
            });
        });
*/
</script>
@stop
@section('estilos')
<style type="text/css">
    body.DTTT_Print {
        background: #fff;

    }
    .c3-chart-arcs-title {
  fill:#0e9aef     ;
  font-size: 18%;
  font-weight: bold;
}
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
/*MAPA*/
.mapacantonmateo {
    fill: #bf3954;
    cursor: pointer;
}
.mapacantonalfaro {
    fill: #f19750;
    cursor: pointer;
}
.mapacantonmariana {
    fill: #0e2088;
    cursor: pointer;
}
.mapacantonlorenzo {
    fill: #c5a657;
    cursor: pointer;
}
.mapacantonesteros {
    fill: #57c2c5;
    cursor: pointer;
}
.mapacantonmanta {
    fill: #93dc6b;
    cursor: pointer;
}
.mapacanton {
    fill: #21669E;
    cursor: pointer;
}
.heyo:hover {
  fill: #a2a29b;/*#3997E5;*/
  -moz-transition: 0.3s;
  -o-transition: 0.3s;
  -webkit-transition: 0.3s;
  transition: 0.3s;

}
.description{
  pointer-events: none;  
  position: absolute;    
  font-size: 1em;
  text-align: center;
  background: white;
  padding: 10px 15px;  
  z-index: 5;
  height: 30px;
  line-height: 30px;
  /*margin: 0 auto;*/
  color: #21669e;
  border-radius: 5px;
  width: 12em;
  height: auto;
  box-shadow: 0 0 0 2px #eee; 
  font-weight: bold;
  margin-left: 10%; 
  /*
  -moz-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
  */
  display: none;
  
}
.description.active {
  display: block;
}
.description:after {
  content: "";
  /*
  position: absolute;
  left: 50%;
  top: 100%;
  width: 0;
  height: 0;
  margin-left: -10px;
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-top: 10px solid white;*/
}
.mapasize{
    width:380.000000pt;
    height:293.000000pt;
}
@media (max-width: 1200px) {
    .mapasize{
        width:100%;
        height:auto;
    }
}
</style>
@stop
@section ('contenido')
<h1 class="col-lg-7"> {{ $configuraciongeneral[0] }}</h1>

@if (Session::has('message'))
<script>    
$(document).ready(function() {
//toastr.succes("{{ Session::get('message') }}");
toastr["success"]("{{ Session::get('message') }}");
//$.notify("{{ Session::get('message') }}","success");
});
</script>    
 <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif   

 @if (isset($totalacti->total))
 <div class="row">
    <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right">HOY</span>
                    <h3>ACTIVIDADES <span class="badge" style="font-size: 1em;">{!! $totalacti->total !!}</span></h3>
                </div>
                <div class="ibox-content">

                    <div class="row">
                            @foreach ($iboxActividades as $item)
                            <div class="col-lg-2">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                        <span class="label pull-right" style="background-color:{{$item->color}};color:#FFFFFF;">     </span>
                                            <h5>{{$item->estado_actividad}}</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <h1 class="no-margins">{{$item->total}}</h1>
                                            <div class="stat-percent font-bold " style="color:{{$item->color}}">{{$item->porcentaje}}% <i class="fa fa-bolt"></i></div>
                                            <small>{{$item->estado_actividad}} </small>
                                        </div>
                                    </div>
                                </div>
                            @endforeach     
                    </div>


                </div>
            </div>
        </div>

</div>
<div>
 @endif


        

<div hidden  style="float: right;">
       
       
        <div class="col-lg-2">
                <div class="input-group date">
                        <span class="input-group-addon">Desde <i class="fa fa-calendar"></i></span>
                            <input data-placement="top" data-toogle="tooltip" class="form-control datefecha" placeholder="Desde" desde="" readonly="readonly" name="desde" type="text" id="desde">
                     </div>
        </div>
        <div class="col-lg-2">
                <div class="input-group date">
                        <span class="input-group-addon">Hasta  <i class="fa fa-calendar"></i></span>
                            <input data-placement="top" data-toogle="tooltip" class="form-control datefecha" placeholder="Hasta" hasta="" readonly="readonly" name="hasta" type="text" id="hasta">
                     </div>
        </div>
        <div class="col-lg-1">
                <div class="input-group ">
                        <button type="button" class="btn btn-w-m btn-success" >
                                <i class="fa fa-search" style="font-size: 1.5em;">   </i> Mostrar</button><br><br>
                     </div>
        </div>
</div>

                   <div class="ibox float-e-margins">
                    
                    @if($configuraciongeneral[1]=="coordinacioncronograma/coordinaciongraficos")
                 


                            <div class="ibox-title"> 
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                       
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">   
                                    <div>
                                            <select  class="form-control chosen-select" name="direccion" id="direccion">
                                            <option value="">Seleccione una Direccíón</option>
                                            @foreach($direcciones as $direccion)
                                                <option value="{{$direccion->id}}">{{$direccion->direccion}}</option>
                                            @endforeach
                                            </select>
                                            
                                            
                                        </div>
                                    <br>
        
                            <div class="row">
                    
                    @foreach($objetos as $key => $value)

                    @if($value->Nombre=='chart2'|| $value->Nombre=='chart4')
                    
                    <div class="col-lg-6">
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                                <div>
                                        
                                    <div id="{{ $value->Nombre }}"></div>

                                   
                                    
                                </div>
                                
                            </div>
                           
                            
                            </div>
                           
                            
                    </div>
                    
                   
                    @elseif($value->Nombre=='chart3')
                    <div class="col-lg-12">
                    
                            <div class="ibox float-e-margins">
                                    <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <h5>Movimientos</h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                        <i class="fa fa-wrench"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-user">
                                                        <li><a href="#">Config option 1</a>
                                                        </li>
                                                        <li><a href="#">Config option 2</a>
                                                        </li>
                                                    </ul>
                                                    <a class="close-link">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </div>
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                           
                                
                                <div>
                                    <div id="{{ $value->Nombre }}"></div>
                                </div>
                            </div>
                            </div>
                            
                        
                    </div>
                    @elseif($value->Nombre=='actividadespordireccion')
                    <div class="col-lg-6">
                    
                            <div class="ibox float-e-margins">
                                
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                           
                                  
                                                <div class="ibox-content" style="position: relative">
                                                        
                                                        <div id="lineChart"></div>
                                                        
                                                     
                                                </div>    
                                        
                            </div>
                            </div>
                            
                        
                    </div>
                    @elseif($value->Nombre=='chart5')
                    <div class="col-lg-12">

                    
                            <div class="ibox float-e-margins">
                                    <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <h5>Movimientos</h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                        <i class="fa fa-wrench"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-user">
                                                        <li><a href="#">Config option 1</a>
                                                        </li>
                                                        <li><a href="#">Config option 2</a>
                                                        </li>
                                                    </ul>
                                                    <a class="close-link">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </div>
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                           
                                
                                <div  >                                    
                                    <div id="{{ $value->Nombre }}"  style="min-width: 100%; height: 100%; margin: 0 auto" ></div>
                                </div>
                            </div>
                            </div>
                            
                        
                    </div>
                    @elseif($value->Nombre=='actividades')
                    
                    <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Movimientos</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content inspinia-timeline">
                                    @foreach ($actividadesActualizadas as $item)
                                    <div class="timeline-item">
                                            <div class="row">
                                                <div class="col-xs-3 date">
                                                    <i class="fa fa-briefcase"></i>
                                                    {{ fechas(2,$item->updated_at) }}
                                                    <br/>
                                                    <small class="text-navy">{{Carbon\Carbon::parse($item->updated_at)->diffForHumans()}}</small>
                                                </div>
                                                <div class="col-xs-11 content no-top-border">
                                                    <p class="m-b-xs"><strong>Actividad</strong></p>
    
                                                    {{-- <pre style="text-align: left; white-space: pre-line;"> --}}
                                                            <strong>Actividad: </strong>{{$item->actividad}} <br>
                                                            <strong>Estado de la actividad:</strong> {{$item->estado_actividad}}<br>
                                                            <strong>Dirección a cargo:</strong>  {{$item->direccion}}<br>
                                                                            <strong>Fecha de inicio:</strong>  {{$item->fecha_inicio}}<br>
                                                                                <strong>Fecha de fin:</strong>  {{$item->fecha_fin}}<br>
                                                                                    <strong>Observación:</strong>  {{$item->observacion}}<br>
                                                            <strong>Modificado por el usuario:</strong>  {{$item->name}}<br>
                                                            <strong>Prioridad:</strong>  {{$item->prioridad}}
                                                            
                                                          {{-- </pre> --}}
    
                                                    <p>
                                                            <div class="ibox-content">                                              
                                                                    <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                                                                    <div class="progress">
                                                                      <div style="width: {{$item->avance}}%;" class="progress-bar"></div>
                                                                    </div>
                                                        
                                                                
                                                              </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    


                                </div>
                            </div>
                        </div>

                        </div>





                    @endif

                    @if ( $value->Nombre=='chart4')

                    <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                          
                              <!-- Modal content-->
                              <div class="modal-content" style="width:65em; ">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Actividades</h4>
                                </div>
                                <div class="modal-body">
                                        <div id="tablaactividades" >


                                            </div>  
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-primary " data-dismiss="modal">Close</button>
                                </div>
                              </div>
                          
                            </div>
                          </div>
                    
                    @endif
                
                    @endforeach


                @elseif($configuraciongeneral[1]=="comunicacionalcaldia/comunicaciongraficos")
                @foreach($objetos as $key => $value)

                    @if($value->Nombre=='chart2'|| $value->Nombre=='chart4')
                    <div class="col-lg-6">
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                            @if($value->Nombre=='chart4')
                                <div>
                                <select  class="form-control" name="direccion" id="direccion">
                                <option value="">Seleccione una Direccíon</option>
                                @foreach($direcciones as $direccion)
                                    <option value="{{$direccion->id}}">{{$direccion->direccion}}</option>
                                @endforeach
                                </select>
                                
                                
                                </div>
                              
                            @endif

                                <div>
                                    <div id="{{ $value->Nombre }}"></div>
                                </div>
                            </div>
                            </div>
                            
                        
                    </div>
                    @elseif($value->Nombre=='chart3')
                    <div class="col-lg-12">
                    
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                           
                                
                                <div>
                                    <div id="{{ $value->Nombre }}"></div>
                                </div>
                            </div>
                            </div>
                            
                        
                    </div>
                   
                   

                    @endif


                    @endforeach

                    @elseif($configuraciongeneral[1]=="coordinacioncronograma/coordinaciongraficosobras")

                    <div class="row">
                        <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <span class="label label-danger pull-right">HOY</span>
                                        <h3>OBRAS <span class="badge" style="font-size: 1em;">{!! $total->total !!}</span></h3>
                                    </div>
                                    <div class="ibox-content">
                                            
                    
                                        <div class="row">
                                                @foreach ($iboxObras as $item)
                                                <div class="col-lg-2">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                            <span class="label pull-right" style="background-color:{{$item->color}};color:#FFFFFF;">   </span>
                                                                <h5>{{$item->estado_obra}}</h5>
                                                            </div>
                                                            <div class="ibox-content">
                                                                <h1 class="no-margins">{{$item->total}}</h1>
                                                                <div class="stat-percent font-bold " style="color:{{$item->color}}">{{$item->porcentaje}}% <i class="fa fa-bolt"></i></div>
                                                                <small>{{$item->estado_obra}} </small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach     
                                        </div>
                    
                    
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="ibox-title"> 
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                   
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">   
                                <div>
                                        <select  class="form-control chosen-select" name="direccion" id="direccion">
                                                <option value="">Seleccione la Parroquia</option>
                                                @foreach($direcciones as $direccion)
                                                    <option value="{{$direccion->id}}">{{$direccion->parroquia}}</option>
                                                @endforeach
                                                </select>
                                        
                                    </div>
                                <br>
                                <div class="row">
                                        @foreach($objetos as $key => $value)

                                        @if($value->Nombre=='chart2'|| $value->Nombre=='chart4')
                                        <div class="col-lg-6">
                                                <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>{{ $value->Descripcion }}</h5>
                                                </div>
                                                <div>
                                                       
                                                        
                                                        
                                                    </div>
                                                <br>
                                                <div class="ibox-content">
                                               
                    
                                                    <div>
                                                        <div id="{{ $value->Nombre }}"></div>
                                                    </div>
                                                    
                                                </div>
                                                </div>
                                                
                                            
                                        </div>
                                        @elseif($value->Nombre=='chart3')
                                        <div class="col-lg-12">
                                        
                                                <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>{{ $value->Descripcion }}</h5>
                                                </div>
                                                <div class="ibox-content">
                                               
                                                    
                                                        <div class="col-lg-6">
                    
                            <div class="ibox float-e-margins">
                                
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                           
                                  
                                                <div class="ibox-content" style="position: relative">
                                                        
                                                        <div id="lineChart"></div>
                                                        
                                                     
                                                </div>    
                                        
                            </div>
                            </div>
                            
                        
                    </div>
                                                  
                                                </div>
                                                </div>
                                                
                                            
                                        </div>
                                        @elseif($value->Nombre=='chartnuevo')
                                        <div class="col-lg-6">
                                        
                                                <div class="ibox float-e-margins">
                                                    
                                                <div class="ibox-title">
                                                    <h5>{{ $value->Descripcion }}</h5>
                                                </div>
                                                <div class="ibox-content">
                                               
                                                      
                                                                    <div class="ibox-content" style="position: relative">
                                                                            
                                                                            <div id="lineChart"></div>
                                                                            
                                                                         
                                                                    </div>    
                                                            
                                                </div>
                                                </div>
                                                
                                            
                                        </div>
                                        @elseif($value->Nombre=='chart5')                                        
                                            @include("vistas.mapamanta.mapadasboard")
                                        @endif
                    
                    
                                        @endforeach

                                </div>
    
                        </div>
                



                    
                    @elseif($configuraciongeneral[1]=="tramitesalcaldia/tramitesgraficos")
                    @foreach($objetos as $key => $value)
    
                        @if($value->Nombre=='chart2'|| $value->Nombre=='chart4')
                        <div class="col-lg-6">
                                <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>{{ $value->Descripcion }}</h5>
                                </div>
                                <div class="ibox-content">

                                    <div>
                                        <div id="{{ $value->Nombre }}"></div>
                                    </div>
                                </div>
                                </div>
                                
                            
                        </div>
                        @elseif($value->Nombre=='chart3')
                        <div class="col-lg-12">
                        
                                <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>{{ $value->Descripcion }}</h5>
                                </div>
                                <div class="ibox-content">
                               
                                    
                                    <div>
                                        <div id="{{ $value->Nombre }}"></div>
                                    </div>
                                </div>
                                </div>
                                
                            
                        </div>
                        @endif
    
    
                        @endforeach
    
                @endif

            </div>
        </div>
                      </div>

                        
                </div> <!-- ibox-content -->
               
</div> <!-- ibox float-e-margins -->
@stop
