<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:v="urn:schemas-microsoft-com:vml">

<head>

	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<meta content="width=device-width" name="viewport" />

	<meta content="IE=edge" http-equiv="X-UA-Compatible" />

	<title></title>

	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css" />

	<style type="text/css">
		body {
			margin: 0;
			padding: 0;
		}

		table,
		td,
		tr {
			vertical-align: top;
			border-collapse: collapse;
		}

		* {
			line-height: inherit;
		}

		a[x-apple-data-detectors=true] {
			color: inherit !important;
			text-decoration: none !important;
		}

		.ie-browser table {
			table-layout: fixed;
		}

		[owa] .img-container div,
		[owa] .img-container button {
			display: block !important;
		}

		[owa] .fullwidth button {
			width: 100% !important;
		}

		[owa] .block-grid .col {
			display: table-cell;
			float: none !important;
			vertical-align: top;
		}

		.ie-browser .block-grid,
		.ie-browser .num12,
		[owa] .num12,
		[owa] .block-grid {
			width: 650px !important;
		}

		.ie-browser .mixed-two-up .num4,
		[owa] .mixed-two-up .num4 {
			width: 216px !important;
		}

		.ie-browser .mixed-two-up .num8,
		[owa] .mixed-two-up .num8 {
			width: 432px !important;
		}

		.ie-browser .block-grid.two-up .col,
		[owa] .block-grid.two-up .col {
			width: 324px !important;
		}

		.ie-browser .block-grid.three-up .col,
		[owa] .block-grid.three-up .col {
			width: 324px !important;
		}

		.ie-browser .block-grid.four-up .col [owa] .block-grid.four-up .col {
			width: 162px !important;
		}

		.ie-browser .block-grid.five-up .col [owa] .block-grid.five-up .col {
			width: 130px !important;
		}

		.ie-browser .block-grid.six-up .col,
		[owa] .block-grid.six-up .col {
			width: 108px !important;
		}

		.ie-browser .block-grid.seven-up .col,
		[owa] .block-grid.seven-up .col {
			width: 92px !important;
		}

		.ie-browser .block-grid.eight-up .col,
		[owa] .block-grid.eight-up .col {
			width: 81px !important;
		}

		.ie-browser .block-grid.nine-up .col,
		[owa] .block-grid.nine-up .col {
			width: 72px !important;
		}

		.ie-browser .block-grid.ten-up .col,
		[owa] .block-grid.ten-up .col {
			width: 60px !important;
		}

		.ie-browser .block-grid.eleven-up .col,
		[owa] .block-grid.eleven-up .col {
			width: 54px !important;
		}

		.ie-browser .block-grid.twelve-up .col,
		[owa] .block-grid.twelve-up .col {
			width: 50px !important;
		}
	</style>
	<style id="media-query" type="text/css">
		@media only screen and (min-width: 670px) {
			.block-grid {
				width: 650px !important;
			}

			.block-grid .col {
				vertical-align: top;
			}

			.block-grid .col.num12 {
				width: 650px !important;
			}

			.block-grid.mixed-two-up .col.num3 {
				width: 162px !important;
			}

			.block-grid.mixed-two-up .col.num4 {
				width: 216px !important;
			}

			.block-grid.mixed-two-up .col.num8 {
				width: 432px !important;
			}

			.block-grid.mixed-two-up .col.num9 {
				width: 486px !important;
			}

			.block-grid.two-up .col {
				width: 325px !important;
			}

			.block-grid.three-up .col {
				width: 216px !important;
			}

			.block-grid.four-up .col {
				width: 162px !important;
			}

			.block-grid.five-up .col {
				width: 130px !important;
			}

			.block-grid.six-up .col {
				width: 108px !important;
			}

			.block-grid.seven-up .col {
				width: 92px !important;
			}

			.block-grid.eight-up .col {
				width: 81px !important;
			}

			.block-grid.nine-up .col {
				width: 72px !important;
			}

			.block-grid.ten-up .col {
				width: 65px !important;
			}

			.block-grid.eleven-up .col {
				width: 59px !important;
			}

			.block-grid.twelve-up .col {
				width: 54px !important;
			}
		}

		@media (max-width: 670px) {

			.block-grid,
			.col {
				min-width: 320px !important;
				max-width: 100% !important;
				display: block !important;
			}

			.block-grid {
				width: 100% !important;
			}

			.col {
				width: 100% !important;
			}

			.col>div {
				margin: 0 auto;
			}

			img.fullwidth,
			img.fullwidthOnMobile {
				max-width: 100% !important;
			}

			.no-stack .col {
				min-width: 0 !important;
				display: table-cell !important;
			}

			.no-stack.two-up .col {
				width: 50% !important;
			}

			.no-stack .col.num4 {
				width: 33% !important;
			}

			.no-stack .col.num8 {
				width: 66% !important;
			}

			.no-stack .col.num4 {
				width: 33% !important;
			}

			.no-stack .col.num3 {
				width: 25% !important;
			}

			.no-stack .col.num6 {
				width: 50% !important;
			}

			.no-stack .col.num9 {
				width: 75% !important;
			}

			.video-block {
				max-width: none !important;
			}

			.mobile_hide {
				min-height: 0px;
				max-height: 0px;
				max-width: 0px;
				display: none;
				overflow: hidden;
				font-size: 0px;
			}

			.desktop_hide {
				display: block !important;
				max-height: none !important;
			}
		}






		.container {
			width: 600px;
			margin: 100px auto;
		}

		.progressbar {
			counter-reset: step;
		}

		.progressbar li {
			list-style-type: none;
			width: 25%;
			float: left;
			font-size: 12px;
			position: relative;
			text-align: center;
			text-transform: uppercase;
			color: #7d7d7d;
		}

		.progressbar li:before {
			width: 30px;
			height: 30px;
			content: counter(step);
			counter-increment: step;
			line-height: 30px;
			border: 2px solid #7d7d7d;
			display: block;
			text-align: center;
			margin: 0 auto 10px auto;
			border-radius: 50%;
			background-color: white;
		}

		.progressbar li:after {
			width: 100%;
			height: 2px;
			content: '';
			position: absolute;
			background-color: #7d7d7d;
			top: 15px;
			left: -50%;
			z-index: -1;
		}

		.progressbar li:first-child:after {
			content: none;
		}

		.progressbar li.active {
			color: green;
		}

		.progressbar li.active:before {
			border-color: #55b776;
		}

		.progressbar li.active+li:after {
			background-color: #55b776;
		}
	</style>
</head>

<body class="clean-body" style="margin: 0; padding: 2%; -webkit-text-size-adjust: 100%; background-color: #F5F5F5;">
	<style id="media-query-bodytag" type="text/css">
		@media (max-width: 670px) {
			.block-grid {
				min-width: 320px !important;
				max-width: 100% !important;
				width: 100% !important;
				display: block !important;
			}

			.col {
				min-width: 320px !important;
				max-width: 100% !important;
				width: 100% !important;
				display: block !important;
			}

			.col>div {
				margin: 0 auto;
			}

			img.fullwidth {
				max-width: 100% !important;
				height: auto !important;
			}

			img.fullwidthOnMobile {
				max-width: 100% !important;
				height: auto !important;
			}

			.no-stack .col {
				min-width: 0 !important;
				display: table-cell !important;
			}

			.no-stack.two-up .col {
				width: 50% !important;
			}

			.no-stack.mixed-two-up .col.num4 {
				width: 33% !important;
			}

			.no-stack.mixed-two-up .col.num8 {
				width: 66% !important;
			}

			.no-stack.three-up .col.num4 {
				width: 33% !important
			}

			.no-stack.four-up .col.num3 {
				width: 25% !important
			}
		}
	</style>

	<table bgcolor="#F5F5F5" cellpadding="0" cellspacing="0" class="nl-container" role="presentation"
		style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #F5F5F5; width: 100%;"
		valign="top" width="100%">
		<tbody>
			<tr style="vertical-align: top;" valign="top">
				<td style="word-break: break-word; vertical-align: top; border-collapse: collapse;" valign="top">

					<div style="background-color:transparent;">
						<div class="block-grid"
							style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;;">
							<div
								style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
								<div class="col num12"
									style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top;;">
									<div style="width:100% !important;">
										<!--[if (!mso)&(!IE)]><!-->
										<div
											style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
											<!--<![endif]-->
											<table border="0" cellpadding="0" cellspacing="0" class="divider"
												role="presentation"
												style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
												valign="top" width="100%">
												<tbody>
													<tr style="vertical-align: top;" valign="top">
														<td class="divider_inner"
															style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; border-collapse: collapse;"
															valign="top">
															<table align="center" border="0" cellpadding="0"
																cellspacing="0" class="divider_content" height="10"
																role="presentation"
																style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 0px solid transparent; height: 10px;"
																valign="top" width="100%">
																<tbody>
																	<tr style="vertical-align: top;" valign="top">
																		<td height="10"
																			style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse;"
																			valign="top"><span></span></td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>

										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color:transparent;">
						<div class="block-grid two-up no-stack"
							style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;">
							<div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
								<div class="col num6"
									style="min-width: 320px; max-width: 325px; display: table-cell; vertical-align: top;;">
									<div style="width:100% !important;">

										<div
											style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:25px; padding-bottom:25px; padding-right: 0px; padding-left: 25px;">

											<div align="left" class="img-container left fixedwidth"
												style="padding-right: 10px;padding-left: 10px;">
												<div style="font-size:1px;line-height:10px"> </div><img alt="Image"
													border="0" class="left fixedwidth"
													src="http://sistemasic.manta.gob.ec/img/mantafirmes.png"
													style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; max-width: 195px; display: block;"
													title="Image" width="195" />
												<div style="font-size:1px;line-height:10px"> </div>

											</div>

										</div>

									</div>
								</div>
								<div class="col num6"
									style="min-width: 320px; max-width: 325px; display: table-cell; vertical-align: top;;">
									<div style="width:100% !important;">

										<div
											style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:25px; padding-bottom:25px; padding-right: 25px; padding-left: 0px;">

											<div align="right" class="button-container"
												style="padding-top:10px;padding-right:0px;padding-bottom:10px;padding-left:10px;">
												<a href="#"
													style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #052d3d; background-color: #e3edfe; border-radius: 14px; -webkit-border-radius: 14px; -moz-border-radius: 14px; width: auto; width: auto; border-top: 1px solid #e3edfe; border-right: 1px solid #e3edfe; border-bottom: 1px solid #e3edfe; border-left: 1px solid #e3edfe; padding-top: 5px; padding-bottom: 5px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
													target="_blank"><span
														style="padding-left:20px;padding-right:20px;font-size:14px;display:inline-block;">
														<span style="font-size: 16px; line-height: 32px;"><span
																style="font-size: 14px; line-height: 28px;">Sistemas
																TI</span></span>
													</span></a>

											</div>

										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color:transparent;">
						<div class="block-grid"
							style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #E3EDFE; ">
							<div style="border-collapse: collapse;display: table;width: 100%;background-color:#E3EDFE;">
								<div class="col num12"
									style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top;;">
									<div style="width:100% !important;">
										<!--[if (!mso)&(!IE)]><!-->
										<div align="center" class="img-container center autowidth fullwidth"
											style="padding-right: 0px;padding-left: 0px;">
											<img align="center" alt="Image" border="0"
												class="center autowidth fullwidth"
												src="{{ "http://sistemasic.manta.gob.ec/img/lineafirmes.png" }}"
												style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; max-width: 595px; display: block;"
												title="Image" width="595" />

										</div>

										@foreach($objetos as $key => $value)

										@if ($value->Tipo=="saludousuario")

										<div
											style="color:#052d3d;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:20px;padding-right:10px;padding-bottom:0px;padding-left:15px;">
											<div
												style="line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; font-size: 12px; color: #052d3d;">
												<p
													style="line-height: 57px; text-align: center; font-size: 12px; margin: 0;">
													<span style="font-size: 38px;"><strong>Bienvenido</strong></span>
												</p>
												<p
													style="font-size: 14px; line-height: 51px; text-align: center; margin: 0;">
													<span style="font-size: 34px;"><strong><span
																style="line-height: 51px; font-size: 34px;"><span
																	style="color: #2190e3; line-height: 51px; font-size: 34px;">{{$value->Valor}}</span></span></strong></span>
												</p>
											</div>
										</div>

										@elseif($value->Tipo=="mensajeregistro")

										<div
											style="font-size: 12px; line-height: 14px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #555555;">
											<p
												style="font-size: 14px; line-height: 26px; text-align: center; margin: 0;">
												<span style="font-size: 22px; color: #800080;">¡Gracias por
													registrarse!</span></p>
											<p
												style="font-size: 14px; line-height: 16px; text-align: center; margin: 0;">
												Para activar su cuenta, por favor pulse el botón.</p>
										</div>
										@elseif($value->Tipo=="buttonlink")

										<div align="center" class="button-container"
											style="padding-top:20px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
											<a href="{{ $value->Valor}}"
												style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #fc7318; border-radius: 35px; -webkit-border-radius: 35px; -moz-border-radius: 35px; width: auto; width: auto; border-top: 1px solid #fc7318; border-right: 1px solid #fc7318; border-bottom: 1px solid #fc7318; border-left: 1px solid #fc7318; padding-top: 5px; padding-bottom: 10px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
												target="_blank"><span
													style="padding-left:40px;padding-right:45px;font-size:16px;display:inline-block;">
													<span
														style="font-size: 16px; line-height: 32px;"><strong>{{ $value->Descripcion}}</strong></span>
												</span></a>

										</div>
										@elseif($value->Tipo=="tipo")

										<div
											style="font-size: 12px; line-height: 14px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #555555;">

											<?php echo $value->Valor; ?>
										</div>



										@elseif($value->Tipo=="mensaje")

										<div
											style="font-size: 12px; line-height: 14px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #555555;">

											<p
												style="font-size: 14px; line-height: 16px; text-align: justify; margin: 0;">
												<?php echo $value->Valor; ?> </p>
										</div>

										@elseif($value->Tipo=="mensaje")

										<div
											style="font-size: 12px; line-height: 14px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #555555;">

											<p
												style="font-size: 14px; line-height: 16px; text-align: justify; margin: 0;">
												<?php echo $value->Valor; ?> </p>
										</div>
										@elseif($value->Tipo=="timeline")
										<div class="container">
											<ul class="progressbar">
												@foreach ($value->Valor as $item)
												{{-- @if ($item->valor_estado==1) --}}
													<li>{{$item->estado}} <br> {{$item->fecha}} </li>
												{{-- @else
												<li>{{$item->estado}}</li>
												@endif --}}
												@endforeach
											</ul>
										</div>
										@else
										<div
											style="font-size: 12px; line-height: 14px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #555555;">
											<p
												style="font-size: 14px; line-height: 16px; text-align: center; margin: 0;">
												{{$value->Valor}}</p>
										</div>
										@endif






										@endforeach






										<div align="center" class="img-container center autowidth fullwidth"
											style="padding-right: 20px;padding-left: 20px;">
											<div style="font-size:1px;line-height:20px"> </div><img align="center"
												alt="Image" border="0" class="center autowidth fullwidth"
												src="{{ "http://sistemasic.manta.gob.ec/img/lineafirmes.png" }}"
												style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; max-width: 595px; display: block;"
												title="Image" width="555" />


										</div>

									</div>

								</div>
							</div>
						</div>
					</div>
					</div>
					<div style="background-color:transparent;">


						<div
							style="color:#555555;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
							<div
								style="font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #555555;">
								<p style="font-size: 14px; line-height: 21px; text-align: center; margin: 0;"><span
										style="color: #2c438f; font-size: 14px; line-height: 21px;"><span
											style="font-size: 14px; line-height: 21px;">DIRECCIÓN:</span><span
											style="font-size: 14px; line-height: 21px;"> CALLE 9 Y AVENIDA 4</span>
									</span><br /><span style="color: #009bde; font-size: 14px; line-height: 21px;"><span
											style="font-size: 14px; line-height: 21px;">CORREO ELECTRÓNICO:</span> <span
											style="color: #8ab43c; font-size: 14px; line-height: 21px;">SISTEMASIC@MANTA.GOB.EC</span></span><br><span
										style="font-size: 14px;"><span
											style="color: #e09f1a; font-size: 14px; line-height: 16px;"><span
												style="font-size: 14px; line-height: 16px;">TELÉFONOS: 2 611558 / 2
												611479</span></p>
							</div>
						</div>
						<table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation"
							style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
							valign="top" width="100%">
							<tbody>
								<tr style="vertical-align: top;" valign="top">

									<table align="center" border="0" cellpadding="0" cellspacing="0"
										class="divider_content" height="0" role="presentation"
										style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 60%; border-top: 1px dotted #C4C4C4; height: 0px;"
										valign="top" width="60%">
										<tbody>
											<tr style="vertical-align: top;" valign="top">
												<td height="0"
													style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse;"
													valign="top"><span></span></td>
											</tr>
										</tbody>
									</table>
				</td>
			</tr>
		</tbody>
	</table>

	</div>

	</div>
	</div>
	</div>
	</div>
	</div>

	</td>
	</tr>
	</tbody>
	</table>

</body>

</html>