<?php
if(!isset($nobloqueo)) 
    //Autorizar(Request::path());
?>
@extends ('layout_basic_no_head')
@section ('titulo') {!! $configuraciongeneral[0] !!} @stop
@section ('scripts')
<script type="text/javascript">
    function imprimirdiv(obj){
        $(".graficos").show();
        var printWindow = window.open("", "Imprimir" );
        $("link, style").each(function() {
            $(printWindow.document.head).append($(this).clone())
        });
        //$(printWindow.document.head).append(estilo);
        var toInsert = $(obj).clone();//.html();
//        toInsert= toInsert.find('#divpagoonline').remove().end();
        //var tituloreporte= $("#tituloreporte").val();
        //$("#tdtitulo").text(tituloreporte);
        var divheader= $("#divheader").html();
        toInsert= toInsert.html(divheader+toInsert.html());
        $(printWindow.document.body).append(toInsert);
        setTimeout(function(){
            printWindow.print();
            //printWindow.close();
            $(".graficos").hide();
        },1000);
        //printWindow.print();
        //printWindow.close();
    }
</script>
@stop
@section ('contenido')
    <!-- Cabecera de Impresión-->
    <div id="divheader" style="display:none;">
        <center>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img style="width: 200px; height: auto;" src="{{ asset("img/mantafirmes.png") }}" />
                    </td>
                    <td style="text-align: center;">
                        <h2>GOBIERNO AUTÓNOMO DESCENTRALIZADO MUNICIPAL DEL CANTÓN MANTA</h2>
                        <h3>COORDINACIÓN GENERAL DE PLANIFICACIÓN PARA EL DESARROLLO</h3>
                    </td>
                    <td></td>
                </tr>
                <tr><td colspan="3"><img align="center" alt="Image" border="0" style="height: 5px;" src="{{ asset("img/lineafirmes.png")}}"  title="Image" width="100%"/></td></tr>
                <!--
                <tr>
                    <td colspan="3"><div id="tdtitulo"></div></td>
                </tr>
                -->
            </table>
        </center>
    </div>
    <!-- Fin Cabecera-->
    <div class="pull-right">
        <input id="btnImprime" type="button" class="btn btn-danger" value="Imprimir" onclick="return imprimirdiv('#restable')">
    </div>
<div class="container" id="restable">
<!-- /.panel-heading -->
<h1>{!! $configuraciongeneral[0] !!}</h1>
<hr />
@if($tablahead)
<div style="text-align: center;">
  <h3>{!! $tablahead->direccion !!}</h3>
</div>
@foreach($tabladeta as $key => $value)
<div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <span class="badge badge-primary">{!! $value->estado_documentacion !!}</span> 
                        <span class="badge badge-success">Modificado por: {!! $value->name !!}</span> 
                        <span class="badge badge-danger">{!! fechas(600,$value->created_at)." siendo las ".fechas(1000,$value->created_at) !!}</span> 
                    </div>
                    <div class="">
                        <b> Semana:</b> {!! $value->semana !!} 
                        <br>
                        <b> Tipo de actividad: </b>{!! $value->tipo_actividad !!} 
                        <br>
                        <b> Observación: </b>{!! $value->observacion !!} 
                        <br>
                        <span class="badge badge-primary"></span>
                    </div>

                </div>
                <div class="ibox-content">
                
            <table style="width:100%">
            <tbody>
<?php 
  $tablearchivos = Modules\CoordinacionCronograma\Entities\TeleTeletrabajoArchivosHistorialModel::where("id_cab_teletrabajo",$value->id)->get();
  $sw=0;
  foreach($tablearchivos as $keyfile => $valuefile)
      $documentostxt = (isset($valuefile->archivo))?$valuefile->archivo :'[]';
      $documentos = json_decode($documentostxt);
      $tatofile=0;
      foreach ($documentos as $p)
      {
          $documentotb= DB::table("tele_tmov_teletrabajo_archivo")
          ->where("id", $p)->select("archivo","created_at")->first(); 
          if($documentotb){
            $sw++;

        $tafile=(is_file(public_path() . '/teletrabajo/' . $value->id_cab_tele_trab . '/' . $documentotb->archivo))?round(filesize(public_path() . '/teletrabajo/' . $value->id_cab_tele_trab . '/' . $documentotb->archivo)/1024,2): "0";
                $tatofile+=floatval($tafile);                       
?>
            <tr style="background-color: rgb(205, 247, 200);">
                  <td>{!! $sw !!}</td>
                  <td>
                    <a href="{!! URL::to("/") . '/teletrabajo/' . $value->id_cab_tele_trab . '/' . $documentotb->archivo !!}" class="divpopup cboxElement" target="_blank" onclick="popup(this)"><i class="fa fa-file-pdf-o" aria-hidden="true"> {!! $documentotb->archivo !!} </i>
                    </a>
                  </td>
                  <td>Ingresado: {!! $documentotb->created_at !!}</td>
                       <td style="text-align: right;">{!!$tafile !!} Kb</td>
                </tr>
<?php

        }   
}
            $tatofilemb=0;
            if($tatofile>=1024)
                $tatofilemb = round($tatofile / 1024,2);
            $tatofilembtxt = ($tatofilemb>0) ? "&nbsp;<span class='label label-warning-light'>$tatofilemb MB</span>" : "";
?>
                <tr>
                  <th colspan="3">
                    <span class="label label-primary">{!! $sw !!} archivo(s) cargado(s)</span>&nbsp;
                    <span class='label label-warning'>{!! $tatofile !!} KB</span>{!! $tatofilembtxt !!}</th></tr>
              </tbody>
          </table>
                </div>
            </div>

        </div>  
@endforeach
@else
  <div style="text-align: center;">
    <h3>No existe historial para este registro... :(</h3>
</div>
@endif
@stop
