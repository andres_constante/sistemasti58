<script type="text/javascript">
        $(document).ready(function() {
                $('#tbbuzonmain').dataTable({
                    responsive : true,
                    language: {
                        "emptyTable":     "No hay datos disponibles en la tabla",
                        "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                        "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                        "infoFiltered":   "(filtered from _MAX_ total entries)",
                        "infoPostFix":    "",
                        "thousands":      ",",
                        "lengthMenu":     "Mostrar _MENU_ entradas",
                        "loadingRecords": "Cargando...",
                        "processing":     "Procesando...",
                        "search":         "Buscar:",
                        "zeroRecords":    "No se encontraron registros coincidentes",
                        "paginate": {
                            "first":      "Primero",
                            "last":       "Último",
                            "next":       "Siguiente",
                            "previous":   "Atrás"
                        },
                        "aria": {
                            "sortAscending":  ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        }
                    },
                    "dom": 'T<"clear">lfrtip',

                    "tableTools": {
                        "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                    },
                        "order": ([[ 0, 'desc' ]])
                        //"order": ([[ 2, 'asc' ], [ 1, 'asc' ]])
                        //../js/plugins/dataTables/swf/copy_csv_xls_pdf.swf

                });
});
function popup(obj)
{
    //console.log(obj);
    $(obj).colorbox({fixed:true,iframe:true, innerWidth:screen.width -(screen.width * 0.20), innerHeight:screen.height -(screen.height * 0.25)});
}
</script>
<style type="text/css">
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
    #map_canvas{
    width: 100%; height: 1000px; position: relative; overflow: hidden;
    }

</style>
    <div class="ibox-content">
        @isset($incidencias)

        <script type="text/javascript">

            var map = null;
            var marker;
            var myLatlng;
            var i;

            function initMap(lat, lng)
            {
                myLatlng = new google.maps.LatLng(lat, lng);
                
                let myOptions = {
                    zoom: 14,
                    zoomControl: true,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                <?php
                $a_tiempo=0;
                $a_culminada=0;
                $a_retrasada=0;
                    foreach ($tabla as $kk => $vv) {
                        if($vv->latitud!=""){
                            $latitud=$vv->latitud;
                            // dd($vv);
                        }else{
                            $latitud='-0.9484151471928869';
                        }
                        if($vv->longitud!=""){
                            $longitud=$vv->longitud;
                        }else{
                            $longitud='-80.72165359236527';
                        }
                        $url = "http://maps.google.com/mapfiles/ms/icons/";
                        
                        if($vv->resultado=="CULMINADA"){
                            $url.= "green-dot.png";
                            $a_culminada++;
                        }else if($vv->resultado=="A TIEMPO"){
                            $url.= "blue-dot.png";
                            $a_tiempo++;
                        }else if($vv->resultado=="RETRASADA"){
                            $url.= "red-dot.png";
                            $a_retrasada++;
                      
                        }
                        

                        echo 'marker = new google.maps.Marker({
                            position: new google.maps.LatLng('.$latitud.','.$longitud.'),
                            map: map,
                            icon: {
                                url: "'.$url.'"
                            }
                        });
                        var infowindow = new google.maps.InfoWindow();
                        google.maps.event.addListener(marker, "click", (function(marker, i) {
                            return function() {
                                infowindow.setContent("<b>ID: </b>'.$vv->id.'<br> <b>Denuncia: </b>'.$vv->nombre_denuncia.'<br> <b>Parroquia: </b>'.$vv->parroquia.'<br> <b>Observacion: </b>'.str_replace(array("\r\n", "\r","\n","'",'"'), "'",$vv->observacion).'<br><b>Observación de respuesta: </b> '.str_replace(array("\r\n", "\r","\n","'",'"'), "",$vv->observacion_respuesta).'<br>");
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        
                        
                        
                        ';
                    }

                    echo '$("#t_a_tiempo").html('.$a_tiempo.');';
                    echo '$("#t_retrasada").html('.$a_retrasada.');';
                    echo '$("#t_culminada").html('.$a_culminada.');';


                ?>
            }

            function mostrar(){
                    var agenda=document.getElementById('map_canvas').getAttribute('display');
                    if ($('#map_canvas').is(':hidden')) {
                        $('#map_canvas').show();
                    } else {
                        $('#map_canvas').hide();
                    }


                    // $('#agenda').show();
            }
            $(function () {
                initMap(-0.9484151471928869, -80.72165359236527);
            });
        </script>
        <div>
    <table class="table">
      <tr style="font-size: larger;">
        <td>A TIEMPO  <img src="http://maps.google.com/mapfiles/ms/icons/blue-dot.png" alt=""><br><span class="badge" style="background-color: #00e64d; font-size: large;" id="t_a_tiempo">0</span></td>
        <td>RETRASADA <img src="http://maps.google.com/mapfiles/ms/icons/red-dot.png" alt=""><br><span class="badge"  style="background-color: #ff9900; font-size: large;"  id="t_retrasada">0</span> </td>
        <td>CULMINADA  <img src="http://maps.google.com/mapfiles/ms/icons/green-dot.png" alt=""><br><span class="badge" style="background-color: #ffff6e; font-size: large;"  id="t_culminada">0</span></td>
      </tr>
    </table>
    
    

  </div>
        <button class="btn btn-info" onclick="mostrar()"><i class="fa fa-eye" aria-hidden="true"></i>  Mostrar mapa</button>
        <div style="width: 100%; height: 800px;" hidden id="map_canvas"></div>
        </div>
        
        @endisset
    </div>
    <div class="ibox-content">
                <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
                    <thead>
                        <tr>                            
                            <th>ID</th>                        
                           @foreach($objetos as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                            <th>Acción</th>
                    </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>ID</th>                            
                           @foreach($objetos as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                            <th>Acción</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($tabla as $key => $value)
                    <tr>                        
                        <td>{{ $value->id }} </td>                        

                        @foreach($objetos as $keycam => $valuecam)
                               <td>
                               <?php
                               if($valuecam->Tipo=="file")
                               {     $cam=$valuecam->Nombre;
                                    if($value->$cam!="")
                                        $cadena="echo '<a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$value->".$valuecam->Nombre.").' class=\"btn btn-success dropdown-toggle divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i></a>';";
                                    else
                                        $cadena="echo '';";
                               }else
                               {
                                $cadena="echo trim(\$value->".$valuecam->Nombre.");";
                               }

                                ?>
                                    @if($valuecam->Nombre=="valor_predefinido")
                                        <textarea style="margin: 0px; width: 343px; height: 185px;" disabled>
                                            <?php eval($cadena);?>
                                         </textarea>
                                    @else
                                        <?php eval($cadena); ?>
                                    @endif
                               </td>
                        @endforeach
                        <td>
                        <!--      Cuando es vista de muchos botones -->
                                        @if(!isset($show))

                                        <a href="{{ URL::to($configuraciongeneral[1]."/".$value->id) }}" onclick="popup(this)" target="_blank"">
                                            <i class="fa fa-newspaper-o" ></i></a>&nbsp;&nbsp;
                                        @endif
                                            @if(!isset($edit))
                                                <a href="{{ URL::to($configuraciongeneral[1]."/".$value->id."/edit?menu=no") }}" onclick="popup(this)"><i class="fa fa-pencil-square-o"></i></a>
                                            @endif                                    
                        </td>
                    </tr>
                      @endforeach
                     </tbody>
                    </tfoot>

                </table>
                        {{-- $tabla->links() --}}
                </div>
