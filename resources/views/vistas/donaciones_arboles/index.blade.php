<?php
if(!isset($nobloqueo))
//    Autorizar(Request::path());
?>
<!--extends('layout')-->
@extends(Input::has("menu") ? 'layout_basic_no_head':'layout' )
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
@include("vistas.includes.mainjs")
<script type="text/javascript">
    ////donaciones
function cambiar_estado(id,estado,token,fecha,direccion,cantidad,tipo, lng, lat,mapa) {
    const swalWithBootstrapButtons = Swal.mixin({
         customClass: {
             confirmButton: 'btn btn-success',
             cancelButton: 'btn btn-danger',
            //  container: 'sweet_containerImportant',
             popup:'sweet_popupImportant'
             
         },
         buttonsStyling: false
     })
    swalWithBootstrapButtons.fire({
             title: 'Donación',
             html:
             '<div class="row"><div class="col-lg-6"><span style="font-size: medium;">Tipo</span><br>'+
             '<select id="id_arbol" type="text" class=" form-control swal2-text"></select></div>'+
             '<div class="col-lg-6"><span style="font-size: medium;">Cantidad</span><br>'+
             '<input id="cantidad" type="text" value="'+cantidad+'" class="form-control swal2-text"></div></div>'+
             '<div style="font-size: medium;"><span style="font-size: medium;">Direccion</span><br>'+
             '<textarea id="txt_direccion" type="text" class="form-control swal2-textarea">'+direccion+'</textarea></div>'+
             '<div><span style="font-size: medium;">Mapa</span><br>'+
             '<div id="mapa_donacion" style="height: 200px;" ></div><input  id="mapa_lat_lng" value="'+mapa+'"></div>'+
             '<div><span style="font-size: medium;">Fecha de entrega</span><br>'+
             '<input id="fecha_entrega" type="date" value="'+fecha+'" class="form-control swal2-date"></div>'+
             '<div class="row"><div class="col-lg-6"><span style="font-size: medium;">Seleccione el estado</span><br>'+
             '<select id="estado" class="form-control swal2-select"> <option value="Por entregar">Por entregar</option><option value="Entregado">Entregado</option> </select></div>'+            
             '<div  class="col-lg-6"><span style="font-size: medium;">Eliminar registro</span><br>'+
             '<select id="eliminar" class="form-control swal2-select"> <option value="NO">NO</option><option value="SI">SI</option> </select></div></div>'
             
             ,            
             showCancelButton: true,
             confirmButtonText: 'Acualizar!',
             cancelButtonText: 'Cancelar',
             showLoaderOnConfirm: true,
             width:800,
             //customClass:'swal-height-show-agenda_2',
             onOpen: function() {
                // inicializar_mapBox('mapa_donacion', lng, lat);
                $.get("https://portalciudadano.manta.gob.ec/get_tipo_arboles", function(data) {
                        $("#id_arbol").empty();  
                        $.each(data.data, function(key, element) {
                            $("#id_arbol").append("<option value='" + element.id + "'>" + element.tipo + "</option>");
                        });
                });
                 document.getElementById('estado').value=estado;
                 document.getElementById('id_arbol').value=tipo;

                 mapboxgl.accessToken = 'pk.eyJ1IjoiY2dwZzk0IiwiYSI6ImNrOHl6cDByMDA0ZTAzZG55ZDNhNzR6ZzQifQ.S6YdKKPLdCVTVd-xxvim0g';
                map = new mapboxgl.Map({
                    container: 'mapa_donacion', // container id
                    style: 'mapbox://styles/cgpg94/ck8yzs10c04nf1io4oth7zjuf', // stylesheet location
                    center: [lng, lat], // starting position [lng, lat]
                    zoom: 16, // starting zoom,
                    attributionControl: false
                });
                marker = new mapboxgl.Marker({
                    draggable: true
                }).setLngLat([lng, lat]).addTo(map);

                function onDragEnd() {
                    var lngLat = marker.getLngLat();
                    try {
                        lat=lngLat.lat;
                        lng=lngLat.lng;
                        document.getElementById('mapa_lat_lng').value = '[' + lngLat.lat + ',' + lngLat.lng + ']';
                    } catch (error) {

                    }
                }
                marker.on('dragend', onDragEnd);
                 
             },
             preConfirm: (obs) => {
            
                 var estado=document.getElementById('estado').value
                 var id_arbol=document.getElementById('id_arbol').value
                 var estado=document.getElementById('estado').value
                 var cantidad=document.getElementById('cantidad').value
                 var fecha_entrega=document.getElementById('fecha_entrega').value
                 var mapa_lat_lng=document.getElementById('mapa_lat_lng').value
                 var eliminar=document.getElementById('eliminar').value
                 var txt_direccion=document.getElementById('txt_direccion').value
                
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token":token,
                         "id": id,
                         "id_arbol": id_arbol,
                         "cantidad": cantidad,
                         "fecha_entrega": fecha_entrega,
                         "mapa_lat_lng": mapa_lat_lng,
                         "eliminar": eliminar,
                         "txt_direccion": txt_direccion,
                         "estado": estado
                
                     },
                     
                     url: 'https://portalciudadano.manta.gob.ec/cambiar_estado',
                    //  url: 'http://localhost:8091/mantaentusmanos/public/cambiar_estado',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     tabla.draw();

                                 });
                             }else{
                                cambiar_estado(id,estado,token,fecha_entrega,txt_direccion,cantidad,id_arbol, lng, lat,mapa_lat_lng);
                             }

                         } catch (error) {

                         }

                     },
                     error: function (data) {
                        cambiar_estado(id,estado,token,fecha_entrega,txt_direccion,cantidad,id_arbol, lng, lat,mapa_lat_lng);
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
}
function mostrar_mapa_donacion(lng,lat) {
    const swalWithBootstrapButtons = Swal.mixin({
         customClass: {
             confirmButton: 'btn btn-success',
             cancelButton: 'btn btn-danger',
            //  container: 'sweet_containerImportant',
             popup:'sweet_popupImportant'
             
         },
         buttonsStyling: false
     })
    swalWithBootstrapButtons.fire({
             text: "Ingrese observación por la cual se reprograma",
             //type: 'warning',
             html:
             '<div id="mapa_donacion" style="height: 400px;" ></div>'
             ,            
             showCancelButton: false,
             confirmButtonText: 'OK!',
             showLoaderOnConfirm: true,
             width:800,
             onOpen: function() {
                
                inicializar_mapBox('mapa_donacion', lng, lat);
             },
             preConfirm: (obs) => {
            
             },
             allowOutsideClick: () => !Swal.isLoading()
         });
}

function plaquita(placa) {
    if (placa == null || placa == '') { placa = 0; }
    $("#gif_cargando-placa_"+placa).css('display','block');;
    console.log($("#gif_cargando-placa_"+placa));
    
    $.ajax({
        type: "GET",
        url: '{{$configuraciongeneral[8]}}/consultar_placa/' + placa,
        statusCode: { 404: function() { toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :("); }, 401: function() { toastr["error"]("Inicie sesión de nuevo por favor... :("); } },
        error: function(objeto, opciones, quepaso) { toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :("); },
        success: function(respuesta) {
            if (respuesta.estado) { toastr["info"](respuesta.msg); } else {
                toastr["error"](respuesta.msg);
            }

            $("#gif_cargando-placa_"+placa).css('display','none');

        }
    });
}
function licencia(cedula) {
    if (cedula == null || cedula == '') { cedula = 0; }
    $("#gif_cargando-licencia_"+cedula).css('display','block');
    
    $.ajax({
        type: "GET",
        url: '{{$configuraciongeneral[8]}}/validarPersona/' + cedula,
        statusCode: { 404: function() { toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :("); }, 401: function() { toastr["error"]("Inicie sesión de nuevo por favor... :("); } },
        error: function(objeto, opciones, quepaso) { toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :("); },
        success: function(respuesta) {
            if (respuesta.estado) { toastr["info"](respuesta.msg); } else {
                toastr["error"](respuesta.msg);
            }
            $("#gif_cargando-licencia_"+cedula).css('display','none');
        }
    });

}
function actividaesRuc(ruc,id) {
    if (ruc == null || ruc == '') { ruc = 0; }
  
    $.ajax({
        type: "GET",
        url: '{{$configuraciongeneral[8]}}/actividaesRuc/' + ruc,
        statusCode: { 404: function() { toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :("); }, 401: function() { toastr["error"]("Inicie sesión de nuevo por favor... :("); } },
        error: function(objeto, opciones, quepaso) { toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :("); },
        success: function(respuesta) {
            if (respuesta.estado) {
                try {
                    toastr["info"](respuesta.actividades, { timeOut: 19500 });
                    document.getElementById("actividades_"+id).innerHTML=respuesta.actividades;
                } catch (error) {
                    
                }
            } else {
                toastr["error"](respuesta.msg);
            }
        }
    });

}

function mostrarImagen(ruta) { 
    Swal.fire({
        imageUrl: ruta,
        width:800,
        imageHeight: 400
    })

}
function antender_permiso(id,t,token) {
    const swalWithBootstrapButtons = Swal.mixin({
         customClass: {
             confirmButton: 'btn btn-success',
             cancelButton: 'btn btn-danger',
            //  container: 'sweet_containerImportant',
             popup:'sweet_popupImportant'
             
         },
         buttonsStyling: false
     })

    var tabla_ = $("#"+t).html();
    console.log(tabla_);
    
    swalWithBootstrapButtons.fire({
             title: 'PERMISO DE CIRCULACIÓN',
             html:
             '<div class="row">'+
             '<div class="col-lg-12"><br>'+tabla_+'</div>'+
             '<div class="col-lg-12"><span style="font-size: medium;">Aprobar</span>'+
             '<select id="estado" class="form-control"> <option value="SI">SI</option><option value="NO">NO</option> </select></div>'+            
             '<div  class="col-lg-12"><span style="font-size: medium;">Observación</span><br>'+
             '<textarea id="observacion" type="text" class="form-control swal2-textarea"></textarea></div></div>'
             
             ,            
             showCancelButton: true,
             confirmButtonText: 'Guardar!',
             cancelButtonText: 'Cancelar',
             showLoaderOnConfirm: true,
             width:800,
             //customClass:'swal-height-show-agenda_2',
             onOpen: function() {
                
             },
             preConfirm: (obs) => {
            
                // $("#estado").val(est);
                //  var fecha_caducidad=document.getElementById('fecha_caducidad').value
                 var estado=document.getElementById('estado').value
                 var observacion=document.getElementById('observacion').value
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token":token,
                         "id": id,
                        //  "fecha_caducidad": fecha_caducidad,
                         "estado": estado,
                         "observacion": observacion,
                         "cedula":'{{Auth::user()->cedula}}'
                
                     },
                     
                     url: '{{$configuraciongeneral[8]}}/antender_circulacion',
                    //  url: 'http://localhost:8091/mantaentusmanos/public/antender_circulacion',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     tabla.draw();

                                 });
                             }else{

                                toastr["error"](data['msg']);
                                antender_permiso(id,t,token);
                             }

                         } catch (error) {

                         }

                     },
                     error: function (data) {
                        toastr["error"](data['msg']);
                        antender_permiso(id,t,token);
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
}

function antender_establecimiento(id,token,estado,observacion,t) {
    const swalWithBootstrapButtons = Swal.mixin({
         customClass: {
             confirmButton: 'btn btn-success',
             cancelButton: 'btn btn-danger',
            //  container: 'sweet_containerImportant',
             popup:'sweet_popupImportant'
             
         },
         buttonsStyling: false
     })

    var tabla_ = $("#"+t).html();
    
    swalWithBootstrapButtons.fire({
             title: 'SOLICITUD DE GUIA COMERCIAL',
             html:
             '<div class="row">'+
             '<div class="col-lg-12"><br>'+tabla_+'</div>'+
             '<div class="col-lg-12"><span style="font-size: medium;">Selecione el estado del establecimiento</span>'+
             '<select id="estado" class="form-control"> <option value="PENDIENTE">PENDIENTE</option><option value="APROBADO">APROBADO</option><option value="NEGADO">NEGADO</option> </select></div>'+            
             '<div  class="col-lg-12"><span style="font-size: medium;">Observación</span><br>'+
             '<textarea id="observacion" type="text" class="form-control swal2-textarea"></textarea></div></div>'
             ,
             showCancelButton: true,
             confirmButtonText: 'Guardar!',
             cancelButtonText: 'Cancelar',
             showLoaderOnConfirm: true,
             width:800,
             onOpen: function() {
                document.getElementById('estado').value=estado;
                document.getElementById('observacion').value=observacion;
             },
             preConfirm: (obs) => {
                 estado=document.getElementById('estado').value
                 observacion=document.getElementById('observacion').value
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token":token,
                         "id": id,
                         "estado": estado,
                         "observacion": observacion,
                         "cedula":'{{Auth::user()->cedula}}'
                
                     },
                     
                     url: '{{$configuraciongeneral[8]}}/antender_establecimiento',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     tabla.draw();

                                 });
                             }else{

                                toastr["error"](data['msg']);
                                antender_establecimiento(id,token,estado,observacion,t)
                             }

                         } catch (error) {

                         }

                     },
                     error: function (data) {
                        toastr["error"](data['msg']);
                        antender_establecimiento(id,token,estado,observacion,t)
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
}



@if(Auth::user()->id==1)
            function eliminar_tramite(id,token) {
            
              Swal.fire({
                title: '¿Desea inactivar este registro?',
                showCancelButton: true,
                confirmButtonText: 'INACTIVAR',
                cancelButtonText: 'CANCELAR',
                showLoaderOnConfirm: true,
                preConfirm: (codigo) => {
                    $.ajax({
                        type: "DELETE",
                        data: {
                            "_token":token,
                        },
                        url: '{{$configuraciongeneral[8]}}/tramites/tramitesenlinea/'+id,
                        success: function (data) {
                            try {
                                if (data.estado) {
                                    Swal.fire({
                                        position: 'center',
                                        type: 'success',
                                        title: data['msg'],
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then(() => {
                                        try {
                                          tabla.draw();
                                        } catch (error) {
                                          location.reload();
                                        }

                                    });
                                } else {
                                    Swal.fire({
                                        type: 'error',
                                        title: 'Oops...',
                                        text: data['msg']
                                    });
                                }

                            } catch (error) {

                            }

                        },
                        error: function (data) {
                          Swal.fire({
                                        type: 'error',
                                        title: 'Oops...',
                                        text: data['msg']
                                    });
                        }
                    });

                },
                allowOutsideClick: () => !Swal.isLoading()
            });
              
            }

          
@endif


var URL="{!! "$configuraciongeneral[6]" !!}";
var tabla;

$(document).ready(function() {

            tabla= $('#tbbuzonmain').DataTable({
                responsive : true,
                destroy: true,
                processing: true,
                serverSide: true,
                // "searching": false,
                lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
                ajax: {
                    url: URL,
                    dataType: "json",
                    type: "GET",
                    "data": function ( d ) {
                        d._token = "{{csrf_token()}}", 
                        @if(isset($filtros))
                        <?php
                            foreach($filtros as $filtro){
                                echo 'd.'.$filtro->Nombre.'= $("#'.$filtro->Nombre.'").val();' ;
                            }
                        ?>  
                               
                        @endif
                        d.cedula='{{Auth::user()->cedula}}';
                        console.log(d);
                    }

    
                },

                columns:[
                    @foreach($objetos as $key => $value)
                            { "data":'{{$value->Nombre}}' },
                    @endforeach

                    { "data": 'acciones' }
                ],
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                order: ([[ 0, 'desc' ]]),
                
            });
            
    
            
            $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.10), innerHeight:screen.height -(screen.height * 0.30)});
            $("#estado_solicitud").change(function (e) { 
                tabla.draw();
                
            });
            
});//End Document Ready


</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }

    .DTTT_Print #page-wrapper {
        margin: 0;
        background: #fff;
    }

    button.DTTT_button,
    div.DTTT_button,
    a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    button.DTTT_button:hover,
    div.DTTT_button:hover,
    a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }

    .swing li {
        opacity: 0;
        transform: rotateY(-90deg);
        transition: all 0.5s cubic-bezier(.36, -0.64, .34, 1.76);
    }

    .colores {
        list-style: none;
        height: 0;
        line-height: 2em;
        margin: 0;
        padding: 0 0.5em;
        background: #fafaf9;
        /* display: none */
        overflow: hidden;
        border: 1px solid #d8d8d8;
        box-shadow: 0 3px 8px rgba(0, 0, 0, 0.25);
        margin-bottom: 1.5% !important;
    }

    li.show {
        height: 10%;
        margin: 2px 0;
    }

    .swing {
        perspective: 100px;
    }

    .swing li {
        /* opacity: 0; */
        transform: rotateX(-90deg);
        transition: all 0.5s cubic-bezier(.36, -0.64, .34, 1.76);
    }

    .swing li.show {
        /* opacity: 1; */
        transform: none;
        transition: all 0.5s cubic-bezier(.36, -0.64, .34, 1.76);
    }


    .scrollbar {
        overflow: auto;
        width: 0.5em !important;
        scroll-behavior: smooth !important;
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3) !important;
        background-color: #f2f2f2 !important;
        outline: 1px solid #f2f2f2 !important;
        /* border-radius: 10px !important; */
        height: auto !important;
        width: 100% !important;
    }

    ::-webkit-scrollbar {
        width: 0.5em !important;
        scroll-behavior: smooth !important;
    }

    ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3) !important;
    }

    ::-webkit-scrollbar-thumb {
        background-color: darkgrey !important;
        outline: 1px solid slategrey !important;
        border-radius: 10px !important;
    }
</style>

@stop
@section ('contenido')
<h1 style="background-color: #FFFFFF; padding: 1% 0 1% 2%;"> {{ $configuraciongeneral[0] }}</h1>


<div class="ibox float-e-margins">
    <div class="ibox-title">
        @if (Session::has('message'))
        <script>
            $(document).ready(function() {
                toastr.options = {
                    "positionClass": "toast-bottom-right"
                };
                toastr["success"]("{{ Session::get('message') }}");
            });
        </script>
        <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif

        @if (Session::has('error'))
        <script>
            $(document).ready(function() {  
                toastr["error"]("{{ Session::get('error') }}");
            });
        </script>
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
    </div>
    <div id="divindex" class="ibox-content imgback">
        <div class="row">
            <div class="col-md-12">
                @if (isset($filtros))
                {!! Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' ,
                'id'=>'form1','class'=>'form-row')) !!}
                @foreach ($filtros as $key => $campos)

                @if($campos->Tipo=="date")
                <div class="col-md-3 col-lg-2" id="divobj-{{ $campos->Nombre }}">
                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-6
                    control-label")) !!}
                    <div class="col-lg-12 col-md-12">
                        <div class="input-group date">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>

                            {!! Form::text($campos->Nombre, null , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha',
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            )) !!}
                        </div>

                    </div>
                </div>
                @elseif($campos->Tipo=="select")
                <div class="col-md-3 col-lg-3" id="group-{{$campos->Nombre}}">
                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-12
                    control-label")) !!}
                    <div class="col-lg-12 col-md-12">
                        {!! Form::select($campos->Nombre,$campos->Valor,$campos->ValorAnterior,
                        array('class' => "form-control")) !!}
                    </div>
                </div>
                @else
                <div class="col-lg-2" id="group-{{$campos->Nombre}}">
                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-12 control-label"))
                    !!}
                    <div class="col-lg-12">
                        {!! Form::text($campos->Nombre, $campos->Valor=="Null" ?
                        Input::old($campos->Valor):$campos->Valor ,
                        array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion
                        )) !!}
                    </div>
                </div>
                @endif
                @endforeach
                <br>

                {!! Form::close() !!}
                @endif
            </div>
        </div>
        <br>
        <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display">
            <thead>
                <tr>
                    @foreach($objetos as $key => $value)
                    <th>{{ str_replace("(*)","",$value->Descripcion) }}</th>
                    @endforeach
                    <th>Acción</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    @foreach($objetos as $key => $value)
                    <th>{{ str_replace("(*)","",$value->Descripcion) }}</th>
                    @endforeach
                    <th>Acción</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach($tabla as $key => $value)
                <tr>
                    @foreach($objetos as $keycam => $valuecam)
                    <td>
                        <?php
                               if($valuecam->Tipo=="file")
                               {     $cam=$valuecam->Nombre;
                                    if($value->$cam!="")
                                        $cadena="echo '<a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$value->".$valuecam->Nombre.").' class=\"btn btn-success dropdown-toggle divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i></a>';";
                                    else
                                        $cadena="echo '';";
                               }else
                               {
                                $cadena="echo trim(\$value->".$valuecam->Nombre.");";
                               }

                                ?>
                        @if($valuecam->Nombre=="valor_predefinido")
                        <textarea style="margin: 0px; width: 343px; height: 185px;" disabled>
                                            <?php eval($cadena);?>
                                         </textarea>
                        @else
                        <?php eval($cadena); ?>
                        @endif
                    </td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
            </tfoot>

        </table>
    </div>
</div>


@stop