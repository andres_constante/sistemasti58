<?php
if(!isset($nobloqueo))
    //Autorizar(Request::path());
    ?>
@extends ('layout_basic_no_head')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
    <script>
        $(document).ready(function() {
            toastr.options = {
                "positionClass": "toast-bottom-right"
            };
            toastr["success"]("{{ $msg }}");
        });
    </script>
@stop
@section ('contenido')
    <div class="alert alert-info"><h2>{{ $msg }}</h2></div>
@stop

