<p>&nbsp;</p>
<center>
<div>
<div style="width: 600px; padding: 15px; border-left: 1px solid #B4B5B5;">
    <img style="width: 40%; height: auto;" src="http://sistemasic.manta.gob.ec/img/mantafirmes.png" />
</div>
<div style="background-color: white; width: 600px; text-align: justify; padding: 15px; border-left: 1px solid #B4B5B5; border-bottom: 1px solid #B4B5B5;">
    <img align="center" alt="Image" border="0" class="center autowidth fullwidth" src="http://sistemasic.manta.gob.ec/img/lineafirmes.png"  title="Image" width="100%"/>
<h3 style='text-align: center;'>{{ trans('html.main.sistema') }}</h3>
<p>Hola, @if(isset($usuario))<strong> {{ $usuario }}</strong>:@endif</p>
<!--OBJETOS -->
@foreach($objetos as $key => $value)

    @if($value->Tipo=="buttonlink")
        <p>
            Para más información haga clic en el siguiente enlace:<br><br>
            <a href="{{ $value->Valor }}">{{ $value->Valor }}</a>
        </p>
        @elseif($value->Tipo=="tipo")
            <p>
                <strong>{{$value->Valor}}</strong>
            </p>
        @elseif($value->Tipo=="mensaje")

                <p>{{$value->Valor}}</p>
        @else
    <div style="font-size: 12px; line-height: 14px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #555555;">
        <p style="font-size: 14px; line-height: 16px; text-align: center; margin: 0;">{{$value->Valor}}</p>
        </div>
    @endif
@endforeach
<!--OBJETOS -->

<p>Gracias por su atenci&oacute;n,</p>
<p>&nbsp;</p>
<p>Atentamente, <br /> <strong>GAD Municipal Manta</strong>.</p>
<i>Correo generado automáticamente: {{ fechas(300) }}</i>
</div>
</div>
</center>