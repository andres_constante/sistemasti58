@if(isset($timeline))
    @php
    $colores=array(
      "navy-bg",
      "blue-bg",
      "red-bg",
      "yellow-bg",
      "lazur-bg",
      "navy-bg",
      "blue-bg",
      "lazur-bg",
      "blue-bg",
      "lazur-bg",
      "blue-bg",
      "yellow-bg",
      "navy-bg",
      "red-bg",
    "lazur-bg");
    @endphp
        <h1>Historial</h1>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
           @foreach($timeline[0] as $key => $value)
           <div class="vertical-timeline-block">
            <div class="vertical-timeline-icon {!! $colores[rand(0,14)]!!}">
                <i class="fa fa-clock-o"></i>
            </div>
            <div class="vertical-timeline-content">            
                <p>
                    <!-- Recorrido de Objetos-->
                    @foreach($timeline[1] as $keycam => $valuecam)
                        @if($valuecam->Nombre=="updated_at")
                            @continue;
                        @endif
                        <strong>{{ $valuecam->Descripcion }}: </strong>
                        @php
                        if($valuecam->Tipo=="file")
                        {     
                            $cam=$valuecam->Nombre;
                            if($value->$cam!="")
                                $cadena="echo '<a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$value->".$cam.").' class=\"btn btn-success dropdown-toggle divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i></a>';";
                            else
                                $cadena="echo '';";
                        }else
                        {
                            $cadena="echo trim(\$value->".$valuecam->Nombre.");";                      
                        }
                        eval($cadena);
                        @endphp  
                        <br>                    
                    @endforeach
                </p>
                <!--a href="#" class="btn btn-sm btn-primary"> More info</a-->
                <span class="vertical-date">
                    {!! fechas(900,$value->updated_at); !!} <br/>
                    <small>{!! fechas(600,$value->updated_at). " ".fechas(1000,$value->updated_at) !!}</small>                                        
                </span>            
            </div>
        </div>
        @endforeach
    </div>
@endif