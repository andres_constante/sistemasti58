<?php
if(!isset($nobloqueo) && Auth::user()->id_perfil!=4)
    Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
<style>
.circulo {
     width: 100px;
     height: 100px;
     -moz-border-radius: 50%;
     -webkit-border-radius: 50%;
     border-radius: 50%;
     background: white;
     padding: 1%;
     -moz-box-shadow: 0px 6px 5px #2c438f96;
  -webkit-box-shadow: 0px 6px 5px #2c438f96;
  box-shadow: 0px 6px 5px #2c438f96;
     
     
     
}
.zoom{
        /* Aumentamos la anchura y altura durante 2 segundos */
        transition: width 2s, height 2s, transform 2s;
        -moz-transition: width 2s, height 2s, -moz-transform 2s;
        -webkit-transition: width 2s, height 2s, -webkit-transform 2s;
        -o-transition: width 2s, height 2s,-o-transform 2s;
    }
    .zoom:hover{
        /* tranformamos el elemento al pasar el mouse por encima al doble de
           su tamaño con scale(2). */
        transform : scale(1.3);
        -moz-transform : scale(1.3);      /* Firefox */
        -webkit-transform : scale(1.3);   /* Chrome - Safari */
        -o-transform : scale(1.3);        /* Opera */
    }
.swal-wide{
    width:auto !important;
}
</style>
@section ('scripts')
<script type="text/javascript">
$(document).ready(function() {
  $("#reporte").change(function (e) { 
    
    window.location.assign("{{URL::to('')}}/"+this.value)
    
  });
});
</script>

@stop
@section ('contenido')

    <?php
        $bolitas=$res;

        // dd($bolitas);
    ?>
    <h1 style="background-color: #FFFFFF; padding: 1% 0 1% 2%;"> {{ $configuraciongeneral[0] }}</h1>
    <div class=" col-md-12 col-lg-12" style="background-color: #ffffff;">
        <br>
            @isset($objetos_reportes)
                @foreach($objetos_reportes as $i => $campos)     
                <div class="{{((isset($campos->Ancho))?'col-lg-'.$campos->Ancho.'':'col-lg-6') }} "  style="{{((isset($campos->Oculto))?'display:none;':'') }}">   
                    {!! Form::label($campos->Nombre, $campos->Descripcion.':', array("class"=>"control-label")) !!}     
                    @if($campos->Tipo=="select")
                         {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                                
                    @else
                         
                    @endif
                </div>
                @endforeach
            
            @endisset
    </div>    
    <div class=" col-md-12 col-lg-12" style="background-color: #ffffff;">
        
    @foreach($bolitas as $bolita)
        <div class="col-md-6  zoom col-lg-6" style="text-align: center; padding: 1%;">
        <img class="circulo " style="box-shadow: 0px 6px 5px #0092d6;"src="{{$bolita['icono_app']}}" width="35%" onclick="javascript:location.href='{{$bolita['ruta']}}'"  height="30%">
        
        <div style="padding-top: 10px;"><p style="font-size: mediun; ">{{$bolita['nombre']}}</p></div>

          
        </div>
    @endforeach
</div>
@stop