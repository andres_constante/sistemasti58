<div class="modal fade" id="modal-direcciones" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Elija a quién desea solicitar informe</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        {{-- {!! Form::open(['method' => 'POST', 'id' => 'form_direcciones']) !!} --}}
                            <select name="direcciones[]" id="direcciones" class="selective-tags" multiple="multiple" data-placeholder="Seleccione">
                                @foreach ($direcciones as $idx => $item)
                                    <option value="{{ $idx }}">{{ $item }}</option>
                                @endforeach
                            </select>
                        {{-- {!! Form::close() !!} --}}
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" onclick="solicitarInforme({{ $tabla->id }})" class="btn btn-primary">Solicitar</button>
            </div>
        </div>
    </div>
</div>
