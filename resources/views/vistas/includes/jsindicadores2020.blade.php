<script type="text/javascript">

    $(function () {
        try {
            if($('#id_sancion').length){
            sanciones();
            // alert();
        }
        $('#id_sancion').change(function (e) { 
            sanciones();
            
        });
        
        arbolesmetros()
        $('#id_tipo_servicio').change(function (e) { 
            arbolesmetros();
            
        });

        actas()
        $('#acta_emitida').change(function (e) { 
            actas();
            
        });
        } catch (error) {
            
        }
       

       $("#custodia").change(function (e) { 
            custodia();   
       });
       $("#custodia").trigger('change');
       $("#id_tipo_mascota").change(function (e) { 
            tipo_mascota();   
       });
       $("#id_tipo_mascota").trigger('change');

        $("#sp").change(function (e) { 
      if(this.value=='SI'){
        $("#div_cedula").show();
        $("#div_nombres").show();
        $("#div_apellidos").show();
      }else{
        $("#div_cedula").hide();
        $("#div_nombres").hide();
        $("#div_apellidos").hide();    
      }
    });

    $("#id_tipo_atencion").change(function (e) { 
      $.ajax({
              type: "GET",
              url: "{{ URL::to('coordinacioncronograma/consultarvalorAtencion') }}?tipo="+this.value,
              success: function (response) {
                  $("#inversion").val(response);
              }
          });
      
    });




    });

    function arbolesmetros(){
        // var valor = $('#id_tipo_servicio').val();
        // var id = $('#id_indicador').val();
        // if(id==6){
        //     if(valor==1)
        // {
        //     $('#label_cantidad').text('Cantidad:');
        // }else{
        //     $('#label_cantidad').text('Metros cuadrados:');
        // }
        // }
        


       
    }
    function custodia(){
        
        var id = $('#custodia').val();
        if(id=="SI"){
            $("#div_ci_custodio").show();
            $("#div_apellido_custodio").show();
            $("#div_nombre_custodio").show();
        }else{
            $("#div_ci_custodio").hide();
            $("#div_apellido_custodio").hide();
            $("#div_nombre_custodio").hide();
        }       
    }
    function tipo_mascota(){
        
        var id = $('#id_tipo_mascota').val();
        $.ajax({
            type: "GET",
            url: "{{URL::to('')}}/coordinacioncronograma/consultar_mascotas?tipo="+id,
            data: {
                'id':id
            },
            success: function (response) {
                $("#id_raza").empty();
                $.each(response, function(key, element) {
                  $("#id_raza").append("<option value=\'" + element.id + "\'>" + element.raza+"</option>");
                });
                $("#id_raza").trigger("chosen:updated");
            }
        });
             
    }

    

    function actas(){
        var valor = $('#acta_emitida').val();
        if(valor=='INFRACCIÓN'){
            $('#estado_indicador').val('FINALIZADA');
        }else{

            $('#estado_indicador').val('EN PROCESO');
        }
        // if(valor.indexOf("1") > -1)
        // {
        //     $('#numero_acta').val(1);
        //     console.log('1');
        // }else{
        //     $('#numero_acta').val(2);
        //     console.log('2');
        // }


       
    }


    function sanciones(){
        var id=$("#id_sancion").val();
        $.ajax({
            type: "GET",
            url: "{{URL::to('')}}/coordinacioncronograma/consultar_sancion",
            data: {
                'id':id
            },
            success: function (response) {
                $('#valor').val(response);
            }
        });
    }
    function agregar(tipo,id_etiquita_resultado) {
      

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger',
                    //  container: 'sweet_containerImportant',
                    popup: 'sweet_popupImportant'

                },
                buttonsStyling: false
            });

            swalWithBootstrapButtons.fire({
                title: 'Ingrese el nombre item',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Agregar!',
                cancelButtonText: 'No, cancelar',
                showLoaderOnConfirm: true,
                //customClass:'swal-height-show-agenda_2',
                preConfirm: (obs) => {
                    $.ajax({
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "nombre": obs,
                            "tipo": tipo,
                        },
                        url: '{{URL::to('')}}/coordinacioncronograma/agregar_item',
                        success: function(data) {
                            try {

                                if (data['estado'] == 'ok') {
                                    Swal.fire({
                                        position: 'center',
                                        type: 'success',
                                        title: data['msg'],
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    $("#"+id_etiquita_resultado).empty();
                                    $.each(data['datos'], function(key, element) {
                                        $("#"+id_etiquita_resultado).append("<option value=\'" + element.id + "\'>" + element.nombre + "</option>");
                                    });
                                   $("#"+id_etiquita_resultado).trigger("chosen:updated");
                                   $("#"+id_etiquita_resultado).val(data['seleccionado']);
                                   $("#"+id_etiquita_resultado).trigger("chosen:updated");


                                } else {
                                    Swal.fire({
                                        type: 'error',
                                        title: data['msg']
                                        // text: data['msg']
                                    });
                                   $("#"+id_etiquita_resultado).empty();
                                    $.each(data['datos'], function(key, element) {
                                       $("#"+id_etiquita_resultado).append("<option value=\'" + element.id + "\'>" + element.nombre + "</option>");
                                    });
                                   $("#"+id_etiquita_resultado).trigger("chosen:updated");
                                }

                            } catch (error) {

                            }

                        },
                        error: function(data) {
                            //swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                        }
                    });
                }
            });


    }
</script>