<script type="text/javascript">

document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'dayGrid', 'timeGrid', 'list' ] ,
            header: { center: 'dayGridMonth,timeGridWeek,dayGridDay' },
            // views: {
            //     dayGrid: {
            //     // options apply to dayGridMonth, dayGridWeek, and dayGridDay views
            //     },
            //     timeGrid: {
            //     // options apply to timeGridWeek and timeGridDay views
            //     },
            //     week: {
            //     // options apply to dayGridWeek and timeGridWeek views
            //     },
            //     day: {
            //     // options apply to dayGridDay and timeGridDay views
            //     }
            // },
            height: 600,
            eventLimit: true,
            editable: true,
            events:'{{URL::to('')}}/coordinacioncronograma/getActividadesAgendaTuristica',
            eventRender: function(info) {
                $(info.el).tooltip({ 
                    title: info.event.extendedProps.description,
                    placement: "top",
                    trigger: "hover",
                    container: "body"
                });

            },
            eventClick: function(info) {
                console.log(info.event);
                info.el.style.borderColor = 'red';
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                       
                    },
                    buttonsStyling: true
                })
   
                swalWithBootstrapButtons.fire({

                html:'<iframe id="myIframe" src="{{URL::to('')}}/coordinacioncronograma/agendaturistica/'+info.event.id+'" style="width: 100%;  height: 50em !important"  frameborder=0></iframe><div>'+info.event.extendedProps.acciones+'</div>',

                showCancelButton: false,
                showConfirmButton: false,
                customClass:'swal-height-show-agenda'
                });
            },

            
        });
        calendar.setOption('locale', 'es');
        try {
        calendar.render();
            
        } catch (error) {
            
        }
});

function llenarcalendario(){

    var length =$('#length').val();
    var campos =$('#campos').val();
    var estado_aprobacion =$('#estado_aprobacion').val();
    var fecha_inicio =$('#fecha_inicio').val();
    var fecha_fin =$('#fecha_fin').val();

    document.getElementById('calendar').innerHTML='';
    var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: [ 'dayGrid', 'timeGrid', 'list' ] ,
            header: { center: 'dayGridMonth,timeGridWeek,dayGridDay' },
            views: {
                dayGrid: {
                // options apply to dayGridMonth, dayGridWeek, and dayGridDay views
                },
                timeGrid: {
                // options apply to timeGridWeek and timeGridDay views
                },
                week: {
                // options apply to dayGridWeek and timeGridWeek views
                },
                day: {
                // options apply to dayGridDay and timeGridDay views
                }
            },
            height: 600,
            eventLimit: true,
            editable: true,
            events:'{{URL::to('')}}/coordinacioncronograma/getActividadesAgendaTuristica?length='+length+'&campos='+campos+'&estado_aprobacion='+estado_aprobacion+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin,
            eventRender: function(info) {
                $(info.el).tooltip({ 
                    title: info.event.extendedProps.description,
                    placement: "top",
                    trigger: "hover",
                    container: "body"
                });

            },
            eventClick: function(info) {
                console.log(info.event.extendedProps.acciones);
                info.el.style.borderColor = 'red';
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                       
                    },
                    buttonsStyling: true
                })
   
                swalWithBootstrapButtons.fire({

                html:'<iframe id="myIframe" src="{{URL::to('')}}/coordinacioncronograma/agendaturistica/'+info.event.id+'" style="width: 100%;  height: 50em !important"  frameborder=0></iframe><div>'+info.event.extendedProps.acciones+'</div>',

                showCancelButton: false,
                showConfirmButton: false,
                customClass:'swal-height-show-agenda'
                });
            },
        });
        calendar.setOption('locale', 'es');
        try {
        calendar.render();
            
        } catch (error) {
            
        }
}


function atender(id,tipo,responsable,fecha_inicio="",fecha_fin=""){
     
     const swalWithBootstrapButtons = Swal.mixin({
         customClass: {
             confirmButton: 'btn btn-success',
             cancelButton: 'btn btn-danger',
            //  container: 'sweet_containerImportant',
             popup:'sweet_popupImportant'
             
         },
         buttonsStyling: false
     })

     
     if(tipo==1){
         swalWithBootstrapButtons.fire({
         title: '¿Seguro de Continuar?',
         text: "Ingrese observación.",
         input: 'textarea',
         inputValue: responsable,
         inputAttributes: {
         autocapitalize: 'off'
         },
         showCancelButton: true,
         confirmButtonText: 'Si, continuar!',
         cancelButtonText: 'No, cancelar',
         showLoaderOnConfirm: true,
         
         preConfirm: (obs) => {
             $.ajax({
                 type: "POST",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "id": id,
                     "tipo": tipo,
                     "obs": obs,
         
                 },
                 url: '{{URL::to('')}}/coordinacioncronograma/atender',
                 success: function (data) {
                     try {

                         if (data['estado'] == 'ok') {
                             Swal.fire({
                                 position: 'center',
                                 type: 'success',
                                 title: data['msg'],
                                 showConfirmButton: false,
                                 timer: 1500
                             }).then(() => {
                                 // location.reload();
                                 tabla.draw();
                                 llenarcalendario();

                             });
                         } else {
                             Swal.fire({
                                 type: 'error',
                                 title: 'Uups...',
                                 text: data['msg']
                             });
                         }

                     } catch (error) {

                     }

                 },
                 error: function (data) {
                     swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                 }
             });

         },
         allowOutsideClick: () => !Swal.isLoading()
         });
     }else if(tipo==3){
         swalWithBootstrapButtons.fire({
             title: '¿Seguro de Continuar?',
             text: "Ingrese el motivo  por la cual se suspende la actividad",
             //type: 'warning',
             input: 'textarea',
             inputAttributes: {
             autocapitalize: 'off'
             },
             showCancelButton: true,
             confirmButtonText: 'Si, continuar!',
             cancelButtonText: 'No, cancelar',
             showLoaderOnConfirm: true,
             //customClass:'swal-height-show-agenda_2',
             preConfirm: (obs) => {
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id,
                         "tipo": tipo,
                         "obs": obs,
                     },
                     url: '{{URL::to('')}}/coordinacioncronograma/atender',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     // location.reload();
                                     tabla.draw();
                                     llenarcalendario();
                                     //  document.getElementById(""+responsable).remove;

                                     $('#'+responsable).remove();

                                 });
                             } else {
                                 Swal.fire({
                                     type: 'error',
                                     title: 'Uups...',
                                     text: data['msg']
                                 });
                             }

                         } catch (error) {

                         }

                     },
                     error: function (data) {
                         swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
     }else if(tipo==2){
         swalWithBootstrapButtons.fire({
             title: '¿Seguro de Continuar?',
             text: "Ingrese observación por la cual se rechaza la actividad",
             //type: 'warning',
             input: 'textarea',
             inputAttributes: {
             autocapitalize: 'off'
             },
             showCancelButton: true,
             confirmButtonText: 'Si, continuar!',
             cancelButtonText: 'No, cancelar',
             showLoaderOnConfirm: true,
             //customClass:'swal-height-show-agenda_2',
             preConfirm: (obs) => {
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id,
                         "tipo": tipo,
                         "obs": obs,
                     },
                     url: '{{URL::to('')}}/coordinacioncronograma/atender',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     // location.reload();

                                     tabla.draw();
                                     llenarcalendario();

                                 });
                             } else {
                                 Swal.fire({
                                     type: 'error',
                                     title: 'Uups...',
                                     text: data['msg']
                                 });
                             }

                         } catch (error) {

                         }

                     },
                     error: function (data) {
                         swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
     }else if(tipo==4){
         swalWithBootstrapButtons.fire({
             title: '¿Seguro de Continuar?',
             text: "Ingrese observación por la cual se reprograma",
             //type: 'warning',
             html:
             '<span>Ingrese observación por la cual se reprograma</span><br>'+
             '<div class="input-group datetime"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>'+
             '<input data-placement="top" data-toogle="tooltip" class="form-control datefecha" placeholder="Inicio" value="'+fecha_inicio+'" name="fecha_inicio" type="text" id="fecha_inicio_r"></div>' +
             '<div class="input-group datetime"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>'+
             '<input data-placement="top" data-toogle="tooltip" class="form-control datefecha" placeholder="Fin" value="'+fecha_fin+'" name="fecha_fin" type="text" id="fecha_fin_r"></div>' +
             '<input id="obs" class="swal2-textarea">',            
             showCancelButton: true,
             confirmButtonText: 'Si, continuar!',
             cancelButtonText: 'No, cancelar',
             showLoaderOnConfirm: true,
             //customClass:'swal-height-show-agenda_2',
             onOpen: function() {
                 $('.input-group.datetime').datetimepicker({
                             format: 'YYYY-MM-DD HH:mm',
                             locale: 'es',
                             sideBySide: true,
                             widgetPositioning: {
                                 horizontal: 'right',
                                 vertical: 'top'
                             }
                         }).on('keypress paste', function (e) {
                     e.preventDefault();
                     return false;
                 });;

                 

             },
             preConfirm: (obs) => {
                 var i=document.getElementById('fecha_inicio_r').value;
                 var f=document.getElementById('fecha_fin_r').value;
                 var obs=document.getElementById('obs').value
                 // console.log(i,f,obs);
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id,
                         "tipo": tipo,
                         "obs": obs,
                         "fecha_inicio": i,
                         "fecha_fin": f,
                     },
                     url: '{{URL::to('')}}/coordinacioncronograma/atender',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     // location.reload();

                                     tabla.draw();
                                     llenarcalendario();

                                 });
                             } else {
                                 Swal.fire({
                                     title: data['error'],
                                     width:1200,
                                     html: data['actividades'],
                                     onOpen: function() {
                                         $('.input-group.datetime').datetimepicker({
                                                     format: 'YYYY-MM-DD HH:mm',
                                                     locale: 'es',
                                                     sideBySide: true,
                                                    
                                                     // widgetPositioning: {
                                                     //     horizontal: 'right',
                                                     //     vertical: 'top'
                                                     // }
                                                    
                                                 }).on('keypress paste', function (e) {
                                             e.preventDefault();
                                             return false;
                                         });

                                         $(".chosen-select").chosen(
                                         {
                                             no_results_text: "No existe coincidencia con lo que busca...",
                                             placeholder_text_single: "Seleccione...",
                                             placeholder_text_multiple: "Seleccione...",
                                             width: "200px"
                                         });

                                       
                                         
                                       
                                         var ruta_indicadores= '{{URL::to('getDelegadosAlcalde')}}';
                                         $.get(ruta_indicadores, function(data)  {
                                             datos=data;
                                             var select=document.getElementsByName("delegados");    
                                             select.forEach(element =>  {
                                             var elemento=element.id;   
                                             $('#'+elemento).empty();
                                           
                                           $.each(datos, function(key, data) {
                                                     $('#'+elemento).append("<option value=\'" + data.id + "\'>" + data.name+ "</option>");
                                                     $('#'+elemento).trigger("chosen:updated");
                                                 }); 
                                               
                                                 
                                             });
                                         
                                         });
                                        
                                     }



                                 });
                             }

                         } catch (error) {

                         }

                     },
                     error: function (data) {
                         swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
     }else if(tipo==5){
         swalWithBootstrapButtons.fire({
             title: '¿Seguro de Continuar?',
             showCancelButton: true,
             confirmButtonText: 'Si, continuar!',
             cancelButtonText: 'No, cancelar',
             showLoaderOnConfirm: true,
             closeOnConfirm: false,
             //customClass:'swal-height-show-agenda_2',
             preConfirm: (obs) => {
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id,
                         "tipo": tipo,
                     },
                     url: '{{URL::to('')}}/coordinacioncronograma/atender',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     // closeOnConfirm: false,
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     // location.reload();

                                     tabla.draw();
                                     llenarcalendario();

                                 });
                             } else {
                                 Swal.fire({
                                 //    type: 'error',
                                     title: data['error'],
                                     width:1200,
                                     html: data['actividades'],
                                     onOpen: function() {
                                         $('.input-group.datetime').datetimepicker({
                                                     format: 'YYYY-MM-DD HH:mm',
                                                     locale: 'es',
                                                     sideBySide: true,
                                                    
                                                     // widgetPositioning: {
                                                     //     horizontal: 'right',
                                                     //     vertical: 'top'
                                                     // }
                                                    
                                                 }).on('keypress paste', function (e) {
                                             e.preventDefault();
                                             return false;
                                         });

                                         $(".chosen-select").chosen(
                                         {
                                             no_results_text: "No existe coincidencia con lo que busca...",
                                             placeholder_text_single: "Seleccione...",
                                             placeholder_text_multiple: "Seleccione...",
                                             width: "200px"
                                         });

                                         
                                       
                                         var ruta_indicadores= '{{URL::to('getDelegadosAlcalde')}}';
                                         $.get(ruta_indicadores, function(data)  {
                                             datos=data;
                                             var select=document.getElementsByName("delegados");    
                                             select.forEach(element =>  {
                                             var elemento=element.id;   
                                             $('#'+elemento).empty();
                                           
                                           $.each(datos, function(key, data) {
                                                     $('#'+elemento).append("<option value=\'" + data.id + "\'>" + data.name+ "</option>");
                                                     $('#'+elemento).trigger("chosen:updated");
                                                 }); 
                                               
                                                 
                                             });
                                         
                                         });
                                        
                                     }
                                 }).then((result) => {
                                   
                                   
                                 });
                             }

                         } catch (error) {

                         }
                     },
                     error: function (data) {
                         swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
     }else if(tipo==6){
         swalWithBootstrapButtons.fire({
             title: '¿Seguro de Continuar?',
             text: "Ingrese observación",
             input: 'textarea',
             inputAttributes: {
             autocapitalize: 'off'
             },
             showCancelButton: true,
             confirmButtonText: 'Si, continuar!',
             cancelButtonText: 'No, cancelar',
             showLoaderOnConfirm: true,
             //customClass:'swal-height-show-agenda_2',
             preConfirm: (obs) => {
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id,
                         "tipo": tipo,
                         "obs": obs,
                     },
                     url: '{{URL::to('')}}/coordinacioncronograma/atender',
                     success: function (data) {
                         try {
                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     tabla.draw();
                                     llenarcalendario();
                                 });
                             } else {
                                 Swal.fire({
                                     type: 'error',
                                     title: 'Uups...',
                                     text: data['msg']
                                 });
                             }

                         } catch (error) {

                         }
                     },
                     error: function (data) {
                         swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
     }else if(tipo==7){
         swalWithBootstrapButtons.fire({
             title: '¿Seguro de Continuar?',
             text: "Ingrese observación por la cual se delega",
             //type: 'warning',
            //  height:800,
            //  width:800,
             html:
             '<span>Ingrese observación por la cual se delega</span><br>'+
             '<select class="chosen-select" id="delegados" multiple  name="delegados"></select>'
             +'<input id="obs" class="swal2-textarea">',            
             showCancelButton: true,
             confirmButtonText: 'Si, continuar!',
             cancelButtonText: 'No, cancelar',
             showLoaderOnConfirm: true,
             //customClass:'swal-height-show-agenda_2',
             onOpen: function() {
                 $(".chosen-select").chosen(
                 {
                     no_results_text: "No existe coincidencia con lo que busca...",
                     placeholder_text_single: "Seleccione...",
                     placeholder_text_multiple: "Seleccione...",
                     width: "100%"
                 });
                 
                 $("#delegados").empty();

                 var ruta_indicadores= '{{URL::to('getDelegadosAlcalde')}}';
                 $.get(ruta_indicadores, function(data) {
                     $.each(data, function(key, data) {
                         $("#delegados").append("<option value=\'" + data.id + "\'>" + data.name+ "</option>");
                         $("#delegados").trigger("chosen:updated");
                     });           
                 });


                 $('#delegados').on('change', function(evt, params) {
                                            var selectedValue = params.selected;
                                            var fecha_inicio='0';
                                            var fecha_fin='0';

                                            $.ajax({  
                                                type: "GET",
                                                url: "{{URL::to('')}}/verificarDisponibilidad/"+selectedValue+"/"+fecha_inicio+"/"+fecha_fin+"?tipo="+id,
                                                success: function (response) {
                                                    if(response.respuesta){
                                                        
                                                        $("#delegados option[value='" + selectedValue + "']").prop('selected', false);
                                                        $("#delegados").trigger("chosen:updated");
                                                        toastr['error'](response.mensaje);
                                                }
                                                
                                                }
                                            });
                                            
                                            console.log(selectedValue);
                });

             },
             preConfirm: (obs) => {
                 var foo = [];
                 $('#delegados :selected').each(function (i, selected) {
                     foo[i] = $(selected).val();
                 });
                 var obs=document.getElementById('obs').value
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id,
                         "tipo": tipo,
                         "obs": obs,
                         "delegacion_alcalde": JSON.stringify(foo),
                     },
                     url: '{{URL::to('')}}/coordinacioncronograma/atender',
                     success: function (data) {
                         try {
                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     tabla.draw();
                                     llenarcalendario();
                                 });
                             } else {
                                 Swal.fire({
                                     type: 'error',
                                     title: 'Uups...',
                                     text: data['msg']
                                 });
                             }

                         } catch (error) {

                         }
                     },
                     error: function (data) {
                         swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
     }else if(tipo==8){
         swalWithBootstrapButtons.fire({
             title: '¿Seguro de Continuar?',
             text: "Asisistir a actividad",
             showCancelButton: true,
             confirmButtonText: 'Si, continuar!',
             cancelButtonText: 'No, cancelar',
             showLoaderOnConfirm: true,
             //customClass:'swal-height-show-agenda_2',
             preConfirm: (obs) => {
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id,
                         "tipo": tipo
                     },
                     url: '{{URL::to('')}}/coordinacioncronograma/atender',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     // location.reload();

                                     tabla.draw();
                                     llenarcalendario();

                                 });
                             } else {
                                 Swal.fire({
                                     type: 'error',
                                     title: 'Uups...',
                                     text: data['msg']
                                 });
                             }

                         } catch (error) {

                         }

                     },
                     error: function (data) {
                         swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
     }else if(tipo==9){
         swalWithBootstrapButtons.fire({
             title: '¿Seguro de Continuar?',
             text: "Inregrarse a actividad",
             showCancelButton: true,
             confirmButtonText: 'Si, continuar!',
             cancelButtonText: 'No, cancelar',
             showLoaderOnConfirm: true,
             //customClass:'swal-height-show-agenda_2',
             preConfirm: (obs) => {
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id,
                         "tipo": tipo
                     },
                     url: '{{URL::to('')}}/coordinacioncronograma/atender',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 }).then(() => {
                                     // location.reload();

                                     tabla.draw();
                                     llenarcalendario();

                                 });
                             } else {
                                 Swal.fire({
                                     type: 'error',
                                     title: 'Uups...',
                                     text: data['msg']
                                 });
                             }

                         } catch (error) {

                         }

                     },
                     error: function (data) {
                         swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                     }
                 });

             },
             allowOutsideClick: () => !Swal.isLoading()
         });
     }

}



function atender_modal(id,tipo,responsable,fecha_inicio="",fecha_fin="",id_encriptado){

     var obs=document.getElementById('obs_'+id).value;
     if(tipo==1){
         $('#lista_'+id).attr('disabled',true);
         $.ajax({
                 type: "POST",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "id": id_encriptado,
                     "tipo": 11,
                     "obs": obs,
         
                 },
                 url: '{{URL::to('')}}/coordinacioncronograma/atender',
                 success: function (data) {
                     try {

                         if (data['estado'] == 'ok') {
                             toastr['success'](data['msg']);
                             $('#fila_'+id).remove();
                             tabla.draw();
                             llenarcalendario();
                             
                         } else {
                             toastr['error'](data['msg']);
                             $('#lista_'+id).attr('disabled',false);
                         }
                     } catch (error) {
                         $('#lista_'+id).attr('disabled',false);
                     }

                 },
                 error: function (data) {
                     toastr['error'](data['msg']);
                     $('#lista_'+id).attr('disabled',false);
                 }
             });

     }else if(tipo==2){
         $('#delegar_'+id).attr('disabled',true);
         var foo = [];
                 $('#delegados_'+id+' :selected').each(function (i, selected) {
                     foo[i] = $(selected).val();
                 });
                 var obs=document.getElementById('obs_'+id).value
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id_encriptado,
                         "tipo": 7,
                         "obs": obs,
                         "delegacion_alcalde": JSON.stringify(foo),
                     },
                     url: '{{URL::to('')}}/coordinacioncronograma/atender',
                     success: function (data) {
                         try {
                             if (data['estado'] == 'ok') {
                                 toastr['success'](data['msg']);
                                 $('#fila_'+id).remove();
                                 tabla.draw();
                                 llenarcalendario();
                             
                             } else {
                                 toastr['error'](data['msg']);
                                 $('#delegar_'+id).attr('disabled',false);
                             }
                         } catch (error) {
                             $('#delegar_'+id).attr('disabled',false);
                             
                         }

                     },
                     error: function (data) {
                         toastr['error'](data['msg']);
                     }
                 });

         
     }else if(tipo==3){
         $('#aprobar_'+id).attr('disabled',true);
         $.ajax({
                 type: "POST",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "id": id_encriptado,
                     "tipo": 5,
                     "obs": obs,
         
                 },
                 url: '{{URL::to('')}}/coordinacioncronograma/atender',
                 success: function (data) {
                     try {

                         if (data['estado'] == 'ok') {
                             toastr['success'](data['msg']);
                             $('#fila_'+id).remove();
                             tabla.draw();
                             llenarcalendario();
                             
                         } else {
                             toastr['error'](data['error']);
                             $('#aprobar_'+id).attr('disabled',false);
                         }
                     } catch (error) {
                         $('#aprobar_'+id).attr('disabled',false);
                     }

                 },
                 error: function (data) {
                     toastr['error'](data['error']);
                     $('#aprobar_'+id).attr('disabled',false);
                 }
             });
     }else if(tipo==4){
                 $('#reprogramar_'+id).attr('disabled',true);
                 var i=document.getElementById('fecha_inicio_'+id).value;
                 var f=document.getElementById('fecha_fin'+id).value;
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id_encriptado,
                         "tipo": 4,
                         "obs": obs,
                         "fecha_inicio": i,
                         "fecha_fin": f,
                     },
                     url: '{{URL::to('')}}/coordinacioncronograma/atender',
                     success: function (data) {
                         try {
                             if (data['estado'] == 'ok') {
                                 toastr['success'](data['msg']);
                                 $('#fila_'+id).remove();
                                 tabla.draw();
                                 llenarcalendario();
                             
                             } else {
                                 toastr['error'](data['error']);
                                 $('#reprogramar_'+id).attr('disabled',false);
                             }
                         } catch (error) {
                         $('#reprogramar_'+id).attr('disabled',false);
                         }
                     },
                     error: function (data) {
                         $('#reprogramar_'+id).attr('disabled',false);
                     }
                 });
     }
}


$(document).ready(function () {
    
    
    $("#div-id_categoria").removeClass('col-lg-8');
    $("#div-id_categoria").addClass('col-lg-6');

    $("#agregar_categoria").click(function (e) { 
    
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger',
            //  container: 'sweet_containerImportant',
            popup:'sweet_popupImportant'
            
        },
        buttonsStyling: false
    });

    swalWithBootstrapButtons.fire({
            title: 'Ingrese el nombre de la categoría',
            input: 'text',
            inputAttributes: {
            autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Agregar!',
            cancelButtonText: 'No, cancelar',
            showLoaderOnConfirm: true,
            //customClass:'swal-height-show-agenda_2',
            preConfirm: (obs) => {
                $.ajax({
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "categoria": obs,
                    },
                    url: '{{URL::to('')}}/agregar_categoria',
                    success: function (data) {
                        try {

                            if (data['estado'] == 'ok') {
                                Swal.fire({
                                    position: 'center',
                                    type: 'success',
                                    title: data['msg'],
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                       $("#id_categoria").empty();
                                      $.each(data['datos'], function(key, element) {
                                        $("#id_categoria").append("<option value=\'" + element.id + "\'>" + element.categoria+"</option>");
                                      });
                                      $("#id_categoria").trigger("chosen:updated");
                                      $("#id_categoria").val(data['seleccionado']);
                                      $("#id_categoria").trigger("chosen:updated");
                                    

                            } else {
                                Swal.fire({
                                    type: 'error',
                                    title: data['msg']
                                    // text: data['msg']
                                });
                                $("#id_categoria").empty();
                                      $.each(data['datos'], function(key, element) {
                                        $("#id_categoria").append("<option value=\'" + element.id + "\'>" + element.categoria+"</option>");
                                      });
                                      $("#id_categoria").trigger("chosen:updated");
                            }

                        } catch (error) {

                        }

                    },
                    error: function (data) {
                        //swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                    }
                });
            }
      });

    });
});

</script>