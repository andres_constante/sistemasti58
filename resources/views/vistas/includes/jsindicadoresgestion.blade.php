<script type="text/javascript">
    $(document).ready(function() {
        $("#IDDIRECCION").change(function(e) {
            getIndicadoresGestionBydireccion(this.value);
        });


        $("#IDPARROQUIA").change(function (e) { 
            var parroquia=$(this).val();
            $.ajax({
                    type: "GET",
                    url: "{{ URL::to('') }}/coordinacioncronograma/getBarrios/"+parroquia,
                    success: function (response) {
                        $("#IDBARRIO").empty();
                        $("#IDBARRIO").append("<option value='0'>TODOS</option>");
                        $.each(response, function(key, element) {
                        $("#IDBARRIO").append("<option value=\'" + element.id + "\'>" + element.barrio+"</option>");
                        });
                        // $('#IDBARRIO').val(165).change();
                        $("#IDBARRIO").trigger("chosen:updated");  
                    }
                });
            
        });



    });

    function getIndicadoresGestionBydireccion(id) {

       
        var ruta = "{{URL::to('coordinacioncronograma/getIndicadoresGestion')}}?id=" + id + "&t=5";
        $.get(ruta, function(data) {
            $("#IDINDICADOR").empty();
            $("#tabla_indicadores tbody").empty();
            $.each(data, function(key, element) {
                $("#IDINDICADOR").append("<option value=\'" + element.id + "\'>" + element.nombre + "</option>");
            });
            $("#IDINDICADOR").trigger("chosen:updated");
        });
      

    }
</script>


