<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
<style>
.circulo {
     width: 100px;
     height: 100px;
     -moz-border-radius: 50%;
     -webkit-border-radius: 50%;
     border-radius: 50%;
     background: white;
     padding: 1%;
     -moz-box-shadow: 0px 6px 5px #2c438f96;
  -webkit-box-shadow: 0px 6px 5px #2c438f96;
  box-shadow: 0px 6px 5px #2c438f96;
     
     
     
}
.zoom{
        /* Aumentamos la anchura y altura durante 2 segundos */
        transition: width 2s, height 2s, transform 2s;
        -moz-transition: width 2s, height 2s, -moz-transform 2s;
        -webkit-transition: width 2s, height 2s, -webkit-transform 2s;
        -o-transition: width 2s, height 2s,-o-transform 2s;
    }
    .zoom:hover{
        /* tranformamos el elemento al pasar el mouse por encima al doble de
           su tamaño con scale(2). */
        transform : scale(1.3);
        -moz-transform : scale(1.3);      /* Firefox */
        -webkit-transform : scale(1.3);   /* Chrome - Safari */
        -o-transform : scale(1.3);        /* Opera */
    }
.swal-wide{
    width:auto !important;
}
</style>
@section ('scripts')
    <script type="text/javascript">
        @foreach($res as $key=>$value)
            var grafico_{{$value['id']}};
        @endforeach

        function mostrar(id){
            // console.log($('#grafico-'+id).show());
            // $(".graficos").hide();
            // $('#grafico-'+id).removeClass('col-lg-6');
            // $('#grafico-'+id).addClass('col-lg-12');
            // $('#grafico-'+id).show();
            var html_div = document.getElementById('grafico-'+id).innerHTML;
            // console.log(html_div);
            Swal.fire({
                // title: 'DETALLE',
                html: html_div,
                showCloseButton: false,
                
                showConfirmButon: true,
                showCancelButton: true,
                confirmButtonText:'<i class="fa fa-print" aria-hidden="true"></i>  Imrprimir',
                cancelButtonText:'<i class="fa fa-times-circle-o" aria-hidden="true"></i>  Cerrar',
                customClass: 'swal-wide'
            }).then((result) => {
                if (result.value) {
                    $('#grafico-'+id).show();
                    var printWindow = window.open("", "Imprimir" );
                    $("link, style").each(function() {
                        $(printWindow.document.head).append($('#grafico-'+id).clone())
                    });
                    var toInsert = $('#grafico-'+id).clone();
                    console.log(toInsert);
                    var divheader= $("#divheader").html();
                    toInsert= toInsert.html(divheader+toInsert.html());
                    $(printWindow.document.body).append(toInsert);
                    setTimeout(function(){
                        printWindow.print();
                        $('#grafico-'+id).hide();
                    },1000);
                }
            });


        }
        $(document).ready(function() {
            @foreach($res as $key=>$value)
            grafico_{{$value['id']}}= Highcharts.chart('container-{!! $value["id"] !!}', {

                chart: {
                    type: '{!! $value["tipografico"] !!}',
                    width: 800,
                    // height:600
                },
                // chartWidth = 1000,
                // width:1000000,

                title: {
                    text: ''//{! $value["detalle"] !!}
                },
                subtitle: {
                    text: '<br> <br>Fuente {!! $value["fuente"] !!}',
                    verticalAlign: 'bottom',
                    // y: -1
                },
                xAxis: {
                    categories: [{!! $value["parroquias"] !!}],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Totales',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify',
                        // x: 0,
                        // y: -8
                    },
                    
                },
                tooltip: {
                    valueSuffix: ' {!! $value["medida"] !!}'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    },
                    series: {
                        colorByPoint: {!! $value["color"] !!},
                        dataLabels: {
                            enabled: true // Mostrar Valores Arriba COlumn
                            //,rotation: -45,
                        }
                    }
                },
                legend: {
                    enabled: true,
                    layout: 'vertical',
                    align: 'right',
                    // verticalAlign: 'top',
                     verticalAlign: 'middle',
                    x: 150,
                    y: -300,
                    itemStyle: {
                        fontSize:'8px',
                        font: '8pt Trebuchet MS, Verdana, sans-serif',
                        color: '#A0A0A0'
                    },
                    floating: true,
                    borderWidth: 1,
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: [{!! $value["valores"]  !!}],
                responsive: {
                    rules: [{
                        condition: {
                            // maxWidth: 800
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: "Totales"
                                }
                            },
                            subtitle: {
                                text: 'Fuente {!! $value["fuente"] !!}'
                            },
                            credits: {
                                enabled: false
                            }
                        }
                    }]
                }
            });
            @endforeach
        });
            function imprimirdiv(obj){
                $(".graficos").show();
                var printWindow = window.open("", "Imprimir" );
                $("link, style").each(function() {
                    $(printWindow.document.head).append($(this).clone())
                });
                //$(printWindow.document.head).append(estilo);
                var toInsert = $(obj).clone();//.html();
//        toInsert= toInsert.find('#divpagoonline').remove().end();
                //var tituloreporte= $("#tituloreporte").val();
                //$("#tdtitulo").text(tituloreporte);
                var divheader= $("#divheader").html();
                toInsert= toInsert.html(divheader+toInsert.html());
                $(printWindow.document.body).append(toInsert);
                setTimeout(function(){
                    printWindow.print();
                    //printWindow.close();
                    $(".graficos").hide();
                },1000);
                //printWindow.print();
                //printWindow.close();
            }
    </script>
@stop
@section ('contenido')
    <!-- Cabecera de Impresión-->
    <div id="divheader" style="display:none;">
        <center>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img style="width: 200px; height: auto;" src="{{ asset("img/mantafirmes.png") }}" />
                    </td>
                    <td style="text-align: center;">
                        <h2>GOBIERNO AUTÓNOMO DESCENTRALIZADO MUNICIPAL DEL CANTÓN MANTA</h2>
                        <h3>COORDINACIÓN GENERAL DE PLANIFICACIÓN PARA EL DESARROLLO</h3>
                    </td>
                    <td></td>
                </tr>
                <tr><td colspan="3"><img align="center" alt="Image" border="0" style="height: 5px;" src="{{ asset("img/lineafirmes.png")}}"  title="Image" width="100%"/></td></tr>
                <!--
                <tr>
                    <td colspan="3"><div id="tdtitulo"></div></td>
                </tr>
                -->
            </table>
        </center>
    </div>
    <!-- Fin Cabecera-->
    <div class="pull-right">
        <input id="btnImprime" type="button" class="btn btn-danger" value="Imprimir" onclick="return imprimirdiv('#restable')">
    </div>
    <?php
        $bolitas=$res;

        // dd($bolitas);
    ?>
    <h1 style="background-color: #FFFFFF; padding: 1% 0 1% 2%;"> {{ $configuraciongeneral[0] }}</h1>
    <div class=" col-md-12 col-lg-12" style="background-color: #e7eaec">
        
    
    @foreach($bolitas as $bolita)
        <div class="col-md-6  zoom col-lg-6" style="text-align: center; padding: 1%;">
        <img class="circulo " src="{{$bolita['icono_app']}}" width="35%" onclick="mostrar('{{$bolita['id']}}')"  height="30%">
        <?php 
            $titulo=str_replace('MANTA','',$bolita['detalle']);
            $titulo=str_replace('-','',$titulo);
         ?>
        <div style="padding-top: 10px;"><p style="font-size: xx-small; ">{{ $titulo}}</p></div>

          
        </div>
    @endforeach
</div>
<div class="page-break"></div>
    <div id="restable">
        @php $k=0;@endphp
    @foreach($res as $key=>$value)
            @php $k++;$j=$k%2; @endphp
        @if($j==1)
    <div class="row">
        @endif
        <div class="col-lg-6 graficos" style="display:none"  id="grafico-{{$value['id']}}">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3>{!! $value["detalle"] !!}</h3>
                    <p style="text-align: left; margin: 0 0 -4px;">TOTAL: <span class="badge" style="font-size: 1em;">{!! ($value["sumatotal"]==0 ? '%' :  $value["sumatotal"])!!} {!! $value["medida"] !!}</span></p>
                </div>
                <div class="ibox-content">
                    <div id="container-{!! $value["id"] !!}">

                    </div>
                </div>
            </div>
            
            </div>
        <div class="page-break"></div>
        @if($j==0)
            </div>
            @if($k==1)
                <div class="page-break"></div>
                @endif
         @endif

    @endforeach
    </div>
@stop