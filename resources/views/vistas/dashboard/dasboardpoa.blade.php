<?php
if(!isset($nobloqueo) && Auth::user()->id_perfil!=4)
    Autorizar(Request::path());
?>

@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
@include('vistas.includes.jsDashboard')


</script>
@stop
@section('estilos')
 <style>
        .chart {
          width: 100%; 
          min-height: 450px;
        }
        body.DTTT_Print {
            background: #fff;
        }
        .DTTT_Print #page-wrapper {
            margin: 0;
            background: #fff;
        }
        button.DTTT_button, div.DTTT_button, a.DTTT_button {
            border: 1px solid #e7eaec;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }
        button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
            border: 1px solid #d2d2d2;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }

        .dataTables_filter label {
            margin-right: 5px;

        }

         td{
        color: #000 !important;
        font-size: 0.88em;
    }

    th{
        font-weight: bold !important;
        color: #072e6b !important;
        text-align: center !important;
        text-transform: uppercase !important;
        font-size: 0.88em;

    }

    .scroll {
     width:100%;
     height:1000px;
     overflow:auto;
    }

    .dataTables_info {
        font-size: 0.8em;
    }

    .pagination {
        font-size: 12
    }

    ul li {
        padding-left: 0%;
    }
    </style>

<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')

{{ HTML::script('js/exporting.js')}}
    
<div style="height:auto;width:100%;overflow:auto; padding: 0%; margin: 0%">

        <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h1>{{$configuraciongeneral[0]}}</h1>
            <div>
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0%; height: 100%">
            <div id="data_busqueda" >
                    <!-- <a target="_blank" href="documentos_view_incidencias/2000-01-01/2024-01-01/0"><i class="fa fa-download" style="padding-left: 3%; color:#0d47a1 "> Descargar Informe</i></a> -->
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="box box-success" style="height: 80%">
                        <div class="box-header with-border" style="height: 100%; padding: 0%; padding-bottom: 4%">
                            <div class="col-lg-12" style="padding-top: 0%">
                                <p style=" padding-bottom: 0%; padding-top: 3%; font-weight: bold; color: #000; font-size: 1.2em;">TOTAL</p>
                                <h1 style="font-weight: bold; padding-top: 0%; font-size: 2.5em; text-align: center; padding: 1%" id="total_poa">0</h1>
                            </div>
                        </div>
                    </div>
                </div>
            @isset($filtros)
                @foreach($filtros as $k => $value)
                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group" style="padding: 0%">
                        <p style="padding: 0%; font-weight: bold;">{{$value->Descripcion}}:</p>
                        @if($value->Tipo=='fecha')
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input  id="{{$value->Nombre}}" type="text" class="form-control" value="{{($value->Valor!='Null')?$value->Valor:''}}">
                            </div>
                        @elseif($value->Tipo=='select')
                            <div class="input-group">
                                <select  class="js-example-basic-single form-control" name="{{$value->Nombre}}" id="{{$value->Nombre}}">
                                    <option value="0">TODOS</option>
                                @foreach ($value->Valor as $ks=> $d)
                                    <option value="{{$ks}}">{{$d}}</option>
                                @endforeach
                                </select>
                            </div>
                        @else
                        <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input  id="{{$value->Nombre}}" type="text" class="form-control" value="{{($value->Valor!='Null')?$value->Valor:''}}">
                            </div>
                        @endif
                        
                    </div>
                </div>
                @endforeach                 
            @endisset
         

        
            </div> 
        </div>
           
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0%; height: 100%">
             

                @isset($estados)
                    @foreach($estados as $ksestados => $value_estados)
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="small-box" style="  background-color: #2c438f;   color:#ffffff;">
                            <div class="inner" >
                                <p style="font-weight: bold; font-size: 1.2em;">{{str_replace('_',' ',$value_estados)}}</p>
                                <h3 id="{{$value_estados}}" style="padding-left: 2%;">0</h3>
                            </div>
                        </div>
                    </div>
                    @endforeach                 
                @endisset
                
            </div>
            
        </div>
   

    <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-success" style="height: 100%">
                <div class="box-header with-border" style="height: 100%;">
                    <div class="col-lg-12" style="padding-top: 0%">
                        <div class="col-lg-12 col-md-12 col-sm-7 col-xs-12">
                            <div id="linechart_material_barras" class="chart"><div style="text-align: center; padding: 0%"><img src="{{asset('img/loading35.gif')}}"></div></div>
                        </div>
                        <!-- <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div id="linechart_material_two" ><div style="text-align: center; padding: 0%"><img src="{{asset('img/loading35.gif')}}"></div></div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary" style="height: 100%">
                <div class="box-header with-border" style="height: 100%;">
                    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 0%">
                            <div id="linechart_material_barras_entidades" class="chart"><div style="text-align: center; padding: 0%"><img src="{{asset('img/loading35.gif')}}"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--     
    <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-info" style="height: 100%">
                <div class="box-header with-border" style="height: 100%;">
                    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 0%">
                            <div id="linechart_material_direcciones_estados" class="chart"><div style="text-align: center; padding: 0%"><img src="{{asset('img/loading35.gif')}}"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="margin-left: 10px; margin-right: 10px; padding: 0%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-info" style="height: 100%">
                <div class="box-header with-border" style="height: 100%;">
                    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 0%">
                            <div id="linechart_material_resulestas_semana" class="chart"><div style="text-align: center; padding: 0%"><img src="{{asset('img/loading35.gif')}}"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0%" > 
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right ui-sortable-handle">
                  <li class="active"><a href="#lineas-chart_four_dr" data-toggle="tab" aria-expanded="true" style="color: #000; font-size: 0.8em">USUARIOS</a></li>
                  <li class=""><a href="#curvas-chart_five_fr" data-toggle="tab" aria-expanded="false" style="color: #000; font-size: 0.8em">FUNCIONARIOS</a></li>
                </ul>
                <div class="tab-content no-padding">
                    <div class="chart tab-pane active" id="lineas-chart_four_dr" style="position: relative; height: relative">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                </div>
                                <div class="box-body" >
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linechart_material_div23" style="padding: 0%">
                                        <div id="linear_round" >hola<div style="text-align: center; padding: 0%"><img src="{{asset('img/loading35.gif')}}"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                </div>
                                <div class="box-body" >
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linechart_material_div23" style="padding: 0%">
                                        <div id="linechart_top_10_resueltas" ><div style="text-align: center; padding: 0%"><img src="{{asset('img/loading35.gif')}}"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="chart tab-pane " id="curvas-chart_five_fr" style="position: relative; height: relative" >
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                </div>
                                <div class="box-body">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linechart_material_div2" style="padding: 0%">
                                        <div id="linear_round2" ><div style="text-align: center; padding: 0%"><img src="{{asset('img/loading35.gif')}}"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                </div>
                                <div class="box-body">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linechart_material_div2" style="padding: 0%">
                                        <div id="linechart_top_10_resueltas_dos" ><div style="text-align: center; padding: 0%"><img src="{{asset('img/loading35.gif')}}"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right ui-sortable-handle">
                  <li class="active"><a href="#lineas-chart4" data-toggle="tab" aria-expanded="true" style="color: #000; font-size: 1em">Incidencias con mas reportes</a></li>
                </ul>
                <div class="tab-content no-padding">
                    <div class="chart tab-pane active" id="lineas-chart4" style="position: relative; height: relative">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                </div>
                                <div class="box-body" >
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linechart_material_div23" style="padding: 0%">
                                        <div id="linechart_top_15_55" ><div style="text-align: center; padding: 0%"><img src="{{asset('img/loading35.gif')}}"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  -->

</div>

    

  
<div id="divheader" style="display:none;">
        <center>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img style="width: 200px; height: auto;" src="{{ asset("img/mantafirmes.png") }}" />
                    </td>
                    <td style="text-align: center;">
                        <h2>GOBIERNO AUTÓNOMO DESCENTRALIZADO MUNICIPAL DEL CANTÓN MANTA</h2>
                        <h3>COORDINACIÓN GENERAL DE PLANIFICACIÓN PARA EL DESARROLLO</h3>
                    </td>
                    <td></td>
                </tr>
                <tr><td colspan="3"><img align="center" alt="Image" border="0" style="height: 5px;" src="{{ asset("img/lineafirmes.png")}}"  title="Image" width="100%"/></td></tr>
                <!--
                <tr>
                    <td colspan="3"><div id="tdtitulo"></div></td>
                </tr>
                -->
            </table>
        </center>
    </div>


@stop
