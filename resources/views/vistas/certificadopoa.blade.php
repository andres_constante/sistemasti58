<?php
if(!isset($nobloqueo)) 
    //Autorizar(Request::path());
?>
@extends ('layout_basic_no_head')
@section ('titulo') {!! $configuraciongeneral[0] !!} @stop
@section ('scripts')
    <style>
        *{
            margin: 0px;
            padding: 0px;
        }
        /*/
        .sin_bordes {
            width: 80%;
        }*/
        .bordes {
            border: 1px solid #ddd;
        }

        #imgbarra {
            position: relative;
            left: 30%;
        }
/*
        #barra p {
            position: relative;
            left: -10%;
            font-size: 12px;
        }*/
        #tb_segj {
            border-collapse: collapse;
            text-align: center;
            left: -10%;
        }
        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        @if($tabla->estado_solicitud!="APROBADA")
            body {
                @if($tabla->estado_solicitud=="NEGADA")
                    background-image: url({{ asset("img/negado.png") }});
                    background-repeat:no-repeat;
            background-position: center;
                @else
                    background-image: url({{ asset("img/borrador2.jpg") }});
                @endif
                /*background-repeat:no-repeat;*/
                background-size:cover;
            }
        @endif
    </style>
<script type="text/javascript">
    function imprimirdiv(obj){
        $(".graficos").show();
        var printWindow = window.open("", "Imprimir" );
        $("link, style").each(function() {
            $(printWindow.document.head).append($(this).clone())
        });
        //$(printWindow.document.head).append(estilo);
        var toInsert = $(obj).clone();//.html();
//        toInsert= toInsert.find('#divpagoonline').remove().end();
        //var tituloreporte= $("#tituloreporte").val();
        //$("#tdtitulo").text(tituloreporte);
        var divheader= $("#divheader").html();
        toInsert= toInsert.html(divheader+toInsert.html());
        $(printWindow.document.body).append(toInsert);
        setTimeout(function(){
            printWindow.print();
            //printWindow.close();
            $(".graficos").hide();
        },1000);
        //printWindow.print();
        //printWindow.close();
    }
</script>
@stop
@section ('contenido')
    @if(isset($pdf))
    <div class="pull-right">
        <input id="btnImprime" type="button" class="btn btn-danger" value="Imprimir" onclick="return imprimirdiv('#restable')">
    </div>
    @endif
<div class="container" id="restable">
    <div style="height: 25px;">&nbsp;</div>
    <p style="text-align: center;">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    @if(isset($pdf))
                    <img style="width: 200px; height: auto;" src="{{ asset("img/mantafirmes.png") }}" />
                    @else
                        <img style="width: 200px; height: 50px;" src="{{ asset("img/mantafirmes.png") }}" />
                    @endif

                </td>
                <td style="text-align: center;">
                    <strong>GOBIERNO AUTÓNOMO DESCENTRALIZADO MUNICIPAL DEL CANTÓN MANTA</strong>
                    <!-- <h3>COORDINACIÓN GENERAL DE PLANIFICACIÓN PARA EL DESARROLLO</h3>-->
                </td>
            </tr>
            @if(isset($pdf))
                <tr><td colspan="3"><img align="center" alt="Image" border="0" style="height: 5px;" src="{{ asset("img/lineafirmes.png")}}"  title="Image" width="100%"/></td></tr>
            @else
                <tr><td colspan="3"><img align="center" alt="Image" border="0" style="height: 5px;" src="{{ asset("img/lineafirmes.png")}}"  title="Image" width="750px"/></td></tr>
            @endif
            <!--
            <tr>
                <td colspan="3"><div id="tdtitulo"></div></td>
            </tr>
            -->
        </table>

        <h1 style="text-align: center;">CERTIFICACIÓN</h1>
    <h2 style="text-align: center;"><strong>No. {{ str_pad($tabla->id,5,"0",STR_PAD_LEFT)  }}</strong></h2>
    </p>
    <p style="text-align: justify;">
    <strong>FECHA: </strong>{{  str_replace('Miercoles','Miércoles',fechas(2,$tabla->fecha_aprueba)) }}<br>
        <table class="table table-bordered">
        <tr>
            <th>Dirección Requiriente: </th><td>{{ $tabla->direccion_adquiriente }}</td>
        </tr>
        <tr>
            <th>Proyecto POA: </th><td>{{ $tabla->proyecto }}</td>
        </tr>
        <tr>
            <th>Actividad POA: </th><td>{{ $tabla->actividad }}</td>
        </tr>
        <tr>
            <th>Objeto de la Contratación: </th><td>{{ $tabla->objeto_contrato }}</td>
        </tr>
        <tr>
            <th>Monto certificado: </th><td>${{ number_format($tabla->monto_solicita,2) }}</td>
        </tr>
    </table>
    </p>
    @if($tabla->estado_solicitud=="APROBADA")
        <div style="height: 50px;">&nbsp;</div>
        <p style="text-align: center;">
            <strong>APROBADO POR</strong><br>
            COORDINACIÓN GENERAL DE PLANIFICACIÓN PARA EL DESARROLLO
            
        </p>
        <div style="height: 50px;">&nbsp;</div>
        <p style="text-align: center;">
            <strong>SOLICITADO POR</strong><br>
            {{ $tabla->cargo_solicita }}
        </p>
        <!-- CODIGO DE BARRAS-->
        <br>
        <div class="text-center">
            <div width="100%">
                <center>
                    <table width="100%" style="margin-left: 50px;" >
                        <tr>
                            <td class="sin_bordes" width="60%;" valign="top">
                                <table id="tb_seg" width="98%">
                                    <tr class="bordes">
                                        <td style="font-size: 10px; text-align: center;" class="bordes" valign="top">Código Seguro de
                                            Verificación (CSV)
                                        </td>
                                    </tr>
                                    <tr class="bordes">
                                        <td class="bordes">
                                            <br>
                                                <?php
                                                //$codbar = DNS1D::getBarcodeHTML($tabla->codigobarras, "C39", 2, 33);
                                                //echo $codbar;
                                                echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($tabla->codigobarras, "C39", 2, 33) . '" alt="barcode" style="margin-left:20px;"  />';
                                                ?>
                                                <center><p>{{$tabla->codigobarras}}</p></center>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="40%;">
                                <?php
                                $datosqr = "DOCUMENTO: " . str_pad($tabla->id,5,"0",STR_PAD_LEFT) . "\n";
                                $datosqr .= "FECHA: " . $tabla->fecha_aprueba . "\n";
                                $datosqr .= "SOLICITADO POR: " . $tabla->solicitado_por . "\n";
                                $datosqr .= "CARGO: " . $tabla->cargo_solicita . "\n";
                                $datosqr .= "APROBADO POR: " . $tabla->aprobado_por . "\n";
                                $datosqr .= "CARGO: " . $tabla->cargo_aprobado . "\n";
                                $datosqr .= "FECHA CREACION: " . $tabla->created_at . "\n";
                                //URL::to('coordinacioncronograma/printautorizacionpoa') ."/". $idcab
                                $datosqr .= "VIZUALIZAR PDF.: https://docs.google.com/gview?embedded=true&url=" . url('/coordinacioncronograma/printautorizacionpoa/') . '/' . \Illuminate\Support\Facades\Crypt::encrypt($tabla->id) . "\n";

                                $qrdato = qrvcard($tabla->codigobarras, "CERTIFICACION POA", null, $datosqr);
                                //$codqr = '<img src="data:image/png;base64,' . base64_encode(QrCode::format('png')->margin(0)->merge('/public/images/logo_qr.png',.2)->size(130)->generate('http://'.$dire.'/validacionsolvenciaqr/'.$datos->codqr)).'">' ;
                                $codqr = '<img src="data:image/png;base64,' . base64_encode(QrCode::format('png')->margin(0)->merge('/public/images/logo_qr.png', .2)->size(154)->generate(trim($qrdato))) . '">';
                                echo $codqr;
                                ?>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
        @elseif($tabla->estado_solicitud=="NEGADA")
            <h3>* {{ $tabla->observacion }}</h3>
        @endif<!-- Esatdo-->
</div>


@stop
