<?php
//use Request;
?>
<!DOCTYPE html>
<html>

<head>

<meta charset='utf-8'/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>@yield('titulo')</title>
   
    {{ HTML::style('css/bootstrap.min.css') }}       
    {{ HTML::style('font-awesome/css/font-awesome.css') }}
    {{ HTML::style('css/animate.css') }}
    {{ HTML::style('css/style.css') }}
 
   <!-- Data Tables -->
    {{ HTML::style('css/plugins/dataTables/dataTables.bootstrap.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.responsive.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.tableTools.min.css') }}
       
<!-- Mensaje -->

{{ HTML::style('css/plugins/toastr/toastr.min.css') }}

    @yield('estilos')
<!-- Mainly scripts -->
{{ HTML::script('js/jquery-2.1.1.js') }} 
{{ HTML::script('js/bootstrap.min.js') }} 
{{ HTML::script('js/plugins/metisMenu/jquery.metisMenu.js') }} 
{{ HTML::script('js/plugins/slimscroll/jquery.slimscroll.min.js') }} 


<!-- Custom and plugin javascript -->
{{ HTML::script('js/inspinia.js') }} 
{{ HTML::script('js/plugins/pace/pace.min.js') }} 

 <!-- Data Tables -->
 {{ HTML::script('js/plugins/dataTables/jquery.dataTables.js') }}     
    {{ HTML::script("js/plugins/dataTables/dataTables.bootstrap.js") }} 
    {{ HTML::script("js/plugins/dataTables/dataTables.responsive.js") }} 
    {{ HTML::script("js/plugins/dataTables/dataTables.tableTools.min.js") }} 

 <!-- Mensaje -->
 {{ HTML::script('js/plugins/toastr/toastr.min.js') }}     
 
      <!-- CSS Notificacion -->
    {{ HTML::style('ventanas-modales/ventanas-modales.css') }}
    <!-- CSS Notificacion -->
    {{ HTML::script('ventanas-modales/ventanas-modales.js') }}  
    <!-- Graficos estadissticos -->
{{ HTML::style('css/plugins/morris/morris-0.4.3.min.css') }}
{{ HTML::script('js/plugins/morris/raphael.js') }}
{{-- {{ HTML::script('js/plugins/morris/raphael-2.1.0.min.js') }} --}}
{{ HTML::script('js/plugins/morris/morris.js') }}
<!-- Chosen -->
    {{ HTML::style('chosen/docsupport/prism.css') }}
    {{ HTML::style('chosen/chosen.css') }}
    
    {{ HTML::script('chosen/chosen.jquery.js') }}
    {{ HTML::script('chosen/docsupport/prism.js') }}
<!-- ColorBox -->
    {{ HTML::style('colorbox/colorbox.css') }}    
    
    {{ HTML::script('colorbox/jquery.colorbox.js') }}    
    <!-- dropzone -->
    {!! HTML::style('css/plugins/dropzone/dropzone.css'); !!}
    {!! Html::script('js/plugins/dropzone/dropzone.js') !!}
     <!--sweetalert-->
     {{ HTML::style('css/plugins/sweetalert/sweetalert.css')}}
      {{ HTML::script('js/plugins/sweetalert/ciudadano/sweetalert.min.js') }}
  
<style type="text/css">
    @media only screen and (max-width: 600px) {
        .fa-newspaper-o:before {
                font-size:  3em !important;
            }
        .fa-edit:before, .fa-pencil-square-o:before {
            font-size:  3em !important;
        }
        .vertical-timeline-block .vertical-timeline-content{
            width: 100% !important;
        }
}

#mapa {
            height: 500px;
            border-style: none;
            width: auto;
            border: none;
            text-align: center;
        }
</style>
<!-- DatePicker -->    
    {{ HTML::style('bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ HTML::script('bootstrap-datepicker/bootstrap-datepicker.js') }}   
    {{ HTML::script('bootstrap-datepicker/locales/bootstrap-datepicker.es.js') }}
<!-- DateTimePicker -->
    {{ HTML::style('css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}
    {{ HTML::script('js/plugins/datetimepicker/moment.min.js') }}
    {{ HTML::script('js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}
    {!! HTML::style('lightgallery/css/lightgallery.css'); !!}
    {!! Html::script('lightgallery/js/lightgallery.min.js') !!}

<!-- Selective -->
    {!! HTML::style('selective/selectize.bootstrap3.css'); !!}
    {!! Html::script('selective/selectize.js') !!}

    {!! HTML::style('css/plugins/blueimp/css/blueimp-gallery.min.css'); !!}
    {!! Html::script('js/plugins/blueimp/jquery.blueimp-gallery.min.js') !!}
    {!! Html::script('js/randomColor.js') !!}


    
    @yield('scripts')


    {{-- mapbox --}}
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />
  <link rel="stylesheet"
      href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css"
      type="text/css" />
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
  <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
  <script src="{{ asset('js/mapbox.js').'?v='.rand(1,1000)  }}"></script>
 <script>
 function cargaatender()
 {
	 
	    $.ajax({
			type: "GET",
			url: '{{ URL::to("getnotificatodos") }}',
			data: '',
			error: function(objeto, quepaso, otroobj){
				$("#txttodos").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
			},				
			success: function(datos){
				$("#txttodos").html(datos);
		  },
  			statusCode: {
			    404: function() {
				$("#txttodos").html("<div class='alert alert-danger'>No existe URL</div>");
		    }	
			}	  
    }); 	 
 }
 function NotificacionTotal()
 {
	    $.ajax({
			type: "GET",
			url: '{{ URL::to("getnotificacuenta") }}',
			data: '',
			error: function(objeto, quepaso, otroobj){
				$("#txtcontar").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
			},				
			success: function(datos){
				$("#txtcontar").html(datos);
		  },
  			statusCode: {
			    404: function() {
				$("#txtcontar").html("<div class='alert alert-danger'>No existe URL</div>");
		    }
            
            
			}	  
    }); 
 	 //setTimeout(NotificacionTotal, 5000);
 }
 //setTimeout(NotificacionTotal, 1000);
 

 </script>
</head>
<body class="top-navigation">
<div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
       
    <!-- Cuerpo -->    
    <div class="row wrapper border-bottom white-bg page-heading">
                    @yield('contenido')
            </div>
            
        <div class="footer">
            <div class="pull-right">
                <!--10GB of <strong>250GB</strong> Free. -->
                {{ isset(Auth::user()->name) ? Auth::user()->name : '' }}                
            </div>
            <div>               
                {!! trans('html.main.copyright') !!}
            </div>
        </div>

        </div>
        </div>

</body>

</html>
