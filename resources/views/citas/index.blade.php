<?php
if(!isset($nobloqueo))
   Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{$title}} @stop
@section ('scripts')
<script>
    var fecha = new Date();
    $(document).ready(function () {

            var fecha = new Date();
            $('#fecha_paramt').datetimepicker({
                minDate: fecha.getFullYear()+'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
                maxDate: (fecha.getFullYear() + 1 )+'-'+fecha.getMonth()+'-'+fecha.getDate(),
            });


            $('#example tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input style="width:100%" type="text" placeholder="'+title+'" />' );
            });

            var table = $('#example').DataTable({
                responsive: true,
                "pagingType": "full_numbers",
                lengthMenu: [[20, 30, 50, 100, -1], [20, 30, 50, 100, "All"]],
                order: ([0, 'desc']),
                dom: 'flrBtip',
                buttons: ['print', 'copy', 'excel', 'pdf'],
                buttons: [
                     'csv', 
                     'excel',
                      'pdf', 
                      'print'
                ],
                language: {
                    "emptyTable": "No hay datos disponibles en la tabla",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtered from _MAX_ total entries)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontraron registros coincidentes",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Atrás"
                    },
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
            });

            table.columns().every( function () {
                var that = this;
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            });

        });


        
    

        function otroestado(estado){
            if(estado == 1){ //Atender
                $('#tablemaster').attr("hidden", true);
                $('#loading').attr("hidden", false);
            }
            if(agendar == 1){ //Atender

            }
            if(estado ==3){//Rechazar
                $('#tablemaster').attr("hidden", true);
                $('#tablerechazar').attr("hidden", false);
            }
        }

        var id_seleccion = null;
        var id_estado = null;
        var e_mail = null;
        function metodos(estado, id_modo, nombre, email){
            id_seleccion = id_modo;
            id_estado = estado;
            e_mail = email;
           
            if(estado == 1){ //Atender
                $("#nombre_soli").html('ATENDER CITA DE: '+nombre); 
                $('#atender').attr("hidden", false);
                $('#rechazo').attr("hidden", true);
                $('#loading2').attr("hidden", true);
                $('#loading').attr("hidden", true);
                $('#presento').attr("hidden", true);
                $('#tablemaster').attr("hidden", true);
                // $('#btn_calender').attr("hidden", true);
            }
            if(estado == 2){ //Agendar
                $("#nombre_soli").html('AGENDAR CITA PARA: '+nombre); 
                $('#atender').attr("hidden", true);
                $('#rechazo').attr("hidden", true);
                $('#loading2').attr("hidden", true);
                $('#loading').attr("hidden", true);
                $('#presento').attr("hidden", true);
                $('#tablemaster').attr("hidden", false);
            }
            if(estado == 3){ //Rechazar
                $("#nombre_soli").html('RECHAZAR CITA DE: ' +nombre); 
                // $('#btn_calender').attr("hidden", true);
                $('#atender').attr("hidden", true);
                $('#rechazo').attr("hidden", false);
                $('#loading2').attr("hidden", true);
                $('#loading').attr("hidden", true);
                $('#tablemaster').attr("hidden", true);
                $('#presento').attr("hidden", true);
            }
            if(estado == 4){ //Rechazar
                $("#nombre_soli").html('RECHAZAR CITA DE: ' +nombre); 
                // $('#btn_calender').attr("hidden", true);
                
                $('#presento').attr("hidden", false);
                $('#atender').attr("hidden", true);
                $('#rechazo').attr("hidden", true);
                $('#loading2').attr("hidden", true);
                $('#loading').attr("hidden", true);
                $('#tablemaster').attr("hidden", true);
            }

            

        }
 
       
        function updatecita(){
            if(id_estado == 1){ //Atender
                $('#atender').attr("hidden", true);
                $('#loading').attr("hidden", false);
                $('#btn_calender').attr("hidden", true);
                let data = {"id_cita": id_seleccion, "correo": e_mail, "estado": id_estado, "fecha": $("#fecha_paramt").val(), 'respuesta':$("#paramant_paramt").val()};
                $.ajax({
                    type: 'POST',
                    url: '../CitaSave',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: data
                }).done(function(msg) {
                    if (msg.status == true) {
                        $('#loading').attr("hidden", true);
                        $('#loading2').attr("hidden", false);
                        setTimeout(function(){ 
                            $('#loading2').attr("hidden", true);
                            location.reload();
                        }, 1500);
                    }else{
                        $('#btn_calender').attr("hidden", false);
                    }
                });
            }
            if(id_estado == 2){ //Agendar
                var fecha_rt = $("#fecha_paramt").val(); 
                var paramt_part = $("#paramant_paramt").val();
                if(fecha_rt == null || fecha_rt == undefined || fecha_rt == ''){
                    alert('Para continuar por favor seleccione una fecha ');
                }else{
                    if(paramt_part == null || paramt_part == undefined || paramt_part == ''){
                        alert('Para continuar por favor ingrese una observación ');
                    }else{
                        $('#loading').attr("hidden", false);
                        $('#tablemaster').attr("hidden", true);
                        $('#btn_calender').attr("hidden", true);
                        let data = {"id_cita": id_seleccion,"correo": e_mail, "estado": id_estado, "fecha": fecha_rt, 'respuesta':paramt_part};
                        $.ajax({
                            type: 'POST',
                            url: '../CitaSave',
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            data: data
                        }).done(function(msg) {
                            if (msg.status == true) {
                                $('#loading').attr("hidden", true);
                                $('#loading2').attr("hidden", false);
                                setTimeout(function(){ 
                                    $('#loading2').attr("hidden", true);
                                    location.reload();
                                }, 1500);
                            }else{
                                $('#tablemaster').attr("hidden", false);
                                $('#btn_calender').attr("hidden", false);
                            }
                        });
                    }
                }
            }
            if(id_estado ==3){//Rechazar
                $('#rechazo').attr("hidden", true);
                $('#tablemaster').attr("hidden", true);
                $('#loading').attr("hidden", false);
                $('#btn_calender').attr("hidden", true);
                let data = {"id_cita": id_seleccion, "correo": e_mail, "estado": id_estado, "fecha": $("#fecha_paramt").val(), 'respuesta':$("#paramant_paramt").val()};
                $.ajax({
                    type: 'POST',
                    url: '../CitaSave',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: data
                }).done(function(msg) {
                    if (msg.status == true) {
                        $('#loading').attr("hidden", true);
                        $('#loading2').attr("hidden", false);
                        setTimeout(function(){ 
                            $('#loading2').attr("hidden", true);
                            location.reload();
                        }, 1500);
                    }else{
                        $('#btn_calender').attr("hidden", false);
                    }
                });
            }
            if(id_estado ==4){//Rechazar
                $('#rechazo').attr("hidden", true);
                $('#tablemaster').attr("hidden", true);
                $('#loading').attr("hidden", false);
                $('#btn_calender').attr("hidden", true);
                let data = {"id_cita": id_seleccion, "correo": e_mail, "estado": id_estado, "fecha": $("#fecha_paramt").val(), 'respuesta':$("#paramant_paramt").val()};
                $.ajax({
                    type: 'POST',
                    url: '../CitaSave',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: data
                }).done(function(msg) {
                    if (msg.status == true) {
                        $('#loading').attr("hidden", true);
                        $('#loading2').attr("hidden", false);
                        setTimeout(function(){ 
                            $('#loading2').attr("hidden", true);
                            location.reload();
                        }, 1500);
                    }else{
                        $('#btn_calender').attr("hidden", false);
                    }
                });
            }

        }

</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }


    thead tr {
        color:#000;
        /* background-color:#fff; */
    }

    .DTTT_Print #page-wrapper {
        margin: 0;
        background: #fff;
    }

    button.DTTT_button,
    div.DTTT_button,
    a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    button.DTTT_button:hover,
    div.DTTT_button:hover,
    a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')


<div class="ibox float-e-margins">
    <div class="ibox-title">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <h1 style="margin-bottom: 2%">{{$title}}</h1>
        <table  id="example" class="table table-striped table-bordered table-hover display" >
            <thead>
                @if($tipo==1)
                <tr>
                    <th>Fecha</th>
                    <th>Área</th>
                    <th>Nombre</th>
                    <th>Cédula / PASAPORTE</th>
                    <th>Contácto</th>
                    <th>Observación</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
                @endif
                @if($tipo==2 || $tipo==3)
                <tr>
                    <th>Solicitud</th>
                    @if($tipo==2)
                    <th>Agendado</th>
                    @endif
                    <th>Área</th>
                    <th>Nombre</th>
                    <th>Cédula / PASAPORTE</th>
                    <th>Contácto</th>
                    <th>Observación</th>
                    @if($tipo==2)
                    <th>Indicaciones</th>
                    @endif
                   
                    <th>Estado</th>
                    @if($tipo==2)
                    <th></th>
                    @endif
                </tr>
                @endif
               
            </thead>
            <tbody>
                @foreach($citas->data as $t)
                <tr>
                    <td>{{$t->fecha}}</td>
                    @if($tipo==2)
                    <td>{{$t->fecha_cita}}</td>
                    @endif
                    <td>{{$t->area_direccion}}</td>
                    <td>{{$t->name}}</td>
                    <td>{{$t->cedula}}</td>
                    <td>{{$t->email}}  -  {{$t->telefono}}</td>
                    <td>{{$t->observacion}}</td>
                    @if($tipo==2)
                    <td>{{$t->respuesta}}</td>
                    @endif
                    <td> 
                        @if($t->estado=="SOLICITADO")
                            <span class="badge badge-success">{{$t->estado}}</span></td>
                        @endif
                        @if($t->estado=="AGENDADO")
                            <span class="badge badge-light">{{$t->estado}}</span></td>
                        @endif
                        @if($t->estado=="ATENDIDO")
                            <span class="badge badge-primary">{{$t->estado}}</span></td>
                        @endif
                        @if($t->estado=="RECHAZADO")
                            <span class="badge badge-danger">{{$t->estado}}</span></td>
                        @endif
                         @if($t->estado=="NO SE PRESENTO")
                            <span class="badge badge-warning">{{$t->estado}}</span></td>
                        @endif

                    @if($tipo !=3)
                    <td>
                        @if($tipo==2)
                        <a data-toggle="modal" data-target="#exampleModalScrollable" onclick="metodos(1, '{{$t->id}}','{{$t->name}}','{{$t->email}}')"><i class="fa fa-check-circle-o"></i>Atender</a> &nbsp;
                        @endif
                        @if($tipo==1)
                        <a data-toggle="modal" data-target="#exampleModalScrollable" onclick="metodos(2, '{{$t->id}}','{{$t->name}}','{{$t->email}}')" ><i class="fa fa-calendar"></i>Agendar</a> &nbsp;
                        @endif
                        @if($tipo==1)
                        <a data-toggle="modal" data-target="#exampleModalScrollable" onclick="metodos(3, '{{$t->id}}','{{$t->name}}','{{$t->email}}')"><i class="fa fa-ban"></i>Rechazar</a>
                        @endif
                        @if($tipo==2)
                        <a data-toggle="modal" data-target="#exampleModalScrollable" onclick="metodos(4, '{{$t->id}}','{{$t->name}}','{{$t->email}}')"><i class="fa fa-ban"></i>No se presento</a>
                        @endif
                    </td>
                           @endif
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                @if($tipo==1)
                <tr>
                    <th>Fecha</th>
                    <th>Área</th>
                    <th>Nombre</th>
                    <th>Cédula / PASAPORTE</th>
                    <th>Contácto</th>
                    <th>Observación</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
                @endif
                @if($tipo==2 || $tipo==3)
                <tr>
                    <th>Solicitud</th>
                    @if($tipo==2)
                    <th>Agendado</th>
                    @endif
                    <th>Área</th>
                    <th>Nombre</th>
                    <th>Cédula / PASAPORTE</th>
                    <th>Contácto</th>
                    <th>Observación</th>
                     @if($tipo==2)
                    <th>Indicaciones</th>
                    @endif
                    <th>Estado</th>
                     @if($tipo==2)
                    <th></th>
                    @endif
                </tr>
                @endif
            </tfoot>
        </table>
    </div>
</div>


<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalScrollableTitle" > <label id="nombre_soli"></label></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="text-center" id="atender" hidden="true" >
                <h2>¿Esta seguro(a) de atender la cita?</h2>
            </div>

            <div class="text-center" id="presento" hidden="true" >
                <h2>No se presento, ¿desea continuar?</h2>
            </div>


            <div class="text-center" id="rechazo" hidden="true" >
                <h2>¿Esta seguro(a) de rechazar la cita?</h2>
            </div>

            <div class="text-center" id="loading" hidden="true" >
                <img src="../../img/loading.gif">
            </div>
            <div class="text-center" id="loading2"  hidden="true">
                <img src="../../img/success2.gif" width="300">
            </div> 
           <!--  {!! Form::open(['route' => 'notificarciudadano', 'method' => 'POST']) !!} -->
            <div class="row" id="tablemaster">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>Seleccione una fecha y hora</label>
                        <div class='input-group date' >
                            <input type='text' id='fecha_paramt' name="fecha" data-date-format='YYYY-MM-DD HH:mm'  class="form-control" placeholder="Seleccione una fecha y hora" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>Observaciones</label>
                            <textarea type='text' id='paramant_paramt'   class="form-control" placeholder="Indique las observaciones que se mostraran al usuario"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" id="btn_calender">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" onclick="updatecita()">Guardar</button>
        </div>
        <!-- {!! Form::close() !!} -->
    </div>
  </div>
</div>






@stop